﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Localization;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// 
	/// </summary>
	internal class RegexFormulaEvaluatorHelper {
		#region private variables
		/// <summary>
		/// Private NumberFormat to parse or write double with "." as decimal separator.
		/// </summary>
		private System.Globalization.NumberFormatInfo m_NumberFormat;

		/// <summary>
		/// Private Dictionary to store the temporary Dataseries use in the calculus.
		/// </summary>
		private ConcurrentDictionary<string, DataSerie> m_DataSeriesDictionary;

		/// <summary>
		/// Private Dictionary to store the initial Dataseries.
		/// </summary>
		private ConcurrentDictionary<string, DataSerie> m_InitialDataSeriesDictionary;

		/// <summary>
		/// Private regex to recognize numbers.
		/// </summary>
		private string m_InsideRegex;

		/// <summary>
		/// Private regex to parse brackets ().
		/// </summary>
		private Regex m_BracketsRegex;

		/// <summary>
		/// Private regex to parse power operator: a^b.
		/// </summary>
		private Regex m_PowerRegex;

		/// <summary>
		/// Private regex to parse multiply operator: a*b.
		/// </summary>
		private Regex m_MultiplyRegex;

		/// <summary>
		/// Private regex to parse divide operator: a/b.
		/// </summary>
		private Regex m_DivideRegex;

		/// <summary>
		/// Private regex to parse add operator: a+b.
		/// </summary>
		private Regex m_AddRegex;

		/// <summary>
		/// Private regex to parse subtract operator: a-b.
		/// </summary>
		private Regex m_SubstractRegex;

		/// <summary>
		/// If supplied and an evaluation error occurs, an error message will be added.
		/// </summary>
		private IFormulaEvaluatorContext m_Context;

		/// <summary>
		/// Let's various functions set a flag when they encounter an error
		/// so that the Evaluate method can more easily know when to call OutputErrorNotice.
		/// </summary>
		private bool m_NeedsErrorNotice = false;

		/// <summary>
		/// Solve() sets this initially to capture the state for
		/// ErrorCorrectToZero/One to undo.
		/// </summary>
		private bool m_UndoNeedsErrorNotice = false;

		/// <summary>
		/// Various methods called by Solve() return this value when they encounter
		/// an error. The value of "1" is a flaw as it creates incorrect and
		/// unpredictable calculated results.
		/// Moving forward, this value should be "NaN", allowing the methods
		/// to recognize an error and process it accordingly.
		/// If this code is consumed by any other application than EO,
		/// definitely make this change! Even for EO, it may help to
		/// default to "NaN" and have a compatibility mode on individual formulas
		/// that uses "1". One way to install compatibility mode is to make
		/// a special lead character of the formula as a trigger.
		/// For example, "!formula goes here" uses "!" to switch this to "1".
		/// </summary>
		string m_RepresentsError = "1";

		/// <summary>
		/// EO has a calculation flaw. This option preserves that flaw to 
		/// avoid changing calculations between releases.
		/// The flaw involves methods called by Solve() that return "1"
		/// when there is an error instead of something meaning "error detected".
		/// When true, existing behavior is retained.
		/// When false, it is not and a value of "NaN" is returned.
		/// "NaN" is fully compatible with conversion from string to double,
		/// making the value into Double.NaN. Internally calculations respect Double.NaN.
		/// Defaults to true. However, when moving this code to another project,
		/// it should default to false, or get removed entirely.
		/// </summary>
		public bool CompatibilityMode {
			get { return m_RepresentsError == "1"; }
			set { m_RepresentsError = value ? "1" : constErrorValue; }
		}

		public readonly string constErrorValue = Double.NaN.ToString();

		#endregion

		#region Constructor
		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="series">the initial Dataseries collection.</param>
		/// <param name="context">If supplied and an evaluation error occurs, an error message will be added.</param>
		public RegexFormulaEvaluatorHelper(List<DataSerie> series, IFormulaEvaluatorContext context) {
			UpdateRegexForCulture(null);
			m_DataSeriesDictionary = new ConcurrentDictionary<string, DataSerie>();
			foreach (var s in series) {
				if (s != null && string.IsNullOrEmpty(s.Name) == false) {
					string hash = CryptoHelper.CalculateHash(s.LocalId);
					if (!m_DataSeriesDictionary.ContainsKey(hash))
						m_DataSeriesDictionary[hash] = s;
				}
			}
			m_InitialDataSeriesDictionary = new ConcurrentDictionary<string, DataSerie>(m_DataSeriesDictionary);
			m_Context = context;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="RegexFormulaEvaluatorHelper"/> class.
		/// </summary>
		/// <param name="context">If supplied and an evaluation error occurs, an error message will be added.</param>
		public RegexFormulaEvaluatorHelper(IFormulaEvaluatorContext context) {
			UpdateRegexForCulture(null);
			m_InitialDataSeriesDictionary = m_DataSeriesDictionary = new ConcurrentDictionary<string, DataSerie>();
			m_Context = context;
		}

		/// <summary>
		/// Updates the regex for culture.
		/// </summary>
		/// <param name="KeyLocalizationCulture">The key localization culture.(called with null in the constructor, ansd then for each calcul we check the local culture of the calculated serie)</param>
		private void UpdateRegexForCulture(string KeyLocalizationCulture) {
			m_NumberFormat = System.Threading.Thread.CurrentThread.CurrentUICulture.NumberFormat;

			if (!string.IsNullOrEmpty(KeyLocalizationCulture)) {
				m_NumberFormat = new System.Globalization.CultureInfo(KeyLocalizationCulture, false).NumberFormat;
			}

			m_InsideRegex = @"(-?[0-9a-zA-Z_]*\" + m_NumberFormat.NumberDecimalSeparator + "?[0-9a-zA-Z]*)";

			m_PowerRegex = new Regex(string.Format(@"{0}\^{0}", m_InsideRegex), RegexOptions.Compiled);
			m_MultiplyRegex = new Regex(string.Format(@"{0}\*{0}", m_InsideRegex), RegexOptions.Compiled);
			m_DivideRegex = new Regex(string.Format(@"{0}/{0}", m_InsideRegex), RegexOptions.Compiled);
			m_AddRegex = new Regex(string.Format(@"{0}\+{0}", m_InsideRegex), RegexOptions.Compiled);
			m_SubstractRegex = new Regex(string.Format(@"{0}\-{0}", m_InsideRegex), RegexOptions.Compiled);

			m_BracketsRegex = new Regex(@"([a-z]*)\(([^\(\)]+)\)(\^|!?)", RegexOptions.Compiled);
		}



		/// <summary>
		/// Checks the regex of the current calculated serie versus the current culture in the regex and update it if necessary.
		/// </summary>
		/// <param name="KeyLocalizationCulture">The key localization culture.</param>
		private void CheckRegexForCulture(string KeyLocalizationCulture) {
			if (!string.IsNullOrEmpty(KeyLocalizationCulture)) {
				var numberFormat = new System.Globalization.CultureInfo(KeyLocalizationCulture, false).NumberFormat;
				if (numberFormat.NumberDecimalSeparator != m_NumberFormat.NumberDecimalSeparator) {
					UpdateRegexForCulture(KeyLocalizationCulture);
				}
			}
		}
		#endregion



		/// <summary>
		/// Evaluate a formula a return a Dataserie as a result.
		/// </summary>
		/// <param name="calcul">The calcul.</param>
		/// <returns></returns>
		public DataSerie Evaluate(CalculatedSerie calcul) {
			m_NeedsErrorNotice = false;
			try {
				CheckRegexForCulture(calcul.KeyLocalizationCulture);
				string formula = calcul.Formula.Trim();
				CheckCompatibilityMode(formula);
				var expr = UpdateFormula(formula, calcul.HideUsedSeries);
				expr = LocalizeTextInFormula(expr, calcul.KeyLocalizationCulture);

				Match m = m_BracketsRegex.Match(expr);
				while (m.Success) {
					expr = expr.Replace("(" + m.Groups[2].Value + ")", Solve(m.Groups[2].Value));
					m = m_BracketsRegex.Match(expr);
				}

				var s = Solve(expr);

				DataSerie result = null;
				// a data series was resolved
				if (m_DataSeriesDictionary.ContainsKey(s)) {
					result = m_DataSeriesDictionary[s];
					result = result.Copy(calcul.LocalId);
				}
				else {
					// The value is expected to be a number.
					// Create a simple DataSerie with it.
					double solvedValue;
					if (double.TryParse(s, System.Globalization.NumberStyles.Any, m_NumberFormat, out solvedValue)) {
						result = new DataSerie() { LocalId = calcul.LocalId };
						result.Points.Add(new DataPoint { XDateTime = null, YValue = solvedValue });
					}
					else {
						m_NeedsErrorNotice = true;
						return null;	// Before EO 2.11, FormulaService returns a null for exceptions raised and this logic occurred within an exception
					}
				}
				result.Name = calcul.Name;

				m_DataSeriesDictionary = new ConcurrentDictionary<string, DataSerie>(m_InitialDataSeriesDictionary);
				m_DataSeriesDictionary[result.LocalId] = result;

				if (!result.HasPointsWithValue)
					OutputErrorNotice(Langue.msg_error_calculatedserie_emptyseriesreturned_calcseries, calcul.Name);

				return result;
			}
			catch (Exception) {
				m_NeedsErrorNotice = true;
				return null;	// Before EO 2.11, FormulaService returns a null for exceptions raised
			}

			finally {
				if (m_NeedsErrorNotice) {
					// While we have detected an error, 
					// we don't want to change how calculations worked prior to EO 2.11. 
					// So we just continue with the calculation but inject our notice.
					OutputErrorNotice(Langue.msg_error_calculatedserie_invalidformula_calcseries, calcul.Name);
				}
			}

		}


		/// <summary>
		/// Evaluates the specified formula.
		/// </summary>
		/// <param name="formula">The formula.</param>
		/// <param name="KeyLocalizationCulture">The key localization culture.</param>
		/// <returns></returns>
		public double Evaluate(string formula, string KeyLocalizationCulture = null) {
			m_NeedsErrorNotice = false;
			try {
				CheckRegexForCulture(KeyLocalizationCulture);
				CheckCompatibilityMode(formula);
				var expr = UpdateFormula((formula ?? ""));
				expr = LocalizeTextInFormula(expr, KeyLocalizationCulture);

				Match m = m_BracketsRegex.Match(expr);
				while (m.Success) {
					expr = expr.Replace("(" + m.Groups[2].Value + ")", Solve(m.Groups[2].Value));
					m = m_BracketsRegex.Match(expr);
				}

				var s = Solve(expr);
				double retVal;
				if (double.TryParse(s, System.Globalization.NumberStyles.Any, m_NumberFormat, out retVal)) {
					return retVal;
				}
				else {
					m_NeedsErrorNotice = true;
					return double.NaN;
				}
			}
			catch (Exception) {
				m_NeedsErrorNotice = true;
				return double.NaN;	// Before EO 2.11, FormulaService returns a double.NaN for exceptions raised
			}

			finally {
				// This may occur when we've actually solved with a valid number,
				// but the m_NeedsErrorNotice is true.
				// We don't want to change how calculations worked prior to EO 2.11. 
				// So we just continue with the calculation but inject our notice.
				// This is particularly important when ApplyArithmeticOperation,
				// ApplyAlgebraicFunction, or ApplyDataSeriesProjection decides to return "1"
				// in response to an error. Pre EO 2.11 calculations may be tuned to using that "1"

				if (m_NeedsErrorNotice)
					OutputErrorNotice(Langue.msg_error_calculatedserie_invalidformula, formula);

			}
		}

		/// <summary>
		/// Outputs any error to the FormulaEvaluatorContext or if not supplied,
		/// the TracingService.
		/// </summary>
		/// <param name="msgcode">Localization msg code with "{0}" token.</param>
		/// <param name="tokenreplacement">The value to replace in the {0} token.</param>
		private void OutputErrorNotice(string msgcode, string tokenreplacement) {
			string msg = String.Format(msgcode, tokenreplacement);
			if (m_Context != null) {
				m_Context.ReportError(msg);
			}
			else
				Vizelia.FOL.Providers.TracingService.Write(TraceEntrySeverity.Warning, msg);

		}

		/// <summary>
		/// Updates the formula by replacing the dataseries name with the corresponding hash key.
		/// </summary>
		/// <param name="expr">The expr.</param>
		/// <param name="hideUsedSeries">if set to <c>true</c> [hide used series].</param>
		/// <returns></returns>
		private string UpdateFormula(string expr, bool hideUsedSeries = false) {
			if (this.m_DataSeriesDictionary != null) {
				foreach (var d in this.m_DataSeriesDictionary.OrderByDescending(kvp => kvp.Value.LocalId.Length)) {
					if (expr.IndexOf(d.Value.LocalId) >= 0 && hideUsedSeries) {
						d.Value.Hidden = true;
					}
					expr = expr.Replace(d.Value.LocalId, d.Key);
				}
			}
			//we remove text formating razor could have introduced.
			var retVal = expr.Replace(" , ", "|").Replace(" ", "").Replace("&#160;", "").Replace(m_NumberFormat.NumberGroupSeparator, "").Replace("|", ",");
			return retVal;
		}

		/// <summary>
		/// Localizes the text in formula.
		/// </summary>
		/// <param name="formula">The formula.</param>
		/// <param name="KeyLocalizationCulture">The key localization culture.</param>
		/// <returns></returns>
		private string LocalizeTextInFormula(string formula, string KeyLocalizationCulture) {
			var currentCulture = System.Threading.Thread.CurrentThread.CurrentCulture;
			var currentUICulture = System.Threading.Thread.CurrentThread.CurrentUICulture;

			if (string.IsNullOrEmpty(KeyLocalizationCulture) == false) {
				System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(KeyLocalizationCulture, false);
				System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(KeyLocalizationCulture, false);
			}
			var retVal = Helper.LocalizeInText(formula, "%");

			System.Threading.Thread.CurrentThread.CurrentCulture = currentCulture;
			System.Threading.Thread.CurrentThread.CurrentUICulture = currentUICulture;

			return retVal;
		}

		/// <summary>
		/// Compatibility mode is used to preserve the old and flawed calculations
		/// where "1" was returned in several cases where an error was detected.
		/// The act of using ErrorCorrectToOne or ErrorCorrectToZero is enough to override
		/// the old calculation because it means the user has opted into better handling of errors.
		/// </summary>
		/// <param name="formula"></param>
		private void CheckCompatibilityMode(string formula) {
			CompatibilityMode =
				!formula.Contains(AlgebricFunction.ErrorCorrectToOne + "(", StringComparison.InvariantCultureIgnoreCase)
				&&
				!formula.Contains(AlgebricFunction.ErrorCorrectToZero + "(", StringComparison.InvariantCultureIgnoreCase);
		}

		/// <summary>
		/// Applies an algebric function to a dataserie or a number.
		/// </summary>
		/// <param name="x">the dataserie or the number.</param>
		/// <param name="func">the function to apply.</param>
		/// <returns></returns>
		private string ApplyAlgebricFunction(Match x, AlgebricFunction func) {
			string v1 = x.Groups[1].Value;
			if (string.IsNullOrEmpty(v1) == false) {
				if (m_DataSeriesDictionary.ContainsKey(v1)) {
					var retVal = DataSerieHelper.AlgebricFunction(m_DataSeriesDictionary[v1], func);
					retVal.Name = this.GUID();
					m_DataSeriesDictionary[retVal.Name] = retVal;
					switch (func) {
						case AlgebricFunction.ErrorCorrectToOne:
							m_NeedsErrorNotice = m_UndoNeedsErrorNotice;
							if (!retVal.HasPointsWithValue)
								return "1";
							break;
						case AlgebricFunction.ErrorCorrectToZero:
							m_NeedsErrorNotice = m_UndoNeedsErrorNotice;
							if (!retVal.HasPointsWithValue)
								return "0";
							break;
					}
					return retVal.Name;
				}
				if (v1.IsDouble(m_NumberFormat)) {
					double v = Convert.ToDouble(v1, m_NumberFormat);
					switch (func) {
						case AlgebricFunction.ErrorCorrectToOne:
							m_NeedsErrorNotice = m_UndoNeedsErrorNotice;
							return !Double.IsNaN(v) ? v1 : "1" ;
						case AlgebricFunction.ErrorCorrectToZero:
							m_NeedsErrorNotice = m_UndoNeedsErrorNotice;
							return !Double.IsNaN(v) ? v1 : "0";
					}


					double result = DataSerieHelper.AlgebricFunction(v, func);
					if (Double.IsNaN(result))
						m_NeedsErrorNotice = true;
					var retVal = result.ToString(m_NumberFormat);
					return retVal;
				}
			}
			switch (func) {
				case AlgebricFunction.ErrorCorrectToOne:
					m_NeedsErrorNotice = m_UndoNeedsErrorNotice;
					return "1";
				case AlgebricFunction.ErrorCorrectToZero:
					m_NeedsErrorNotice = m_UndoNeedsErrorNotice;
					return "0";
			}

			m_NeedsErrorNotice = true;
			return m_RepresentsError;
		}


		/// <summary>
		/// Applies an algebric function to a dataserie or a number.
		/// </summary>
		/// <param name="formula">The formula.</param>
		/// <param name="func">the function to apply.</param>
		/// <returns></returns>
		private string ApplyOneParameterFunction(string formula, OneParameterFunction func) {
			string v1 = formula;
			if (string.IsNullOrEmpty(v1) == false) {
				var args = v1.Split(',');
				if (args.Length == 2 || args.Length == 3) {
					var dataSerie = args[0];
					var number = args[1];
					if (args.Length == 3)	// handle localized number where the decimal separate is a comma 
						number += "," + args[2];
					if (m_DataSeriesDictionary.ContainsKey(dataSerie) && number.IsDouble(m_NumberFormat)) {
						var retVal = DataSerieHelper.OneParameterFunction(m_DataSeriesDictionary[dataSerie], Convert.ToDouble(number, m_NumberFormat), func);
						retVal.Name = this.GUID();
						m_DataSeriesDictionary[retVal.Name] = retVal;
						return retVal.Name;
					}
				}
			}
			m_NeedsErrorNotice = true;
			return m_RepresentsError;
		}

		/// <summary>
		/// Applies an algebric function to a dataserie.
		/// </summary>
		/// <param name="x">the dataserie.</param>
		/// <param name="func">the function to apply.</param>
		/// <returns></returns>
		private string ApplyDataSerieProjection(Match x, DataSerieProjection func) {
			string v1 = x.Groups[1].Value;

			if (m_DataSeriesDictionary.ContainsKey(v1)) {
				var retVal = DataSerieHelper.DataSerieProjection(m_DataSeriesDictionary[v1], func);
				retVal.Name = this.GUID();
				m_DataSeriesDictionary[retVal.Name] = retVal;
				return retVal.Name;
			}
			m_NeedsErrorNotice = true;
			return m_RepresentsError;
		}



		/// <summary>
		/// Applies the arithmetic operation between 2 dataseries, 1 dataserie - 1 number, 2 numbers.
		/// </summary>
		/// <param name="x">The regex match.</param>
		/// <param name="op">The op.</param>
		/// <returns></returns>
		private string ApplyArithmeticOperation(Match x, ArithmeticOperator op) {
			string v1 = x.Groups[1].Value;
			string v2 = x.Groups[2].Value;
			// TryParse supports integer in addition to floating point formats
			double v1Number;
			bool hasV1Number = true;
			// expect that "NaN" is converted to Double.NaN here.
			// If that happens, the hasVxNumber variable will stay true.
			// Number handling functions should process Double.NaN.
			if (!double.TryParse(v1, System.Globalization.NumberStyles.Any, m_NumberFormat, out v1Number)) {
				v1Number = double.NaN;
				hasV1Number = false;
			}
			double v2Number;
			bool hasV2Number = true;
			if (!double.TryParse(v2, System.Globalization.NumberStyles.Any, m_NumberFormat, out v2Number)) {
				v2Number = double.NaN;
				hasV2Number = false;
			}


			// number1 operator number2
			if (hasV1Number && hasV2Number) {
				
				// Instead of blocking the call, we want the call to return the same value it did pre EO 2.11
				if (op == ArithmeticOperator.Divide && v2Number == 0.0) {
					m_NeedsErrorNotice = true;
					if (!CompatibilityMode)
						return constErrorValue;
				}

				var retValNumeric = DataSerieHelper.ArithmeticOperation(v1Number, v2Number, op);
				if (Double.IsNaN(retValNumeric)) {
					m_NeedsErrorNotice = true;
					if (!CompatibilityMode)
						return constErrorValue;
				}
				return retValNumeric.ToString(m_NumberFormat);


			}
			// +number and -number
			if (string.IsNullOrEmpty(v1) && hasV2Number) {
				if (!CompatibilityMode && Double.IsNaN(v2Number))
					return v2;
				switch (op) {
					case ArithmeticOperator.Add:
						return v2;

					case ArithmeticOperator.Subtract:
						return "-" + v2;
				}
			}

			DataSerie retVal = null;
			try {
				// series1 operator series2
				if (m_DataSeriesDictionary.ContainsKey(v1) && m_DataSeriesDictionary.ContainsKey(v2)) {
					retVal = DataSerieHelper.ArithmeticOperation(m_DataSeriesDictionary[v1], m_DataSeriesDictionary[v2], op);
				}
				// series1 operator number2
				else if (m_DataSeriesDictionary.ContainsKey(v1) && !m_DataSeriesDictionary.ContainsKey(v2) && hasV2Number) {
					if (!CompatibilityMode && Double.IsNaN(v2Number))
						return constErrorValue;

					// Instead of blocking the call, we want the call to return the same value it did pre EO 2.11
					if (op == ArithmeticOperator.Divide && v2Number == 0.0)
						m_NeedsErrorNotice = true;
					retVal = DataSerieHelper.ArithmeticOperation(m_DataSeriesDictionary[v1], v2Number, op);
				}
				// number1 operator series2
				else if (!m_DataSeriesDictionary.ContainsKey(v1) && m_DataSeriesDictionary.ContainsKey(v2) && hasV1Number) {
	
					if (!CompatibilityMode && Double.IsNaN(v1Number))
						return constErrorValue;
					retVal = DataSerieHelper.ArithmeticOperation(v1Number, m_DataSeriesDictionary[v2], op);
				}
				// series1 operator series2 when series2 doesn't exist
				else if (m_DataSeriesDictionary.ContainsKey(v1) && !m_DataSeriesDictionary.ContainsKey(v2) && !hasV2Number) {
					if (op == ArithmeticOperator.Divide) {
						m_NeedsErrorNotice = true;
						if (!CompatibilityMode)
							return constErrorValue;
					}
					//NOTE: This is a poor handling of the problem.
					// When a series name is unknown, it should only be an error.
					// That's how it works if it was [number] [op] [invalid series name].
					// Only when the series name is empty should we apply 0.
					// In addition, we may want to apply 1 for multiply or divide.
					if (!String.IsNullOrEmpty(v2)) {
						m_NeedsErrorNotice = true;
					} 
					retVal = DataSerieHelper.ArithmeticOperation(m_DataSeriesDictionary[v1], 0, op);

#if false // this is the recommended replacement code
					if (!String.IsNullOrEmpty(v1)) {
						retVal = null;
					}
					else {
						switch (op) {
							case ArithmeticOperator.Multiply:
							case ArithmeticOperator.Divide:
								retVal = DataSerieHelper.ArithmeticOperation(m_DataSeriesDictionary[v2], 1, op);
								break;
							default:
								retVal = DataSerieHelper.ArithmeticOperation(m_DataSeriesDictionary[v2], 0, op);
								break;
						}
					}
#endif
				}
				// series1 operator series2 when series1 doesn't exist
				else if (!m_DataSeriesDictionary.ContainsKey(v1) && m_DataSeriesDictionary.ContainsKey(v2) && !hasV1Number) {
					//NOTE: This is a poor handling of the problem.
					// When a series name is unknown, it should only be an error.
					// That's how it works if it was [invalid series name] [op] [number].
					// Only when the series name is empty should we apply 0.
					// In addition, we may want to apply 1 for multiply or divide.
					if (!String.IsNullOrEmpty(v1)) {
						m_NeedsErrorNotice = true;
					}
					retVal = DataSerieHelper.ArithmeticOperation(0, m_DataSeriesDictionary[v2], op);
#if false // this is the recommended replacement code
					if (!String.IsNullOrEmpty(v1)) {
						retVal = null;
					}
					else {
						switch (op) {
							case ArithmeticOperator.Multiply:
							case ArithmeticOperator.Divide:
								retVal = DataSerieHelper.ArithmeticOperation(1, m_DataSeriesDictionary[v2], op);
								break;
							default:
								retVal = DataSerieHelper.ArithmeticOperation(0, m_DataSeriesDictionary[v2], op);
								break;
						}
					}
#endif
				}
			}
			catch (Exception) {
				m_NeedsErrorNotice = true;
			}

			if (retVal != null) {
				retVal.Name = this.GUID();
				m_DataSeriesDictionary[retVal.Name] = retVal;
				return retVal.Name;
			}
			m_NeedsErrorNotice = true;
			return m_RepresentsError;
		}
		/// <summary>
		/// Parse the formula or a part of the formula and return the result as string.
		/// </summary>
		/// <param name="expr"></param>
		/// <returns></returns>
		private string Solve(string expr) {

			// resolve ErrorCorrectToZero/One early to allow it to reverse m_NeedsErrorNotice
			// using the last value of m_UndoNeedsErrorNotice.
			// When these are found, no further processing is expected.
			// This special case allows case insensitive matching to functions, unlike the rest
			// by request on user story 6498.
			expr = ErrorCorrect(AlgebricFunction.ErrorCorrectToZero, expr);
			expr = ErrorCorrect(AlgebricFunction.ErrorCorrectToOne, expr);

			m_UndoNeedsErrorNotice = m_NeedsErrorNotice;

			Enum.GetNames(typeof(AlgebricFunction)).ToList().ForEach(function => {
				var functionRegex = new Regex(function.ToLower() + m_InsideRegex, RegexOptions.Compiled);
				if (expr.IndexOf(function.ToLower()) != -1)
					expr = Do(functionRegex, expr, (x) => ApplyAlgebricFunction(x, function.ParseAsEnum<AlgebricFunction>()));
			});

			Enum.GetNames(typeof(OneParameterFunction)).ToList().ForEach(function => {
				var functionRegex = new Regex(function.ToLower() + m_InsideRegex, RegexOptions.Compiled);
				if (expr.IndexOf(function.ToLower()) != -1)
					expr = DoSimple(function.ToLower(), expr, (x) => ApplyOneParameterFunction(x, function.ParseAsEnum<OneParameterFunction>()));
			});


			Enum.GetNames(typeof(DataSerieProjection)).ToList().ForEach(function => {
				var functionRegex = new Regex(function.ToLower() + m_InsideRegex, RegexOptions.Compiled);
				if (expr.IndexOf(function.ToLower()) != -1)
					expr = Do(functionRegex, expr, (x) => ApplyDataSerieProjection(x, function.ParseAsEnum<DataSerieProjection>()));
			});

			//if (expr.IndexOf("sqrt") != -1)
			//    expr = Do(m_SqrtRegex, expr, (x) => ApplyAlgebricFunction(x, AlgebricFunction.Sqrt));

			if (expr.IndexOf("^", StringComparison.Ordinal) != -1)
				expr = Do(m_PowerRegex, expr, (x) => ApplyArithmeticOperation(x, ArithmeticOperator.Power));


			if (expr.IndexOf("/", StringComparison.Ordinal) != -1)
				expr = Do(m_DivideRegex, expr, (x) => ApplyArithmeticOperation(x, ArithmeticOperator.Divide));


			if (expr.IndexOf("*", StringComparison.Ordinal) != -1)
				expr = Do(m_MultiplyRegex, expr, (x) => ApplyArithmeticOperation(x, ArithmeticOperator.Multiply));

			if (expr.IndexOf("+", StringComparison.Ordinal) != -1)
				expr = Do(m_AddRegex, expr, (x) => ApplyArithmeticOperation(x, ArithmeticOperator.Add));

			if (expr.IsDouble(m_NumberFormat)) {
				return expr;
			}
			if (expr.IndexOf("-", StringComparison.Ordinal) != -1)
				expr = Do(m_SubstractRegex, expr, (x) => ApplyArithmeticOperation(x, ArithmeticOperator.Subtract));
			return expr;
		}

		/// <summary>
		/// Handles an ErrorCorrectToZero/One function. If the function is found, it is run
		/// and this returns true, along with the result from ErrorCorrectToZero/One.
		/// Otherwise it returns false and the original value in resolveExpr.
		/// </summary>
		/// <param name="func">Must be ErrorCorrectToZero or ErrorCorrectToOne</param>
		/// <param name="expr">The expression to parse</param>
		/// <param name="resolveExpr">The expression the caller should used next.</param>
		/// <returns>When true, the function was found. When false, it was not.</returns>
		private string ErrorCorrect(AlgebricFunction func, string expr){
			var functionRegex = new Regex(func.ToString() + m_InsideRegex, RegexOptions.Compiled | RegexOptions.IgnoreCase);
			if (expr.IndexOf(func.ToString(), StringComparison.InvariantCultureIgnoreCase) != -1) {
				return Do(functionRegex, expr, (x) => ApplyAlgebricFunction(x,func));
			}
			return expr;
		}

		/// <summary>
		/// Update and calculate the formula.
		/// </summary>
		/// <param name="regex">the regex to use.</param>
		/// <param name="formula">the formula.</param>
		/// <param name="func">the function.</param>
		/// <returns></returns>
		private string Do(Regex regex, string formula, Func<Match, string> func) {
			Match m = regex.Match(formula);
			while (m.Success) {
				var r = func(m);
				formula = formula.Replace(m.Groups[0].Value, r);
				//for negative result, we dont want to parse it twice.
				if (formula.IsDouble(m_NumberFormat)) {
					return formula;
				}
				if (formula.Contains(",")) {
					var arr = formula.Split(',');
					if (arr.Length == 2 && m_DataSeriesDictionary.ContainsKey(arr[0]) && arr[1].IsDouble(m_NumberFormat))
						return formula;
				}
				m = regex.Match(formula);
			}
			return formula;
		}

		/// <summary>
		/// Update and calculate the formula.
		/// </summary>
		/// <param name="functionName">Name of the function.</param>
		/// <param name="formula">the formula.</param>
		/// <param name="func">the function.</param>
		/// <returns></returns>
		private string DoSimple(string functionName, string formula, Func<string, string> func) {
			var r = func(formula.Replace(functionName, ""));
			formula = r;
			return formula;
		}

		#region Utils

		/// <summary>
		/// Generate a temporary Guid to be used as a DataSerie name in the calculus.
		/// </summary>
		/// <returns></returns>
		private string GUID() {
			return "serie" + Guid.NewGuid().ToString().Replace("-", "");
		}
		#endregion
	}
}

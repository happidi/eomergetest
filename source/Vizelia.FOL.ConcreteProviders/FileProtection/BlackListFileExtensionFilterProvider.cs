﻿using System;
using System.IO;
using System.Linq;
using Vizelia.FOL.Providers;
using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.ConcreteProviders {

	/// <summary>
	/// Any extension in this provider will not be allowed to be uploaded to the system
	/// </summary>
	public class BlackListFileExtensionFilterProvider : FileProtectionProvider {
		/// <summary>
		/// Validates a document is safe to process in the system.
		/// </summary>
		/// <param name="document">The document.</param>
		/// <returns>
		/// True if validation succeeded, false otherwise
		/// </returns>
		public override bool Validate(Document document) {
			try {
				var extensions = Extensions.ToLower().Split(',').ToList();
				int indexOfDot = document.DocumentName.LastIndexOf('.');

				if (indexOfDot == -1) {
					// No extension.
					return true;
				}


				//AmirTODO- replace with Path.GetExtension() and in white list
				string documentExtension = document.DocumentName.Substring(indexOfDot + 1).ToLower();

				if (string.Empty.Equals(documentExtension)) {
					// No extension.
					return true;
				}

				bool matches = extensions.Contains(documentExtension);

				return !matches;
			}
			catch (Exception) {
				return false;
			}
		}
	}
}

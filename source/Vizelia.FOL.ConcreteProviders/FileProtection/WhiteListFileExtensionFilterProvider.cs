﻿using System;
using System.Linq;
using Vizelia.FOL.Providers;
using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// Only extensions in this provider will be allowed to be uploaded to the system
	/// </summary>
	public class WhiteListFileExtensionFilterProvider : FileProtectionProvider {
		/// <summary>
		/// Validates a document is safe to process in the system.
		/// </summary>
		/// <param name="document">The document.</param>
		/// <returns>
		/// True if validation succeeded, false otherwise
		/// </returns>
		public override bool Validate(Document document) {
			try {
				var extensions = Extensions.ToLower().Split(',').ToList();
				int indexOfDot = document.DocumentName.LastIndexOf('.');

				if (indexOfDot == -1) {
					// No extension.
					return false;
				}

				string documentExtension = document.DocumentName.Substring(indexOfDot + 1).ToLower();

				if (string.Empty.Equals(documentExtension)) {
					// No extension.
					return false;
				}

				bool matches = extensions.Contains(documentExtension);

				return matches;
			}
			catch (Exception) {
				return false;
			}
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Vizelia.FOL.BusinessEntities;
using System.Threading.Tasks;
using System.Threading;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// File Helper for 
	/// </summary>
	public static class MeterDataStorageHelper {

		/// <summary>
		/// Filters the specified sorted list.
		/// </summary>
		/// <param name="innerList">The inner list.</param>
		/// <param name="startDate">The start date.</param>
		/// <param name="includeStartDate">if set to <c>true</c> [include start date].</param>
		/// <param name="endDate">The end date.</param>
		/// <param name="includeEndDate">if set to <c>true</c> [include end date].</param>
		/// <returns></returns>
		public static Dictionary<DateTime, MD> Filter(SortedList<DateTime, MD> innerList, DateTime? startDate = null, bool includeStartDate = true, DateTime? endDate = null, bool includeEndDate = false) {
			var retVal = new Dictionary<DateTime, MD>();

			if (startDate.HasValue && endDate.HasValue && innerList != null) {
				var sortedList = innerList;

				var indexLow = sortedList.FindFirstIndexGreaterThanOrEqualTo(startDate.Value);
				if (indexLow >= 0) {
					if (sortedList.Keys[indexLow] == startDate.Value && includeStartDate == false)
						indexLow += 1;

					while (indexLow < innerList.Count) {
						var key = sortedList.Keys[indexLow];
						var value = sortedList.Values[indexLow];
						if (key < endDate.Value || (key == endDate.Value && includeEndDate)) {
							retVal.Add(key, value);
							indexLow++;
						}
						else
							break;
					}
				}
			}
			return retVal;
		}

		/// <summary>
		/// Filters the specified sorted list.
		/// </summary>
		/// <param name="innerList">The inner list.</param>
		/// <param name="startDate">The start date.</param>
		/// <param name="includeStartDate">if set to <c>true</c> [include start date].</param>
		/// <param name="endDate">The end date.</param>
		/// <param name="includeEndDate">if set to <c>true</c> [include end date].</param>
		/// <returns></returns>
		public static List<MD> FilterAsList(SortedList<DateTime, MD> innerList, DateTime? startDate , bool includeStartDate , DateTime? endDate, bool includeEndDate) {
			var retVal = new List<MD>();
			
			//These lines should be deleted soon, right now im leaving them in case we see that this refactoring was not good.
			//if (innerList is SortedList<DateTime, MD>) {
				if (startDate.HasValue && endDate.HasValue && innerList != null && innerList.Count > 0) {
					var sortedList = innerList;// as SortedList<DateTime, MD>;

					var indexLow = sortedList.FindFirstIndexGreaterThanOrEqualTo(startDate.Value);
					if (indexLow >= 0 && indexLow < innerList.Count) {
						//NOTE: This is if we want to make the change that accumulated meters will take an additional data point outside the given dates.
						////if the meter is accumlated - it means we need two points to create a point, because we need a delta to create our point.
						////So if the chart is set up to be from 1/1 to 31/12 for monthly data, we only have 12 original points, meaning we can create only 11 data points.
						////However the customers wanted to get 12 data points, so we need to "cheat" and bring in another data point, either before or after, based on if the acquistion 
						////is the end of the interval.
						//if (meter.IsAccumulated && meter.IsAcquisitionDateTimeEndInterval && indexLow > 0) {
						//    //Take an extra one before
						//    indexLow--;
						//}
						if (sortedList.Keys[indexLow] == startDate.Value && includeStartDate == false)
							indexLow++;

						while (indexLow < innerList.Count) {
							var key = sortedList.Keys[indexLow];
							var value = sortedList.Values[indexLow];
							if (key < endDate.Value || (key == endDate.Value && includeEndDate)) {
								retVal.Add(value);
								indexLow++;
							}
							else {
								//NOTE: This is if we want to make the change that accumulated meters will take an additional data point outside the given dates.
								//if (meter.IsAccumulated && !meter.IsAcquisitionDateTimeEndInterval) {
								//    //Take an extra one after.
								//    retVal.Add(value);
								//}
								break;
							}
						}
					}
				}
				return retVal;
			//}
			//else {
			//    if (startDate.HasValue && endDate.HasValue && innerList != null) {
			//        if (!includeStartDate && !includeEndDate) {
			//            foreach (KeyValuePair<DateTime, MD> kvp in innerList) {
			//                if (kvp.Key > startDate && kvp.Key < endDate) {
			//                    retVal.Add(kvp.Value);
			//                }
			//            }
			//        }
			//        else if (includeStartDate && !includeEndDate) {
			//            foreach (KeyValuePair<DateTime, MD> kvp in innerList) {
			//                if (kvp.Key >= startDate && kvp.Key < endDate) {
			//                    retVal.Add(kvp.Value);
			//                }
			//            }
			//        }
			//        else if (includeStartDate && includeEndDate) {
			//            foreach (KeyValuePair<DateTime, MD> kvp in innerList) {
			//                if (kvp.Key >= startDate && kvp.Key <= endDate) {
			//                    retVal.Add(kvp.Value);
			//                }
			//            }
			//        }
			//        else {
			//            foreach (KeyValuePair<DateTime, MD> kvp in innerList) {
			//                if (kvp.Key > startDate && kvp.Key <= endDate) {
			//                    retVal.Add(kvp.Value);
			//                }
			//            }
			//        }
			//    }
			//    return retVal;
			//}
		}
	}
}

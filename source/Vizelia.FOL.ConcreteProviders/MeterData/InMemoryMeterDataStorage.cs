﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.ConcreteProviders {
	internal class InMemoryMeterDataStorage : IMeterDataStorage {
		/// <summary>
		/// Static InMemory Collection
		/// </summary>
		private ConcurrentDictionary<int, SortedList<DateTime, MD>> m_MeterDatas = new ConcurrentDictionary<int, SortedList<DateTime, MD>>();

		/// <summary>
		/// Adds the specified key meter.
		/// </summary>
		/// <param name="keyMeter">The key meter.</param>
		/// <param name="values">The values.</param>
		/// <returns></returns>
		public bool Add(int keyMeter, IDictionary<DateTime, MD> values) {
			m_MeterDatas[keyMeter] = new SortedList<DateTime, MD>(values);
			return true;
		}

		/// <summary>
		/// Gets the meter data count.
		/// </summary>
		/// <returns></returns>
		public int GetMeterDataCount() {
			return m_MeterDatas.Sum(kvp => kvp.Value.Count);
		}

		/// <summary>
		/// Gets the or add.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <param name="valueFactory">The value factory.</param>
		/// <returns></returns>
		public IDictionary<DateTime, MD> GetOrAdd(int key, Func<int, IDictionary<DateTime, MD>> valueFactory) {
			Func<int, SortedList<DateTime, MD>> valueFactoryWithCast = (i) => new SortedList<DateTime, MD>(valueFactory(i));
			return m_MeterDatas.GetOrAdd(key, valueFactoryWithCast);
		}

		/// <summary>
		/// Inits this instance.
		/// </summary>
		public void Init() {
			m_MeterDatas = new ConcurrentDictionary<int, SortedList<DateTime, MD>>();
		}

		/// <summary>
		/// Gets or sets the Dictionary DateTime,MD  with the specified key.
		/// </summary>
		public IDictionary<DateTime, MD> this[int key] {
			get {
				return m_MeterDatas[key];
			}
			set {
				m_MeterDatas[key] = new SortedList<DateTime, MD>(value);
			}
		}


		/// <summary>
		/// Tries the get value.
		/// </summary>
		/// <param name="keyMeter">The key meter.</param>
		/// <param name="value">The value.</param>
		/// <param name="startDate">The start date.</param>
		/// <param name="includeStartDate">if set to <c>true</c> [include start date].</param>
		/// <param name="endDate">The end date.</param>
		/// <param name="includeEndDate">if set to <c>true</c> [include end date].</param>
		/// <returns></returns>
		public bool TryGetValue(int keyMeter, out IDictionary<DateTime, MD> value, DateTime? startDate = null, bool includeStartDate = true, DateTime? endDate = null, bool includeEndDate = false) {
			SortedList<DateTime, MD> innerList;
			var retVal = m_MeterDatas.TryGetValue(keyMeter, out innerList);
			value = MeterDataStorageHelper.Filter(innerList, startDate, includeStartDate, endDate, includeEndDate);
			return retVal;
		}


		/// <summary>
		/// Tries the get value.
		/// </summary>
		/// <param name="keyMeter">The key meter.</param>
		/// <param name="value">The value.</param>
		/// <param name="startDate">The start date.</param>
		/// <param name="includeStartDate">if set to <c>true</c> [include start date].</param>
		/// <param name="endDate">The end date.</param>
		/// <param name="includeEndDate">if set to <c>true</c> [include end date].</param>
		/// <returns></returns>
		public bool TryGetValueAsList(int keyMeter, out List<MD> value, DateTime? startDate , bool includeStartDate , DateTime? endDate , bool includeEndDate) {
		//NOTE: This is if we want to make the change that accumulated meters will take an additional data point outside the given dates.
		//public bool TryGetValueAsList(int keyMeter, out List<MD> value, DateTime? startDate , bool includeStartDate , DateTime? endDate , bool includeEndDate, Meter meter) {
			SortedList<DateTime, MD> innerList;
			var retVal = m_MeterDatas.TryGetValue(keyMeter, out innerList);
			
			//This list filters the entire data for the the given meter by the dates  we are working on and returns the data as a list. Consider renaming the method to something more clear.
			//NOTE: This is if we want to make the change that accumulated meters will take an additional data point outside the given dates.
			//value = MeterDataStorageHelper.FilterAsList(innerList, startDate, includeStartDate, endDate, includeEndDate,meter);
			value = MeterDataStorageHelper.FilterAsList(innerList, startDate, includeStartDate, endDate, includeEndDate);
			return retVal;
		}



		/// <summary>
		/// Adds the specified data.
		/// </summary>
		/// <param name="values">The values.</param>
		/// <returns></returns>
		public bool AddRange(ConcurrentDictionary<int, SortedList<DateTime, MD>> values) {
			this.m_MeterDatas = values;
			return true;
		}

	}
}
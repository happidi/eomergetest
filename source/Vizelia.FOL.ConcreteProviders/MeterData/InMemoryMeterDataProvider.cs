﻿using Vizelia.FOL.Providers;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// In-memory data provider
	/// </summary>
	public class InMemoryMeterDataProvider : MeterDataProvider {

		
		
		/// <summary>
		/// Gets the storage.
		/// </summary>
		/// <returns></returns>
		public override IMeterDataStorage GetStorage() {
			var storage = new InMemoryMeterDataStorage();
			return storage;
		}
	}
}

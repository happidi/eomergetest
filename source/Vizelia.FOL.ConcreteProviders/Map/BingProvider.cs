﻿using System;
using System.Globalization;
using System.Linq;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using Vizelia.FOL.Providers;
using System.Configuration.Provider;
using System.Net.Mail;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.ServiceAgents;
using System.Xml.Linq;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// Concrete implementation of a standard mail provider using the SmtpClient.
	/// </summary>
	public class BingProvider : MapProvider {



		private const string const_attribute_key = "key";

		private const string const_namespace = "http://schemas.microsoft.com/search/local/ws/rest/v1";


		/// <summary>
		/// Gets or sets the key.
		/// </summary>
		/// <value>
		/// The key.
		/// </value>
		protected string Key { get; set; }
		/// <summary>
		/// Initializes the provider.
		/// </summary>
		/// <param name="name">The provider name.</param>
		/// <param name="config">The collection of config attributes.</param>
		public override void Initialize(string name, System.Collections.Specialized.NameValueCollection config) {
			base.Initialize(name, config);

			if (String.IsNullOrEmpty(config[const_attribute_key])) {
				throw new ProviderException(const_attribute_key + " is missing");
			}
			Key = config[const_attribute_key];
			config.Remove(const_attribute_key);

			if (config.Count > 0)
				throw new ProviderException(String.Format("attribute {0} is unauthorized", config.GetKey(0)));
		}



		/// <summary>
		/// Gets the Key to use with the current provider.
		/// </summary>
		/// <returns></returns>
		public override string GetKey() {
			return this.Key;
		}
		/// <summary>
		/// Returns a list of WeatherLocation based on a search string.
		/// </summary>
		/// <param name="address">the address to search for.</param>
		/// <returns></returns>
		public override MapLocation GetLocationFromAddress(string address) {
			if (string.IsNullOrEmpty(address))
				throw new VizeliaException("Address empty");
			var location = CacheService.Get<MapLocation>(CacheKey.const_cache_bing_map_location + address, () => GetLocation(address),0);
			return location;
		}

        /// <summary>
        /// Gets the location.
        /// </summary>
        /// <param name="address">The address.</param>
        /// <returns></returns>
        private MapLocation GetLocation(string address)
        {
            MapLocation mapLocation = null;
            var bingServiceAgent = new BingServiceAgent();
            try {
                dynamic location = bingServiceAgent.GetLocationFromAddressJson(address, Key);
                if (location.resourceSets[0].estimatedTotal == 0) {
                    mapLocation = new MapLocation { IsValid = false };
                }
                else {
                    mapLocation = new MapLocation
                    {
                        Address = location.resourceSets[0].resources[0].name,
                        Latitude = location.resourceSets[0].resources[0].point.coordinates[0],
                        Longitude = location.resourceSets[0].resources[0].point.coordinates[1],
                        IsValid = true
                    };
                }
            }
            catch (Exception ex) {
                TracingService.Write(ex);
            }
         
            return mapLocation;
        }
	}
}

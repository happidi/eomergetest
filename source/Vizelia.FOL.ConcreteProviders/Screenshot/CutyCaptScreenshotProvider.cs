﻿using System;
using System.Configuration.Provider;
using System.Diagnostics;
using System.Web;
using System.Web.Security;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Configuration;
using Vizelia.FOL.Providers;
using System.IO;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// CutyCapt implementation of ScreenshotProvider
	/// </summary>
	public class CutyCaptScreenshotProvider : ScreenshotProvider {

		private const string const_attribute_tempdirectory = "tempDirectory";

		private const string const_attribute_exerelativepath = "exeRelativePath";

		private const string const_attribute_screenshotserviceurl = "screenshotServiceUrl";

		private const string const_attribute_printscreenusername = "printscreenUsername";

		private const string const_attribute_printscreenprovider = "printscreenProvider";


		/// <summary>
		/// Initializes the provider.
		/// </summary>
		/// <param name="name"></param>
		/// <param name="config"></param>
		public override void Initialize(string name, System.Collections.Specialized.NameValueCollection config) {
			base.Initialize(name, config);

			if (String.IsNullOrEmpty(config[const_attribute_tempdirectory])) {
				TempDirectory = VizeliaConfiguration.Instance.Deployment.TempDirectory;
			}
			else {
				TempDirectory = config[const_attribute_tempdirectory];
				if (!Directory.Exists(TempDirectory))
					throw new ProviderException(const_attribute_tempdirectory + " does not exist");
				TempDirectory = Path.GetFullPath(TempDirectory);
			}
			config.Remove(const_attribute_tempdirectory);


			if (String.IsNullOrEmpty(config[const_attribute_exerelativepath])) {
				throw new ProviderException(const_attribute_exerelativepath + " is missing");
			}
			ExeRelativePath = config[const_attribute_exerelativepath];
			config.Remove(const_attribute_exerelativepath);

			if (String.IsNullOrEmpty(config[const_attribute_screenshotserviceurl])) {
				throw new ProviderException(const_attribute_screenshotserviceurl + " is missing");
			}
			ScreenshotServiceUrl = config[const_attribute_screenshotserviceurl];
			config.Remove(const_attribute_screenshotserviceurl);


			if (String.IsNullOrEmpty(config[const_attribute_printscreenusername])) {
				PrintscreenUsername = VizeliaConfiguration.Instance.Authentication.PrintscreenUsername;
			}
			else {
				PrintscreenUsername = config[const_attribute_printscreenusername];
			}
			config.Remove(const_attribute_printscreenusername);

			if (String.IsNullOrEmpty(config[const_attribute_printscreenprovider])) {
				PrintscreenProvider = VizeliaConfiguration.Instance.Authentication.DefaultProvider;
			}
			else {
				PrintscreenProvider = config[const_attribute_printscreenprovider];
			}
			config.Remove(const_attribute_printscreenprovider);



			if (config.Count > 0)
				throw new ProviderException(String.Format("attribute {0} is unauthorized", config.GetKey(0)));
		}


		/// <summary>
		/// Gets or sets the printscreen provider.
		/// </summary>
		/// <value>
		/// The printscreen provider.
		/// </value>
		public string PrintscreenProvider { get; set; }

		/// <summary>
		/// Gets or sets the printscreen username.
		/// </summary>
		/// <value>
		/// The printscreen username.
		/// </value>
		public string PrintscreenUsername { get; set; }

		/// <summary>
		/// Gets or sets the relative of the exe responsible for generating the image.
		/// </summary>
		/// <value>
		/// The relative path of the exe.
		/// </value>
		public string ExeRelativePath { get; set; }

		/// <summary>
		/// Gets or sets the temp directory location.
		/// </summary>
		/// <value>
		/// The temp directory location.
		/// </value>
		public string TempDirectory { get; set; }


		/// <summary>
		/// Gets or sets the Screenshot service URL.
		/// </summary>
		public string ScreenshotServiceUrl { get; set; }

		/// <summary>
		/// Gets the screen shot.
		/// </summary>
		/// <param name="url">The URL.</param>
		/// <param name="localUrl">if set to <c>true</c> the url is local and the servicescreenshoturl will be appended.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <param name="outputType">Type of the output.</param>
		/// <param name="mimeType">Mimetype.</param>
		/// <param name="keyLocalisationCulture">Culture.</param> 
		/// <param name="retryNumber"></param>
		/// <returns></returns>
		public override StreamResult GetScreenshot(string url, bool localUrl, int width, int height, ScreenshotType outputType, string mimeType,
			string keyLocalisationCulture, int retryNumber = 0) {
			string exeFile = AppDomain.CurrentDomain.BaseDirectory + ExeRelativePath;
			string resultFile = Path.Combine(TempDirectory, "Screenshot" + Guid.NewGuid() + "." + outputType.ParseAsString().ToLower());

			if (localUrl) {
				url = Helper.GetFrontendUrl(this.ScreenshotServiceUrl + url, VizeliaConfiguration.Instance.Deployment.FrontendUrl);
			}

			HttpCookie authCookie = HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName];
			string encTicket = authCookie.Value;

			url += string.Format("&username={0}&provider={1}&timeout={2}&formsvalue={3}", this.PrintscreenUsername, this.PrintscreenProvider, 20000, encTicket);

			//string requestId = ServerNetworkingHelper.CreateInnerServerCallIdentifier();
			//Url += "&" + ServerNetworkingHelper.const_inner_server_network_call + "=" + requestId;

			string customArguments = string.Format("--url={0} --out={1} --min-width={2} --min-height={3} --delay={4}", url, resultFile, width, height);
			// doesn't have to be a const as this will be refactored into parameters if needed
			string fixedArguments = "--javascript=on --java=on --plugins=on --js-can-open-windows=on --js-can-access-clipboard=on --private-browsing=off";

			string arguments = customArguments + " " + fixedArguments;
			string cutyCaptOutput;

			using (Process p = new Process {
				StartInfo = {
					FileName = exeFile,
					Arguments = string.Format(arguments),
					CreateNoWindow = false,
					UseShellExecute = false,
					RedirectStandardError = true,
					RedirectStandardOutput = true
				}
			}) {

				p.Start();
				p.WaitForExit();
				cutyCaptOutput = p.StandardOutput.ReadToEnd();
			}
			if (!string.IsNullOrEmpty(cutyCaptOutput))
				throw new Exception(cutyCaptOutput);

			//var retVal = new StreamResult(ImageHelper.ConvertImageFileToMemoryStream(resultFile, true), mimeType);
			var retVal = new StreamResult(resultFile, mimeType, true);

			return retVal;

		}
	}
}

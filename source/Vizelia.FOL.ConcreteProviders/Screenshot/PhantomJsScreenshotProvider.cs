﻿using System;
using System.Collections.Specialized;
using System.Configuration.Provider;
using System.Diagnostics;
using System.IO;
using System.Web;
using System.Web.Security;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Configuration;
using Vizelia.FOL.Common.Localization;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// PhantomJs implementation of ScreenshotProvider
	/// </summary>
	public class PhantomJsScreenshotProvider : ScreenshotProvider {

		private const string const_attribute_tempdirectory = "tempDirectory";

		private const string const_attribute_exerelativepath = "exeRelativePath";

		private const string const_attribute_screenshotserviceurl = "screenshotServiceUrl";

		private const string const_attribute_printscreenusername = "printscreenUser";

		private const string const_attribute_printscreenprovider = "printscreenProvider";

		private const int const_max_retry_number = 3;
		private const int const_max_time_to_wait = 60000;

		//private byte[] blankFileBytes;
		
		/// <summary>
		/// Initializes the provider.
		/// </summary>
		/// <param name="name"></param>
		/// <param name="config"></param>
		public override void Initialize(string name, NameValueCollection config) {
			base.Initialize(name, config);

			if (String.IsNullOrEmpty(config[const_attribute_tempdirectory])) {
				//throw new ProviderException(const_attribute_tempdirectory + " is missing");
				TempDirectory = VizeliaConfiguration.Instance.Deployment.TempDirectory;
			}
			else {
				TempDirectory = config[const_attribute_tempdirectory];
				if (!Directory.Exists(TempDirectory))
					throw new ProviderException(const_attribute_tempdirectory + " does not exist");
				TempDirectory = Path.GetFullPath(TempDirectory);
			}
			config.Remove(const_attribute_tempdirectory);


			if (String.IsNullOrEmpty(config[const_attribute_exerelativepath])) {
				throw new ProviderException(const_attribute_exerelativepath + " is missing");
			}
			ExeRelativePath = config[const_attribute_exerelativepath];
			config.Remove(const_attribute_exerelativepath);

			if (String.IsNullOrEmpty(config[const_attribute_screenshotserviceurl])) {
				throw new ProviderException(const_attribute_screenshotserviceurl + " is missing");
			}
			ScreenshotServiceUrl = config[const_attribute_screenshotserviceurl];
			config.Remove(const_attribute_screenshotserviceurl);


			if (String.IsNullOrEmpty(config[const_attribute_printscreenusername])) {
				PrintscreenUsername = VizeliaConfiguration.Instance.Authentication.PrintscreenUsername;
			}
			else {
				PrintscreenUsername = config[const_attribute_printscreenusername];
			}
			config.Remove(const_attribute_printscreenusername);

			if (String.IsNullOrEmpty(config[const_attribute_printscreenprovider])) {
				PrintscreenProvider = VizeliaConfiguration.Instance.Authentication.DefaultProvider;
			}
			else {
				PrintscreenProvider = config[const_attribute_printscreenprovider];
			}
			config.Remove(const_attribute_printscreenprovider);
			
			if (config.Count > 0)
				throw new ProviderException(String.Format("attribute {0} is unauthorized", config.GetKey(0)));
		}


		/// <summary>
		/// Gets or sets the printscreen provider.
		/// </summary>
		/// <value>
		/// The printscreen provider.
		/// </value>
		public string PrintscreenProvider { get; set; }

		/// <summary>
		/// Gets or sets the printscreen username.
		/// </summary>
		/// <value>
		/// The printscreen username.
		/// </value>
		public string PrintscreenUsername { get; set; }

		/// <summary>
		/// Gets or sets the relative of the exe responsible for generating the image.
		/// </summary>
		/// <value>
		/// The relative path of the exe.
		/// </value>
		public string ExeRelativePath { get; set; }

		/// <summary>
		/// Gets or sets the temp directory location.
		/// </summary>
		/// <value>
		/// The temp directory location.
		/// </value>
		public string TempDirectory { get; set; }


		/// <summary>
		/// Gets or sets the Screenshot service URL.
		/// </summary>
		public string ScreenshotServiceUrl { get; set; }


		/// <summary>
		/// Gets the screen shot.
		/// </summary>
		/// <param name="url">The URL.</param>
		/// <param name="localUrl">if set to <c>true</c> the url is local and the servicescreenshoturl will be appended.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <param name="outputType">Type of the output.</param>
		/// <param name="mimeType">Mimetype.</param>
		/// <param name="keyLocalisationCulture">Culture.</param>
		/// <param name="retryNumber"></param>
		/// <returns></returns>
		public override StreamResult GetScreenshot(string url, bool localUrl, int width, int height, ScreenshotType outputType,
			string mimeType, string keyLocalisationCulture, int retryNumber = 0) {
			bool shouldRetry = false;
			string exeFile = AppDomain.CurrentDomain.BaseDirectory + ExeRelativePath;

			string resultFile = Path.Combine(TempDirectory, "Screenshot" + "_" + Guid.NewGuid() + "." + outputType.ParseAsString().ToLower());

			if (localUrl && retryNumber == 0) {
				url = Helper.GetFrontendUrl(ScreenshotServiceUrl + url, VizeliaConfiguration.Instance.Deployment.FrontendUrl);
			}

			// maybe fixes the login screen
			//HttpCookie authCookie = null;
			HttpCookie authCookie =  HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName];
			if (authCookie == null) {
				string pageId;
				SessionService.SetAuthCookie(VizeliaConfiguration.Instance.Authentication.PrintscreenUsername, false, out pageId);
				authCookie = HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName];
				HttpContext.Current.Response.Cookies.Remove(FormsAuthentication.FormsCookieName);
			}
			string authCookieValue = authCookie.Value;

			var ticket = FormsAuthentication.Decrypt(authCookieValue);
			if (ticket.Expired) {
				var newTicket = new FormsAuthenticationTicket(
					ticket.Version,
					ticket.Name,
					ticket.IssueDate,
					DateTime.UtcNow.AddMinutes(5),
					ticket.IsPersistent,
					ticket.UserData,
					ticket.CookiePath);

				authCookieValue = FormsAuthentication.Encrypt(newTicket);
			}

			if (retryNumber == 0) {
				url += string.Format("&username={0}", PrintscreenUsername);
			}

			string customArguments = string.Format(" \"{0}js\\rasterize.js\" \"{1}\" \"{2}\" {3} {4} \"{5}\" {6} {7}", AppDomain.CurrentDomain.BaseDirectory,
				url, resultFile, width, height, authCookieValue, keyLocalisationCulture, const_max_time_to_wait);
			const string fixedArguments = "--disk-cache=true --proxy-type=none --ignore-ssl-errors=true --ssl-protocol=any --web-security=false --local-to-remote-url-access=true";

			string arguments = fixedArguments + " " + customArguments;
			string standardOutput;
			string standardError;
			bool timeoutOccurred;
			int processExitCode;

			using (var p = new Process {
				StartInfo = {
					FileName = exeFile,
					Arguments = string.Format(arguments),
					CreateNoWindow = false,
					UseShellExecute = false,
					RedirectStandardError = true,
					RedirectStandardOutput = true
				}
			}) {

				p.Start();
				timeoutOccurred = !p.WaitForExit(const_max_time_to_wait + 5000);
				if (timeoutOccurred) p.Kill(); // hard process kill - in case where phantom didn't kill himself
				standardOutput = p.StandardOutput.ReadToEnd();
				standardError = p.StandardError.ReadToEnd();
				processExitCode = p.ExitCode;
			}

			shouldRetry = ShouldRetry(retryNumber, standardError, standardOutput, timeoutOccurred, resultFile, processExitCode, arguments);

			/*if (File.Exists(resultFile) && !shouldRetry) {
				File.Copy(resultFile,
					@"D:\phantom\" + DateTime.Now.ToString("ddHHmmss ") + "Screenshot" + "_" + retryNumber + "_" + Guid.NewGuid() + "." +
					outputType.ParseAsString().ToLower());
			}*/

			var retVal = shouldRetry ?
				GetScreenshot(url, localUrl, width, height, outputType, mimeType, keyLocalisationCulture, retryNumber + 1) :
				new StreamResult(resultFile, mimeType, true);

			return retVal;
		}
		
		/// <summary>
		/// Shoulds the retry.
		/// </summary>
		/// <param name="retryNumber">The retry number.</param>
		/// <param name="standardError">The standard error.</param>
		/// <param name="standardOutput">The standard output.</param>
		/// <param name="timeoutOccurred">if set to <c>true</c> [timeout occurred].</param>
		/// <param name="resultFile">The result file.</param>
		/// <param name="processExitCode">The process exit code.</param>
		/// <param name="arguments">The arguments.</param>
		/// <returns></returns>
		private bool ShouldRetry(int retryNumber, string standardError, string standardOutput, bool timeoutOccurred, string resultFile, int processExitCode, string arguments) {
			var outputExists = !string.IsNullOrEmpty(standardOutput) || !string.IsNullOrEmpty(standardError);
			bool phantomTimeoutOccured = processExitCode == 759;
			var isBlankFile = false;
			var fileMissing = !File.Exists(resultFile);
			if (!fileMissing) {
				isBlankFile = IsBlankFile(resultFile);
			}
			
			bool shouldRetry = false;
			if (outputExists || timeoutOccurred || fileMissing || isBlankFile || phantomTimeoutOccured) {
				if (outputExists) {
					// If you have output then write to log and throw exception
					var error = string.Format("{0}{1}Playlist output: {2}{3}Playlist error: {4}", arguments, Environment.NewLine,
					                          standardOutput, Environment.NewLine, standardError);
					error = string.Format(Langue.error_playlist_generate, error);
					TracingService.Write(TraceEntrySeverity.Warning, error);
				}
				
				if (timeoutOccurred) {
					var error = string.Format("{0}{1}{2}", "Timeout occured during playlist generation in server.", Environment.NewLine, arguments);
					TracingService.Write(TraceEntrySeverity.Warning, error);
				}

				if (fileMissing) {
					var error = string.Format("{0}{1}{2}", "Error occurred during playlist generation. Generated file is missing.", Environment.NewLine, arguments);
					TracingService.Write(TraceEntrySeverity.Warning, error);
				}
				
				if (isBlankFile) {
					var error = string.Format("{0}{1}{2}", "Error occurred during playlist generation. Generated file is blank.", Environment.NewLine, arguments);
					TracingService.Write(TraceEntrySeverity.Warning, error);
				}

				if (phantomTimeoutOccured) {
					var error = string.Format("{0}{1}{2}", "Timeout occured during playlist generation in phantomjs.", Environment.NewLine, arguments);
					TracingService.Write(TraceEntrySeverity.Warning, error);
				}

				ValidateRetryNumber(retryNumber);
				shouldRetry = true;
			}
			return shouldRetry;
		}

		/// <summary>
		/// check if the file is blank
		/// </summary>
		/// <param name="file1">file path</param>
		/// <returns>blank or not</returns>
		private bool IsBlankFile(string file1) {
			bool timeoutOccurred, blankResult;
			string standardOutput, standardError;
			using (var p = new Process {
				StartInfo = {
					FileName = AppDomain.CurrentDomain.BaseDirectory + "tools\\BlankCheck.exe",
					Arguments = file1,
					CreateNoWindow = false,
					UseShellExecute = false,
					RedirectStandardError = true,
					RedirectStandardOutput = true
				}
			}) {
				p.Start();
				timeoutOccurred = !p.WaitForExit(3000);
				if (timeoutOccurred) p.Kill(); // hard process kill - in case where phantom didn't kill himself
				standardOutput = p.StandardOutput.ReadToEnd();
				standardError = p.StandardError.ReadToEnd();
			}

			if (!bool.TryParse(standardOutput, out blankResult)) {
				throw new VizeliaException("Error occured during playlist generation. Blank file check failed. " + (string.IsNullOrWhiteSpace(standardError) ? "" : standardError));
			}

			return blankResult;
		}
		
		private static void ValidateRetryNumber(int retryNumber) {
			if (retryNumber >= const_max_retry_number) {
				var exceptionString = string.Format(Langue.error_playlist_generate, "Retry number reached " + const_max_retry_number + ".");
				throw new VizeliaException(exceptionString);
			}
		}
	}
}

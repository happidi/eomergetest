﻿using System;
using System.Collections.Generic;
using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// A factory for Excel Chart Renderers.
	/// </summary>
	internal class ExcelChartRendererFactory {
		/// <summary>
		/// Creates the relevant chart render for the given chart.
		/// </summary>
		/// <param name="chart">The chart.</param>
		public static StandardExcelChartRenderer Create(Chart chart) {
			// Specific analysis charts take precedence over all other types.
			switch (chart.SpecificAnalysis) {
				case ChartSpecificAnalysis.Correlation:
					return new CorrelationExcelChartRenderer();

				case ChartSpecificAnalysis.None:
				case ChartSpecificAnalysis.DifferenceHighlight:
					return CreateExcelChartRenderer(chart);

				default:
					return new ImageExcelChartRenderer();
			}
		}

		private static StandardExcelChartRenderer CreateExcelChartRenderer(Chart chart) {
			bool isSummarizingChartType = IsSummarizingChartType(chart);

			if (isSummarizingChartType) {
				return new SummarizingExcelChartRenderer();
			}

			return new StandardExcelChartRenderer();
		}

		/// <summary>
		/// Determines whether the chart is of a summarizing type or not.
		/// </summary>
		/// <param name="chart">The chart.</param>
		/// <returns>
		///   <c>true</c> if the chart is of a summarizing type ; otherwise, <c>false</c>.
		/// </returns>
		private static bool IsSummarizingChartType(Chart chart) {
			var summarizingTypes = new List<ChartType> { ChartType.Donut, ChartType.Gauge, ChartType.Pie };
			bool isSummarizingChartType = summarizingTypes.Contains(chart.ChartType);

			return isSummarizingChartType;
		}


	}
}
using System;
using System.Collections.Generic;
using Syncfusion.XlsIO;
using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// A renderable chart
	/// </summary>
	internal class RenderableChart {
		public List<Tuple<string, DateTime>> Categories { get; set; }
		public List<DataSerie> DataSeries { get; set; }
		public ChartHistoricalAnalysis HistoricalAnalysis { get; set; }
		public IRange Range { get; set; }
		public int ChartNumber { get; set; }
		public Chart Chart { get; set; }
		public StreamResult Image { get; set; }
	}
}
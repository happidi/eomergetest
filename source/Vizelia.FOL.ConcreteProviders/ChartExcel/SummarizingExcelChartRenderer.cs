using System.Collections.Generic;
using Syncfusion.XlsIO;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common.Localization;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// An excel chart renderer for summarizing charts such as pie and radar.
	/// </summary>
	internal class SummarizingExcelChartRenderer : StandardExcelChartRenderer {
		/// <summary>
		/// Creates the Excel chart within the given sheet according to the given parameters.
		/// </summary>
		/// <param name="dataRange">The data range.</param>
		/// <param name="chart">The chart.</param>
		/// <returns></returns>
		protected override IChartShape CreateChart(IRange dataRange, Chart chart) {
			IChartShape excelChart = dataRange.Worksheet.Charts.Add();
			IChartSerie excelSerie = excelChart.Series.Add(Langue.msg_chart_totals);

			// Here we use the summary row we added in the AddTotalsRowToTable method.
			excelSerie.Values = dataRange[dataRange.LastRow + 1, 2, dataRange.LastRow + 1, dataRange.LastColumn];
			excelSerie.CategoryLabels = dataRange[1, 2, 1, dataRange.LastColumn];

			return excelChart;
		}

		protected override void AddTotalsRowToTable(IListObject table, IWorksheet worksheet, List<DataSerie> series, IRange dataRange) {
			// HACK: We cannot use the table totals row, as in Excel 2007 client, it is 
			// not a valid chart data source and the graph would appear empty.
			// So we shall create our own totals data row... 
			dataRange.Worksheet[dataRange.LastRow + 1, dataRange.Column].Value = Langue.msg_chart_totals;

			for (int c = dataRange.Column + 1; c <= dataRange.LastColumn; c++) {
				// We cannot do this due to a syncfusion defect...
				//string formula = string.Format("SUBTOTAL(109,{0}[{1}])", table.Name, table.Columns[c - dataRange.Column].Name);
				string formula = string.Format("SUM({0})", worksheet[dataRange.Row + 1, c, dataRange.LastRow, c].AddressLocal);
				dataRange.Worksheet.Range[dataRange.LastRow + 1, c].Formula = formula;
			}

			WriteComment(dataRange.Worksheet.Range[dataRange.LastRow + 1, dataRange.Column], Langue.msg_excel_export_totals_row_comment, false);
		}

		/// <summary>
		/// Writes the comment to chart worksheet.
		/// </summary>
		/// <param name="chartShape">The chart shape.</param>
		/// <param name="comment">The comment.</param>
		/// <param name="visible">if set to <c>true</c> [visible].</param>
		protected override ICommentShape WriteComment(IChartShape chartShape, string comment, bool visible = true) {
			// We define series directly on this chart instead of DataRange in the original implementation.
			IRange dataRange = chartShape.Series[0].Values.Worksheet.Range;
			return WriteComment(dataRange, comment, visible);
		}

		/// <summary>
		/// Sets the series.
		/// </summary>
		/// <param name="excelChart">The excel chart.</param>
		/// <param name="vizChart">The viz chart.</param>
		/// <param name="dataSeries">The data series.</param>
		/// <param name="isComboChart">if set to <c>true</c> [is combo chart].</param>
		/// <param name="axisInUse"> </param>
		protected override void SetSeries(IChartShape excelChart, Chart vizChart, List<DataSerie> dataSeries, bool isComboChart, List<ChartAxis> axisInUse) {
			foreach (IChartSerie excelSerie in excelChart.Series) {
				excelSerie.DataPoints.DefaultDataPoint.DataLabels.IsValue = true;
				excelSerie.DataPoints.DefaultDataPoint.DataLabels.IsPercentage = true;
			}
		}
	}
}
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using Syncfusion.XlsIO;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Localization;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// A renderer of Excel charts that renders a correlation graph.
	/// </summary>
	internal class CorrelationExcelChartRenderer : StandardExcelChartRenderer {
		/// <summary>
		/// Extracts subcharts from the chart.
		/// This is relevant for a case of historical analysis charts.
		/// </summary>
		/// <param name="chart">The chart.</param>
		/// <param name="chartImage"></param>
		/// <returns></returns>
		protected override List<RenderableChart> ExtractSubCharts(Chart chart, StreamResult chartImage) {
			var chartsToPopulate = new List<RenderableChart>();

			// We need the hidden series because they contain the raw data. 
			// The visible series are the calculated points of the cloud and the correlation line.
			// We want Excel to do this calculation on the raw data, not just show the points like it's a regular chart.
			List<DataSerie> series = chart.Series.ToList().Where(s => s.Hidden).ToList();
			chart.Series.Clear();

			foreach (DataSerie serie in series) {
				serie.Hidden = false;
				chart.Series.Add(serie);
			}

			// Get categories for hidden series.
			List<Tuple<string, DateTime>> categories = ChartHelper.GetCategories(chart);

			chartsToPopulate.Add(new RenderableChart { Chart = chart, Categories = categories, DataSeries = series, ChartNumber = 1 });

			return chartsToPopulate;
		}

		/// <summary>
		/// Sets the type of the chart, if needed.
		/// If the chart is a combo chart, then setting its type isn't needed.
		/// </summary>
		/// <param name="excelChart">The excel chart.</param>
		/// <param name="vizChart">The viz chart.</param>
		/// <param name="dataSeries">The data series.</param>
		/// <param name="axisInUse"> </param>
		/// <returns>
		/// true if the chart is a combo chart, false otherwise.
		/// </returns>
		protected override bool SetChartType(IChartShape excelChart, Chart vizChart, List<DataSerie> dataSeries, List<ChartAxis> axisInUse) {
			excelChart.ChartType = ExcelChartType.Scatter_Markers;
			return false;
		}


		/// <summary>
		/// Creates the Excel chart within the given sheet according to the given parameters.
		/// </summary>
		/// <param name="dataRange">The data range.</param>
		/// <param name="chart">The chart.</param>
		/// <returns></returns>
		protected override IChartShape CreateChart(IRange dataRange, Chart chart) {
			IChartShape excelChart = dataRange.Worksheet.Charts.Add();
			IChartSerie excelSerie = excelChart.Series.Add(Langue.msg_enum_chartspecificanalysis_correlation);

			List<DataSerie> dataSeries = chart.Series.ToList();
			DataSerie xSerie = dataSeries.First(s => s.LocalId.Equals(chart.CorrelationXSerieLocalId));

			if(xSerie.Name.Equals(dataRange[1, 2].Text)) {
				excelSerie.Values = dataRange[2, 3, dataRange.LastRow, 3];
				excelSerie.CategoryLabels = dataRange[2, 2, dataRange.LastRow, 2];
			}
			else {
				excelSerie.Values = dataRange[2, 2, dataRange.LastRow, 2];
				excelSerie.CategoryLabels = dataRange[2, 3, dataRange.LastRow, 3];
			}

			return excelChart;
		}

		/// <summary>
		/// Sets the series.
		/// </summary>
		/// <param name="excelChart">The excel chart.</param>
		/// <param name="vizChart">The viz chart.</param>
		/// <param name="dataSeries">The data series.</param>
		/// <param name="isComboChart">if set to <c>true</c> [is combo chart].</param>
		/// <param name="axisInUse"> </param>
		protected override void SetSeries(IChartShape excelChart, Chart vizChart, List<DataSerie> dataSeries, bool isComboChart, List<ChartAxis> axisInUse) {
			IChartTrendLine trendLine = excelChart.Series[0].TrendLines.Add(ExcelTrendLineType.Linear);
			trendLine.Border.LineWeight = ExcelChartLineWeight.Medium;
			trendLine.Border.LineColor = Color.DarkOrange;

			excelChart.Series[0].DataPoints.DefaultDataPoint.DataFormat.MarkerSize = 10;
			excelChart.Series[0].DataPoints.DefaultDataPoint.DataFormat.MarkerBackgroundColor = Color.DodgerBlue;
			excelChart.Series[0].DataPoints.DefaultDataPoint.DataFormat.MarkerForegroundColor = Color.DodgerBlue;
			excelChart.Series[0].DataPoints.DefaultDataPoint.DataFormat.MarkerStyle = ExcelChartMarkerType.Diamond;
		}
	}
}
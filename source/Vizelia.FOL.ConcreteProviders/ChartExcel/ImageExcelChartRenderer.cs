﻿using System;
using System.Collections.Generic;
using System.Linq;
using Syncfusion.XlsIO;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// A renderer for Excel Charts that uses the given image inside the Excel sheet instead of rendering the chart.
	/// </summary>
	internal class ImageExcelChartRenderer : StandardExcelChartRenderer {
		/// <summary>
		/// Extracts subcharts from the chart.
		/// This is relevant for a case of historical analysis charts.
		/// </summary>
		/// <param name="chart">The chart.</param>
		/// <param name="chartImage"></param>
		/// <returns></returns>
		protected override List<RenderableChart> ExtractSubCharts(Chart chart, StreamResult chartImage) {
			var chartsToPopulate = new List<RenderableChart>();

			// Add the original chart
			List<Tuple<string, DateTime>> categories = ChartHelper.GetCategories(chart);
			List<DataSerie> series = chart.Series.GetVisibleDataSeries().ToList();
			chartsToPopulate.Add(new RenderableChart { Chart = chart, Categories = categories, DataSeries = series, ChartNumber = 1, Image = chartImage });

			return chartsToPopulate;
	
		}

		/// <summary>
		/// Renders the chart in the workbook.
		/// </summary>
		/// <param name="chartToRender">The chart to render.</param>
		protected override void RenderChart(RenderableChart chartToRender) {
			chartToRender.Range.Worksheet.Pictures.AddPicture(1, chartToRender.DataSeries.Count + 3, chartToRender.Image.ContentStream, ExcelImageFormat.Png);
		}
	}
}
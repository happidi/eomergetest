﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common.Localization;
using Vizelia.FOL.Providers;
using Syncfusion.XlsIO;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// Concrete implementation of a standard charting provider using the library Syncfusion.
	/// </summary>
	public class ChartExcelSyncfusionProvider : ChartExcelProvider {
		private const int const_sheet_name_max_length = 31;

		/// <summary>
		/// Renders the chart as an excel spreadsheet stream.
		/// </summary>
		/// <param name="chartsWithImages">The charts with images.</param>
		/// <param name="title">The title.</param>
		/// <returns></returns>
		public override StreamResult RenderExcelStream(List<Tuple<Chart, StreamResult>> chartsWithImages, string title) {
			if (!chartsWithImages.Any()) {
				throw new ArgumentException(Langue.error_excel_export_no_images);
			}

			var excelEngine = new ExcelEngine();
			IApplication application = excelEngine.Excel;
			application.DataProviderType = ExcelDataProviderType.Native;
			application.DefaultVersion = ExcelVersion.Excel2007;

			List<Tuple<Chart, StreamResult, string>> chartSheets = CoerceSheetNames(chartsWithImages);
			string[] sheetNames = chartSheets.Select(c => c.Item3).ToArray();
			IWorkbook workbook = excelEngine.Excel.Workbooks.Create(sheetNames);

			// Render each chart in its own worksheet
			foreach (Tuple<Chart, StreamResult, string> chartSheet in chartSheets) {
				try {
					if (ChartHelper.HasData(chartSheet.Item1)) {
						IWorksheet worksheet = workbook.Worksheets[chartSheet.Item3];
						RenderChartInSheet(worksheet, chartSheet.Item1, chartSheet.Item2);
					}
				}
				catch (SystemException e) {
					TracingService.Write(e);
				}
			}

			StreamResult retVal = SaveToStream(excelEngine, workbook, title);

			return retVal;
		}

		private List<Tuple<Chart, StreamResult, string>> CoerceSheetNames(IEnumerable<Tuple<Chart, StreamResult>> chartsWithImages) {
			// Coerce the names to be valid - allowed length and allowed chars.
			var sheetNames = from c in chartsWithImages
							 let sheetName = CoerceSheetName(c.Item1.Title)
							 select new Tuple<Tuple<Chart, StreamResult>, string>(new Tuple<Chart, StreamResult>(c.Item1, c.Item2), sheetName);

			List<Tuple<Tuple<Chart, StreamResult>, string>> chartSheets = Helper.PreventDuplicateNames(sheetNames, const_sheet_name_max_length, "_");
			// Flatten the tuple
			List<Tuple<Chart, StreamResult, string>> coercedSheets = chartSheets.Select(t => new Tuple<Chart, StreamResult, string>(t.Item1.Item1, t.Item1.Item2, t.Item2)).ToList();

			return coercedSheets;
		}



		/// <summary>
		/// Coerces the name of the sheet. Makes sure it does not contain illegal characters and that it is not longer than 31 chars.
		/// </summary>
		/// <param name="sheetOriginalName">Name of the sheet original.</param>
		/// <returns></returns>
		private string CoerceSheetName(string sheetOriginalName) {
			// Disallowed chars on sheet title: \ / ? * [ ]
			string validSheetName = Regex.Replace(sheetOriginalName, @"[/\\\?\*\[\]]", string.Empty);
			string sheetName = validSheetName.Substring(0, Math.Min(validSheetName.Length, const_sheet_name_max_length));

			return sheetName;
		}

		/// <summary>
		/// Renders the chart in sheet.
		/// </summary>
		/// <param name="worksheet">The worksheet.</param>
		/// <param name="chart">The chart.</param>
		/// <param name="image">The image.</param>
		private static void RenderChartInSheet(IWorksheet worksheet, Chart chart, StreamResult image) {
			// Get the renderer according to the chart type.
			var renderer = ExcelChartRendererFactory.Create(chart);

			if (ChartHelper.HasData(chart)) {
				renderer.Render(chart, image, worksheet);
			}
			else {
				renderer.WriteComment(worksheet.Range[1, 1], Langue.error_chart_nodata);
			}
		}

		/// <summary>
		/// Saves the Excel workbook to a stream.
		/// </summary>
		/// <param name="excelEngine">The excel engine.</param>
		/// <param name="workbook">The workbook.</param>
		/// <param name="title">The title.</param>
		/// <returns></returns>
		private StreamResult SaveToStream(ExcelEngine excelEngine, IWorkbook workbook, string title) {
			// Perform the saving itself
			var memoryStream = new MemoryStream();
			workbook.SaveAs(memoryStream, ExcelSaveType.SaveAsXLS);

			// Copy the stream contents
			memoryStream.Position = 0;
			var copyStream = new MemoryStream(memoryStream.GetBytes(), 0, (int)memoryStream.Length, true);

			// Prepare long-running operation result
			var filename = TimeZoneHelper.GetFilenameTimestamped(title);
			var result = new StreamResult(copyStream, filename, "xlsx", MimeType.ExcelSpreadSheet);

			// Dispose. No exception will be thrown if there are unsaved workbooks.
			excelEngine.ThrowNotSavedOnDestroy = false;
			excelEngine.Dispose();
			memoryStream.Dispose();

			return result;
		}

	}
}

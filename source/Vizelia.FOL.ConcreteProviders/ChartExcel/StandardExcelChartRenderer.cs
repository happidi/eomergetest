using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Web;
using Syncfusion.XlsIO;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Localization;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// A renderer for standard Excel charts - supports combo charts, multi-y-axis.
	/// </summary>
	internal class StandardExcelChartRenderer {
		/// <summary>
		/// Gets or sets a value indicating whether this instance is historical analysis enabled.
		/// </summary>
		/// <value>
		/// 	if set to <c>true</c> the chart is historical analysis enabled.
		/// </value>
		protected bool IsHistoricalAnalysisEnabled { get; set; }

		/// <summary>
		/// Renders the specified chart to the worksheet.
		/// </summary>
		/// <param name="chart">The chart.</param>
		/// <param name="chartImage">The chart image.</param>
		/// <param name="worksheet">The worksheet.</param>
		public void Render(Chart chart, StreamResult chartImage, IWorksheet worksheet) {

			switch (chart.DisplayMode) {
				case ChartDisplayMode.CustomGrid:
					RenderCustomGrid(worksheet, chart);
					break;
				case ChartDisplayMode.Grid:
					PopulateData(chart, chartImage, worksheet);
					break;
				case ChartDisplayMode.Image:
				case ChartDisplayMode.HighCharts:
				case ChartDisplayMode.Html:
					// We may get several ranges if the chart is a historical analysis chart.
					List<RenderableChart> chartsToRender = PopulateData(chart, chartImage, worksheet);
					IsHistoricalAnalysisEnabled = chartsToRender.Count > 1;

					foreach (var chartToRender in chartsToRender) {
						RenderChart(chartToRender);
					}
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		private void RenderCustomGrid(IWorksheet sheet, Chart chart) {
			if (chart.ChartCustomGridCells == null)
				return;

			var migrantRange = sheet.MigrantRange;

			// Writing Data.
			for (int row = 1; row <= chart.CustomGridRowCount; row++) {
				for (int column = 1; column <= chart.CustomGridColumnCount; column++) {
					// Writing values.
					migrantRange.ResetRowColumn(row, column);
					var customGridCell = chart.ChartCustomGridCells.FirstOrDefault(cell => cell.RowIndex == (row - 1) && cell.ColumnIndex == (column - 1));
					if (customGridCell != null) {
						migrantRange.Text = customGridCell.Value.StripTagsCharArray();
					}
				}
			}
		}

		/// <summary>
		/// Renders the chart in the workbook.
		/// </summary>
		/// <param name="chartToRender">The chart to render.</param>
		protected virtual void RenderChart(RenderableChart chartToRender) {
			IChartShape excelChart = CreateChart(chartToRender.Range, chartToRender.Chart);
			SetChartLayout(excelChart, chartToRender);
			SetChartDesign(excelChart, chartToRender);
		}

		/// <summary>
		/// Sets the chart design.
		/// </summary>
		/// <param name="excelChart">The excel chart.</param>
		/// <param name="chartToRender">The chart to render.</param>
		private void SetChartDesign(IChartShape excelChart, RenderableChart chartToRender) {
			SetChartTitle(excelChart, chartToRender);
			FormatChartArea(excelChart, chartToRender.Chart);
			BuildLegend(excelChart, chartToRender.Chart);
			SetChartPosition(excelChart, chartToRender);
		}

		/// <summary>
		/// Builds the chart title.
		/// </summary>
		/// <param name="excelChart">The excel chart.</param>
		/// <param name="chartToRender">The chart to render.</param>
		private void SetChartTitle(IChartShape excelChart, RenderableChart chartToRender) {
			if (!IsHistoricalAnalysisEnabled) {
				excelChart.ChartTitle = chartToRender.Chart.Title;
			}
			else {
				string dateRangeLabel;

				if (chartToRender.ChartNumber == 1) {
					dateRangeLabel = ChartHelper.GenerateDateRangeLabel(chartToRender.Chart.StartDate, chartToRender.Chart.EndDate, chartToRender.Chart.GetTimeZone());
				}
				else {
					dateRangeLabel = ChartHelper.GenerateDateRangeLabel(chartToRender.HistoricalAnalysis.StartDate.Value, chartToRender.HistoricalAnalysis.EndDate.Value, chartToRender.Chart.GetTimeZone());
				}

				excelChart.ChartTitle = string.Format("{0} - {1}", chartToRender.Chart.Title, dateRangeLabel);
			}


			excelChart.ChartTitleArea.Bold = true;
			excelChart.ChartTitleArea.Color = ExcelKnownColors.Indigo;
			excelChart.ChartTitleArea.FontName = !string.IsNullOrWhiteSpace(chartToRender.Chart.DisplayFontFamily)
													? chartToRender.Chart.DisplayFontFamily
													: ChartHelper.GetDefaultFontFamily();

			excelChart.ChartTitleArea.Italic = true;
			excelChart.ChartTitleArea.Size = 10f;
		}


		/// <summary>
		/// Sets the chart position within the worksheet so it doesn't cover the data.
		/// </summary>
		/// <param name="excelChart">The excel chart.</param>
		/// <param name="chartToRender">The chart to render.</param>
		private void SetChartPosition(IChartShape excelChart, RenderableChart chartToRender) {
			int historicalDataSerieCount = chartToRender.Chart.HistoricalAnalysis.Values.SelectMany(v => v.GetVisibleDataSeries()).Count();
			int historicalChartsCount = chartToRender.Chart.HistoricalAnalysis.Values.Count;
			int totalAmountOfColumns = historicalDataSerieCount + (historicalChartsCount * 2) + chartToRender.Chart.Series.Count;
			excelChart.LeftColumn = totalAmountOfColumns + 3;
			excelChart.RightColumn = totalAmountOfColumns + 15;
			excelChart.TopRow = ((chartToRender.ChartNumber - 1) * 30) + 1;
			excelChart.BottomRow = ((chartToRender.ChartNumber - 1) * 30) + 30;
		}

		/// <summary>
		/// Builds the legend, showing the series names and colors.
		/// </summary>
		/// <param name="excelChart">The excel chart.</param>
		/// <param name="chart">The chart.</param>
		private void BuildLegend(IChartShape excelChart, Chart chart) {
			if (!chart.DisplayLegendVisible) {
				return;
			}

			excelChart.Legend.FrameFormat.IsBorderCornersRound = true;
			excelChart.Legend.FrameFormat.Border.ColorIndex = ExcelKnownColors.Light_orange;
			excelChart.Legend.Position = ExcelLegendPosition.Top;
			excelChart.Legend.TextArea.Size = 8.25f;
			excelChart.Legend.TextArea.FontName = !string.IsNullOrWhiteSpace(chart.DisplayFontFamily)
													? chart.DisplayFontFamily
													: ChartHelper.GetDefaultFontFamily();
		}

		private void FormatChartArea(IChartShape excelChart, Chart vizChart) {
			// Show DataTable under chart
			// Do we need it? it doesn't show up in the web chart.
			//SetDataTable(excelChart);

			// Format Chart Area
			SetChartArea(vizChart, excelChart);

			// Format Plot Area
			SetPlotArea(excelChart);
		}

		private void SetPlotArea(IChartShape excelChart) {
			IChartFrameFormat excelChartPlotArea = excelChart.PlotArea;
			excelChartPlotArea.Border.LinePattern = ExcelChartLinePattern.None;
			excelChartPlotArea.Fill.Visible = false;
		}

		private void SetChartArea(Chart vizChart, IChartShape excelChart) {
			SetChartBorder(excelChart);
			SetChartGradientBackground(excelChart, vizChart);
		}

		private void SetChartBorder(IChartShape excelChart) {
			IChartFrameFormat excelChartArea = excelChart.ChartArea;

			//Style
			excelChartArea.Border.LinePattern = ExcelChartLinePattern.Solid;
			excelChartArea.Border.LineColor = Color.Tan;
			excelChartArea.Border.LineWeight = ExcelChartLineWeight.Hairline;
		}

		/// <summary>
		/// Sets the gradient background for the chart.
		/// </summary>
		/// <param name="excelChart">The excel chart.</param>
		/// <param name="vizChart">The viz chart.</param>
		private void SetChartGradientBackground(IChartShape excelChart, Chart vizChart) {
			IChartFrameFormat excelChartArea = excelChart.ChartArea;

			//Set a gradient fill
			excelChartArea.Fill.FillType = ExcelFillType.Gradient;
			excelChartArea.Fill.GradientColorType = ExcelGradientColor.TwoColor;

			//Set two colors
			if (vizChart.DynamicDisplay != null && vizChart.UseDynamicDisplay) {
				DynamicDisplayColorTemplate colors = vizChart.DynamicDisplay.GetColorTemplate();
				excelChartArea.Fill.ForeColor = colors.GetStartColor();
				excelChartArea.Fill.BackColor = colors.GetEndColor();
			}
			else {
				excelChartArea.Fill.ForeColor = ChartHelper.GetDefaultBackgroundStartColor();
				excelChartArea.Fill.BackColor = ChartHelper.GetDefaultBackgroundEndColor();
			}
			excelChartArea.Fill.GradientStyle = ExcelGradientStyle.Horizontal;
		}


		/// <summary>
		/// Populates the raw data for a chart.
		/// </summary>
		/// <param name="chart">The chart.</param>
		/// <param name="chartImage"></param>
		/// <param name="worksheet">The worksheet.</param>
		/// <returns></returns>
		protected virtual List<RenderableChart> PopulateData(Chart chart, StreamResult chartImage, IWorksheet worksheet) {
			var chartsToPopulate = ExtractSubCharts(chart, chartImage);
			EnforceDataSerieUniqueNames(chartsToPopulate);
			PopulateDataInRanges(chart, worksheet, chartsToPopulate);

			worksheet.UsedRange.AutofitColumns();

			return chartsToPopulate;
		}

		/// <summary>
		/// Enforces the data series will have unique names.
		/// This is due to a problem in Excel: Tables cannot have multiple columns with same name.
		/// </summary>
		/// <param name="renderableCharts">The renderable charts.</param>
		private void EnforceDataSerieUniqueNames(IEnumerable<RenderableChart> renderableCharts) {
			// We do this for each chart.
			foreach (var renderableChart in renderableCharts) {
				IEnumerable<Tuple<DataSerie, string>> seriesWithNames = renderableChart.DataSeries.Select(d => new Tuple<DataSerie, string>(d, d.Name));
				
				List<Tuple<DataSerie, string>> objectsWithDistinctNames = Helper.PreventDuplicateNames(seriesWithNames);

				// Assign the changed names back to the data series.
				foreach (Tuple<DataSerie, string> objectWithDistinctNames in objectsWithDistinctNames) {
					objectWithDistinctNames.Item1.Name = objectWithDistinctNames.Item2;
				}
			}
		}

		private void PopulateDataInRanges(Chart chart, IWorksheet worksheet, IEnumerable<RenderableChart> chartsToPopulate) {
			int baseColumnNumber = 1;
			int rangeIndex = 1;

			foreach (RenderableChart chartToPopulate in chartsToPopulate) {
				IRange range = PopulateRange(chart, worksheet, chartToPopulate.DataSeries, chartToPopulate.Categories, baseColumnNumber, rangeIndex);
				chartToPopulate.Range = range;
				baseColumnNumber = range.LastColumn + 2; // take 1 column as space.
				rangeIndex++;
			}

		}

		/// <summary>
		/// Extracts subcharts from the chart.
		/// This is relevant for a case of historical analysis charts.
		/// </summary>
		/// <param name="chart">The chart.</param>
		/// <param name="chartImage"></param>
		/// <returns></returns>
		protected virtual List<RenderableChart> ExtractSubCharts(Chart chart, StreamResult chartImage) {
			var chartsToPopulate = new List<RenderableChart>();

			// Add the original chart
			List<Tuple<string, DateTime>> originalCategories = ChartHelper.GetCategories(chart);
			List<DataSerie> originalSeries = chart.Series.GetVisibleDataSeries().ToList();
			chartsToPopulate.Add(new RenderableChart { Chart = chart, Categories = originalCategories, DataSeries = originalSeries, ChartNumber = 1 });

			// Now add each historical analysis chart to the list
			int chartNumber = 2;
			foreach (KeyValuePair<ChartHistoricalAnalysis, DataSerieCollection> historicalAnalysises in chart.HistoricalAnalysis) {
				List<DataSerie> historicalDataSeries = historicalAnalysises.Value.GetVisibleDataSeries().ToList();
				List<Tuple<string, DateTime>> historicalCategories = ChartHelper.GetCategories(chart, historicalAnalysises.Value);

				// We ignore cases in which a historical analysis is defined, but no historical series are created in the chart.
				if (historicalCategories.Any()) {
					chartsToPopulate.Add(new RenderableChart { Chart = chart, Categories = historicalCategories, DataSeries = historicalDataSeries, HistoricalAnalysis = historicalAnalysises.Key, ChartNumber = chartNumber });
					chartNumber++;
				}
			}

			return chartsToPopulate;
		}

		/// <summary>
		/// Populates the range.
		/// </summary>
		/// <param name="chart">The chart.</param>
		/// <param name="worksheet">The worksheet.</param>
		/// <param name="series">The series.</param>
		/// <param name="categories">The categories.</param>
		/// <param name="baseColumnNumber">The base column number.</param>
		/// <param name="rangeIndex">Index of the range.</param>
		/// <returns></returns>
		private IRange PopulateRange(Chart chart, IWorksheet worksheet, List<DataSerie> series, List<Tuple<string, DateTime>> categories, int baseColumnNumber, int rangeIndex) {
			Dictionary<string, int> categoryRow = PopulateCategoriesColumn(chart, worksheet, categories, baseColumnNumber);
			PopulateSeries(chart, worksheet, series, baseColumnNumber, categoryRow);

			bool isTransposed = ChartHelper.IsTransposed(chart);
			int firstRow = 1;
			int firstColumn = baseColumnNumber;
			int lastRow = categories.Count + 1;
			int lastColumn = baseColumnNumber + series.Count;
			IRange dataRange = worksheet.Range[(isTransposed ? firstColumn : firstRow), (isTransposed ? firstRow : firstColumn), (isTransposed ? lastColumn : lastRow), (isTransposed ? lastRow : lastColumn)];

			DesignRangeAsTable(worksheet, series, rangeIndex, dataRange);

			return dataRange;
		}

		/// <summary>
		/// Populates the categories column.
		/// </summary>
		/// <param name="chart">The chart.</param>
		/// <param name="worksheet">The worksheet.</param>
		/// <param name="categories">The categories.</param>
		/// <param name="baseColumnNumber">The base column number.</param>
		/// <returns>
		/// A dictionary representing the mapping between a category name and its row number.
		/// </returns>
		private Dictionary<string, int> PopulateCategoriesColumn(Chart chart, IWorksheet worksheet, List<Tuple<string, DateTime>> categories, int baseColumnNumber) {
			bool isTransposed = ChartHelper.IsTransposed(chart);

			int row = 1;
			//worksheet[(isTransposed ? baseColumnNumber : row), (isTransposed ? row : baseColumnNumber)].Text = Langue.msg_chart_categories;

			var categoriesArray = new string[categories.Count + 1];
			categoriesArray[0] = Langue.msg_chart_categories;

			bool hasDateCategories = chart.Series.ToList().Any(s => s.IsDateTime);

			// We need unique categories in case the grid is transposed.
			string[] uniqueCategoryNames = isTransposed
											? EnforceCategoryUniqueNames(categories)
											: categories.Select(c => c.Item1).ToArray();

			for (int i = 0; i < categories.Count; i++) {
				string categoryName = uniqueCategoryNames[i];
				int v;

				// We want the category to be textual in two cases:
				// 1. if the value is a date: Excel does not handle date categories in charts well.  
				// 2. if both the grid is transposed and the value is numeric: An Excel Table with columns that are numeric 
				//    overwrites the column names to Column1, Column2, ...

				string prefix = (hasDateCategories || (isTransposed && int.TryParse(categoryName, out v))) ? "'" : string.Empty;
				categoriesArray[i + 1] = prefix + categoryName;
			}

			worksheet.ImportArray(categoriesArray, (isTransposed ? baseColumnNumber : row), (isTransposed ? row : baseColumnNumber), !isTransposed);

			// We store the categories and their rows in the sheet in a dictionary for later fast access.
			var categoryRow = new Dictionary<string, int>();

			for (var i = 0; i < categories.Count; i++) {
				string key = EncodeDictionaryKey(categories[i]);
				categoryRow[key] = i;
			}

			return categoryRow;
		}

		/// <summary>
		/// Enforces the category unique names.
		/// </summary>
		/// <param name="categories">The categories.</param>
		/// <returns></returns>
		private string[] EnforceCategoryUniqueNames(List<Tuple<string, DateTime>> categories) {
			// Map category name and index.
			var tuples = new List<Tuple<int, string>>();

			for (int i = 0; i < categories.Count; i++) {
				tuples.Add(new Tuple<int, string>(i, categories[i].Item1));
			}

			// Transform duplicate names.
			List<Tuple<int, string>> transformation = Helper.PreventDuplicateNames(tuples);

			// Reassign transformed names to array by index saved earlier.
			string[] uniqueCategories = new string[categories.Count];

			foreach (Tuple<int, string> tuple in transformation) {
				uniqueCategories[tuple.Item1] = tuple.Item2;
			}

			return uniqueCategories;
		}

		/// <summary>
		/// Encodes the dictionary key.
		/// </summary>
		/// <param name="tuple">The tuple.</param>
		/// <returns></returns>
		private static string EncodeDictionaryKey(Tuple<string, DateTime> tuple) {
			string key = tuple.Item2.Equals(DateTime.MinValue)
							? tuple.Item1
							: tuple.Item2.ToString("o", CultureInfo.InvariantCulture);
			return key;
		}

		/// <summary>
		/// Populates all the series in the worksheet.
		/// </summary>
		/// <param name="chart">The chart.</param>
		/// <param name="worksheet">The worksheet.</param>
		/// <param name="series">The series.</param>
		/// <param name="baseColumnNumber">The base column number.</param>
		/// <param name="categoryRow">The dictionary containing the category row number by the category name.</param>
		private void PopulateSeries(Chart chart, IWorksheet worksheet, List<DataSerie> series, int baseColumnNumber, Dictionary<string, int> categoryRow) {
			bool isTransposed = ChartHelper.IsTransposed(chart);

			for (var j = 0; j < series.Count; j++) {
				var serie = series[j];
				int serieColumn = j + 1 + baseColumnNumber;

				int col = serieColumn;

				// Fill the serie name
				int serieRow = 1;
				worksheet[(isTransposed ? serieColumn : serieRow), (isTransposed ? serieRow : serieColumn)].Text = serie.Name;

				// Fill the serie values
				var serieValues = new object[categoryRow.Keys.Count];

				// Intentionally using for instead of foreach, it is faster.
				for (int p = 0; p < serie.Points.Count; p++) {
					var point = serie.Points[p];

					if (point.YValue.HasValue) {
						// Get the matching category for the point, as there can be more categories than points.
						Tuple<string, DateTime> category = ChartHelper.GetCategory(point, serie, chart);
						string key = EncodeDictionaryKey(category);
						int categoryIndex = categoryRow[key];

						serieValues[categoryIndex] = point.YValue.Value;
					}
				}

				int firstRow = 2;
				int row = firstRow;
				worksheet.ImportArray(serieValues, (isTransposed ? col : row), (isTransposed ? row : col), !isTransposed);

				// Set the whole column number format (except the for title row)

				int lastRow = isTransposed ? col : (categoryRow.Keys.Count + 1);
				int lastColumn = isTransposed ? (categoryRow.Keys.Count + 1) : col;
				worksheet[(isTransposed ? col : row), (isTransposed ? row : col), lastRow, lastColumn].NumberFormat = GetExcelFormatNumber(chart);
			}
		}

		protected void DesignRangeAsTable(IWorksheet worksheet, List<DataSerie> series, int rangeIndex, IRange dataRange) {
			// Make the table a proper Excel Table object.
			int worksheetIndex = GetWorksheetIndex(worksheet);

			string tableName = string.Format("ChartData_{0}_{1}", worksheetIndex + 1, rangeIndex);

			IListObject table = worksheet.ListObjects.Create(tableName, dataRange);
			table.BuiltInTableStyle = TableBuiltInStyles.TableStyleMedium11;

			AddTotalsRowToTable(table, worksheet, series, dataRange);
		}

		/// <summary>
		/// Adds the totals row to table.
		/// </summary>
		/// <param name="table">The table.</param>
		/// <param name="worksheet">The worksheet.</param>
		/// <param name="series">The series.</param>
		/// <param name="dataRange">The data range.</param>
		protected virtual void AddTotalsRowToTable(IListObject table, IWorksheet worksheet, List<DataSerie> series, IRange dataRange) {
			table.ShowTotals = true;
			table.Columns[0].TotalsRowLabel = Langue.msg_chart_totals;

			for (int i = 1; i < table.Columns.Count; i++) {
				table.Columns[i].TotalsCalculation = ExcelTotalsCalculation.Sum;
			}
		}

		/// <summary>
		/// Gets the index of the worksheet in the workbook.
		/// </summary>
		/// <param name="worksheet">The worksheet.</param>
		/// <returns></returns>
		private static int GetWorksheetIndex(IWorksheet worksheet) {
			int worksheetIndex = -1;
			for (int i = 0; i < worksheet.Workbook.Worksheets.Count; i++) {
				if (worksheet.Workbook.Worksheets[i] == worksheet) {
					worksheetIndex = i;
					break;
				}
			}
			return worksheetIndex;
		}

		/// <summary>
		/// Return the Excel Format number.
		/// </summary>
		/// <param name="chart">The Chart.</param>
		protected string GetExcelFormatNumber(Chart chart) {
			string excelFormatNumber = "#,##0";

			if (chart.DisplayDecimalPrecision > 0) {
				var zeros = new string('0', chart.DisplayDecimalPrecision);
				excelFormatNumber = string.Format("#,##0.{0}", zeros);
			}

			return excelFormatNumber;
		}

		/// <summary>
		/// Writes the comment to chart worksheet.
		/// </summary>
		/// <param name="chartShape">The chart shape.</param>
		/// <param name="comment">The comment.</param>
		/// <param name="visible">if set to <c>true</c> [visible].</param>
		protected virtual ICommentShape WriteComment(IChartShape chartShape, string comment, bool visible = true) {
			IRange dataRange = chartShape.DataRange;
			return WriteComment(dataRange, comment, visible);
		}

		/// <summary>
		/// Writes the comment to the data range.
		/// </summary>
		/// <param name="dataRange">The data range.</param>
		/// <param name="comment">The comment.</param>
		/// <param name="visible">if set to <c>true</c> [visible].</param>
		/// <returns></returns>
		public ICommentShape WriteComment(IRange dataRange, string comment, bool visible = true) {
			ICommentShape commentShape = dataRange.Cells[0].AddComment();
			commentShape.Text = comment;
			commentShape.Height = 300;
			commentShape.Width = 300;
			commentShape.IsVisible = visible;

			return commentShape;
		}

		/// <summary>
		/// Sets the axis of the chart.
		/// </summary>
		/// <param name="excelChart">The excel chart.</param>
		/// <param name="axisInUse">The axis in use.</param>
		/// <param name="chart">The chart.</param>
		private void SetAxis(IChartShape excelChart, List<ChartAxis> axisInUse, Chart chart) {
			ChartAxis primaryAxis = axisInUse[0];
			SetYAxisRange(excelChart.PrimaryValueAxis, primaryAxis);

			// If the chart is Horizontal,than the axes should be adjusted since in that case by default syncfusion plots the series in a reverse oder
			if (IsHorizontalChart(excelChart)) {
				excelChart.PrimaryCategoryAxis.ReversePlotOrder = true;
				excelChart.PrimaryCategoryAxis.IsMaxCross = true;
				excelChart.PrimaryCategoryAxis.Title = primaryAxis.Name;
				excelChart.PrimaryCategoryAxis.TitleArea.TextRotationAngle = -90;
				excelChart.PrimaryValueAxis.TickLabelPosition = ExcelTickLabelPosition.TickLabelPosition_High;
			}
			else{
				excelChart.PrimaryValueAxis.Title = primaryAxis.Name;
				excelChart.PrimaryValueAxis.TitleArea.TextRotationAngle = -90;
			}

			// Handle a second Y axis if such is defined
			bool chartIsEnforcedSingleSerieType = DoesChartTypeEnforceSingleSerieType(chart);

			if (axisInUse.Count > 1 && !chartIsEnforcedSingleSerieType) {
				if (excelChart.SecondaryCategoryAxis == null) {
					throw new VizeliaException("Chart uses more than one axis but the excelChart object does not detect a secondary axis. This is probably since no serie was defined to use the secondary axis.");
				}

				ChartAxis secondaryValueAxis = axisInUse[1];
				SetSecondaryYAxis(excelChart, secondaryValueAxis.Name);

				if (axisInUse.Count == 2) {
					// Only set the min-max range for the secondary Y axis if there are exactly two axis in use.
					SetYAxisRange(excelChart.SecondaryValueAxis, secondaryValueAxis);
				}

				if (axisInUse.Count > 2) {
					// In this case, we have more than 2 series that use more than 2 axis.
					// All series that don't use the primary axis are consolidated to use the secondary axis.
					// Therefore, their values ranges may differ, so there's no use in setting min-max by the original definition,
					// as it did not intend to include the other series in the axis.
					WriteComment(excelChart, Langue.msg_chart_excel_too_many_axis);
				}
			}
		}

		/// <summary>
		/// Sets the secondary Y axis.
		/// </summary>
		/// <param name="excelChart">The excel chart.</param>
		/// <param name="secondaryValueAxisName">Name of the secondary value axis.</param>
		private void SetSecondaryYAxis(IChartShape excelChart, string secondaryValueAxisName) {
			//Display secondary axis for the series.
			excelChart.SecondaryCategoryAxis.IsMaxCross = true;
			excelChart.SecondaryValueAxis.IsMaxCross = true;

			//Set the title
			excelChart.SecondaryValueAxis.Title = secondaryValueAxisName;
			excelChart.SecondaryValueAxis.TitleArea.TextRotationAngle = 90;
		}

		/// <summary>
		/// Sets the Y axis range.
		/// </summary>
		/// <param name="excelValueAxis">The excel value axis.</param>
		/// <param name="axis">The axis.</param>
		private static void SetYAxisRange(IChartValueAxis excelValueAxis, ChartAxis axis) {
			if (axis.Min.HasValue) {
				excelValueAxis.MinimumValue = axis.Min.Value;
			}
			if (axis.Max.HasValue) {
				excelValueAxis.MaximumValue = axis.Max.Value;
			}
		}


		/// <summary>
		/// Sets the type of the chart, if needed.
		/// If the chart is a combo chart, then setting its type isn't needed.
		/// </summary>
		/// <param name="excelChart">The excel chart.</param>
		/// <param name="vizChart">The viz chart.</param>
		/// <param name="dataSeries">The data series.</param>
		/// <param name="axisInUse"> </param>
		/// <returns>true if the chart is a combo chart, false otherwise.</returns>
		protected virtual bool SetChartType(IChartShape excelChart, Chart vizChart, List<DataSerie> dataSeries, List<ChartAxis> axisInUse) {
			// Setting the chart type should be done:
			// * If the chart has only one serie type, at first.
			// * Else, not at all, since it detects it by itself because the series have different types.

			List<DataSerieType> effectiveSerieTypes = dataSeries
				.Select(s => GetDataSerieEffectiveType(s, vizChart))
				.Distinct()
				.ToList();

			bool doesChartTypeEnforceSingleSerieType = DoesChartTypeEnforceSingleSerieType(vizChart);

			bool isSingleSerieType = effectiveSerieTypes.Count == 1 || doesChartTypeEnforceSingleSerieType;
			bool hasOnlyOneAxis = axisInUse.Count == 1;

			if (isSingleSerieType) {
				// All of the vizCharts' series have the same effective type and only one Y axis. 
				// Assign the series' type to the Excel chart and it will be applied to all of the Excel series implicitly.
				ExcelChartType chartType = ConvertChartType(effectiveSerieTypes[0], vizChart, excelChart, true);
				excelChart.ChartType = chartType;
			}

			// If the chart contains more than one serie type or more than one axis - it's a combo chart and its type
			// will be detected by the Syncfusion object.
			bool isComboChart = !(isSingleSerieType && hasOnlyOneAxis);

			return isComboChart;
		}

		/// <summary>
		/// Does the type of the chart type enforce a single type of all series in it.
		/// </summary>
		/// <param name="vizChart">The viz chart.</param>
		private bool DoesChartTypeEnforceSingleSerieType(Chart vizChart) {
			var enforcingTypes = new List<ChartType> { ChartType.Donut, ChartType.Gauge, ChartType.Pie, ChartType.Radar };
			bool doesChartTypeEnforceSingleSerieType = enforcingTypes.Contains(vizChart.ChartType);

			return doesChartTypeEnforceSingleSerieType;
		}

		/// <summary>
		/// Gets the effective type of the data serie.
		/// </summary>
		/// <param name="dataSerie">The data serie.</param>
		/// <param name="chart">The chart.</param>
		/// <returns></returns>
		private DataSerieType GetDataSerieEffectiveType(DataSerie dataSerie, Chart chart) {
			var serieType = (dataSerie.Type != DataSerieType.None) ? dataSerie.Type : chart.DataSerieType;

			return serieType;
		}

		/// <summary>
		/// Sets the type of the serie.
		/// </summary>
		/// <param name="dataSerie">The data serie.</param>
		/// <param name="excelSerie">The excel serie.</param>
		/// <param name="chart">The chart.</param>
		/// <param name="excelChart">The excel chart.</param>
		/// <param name="isComboChart">if set to <c>true</c> the chart is a combo chart.</param>
		/// <param name="useSecondaryAxis">if set to <c>true</c> the serie uses the secondary Y axis.</param>
		private void SetSerieType(DataSerie dataSerie, IChartSerie excelSerie, Chart chart, IChartShape excelChart, bool isComboChart, bool useSecondaryAxis) {
			DataSerieType effectiveSerieType = GetDataSerieEffectiveType(dataSerie, chart);

			if (isComboChart) {
				// If this is a combo chart, then each serie has its own type.
				ExcelChartType serieType = ConvertChartType(effectiveSerieType, chart, excelChart, false);

				ExcelChartType[] disallowedSecondAxisSerieTypes = new[] {
				                                                        	ExcelChartType.Column_Clustered,
				                                                        	ExcelChartType.Column_Clustered_3D,
				                                                        	ExcelChartType.Column_Stacked,
				                                                        	ExcelChartType.Column_Stacked_3D
				                                                        };

				if (useSecondaryAxis && disallowedSecondAxisSerieTypes.Contains(serieType)) {
					excelSerie.SerieType = ExcelChartType.Line_Markers_Stacked;
				}
				else {
					excelSerie.SerieType = serieType;
				}

			}
			// Else - the serie type is assigned implictly from the chart type.

		}

		/// <summary>
		/// Sets the serie.
		/// </summary>
		/// <param name="dataSerie">The data serie.</param>
		/// <param name="excelSerie">The excel serie.</param>
		/// <param name="chart">The chart.</param>
		/// <param name="excelChart">The excel chart.</param>
		/// <param name="isComboChart">if set to <c>true</c> the chart is a combo chart.</param>
		/// <param name="axisInUse">The axis in use.</param>
		/// <param name="usePrimaryAxis"> </param>
		protected void SetSerie(DataSerie dataSerie, IChartSerie excelSerie, Chart chart, IChartShape excelChart, bool isComboChart, List<ChartAxis> axisInUse, bool usePrimaryAxis) {
			// The type of the serie
			SetSerieType(dataSerie, excelSerie, chart, excelChart, isComboChart, !usePrimaryAxis);

			// This must be set only after the serie type was set, because of a Syncfusion problem.
			if (axisInUse.Count > 1) {
				excelSerie.UsePrimaryAxis = usePrimaryAxis;
			}

			// The value labels on the serie.
			excelSerie.DataPoints.DefaultDataPoint.DataLabels.IsValue = chart.DisplayValues;

			// The fill of the serie.
			SetSerieFill(dataSerie, excelSerie, chart);
		}

		/// <summary>
		/// Determines whether the serie is in the primary axis or not.
		/// </summary>
		/// <param name="dataSerie">The data serie.</param>
		/// <param name="chart">The chart.</param>
		/// <param name="axisInUse">The axis in use.</param>
		/// <returns>
		///   <c>true</c> if the serie is in the primary axis; otherwise, <c>false</c>.
		/// </returns>
		private static bool IsSerieInPrimaryAxis(DataSerie dataSerie, Chart chart, List<ChartAxis> axisInUse) {
			bool usePrimaryAxis = true;

			// The axis of the serie: Only set this property if there are multiple axis in the chart
			if (axisInUse.Count > 1 && !string.IsNullOrEmpty(dataSerie.KeyYAxis)) {
				// The series is using an explicit axis, check whether it is primary or not
				ChartAxis serieAxis = chart.Axis.Values.First(x => x.KeyChartAxis == dataSerie.KeyYAxis);
				usePrimaryAxis = serieAxis.IsPrimary;
			}
			return usePrimaryAxis;
		}


		/// <summary>
		/// Sets the serie fill.
		/// </summary>
		/// <param name="dataSerie">The data serie.</param>
		/// <param name="excelSerie">The excel serie.</param>
		/// <param name="chart"></param>
		private void SetSerieFill(DataSerie dataSerie, IChartSerie excelSerie, Chart chart) {
			var effectiveSerieType = GetDataSerieEffectiveType(dataSerie, chart);

			// Specific handling for a case this a serie of markers (points) without a line.
			if (effectiveSerieType == DataSerieType.Marker) {
				excelSerie.SerieFormat.LineProperties.LinePattern = ExcelChartLinePattern.None;
			}

			var lineSerieTypes = new List<ExcelChartType> {
			                                              	ExcelChartType.Radar,
			                                              	ExcelChartType.Radar_Filled,
			                                              	ExcelChartType.Radar_Markers,
			                                              	ExcelChartType.Line_Markers,
			                                              	ExcelChartType.Line_Markers_Stacked,
			                                              	ExcelChartType.Scatter_Line,
			                                              	ExcelChartType.Line_Stacked,
			                                              	ExcelChartType.Scatter_Line_Markers,
			                                              	ExcelChartType.Scatter_SmoothedLine,
			                                              	ExcelChartType.Scatter_SmoothedLine_Markers
			                                              };

			// Set the color of the serie (line/fill).
			if (!dataSerie.Color.IsEmpty) {
				if (lineSerieTypes.Contains(excelSerie.SerieType)) {
					// This is a serie that is based on a line.

					// Only color the line if the serie is different than Marker (that has no line).
					if (effectiveSerieType != DataSerieType.Marker) {
						// This is a serie that has a line.
						excelSerie.SerieFormat.LineProperties.LinePattern = ExcelChartLinePattern.Solid;
						excelSerie.SerieFormat.LineProperties.LineColor = dataSerie.Color;

						if (dataSerie.DisplayTransparency.HasValue) {
							excelSerie.SerieFormat.LineProperties.Transparency = dataSerie.DisplayTransparency.Value / 100.0;
						}
					}

					excelSerie.SerieFormat.MarkerBackgroundColor = dataSerie.Color;
					excelSerie.SerieFormat.MarkerForegroundColor = dataSerie.Color;
				}
				else {
					// This is a serie of a type that is a shape with fill.
					excelSerie.SerieFormat.Fill.FillType = ExcelFillType.SolidColor;
					excelSerie.SerieFormat.Fill.Solid();
					excelSerie.SerieFormat.Fill.ForeColor = dataSerie.Color;
					excelSerie.SerieFormat.Fill.BackColor = dataSerie.Color;

					if (dataSerie.DisplayTransparency.HasValue) {
						excelSerie.SerieFormat.Fill.Transparency = dataSerie.DisplayTransparency.Value / 100.0;
					}
				}
			}
		}


		/// <summary>
		/// Gets the type of the excel chart by the data serie type.
		/// </summary>
		/// <param name="dataSerieType">Type of the data serie.</param>
		/// <param name="chart">The chart.</param>
		/// <param name="excelChart">The excel chart.</param>
		/// <param name="isSingleTypeChart">if set to <c>true</c> the chart has a single serie type.</param>
		private ExcelChartType ConvertChartType(DataSerieType dataSerieType, Chart chart, IChartShape excelChart, bool isSingleTypeChart) {
			ExcelChartType chartType;

			switch (chart.ChartType) {
				case ChartType.Combo:
					chartType = ConvertComboChartType(dataSerieType, chart, isSingleTypeChart);
					break;
				case ChartType.ComboHorizontal:
					chartType = ConvertHorizontalComboChartType(dataSerieType, chart, excelChart, isSingleTypeChart);
					break;
				case ChartType.Radar:
					chartType = ConvertRadarChartType(dataSerieType);
					break;
				case ChartType.Pie:
					chartType = chart.Display3D ? ExcelChartType.Pie_3D : ExcelChartType.Pie;
					break;
				case ChartType.Donut:
					chartType = ExcelChartType.Doughnut;
					break;
				case ChartType.Gauge:
					WriteComment(excelChart, Langue.msg_chart_excel_gauge_unsupported);
					chartType = ExcelChartType.Doughnut;
					break;
				default:
					throw new ArgumentOutOfRangeException("chart.ChartType");
			}

			return chartType;
		}

		/// <summary>
		/// Gets the type of Excel chart as a radar graph by the given data serie type.
		/// </summary>
		/// <param name="dataSerieType">Type of the data serie.</param>
		/// <returns></returns>
		private ExcelChartType ConvertRadarChartType(DataSerieType dataSerieType) {
			// For now, radar charts are limited to have all series with the same type.
			return ExcelChartType.Radar_Markers;

			/*
			ExcelChartType chartType;

			switch (dataSerieType) {
				case DataSerieType.None:
				case DataSerieType.Column:
				case DataSerieType.Marker:
					chartType = ExcelChartType.Radar_Markers;
					break;
				case DataSerieType.Spline:
				case DataSerieType.Line:
					chartType = ExcelChartType.Radar;
					break;
				case DataSerieType.AreaLine:
				case DataSerieType.AreaSpline:
					chartType = ExcelChartType.Radar_Filled;
					break;
				default:
					throw new ArgumentOutOfRangeException("dataSerieType");
			}

			return chartType;*/
		}

		/// <summary>
		/// Gets the type of Excel chart as a combo graph by the given data serie type.
		/// </summary>
		/// <param name="dataSerieType">Type of the data serie.</param>
		/// <param name="chart">The chart.</param>
		/// <param name="isSingleTypeChart"></param>
		/// <returns></returns>
		private ExcelChartType ConvertComboChartType(DataSerieType dataSerieType, Chart chart, bool isSingleTypeChart) {
			ExcelChartType excelSerieType;

			switch (dataSerieType) {
				case DataSerieType.Marker:
					// We use the line markers type but with transparent line.
					excelSerieType = ExcelChartType.Line_Markers;
					break;
				case DataSerieType.Line:
					excelSerieType = ExcelChartType.Line_Markers;
					break;
				case DataSerieType.AreaLine:
					excelSerieType = chart.DisplayStackSeries ? ExcelChartType.Area_Stacked : ExcelChartType.Area;
					break;
				case DataSerieType.None: // The default serie type is Column. 
				case DataSerieType.Column:
					if (chart.Display3D && isSingleTypeChart) {
						excelSerieType = chart.DisplayStackSeries ? ExcelChartType.Column_Stacked_3D : ExcelChartType.Column_Clustered_3D;
					}
					else {
						excelSerieType = chart.DisplayStackSeries ? ExcelChartType.Column_Stacked : ExcelChartType.Column_Clustered;
					}
					break;
				case DataSerieType.Spline:
				case DataSerieType.AreaSpline: // we don't have AreaSpline in Excel. Do same as spline.
					excelSerieType = ExcelChartType.Scatter_SmoothedLine_Markers;
					break;
				// Do not assign a type.
				default:
					throw new ArgumentOutOfRangeException("dataSerieType");
			}
			return excelSerieType;
		}

		/// <summary>
		/// Gets the type of Excel chart as a horizontal combo graph by the given data serie type.
		/// </summary>
		/// <param name="dataSerieType">Type of the data serie.</param>
		/// <param name="chart">The chart.</param>
		/// <param name="excelChart">The excel chart.</param>
		/// <param name="isSingleTypeChart">if set to <c>true</c> the chart has a single serie type.</param>
		private ExcelChartType ConvertHorizontalComboChartType(DataSerieType dataSerieType, Chart chart, IChartShape excelChart, bool isSingleTypeChart) {
			if (!isSingleTypeChart) {
				// We cannot have a horizontal chart with two axis or two serie types.
				WriteComment(excelChart, Langue.msg_chart_excel_horizontal_types_unsupported);
				return ConvertComboChartType(dataSerieType, chart, false);
			}

			ExcelChartType excelSerieType;

			switch (dataSerieType) {
				case DataSerieType.Marker:
				case DataSerieType.Line:
				case DataSerieType.AreaLine:
				case DataSerieType.Spline:
				case DataSerieType.AreaSpline:
					// Horizontal combo of these types is unsupported. Do it vertical instead.
					WriteComment(excelChart, Langue.msg_chart_excel_horizontal_types_unsupported);
					excelSerieType = ConvertComboChartType(dataSerieType, chart, true);
					break;
				case DataSerieType.None: // The default serie type is Column.
				case DataSerieType.Column:
					if (chart.Display3D) {
						excelSerieType = chart.DisplayStackSeries ? ExcelChartType.Bar_Stacked_3D : ExcelChartType.Bar_Clustered_3D;
					}
					else {
						excelSerieType = chart.DisplayStackSeries ? ExcelChartType.Bar_Stacked : ExcelChartType.Bar_Clustered;
					}
					break;
				default:
					throw new ArgumentOutOfRangeException("dataSerieType");
			}
			return excelSerieType;
		}

		/// <summary>
		/// Sets the chart layout.
		/// This method takes care of the chart and series types, and the axis definitions.
		/// </summary>
		/// <param name="excelChart">The excel chart.</param>
		/// <param name="chartToRender">The chart to render.</param>
		protected void SetChartLayout(IChartShape excelChart, RenderableChart chartToRender) {
			List<ChartAxis> axisInUse = GetAxisInUse(chartToRender);

			bool isComboChart = SetChartType(excelChart, chartToRender.Chart, chartToRender.DataSeries, axisInUse);

			SetSeries(excelChart, chartToRender.Chart, chartToRender.DataSeries, isComboChart, axisInUse);
			SetAxis(excelChart, axisInUse, chartToRender.Chart);
		}

		/// <summary>
		/// Check if the excel chart has a horizontal layout
		/// </summary>
		/// <param name="excelChart">The excel chart.</param>
		/// <returns></returns>
		private bool IsHorizontalChart(IChartShape excelChart){
			ExcelChartType chartType = excelChart.ChartType;

			return chartType == ExcelChartType.Bar_Clustered ||
			       chartType == ExcelChartType.Bar_Clustered_3D ||
			       chartType == ExcelChartType.Bar_Stacked ||
			       chartType == ExcelChartType.Bar_Stacked_100 ||
			       chartType == ExcelChartType.Bar_Stacked_3D;
		}

		/// <summary>
		/// Gets the axis in use by series in the chart.
		/// </summary>
		/// <param name="chartToRender">The chart to render.</param>
		/// <returns></returns>
		private static List<ChartAxis> GetAxisInUse(RenderableChart chartToRender) {
			IEnumerable<DataSerie> dataSeries = chartToRender.Chart.Series.GetVisibleDataSeries();
			List<string> yAxisKeys = dataSeries.Select(s => s.KeyYAxis).Distinct().ToList();

			// Get axis that are either directly referenced by series, 
			// or the primary axis if exists a series that does not directly reference any axis.
			// The result is sorted such that the primary axis, if used, is the first axis in the list.
			List<ChartAxis> axisInUse =
				chartToRender.Chart.Axis.Values.Where(
					a => yAxisKeys.Contains(a.KeyChartAxis) || (a.IsPrimary && yAxisKeys.Any(string.IsNullOrEmpty))).OrderBy(a => !a.IsPrimary).
					ToList();

			if (!axisInUse.Any()) {
				throw new InvalidOperationException("No axis in use were found. Inconsistant state of the Chart object.");
			}

			return axisInUse;
		}

		/// <summary>
		/// Sets the series.
		/// </summary>
		/// <param name="excelChart">The excel chart.</param>
		/// <param name="vizChart">The viz chart.</param>
		/// <param name="dataSeries">The data series.</param>
		/// <param name="isComboChart">if set to <c>true</c> [is combo chart].</param>
		/// <param name="axisInUse">The axis in use.</param>
		protected virtual void SetSeries(IChartShape excelChart, Chart vizChart, List<DataSerie> dataSeries, bool isComboChart, List<ChartAxis> axisInUse) {
			// Break down the series to using the primary and secondary axis.
			var seriesInPrimaryAxis = new List<Tuple<DataSerie, IChartSerie>>();
			var seriesInSecondaryAxis = new List<Tuple<DataSerie, IChartSerie>>();

			foreach (IChartSerie excelSerie in excelChart.Series.Where(s => !s.Name.Equals("Categories"))) {
				DataSerie dataSerie = dataSeries.First(d => d.Name.Equals(excelSerie.Name));
				var usePrimaryAxis = IsSerieInPrimaryAxis(dataSerie, vizChart, axisInUse);
				var tuple = new Tuple<DataSerie, IChartSerie>(dataSerie, excelSerie);

				if (usePrimaryAxis) {
					seriesInPrimaryAxis.Add(tuple);
				}
				else {
					seriesInSecondaryAxis.Add(tuple);
				}
			}

			// Style each serie. First the ones on the primary axis, then on the second axis.
			// This is because Syncfusion throws if axis are styled not by axis order. Grrr!

			foreach (var seriesInPrimaryAxi in seriesInPrimaryAxis) {
				SetSerie(seriesInPrimaryAxi.Item1, seriesInPrimaryAxi.Item2, vizChart, excelChart, isComboChart, axisInUse, true);
			}

			foreach (var seriesInSecondaryAxi in seriesInSecondaryAxis) {
				SetSerie(seriesInSecondaryAxi.Item1, seriesInSecondaryAxi.Item2, vizChart, excelChart, isComboChart, axisInUse, false);
			}

		}

		/// <summary>
		/// Creates the Excel chart within the given sheet according to the given parameters.
		/// </summary>
		/// <param name="dataRange">The data range.</param>
		/// <param name="chart">The chart.</param>
		/// <returns></returns>
		protected virtual IChartShape CreateChart(IRange dataRange, Chart chart) {
			IChartShape excelChart = dataRange.Worksheet.Charts.Add();

			// Order of these two statements is important: Performance improves greatly if the object 
			// knows in advance that it is going to create series out of the columns in the data range.
			excelChart.IsSeriesInRows = false;
			excelChart.DataRange = dataRange;

			return excelChart;
		}

	}
}
﻿using System;
using System.Collections.Generic;
using System.Configuration.Provider;
using System.Linq;
using System.Reflection;
using System.Web.Security;
using BookSleeve;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// A concrete implementation of the Session provider using Redis.
	/// </summary>
	public class RedisSessionProvider : SessionProvider {

		private const string const_attribute_redis_host = "redisHost";
		private const string const_attribute_redis_port = "redisPort";
		private const string const_attribute_redis_database = "redisDatabase";
		private const string const_attribute_redis_password = "redisPassword";


		private const string const_sessionid_prefix = "sessionID_";
		private const string const_requestid = "requestID";

		private static Object _redisConnectionLock = new Object();
		private static RedisConnection _redisClient;


		/// <summary>
		/// Gets or sets the redis host.
		/// </summary>
		/// <value>
		/// The redis host.
		/// </value>
		public string RedisHost { get; set; }


		/// <summary>
		/// Gets or sets the redis port.
		/// </summary>
		/// <value>
		/// The redis port.
		/// </value>
		public int RedisPort { get; set; }

		/// <summary>
		/// Gets or sets the redis database.
		/// </summary>
		/// <value>
		/// The redis database.
		/// </value>
		public int RedisDatabase { get; set; }

		/// <summary>
		/// Gets or sets the redis password.
		/// </summary>
		/// <value>
		/// The redis password.
		/// </value>
		public string RedisPassword { get; set; }

		/// <summary>
		/// Gets the redis client.
		/// </summary>
		private RedisConnection RedisClient {
			get {
				if (_redisClient != null && (_redisClient.State == RedisConnectionBase.ConnectionState.Open || _redisClient.State == RedisConnectionBase.ConnectionState.Opening)) return _redisClient;
				lock (_redisConnectionLock) {
					if (_redisClient != null && (_redisClient.State == RedisConnectionBase.ConnectionState.Open || _redisClient.State == RedisConnectionBase.ConnectionState.Opening)) return _redisClient;
					TracingService.Write(TraceEntrySeverity.Information, "Redis connection is opening");
					_redisClient = new RedisConnection(RedisHost, RedisPort, password: RedisPassword);
					_redisClient.Closed += (sender, e) => TracingService.Write(TraceEntrySeverity.Warning, "Redis connection was closed");
					_redisClient.Open();
				}
				return _redisClient;
			}
		}


		/// <summary>
		/// Initializes the provider.
		/// </summary>
		/// <param name="name"></param>
		/// <param name="config"></param>
		public override void Initialize(string name, System.Collections.Specialized.NameValueCollection config) {
			base.Initialize(name, config);

			if (String.IsNullOrEmpty(config[const_attribute_redis_host])) {
				throw new ProviderException(const_attribute_redis_host + " is missing");
			}
			RedisHost = config[const_attribute_redis_host];
			config.Remove(const_attribute_redis_host);

			if (String.IsNullOrEmpty(config[const_attribute_redis_port])) {
				throw new ProviderException(const_attribute_redis_port + " is missing");
			}
			RedisPort = int.Parse(config[const_attribute_redis_port]);
			config.Remove(const_attribute_redis_port);

			if (String.IsNullOrEmpty(config[const_attribute_redis_database])) {
				throw new ProviderException(const_attribute_redis_database + " is missing");
			}
			RedisDatabase = int.Parse(config[const_attribute_redis_database]);
			config.Remove(const_attribute_redis_database);


			if (!String.IsNullOrEmpty(config[const_attribute_redis_password])) {
				RedisPassword = (config[const_attribute_redis_password]);
				config.Remove(const_attribute_redis_password);
			}
			else {
				RedisPassword = null;
			}

		}


		/// <summary>
		/// Encodes the session hash key.
		/// </summary>
		/// <param name="SessionID">The session ID.</param>
		/// <returns></returns>
		private string EncodeSessionHashKey(string SessionID) {
			return EncodeCacheKey(const_sessionid_prefix + SessionID);
		}

		/// <summary>
		/// Decodes the session hash key.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <returns>
		/// the real session id
		/// </returns>
		private string DecodeSessionHashKey(string key) {
			key = DecodeCacheKey(key);
			var retVal = key.Replace(const_sessionid_prefix, "");
			return retVal;
		}

		/// <summary>
		/// Abandons the session. (deletes cookie session, and signout the user).
		/// </summary>
		public override void Abandon() {
			var sessionID = EncodeSessionHashKey(GetSessionID());
			if (!String.IsNullOrEmpty(sessionID)) {
				RedisClient.Keys.Remove(RedisDatabase, sessionID).Wait();
				RedisClient.Hashes.Remove(RedisDatabase, ActiveSessionsKey, sessionID).Wait();
			}
			FormsAuthentication.SignOut();
		}


		/// <summary>
		/// Adds the specified session key and value to the session.
		/// </summary>
		/// <param name="name">The name of the key.</param>
		/// <param name="value">The value.</param>
		public override void Add(string name, object value) {
			var expirationInMinutes = GetExpirationInMinutes(name);
			Add(name, value, expirationInMinutes);
		}

		/// <summary>
		/// Adds the specified session key and value to the session.
		/// </summary>
		/// <param name="dictionary">The dictionary.</param>
		public override void Add(IDictionary<string, object> dictionary) {

			var expirationInMinutes = GetExpirationInMinutes(string.Empty);

			Dictionary<string, byte[]> serializedValuesDictionary = new Dictionary<string, byte[]>(dictionary.Count);
			foreach (var item in dictionary) {
				serializedValuesDictionary.Add(item.Key, item.Value.ToByteArray());
			}

			using (var tx = RedisClient.CreateTransaction()) {
				var sessionID = EncodeSessionHashKey(GetSessionID());
				RedisClient.Hashes.Set(RedisDatabase, sessionID, serializedValuesDictionary);
				RedisClient.Keys.Expire(RedisDatabase, sessionID, Convert.ToInt32(expirationInMinutes * 60));
				tx.Execute();
			}
		}


		/// <summary>
		/// Adds the specified session key and value to the session.
		/// </summary>
		/// <param name="name">The name of the key.</param>
		/// <param name="value">The value.</param>
		/// <param name="expirationInMinutes">The expiration in minutes.</param>
		private void Add(string name, object value, double expirationInMinutes) {
			using (var tx = RedisClient.CreateTransaction()) {
				var sessionID = EncodeSessionHashKey(GetSessionID());
				RedisClient.Hashes.Set(RedisDatabase, sessionID, name, value.ToByteArray());
				RedisClient.Keys.Expire(RedisDatabase, sessionID, Convert.ToInt32(expirationInMinutes * 60));
				tx.Execute();
			}
		}

		/// <summary>
		/// Gets the expiration in minutes.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <returns></returns>
		private int GetExpirationInMinutes(string key) {
			var expirationInMinutes = key == SessionKey.const_session_context ? SessionService.Timeout.TotalMinutes : SessionService.Timeout.TotalMinutes + 2;
			return Convert.ToInt32(expirationInMinutes);
		}

		/// <summary>
		/// Creates or slides the session when it already exists.
		/// </summary>
		/// <param name="sessionID">The session ID.</param>
		/// <param name="userName">The user name.</param>
		/// <param name="force">if set to <c>true</c> force create or slide. Otherwise, depending on context.</param>
		public override void CreateOrSlideSession(string sessionID, string userName, bool force) {
			try {
				if (!String.IsNullOrEmpty(sessionID)) {
					if (MustRenewSession(sessionID, force)) {

						Helper.StartNewThread(() => {
							// Remove inactive sessions
							// TODO: check how to pipeline
							// take sessions from the active session single hash
							var activeSessions = new List<string>(RedisClient.Hashes.GetKeys(RedisDatabase, ActiveSessionsKey).Result);
							// find all session hashes by pattern
							var realActiveSessions = new List<string>(RedisClient.Keys.Find(RedisDatabase, EncodeSessionHashKey("*")).Result);
							// remove inactive sessions
							var inactiveSessions = activeSessions.Where(session => !realActiveSessions.Contains(session)).ToList();

							foreach (var inactiveSession in inactiveSessions) {
								byte[] sessionContextBinary = RedisClient.Hashes.Get(RedisDatabase, ActiveSessionsKey, inactiveSession).Result;
								// defensive against multiple threads, but no locking.
								if (sessionContextBinary != null) {
									string sessionContexString = System.Text.Encoding.Default.GetString(sessionContextBinary);
									RedisClient.Hashes.Remove(RedisDatabase, ActiveSessionsKey, inactiveSession);
									var sc = new SessionContext(sessionContexString);
									EndSession(inactiveSession, sc.UserName, sc.ExpirationDate);
								}
								else {
									RedisClient.Hashes.Remove(RedisDatabase, ActiveSessionsKey, inactiveSession);
								}
							}
						}, copyContext: false);

						// Create a new session 
						DateTime expirationDate = DateTime.UtcNow + SessionService.Timeout;
						int expirationInSeconds = Convert.ToInt32((expirationDate - DateTime.UtcNow).TotalSeconds);
						// Set is required required in order for redis to create the hash in the first place, (expire on an empty hash doesn't work). 
						// RequestId can be used for diagnostics when compared with the trace
						RedisClient.Hashes.Set(RedisDatabase, EncodeSessionHashKey(sessionID), const_requestid, ContextHelper.RequestId);
						RedisClient.Keys.Expire(RedisDatabase, EncodeSessionHashKey(sessionID), expirationInSeconds).Wait();

						var sessionContext = new SessionContext { ExpirationDate = expirationDate, LastRefreshDate = DateTime.UtcNow, SessionID = sessionID, UserName = userName };
						RedisClient.Hashes.Set(RedisDatabase, ActiveSessionsKey, EncodeSessionHashKey(sessionID), sessionContext.ToString()).Wait();
						Membership.GetUser(new UpdateLastActivityArgument { UserName = userName }, true);

					}
				}
			}
			catch (Exception ex) {
				TracingService.Write(TraceEntrySeverity.Error, ex.Message);
			}
		}

		/// <summary>
		/// Ends the session.
		/// </summary>
		/// <param name="sessionID">The session ID.</param>
		/// <param name="userName">Name of the user.</param>
		/// <param name="expirationDate">The expiration date.</param>
		public override void EndSession(string sessionID, string userName, DateTime expirationDate) {
			object provider = Membership.Provider;
			Type type = provider.GetType();
			object[] arguments = new object[] { userName, DecodeSessionHashKey(sessionID), null, expirationDate };
			type.InvokeMember("AddLoginHistory",
							  BindingFlags.Default | BindingFlags.InvokeMethod,
							  null,
							  provider,
							  arguments);
			RedisClient.Hashes.Remove(RedisDatabase, ActiveSessionsKey, sessionID).Wait();
			RedisClient.Keys.Remove(RedisDatabase, EncodeSessionHashKey(sessionID)).Wait();
		}

		/// <summary>
		/// Gets the session item with the specified name.
		/// </summary>
		/// <value></value>
		public override object Get(string name) {
			var retVal = Get(GetSessionID(), name);
			return retVal;
		}

		/// <summary>
		/// Gets the session item with the specified name.
		/// </summary>
		/// <param name="sessionID">The session ID.</param>
		/// <param name="name">The name.</param>
		/// <returns></returns>
		protected override object Get(string sessionID, string name) {
			object retVal = ContextHelper.Get(name);
			if (retVal != null)
				return retVal;

			byte[] byteArray = RedisClient.Hashes.Get(RedisDatabase, EncodeSessionHashKey(sessionID), name).Result;
			if (byteArray == null)
				return null;

			retVal = byteArray.Deserialize<object>();
			ContextHelper.Add(name, retVal);
			return retVal;
		}

		/// <summary>
		/// Gets the data.
		/// </summary>
		/// <param name="keys">The keys.</param>
		/// <returns>A dictionary with keys and values found in session, if nothing is found null is placed for that key</returns>
		public override IDictionary<string, object> GetData(List<string> keys) {
			var sessionId = GetSessionID();
			// efficency trick to prevent out of process call 
			Dictionary<string, object> retVal = new Dictionary<string, object>();
			List<String> newKeys = new List<string>();
			foreach (var key in keys) {
				object value = ContextHelper.Get(key);
				if (value != null) {
					retVal.Add(key, value);
				}
				else {
					newKeys.Add(key);
				}
			}
			if (newKeys.Count > 0) {
				byte[][] values = RedisClient.Hashes.Get(RedisDatabase, EncodeSessionHashKey(sessionId), newKeys.ToArray()).Result;

				for (int i = 0; i < values.Count(); i++) {
					var key = newKeys[i];
					object value = null;
					if ((values[i] != null) && (values[i].Count() > 0)) {
						value = values[i].Deserialize<object>();
						if (value != null) {
							ContextHelper.Add(key, value);
						}
					}
					retVal.Add(key, value);
				}
			}
			return retVal;
		}

		/// <summary>
		/// Removes the specified name.
		/// </summary>
		/// <param name="name">The name.</param>
		public override void Remove(string name) {
		
			ContextHelper.Remove(name);

			string sessionID = EncodeSessionHashKey(GetSessionID());
			RedisClient.Hashes.Remove(RedisDatabase, sessionID, name).Wait();
		}

		/// <summary>
		/// Removes the specified keys.
		/// </summary>
		/// <param name="keys">The keys.</param>
		public override void Remove(List<string> keys) {
			
			foreach (string key in keys) {
				ContextHelper.Remove(key);
			}

			string sessionID = EncodeSessionHashKey(GetSessionID());
			RedisClient.Hashes.Remove(RedisDatabase, sessionID, keys.ToArray()).Wait();
		}

		/// <summary>
		/// Sessions the exist.
		/// </summary>
		/// <returns></returns>
		public override bool SessionExist() {
			string sessionID = EncodeSessionHashKey(GetSessionID());
			var retVal = RedisClient.Hashes.Exists(RedisDatabase, ActiveSessionsKey, sessionID).Result;
			return retVal;
		}

		/// <summary>
		/// Gets the active sessions.
		/// </summary>
		/// <returns></returns>
		public override string[] GetActiveSessions() {
			var retVal = RedisClient.Hashes.GetKeys(RedisDatabase, ActiveSessionsKey).Result;
			return retVal.Length > 0 ? retVal.Select(DecodeSessionHashKey).ToArray() : retVal;
		}



		/// <summary>
		/// Encodes the cache key.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <returns></returns>
		private static string EncodeCacheKey(string key) {
			string encodeCacheKey = string.Format("{0}::{1}", ContextHelper.ApplicationName, key);
			return encodeCacheKey;
		}

		/// <summary>
		/// Decoded the cache key.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <returns></returns>
		private static string DecodeCacheKey(string key) {
			string decodedCacheKey = key.Substring(key.IndexOf("::", StringComparison.Ordinal) + 2);
			return decodedCacheKey;
		}


		/// <summary>
		/// Gets the active sessions key.
		/// </summary>
		private string ActiveSessionsKey {
			get { return EncodeCacheKey(CacheKey.const_cache_activesessions); }
		}
	}
}

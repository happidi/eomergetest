﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.Security;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// A concrete implementation of the Session provider using the default Cache service.
	/// </summary>
	public class CacheBasedSessionProvider : SessionProvider {

		/// <summary>
		/// The separator used to store session items in cache. See EncodeCacheKey and DecodeCacheKey.
		/// </summary>
		private const string const_separator = ":";

		/// <summary>
		/// Encode the cache key prefixing the name with SessionID.
		/// </summary>
		/// <param name="sessionID">The session id.</param>
		/// <param name="key">The key of the item.</param>
		/// <returns></returns>
		private string EncodeCacheKey(string sessionID, string key) {
			return sessionID + const_separator + key;
		}

		/// <summary>
		/// Encode the cache key prefixing the name with SessionID.
		/// </summary>
		/// <param name="name">The name.</param>
		/// <returns></returns>
		private string EncodeCacheKey(string name) {
			string cacheKey = EncodeCacheKey(GetSessionID(), name);

			if (cacheKey.StartsWith(const_separator)) {
				throw new ArgumentException("No session ID.");
			}

			return cacheKey;
		}

		/// <summary>
		/// Decoded the cache key.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <returns></returns>
		private string DecodeCacheKey(string key) {
			string decodedCacheKey = key.Substring(key.IndexOf(const_separator, StringComparison.Ordinal) + const_separator.Length);
			return decodedCacheKey;
		}

		/// <summary>
		/// Abandons the session. (deletes cookie session, and signout the user).
		/// </summary>
		public override void Abandon() {
			var sessionID = GetSessionID();
			if (!String.IsNullOrEmpty(sessionID)) {
				var sessionContext = Get(SessionKey.const_session_context) as SessionContext;
				if (sessionContext != null && sessionContext.Keys != null) {
					foreach (var key in sessionContext.Keys) {
						CacheService.Remove(EncodeCacheKey(sessionID, key));
					}
					CacheService.Lock(() => {
						var activeSessions = CacheService.Get<List<ActiveSession>>(CacheKey.const_cache_activesessions);
						if (activeSessions == null) return;
						activeSessions = activeSessions.Where(p => p.SessionID != sessionID).ToList();
						CacheService.Add(CacheKey.const_cache_activesessions, activeSessions);
					}, async: true);
				}
			}
			FormsAuthentication.SignOut();
		}

		/// <summary>
		/// Adds the specified session key and value to the session.
		/// </summary>
		/// <param name="name">The name of the key.</param>
		/// <param name="value">The value.</param>
		public override void Add(string name, object value) {
			var expirationInMinutes = GetExpirationInMinutes(name);
			Add(name, value, expirationInMinutes);
		}

	    /// <summary>
	    /// Adds the specified session key and value to the session.
	    /// </summary>
	    /// <param name="dictionary">The dictionary.</param>
	    public override void Add(IDictionary<string, object> dictionary) {
	        foreach(var item in dictionary) {
	            Add(item.Key, item.Value);
	        }
	    }

	    /// <summary>
		/// Adds the specified session key and value to the session.
		/// </summary>
		/// <param name="name">The name of the key.</param>
		/// <param name="value">The value.</param>
		/// <param name="expirationInMinutes">The expiration in minutes.</param>
		private void Add(string name, object value, double expirationInMinutes) {
			string cacheKey = EncodeCacheKey(name);
			AddKeyToSessionContext(GetSessionID(), name);
			CacheService.Add(cacheKey, value, expirationInMinutes);
		}

		/// <summary>
		/// Add the specified name composed of sessionID and key.
		/// The value is initially blank and then will contain the username.
		/// </summary>
		/// <param name="sessionID">The session ID.</param>
		/// <param name="key">The key.</param>
		/// <param name="value">The value.</param>
		/// <param name="addKeyToSession">if set to <c>true</c> [add key to session].</param>
		private void Add(string sessionID, string key, object value, bool addKeyToSession = true) {
			string cacheKey = String.IsNullOrEmpty(sessionID) ? key : EncodeCacheKey(sessionID, key);
			if (cacheKey.StartsWith("_"))
				throw new ArgumentException();
			if (addKeyToSession)
				AddKeyToSessionContext(sessionID, key);
			var expirationInMinutes = GetExpirationInMinutes(key);
			CacheService.Add(cacheKey, value, expirationInMinutes);
		}

		/// <summary>
		/// Gets the expiration in minutes.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <returns></returns>
		private double GetExpirationInMinutes(string key) {
			var expirationInMinutes = key == SessionKey.const_session_context ? SessionService.Timeout.TotalMinutes : SessionService.Timeout.TotalMinutes + 2;
			return expirationInMinutes;
		}


		/// <summary>
		/// Adds the key of a session item to session context.
		/// </summary>
		/// <param name="sessionID">The session ID.</param>
		/// <param name="key">The key.</param>
		private void AddKeyToSessionContext(string sessionID, string key) {
			var sessionContext = Get(sessionID, SessionKey.const_session_context) as SessionContext;
			if (sessionContext != null && sessionContext.Keys != null && sessionContext.Keys.Contains(key))
				return;
			CacheService.Lock(() => {
				sessionContext = Get(sessionID, SessionKey.const_session_context) as SessionContext;
				if (sessionContext == null || sessionContext.Keys == null || sessionContext.Keys.Contains(key)) return;
				sessionContext.Keys.Add(key);
				Add(sessionID, SessionKey.const_session_context, sessionContext, false);
			}, sessionID, async: true);
		}

		/// <summary>
		/// Gets the session item with the specified name.
		/// </summary>
		/// <param name="sessionID">The session ID.</param>
		/// <param name="name">The name.</param>
		/// <returns></returns>
		protected override object Get(string sessionID, string name) {
			return CacheService.GetData(EncodeCacheKey(sessionID, name));
		}

		/// <summary>
		/// Creates or slides the session when it already exists.
		/// </summary>
		/// <param name="sessionID">The session ID.</param>
		/// <param name="userName">The user name.</param>
		/// <param name="force">if set to <c>true</c> force create or slide. Otherwise, depending on context.</param>
		public override void CreateOrSlideSession(string sessionID, string userName, bool force) {
			try {
				if (!String.IsNullOrEmpty(sessionID)) {
					if (MustRenewSession(sessionID, force)) {
						//TracingService.Write(TraceEntrySeverity.Information, "Renewing session " + sessionID);
						CacheService.Lock(() => {
							var activeSessions = CacheService.Get<List<ActiveSession>>(CacheKey.const_cache_activesessions, true);
							var realActiveSessions = (Dictionary<string, object>)CacheService.GetData(activeSessions.Select(p => EncodeCacheKey(p.SessionID, SessionKey.const_session_context)).ToList());
							var inactiveSessions = activeSessions.Where(session => !realActiveSessions.ContainsKey(EncodeCacheKey(session.SessionID, SessionKey.const_session_context))).ToList();
							if (!activeSessions.Exists(p => p.SessionID == sessionID))
								activeSessions.Add(new ActiveSession {
									UserName = userName,
									SessionID = sessionID,
									ExpirationDate = DateTime.UtcNow + SessionService.Timeout
								});
							foreach (var inactiveSession in inactiveSessions) {
								activeSessions.Remove(inactiveSession);
								EndSession(inactiveSession.SessionID, inactiveSession.UserName, inactiveSession.ExpirationDate);

							}
							CacheService.Add(CacheKey.const_cache_activesessions, activeSessions);

						});

						CacheService.Lock(() => {
							DateTime expirationDate = DateTime.UtcNow + SessionService.Timeout;
							var sessionContext = Get(sessionID, SessionKey.const_session_context) as SessionContext
												 ??
												 new SessionContext {
													 ExpirationDate = expirationDate,
													 UserName = userName,
													 Keys = new List<string> { SessionKey.const_session_context } // this list of keys is initialized with the context key.
												 };
							sessionContext.LastRefreshDate = DateTime.UtcNow;
							var cachedValues = CacheService.GetData(sessionContext.Keys.Select(key => EncodeCacheKey(sessionID, key)).ToList());
							// the keys of the result dictionnary are in the form {guid}:key_name, so we don t need to pass againg the sessionID when renewing them.
							foreach (var item in cachedValues) {
								Add(null, item.Key, item.Value, false);

							}
							Membership.GetUser(new UpdateLastActivityArgument { UserName = userName }, true);
							Add(sessionID, SessionKey.const_session_context, sessionContext, false); // no need to add this key to the Keys as it is the context key
						}, sessionID);
					}
				}
			}
			catch { }
		}

		
		/// <summary>
		/// Ends the session.
		/// </summary>
		/// <param name="sessionID">The session ID.</param>
		/// <param name="userName">Name of the user.</param>
		/// <param name="expirationDate">The expiration date.</param>
		public override void EndSession(string sessionID, string userName, DateTime expirationDate) {
			var provider = (IFOLSqlMembershipProvider)Membership.Provider;
			provider.AddLoginHistory(userName, sessionID, null, expirationDate);
		}

		/// <summary>
		/// Gets the session item with the specified name.
		/// </summary>
		/// <value></value>
		public override object Get(string name) {
			return CacheService.GetData(EncodeCacheKey(name));
		}

		/// <summary>
		/// Gets the data.
		/// </summary>
		/// <param name="keys">The keys.</param>
		/// <returns></returns>
		public override IDictionary<string, object> GetData(List<string> keys) {
			var data = CacheService.GetData(keys.Select(EncodeCacheKey).ToList());
			Dictionary<string,object> retVal = new Dictionary<string, object>();
			foreach (var kvp in data) {
				retVal.Add(DecodeCacheKey(kvp.Key),kvp.Value);
			}
			return retVal;
		}

		/// <summary>
		/// Removes the specified name.
		/// </summary>
		/// <param name="name">The name.</param>
		public override void Remove(string name) {
			CacheService.Remove(EncodeCacheKey(name));
		}

	    /// <summary>
	    /// Removes the specified keys.
	    /// </summary>
	    /// <param name="keys">The keys.</param>
	    public override void Remove(List<string> keys) {
	        keys.ForEach(Remove);
	    }

	    /// <summary>
		/// Returns true if the current session exists.
		/// </summary>
		/// <returns></returns>
		public override bool SessionExist() {
			string sessionID = GetSessionID();
			return CacheService.GetData(EncodeCacheKey(sessionID, SessionKey.const_session_context)) != null;
		}

		/// <summary>
		/// Gets the active sessions.
		/// </summary>
		/// <returns></returns>
		public override string[] GetActiveSessions() {
			var lst = CacheService.Get<List<ActiveSession>>(CacheKey.const_cache_activesessions);
			return lst != null ? lst.Select(p => p.SessionID).ToArray() : null;
		}

		
	}
}

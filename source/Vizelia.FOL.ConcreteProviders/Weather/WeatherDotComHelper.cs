﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// WeatherDotCom Helper
	/// </summary>
	internal static class WeatherDotComHelper {

		/// <summary>
		/// Parse the document head node to get the returned weather unit
		/// </summary>
		/// <param name="responseXML">the xml document.</param>
		/// <returns></returns>
		public static WeatherUnit GetDocumentUnit(XmlDocument responseXML) {
			WeatherUnit retVal = WeatherUnit.Celsius;
			if (responseXML != null) {
				XmlNode unitNode = responseXML.SelectSingleNode("//weather/head/ut");
				if (unitNode != null && unitNode.InnerText == "F") {
					retVal = WeatherUnit.Fahrenheit;
				}
			}
			return retVal;
		}

		/// <summary>
		/// Return the current temperature.
		/// </summary>
		/// <param name="xml">The XML.</param>
		/// <param name="documentUnit">the input unit.</param>
		/// <param name="locationUnit">the output unit.</param>
		/// <param name="valuePath">The value path.</param>
		/// <returns></returns>
		public static double GetNodeTemperature(XmlNode xml, WeatherUnit documentUnit, WeatherUnit locationUnit, string valuePath) {
			double retVal = double.NaN;
			var temperature = GetNodeValue(xml, valuePath);
			if (!String.IsNullOrWhiteSpace(temperature)) {
				if (double.TryParse(temperature, out retVal)) {
					retVal = UnitHelper.ConvertTemperature(retVal, documentUnit, locationUnit);
				}
				else { retVal = double.NaN; }
			}
			return retVal;
		}

		/// <summary>
		/// Gets the current day icon CLS.
		/// </summary>
		/// <param name="xml">the xml document.</param>
		/// <param name="path">The path.</param>
		/// <returns></returns>
		public static string GetNodeValue(XmlNode xml, string path) {
			var retVal = "";
			var node = xml.SelectSingleNode(path);
			if (node != null) {
				retVal = node.InnerText;
			}
			return retVal;
		}

		/// <summary>
		/// Gets the current date.
		/// </summary>
		/// <param name="responseXML">the xml document.</param>
		/// <returns></returns>
		public static DateTime GetCurrentDate(XmlDocument responseXML) {
			var retVal = DateTime.Now;
			return retVal;
		}

		/// <summary>
		/// Gets the node int value.
		/// </summary>
		/// <param name="xmlNode">The XML node.</param>
		/// <param name="property">The property.</param>
		/// <returns></returns>
		internal static int GetNodeIntValue(XmlNode xmlNode, string property) {
			int retVal = -1;
			var stringValue = GetNodeValue(xmlNode, property);
			if (!String.IsNullOrWhiteSpace(stringValue)) {
				int.TryParse(stringValue, out retVal);
			}
			return retVal;
		}

		/// <summary>
		/// Gets the time value as date.
		/// </summary>
		/// <param name="xml">The XML.</param>
		/// <param name="path">The path.</param>
		/// <returns></returns>
		public static DateTime GetNodeDateValue(XmlNode xml, string path) {
			DateTime retVal = DateTime.MinValue;
			var node = xml.SelectSingleNode(path);
			if (node != null) {
				retVal = DateTime.Parse(node.InnerText);
			}
			return retVal;
		}

		/// <summary>
		/// Gets the node time value as string.
		/// </summary>
		/// <param name="xml">The XML.</param>
		/// <param name="path">The path.</param>
		/// <returns></returns>
		public static string GetNodeTimeValueAsString(XmlNode xml, string path) {
			string retVal = GetNodeDateValue(xml, path).ToString(Helper.LocalizeText("langue_dotnet_time_format"));
			return retVal;
		}
	}
}

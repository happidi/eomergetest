﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Xml;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Vizelia.FOL.Providers;
using Vizelia.FOL.ServiceAgents;
namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// Concrete implementation of a standard weather provider using data coming from www.weather.com.
	/// </summary>
	public class WeatherDotComProvider : WeatherProvider {

		/// <summary>
		/// Langue weather forecast message prefix
		/// </summary>
		private const string const_weatherforecast_msgprefix = "msg_weatherforecast_";

		/// <summary>
		/// Search Url template.
		/// </summary>
		private const string const_searchurl = "http://gadgets.weather.com/search/search?where={0}";

		/// <summary>
		/// Xml Attribute for the LocationId.
		/// </summary>
		private const string const_locationid = "id";

		/// <summary>
		/// Returns a list of WeatherLocation based on a search string.
		/// </summary>
		/// <param name="search">The search.</param>
		/// <returns></returns>
		public override List<WeatherLocation> GetWeatherLocation(string search) {
			var request = WebRequest.Create(string.Format(const_searchurl, Uri.EscapeUriString(search)));
			var response = request.GetResponse();
			Stream responseStream = response.GetResponseStream();
			if (responseStream != null) {
				var reader = new StreamReader(responseStream);
				var responseXml = new XmlDocument();
				responseXml.Load(reader);

				var nodes = responseXml.SelectNodes("//search/loc");

				return (from XmlElement node in nodes
						select new WeatherLocation {
							LocationId = node.GetAttribute(const_locationid),
							Name = node.InnerText
						}).ToList();
			}
			return new List<WeatherLocation>();
		}

		/// <summary>
		/// Gets the weather five day forecast.
		/// </summary>
		/// <param name="location">The WeatherLocation.</param>
		/// <returns></returns>
		public override List<WeatherForecast> GetWeatherFiveDayForecast(WeatherLocation location) {
			var retVal = new List<WeatherForecast>();

			var weatherSA = new WeatherDotComServiceAgent();

			var responseXml = weatherSA.GetWeatherFiveDayForecast(location);
			var documentUnit = WeatherDotComHelper.GetDocumentUnit(responseXml);

			// extract today forecast
			var todayXml = responseXml.SelectSingleNode("//weather/cc");
			var locationXml = responseXml.SelectSingleNode("//weather/loc");
			var titleNodeValue = WeatherDotComHelper.GetNodeValue(todayXml, "t");
			var titleMsgCode = const_weatherforecast_msgprefix + titleNodeValue.Replace(" ", "").Replace("/", "").Replace("-", "").ToLower();
			var titleValue = Helper.LocalizeText(titleMsgCode, true) ?? titleNodeValue;
			TraceMsgCodeExists(titleMsgCode, titleValue);
			var today = new WeatherForecast {
				Unit = location.Unit,
				CurrentTemperature = Math.Round(WeatherDotComHelper.GetNodeTemperature(todayXml, documentUnit, location.Unit, "tmp")),
				DayIconNumber = WeatherDotComHelper.GetNodeValue(todayXml, "icon"),
				NightIconNumber = WeatherDotComHelper.GetNodeValue(todayXml, "moon//icon"),
				Date = WeatherDotComHelper.GetCurrentDate(responseXml),
				LocationTitle = WeatherDotComHelper.GetNodeValue(locationXml, "dnam"),
				Title = titleValue,
				Humidity = WeatherDotComHelper.GetNodeIntValue(todayXml, "hmid"),
				CurrentTime = WeatherDotComHelper.GetNodeTimeValueAsString(locationXml, "tm"),
				SunriseTime = WeatherDotComHelper.GetNodeTimeValueAsString(locationXml, "sunr"),
				SunsetTime = WeatherDotComHelper.GetNodeTimeValueAsString(locationXml, "suns")
			};

			XmlNode daysforecast = responseXml.SelectSingleNode("//weather/dayf");
			var lastDate = today.Date;
			if (daysforecast != null)
				foreach (XmlNode currDayNode in daysforecast.ChildNodes) {
					var highTemp = Math.Round(WeatherDotComHelper.GetNodeTemperature(currDayNode, documentUnit, location.Unit, "hi"));
					var lowTemp = Math.Round(WeatherDotComHelper.GetNodeTemperature(currDayNode, documentUnit, location.Unit, "low"));
					if (currDayNode.Attributes != null && currDayNode.Attributes["d"] != null)
						// today extra forecast data
						if (currDayNode.Attributes["d"].InnerText == "0") {
							today.HighTemperature = highTemp;
							today.LowTemperature = lowTemp;
							retVal.Add(today);
						}
						// next 4 days forecast data
						else {
							lastDate = lastDate.AddDays(1);
							string dayIconNumber = null;
							string nightIconNumber = null;
							string title = null;
							foreach (XmlNode part in currDayNode.ChildNodes) {
								if (part.Attributes != null && part.Attributes["p"] != null)
									if (part.Attributes["p"].InnerText == "d") {
										dayIconNumber = WeatherDotComHelper.GetNodeValue(part, "icon");
										titleNodeValue = WeatherDotComHelper.GetNodeValue(part, "t");
										titleMsgCode = const_weatherforecast_msgprefix + titleNodeValue.Replace(" ", "").Replace("/", "").Replace("-", "").ToLower();
										title = Helper.LocalizeText(titleMsgCode, true) ?? titleNodeValue;
										TraceMsgCodeExists(title, titleMsgCode);
									}
									else {
										nightIconNumber = WeatherDotComHelper.GetNodeValue(part, "icon");
									}
							}
							var weatherforecast = new WeatherForecast {
								CurrentTemperature = 0,
								Date = lastDate,
								HighTemperature = highTemp,
								LowTemperature = lowTemp,
								DayIconNumber = dayIconNumber,
								NightIconNumber = nightIconNumber,
								LocationTitle = null,
								Title = title,
								Humidity = WeatherDotComHelper.GetNodeIntValue(currDayNode, "hmid"),
								CurrentTime = null,
								SunriseTime = WeatherDotComHelper.GetNodeTimeValueAsString(currDayNode, "sunr"),
								SunsetTime = WeatherDotComHelper.GetNodeTimeValueAsString(currDayNode, "suns"),
								Unit = location.Unit
							};

							retVal.Add(weatherforecast);
						}
				}

			return retVal.OrderBy(x => x.Date).ToList();
		}

		private static void TraceMsgCodeExists(string titleMsgCode, string titleValue) {
			if (titleValue == titleMsgCode) {
				TracingService.Write(string.Format("Weather forecast: message code \"{0}\" was not found.", titleMsgCode));
			}
		}
	}
}
﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessLayer.Broker;
using Vizelia.FOL.BusinessLayer.Interfaces;
using Vizelia.FOL.Common;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.BusinessLayer {
	/// <summary>
	/// Business Layer implementation for Reporting.
	/// This class just forward the calls to the reporting provider.
	/// </summary>
	public class ReportingBusinessLayer : IReportingBusinessLayer {

		/// <summary>
		/// Renders the report as a stream.
		/// This method actually forwards call to the ReportingService static instance
		/// </summary>
		/// <param name="data">The report data.</param>
		/// <param name="columns">The columns of the data passed to the report.</param>
		/// <param name="title">The report's title.</param>
		/// <param name="exportType">The export type of the report.</param>
		/// <param name="pageFormat">The page format of the report.</param>
		/// <param name="mimeType">The resulting mime type.</param>
		/// <param name="extension">The resulting extension of the file which content is returned as a stream.</param>
		/// <returns>The report rendered as a stream.</returns>
		public MemoryStream RenderReport(IList data, IList<Column> columns, string title, ExportType exportType, FormatPageType pageFormat, out string mimeType, out string extension) {
			return ReportingService.RenderReport(data, columns, title, exportType, pageFormat, out mimeType, out extension);
		}


		/// <summary>
		/// Renders the report RDLC.
		/// </summary>
		/// <param name="reportPath">The report path.</param>
		/// <param name="dataSources">The dictionary of data sources. Each entry in the dictionary should be a List of business entities.</param>
		/// <param name="reportParameters">The dictionary of report parameters. Each entry in the dictionary should be a string.</param>
		/// <param name="exportType">Type of the export.</param>
		/// <param name="pageFormat">The page format.</param>
		/// <param name="mimeType">Type of the MIME.</param>
		/// <param name="extension">The extension.</param>
		/// <returns></returns>
		public MemoryStream RenderReportRdlc(string reportPath, Dictionary<string, object> dataSources, Dictionary<string, object> reportParameters, ExportType exportType, FormatPageType pageFormat, out string mimeType, out string extension) {
			return ReportingService.RenderReportRdlc(reportPath, dataSources, reportParameters, exportType, pageFormat, out mimeType, out extension);
		}

		/// <summary>
		/// Generates the json structure of the Reporting treeview.
		/// </summary>
		/// <param name="Key">The Key of the parent node.</param>
		/// <returns>a list of treenodes in json.</returns>
		public List<TreeNode> Reporting_GetTree(string Key) {
			var retVal = ReportingService.GetReportHierarchy(Key);
			return retVal;
		}

		/// <summary>
		/// Gets the report parameters.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <returns></returns>
		public List<ReportParameter> GetReportParameters(string key) {
			List<ReportParameter> reportParameters = ReportingService.GetReportParameters(key);
			return reportParameters;
		}

		/// <summary>
		/// Gets the report parameter values.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <param name="parameterName">Name of the parameter.</param>
		/// <returns></returns>
		public JsonStore<ReportParameterValue> GetReportParameterValues(string key, string parameterName) {
			JsonStore<ReportParameterValue> values = ReportingService.GetReportParameterValues(key, parameterName);
			return values;
		}

		/// <summary>
		/// Creates the folder with the given name under the specified path.
		/// </summary>
		/// <param name="parentPath">The parent path.</param>
		/// <param name="folderName">Name of the folder.</param>
		public void CreateFolder(string parentPath, string folderName) {
			string combinedPath = parentPath + (parentPath.EndsWith("/") ? string.Empty : "/") + folderName;
			ReportingService.CreateFolder(combinedPath);
		}

		/// <summary>
		/// Deletes the folder. All content will be deleted as well.
		/// </summary>
		/// <param name="path">The path.</param>
		public void DeleteFolder(string path) {
			ReportingService.DeleteFolder(path);
		}

		/// <summary>
		/// Deletes the report.
		/// </summary>
		/// <param name="path">The path.</param>
		public void DeleteReport(string path) {
			ReportingService.DeleteItem(path);
		}

		/// <summary>
		/// Renames or moves the item.
		/// </summary>
		/// <param name="path">The path.</param>
		/// <param name="newPath">The new path.</param>
		public void MoveItem(string path, string newPath) {
			ReportingService.MoveItem(path, newPath);
		}

		/// <summary>
		/// Populates the model action request.
		/// </summary>
		/// <param name="keyActionRequest">The key action request.</param>
		/// <returns></returns>
		public Dictionary<string, object> PopulateModelActionRequest(string keyActionRequest) {
			var broker = Helper.CreateInstance<ActionRequestBroker>();
			var brokerTask = Helper.CreateInstance<TaskBroker>();
			var brokerOccupant = Helper.CreateInstance<OccupantBroker>();

			var requestList = broker.GetList(new string[] { keyActionRequest });
			var taskList = new List<Task>();
			var requestorList = new List<Occupant>();
			var approverList = new List<Occupant>();

			List<PsetAttribute> psetAtts = new List<PsetAttribute>();
			PsetBroker psetBroker = Helper.CreateInstance<PsetBroker>();
			// loads the pset.
			foreach (var actionRequest in requestList) {
				requestorList.Add(brokerOccupant.GetItem(actionRequest.Requestor));
				if (actionRequest.KeyApprover != null) {
					var actorOcc = brokerOccupant.GetItemByKeyActor(actionRequest.KeyApprover);
					approverList.Add(actorOcc);
					actionRequest.KeyApprover = actorOcc.KeyOccupant;
				}
				actionRequest.PropertySetList = psetBroker.GetList(new string[] { actionRequest.KeyActionRequest });
				psetAtts.AddRange(GetPsetAttribute(actionRequest));
				var taskReqList = brokerTask.GetListByKeyActionRequest(actionRequest.KeyActionRequest).records;
				taskList.AddRange(taskReqList);
				foreach (var task in taskReqList) {
					task.PropertySetList = psetBroker.GetList(new string[] { task.KeyTask });
					psetAtts.AddRange(GetPsetAttribute(task));
				}
			}
			var occupantList = new List<Occupant>();
			occupantList.AddRange(requestorList);
			occupantList.AddRange(approverList);
			occupantList = occupantList.GroupBy(o => o.KeyOccupant).Select(p => p.First()).ToList();

			var retVal = new Dictionary<string, object> { { "ActionRequest", requestList }, { "Task", taskList }, { "Pset", psetAtts }, { "Occupant", occupantList }, { "Requestor", requestorList }, { "Approver", approverList } };
			return retVal;
		}


		/// <summary>
		/// Gets the pset attribute.
		/// </summary>
		/// <param name="ifcObject">The ifc object.</param>
		/// <returns></returns>
		private IEnumerable<PsetAttribute> GetPsetAttribute(IPset ifcObject) {
			var retVal = new List<PsetAttribute>();
			if (ifcObject != null) {
				foreach (var p in ifcObject.PropertySetList) {
					retVal.AddRange(p.Attributes);
				}
			}
			return retVal;
		}

		/// <summary>
		/// Saves a crud store for the business entity ReportSubscription.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<ReportSubscription> ReportSubscription_SaveStore(CrudStore<ReportSubscription> store) {
			ReportSubscriptionBroker broker = Helper.CreateInstance<ReportSubscriptionBroker>();
			return broker.SaveStore(store);
		}
		
		/// <summary>
		/// Creates a new business entity ReportSubscription and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse ReportSubscription_FormCreate(ReportSubscription item) {
			ReportSubscriptionBroker broker = Helper.CreateInstance<ReportSubscriptionBroker>();
			return broker.FormCreate(item);
		}

		/// <summary>
		/// Deletes an existing business entity ReportSubscription.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool ReportSubscription_Delete(ReportSubscription item) {
			ReportSubscriptionBroker broker = Helper.CreateInstance<ReportSubscriptionBroker>();
			return broker.Delete(item);
		}

		/// <summary>
		/// Reports the subscription_ get all by report.
		/// </summary>
		/// <param name="reportPath">The report path.</param>
		/// <param name="paging">The paging.</param>
		/// <returns></returns>
		public JsonStore<ReportSubscription> ReportSubscription_GetAllByReport(string reportPath, PagingParameter paging) {
			var broker = Helper.CreateInstance<ReportSubscriptionBroker>();
			
			int totalCount;
			var subscriptions = broker.GetAllByReportPath(reportPath, paging, out totalCount);
			var store = subscriptions.ToJsonStore(totalCount);

			return store;
		} 
	}
}

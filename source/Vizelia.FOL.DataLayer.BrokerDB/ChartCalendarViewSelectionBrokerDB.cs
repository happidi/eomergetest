﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Globalization;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.DataLayer.BrokerDB {

	/// <summary>
	/// Database broker for ChartCalendarViewSelection business entity.
	/// </summary>
	public class ChartCalendarViewSelectionBrokerDB : BaseBrokerDB<ChartCalendarViewSelection> {

		/// <summary>
		/// The name of the stored procedure that retreives a list of ChartCalendarViewSelection for a specific chart.
		/// </summary>
		protected const string const_proc_getlistbychart = AppModule.Energy + "_ChartCalendarViewSelectionGetListByKeyChart";

		/// <summary>
		/// The name of the stored procedure that retreives a list of ChartCustomGridCell for a specific chart.
		/// </summary>
		protected const string const_proc_getallpagingbykeychart = AppModule.Energy + "_ChartCalendarViewSelectionGetAllPagingByKeyChart";

		/// <summary>
		/// The prefix of stored procedures of that provider. 
		/// </summary>
		protected override string const_proc_prefix {
			get {
				return AppModule.Energy;
			}
		}

		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, ChartCalendarViewSelection item) {
			db.AddInParameterString(command, "Key", item.KeyChartCalendarViewSelection);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyChart", item.KeyChart);
			db.AddInParameterDateTime(command, "StartDate", item.StartDate);
			db.AddInParameterDateTime(command, "EndDate", item.EndDate);
		}

		/// <summary>
		/// Populates a ChartCalendarViewSelection business entity from a DataSourceDecorator.
		/// </summary>
		/// <param name="source">The data source.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated ChartCalendarViewSelection business entity.</returns>
		protected override ChartCalendarViewSelection FillObject(DataSourceDecorator source, string prefixColumn) {
			return new ChartCalendarViewSelection {
				KeyChartCalendarViewSelection = source.Field<int>(prefixColumn + "KeyChartCalendarViewSelection").ToString(CultureInfo.InvariantCulture),
				KeyChart = source.Field<int>(prefixColumn + "KeyChart").ToString(CultureInfo.InvariantCulture),
				EndDate = source.Field<DateTime>(prefixColumn + "EndDate"),
				StartDate = source.Field<DateTime>(prefixColumn + "StartDate")
			};
		}

		/// <summary>
		/// Get the list of saved ChartCalendarViewSelection from a Chart
		/// </summary>
		/// <param name="KeyChart">The key of the chart.</param>
		public List<ChartCalendarViewSelection> GetListByKeyChart(string KeyChart) {
			List<ChartCalendarViewSelection> results;
			DbCommand command = db.GetStoredProcCommand(const_proc_getlistbychart);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyChart", KeyChart);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}

		/// <summary>
		/// Gets all the ChartCalendarViewSelection for a specific chart.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyChart">The Key of the chart.</param>
		/// <param name="total">The total number of items.</param>
		/// <returns>The list of items.</returns>
		public List<ChartCalendarViewSelection> GetAllPagingByKeyChart(PagingParameter paging, string KeyChart, out int total) {
			DbCommand command = db.GetStoredProcCommand(const_proc_getallpagingbykeychart);
			command.CommandTimeout = 600;
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyChart", KeyChart);
			db.AddInParameterInt(command, "start", paging.start);
			db.AddInParameterInt(command, "limit", paging.limit);
			DataSet ds = db.ExecuteDataSet(command);
			if (ds.Tables[0].Rows.Count > 0)
				total = ds.Tables[0].Rows[0].Field<int>("TotalRecord");
			else
				total = 0;
			return FillObjects(ds);
		}
	}
}
﻿using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Globalization;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.DataLayer.BrokerDB {
	/// <summary>
	/// 
	/// </summary>
	public class AuditEntityBrokerDB : BaseBrokerDB<AuditEntity> {

		/// <summary>
		/// Stored procedure for getting aggregated audit entities.
		/// </summary>
		protected const string const_proc_getallaggregated = AppModule.Core + "_AuditEntityGetAllAggregated";

		/// <summary>
		/// Builds the save params.
		/// </summary>
		/// <param name="command">The command.</param>
		/// <param name="item">The item.</param>
		protected override void BuildSaveParams(DbCommand command, AuditEntity item) {
			db.AddInParameterString(command, "Key", item.KeyAuditEntity);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "EntityName", item.EntityName);
			db.AddInParameterBool(command, "IsAuditEnabled", item.IsAuditEnabled);
		}

		/// <summary>
		/// Fills the object.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="prefixColumn">The prefix column.</param>
		/// <returns></returns>
		protected override AuditEntity FillObject(DataSourceDecorator source, string prefixColumn) {
			return new AuditEntity {
				KeyAuditEntity = source.Field<int>(prefixColumn + "KeyAuditEntity").ToString(CultureInfo.InvariantCulture),
				Application = source.Field<string>(prefixColumn + "App"),
				EntityName = source.Field<string>(prefixColumn + "EntityName"),
				IsAuditEnabled = source.Field<bool>(prefixColumn + "IsAuditEnabled")
			};
		}

		/// <summary>
		/// Gets the audit entities aggregated.
		/// </summary>
		/// <returns></returns>
		public List<AuditEntity> GetAuditEntitiesAggregated() {
			var retVal = new List<AuditEntity>();
			DbCommand command = db.GetStoredProcCommand(const_proc_getallaggregated);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			using (IDataReader reader = db.ExecuteReader(command)) {
				while (reader.Read()) {
					retVal.Add(FillObject(new DataReaderDecorator(reader), ""));
				}
			}
			return retVal;
		}
	}
}

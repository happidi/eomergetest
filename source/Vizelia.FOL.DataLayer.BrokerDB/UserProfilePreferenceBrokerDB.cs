﻿using System.Collections.Generic;
using System.Data.Common;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.DataLayer.BrokerDB {
	/// <summary>
	/// The broker DB for business entity UserProfilePreference
	/// </summary>
	public class UserProfilePreferenceBrokerDB : BaseBrokerDB<UserProfilePreference> {
		/// <summary>
		/// The name of the stored procedure that gets a user profile preferences for a specific user.
		/// </summary>
		const string const_proc_getallbykeyuser = AppModule.Core + "_UserProfilePreferenceGetAllByKeyUser";

		/// <summary>
		/// The name of the stored procedure that deletes all prference for a specific user.
		/// </summary>
		const string const_proc_deletealldatabykeyuser = AppModule.Core + "_UserProfilePreferenceDeleteAllDataByKeyUser";

		/// <summary>
		/// Builds the save params.
		/// </summary>
		/// <param name="command">The command.</param>
		/// <param name="item">The item.</param>
		protected override void BuildSaveParams(DbCommand command, UserProfilePreference item) {
			db.AddInParameterString(command, "Key", item.KeyUserProfilePreference);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyUser", item.KeyUser);
			db.AddInParameterString(command, "PreferenceKey", item.PreferenceKey);
			db.AddInParameterString(command, "PreferenceValue", item.PreferenceValue);
		}

		/// <summary>
		/// Fills the object.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="prefixColumn">The prefix column.</param>
		/// <returns></returns>
		protected override UserProfilePreference FillObject(DataSourceDecorator source, string prefixColumn) {
			return new UserProfilePreference {
				KeyUserProfilePreference = source.Field<int>(prefixColumn + "KeyUserProfilePreference").ToString(),
				KeyUser = source.Field<string>(prefixColumn + "KeyUser"),
				PreferenceKey = source.Field<string>(prefixColumn + "PreferenceKey"),
				PreferenceValue = source.Field<string>(prefixColumn + "PreferenceValue")
			};
		}

		/// <summary>
		/// Gets the list by key user.
		/// </summary>
		/// <param name="KeyUser">The key user.</param>
		/// <returns></returns>
		public List<UserProfilePreference> GetListByKeyUser(string KeyUser) {
			DbCommand command = db.GetStoredProcCommand(const_proc_getallbykeyuser);
			command.CommandTimeout = 600;
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyUser", KeyUser);
			var ds = db.ExecuteDataSet(command);
			return FillObjects(ds);
		}

		/// <summary>
		/// Deletes all data by key user.
		/// </summary>
		/// <param name="KeyUser">The key user.</param>
		public bool DeleteAllDataByKeyUser(string KeyUser) {
			DbCommand command = db.GetStoredProcCommand(const_proc_deletealldatabykeyuser);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyUser", KeyUser);
			db.ExecuteNonQuery(command);
			return true;
		}
	}
}
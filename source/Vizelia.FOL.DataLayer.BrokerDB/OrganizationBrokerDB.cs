﻿using Vizelia.FOL.BusinessEntities;
using System.Data.Common;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.DataLayer.BrokerDB {
	/// <summary>
	/// Database broker for Organization business entity.
	/// </summary>
	public class OrganizationBrokerDB : BaseBrokerDB<Organization> {

		/// <summary>
		/// Initializes a new instance of the <see cref="OrganizationBrokerDB"/> class.
		/// </summary>
		public OrganizationBrokerDB() {
			IsHierarchical = true;
		}

		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, Organization item) {
			db.AddInParameterString(command, "Key", item.KeyOrganization);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "Id", item.Id);
			db.AddInParameterString(command, "Name", item.Name);
			db.AddInParameterString(command, "Description", item.Description);
			db.AddInParameterString(command, "KeyParent", item.KeyParent);
			db.AddInParameterBool(command, "IsMappingInProgress", IsAsynchronousSave);
		}

		/// <summary>
		/// Populates a Organization business entity from a DataSourceDecorator.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated Organization business entity.</returns>
		protected override Organization FillObject(DataSourceDecorator source, string prefixColumn) {
			var retVal = new Organization {
				KeyOrganization = source.Field<string>(prefixColumn + "KeyOrganization"),
				Id = source.Field<string>(prefixColumn + "Id"),
				Name = source.Field<string>(prefixColumn + "Name"),
				Description = source.Field<string>(prefixColumn + "Description"),
				KeyParent = source.Field<string>(prefixColumn + "KeyParent")
			};

			if (source.FieldExists(prefixColumn + "IdPath")) {
				retVal.IdPath = source.Field<string>(prefixColumn + "IdPath");
			}
			if (source.FieldExists(prefixColumn + "LongPath")) {
				retVal.LongPath = source.Field<string>(prefixColumn + "LongPath");
			}
			if (source.FieldExists(prefixColumn + "Level")) {
				retVal.Level = source.Field<int>(prefixColumn + "Level");
			}

			return retVal;
		}


		/// <summary>
		/// Builds the additional delete params.
		/// </summary>
		/// <param name="command">The command.</param>
		protected override void BuildAdditionalDeleteParams(DbCommand command) {
			db.AddInParameterBool(command, "IsMappingInProgress", IsAsynchronousSave);
		}

	}
}

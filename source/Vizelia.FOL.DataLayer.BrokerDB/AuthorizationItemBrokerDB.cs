﻿using System;
using System.Data;
using System.Data.Common;
using System.Linq;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.DataLayer.BrokerDB {
	/// <summary>
	/// Database broker for AuthorizationItem business entity.
	/// Created only for mapping process, not functional.
	/// </summary>
	public class AuthorizationItemBrokerDB : BaseBrokerDB<AuthorizationItem> {
		/// <summary>
		/// The name of the stored procedure that rsave store of AzMan role for a specific Chart KPI.
		/// </summary>
		protected const string const_proc_savechartfilterazmanrole = AppModule.Energy + "_ChartFilterAzManRoleSave";

		/// <summary>
		/// Builds the save params.
		/// </summary>
		/// <param name="command">The command.</param>
		/// <param name="item">The item.</param>
		protected override void BuildSaveParams(DbCommand command, AuthorizationItem item) {
			throw new NotImplementedException();
		}

		/// <summary>
		/// Fills the object.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="prefixColumn">The prefix column.</param>
		/// <returns></returns>
		protected override AuthorizationItem FillObject(DataSourceDecorator source, string prefixColumn) {
			throw new NotImplementedException();
		}

		/// <summary>
		/// Saves the chart azman roles  filter.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="filterAzManRoles">The filter KPI az man roles.</param>
		public void SaveChartFilterAzManRole(Chart entity, CrudStore<AuthorizationItem> filterAzManRoles) {
			DbCommand command = db.GetStoredProcCommand(const_proc_savechartfilterazmanrole);
			command.CommandTimeout = 600;

			if (filterAzManRoles == null)
				return;
			string inserts = null;
			string deletes = null;

			if (filterAzManRoles.create != null) {
				inserts = Helper.BuildXml(filterAzManRoles.create.Select(p => p.Id).ToArray());
			}

			if (filterAzManRoles.destroy != null) {
				deletes = Helper.BuildXml(filterAzManRoles.destroy.Select(p => p.Id).ToArray());
			}

			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyChart", entity.KeyChart);
			db.AddInParameterString(command, "Inserts", inserts);
			db.AddInParameterString(command, "Deletes", deletes);


			using (IDataReader reader = db.ExecuteReader(command)) {
				//results = FillObjects(reader);
			}
		}
	}
}

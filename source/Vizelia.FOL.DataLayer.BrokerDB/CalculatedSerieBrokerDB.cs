﻿using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.DataLayer.BrokerDB {

	/// <summary>
	/// Database broker for DataSerie business entity.
	/// </summary>
	public class CalculatedSerieBrokerDB : BaseBrokerDB<CalculatedSerie> {

		/// <summary>
		/// The name of the stored procedure that retreives a list of dataseries for a specific chart.
		/// </summary>
		protected const string const_proc_getlistbychart = AppModule.Energy + "_CalculatedSerieGetListByChart";

		/// <summary>
		/// The name of the stored procedure that retreives a list of dataseries for a specific chart.
		/// </summary>
		protected const string const_proc_getallpagingbykeychart = AppModule.Energy + "_CalculatedSerieGetAllPagingByKeyChart";

		/// <summary>
		/// The name of the stored procedure that saves a list of dataseries for a specific chart.
		/// </summary>
		protected const string const_proc_chartdataseriessave = AppModule.Energy + "_ChartCalculatedSeriesSave";

		/// <summary>
		/// Gets the const_proc_prefix.
		/// </summary>
		/// <value>The const_proc_prefix.</value>
		protected override string const_proc_prefix {
			get {
				return AppModule.Energy;
			}
		}

		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, CalculatedSerie item) {
			db.AddInParameterString(command, "Key", item.KeyCalculatedSerie);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "LocalId", item.LocalId);
			db.AddInParameterString(command, "KeyChart", item.KeyChart);
			db.AddInParameterString(command, "Name", item.Name);
			db.AddInParameterString(command, "Formula", item.Formula);
			db.AddInParameterInt(command, "Order", item.Order);
			db.AddInParameterBool(command, "HideUsedSeries", item.HideUsedSeries);
			db.AddInParameterString(command, "Unit", item.Unit);
			db.AddInParameterString(command, "KeyLocalizationCulture", item.KeyLocalizationCulture);
			db.AddInParameterString(command, "KeyClassificationItem", item.KeyClassificationItem);
			db.AddInParameterBool(command, "RunOnlyInMainTimeline", item.RunOnlyInMainTimeline);
		}

		/// <summary>
		/// Populates a CalculatedSerie business entity from a DataSourceDecorator.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated CalculatedSerie business entity.</returns>
		protected override CalculatedSerie FillObject(DataSourceDecorator source, string prefixColumn) {
			return new CalculatedSerie() {
				KeyCalculatedSerie = source.Field<int>(prefixColumn + "KeyCalculatedSerie").ToString(CultureInfo.InvariantCulture),
				LocalId = source.Field<string>(prefixColumn + "LocalId"),
				KeyChart = source.Field<int>(prefixColumn + "KeyChart").ToString(CultureInfo.InvariantCulture),
				Name = source.Field<string>(prefixColumn + "Name"),
				Unit = source.Field<string>(prefixColumn + "Unit"),
				Formula = source.Field<string>(prefixColumn + "Formula"),
				Order = source.Field<int>(prefixColumn + "Order"),
				HideUsedSeries = source.Field<bool>(prefixColumn + "HideUsedSeries"),
				KeyLocalizationCulture = source.Field<string>(prefixColumn + "KeyLocalizationCulture"),
				KeyClassificationItem = source.Field<string>(prefixColumn + "KeyClassificationItem"),
				KeyClassificationItemPath = source.Field<string>(prefixColumn + "KeyClassificationItemPath"),
				ClassificationItemLongPath = source.Field<string>(prefixColumn + "ClassificationItemLongPath"),
				ClassificationItemLocalIdPath = source.Field<string>(prefixColumn + "ClassificationItemLocalIdPath"),
				ClassificationItemTitle = source.Field<string>(prefixColumn + "ClassificationItemTitle"),
				RunOnlyInMainTimeline = source.Field<bool?>(prefixColumn + "RunOnlyInMainTimeline") ?? false
			};
		}

		/// <summary>
		/// Get the list of saved CalculatedSerie from a Chart
		/// </summary>
		/// <param name="KeyChart">The key of the chart.</param>
		public List<CalculatedSerie> GetListByKeyChart(string KeyChart) {
			List<CalculatedSerie> results;
			DbCommand command = db.GetStoredProcCommand(const_proc_getlistbychart);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyChart", KeyChart);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}

		/// <summary>
		/// Gets all the CalculatedSerie for a specific chart.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyChart">The Key of the chart.</param>
		/// <param name="total">The total number of items.</param>
		/// <returns>The list of items.</returns>
		public List<CalculatedSerie> GetAllPagingByKeyChart(PagingParameter paging, string KeyChart, out int total) {
			DbCommand command = db.GetStoredProcCommand(const_proc_getallpagingbykeychart);
			command.CommandTimeout = 600;
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyChart", KeyChart);
			db.AddInParameterInt(command, "start", paging.start);
			db.AddInParameterInt(command, "limit", paging.limit);
			DataSet ds = db.ExecuteDataSet(command);
			if (ds.Tables[0].Rows.Count > 0)
				total = ds.Tables[0].Rows[0].Field<int>("TotalRecord");
			else
				total = 0;
			return FillObjects(ds);
		}

		/// <summary>
		/// Saves the chart CalculatedSerie.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="calculatedseries">The calculatedseries.</param>
		/// <returns></returns>
		public List<CalculatedSerie> SaveChartCalculatedSeries(Chart entity, CrudStore<CalculatedSerie> calculatedseries) {
			DbCommand command = db.GetStoredProcCommand(const_proc_chartdataseriessave);
			command.CommandTimeout = 600;
			List<CalculatedSerie> results;

			if (calculatedseries == null || calculatedseries.IsEmpty())
				return null;
			string deletes = null;

			if (calculatedseries.destroy != null) {
				deletes = Helper.BuildXml(calculatedseries.destroy.Select(p => p.KeyCalculatedSerie).ToArray());
			}

			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyChart", entity.KeyChart);
			db.AddInParameterString(command, "Inserts", null);
			db.AddInParameterString(command, "Deletes", deletes);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}
	}
}
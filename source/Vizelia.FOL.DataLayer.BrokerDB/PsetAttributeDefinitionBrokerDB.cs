﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web.Security;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.DataLayer.BrokerDB {

	/// <summary>
	/// Database broker for PsetDefinition business entity.
	/// </summary>
	public class PsetAttributeDefinitionBrokerDB : BaseBrokerDB<PsetAttributeDefinition> {

		const string const_proc_getattributes = AppModule.Core + "_PsetAttributeDefinitionGetAttributesFromPsetDefinitonList";
		const string const_proc_getlistfromparents = AppModule.Core + "_PsetAttributeDefinitionGetListFromParents";
		const string const_proc_getlistherited = AppModule.Core + "_PsetAttributeDefinitionGetListHerited";

		/// <summary>
		/// The name of the stored procedure that retreives an item by its name.
		/// </summary>
		const string const_proc_getitembyname = AppModule.Core + "_PsetAttributeDefinitionGetItemByName";

		/// <summary>
		/// Populates a PsetAttributeDefinition business entity from a DataSourceDecorator.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated PsetAttributeDefinition business entity.</returns>
		protected override PsetAttributeDefinition FillObject(DataSourceDecorator source, string prefixColumn) {
			return new PsetAttributeDefinition {
				KeyPropertySingleValue = source.Field<string>(prefixColumn + "KeyPropertySingleValue"),
				KeyPropertySet = source.Field<string>(prefixColumn + "KeyPropertySet"),
				LocalId = source.Field<string>(prefixColumn + "LocalId"),
				AttributeName = source.Field<string>(prefixColumn + "AttributeName"),
				AttributeDescription = source.Field<string>(prefixColumn + "AttributeDescription"),
				AttributeOrder = source.Field<int>(prefixColumn + "AttributeOrder"),
				AttributeDataType = source.Field<string>(prefixColumn + "AttributeDataType"),
				AttributeDefaultValue = source.Field<string>(prefixColumn + "AttributeDefaultValue"),
				AttributeComponent = source.Field<string>(prefixColumn + "AttributeComponent"),
				AttributeMaxValue = source.Field<string>(prefixColumn + "AttributeMaxValue"),
				AttributeMinValue = source.Field<string>(prefixColumn + "AttributeMinValue"),
				AttributeMask = source.Field<string>(prefixColumn + "AttributeMask"),
				AttributeVType = source.Field<string>(prefixColumn + "AttributeVType"),
				AttributeList = source.Field<string>(prefixColumn + "AttributeList"),
				AttributeAllowBlank = source.Field<bool?>(prefixColumn + "AttributeAllowBlank"),
				AttributeMsgCode = source.Field<string>(prefixColumn + "AttributeMsgCode"),
				AttributeWidth = source.Field<int?>(prefixColumn + "AttributeWidth"),
				AttributeHasTab = source.Field<bool?>(prefixColumn + "AttributeHasTab")
			};
		}

		

		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, PsetAttributeDefinition item) {
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "Key", item.KeyPropertySingleValue);
			db.AddInParameterString(command, "KeyPropertySet", item.KeyPropertySet);
			db.AddInParameterString(command, "AttributeName", item.AttributeName);
			db.AddInParameterString(command, "AttributeDescription", item.AttributeDescription);
			db.AddInParameterString(command, "AttributeDefaultValue", item.AttributeDefaultValue);
			db.AddInParameterInt(command, "AttributeOrder", item.AttributeOrder);
			db.AddInParameterString(command, "AttributeDataType", item.AttributeDataType);
			db.AddInParameterString(command, "AttributeComponent", item.AttributeComponent);
			db.AddInParameterString(command, "AttributeMaxValue", item.AttributeMaxValue);
			db.AddInParameterString(command, "AttributeMinValue", item.AttributeMinValue);
			db.AddInParameterString(command, "AttributeMask", item.AttributeMask);
			db.AddInParameterString(command, "AttributeVType", item.AttributeVType);
			db.AddInParameterString(command, "AttributeList", item.AttributeList);
			db.AddInParameterString(command, "AttributeMsgCode", item.AttributeMsgCode);
			db.AddInParameterBool(command, "AttributeAllowBlank", item.AttributeAllowBlank);
			db.AddInParameterInt(command, "AttributeWidth", item.AttributeWidth);
			db.AddInParameterBool(command, "AttributeHasTab", item.AttributeHasTab);
		}


		/// <summary>
		/// Gets the attributes definition from a pset definition.
		/// </summary>
		/// <param name="Keys">The list of keys PsetDefinition.</param>
		/// <returns></returns>
		public List<PsetAttributeDefinition> GetAttributesFromPsetDefinitionList(string[] Keys) {
			return this.GetList(Keys, const_proc_getattributes);
		}


		/// <summary>
		/// Gets the list from parents PropertySet keys.
		/// </summary>
		/// <param name="Keys">The parent PropertySet keys.</param>
		/// <returns></returns>
		public List<PsetAttributeDefinition> GetListFromParents(string[] Keys) {
			return this.GetList(Keys, const_proc_getlistfromparents);
		}

		/// <summary>
		/// Gets the list of pset herited from a ClassificationItem.
		/// </summary>
		/// <param name="UsageName">Name of the usage.</param>
		/// <param name="KeyClassificationItem">The key classification item.</param>
		/// <returns></returns>
		public List<PsetAttributeDefinition> GetListHerited(string UsageName, string KeyClassificationItem) {
			List<PsetAttributeDefinition> results;
			DbCommand command = db.GetStoredProcCommand(const_proc_getlistherited);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "UsageName", UsageName);
			db.AddInParameterString(command, "KeyClassificationItem", KeyClassificationItem);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}

		/// <summary>
		/// Gets the item by local id.
		/// The override is necessary because of .read() in FillObjects that is redondant with .read() in sub method GetItemByLocalId.
		/// </summary>
		/// <param name="localId">The local id.</param>
		/// <returns></returns>
		public override PsetAttributeDefinition GetItemByLocalId(string localId) {
			PsetAttributeDefinition item = null;
			DbCommand command = db.GetStoredProcCommand(const_proc_getitembyname);
			var parameters = localId.Split(new[] {"PsetName=", "PsetAttributeName="}, StringSplitOptions.RemoveEmptyEntries);
			var psetName = parameters[0];
			var psetAttributeName = parameters[1];
			db.AddInParameterString(command, "PsetName", psetName);
			db.AddInParameterString(command, "PsetAttributeName", psetAttributeName);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			using (IDataReader reader = db.ExecuteReader(command)) {
				if (reader.Read()) {
					item = FillObject(reader);
				}
			}
			return item;
		}
	}
}


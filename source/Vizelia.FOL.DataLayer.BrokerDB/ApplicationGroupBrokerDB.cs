﻿using System;
using System.Data.Common;
using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.DataLayer.BrokerDB {
	/// <summary>
	/// Database broker for ApplicationGroup business entity.
	/// Created only for mapping process, not functional.
	/// </summary>
	public class ApplicationGroupBrokerDB : BaseBrokerDB<ApplicationGroup> {
		/// <summary>
		/// Builds the save params.
		/// </summary>
		/// <param name="command">The command.</param>
		/// <param name="item">The item.</param>
		protected override void BuildSaveParams(DbCommand command, ApplicationGroup item) {
			throw new NotImplementedException();
		}

		/// <summary>
		/// Fills the object.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="prefixColumn">The prefix column.</param>
		/// <returns></returns>
		protected override ApplicationGroup FillObject(DataSourceDecorator source, string prefixColumn) {
			throw new NotImplementedException();
		}
	}
}


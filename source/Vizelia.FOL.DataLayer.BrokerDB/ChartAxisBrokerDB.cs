﻿using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.DataLayer.BrokerDB {

	/// <summary>
	/// Database broker for ChartAxis business entity.
	/// </summary>
	public class ChartAxisBrokerDB : BaseBrokerDB<ChartAxis> {


		/// <summary>
		/// The name of the stored procedure that retreives a list of ChartAxis for a specific chart.
		/// </summary>
		protected const string const_proc_getlistbychart = AppModule.Energy + "_ChartAxisGetListByKeyChart";


		/// <summary>
		/// The name of the stored procedure that retreives a list of ChartAxis for a specific chart.
		/// </summary>
		protected const string const_proc_getallpagingbykeychart = AppModule.Energy + "_ChartAxisGetAllPagingByKeyChart";


		/// <summary>
		/// The name of the stored procedure that saves a list of ChartAxis for a specific chart.
		/// </summary>
		protected const string const_proc_chartaxissave = AppModule.Energy + "_ChartChartAxisSave";


		/// <summary>
		/// Gets the const_proc_prefix.
		/// </summary>
		/// <value>The const_proc_prefix.</value>
		protected override string const_proc_prefix {
			get {
				return AppModule.Energy;
			}
		}

		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, ChartAxis item) {
			db.AddInParameterString(command, "Key", item.KeyChartAxis);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "LocalId", item.LocalId);
			db.AddInParameterString(command, "KeyChart", item.KeyChart);
			db.AddInParameterString(command, "Name", item.Name);
			db.AddInParameterDouble(command, "Min", item.Min);
			db.AddInParameterDouble(command, "Max", item.Max);
			db.AddInParameterBool(command, "IsPrimary", item.IsPrimary);
			db.AddInParameterBool(command, "FullStacked", item.FullStacked);
		}

		/// <summary>
		/// Populates a ChartAxis business entity from a DataSourceDecorator.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated ChartAxis business entity.</returns>
		protected override ChartAxis FillObject(DataSourceDecorator source, string prefixColumn) {
			return new ChartAxis {
				KeyChartAxis = source.Field<int>(prefixColumn + "KeyChartAxis").ToString(CultureInfo.InvariantCulture),
				KeyChart = source.Field<int>(prefixColumn + "KeyChart").ToString(CultureInfo.InvariantCulture),
				LocalId = source.Field<string>(prefixColumn + "LocalId"),
				Name = source.Field<string>(prefixColumn + "Name"),
				Min = source.Field<double?>(prefixColumn + "Min"),
				Max = source.Field<double?>(prefixColumn + "Max"),
				IsPrimary = source.Field<bool>(prefixColumn + "IsPrimary"),
				FullStacked = source.Field<bool?>(prefixColumn + "FullStacked") ?? false
			};
		}


		/// <summary>
		/// Get the list of saved ChartAxis from a Chart
		/// </summary>
		/// <param name="KeyChart">The key of the chart.</param>
		public List<ChartAxis> GetListByKeyChart(string KeyChart) {
			List<ChartAxis> results;
			DbCommand command = db.GetStoredProcCommand(const_proc_getlistbychart);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyChart", KeyChart);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}

		/// <summary>
		/// Gets all the ChartAxis for a specific chart.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyChart">The Key of the chart.</param>
		/// <param name="total">The total number of items.</param>
		/// <returns>The list of items.</returns>
		public List<ChartAxis> GetAllPagingByKeyChart(PagingParameter paging, string KeyChart, out int total) {
			DbCommand command = db.GetStoredProcCommand(const_proc_getallpagingbykeychart);
			command.CommandTimeout = 600;
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyChart", KeyChart);
			db.AddInParameterInt(command, "start", paging.start);
			db.AddInParameterInt(command, "limit", paging.limit);
			DataSet ds = db.ExecuteDataSet(command);
			if (ds.Tables[0].Rows.Count > 0)
				total = ds.Tables[0].Rows[0].Field<int>("TotalRecord");
			else
				total = 0;
			return FillObjects(ds);
		}


		/// <summary>
		/// Saves the chart ChartAxis.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="chartaxis">The ChartAxis store.</param>
		/// <returns></returns>
		public List<ChartAxis> SaveChartChartAxis(Chart entity, CrudStore<ChartAxis> chartaxis) {
			DbCommand command = db.GetStoredProcCommand(const_proc_chartaxissave);
			command.CommandTimeout = 600;
			List<ChartAxis> results;

			if (chartaxis == null || chartaxis.IsEmpty())
				return null;
			string inserts = null;
			string deletes = null;

			/*if (dataseries.create != null) {
				inserts = Helper.BuildXml(dataseries.create.Select(p => p.KeyDataSerie).ToArray());
			}*/

			if (chartaxis.destroy != null) {
				deletes = Helper.BuildXml(chartaxis.destroy.Select(p => p.KeyChartAxis).ToArray());
			}

			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyChart", entity.KeyChart);
			db.AddInParameterString(command, "Inserts", inserts);
			db.AddInParameterString(command, "Deletes", deletes);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}
	}
}
﻿
namespace Vizelia.FOL.DataLayer.BrokerDB {

	/// <summary>
	/// Database broker for Login history entity.
	/// </summary>
	public class LoginHistoryActiveBrokerDB : LoginHistoryBrokerDB {

		/// <summary>
		/// Gets the name of the business entity.
		/// </summary>
		/// <value>
		/// The name of the business entity.
		/// </value>
		protected override string BusinessEntityName {
			get {
				return "LoginHistoryActive";
			}
		}

	}
}
﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Web.Security;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Vizelia.FOL.DataLayer.Interfaces;

namespace Vizelia.FOL.DataLayer.BrokerDB {

	/// <summary>
	/// Database broker for FlipCard business entity.
	/// </summary>
	public class FlipCardBrokerDB : BaseBrokerDB<FlipCard> {
		/// <summary>
		/// The prefix of stored procedures of that provider. 
		/// </summary>
		protected override string const_proc_prefix {
			get {
				return AppModule.Energy;
			}
		}


		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, FlipCard item) {
			db.AddInParameterString(command, "Key", item.KeyFlipCard);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "LocalId", item.LocalId);
			db.AddInParameterString(command, "Title", item.Title);
			db.AddInParameterInt(command, "ActiveCard", item.ActiveCard);
			db.AddInParameterBool(command, "FlipOnRefresh", item.FlipOnRefresh);
			db.AddInParameterBool(command, "Random", item.Random);
			db.AddInParameterBool(command, "OpenOnStartup", item.OpenOnStartup);
			db.AddInParameterBool(command, "IsFavorite", item.IsFavorite);
		}


		/// <summary>
		/// Populates a FlipCard business entity from a DataSourceDecorator.
		/// </summary>
		/// <param name="source">The data source.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated FlipCard business entity.</returns>
		protected override FlipCard FillObject(DataSourceDecorator source, string prefixColumn) {
			return new FlipCard {
				KeyFlipCard = source.Field<int>(prefixColumn + "KeyFlipCard").ToString(),
				LocalId = source.Field<string>(prefixColumn + "LocalId"),
				Title = source.Field<string>(prefixColumn + "Title"),
				ActiveCard = source.Field<int?>(prefixColumn + "ActiveCard")??0,
				FlipOnRefresh = source.Field<bool>(prefixColumn + "FlipOnRefresh"),
				Random = source.Field<bool>(prefixColumn + "Random"),
				OpenOnStartup = source.Field<bool?>(prefixColumn + "OpenOnStartup") ?? false,
				IsFavorite = source.Field<bool?>(prefixColumn + "IsFavorite") ?? false
			};
		}
	}
}
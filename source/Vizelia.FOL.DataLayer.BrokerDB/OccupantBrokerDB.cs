﻿using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using System.Data;
using System.Data.Common;

namespace Vizelia.FOL.DataLayer.BrokerDB {
	/// <summary>
	/// Database broker for Occupant business entity.
	/// </summary>
	public class OccupantBrokerDB : BaseBrokerDB<Occupant> {
		/// <summary>
		/// The name of the stored procedure that retreives an Occupant from an Actor.
		/// </summary>
		const string const_proc_getitembykeyactor = AppModule.Core + "_OccupantGetItemByKeyActor";
		

		/// <summary>
		/// Special FillObject used from the FOLMembershipProvider.
		/// </summary>
		/// <param name="reader">The reader.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database</param>
		/// <returns>The populated Occupant business entity.</returns>
		public Occupant FillObjectFromUser(IDataReader reader, string prefixColumn) {
			DataSourceDecorator source = new DataReaderDecorator(reader);
			return this.FillObject(source, prefixColumn);
		}

		/// <summary>
		/// Fills the object from user.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="prefixColumn">The prefix column.</param>
		/// <returns></returns>
		public Occupant FillObjectFromUser(DataSourceDecorator source, string prefixColumn) {
			return this.FillObject(source, prefixColumn);
		}

		/// <summary>
		/// Populates a Occupant business entity from a DataSourceDecorator.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated Occupant business entity.</returns>
		protected override Occupant FillObject(DataSourceDecorator source, string prefixColumn) {
			return new Occupant {
				KeyOccupant = source.Field<string>(prefixColumn + "KeyOccupant"),
				Id = source.Field<string>(prefixColumn + "Id"),
				FirstName = source.Field<string>(prefixColumn + "FirstName"),
				LastName = source.Field<string>(prefixColumn + "LastName"),
				JobTitle = source.Field<string>(prefixColumn + "JobTitle"),
				TelephoneNumbers = source.Field<string>(prefixColumn + "TelephoneNumbers"),
				FacsimileNumbers = source.Field<string>(prefixColumn + "FacsimileNumbers"),
				PagerNumber = source.Field<string>(prefixColumn + "PagerNumber"),

				KeyLocation = source.Field<string>(prefixColumn + "KeyLocation"),
				LocationName = source.Field<string>(prefixColumn + "LocationName"),
				LocationLevel = source.Field<int?>(prefixColumn + "LocationLevel"),
				LocationLocalIdPath = source.Field<string>(prefixColumn + "LocationLocalIdPath"),
				LocationLongPath = source.Field<string>(prefixColumn + "LocationLongPath"),
				LocationShortPath = source.Field<string>(prefixColumn + "LocationShortPath"),
				LocationTypeName = source.Field<string>(prefixColumn + "LocationTypeName").ParseAsEnum<HierarchySpatialTypeName>(),

				KeyOrganization = source.Field<string>(prefixColumn + "KeyOrganization"),
				OrganizationId = source.Field<string>(prefixColumn + "OrganizationId"),
				OrganizationName = source.Field<string>(prefixColumn + "OrganizationName"),
				OrganizationIdPath = source.Field<string>(prefixColumn + "OrganizationIdPath"),
				OrganizationLongPath = source.Field<string>(prefixColumn + "OrganizationLongPath"),

				KeyClassificationItemOrganization = source.Field<string>(prefixColumn + "KeyClassificationItemOrganization"),
				ClassificationItemOrganizationTitle = source.Field<string>(prefixColumn + "ClassificationItemOrganizationTitle"),
				ClassificationItemOrganizationLocalIdPath = source.Field<string>(prefixColumn + "ClassificationItemOrganizationLocalIdPath"),
				ClassificationItemOrganizationLongPath = source.Field<string>(prefixColumn + "ClassificationItemOrganizationLongPath"),

				KeyClassificationItemWorktype = source.Field<string>(prefixColumn + "KeyClassificationItemWorktype"),
				ClassificationItemWorktypeTitle = source.Field<string>(prefixColumn + "ClassificationItemWorktypeTitle"),
				ClassificationItemWorktypeLocalIdPath = source.Field<string>(prefixColumn + "ClassificationItemWorktypeLocalIdPath"),
				ClassificationItemWorktypeLongPath = source.Field<string>(prefixColumn + "ClassificationItemWorktypeLongPath")
			};
		}

		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, Occupant item) {
			db.AddInParameterString(command, "Key", item.KeyOccupant);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "Id", item.Id);
			db.AddInParameterString(command, "FirstName", item.FirstName);
			db.AddInParameterString(command, "LastName", item.LastName);
			db.AddInParameterString(command, "JobTitle", item.JobTitle);
			db.AddInParameterString(command, "TelephoneNumbers", item.TelephoneNumbers);
			db.AddInParameterString(command, "FacsimileNumbers", item.FacsimileNumbers);
			db.AddInParameterString(command, "PagerNumber", item.PagerNumber);
			db.AddInParameterString(command, "ElectronicMailAddresses", null);
			db.AddInParameterString(command, "KeyOrganization", item.KeyOrganization);
			db.AddInParameterString(command, "KeyLocation", item.KeyLocation);
			db.AddInParameterString(command, "KeyClassificationItemOrganization", item.KeyClassificationItemOrganization);
			db.AddInParameterString(command, "KeyClassificationItemWorktype", item.KeyClassificationItemWorktype);
		}

		/// <summary>
		/// Gets a value indicating whether the broker should use cache.
		/// </summary>
		/// <value><c>true</c> if use cache; otherwise, <c>false</c>.</value>
		public override bool UseCache {
			get {
				return false;
			}
		}

		/// <summary>
		/// Gets an item by its Actor.
		/// </summary>
		/// <param name="KeyActor">The key of the Actor.</param>
		/// <returns>The item.</returns>
		public Occupant GetItemByKeyActor(string KeyActor) {
			Occupant item = default(Occupant);
			DbCommand command = db.GetStoredProcCommand(const_proc_getitembykeyactor);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyActor", KeyActor);
			using (var decorator = new DataReaderDecorator(db.ExecuteReader(command))) {
				while (decorator.Read()) {
					item =  FillObject(decorator , "");
				}
			}
			return item;
		}


		/// <summary>
		/// Determines whether the current user has the needed filters to see and modify the given object.
		/// </summary>
		/// <param name="item">The item to check on.</param>
		/// <returns>
		///   <c>true</c> if the current user has the needed filters to see and modify the given object; otherwise, <c>false</c>.
		/// </returns>
		protected override bool IsUserAllowed(Occupant item) {
			// An Occupant item may be saved by the RSPMembershipProvider, which works during the login phase, so there is no current user.
			if (ContextHelper.Get(ContextKey.const_bypass_security) != null) return true;

			return base.IsUserAllowed(item);
		}
	}
}

﻿using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.DataLayer.BrokerDB {

	/// <summary>
	/// Database broker for FlipCardEntry business entity.
	/// </summary>
	public class FlipCardEntryBrokerDB : BaseBrokerDB<FlipCardEntry> {

		/// <summary>
		/// The name of the stored procedure that retreives a list of FlipCardEntry for a specific FlipCard.
		/// </summary>
		protected const string const_proc_getlistbyflipcard = AppModule.Energy + "_FlipCardEntryGetListByKeyFlipCard";

		/// <summary>
		/// The name of the stored procedure that retreives a list of FlipCardEntry for a specific FlipCard.
		/// </summary>
		protected const string const_proc_getallpagingbykeyflipcard = AppModule.Energy + "_FlipCardEntryGetAllPagingByKeyFlipCard";

		/// <summary>
		/// The name of the stored procedure that saves a list of FlipCardEntry for a specific FlipCard.
		/// </summary>
		protected const string const_proc_flipcardentrysave = AppModule.Energy + "_FlipCardFlipCardEntrySave";

		/// <summary>
		/// The prefix of stored procedures of that provider. 
		/// </summary>
		protected override string const_proc_prefix {
			get {
				return AppModule.Energy;
			}
		}

		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, FlipCardEntry item) {
			db.AddInParameterString(command, "Key", item.KeyFlipCardEntry);
			db.AddInParameterString(command, "LocalId", item.LocalId);
			db.AddInParameterString(command, "KeyFlipCard", item.KeyFlipCard);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterInt(command, "Order", item.Order);
			db.AddInParameterString(command, "KeyEntity", item.KeyEntity);
			db.AddInParameterString(command, "TypeEntity", item.TypeEntity);
		}

		/// <summary>
		/// Populates a FlipCardEntry business entity from a DataSourceDecorator.
		/// </summary>
		/// <param name="source">The data source.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated FlipCardEntry business entity.</returns>
		protected override FlipCardEntry FillObject(DataSourceDecorator source, string prefixColumn) {
			return new FlipCardEntry {
				KeyFlipCardEntry = source.Field<int>(prefixColumn + "KeyFlipCardEntry").ToString(CultureInfo.InvariantCulture),
				LocalId = source.Field<string>(prefixColumn + "LocalId"),
				KeyFlipCard = source.Field<int>(prefixColumn + "KeyFlipCard").ToString(CultureInfo.InvariantCulture),
				KeyEntity = source.Field<string>(prefixColumn + "KeyEntity"),
				TypeEntity = source.Field<string>(prefixColumn + "TypeEntity"),
				TitleEntity = source.Field<string>(prefixColumn + "TitleEntity"),
				Order = source.Field<int?>(prefixColumn + "Order") ?? 0
			};
		}

		/// <summary>
		/// Get the list of saved FlipCardEntry from a FlipCard.
		/// </summary>
		/// <param name="KeyFlipCard">The key of the portal window.</param>
		public List<FlipCardEntry> GetListByKeyFlipCard(string KeyFlipCard) {
			List<FlipCardEntry> results;
			DbCommand command = db.GetStoredProcCommand(const_proc_getlistbyflipcard);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyFlipCard", KeyFlipCard);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}

		/// <summary>
		/// Gets all the FlipCardEntry for a specific Portal.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyFlipCard">The Key of the portal window.</param>
		/// <param name="total">The total number of items.</param>
		/// <returns>The list of items.</returns>
		public List<FlipCardEntry> GetAllPagingByKeyFlipCard(PagingParameter paging, string KeyFlipCard, out int total) {
			DbCommand command = db.GetStoredProcCommand(const_proc_getallpagingbykeyflipcard);
			command.CommandTimeout = 600;
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyFlipCard", KeyFlipCard);
			db.AddInParameterInt(command, "start", paging.start);
			db.AddInParameterInt(command, "limit", paging.limit);
			DataSet ds = db.ExecuteDataSet(command);
			if (ds.Tables[0].Rows.Count > 0)
				total = ds.Tables[0].Rows[0].Field<int>("TotalRecord");
			else
				total = 0;
			return FillObjects(ds);
		}

		/// <summary>
		/// Saves the portal FlipCardEntrys.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="FlipCardEntry">The FlipCardEntry store.</param>
		/// <returns></returns>
		public List<FlipCardEntry> SaveFlipCardFlipCardEntry(FlipCard entity, CrudStore<FlipCardEntry> FlipCardEntry) {
			DbCommand command = db.GetStoredProcCommand(const_proc_flipcardentrysave);
			command.CommandTimeout = 600;
			List<FlipCardEntry> results;

			if (FlipCardEntry == null || FlipCardEntry.IsEmpty())
				return null;
			string deletes = null;


			if (FlipCardEntry.destroy != null) {
				deletes = Helper.BuildXml(FlipCardEntry.destroy.Select(p => p.KeyFlipCardEntry).ToArray());
			}

			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyFlipCard", entity.KeyFlipCard);
			db.AddInParameterString(command, "Inserts", null);
			db.AddInParameterString(command, "Deletes", deletes);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}
	}
}
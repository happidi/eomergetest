﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Web.Security;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Localization;

namespace Vizelia.FOL.DataLayer.BrokerDB {

	/// <summary>
	/// Database broker for TraceSummary business entity.
	/// </summary>
	public class TraceSummaryBrokerDB : BaseBrokerDB<TraceSummary> {

		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, TraceSummary item) {
			throw new NotSupportedException();
		}
		
		/// <summary>
		/// Populates a TraceEntry business entity from a DataSourceDecorator.
		/// </summary>
		/// <param name="source">The data source.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated TraceEntry business entity.</returns>
		protected override TraceSummary FillObject(DataSourceDecorator source, string prefixColumn) {
			return new TraceSummary {
				LastHour = source.Field<int>(prefixColumn + "LastHour"),
				LastDay = source.Field<int>(prefixColumn + "LastDay"),
				LastWeek = source.Field<int>(prefixColumn + "LastWeek"),
				Severity = source.Field<string>(prefixColumn + "Severity").ParseAsEnum<TraceEntrySeverity>(),
				TraceCategory = source.Field<string>(prefixColumn + "TraceCategory"),
			};
		}

		/// <summary>
		/// Gets all the TraceSummaries
		/// </summary>
		/// <returns></returns>
		public List<TraceSummary> GetAll() {
			var retVal = GetAllNoPaging();
			return retVal;
		}
	}
}
﻿using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.DataLayer.BrokerDB {

	/// <summary>
	/// Database broker for DataSeriePsetRatio business entity.
	/// </summary>
	public class DataSeriePsetRatioBrokerDB : BaseBrokerDB<DataSeriePsetRatio> {

		/// <summary>
		/// The name of the stored procedure that retreives a list of DataSeriePsetRatio for a specific DataSerie.
		/// </summary>
		protected const string const_proc_getlistbydataserie = AppModule.Energy + "_DataSeriePsetRatioGetListByKeyDataSerie";

		/// <summary>
		/// The name of the stored procedure that retreives a list of DataSeriePsetRatio for a specific DataSerie.
		/// </summary>
		protected const string const_proc_getallpagingbykeydataserie = AppModule.Energy + "_DataSeriePsetRatioGetAllPagingByKeyDataSerie";

		/// <summary>
		/// The name of the stored procedure that saves a list of DataSeriePsetRatio for a specific DataSerie.
		/// </summary>
		protected const string const_proc_dataseriedataseriepsetratiosave = AppModule.Energy + "_DataSerieDataSeriePsetRatioSave";

		/// <summary>
		/// The prefix of stored procedures of that provider. 
		/// </summary>
		protected override string const_proc_prefix {
			get {
				return AppModule.Energy;
			}
		}

		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, DataSeriePsetRatio item) {
			db.AddInParameterString(command, "Key", item.KeyDataSeriePsetRatio);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "LocalId", item.LocalId);
			db.AddInParameterInt(command, "Order", item.Order);
			db.AddInParameterString(command, "KeyDataSerie", item.KeyDataSerie);
			db.AddInParameterString(command, "PsetName", item.PsetName);
			db.AddInParameterString(command, "AttributeName", item.AttributeName);
			db.AddInParameterString(command, "Operator", item.Operator.ParseAsString());
			db.AddInParameterString(command, "GroupOperation", item.GroupOperation.ParseAsString());
			db.AddInParameterBool(command, "FillMissingValues", item.FillMissingValues);
			db.AddInParameterBool(command, "StopAtFirstLevelEncountered", item.StopAtFirstLevelEncountered);
		}

		/// <summary>
		/// Populates a DataSeriePsetRatio business entity from a DataSourceDecorator.
		/// </summary>
		/// <param name="source">The data source.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated DataSeriePsetRatio business entity.</returns>
		protected override DataSeriePsetRatio FillObject(DataSourceDecorator source, string prefixColumn) {
			var retVal = new DataSeriePsetRatio {
				KeyDataSeriePsetRatio = source.Field<int>(prefixColumn + "KeyDataSeriePsetRatio").ToString(CultureInfo.InvariantCulture),
				KeyDataSerie = source.Field<int>(prefixColumn + "KeyDataSerie").ToString(CultureInfo.InvariantCulture),
				KeyChart = source.Field<int>(prefixColumn + "KeyChart").ToString(CultureInfo.InvariantCulture),
				LocalId = source.Field<string>(prefixColumn + "LocalId"),
				Order = source.Field<int>(prefixColumn + "Order"),
				PsetName = source.Field<string>(prefixColumn + "PsetName"),
				AttributeName = source.Field<string>(prefixColumn + "AttributeName"),
				KeyPropertySingleValue = source.Field<string>(prefixColumn + "KeyPropertySingleValue"),
				Operator = source.Field<string>(prefixColumn + "Operator").ParseAsEnum<ArithmeticOperator>(),
				UsageName = source.Field<string>(prefixColumn + "UsageName"),
				GroupOperation = source.Field<string>(prefixColumn + "GroupOperation").ParseAsEnum<MathematicOperator>(),
				FillMissingValues = source.Field<bool?>(prefixColumn + "FillMissingValues") ?? false,
				PsetMsgCode = source.Field<string>(prefixColumn + "PsetMsgCode"),
				AttributeMsgCode = source.Field<string>(prefixColumn + "AttributeMsgCode"),
				StopAtFirstLevelEncountered = source.Field<bool?>(prefixColumn + "StopAtFirstLevelEncountered") ?? false
			};
			retVal.PropertySingleValueLongPath = string.Format("{0} / {1}",
				Helper.LocalizeText(retVal.PsetMsgCode),
				Helper.LocalizeText(retVal.AttributeMsgCode));

			return retVal;
		}

		/// <summary>
		/// Get the list of saved DataSeriePsetRatio from a DataSerie
		/// </summary>
		/// <param name="KeyDataSerie">The key of the DataSerie.</param>
		public List<DataSeriePsetRatio> GetListByKeyDataSerie(string KeyDataSerie) {
			List<DataSeriePsetRatio> results;
			DbCommand command = db.GetStoredProcCommand(const_proc_getlistbydataserie);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyDataSerie", KeyDataSerie);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}

		/// <summary>
		/// Gets all the DataSeriePsetRatio for a specific DataSerie.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyDataSerie">The Key of the DataSerie.</param>
		/// <param name="total">The total number of items.</param>
		/// <returns>The list of items.</returns>
		public List<DataSeriePsetRatio> GetAllPagingByKeyDataSerie(PagingParameter paging, string KeyDataSerie, out int total) {
			DbCommand command = db.GetStoredProcCommand(const_proc_getallpagingbykeydataserie);
			command.CommandTimeout = 600;
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyDataSerie", KeyDataSerie);
			db.AddInParameterInt(command, "start", paging.start);
			db.AddInParameterInt(command, "limit", paging.limit);
			DataSet ds = db.ExecuteDataSet(command);
			if (ds.Tables[0].Rows.Count > 0)
				total = ds.Tables[0].Rows[0].Field<int>("TotalRecord");
			else
				total = 0;
			return FillObjects(ds);
		}

		/// <summary>
		/// Saves the DataSerie DataSeriePsetRatio.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="ratios">The DataSeriePsetRatio store.</param>
		/// <returns></returns>
		public List<DataSeriePsetRatio> SaveDataSerieDataSeriePsetRatio(DataSerie entity, CrudStore<DataSeriePsetRatio> ratios) {
			DbCommand command = db.GetStoredProcCommand(const_proc_dataseriedataseriepsetratiosave);
			command.CommandTimeout = 600;
			List<DataSeriePsetRatio> results;

			if (ratios == null || ratios.IsEmpty())
				return null;
			string inserts = null;
			string deletes = null;


			if (ratios.destroy != null) {
				deletes = Helper.BuildXml(ratios.destroy.Select(p => p.KeyDataSeriePsetRatio).ToArray());
			}

			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyDataSerie", entity.KeyDataSerie);
			db.AddInParameterString(command, "Inserts", inserts);
			db.AddInParameterString(command, "Deletes", deletes);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}
	}
}
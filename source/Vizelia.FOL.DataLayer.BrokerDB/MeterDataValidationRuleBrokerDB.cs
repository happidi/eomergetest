﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Web.Security;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Vizelia.FOL.DataLayer.Interfaces;

namespace Vizelia.FOL.DataLayer.BrokerDB {

	/// <summary>
	/// Database broker for MeterValidationRule business entity.
	/// </summary>
	public class MeterValidationRuleBrokerDB : BaseBrokerDB<MeterValidationRule> {


		/// <summary>
		/// The prefix of stored procedures of that provider. 
		/// </summary>
		protected override string const_proc_prefix {
			get { return AppModule.Energy; }
		}


		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, MeterValidationRule item) {
			db.AddInParameterString(command, "Key", item.KeyMeterValidationRule);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "LocalId", item.LocalId);
			db.AddInParameterString(command, "Title", item.Title);
			db.AddInParameterString(command, "Emails", item.Emails);
			db.AddInParameterString(command, "Description", item.Description);
			db.AddInParameterBool(command, "Enabled", item.Enabled);
			db.AddInParameterInt(command, "AcquisitionFrequency", item.AcquisitionFrequency);
			db.AddInParameterString(command, "AcquisitionInterval", item.AcquisitionInterval.ParseAsString());
			db.AddInParameterInt(command, "Delay", item.Delay);
			db.AddInParameterString(command, "DelayInterval", item.DelayInterval.ParseAsString());
			db.AddInParameterBool(command, "IncreaseOnly", item.IncreaseOnly);
			db.AddInParameterDouble(command, "SpikeDetection", item.SpikeDetection);
			db.AddInParameterDouble(command, "SpikeDetectionAbsThreshold", item.SpikeDetectionAbsThreshold);
			db.AddInParameterInt(command, "MaxRecurringValues", item.MaxRecurringValues);
			db.AddInParameterDouble(command, "MinValue", item.MinValue);
			db.AddInParameterDouble(command, "MaxValue", item.MaxValue);
			db.AddInParameterString(command, "KeyLocalizationCulture", item.KeyLocalizationCulture);
			db.AddInParameterDateTime(command, "StartDate", item.StartDate);
			db.AddInParameterDateTime(command, "EndDate", item.EndDate);
			db.AddInParameterDateTime(command, "LastProcessDateTime", item.LastProcessDateTime);
			db.AddInParameterString(command, "Validity", item.Validity.HasValue ? item.Validity.ParseAsString() : null);

		}


		/// <summary>
		/// Populates a MeterValidationRule business entity from a DataSourceDecorator.
		/// </summary>
		/// <param name="source">The data source.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated MeterValidationRule business entity.</returns>
		protected override MeterValidationRule FillObject(DataSourceDecorator source, string prefixColumn) {
			var retVal= new MeterValidationRule {
				KeyMeterValidationRule = source.Field<int>(prefixColumn + "KeyMeterValidationRule").ToString(),
				LocalId = source.Field<string>(prefixColumn + "LocalId"),
				Title = source.Field<string>(prefixColumn + "Title"),
				Description = source.Field<string>(prefixColumn + "Description"),
				Emails = source.Field<string>(prefixColumn + "Emails"),
				Enabled = source.Field<bool>(prefixColumn + "Enabled"),
				AcquisitionFrequency = source.Field<int?>(prefixColumn + "AcquisitionFrequency"),
				AcquisitionInterval = source.Field<string>(prefixColumn + "AcquisitionInterval").ParseAsEnum<AxisTimeInterval>(),
				Delay = source.Field<int?>(prefixColumn + "Delay"),
				DelayInterval = source.Field<string>(prefixColumn + "DelayInterval").ParseAsEnum<AxisTimeInterval>(),
				IncreaseOnly = source.Field<bool>(prefixColumn + "IncreaseOnly"),
				SpikeDetection = source.Field<double?>(prefixColumn + "SpikeDetection"),
				SpikeDetectionAbsThreshold = source.Field<double?>(prefixColumn + "SpikeDetectionAbsThreshold"),
				MaxRecurringValues = source.Field<int?>(prefixColumn + "MaxRecurringValues"),
				MinValue = source.Field<double?>(prefixColumn + "MinValue"),
				MaxValue = source.Field<double?>(prefixColumn + "MaxValue"),
				KeyLocalizationCulture = source.Field<string>(prefixColumn + "KeyLocalizationCulture"),
				StartDate = source.Field<DateTime?>(prefixColumn + "StartDate"),
				EndDate = source.Field<DateTime?>(prefixColumn + "EndDate"),
				LastProcessDateTime = source.Field<DateTime?>(prefixColumn + "LastProcessDateTime"),
			};
			var validity=source.Field<string>(prefixColumn + "Validity");
			if (!string.IsNullOrEmpty(validity))
				retVal.Validity = validity.ParseAsEnum<MeterDataValidity>();

			return retVal;
		}


	}
}
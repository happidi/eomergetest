﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Web.Security;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Vizelia.FOL.DataLayer.Interfaces;

namespace Vizelia.FOL.DataLayer.BrokerDB {

	/// <summary>
	/// Database broker for DrawingCanvas business entity.
	/// </summary>
	public class DrawingCanvasBrokerDB : BaseBrokerDB<DrawingCanvas> {

		/// <summary>
		/// The prefix of stored procedures of that provider. 
		/// </summary>
		protected override string const_proc_prefix {
			get {
				return AppModule.Energy;
			}
		}


		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, DrawingCanvas item) {
			db.AddInParameterString(command, "Key", item.KeyDrawingCanvas);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "LocalId", item.LocalId);
			db.AddInParameterString(command, "Title", item.Title);
			db.AddInParameterBool(command, "OpenOnStartup", item.OpenOnStartup);
			db.AddInParameterBool(command, "IsFavorite", item.IsFavorite);
			db.AddInParameterBool(command, "DisplaySliders", item.DisplaySliders);
		}


		/// <summary>
		/// Populates a DrawingCanvas business entity from a DataSourceDecorator.
		/// </summary>
		/// <param name="source">The data source.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated DrawingCanvas business entity.</returns>
		protected override DrawingCanvas FillObject(DataSourceDecorator source, string prefixColumn) {
			return new DrawingCanvas {
				KeyDrawingCanvas = source.Field<int>(prefixColumn + "KeyDrawingCanvas").ToString(),
                Application = ContextHelper.ApplicationName,
				LocalId = source.Field<string>(prefixColumn + "LocalId"),
				Title = source.Field<string>(prefixColumn + "Title"),
				OpenOnStartup = source.Field<bool?>(prefixColumn + "OpenOnStartup") ?? false,
				IsFavorite = source.Field<bool?>(prefixColumn + "IsFavorite") ?? false,
				DisplaySliders = source.Field<bool?>(prefixColumn + "DisplaySliders") ?? false
			};
		}
	}
}
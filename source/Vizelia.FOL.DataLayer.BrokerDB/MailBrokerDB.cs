﻿using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using System.Net.Mail;
using System.Web.Security;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.DataLayer.BrokerDB {
	/// <summary>
	/// Database broker for Mail business entity.
	/// </summary>
	public class MailBrokerDB : BaseBrokerDB<Mail> {
		/// <summary>
		/// The name of the stored procedure that finds all workflow action elements associated to a specific mail.
		/// </summary>
		protected const string const_proc_getallworkflowactionelementpagingbykeymail = AppModule.ServiceDesk + "_WorkflowActionElementGetAllPagingByKeyMail";

		/// <summary>
		/// The name of the stored procedure that saves a a list of workflow action elements for a specific mail.
		/// </summary>
		protected const string const_proc_mailworkflowactionelementssave = AppModule.ServiceDesk + "_MailWorkflowActionElementsSave";

		/// <summary>
		/// The name of the stored procedure that gets the list of possible mails from inside a workflow.
		/// </summary>
		protected const string const_proc_mailgetlistfromworkflow = AppModule.ServiceDesk + "_MailGetListFromWorkflow";

		/// <summary>
		/// The name of the stored procedure that gets the list of possible mails by its classification local id.
		/// </summary>
		protected const string const_proc_mailgetlistbyclassificationlocalid = AppModule.ServiceDesk + "_MailGetListByClassificationLocalId";

		/// <summary>
		/// The prefix of stored procedures of that provider. 
		/// </summary>
		protected override string const_proc_prefix {
			get { return AppModule.ServiceDesk; }
		}

		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, Mail item) {
			db.AddInParameterString(command, "Key", item.KeyMail);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "Subject", item.Subject);
			db.AddInParameterString(command, "Name", item.Name);
			db.AddInParameterString(command, "Body", item.Body);
			db.AddInParameterString(command, "From", item.From);
			db.AddInParameterString(command, "To", item.To);
			db.AddInParameterString(command, "CC", item.CC);
			db.AddInParameterString(command, "Bcc", item.Bcc);
			db.AddInParameterInt(command, "Priority", item.Priority.ParseAsInt());
			db.AddInParameterBool(command, "IsActive", item.IsActive);
			db.AddInParameterString(command, "KeyClassificationItem", item.KeyClassificationItem);
		}

		/// <summary>
		/// Populates a Mail business entity from a DataSourceDecorator.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated Mail business entity.</returns>
		protected override Mail FillObject(DataSourceDecorator source, string prefixColumn) {
			return new Mail {
				KeyMail = source.Field<int>(prefixColumn + "KeyMail").ToString(CultureInfo.InvariantCulture),
				Subject = source.Field<string>(prefixColumn + "Subject"),
				Name = source.Field<string>(prefixColumn + "Name"),
				Body = source.Field<string>(prefixColumn + "Body"),
				From = source.Field<string>(prefixColumn + "From"),
				To = source.Field<string>(prefixColumn + "To"),
				CC = source.Field<string>(prefixColumn + "CC"),
				Bcc = source.Field<string>(prefixColumn + "Bcc"),
				Priority = source.Field<MailPriority>(prefixColumn + "Priority"),
				IsActive = source.Field<bool>(prefixColumn + "IsActive"),
				KeyClassificationItem = source.Field<string>(prefixColumn + "KeyClassificationItem"),
				KeyClassificationItemPath = source.Field<string>(prefixColumn + "KeyClassificationItemPath"),
				ClassificationItemLongPath = source.Field<string>(prefixColumn + "ClassificationItemLongPath"),
				ClassificationItemLocalIdPath = source.Field<string>(prefixColumn + "ClassificationItemLocalIdPath"),
				ClassificationItemTitle = source.Field<string>(prefixColumn + "ClassificationItemTitle"),
				ClassificationItemLevel = source.Field<int>(prefixColumn + "ClassificationItemLevel")
			};
		}

		/// <summary>
		/// Gets all workflow action elements paging by key mail.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="KeyMail">The key mail.</param>
		/// <param name="total">The total.</param>
		/// <returns>The list of WorkflowActionElement.</returns>
		public List<WorkflowActionElement> GetAllWorkflowActionElementPagingByKeyMail(PagingParameter paging, string KeyMail, out int total) {
			List<WorkflowActionElement> results = new List<WorkflowActionElement>();
			DbCommand command = db.GetStoredProcCommand(const_proc_getallworkflowactionelementpagingbykeymail);
			command.CommandTimeout = 600;
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyMail", KeyMail);
			db.AddInParameterInt(command, "start", paging.start);
			db.AddInParameterInt(command, "limit", paging.limit);
			DataSet ds = db.ExecuteDataSet(command);
			total = ds.Tables[0].Rows.Count > 0 ? ds.Tables[0].Rows[0].Field<int>("TotalRecord") : 0;

			using (IDataReader reader = db.ExecuteReader(command)) {
				while (reader.Read()) {
					results.Add(new WorkflowActionElement {
						Id = reader.GetValue<string>("ActionId"),
						MsgCode = reader.GetValue<string>("ActionId"),
						AssemblyShortName = reader.GetValue<string>("WorkflowAssemblyShortName"),
						AssemblyQualifiedName = reader.GetValue<string>("WorkflowAssemblyQualifiedName")
					});
				}
			}

			return results;
		}

		/// <summary>
		/// Saves the associated WorkflowActionElements for a specific mail.
		/// </summary>
		/// <param name="entityMail">The mail.</param>
		/// <param name="workflowActionElements">The workflow action elements.</param>
		/// <returns></returns>
		public List<WorkflowActionElement> SaveMailWorkflowActionElements(Mail entityMail, CrudStore<WorkflowActionElement> workflowActionElements) {
			List<WorkflowActionElement> results = new List<WorkflowActionElement>();

			if (workflowActionElements == null || workflowActionElements.IsEmpty())
				return null;

			List<string> wflList = new List<string>();

			if (workflowActionElements.create != null)
				wflList = workflowActionElements.create.Select(p => p.AssemblyShortName).ToList();

			if (workflowActionElements.destroy != null)
				wflList.AddRange((workflowActionElements.destroy.Select(p => p.AssemblyShortName).ToList()));

			foreach (var workflowAssemblyShortName in wflList.Distinct()) {
				results = SaveMailWorkflowActionElements(entityMail.KeyMail, workflowAssemblyShortName, workflowActionElements);
			}

			return results;
		}

		/// <summary>
		/// Saves the workflow action elements for a specific mail.
		/// </summary>
		/// <param name="KeyMail">The key mail.</param>
		/// <param name="workflowAssemblyShortName">Name of the workflow assembly definition.</param>
		/// <param name="workflowActionElement">The workflow action elements.</param>
		/// <returns></returns>
		private List<WorkflowActionElement> SaveMailWorkflowActionElements(string KeyMail, string workflowAssemblyShortName, CrudStore<WorkflowActionElement> workflowActionElement) {
			List<WorkflowActionElement> results = new List<WorkflowActionElement>();
			var command = db.GetStoredProcCommand(const_proc_mailworkflowactionelementssave);
			command.CommandTimeout = 600;

			string inserts = null;
			string deletes = null;

			if (workflowActionElement.create != null) {
				inserts =
					Helper.BuildXml(
						workflowActionElement.create.Where(p => p.AssemblyShortName == workflowAssemblyShortName).Select(p => p.Id).
							ToArray());
			}

			if (workflowActionElement.destroy != null) {
				deletes =
					Helper.BuildXml(
						workflowActionElement.destroy.Where(p => p.AssemblyShortName == workflowAssemblyShortName).Select(p => p.Id).
							ToArray());
			}

			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyMail", KeyMail);
			db.AddInParameterString(command, "WorkflowAssemblyShortName", workflowAssemblyShortName);
			db.AddInParameterString(command, "Inserts", inserts);
			db.AddInParameterString(command, "Deletes", deletes);
			using (IDataReader reader = db.ExecuteReader(command)) {
				if (reader.Read()) {
					results.Add(new WorkflowActionElement() {
						Id = reader.GetValue<string>("ActionId"),
						Value = reader.GetValue<string>("ActionId"),
						AssemblyShortName = reader.GetValue<string>("WorkflowAssemblyShortName"),
						AssemblyQualifiedName = reader.GetValue<string>("WorkflowAssemblyQualifiedName")
					});
				}
			}
			return results;
		}

		/// <summary>
		/// Gets the list of possibles mails from a within a workflow.
		/// </summary>
		/// <param name="classificationItemKeyParent">The classification item key parent.</param>
		/// <param name="classificationItemKeyChildren">The classification item key children.</param>
		/// <param name="priorityID">The priority ID.</param>
		/// <param name="keyLocation">The key location.</param>
		/// <param name="actionId">The action id.</param>
		/// <param name="assemblyQualifiedName">Name of the assembly qualified.</param>
		/// <returns></returns>
		public List<Mail> GetListFromWorkflow(string classificationItemKeyParent, string classificationItemKeyChildren, int priorityID, string keyLocation, string actionId, string assemblyQualifiedName) {
			List<Mail> results = default(List<Mail>);
			DbCommand command = db.GetStoredProcCommand(const_proc_mailgetlistfromworkflow);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyClassificationItemParent", classificationItemKeyParent);
			db.AddInParameterInt(command, "KeyPriority", priorityID);
			db.AddInParameterString(command, "KeyLocation", keyLocation);
			db.AddInParameterString(command, "WorkflowActionElement", actionId);
			db.AddInParameterString(command, "WorkflowAssemblyQualifiedName", assemblyQualifiedName);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}

		/// <summary>
		/// Gets the list by classification local id.
		/// </summary>
		/// <param name="classificationItemLocalId">The classification item local id.</param>
		/// <returns></returns>
		public List<Mail> GetListByClassificationLocalId(string classificationItemLocalId) {
			List<Mail> results = default(List<Mail>);
			DbCommand command = db.GetStoredProcCommand(const_proc_mailgetlistbyclassificationlocalid);
			db.AddInParameterString(command, "Application", Membership.ApplicationName);
			db.AddInParameterString(command, "ClassificationItemLocalId", classificationItemLocalId);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}
	}
}
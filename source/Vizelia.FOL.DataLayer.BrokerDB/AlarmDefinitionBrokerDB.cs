﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.DataLayer.BrokerDB {

	/// <summary>
	/// Database broker for AlarmDefinition business entity.
	/// </summary>
	public class AlarmDefinitionBrokerDB : BaseBrokerDB<AlarmDefinition> {

		/// <summary>
		/// The name of the stored procedure that retreives a list of AlarmDefinition for a specific location.
		/// </summary>
		protected const string const_proc_getlistbykeylocation = AppModule.Energy + "_AlarmDefinitionGetListByKeyLocation";


		/// <summary>
		/// The name of the stored procedure that retreives a list of AlarmDefinition for a specific chart.
		/// </summary>
		protected const string const_proc_getlistbykeychart = AppModule.Energy + "_AlarmDefinitionGetListByKeyChart";

		/// <summary>
		/// The name of the stored procedure that retreives a list of  Alarm definition for a specific  Alarm Table.
		/// </summary>
		protected const string const_proc_getlistbykeyalarmtable = AppModule.Energy + "_AlarmDefinitionGetListByKeyAlarmTable";

		/// <summary>
		/// The name of the stored procedure that retreives a list of  Alarm definition for a Alarm Table.
		/// </summary>
		protected const string const_proc_getallpagingbykeyalarmtable = AppModule.Energy + "_AlarmDefinitionGetAllPagingByKeyAlarmTable";


		/// <summary>
		/// The name of the stored procedure that saves a list of alarmdefinitions for a specific AlarmTable.
		/// </summary>
		protected const string const_proc_alarmtablealarmdefinitionssave = AppModule.Energy + "_AlarmTableAlarmDefinitionsSave";

		/// <summary>
		/// The name of the stored procedure that retreives the complete list of alarmdefinitions for a specific AlarmTable.
		/// </summary>
		protected const string const_proc_getcompletelistbykeyalarmtable = AppModule.Energy + "_AlarmDefinitionGetCompleteListByKeyAlarmTable";

		/// <summary>
		/// The prefix of stored procedures of that provider. 
		/// </summary>
		protected override string const_proc_prefix {
			get { return AppModule.Energy; }
		}

		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, AlarmDefinition item) {
			db.AddInParameterString(command, "Key", item.KeyAlarmDefinition);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "LocalId", item.LocalId);
			db.AddInParameterString(command, "Title", item.Title);
			db.AddInParameterString(command, "Description", item.Description);
			db.AddInParameterBool(command, "Enabled", item.Enabled);
			db.AddInParameterBool(command, "ProcessManually", item.ProcessManually);

			db.AddInParameterBool(command, "UsePercentageDelta", item.UsePercentageDelta);

			db.AddInParameterString(command, "KeyLocation", item.KeyLocation);

			db.AddInParameterString(command, "KeyCalendarEventCategory", item.KeyCalendarEventCategory);
			db.AddInParameterString(command, "CalendarMode", item.CalendarMode.ParseAsString());
			db.AddInParameterString(command, "TimeInterval", item.TimeInterval.ParseAsString());

			db.AddInParameterString(command, "KeyClassificationItem", item.KeyClassificationItem);

			db.AddInParameterString(command, "ClassificationKeyParent", item.ClassificationKeyParent);

			db.AddInParameterString(command, "ClassificationKeyChildren", item.ClassificationKeyChildren);

			db.AddInParameterDateTime(command, "StartDate", item.StartDate);
			db.AddInParameterDateTime(command, "EndDate", item.EndDate);
			db.AddInParameterDateTime(command, "LastProcessDateTime", item.LastProcessDateTime);

			db.AddInParameterDouble(command, "Threshold", item.Threshold);
			if (item.Condition == FilterCondition.Between || item.Condition == FilterCondition.Outside)
				db.AddInParameterDouble(command, "Threshold2", item.Threshold2);
			else
				db.AddInParameterDouble(command, "Threshold2", null);
			db.AddInParameterString(command, "Condition", item.Condition.ParseAsString());

			db.AddInParameterString(command, "Emails", item.Emails);
			db.AddInParameterString(command, "KeyLocalizationCulture", item.KeyLocalizationCulture);
			db.AddInParameterString(command, "TimeZoneId", item.TimeZoneId);
		}


		/// <summary>
		/// Populates a AlarmDefinition business entity from a DataSourceDecorator.
		/// </summary>
		/// <param name="source">The data source.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated AlarmDefinition business entity.</returns>
		protected override AlarmDefinition FillObject(DataSourceDecorator source, string prefixColumn) {
			return new AlarmDefinition {
				KeyAlarmDefinition = source.Field<int>(prefixColumn + "KeyAlarmDefinition").ToString(CultureInfo.InvariantCulture),
				LocalId = source.Field<string>(prefixColumn + "LocalId"),
				Title = source.Field<string>(prefixColumn + "Title"),
				Description = source.Field<string>(prefixColumn + "Description"),
				Enabled = source.Field<bool>(prefixColumn + "Enabled"),
				StartDate = source.Field<DateTime?>(prefixColumn + "StartDate"),
				EndDate = source.Field<DateTime?>(prefixColumn + "EndDate"),
				ProcessManually = source.Field<bool?>(prefixColumn + "ProcessManually") ?? false,

				UsePercentageDelta = source.Field<bool?>(prefixColumn + "UsePercentageDelta") ?? false,

				KeyLocation = source.Field<string>(prefixColumn + "KeyLocation"),
				LocationLevel = source.Field<int?>("LocationLevel"),
				LocationLongPath = source.Field<string>(prefixColumn + "LocationLongPath"),
				LocationShortPath = source.Field<string>(prefixColumn + "LocationShortPath"),
				LocationName = source.Field<string>(prefixColumn + "LocationName"),
				LocationTypeName = source.Field<string>(prefixColumn + "LocationTypeName").ParseAsEnum<HierarchySpatialTypeName>(),


				KeyCalendarEventCategory = source.Field<string>(prefixColumn + "KeyCalendarEventCategory"),
				CalendarMode = source.Field<string>(prefixColumn + "CalendarMode").ParseAsEnum<ChartCalendarMode>(),
				TimeInterval = source.Field<string>(prefixColumn + "TimeInterval").ParseAsEnum<AxisTimeInterval>(),

				KeyClassificationItem = source.Field<string>(prefixColumn + "KeyClassificationItem"),
				KeyClassificationItemPath = source.Field<string>(prefixColumn + "KeyClassificationItemPath"),
				ClassificationItemLongPath = source.Field<string>(prefixColumn + "ClassificationItemLongPath"),
				ClassificationItemLocalIdPath = source.Field<string>(prefixColumn + "ClassificationItemLocalIdPath"),
				ClassificationItemTitle = source.Field<string>(prefixColumn + "ClassificationItemTitle"),
				ClassificationItemLevel = source.Field<int>(prefixColumn + "ClassificationItemLevel"),
				ClassificationItemColorR = source.Field<int>(prefixColumn + "ClassificationItemColorR"),
				ClassificationItemColorG = source.Field<int>(prefixColumn + "ClassificationItemColorG"),
				ClassificationItemColorB = source.Field<int>(prefixColumn + "ClassificationItemColorB"),


				ClassificationKeyParent = source.Field<string>(prefixColumn + "ClassificationKeyParent"),
				ClassificationKeyChildren = source.Field<string>(prefixColumn + "ClassificationKeyChildren"),
				ClassificationLongPath = source.Field<string>(prefixColumn + "ClassificationLongPath"),

				Threshold = source.Field<double?>(prefixColumn + "Threshold"),
				Threshold2 = source.Field<double?>(prefixColumn + "Threshold2"),
				Condition = source.Field<string>(prefixColumn + "Condition").ParseAsEnum<FilterCondition>(),
				Emails = source.Field<string>(prefixColumn + "Emails"),

				KeyLocalizationCulture = source.Field<string>(prefixColumn + "KeyLocalizationCulture"),
				TimeZoneId = source.Field<string>(prefixColumn + "TimeZoneId"),
				LastProcessDateTime = source.Field<DateTime?>(prefixColumn + "LastProcessDateTime"),
			};
		}

		/// <summary>
		/// Gets a list of AlarmDefinition for a specific location.
		/// </summary>
		/// <param name="KeyLocation">The Key location.</param>
		/// <returns></returns>
		public List<AlarmDefinition> GetListByKeyLocation(string KeyLocation) {
			List<AlarmDefinition> results;
			DbCommand command = db.GetStoredProcCommand(const_proc_getlistbykeylocation);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyLocation", KeyLocation);

			AddSecurityFilterParam<AlarmDefinition>(command, "AlarmDefinitionFilter");

			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}

		/// <summary>
		/// Gets a list of AlarmDefinition for a specific chart.
		/// </summary>
		/// <param name="KeyChart">The key chart.</param>
		/// <returns></returns>
		public List<AlarmDefinition> GetListByKeyChart(string KeyChart) {
			List<AlarmDefinition> results;
			DbCommand command = db.GetStoredProcCommand(const_proc_getlistbykeychart);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyChart", KeyChart);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}


		/// <summary>
		/// Get the list of saved AlarmDefinition for a specific AlarmTable .
		/// </summary>
		/// <param name="KeyAlarmTable">The key alarm table.</param>
		/// <returns></returns>
		public List<AlarmDefinition> GetListByKeyAlarmTable(string KeyAlarmTable) {
			List<AlarmDefinition> results;
			DbCommand command = db.GetStoredProcCommand(const_proc_getlistbykeyalarmtable);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyAlarmTable", KeyAlarmTable);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}

		/// <summary>
		/// Gets all the AlarmDefinition for a specific AlarmTable.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyAlarmTable">The key alarm table.</param>
		/// <param name="total">The total number of items.</param>
		/// <returns>
		/// The list of items.
		/// </returns>
		public List<AlarmDefinition> GetAllPagingByKeyAlarmTable(PagingParameter paging, string KeyAlarmTable, out int total) {
			DbCommand command = db.GetStoredProcCommand(const_proc_getallpagingbykeyalarmtable);
			command.CommandTimeout = 600;
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyAlarmTable", KeyAlarmTable);
			db.AddInParameterInt(command, "start", paging.start);
			db.AddInParameterInt(command, "limit", paging.limit);
			DataSet ds = db.ExecuteDataSet(command);
			if (ds.Tables[0].Rows.Count > 0)
				total = ds.Tables[0].Rows[0].Field<int>("TotalRecord");
			else
				total = 0;
			return FillObjects(ds);
		}

		/// <summary>
		/// Saves the AlarmTable alarmdefinitions.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="alarmdefinitions">The alarmdefinitions.</param>
		/// <returns></returns>
		public List<AlarmDefinition> SaveAlarmTableAlarmDefinitions(AlarmTable entity, CrudStore<AlarmDefinition> alarmdefinitions) {
			DbCommand command = db.GetStoredProcCommand(const_proc_alarmtablealarmdefinitionssave);
			command.CommandTimeout = 600;
			List<AlarmDefinition> results;

			if (alarmdefinitions == null || alarmdefinitions.IsEmpty())
				return null;
			string inserts = null;
			string deletes = null;

			if (alarmdefinitions.create != null) {
				inserts = Helper.BuildXml(alarmdefinitions.create.Select(p => p.KeyAlarmDefinition).ToArray());
			}

			if (alarmdefinitions.destroy != null) {
				deletes = Helper.BuildXml(alarmdefinitions.destroy.Select(p => p.KeyAlarmDefinition).ToArray());
			}

			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyAlarmTable", entity.KeyAlarmTable);
			db.AddInParameterString(command, "Inserts", inserts);
			db.AddInParameterString(command, "Deletes", deletes);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}

		/// <summary>
		/// Returns the complete list of AlarmDefinition for a specific Alarmtable based on AlarmDefinition Classification Filter, Spatial Filter, AlarmDefinition instances.
		/// </summary>
		/// <param name="KeyAlarmTable">The Key of the AlarmTable.</param>
		/// <returns></returns>
		public List<AlarmDefinition> GetCompleteListByKeyAlarmTable(string KeyAlarmTable) {
			List<AlarmDefinition> results;
			DbCommand command = db.GetStoredProcCommand(const_proc_getcompletelistbykeyalarmtable);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyAlarmTable", KeyAlarmTable);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}
	}
}
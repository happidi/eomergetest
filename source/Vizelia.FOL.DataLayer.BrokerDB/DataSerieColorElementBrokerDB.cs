﻿using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.DataLayer.BrokerDB {

	/// <summary>
	/// Database broker for DataSerieColorElement business entity.
	/// </summary>
	public class DataSerieColorElementBrokerDB : BaseBrokerDB<DataSerieColorElement> {

		/// <summary>
		/// The name of the stored procedure that retreives a list of DataSerieColorElement for a specific DataSerie.
		/// </summary>
		protected const string const_proc_getlistbydataserie = AppModule.Energy + "_DataSerieColorElementGetListByKeyDataSerie";


		/// <summary>
		/// The name of the stored procedure that retreives a list of DataSerieColorElement for a specific DataSerie.
		/// </summary>
		protected const string const_proc_getallpagingbykeydataserie = AppModule.Energy + "_DataSerieColorElementGetAllPagingByKeyDataSerie";


		/// <summary>
		/// The name of the stored procedure that saves a list of DataSerieColorElement for a specific DataSerie.
		/// </summary>
		protected const string const_proc_dataseriedataseriecolorelementsave = AppModule.Energy + "_DataSerieDataSerieColorElementSave";

		/// <summary>
		/// The prefix of stored procedures of that provider. 
		/// </summary>
		protected override string const_proc_prefix {
			get {
				return AppModule.Energy;
			}
		}


		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, DataSerieColorElement item) {
			db.AddInParameterString(command, "Key", item.KeyDataSerieColorElement);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyDataSerie", item.KeyDataSerie);
			db.AddInParameterString(command, "Highlight", item.Highlight.ParseAsString());
			db.AddInParameterString(command, "Color", item.Color);
			db.AddInParameterDouble(command, "MaxValue", item.MaxValue);
			db.AddInParameterDouble(command, "MinValue", item.MinValue);
			db.AddInParameterString(command, "LocalId", item.LocalId);
		}


		/// <summary>
		/// Populates a DataSerieColorElement business entity from a DataSourceDecorator.
		/// </summary>
		/// <param name="source">The data source.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated DataSerieColorElement business entity.</returns>
		protected override DataSerieColorElement FillObject(DataSourceDecorator source, string prefixColumn) {
			return new DataSerieColorElement {
				KeyDataSerieColorElement = source.Field<int>(prefixColumn + "KeyDataSerieColorElement").ToString(CultureInfo.InvariantCulture),
				KeyDataSerie = source.Field<int>(prefixColumn + "KeyDataSerie").ToString(CultureInfo.InvariantCulture),
				Highlight = source.Field<string>(prefixColumn + "Highlight").ParseAsEnum<DataPointHighlight>(),
				Color = source.Field<string>(prefixColumn + "Color"),
				MinValue = source.Field<double?>(prefixColumn + "MinValue"),
				MaxValue = source.Field<double?>(prefixColumn + "MaxValue"),
				LocalId = source.Field<string>(prefixColumn + "LocalId")
			};
		}

		/// <summary>
		/// Get the list of saved DataSerieColorElement from a DataSerie
		/// </summary>
		/// <param name="KeyDataSerie">The key of the DataSerie.</param>
		public List<DataSerieColorElement> GetListByKeyDataSerie(string KeyDataSerie) {
			List<DataSerieColorElement> results;
			DbCommand command = db.GetStoredProcCommand(const_proc_getlistbydataserie);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyDataSerie", KeyDataSerie);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}

		/// <summary>
		/// Gets all the DataSerieColorElement for a specific DataSerie.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyDataSerie">The Key of the DataSerie.</param>
		/// <param name="total">The total number of items.</param>
		/// <returns>The list of items.</returns>
		public List<DataSerieColorElement> GetAllPagingByKeyDataSerie(PagingParameter paging, string KeyDataSerie, out int total) {
			DbCommand command = db.GetStoredProcCommand(const_proc_getallpagingbykeydataserie);
			command.CommandTimeout = 600;
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyDataSerie", KeyDataSerie);
			db.AddInParameterInt(command, "start", paging.start);
			db.AddInParameterInt(command, "limit", paging.limit);
			DataSet ds = db.ExecuteDataSet(command);
			if (ds.Tables[0].Rows.Count > 0)
				total = ds.Tables[0].Rows[0].Field<int>("TotalRecord");
			else
				total = 0;
			return FillObjects(ds);
		}


		/// <summary>
		/// Saves the DataSerie DataSerieColorElement.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="colorelements">The colorelements.</param>
		/// <returns></returns>
		public List<DataSerieColorElement> SaveDataSerieDataSerieColorElement(DataSerie entity, CrudStore<DataSerieColorElement> colorelements) {
			DbCommand command = db.GetStoredProcCommand(const_proc_dataseriedataseriecolorelementsave);
			command.CommandTimeout = 600;
			List<DataSerieColorElement> results;

			if (colorelements == null || colorelements.IsEmpty())
				return null;
			string inserts = null;
			string deletes = null;


			if (colorelements.destroy != null) {
				deletes = Helper.BuildXml(colorelements.destroy.Select(p => p.KeyDataSerieColorElement).ToArray());
			}

			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyDataSerie", entity.KeyDataSerie);
			db.AddInParameterString(command, "Inserts", inserts);
			db.AddInParameterString(command, "Deletes", deletes);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}
	}
}
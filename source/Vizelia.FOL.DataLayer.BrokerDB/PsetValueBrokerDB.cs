﻿using System.Data;
using System.Data.Common;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Localization;

namespace Vizelia.FOL.DataLayer.BrokerDB {
	
	/// <summary>
	/// Database broker for PsetValue business entity.
	/// </summary>
	public class PsetValueBrokerDB : BaseBrokerDB<PsetValue> {

		const string const_proc_get_item_by_value = AppModule.Core + "_PsetValueGetItemByValue";

		/// <summary>
		/// Database table name that is different from the property name.
		/// </summary>
		protected override string EntitySourceName {
			get { return "ObjectPset"; }
		}

		/// <summary>
		/// Database key name that is different from the property name.
		/// </summary>
		protected override string EntityKeyNameInDB {
			get { return "KeyPropertySingleValue"; }
		}

		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(System.Data.Common.DbCommand command, PsetValue item) {
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyPropertySingleValue", item.KeyPsetValue);
			db.AddInParameterString(command, "PsetName", item.PsetName);
			db.AddInParameterString(command, "KeyObject", item.KeyObject);
			db.AddInParameterString(command, "AttributeName", item.AttributeName);
			db.AddInParameterString(command, "AttributeValue", item.AttributeValue);
		}

		/// <summary>
		/// Fills a list of pset from a datareader.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="prefixColumn"></param>
		/// <returns>The list of resulting psets.</returns>
		protected override PsetValue FillObject(DataSourceDecorator source, string prefixColumn) {
			var psetValue = new PsetValue {
				App = source.Field<string>("App"),
				KeyPsetValue = source.Field<string>("KeyPropertySingleValue"),
				KeyObject = source.Field<string>("KeyObject"),
				PsetName = source.Field<string>("PsetName"),
				AttributeName = source.Field<string>("AttributeName"),
				AttributeValue = source.Field<string>("AttributeValue")
			};
			return psetValue;
		}

		/// <summary>
		/// Overriding Create PsetValue with validation of already existing record.
		/// </summary>
		/// <param name="item"></param>
		/// <returns></returns>
		public override PsetValue Create(PsetValue item) {

			//Check if item exists. If yes - exception, otherwise proceed with create.
			ValidatePsetNotExists(item);
			var ret = base.Create(item);
			return ret;
		}

		/// <summary>
		/// Validates if there is PsetValue with the given keys. If yes exception will be thrown.
		/// </summary>
		/// <param name="item"></param>
		private void ValidatePsetNotExists(PsetValue item) {

			DbCommand command = db.GetStoredProcCommand(const_proc_get_item_by_value);

			db.AddInParameterString(command, "PsetName", item.PsetName);
			db.AddInParameterString(command, "KeyObject", item.KeyObject);
			db.AddInParameterString(command, "AttributeName", item.AttributeName);
			
			using (IDataReader reader = db.ExecuteReader(command)) {
				if (reader.Read()) {
					PsetValue psetValue = FillObject(reader);
					if (psetValue != null) {

						//Throwing exception "The field you entered is already in use"
						throw new VizeliaDatabaseException(Langue.ResourceManager.GetString(Langue.error_msg_uniquefield), 2627);
					}
				}
			}
		}
	}
}

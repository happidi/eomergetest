﻿using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web.Security;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.DataLayer.BrokerDB {

	/// <summary>
	/// Database broker for AlgorithmInputValue business entity.
	/// </summary>
	public class AlgorithmInputValueBrokerDB : BaseBrokerDB<AlgorithmInputValue> {

		/// <summary>
		/// The name of the stored procedure that retreives a list of Portal Tabs for a specific Portal.
		/// </summary>
		protected const string const_proc_getlistbykeychart = AppModule.Energy + "_AlgorithmInputValueGetListByKeyChart";

		/// <summary>
		/// The name of the stored procedure that retreives a list of Portal Tabs for a specific Portal.
		/// </summary>
		protected const string const_proc_getallpagingbykeychart = AppModule.Energy + "_AlgorithmInputValueGetAllPagingByKeyChart";

		/// <summary>
		/// The name of the stored procedure that saves a list of meters for a specific Portal.
		/// </summary>
		protected const string const_proc_chartscriptscriptinputvaluesave = AppModule.Energy + "_ChartAlgorithmAlgorithmInputValueSave";

		/// <summary>
		/// The prefix of stored procedures of that provider. 
		/// </summary>
		protected override string const_proc_prefix {
			get {
				return AppModule.Energy;
			}
		}

		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, AlgorithmInputValue item) {
			int key;
			string keyAlgorithmInputDefinition;

			if (!string.IsNullOrEmpty(item.KeyAlgorithmInputDefinition) && int.TryParse(item.KeyAlgorithmInputDefinition, out key) && key > 0) {
				keyAlgorithmInputDefinition = item.KeyAlgorithmInputDefinition;
			}
			else {
				keyAlgorithmInputDefinition = null;
			}


			db.AddInParameterString(command, "Key", item.KeyAlgorithmInputValue);
			db.AddInParameterString(command, "Application", Membership.ApplicationName);
			db.AddInParameterString(command, "KeyChart", item.KeyChart);
			db.AddInParameterString(command, "KeyAlgorithmInputDefinition", keyAlgorithmInputDefinition);
			db.AddInParameterString(command, "Name", item.Name);
			db.AddInParameterString(command, "Value", item.Value);
			db.AddInParameterString(command, "KeyAlgorithm", item.KeyAlgorithm);
		}

		/// <summary>
		/// Populates a AlgorithmInputValue business entity from a DataSourceDecorator.
		/// </summary>
		/// <param name="source">The data source.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated Algorithm business entity.</returns>
		protected override AlgorithmInputValue FillObject(DataSourceDecorator source, string prefixColumn) {
			return new AlgorithmInputValue {
				KeyAlgorithmInputValue = source.Field<int>(prefixColumn + "KeyAlgorithmInputValue").ToString(),
				KeyAlgorithmInputDefinition = source.Field<int?>(prefixColumn + "KeyAlgorithmInputDefinition").ToString(),
				KeyChart = source.Field<int>(prefixColumn + "KeyChart").ToString(),
				KeyAlgorithm = source.Field<string>(prefixColumn + "KeyAlgorithm"),
				Value = source.Field<string>(prefixColumn + "Value"),
				Name = source.Field<string>(prefixColumn + "Name"),
				Description = source.Field<string>(prefixColumn + "Description")
			};
		}

		/// <summary>
		/// Get the list of saved AlgorithmInputValue from a Chart.
		/// </summary>
		/// <param name="KeyChart">The key chart.</param>
		/// <param name="KeyAlgorithm">The key algorithm.</param>
		/// <returns></returns>
		public List<AlgorithmInputValue> GetListByKeyChart(string KeyChart, string KeyAlgorithm) {
			List<AlgorithmInputValue> results;
			DbCommand command = db.GetStoredProcCommand(const_proc_getlistbykeychart);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyChart", KeyChart);
			db.AddInParameterString(command, "KeyAlgorithm", KeyAlgorithm);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}

		/// <summary>
		/// Gets all the AlgorithmInputValue for a specific Chart.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyChart">The key chart.</param>
		/// <param name="KeyAlgorithm">The key algorithm.</param>
		/// <param name="total">The total number of items.</param>
		/// <returns>
		/// The list of items.
		/// </returns>
		public List<AlgorithmInputValue> GetAllPagingByKeyAlgorithm(PagingParameter paging, string KeyChart, string KeyAlgorithm, out int total) {
			DbCommand command = db.GetStoredProcCommand(const_proc_getallpagingbykeychart);
			command.CommandTimeout = 600;
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyChart", KeyChart);
			db.AddInParameterString(command, "KeyAlgorithm", KeyAlgorithm);
			db.AddInParameterInt(command, "start", paging.start);
			db.AddInParameterInt(command, "limit", paging.limit);
			DataSet ds = db.ExecuteDataSet(command);
			if (ds.Tables[0].Rows.Count > 0)
				total = ds.Tables[0].Rows[0].Field<int>("TotalRecord");
			else
				total = 0;
			return FillObjects(ds);
		}

		/// <summary>
		/// Saves the portal AlgorithmInputDefinitions.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="values">The values.</param>
		/// <returns></returns>
		public List<AlgorithmInputValue> SaveChartAlgorithmAlgorithmInputValue(ChartAlgorithm entity, CrudStore<AlgorithmInputValue> values) {
			DbCommand command = db.GetStoredProcCommand(const_proc_chartscriptscriptinputvaluesave);
			command.CommandTimeout = 600;
			List<AlgorithmInputValue> results;

			if (values == null || values.IsEmpty())
				return null;
			string deletes = null;


			if (values.destroy != null) {
				deletes = Helper.BuildXml(values.destroy.Select(p => p.KeyAlgorithmInputValue).ToArray());
			}

			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyChart", entity.KeyChart);
			db.AddInParameterString(command, "KeyAlgorithm", entity.KeyAlgorithm);
			db.AddInParameterString(command, "Inserts", null);
			db.AddInParameterString(command, "Deletes", deletes);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}
	}
}
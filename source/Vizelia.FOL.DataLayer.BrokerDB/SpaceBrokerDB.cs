﻿using Vizelia.FOL.BusinessEntities;
using System.Data.Common;
using System.Web.Security;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.DataLayer.BrokerDB {
	/// <summary>
	/// Database broker for Space business entity.
	/// </summary>
	public class SpaceBrokerDB : BaseBrokerDB<Space> {

		/// <summary>
		/// Populates a Space business entity from a DataSourceDecorator.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated Space business entity.</returns>
		protected override Space FillObject(DataSourceDecorator source, string prefixColumn) {
			return new Space {
				KeySpace = source.Field<string>(prefixColumn + "KeySpace"),
				GlobalId = source.Field<string>(prefixColumn + "GlobalId"),
				LocalId = source.Field<string>(prefixColumn + "LocalId"),
				ElevationWithFlooring = source.Field<double>(prefixColumn + "ElevationWithFlooring"),
				AreaValue = source.Field<double>(prefixColumn + "AreaValue"),
				ObjectType = source.Field<string>(prefixColumn + "ObjectType"),
				Name = source.Field<string>(prefixColumn + "Name"),
				Description = source.Field<string>(prefixColumn + "Description"),
				KeyBuildingStorey = source.Field<string>(prefixColumn + "KeyBuildingStorey")
			};
		}

		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, Space item) {
			db.AddInParameterString(command, "Key", item.KeySpace);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterDouble(command, "ElevationWithFlooring", item.ElevationWithFlooring);
			db.AddInParameterDouble(command, "AreaValue", item.AreaValue);
			db.AddInParameterString(command, "KeyBuildingStorey", item.KeyBuildingStorey);
			db.AddInParameterString(command, "ObjectType", item.ObjectType);
			db.AddInParameterString(command, "LocalId", item.LocalId);
			//db.AddInParameterString(command, "Groupe", item.Groupe);
			db.AddInParameterString(command, "Name", item.Name);
			db.AddInParameterString(command, "Description", item.Description);
            db.AddInParameterBool(command, "IsMappingInProgress", base.IsAsynchronousSave);
		}


        /// <summary>
        /// Builds the additional delete params.
        /// </summary>
        /// <param name="command">The command.</param>
        protected override void BuildAdditionalDeleteParams(DbCommand command) {
            db.AddInParameterBool(command, "IsMappingInProgress", base.IsAsynchronousSave);
        }

	}
}

﻿using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web.Security;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.DataLayer.BrokerDB {

	/// <summary>
	/// Database broker for PsetDefinition business entity.
	/// </summary>
	public class PsetDefinitionBrokerDB : BaseBrokerDB<PsetDefinition> {
        /// <summary>
        /// override - const for pset definition
        /// </summary>
        const string const_proc_getitembyname = AppModule.Core + "_PsetDefinitionGetItemByName";
		/// <summary>
		/// Populates a PsetDefinition business entity from a DataSourceDecorator.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated PsetDefinition business entity.</returns>
		protected override PsetDefinition FillObject(DataSourceDecorator source, string prefixColumn) {
			return new PsetDefinition {
				KeyPropertySet = source.Field<string>(prefixColumn + "KeyPropertySet"),
				PsetName = source.Field<string>(prefixColumn + "PsetName"),
				LocalId = source.Field<string>(prefixColumn + "LocalId"),
				UsageName = source.Field<string>(prefixColumn + "UsageName"),
				PsetDescription = source.Field<string>(prefixColumn + "PsetDescription"),
				MsgCode = source.Field<string>(prefixColumn + "MsgCode")
			};
		}

		
		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, PsetDefinition item) {
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "Key", item.KeyPropertySet);
			db.AddInParameterString(command, "PsetName", item.PsetName);
			db.AddInParameterString(command, "UsageName", item.UsageName);
			db.AddInParameterString(command, "PsetDescription", item.PsetDescription);
			db.AddInParameterString(command, "MsgCode", item.MsgCode);
		}

		/// <summary>
		/// Gets the list of pset herited from a KeyClassificationItem.
		/// We first make a call to PsetAttributeDefinitionBrokerDB to get a distinct list of PsetAttributeDefinitions.
		/// From that list we extract a distinct array of KeyPropertySet and we build a list of PsetDefinition populated with the PsetAttributeDefinitions.
		/// </summary>
		/// <param name="UsageName">Name of the usage.</param>
		/// <param name="KeyClassificationItem">The key classification item.</param>
		/// <returns></returns>
		public List<PsetDefinition> GetListHerited(string UsageName, string KeyClassificationItem) {
			PsetAttributeDefinitionBrokerDB psetAttributeDefinitionBrokerDB = new PsetAttributeDefinitionBrokerDB();
			List<PsetAttributeDefinition> list = psetAttributeDefinitionBrokerDB.GetListHerited(UsageName, KeyClassificationItem);
			string[] keysPropertySet = (from p in list select p.KeyPropertySet).Distinct().ToArray();
			List<PsetDefinition> results = this.GetList(keysPropertySet);
			foreach (PsetDefinition psetDefinition in results) {
				psetDefinition.Attributes = (from p in list where p.KeyPropertySet == psetDefinition.KeyPropertySet orderby p.AttributeOrder select p).ToList();
			}
			return results;
		}

		/// <summary>
		/// Gets the item by local id.
		/// The override is necessary because of .read() in FillObjects that is redondant with .read() in sub method GetItemByLocalId.
		/// </summary>
		/// <param name="localId">The local id.</param>
		/// <returns></returns>
		public override PsetDefinition GetItemByLocalId(string localId) {
			PsetDefinition item = null;
			DbCommand command = db.GetStoredProcCommand(const_proc_getitembyname);
			db.AddInParameterString(command, "PsetName", localId);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			using (IDataReader reader = db.ExecuteReader(command)) {
				if (reader.Read()) {
					item = FillObject(reader);
				}
			}
			return item;
		}
	}
}

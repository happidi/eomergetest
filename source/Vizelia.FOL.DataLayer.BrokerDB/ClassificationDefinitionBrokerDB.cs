﻿using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Web.Security;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Providers;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.DataLayer.BrokerDB {

	/// <summary>
	/// Database broker for ClassificationDefinition business entity.
	/// </summary>
	public class ClassificationDefinitionBrokerDB : BaseBrokerDB<ClassificationDefinition> {


		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, ClassificationDefinition item) {
			db.AddInParameterString(command, "Key", item.Category);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "IconCls", item.IconCls);
			db.AddInParameterString(command, "LocalId", item.LocalId);
			db.AddInParameterBool(command, "IconRootOnly", item.IconRootOnly);
		}

		/// <summary>
		/// Populates a ClassificationDefinition business entity from a DataSourceDecorator.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated ClassificationDefinition business entity.</returns>
		protected override ClassificationDefinition FillObject(DataSourceDecorator source, string prefixColumn) {
			return new ClassificationDefinition {
				Category = source.Field<string>(prefixColumn + "Category"),
				IconCls = source.Field<string>(prefixColumn + "IconCls"),
				LocalId = source.Field<string>(prefixColumn + "LocalId"),
				IconRootOnly = source.Field<bool>(prefixColumn + "IconRootOnly")
			};
		}


	}
}
﻿using System.Data.Common;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.DataLayer.BrokerDB {
	/// <summary>
	/// Database broker for MeterDataExportTask business entity.
	/// </summary>
	public class MeterDataExportTaskBrokerDB : BaseBrokerDB<MeterDataExportTask> {
		/// <summary>
		/// The prefix of stored procedures of that provider. 
		/// </summary>
		protected override string const_proc_prefix {
			get {
				return AppModule.Mapping;
			}
		}

		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, MeterDataExportTask item) {
			db.AddInParameterString(command, "Key", item.KeyMeterDataExportTask);
			db.AddInParameterInt(command, "KeyMappingTask", int.Parse(item.KeyMappingTask));
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "Name", item.Name);
			db.AddInParameterString(command, "Description", item.Description);
			db.AddInParameterString(command, "Filename", item.Filename);
			db.AddInParameterBool(command, "Compress", item.Compress);
			db.AddInParameterBool(command, "SingleOutputFile", item.SingleOutputFile);
			db.AddInParameterBool(command, "ExportAllMeters", item.ExportAllMeters);
			db.AddInParameterInt(command, "TimeRange", item.TimeRange);
			db.AddInParameterString(command, "TimeRangeInterval", item.TimeRangeInterval.ParseAsString());
		}

		/// <summary>
		/// Populates a MappingTask business entity from a DataSourceDecorator.
		/// </summary>
		/// <param name="source">The data source.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated MappingTask business entity.</returns>
		protected override MeterDataExportTask FillObject(DataSourceDecorator source, string prefixColumn) {
			return new MeterDataExportTask {
				KeyMeterDataExportTask = source.Field<int>(prefixColumn + "KeyMeterDataExportTask").ToString(),
				KeyMappingTask = source.Field<int>(prefixColumn + "KeyMappingTask").ToString(),
				Name = source.Field<string>(prefixColumn + "Name"),
				Description = source.Field<string>(prefixColumn + "Description"),
				Compress = source.Field<bool>(prefixColumn + "Compress"),
				ExportAllMeters = source.Field<bool>(prefixColumn + "ExportAllMeters"),
				SingleOutputFile = source.Field<bool>(prefixColumn + "SingleOutputFile"),
				TimeRange = source.Field<int>(prefixColumn + "TimeRange"),
				TimeRangeInterval = source.Field<string>(prefixColumn + "TimeRangeInterval").ParseAsEnum<AxisTimeInterval>(),
				Filename = source.Field<string>(prefixColumn + "Filename")
			};
		}

	}
}
using System.Data.Common;
using System.Globalization;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.DataLayer.BrokerDB {

	/// <summary>
	/// The EWSDataAcquisitionEndpoint Broker DB.
	/// </summary>
	public class EWSDataAcquisitionEndpointBrokerDB : DataAcquisitionEndpointBrokerDB<EWSDataAcquisitionEndpoint> {
		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, EWSDataAcquisitionEndpoint item) {
			db.AddInParameterString(command, "Key", item.KeyDataAcquisitionEndpoint);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "LocalId", item.LocalId);
			db.AddInParameterString(command, "ServerLocalId", item.ServerLocalId);
			db.AddInParameterString(command, "Name", item.Name);
			db.AddInParameterString(command, "Description", item.Description);
			db.AddInParameterString(command, "KeyDataAcquisitionContainer", item.KeyDataAcquisitionContainer);
			db.AddInParameterString(command, "Path", item.Path);
		}

		/// <summary>
		/// Populates a EWSDataAcquisitionEndpoint business entity from a DataSourceDecorator.
		/// </summary>
		/// <param name="source">The data source.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated EWSDataAcquisitionEndpoint business entity.</returns>
		protected override EWSDataAcquisitionEndpoint FillObject(DataSourceDecorator source, string prefixColumn) {
			return new EWSDataAcquisitionEndpoint {
				KeyDataAcquisitionEndpoint = source.Field<int>(prefixColumn + "KeyDataAcquisitionEndpoint").ToString(CultureInfo.InvariantCulture),
				KeyDataAcquisitionContainer = source.Field<int>(prefixColumn + "KeyDataAcquisitionContainer").ToString(CultureInfo.InvariantCulture),
				LocalId = source.Field<string>(prefixColumn + "LocalId"),
				ServerLocalId = source.Field<string>(prefixColumn + "ServerLocalId"),
				Name = source.Field<string>(prefixColumn + "Name"),
				Description = source.Field<string>(prefixColumn + "Description"),
				Path = source.Field<string>(prefixColumn + "Path"),
				TitleContainer = source.Field<string>(prefixColumn + "TitleContainer")
			};
		}
	}
}
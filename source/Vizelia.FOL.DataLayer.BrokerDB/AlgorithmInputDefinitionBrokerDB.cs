﻿using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using System.Web.Security;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.DataLayer.BrokerDB {

	/// <summary>
	/// Database broker for AlgorithmInputDefinition business entity.
	/// </summary>
	public class AlgorithmInputDefinitionBrokerDB : BaseBrokerDB<AlgorithmInputDefinition> {

		/// <summary>
		/// The name of the stored procedure that retreives a list of Portal Tabs for a specific Portal.
		/// </summary>
		protected const string const_proc_getlistbykeyscript = AppModule.Energy + "_AlgorithmInputDefinitionGetListByKeyAlgorithm";

		/// <summary>
		/// The name of the stored procedure that retreives a list of Portal Tabs for a specific Portal.
		/// </summary>
		protected const string const_proc_getallpagingbykeyscript = AppModule.Energy + "_AlgorithmInputDefinitionGetAllPagingByKeyAlgorithm";

		/// <summary>
		/// The name of the stored procedure that saves a list of meters for a specific Portal.
		/// </summary>
		protected const string const_proc_scriptinputdefinitionsave = AppModule.Energy + "_AlgorithmAlgorithmInputDefinitionSave";

		/// <summary>
		/// The prefix of stored procedures of that provider. 
		/// </summary>
		protected override string const_proc_prefix {
			get {
				return AppModule.Energy;
			}
		}

		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, AlgorithmInputDefinition item) {
			db.AddInParameterString(command, "Key", item.KeyAlgorithmInputDefinition);
			db.AddInParameterString(command, "Application", Membership.ApplicationName);
			db.AddInParameterString(command, "Name", item.Name);
			db.AddInParameterString(command, "Description", item.Description);
			db.AddInParameterString(command, "KeyAlgorithm", item.KeyAlgorithm);
		}

		/// <summary>
		/// Populates a AlgorithmInputDefinition business entity from a DataSourceDecorator.
		/// </summary>
		/// <param name="source">The data source.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated Algorithm business entity.</returns>
		protected override AlgorithmInputDefinition FillObject(DataSourceDecorator source, string prefixColumn) {
			return new AlgorithmInputDefinition {
				KeyAlgorithmInputDefinition = source.Field<int>(prefixColumn + "KeyAlgorithmInputDefinition").ToString(CultureInfo.InvariantCulture),
				Name = source.Field<string>(prefixColumn + "Name"),
				Description = source.Field<string>(prefixColumn + "Description"),
				KeyAlgorithm = source.Field<int>(prefixColumn + "KeyAlgorithm").ToString(CultureInfo.InvariantCulture)
			};
		}

		/// <summary>
		/// Get the list of saved AlgorithmInputDefinition from a Algorithm.
		/// </summary>
		/// <param name="KeyAlgorithm">The key of the portal window.</param>
		public List<AlgorithmInputDefinition> GetListByKeyAlgorithm(string KeyAlgorithm) {
			List<AlgorithmInputDefinition> results;
			DbCommand command = db.GetStoredProcCommand(const_proc_getlistbykeyscript);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyAlgorithm", KeyAlgorithm);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}

		/// <summary>
		/// Gets all the AlgorithmInputDefinition for a specific Portal.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyAlgorithm">The Key of the portal window.</param>
		/// <param name="total">The total number of items.</param>
		/// <returns>The list of items.</returns>
		public List<AlgorithmInputDefinition> GetAllPagingByKeyAlgorithm(PagingParameter paging, string KeyAlgorithm, out int total) {
			DbCommand command = db.GetStoredProcCommand(const_proc_getallpagingbykeyscript);
			command.CommandTimeout = 600;
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyAlgorithm", KeyAlgorithm);
			db.AddInParameterInt(command, "start", paging.start);
			db.AddInParameterInt(command, "limit", paging.limit);
			DataSet ds = db.ExecuteDataSet(command);
			if (ds.Tables[0].Rows.Count > 0)
				total = ds.Tables[0].Rows[0].Field<int>("TotalRecord");
			else
				total = 0;
			return FillObjects(ds);
		}

		/// <summary>
		/// Saves the portal AlgorithmInputDefinitions.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="AlgorithmInputDefinition">The AlgorithmInputDefinition store.</param>
		/// <returns></returns>
		public List<AlgorithmInputDefinition> SaveAlgorithmAlgorithmInputDefinition(Algorithm entity, CrudStore<AlgorithmInputDefinition> AlgorithmInputDefinition) {
			DbCommand command = db.GetStoredProcCommand(const_proc_scriptinputdefinitionsave);
			command.CommandTimeout = 600;
			List<AlgorithmInputDefinition> results;

			if (AlgorithmInputDefinition == null || AlgorithmInputDefinition.IsEmpty())
				return null;
			string deletes = null;


			if (AlgorithmInputDefinition.destroy != null) {
				deletes = Helper.BuildXml(AlgorithmInputDefinition.destroy.Select(p => p.KeyAlgorithmInputDefinition).ToArray());
			}

			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyAlgorithm", entity.KeyAlgorithm);
			db.AddInParameterString(command, "Inserts", null);
			db.AddInParameterString(command, "Deletes", deletes);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}
	}
}
﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.DataLayer.BrokerDB {
	/// <summary>
	/// Database broker for Pset business entity.
	/// </summary>
	public class PsetBrokerDB : BaseBrokerDB<Pset> {

		const string const_proc_cleanobsoletevalues = AppModule.Core + "_PsetCleanObsoleteValues";

		const string const_type_psetattribute = "dbo.PsetAttribute";

		private const string const_proc_getlistbypsetname = AppModule.Core + "_PsetGetListByNameAndAttributeName";

		/// <summary>
		/// Gets a list of psets.
		/// </summary>
		/// <param name="psetName">The name of the pset.</param>
		/// <param name="keyObjects">An array of keys object for which we want to retreive the pset.</param>
		/// <returns>The list of items corresponding to the provided keys.</returns>
		public List<Pset> GetList(string psetName, string[] keyObjects) {
			DbCommand command = db.GetStoredProcCommand(const_proc_getlist);
			db.AddInParameterString(command, "PsetName", psetName);
			db.AddInParameterXml(command, "KeyObjects", Helper.BuildXml(keyObjects));
			//db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			return FillObjects(db.ExecuteReader(command));

		}

		/// <summary>
		/// Gets a list of psets by their name and attribute name.
		/// </summary>
		/// <param name="psetName">The name of the pset.</param>
		/// <param name="attributeName">The name of the Attribute.</param>
		/// <param name="keyObjects">An array of keys object for which we want to retreive the pset.</param>
		/// <returns>The list of items corresponding to the provided keys.</returns>
		public List<Pset> GetListByPsetNameAndAttributeName(string psetName, string attributeName, string[] keyObjects) {
			DbCommand command = db.GetStoredProcCommand(const_proc_getlistbypsetname);
			db.AddInParameterXml(command, "Keys", Helper.BuildXml(keyObjects));
			db.AddInParameterString(command, "PsetName", psetName);
			db.AddInParameterString(command, "AttributeName", attributeName);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			return FillObjects(db.ExecuteReader(command));

		}


		/// <summary>
		/// Fills the objects.
		/// </summary>
		/// <param name="reader">The reader.</param>
		/// <returns></returns>
		protected override List<Pset> FillObjects(IDataReader reader) {
			return FillObjects(new DataReaderDecorator(reader));
		}

		/// <summary>
		/// ills a list of pset from a datareader.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <returns>The list of resulting psets.</returns>
		private List<Pset> FillObjects(DataReaderDecorator source) {
			List<Pset> results = new List<Pset>();
			string keyObject = null;
			string keyPset = null;
			Pset item = null;
			using (source) {
				while (source.Read()) {
					if ((keyObject != source.Field<string>("KeyObject")) || (keyPset != source.Field<string>("KeyPset"))) {
						keyObject = source.Field<string>("KeyObject");
						keyPset = source.Field<string>("KeyPset");
						item = new Pset { KeyObject = keyObject, KeyPset = source.Field<string>("KeyPset"), PsetName = source.Field<string>("PsetName"), Attributes = new List<PsetAttribute>() };//new Dictionary<string, string>()
						results.Add(item);
					}
					if (item != null)
						item.Attributes.Add(new PsetAttribute {
							Key = source.Field<string>("AttributeName"),
							KeyObject = source.Field<string>("KeyObject"),
							Value = source.Field<string>("AttributeValue"),
							AttributeName = source.Field<string>("AttributeName"),
							AttributeDescription = source.Field<string>("AttributeDescription"),
							AttributeOrder = source.Field<int>("AttributeOrder"),
							AttributeDataType = source.Field<string>("AttributeDataType"),
							AttributeDefaultValue = source.Field<string>("AttributeDefaultValue"),
							AttributeComponent = source.Field<string>("AttributeComponent"),
							AttributeMaxValue = source.Field<string>("AttributeMaxValue"),
							AttributeMinValue = source.Field<string>("AttributeMinValue"),
							AttributeMask = source.Field<string>("AttributeMask"),
							AttributeVType = source.Field<string>("AttributeVType"),
							AttributeMsgCode = source.Field<string>("AttributeMsgCode"),
							ListValueMsgCode = source.Field<string>("AttributeListValueMsgCode"),
							AttributeWidth = source.Field<int?>("AttributeWidth")
						});
				}
			}
			return results;
		}


		/// <summary>
		/// Gets an pset given its name and the KeyObject it applies to.
		/// </summary>
		/// <param name="psetName">The name of the pset.</param>
		/// <param name="key">The Key of the entity the pset applies to.</param>
		/// <returns>The pset.</returns>
		public Pset GetItem(string psetName, string key) {
			Pset item;
			DbCommand command = db.GetStoredProcCommand(const_proc_getitem);
			db.AddInParameterString(command, "PsetName", psetName);
			db.AddInParameterString(command, "Key", key);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			using (IDataReader reader = db.ExecuteReader(command)) {
				item = FillObject(reader);
			}
			return item;
		}

		/// <summary>
		/// Gets the item.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <returns></returns>
		public override Pset GetItem(string key) {
			return GetItem(null, key);
		}

		/// <summary>
		/// Gets the item by local id.
		/// The override is necessary because of .read() in FillObjects that is redondant with .read() in sub method GetItemByLocalId.
		/// </summary>
		/// <param name="localId">The local id.</param>
		/// <returns></returns>
		public override Pset GetItemByLocalId(string localId) {
			Pset item;
			DbCommand command = db.GetStoredProcCommand(const_proc_getitembylocalid);
			db.AddInParameterString(command, "LocalId", localId);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			using (IDataReader reader = db.ExecuteReader(command)) {
				item = FillObject(reader); // <- no reader.read() here !
			}
			return item;
		}

		/// <summary>
		/// Populates a Pset business entity from a IDataReader.
		/// </summary>
		/// <param name="source">The reader.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated Pset business entity.</returns>
		protected override Pset FillObject(DataSourceDecorator source, string prefixColumn) {
			DataReaderDecorator dataReader = source as DataReaderDecorator;
			if (dataReader != null) {
				return FillObjects(dataReader).FirstOrDefault();
			}
			throw new NotImplementedException();
		}

		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, Pset item) {
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "PsetName", item.PsetName);
			db.AddInParameterString(command, "KeyObject", item.KeyObject);
			//db.AddOutParameter(command, "Return", DbType.String, 255);
			db.AddInParameterTable(command, "PsetAttributes", item.Attributes.AsDataTable(), const_type_psetattribute);
		}

		/// <summary>
		/// Cleans the obsolete values of pset for an object.
		/// </summary>
		/// <param name="keyObject">The Key of the object to clean.</param>
		/// <param name="keyPropertySingleValues">The correct list of PsetAttributes that the object should expose.</param>
		public void CleanObsoleteValues(string keyObject, string[] keyPropertySingleValues) {
			DbCommand command = db.GetStoredProcCommand(const_proc_cleanobsoletevalues);
			db.AddInParameterString(command, "KeyObject", keyObject);
			db.AddInParameterXml(command, "KeyPropertySingleValues", Helper.BuildXml(keyPropertySingleValues));
			db.ExecuteNonQuery(command);
		}
	}
}

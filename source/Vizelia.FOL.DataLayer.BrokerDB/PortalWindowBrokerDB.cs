﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.DataLayer.BrokerDB {

	/// <summary>
	/// Database broker for Portal business entity.
	/// </summary>
	public class PortalWindowBrokerDB : BaseBrokerDB<PortalWindow> {

		/// <summary>
		/// The name of the stored procedure that retreives a paged store of portalwindows for a specific ChartScheduler.
		/// </summary>
		protected const string const_proc_getallpagingbykeychartscheduler = AppModule.Energy + "_PortalWindowGetAllPagingByKeyChartScheduler";

		/// <summary>
		/// The name of the stored procedure that retreives a list of portalwindows for a specific ChartScheduler.
		/// </summary>
		protected const string const_proc_getlistbykeychartscheduler = AppModule.Energy + "_PortalWindowGetListByKeyChartScheduler";

		/// <summary>
		/// The name of the stored procedure that saves a list of meters for a specific chart.
		/// </summary>
		protected const string const_proc_chartschedulerportalwindowsave = AppModule.Energy + "_ChartSchedulerPortalWindowSave";

		/// <summary>
		/// The name of the stored procedure that retreives a paged store of portalwindows for a specific PortalTemplate.
		/// </summary>
		protected const string const_proc_getallpagingbykeyportaltemplate = AppModule.Energy + "_PortalWindowGetAllPagingByKeyPortalTemplate";

		/// <summary>
		/// The name of the stored procedure that retreives a list of portalwindows for a specific PortalTemplate.
		/// </summary>
		protected const string const_proc_getlistbykeyportaltemplate = AppModule.Energy + "_PortalWindowGetListByKeyPortalTemplate";

		/// <summary>
		/// The name of the stored procedure that saves a list of portalwindows for a specific portaltemplate.
		/// </summary>
		protected const string const_proc_portaltemplateportalwindowsave = AppModule.Energy + "_PortalTemplatePortalWindowSave";

		/// <summary>
		/// The name of the stored procedure that retreives a paged store of portalwindows for a specific PortalTemplate.
		/// </summary>
		protected const string const_proc_getlistbycreator = AppModule.Energy + "_PortalWindowGetListByCreator";

        /// <summary>
        /// The name of the stored procedure that updates the last date modified of the portal window.
        /// </summary>
        protected const string const_proc_updatedatemodified = AppModule.Energy + "_PortalWindowUpdateDateModified";

        /// <summary>
        /// The name of the stored procedure that retreives a list of portal windows by portlet key and type.
        /// </summary>
        protected const string const_proc_getlistbyportlet = AppModule.Energy + "_PortalWindowGetListByPortlet";

		/// <summary>
		/// The prefix of stored procedures of that provider. 
		/// </summary>
		protected override string const_proc_prefix {
			get {
				return AppModule.Energy;
			}
		}

		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, PortalWindow item) {
			db.AddInParameterString(command, "Key", item.KeyPortalWindow);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "Title", item.Title);
			db.AddInParameterString(command, "LocalId", item.LocalId);
			db.AddInParameterString(command, "IconCls", string.IsNullOrEmpty(item.KeyImage) == false ? null : item.IconCls);
			db.AddInParameterBool(command, "DisplayInToolbar", item.DisplayInToolbar);
			db.AddInParameterInt(command, "DesktopShortcutIconX", item.DesktopShortcutIconX);
			db.AddInParameterInt(command, "DesktopShortcutIconY", item.DesktopShortcutIconY);
			db.AddInParameterBool(command, "OpenOnStartup", item.OpenOnStartup);
			db.AddInParameterString(command, "PortalTabColumnConfig", item.PortalTabColumnConfig.ParseAsString());
			db.AddInParameterString(command, "KeyPortalTemplateOrigin", item.KeyPortalTemplateOrigin);
			db.AddInParameterBool(command, "PreloadTab", item.PreloadTab);
			db.AddInParameterString(command, "KeyImage", item.KeyImage);
			db.AddInParameterInt(command, "PrintWidth", item.PrintWidth);
			db.AddInParameterInt(command, "PrintHeight", item.PrintHeight);
            db.AddInParameterString(command, "KeyPortalWindowOrigin", item.KeyPortalWindowOrigin);
		}

		/// <summary>
		/// Populates a PortalWindow business entity from a DataSourceDecorator.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated PortalWindow business entity.</returns>
		protected override PortalWindow FillObject(DataSourceDecorator source, string prefixColumn) {
			var keyPortalTemplateOrigin = source.Field<int?>(prefixColumn + "KeyPortalTemplateOrigin");
		    var keyPortalWindowOrigin = source.Field<int?>(prefixColumn + "KeyPortalWindowOrigin");
			var retVal = new PortalWindow {
				KeyPortalWindow = source.Field<int>(prefixColumn + "KeyPortalWindow").ToString(CultureInfo.InvariantCulture),
				Title = source.Field<string>(prefixColumn + "Title"),
				LocalId = source.Field<string>(prefixColumn + "LocalId"),
				IconCls = source.Field<string>(prefixColumn + "IconCls"),
				DisplayInToolbar = source.Field<bool>(prefixColumn + "DisplayInToolbar"),
				DesktopShortcutIconX = source.Field<int?>(prefixColumn + "DesktopShortcutIconX"),
				DesktopShortcutIconY = source.Field<int?>(prefixColumn + "DesktopShortcutIconY"),
				OpenOnStartup = source.Field<bool>(prefixColumn + "OpenOnStartup"),
				PortalTabColumnConfig = source.Field<string>(prefixColumn + "PortalTabColumnConfig").ParseAsEnum<PortalTabColumnConfig>(),
				KeyPortalTemplateOrigin = keyPortalTemplateOrigin.HasValue ? keyPortalTemplateOrigin.Value.ToString(CultureInfo.InvariantCulture) : null,
				PortalWindowTabNumber = source.Field<int>(prefixColumn + "PortalWindowTabNumber"),
				PreloadTab = source.Field<bool?>(prefixColumn + "PreloadTab") ?? false,
				CreatorLogin = source.Field<string>(prefixColumn + "CreatorLogin"),
				KeyImage = source.Field<string>(prefixColumn + "KeyImage"),
				PrintWidth = source.Field<int?>(prefixColumn + "PrintWidth"),
				PrintHeight = source.Field<int?>(prefixColumn + "PrintHeight"),
                KeyPortalWindowOrigin = keyPortalWindowOrigin.HasValue ? keyPortalWindowOrigin.Value.ToString(CultureInfo.InvariantCulture) : null
			};
			return retVal;
		}

		/// <summary>
		/// Gets a list of PortalWindows for a specific ChartScheduler.
		/// </summary>
		/// <param name="KeyChartScheduler">The Key ChartScheduler.</param>
		/// <returns></returns>
		public List<PortalWindow> GetListByKeyChartScheduler(string KeyChartScheduler) {
			List<PortalWindow> results;
			DbCommand command = db.GetStoredProcCommand(const_proc_getlistbykeychartscheduler);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyChartScheduler", KeyChartScheduler);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}

		/// <summary>
		/// Gets all the portalwindows for a specific ChartScheduler.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyChartScheduler">The Key of the ChartScheduler.</param>
		/// <param name="total">The total number of items.</param>
		/// <returns>The list of items.</returns>
		public List<PortalWindow> GetAllPagingByKeyChartScheduler(PagingParameter paging, string KeyChartScheduler, out int total) {
			DbCommand command = db.GetStoredProcCommand(const_proc_getallpagingbykeychartscheduler);
			command.CommandTimeout = 600;
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyChartScheduler", KeyChartScheduler);
			db.AddInParameterInt(command, "start", paging.start);
			db.AddInParameterInt(command, "limit", paging.limit);
			DataSet ds = db.ExecuteDataSet(command);
			if (ds.Tables[0].Rows.Count > 0)
				total = ds.Tables[0].Rows[0].Field<int>("TotalRecord");
			else
				total = 0;
			return FillObjects(ds);
		}

		/// <summary>
		/// Saves the ChartScheduler PortalWindows.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="portalwindows">The portalwindows.</param>
		/// <returns></returns>
		public List<PortalWindow> SavePortalWindowChartScheduler(ChartScheduler entity, CrudStore<PortalWindow> portalwindows) {
			DbCommand command = db.GetStoredProcCommand(const_proc_chartschedulerportalwindowsave);
			command.CommandTimeout = 600;
			List<PortalWindow> results;

			if (portalwindows == null || portalwindows.IsEmpty())
				return null;
			string inserts = null;
			string deletes = null;

			if (portalwindows.create != null) {
				inserts = Helper.BuildXml(portalwindows.create.Select(p => p.KeyPortalWindow).ToArray());
			}

			if (portalwindows.destroy != null) {
				deletes = Helper.BuildXml(portalwindows.destroy.Select(p => p.KeyPortalWindow).ToArray());
			}

			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyChartScheduler", entity.KeyChartScheduler);
			db.AddInParameterString(command, "Inserts", inserts);
			db.AddInParameterString(command, "Deletes", deletes);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}

		/// <summary>
		/// Gets a list of PortalWindows for a specific PortalTemplate.
		/// </summary>
		/// <param name="KeyPortalTemplate">The Key PortalTemplate.</param>
		/// <returns></returns>
		public List<PortalWindow> GetListByKeyPortalTemplate(string KeyPortalTemplate) {
			List<PortalWindow> results;
			DbCommand command = db.GetStoredProcCommand(const_proc_getlistbykeyportaltemplate);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyPortalTemplate", KeyPortalTemplate);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}

		/// <summary>
		/// Gets all the portalwindows for a specific PortalTemplate.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyPortalTemplate">The Key of the PortalTemplate.</param>
		/// <param name="total">The total number of items.</param>
		/// <returns>The list of items.</returns>
		public List<PortalWindow> GetAllPagingByKeyPortalTemplate(PagingParameter paging, string KeyPortalTemplate, out int total) {
			DbCommand command = db.GetStoredProcCommand(const_proc_getallpagingbykeyportaltemplate);
			command.CommandTimeout = 600;
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyPortalTemplate", KeyPortalTemplate);
			db.AddInParameterInt(command, "start", paging.start);
			db.AddInParameterInt(command, "limit", paging.limit);
			DataSet ds = db.ExecuteDataSet(command);
			if (ds.Tables[0].Rows.Count > 0)
				total = ds.Tables[0].Rows[0].Field<int>("TotalRecord");
			else
				total = 0;
			return FillObjects(ds);
		}

		/// <summary>
		/// Saves the PortalTemplate PortalWindows.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="portalwindows">The portalwindows.</param>
		/// <returns></returns>
		public List<PortalWindow> SavePortalWindowPortalTemplate(PortalTemplate entity, CrudStore<PortalWindow> portalwindows) {
			DbCommand command = db.GetStoredProcCommand(const_proc_portaltemplateportalwindowsave);
			command.CommandTimeout = 600;
			List<PortalWindow> results;

			if (portalwindows == null || portalwindows.IsEmpty())
				return null;
			string inserts = null;
			string deletes = null;

			if (portalwindows.create != null) {
				inserts = Helper.BuildXml(portalwindows.create.Select(p => p.KeyPortalWindow).ToArray());
			}

			if (portalwindows.destroy != null) {
				deletes = Helper.BuildXml(portalwindows.destroy.Select(p => p.KeyPortalWindow).ToArray());
			}

			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyPortalTemplate", entity.KeyPortalTemplate);
			db.AddInParameterString(command, "Inserts", inserts);
			db.AddInParameterString(command, "Deletes", deletes);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}

		/// <summary>
		/// Gets a list of PortalWindows for a specific Creator (user).
		/// </summary>
		/// <param name="Creator">The Key of the User.</param>
		/// <returns></returns>
		public List<PortalWindow> GetListByKeyCreator(string Creator) {
			List<PortalWindow> results;
			DbCommand command = db.GetStoredProcCommand(const_proc_getlistbycreator);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "Creator", Creator);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}

        /// <summary>
        /// Updates the date and time the portal was modified null defaults to now.
        /// </summary>
        /// <param name="PortalWindowKey">The portal window key.</param>
        /// <param name="PortalDateTimeModified">The portal date time modified.</param>
        public void UpdateDateTimeModified(string PortalWindowKey, DateTime? PortalDateTimeModified = null) {
            UpdateDateTimeModified(new List<string> { PortalWindowKey }, PortalDateTimeModified);
        }

        /// <summary>
        /// Updates the date and time the portal was modified null defaults to now.
        /// </summary>
        /// <param name="PortalWindowKeys">The portal window keys to update.</param>
        /// <param name="PortalDateTimeModified">The portal date time modified.</param>
        public void UpdateDateTimeModified(List<string> PortalWindowKeys, DateTime? PortalDateTimeModified = null) {
            DbCommand command = db.GetStoredProcCommand(const_proc_updatedatemodified);
            db.AddInParameterString(command, "ApplicationName", ContextHelper.ApplicationName);
            db.AddInParameterString(command, "PortalWindowKeys", Helper.BuildXml(PortalWindowKeys.ToArray()));
            db.AddInParameterDateTime(command, "TimeLastModified", PortalDateTimeModified);
            db.ExecuteNonQuery(command);
        }

        /// <summary>
        /// Gets a list of portal windows by portlet.
        /// </summary>
        /// <param name="KeyEntity">The key entity.</param>
        /// <param name="TypeEntity">The type entity.</param>
        /// <returns></returns>
	    public List<PortalWindow> GetListByPortlet(string KeyEntity, string TypeEntity) {
            List<PortalWindow> results;
            DbCommand command = db.GetStoredProcCommand(const_proc_getlistbyportlet);
            db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
            db.AddInParameterString(command, "KeyPortletEntity", KeyEntity);
            db.AddInParameterString(command, "PortletEntityType", TypeEntity);
            using (IDataReader reader = db.ExecuteReader(command)) {
                results = FillObjects(reader);
            }
            return results;
	    }
	}
}
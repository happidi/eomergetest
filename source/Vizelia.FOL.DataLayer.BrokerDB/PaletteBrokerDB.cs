﻿using Vizelia.FOL.BusinessEntities;
using System.Data.Common;
using System.Web.Security;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.DataLayer.BrokerDB {
	/// <summary>
	/// Database broker for Palette business entity.
	/// </summary>
	public class PaletteBrokerDB : BaseBrokerDB<Palette> {
		
		/// <summary>
		/// Gets the const_proc_prefix.
		/// </summary>
		/// <value>The const_proc_prefix.</value>
		protected override string const_proc_prefix {
			get {
				return AppModule.Energy;
			}
		}


		/// <summary>
		/// Populates a Palette business entity from a DataSourceDecorator.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated Palette business entity.</returns>
		protected override Palette FillObject(DataSourceDecorator source, string prefixColumn) {
			return new Palette {
				KeyPalette = source.Field<int>(prefixColumn + "KeyPalette").ToString(),
				LocalId = source.Field<string>(prefixColumn + "LocalId"),
				MsgCode = source.Field<string>(prefixColumn + "MsgCode")
			};
		}

		

		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, Palette item) {
			db.AddInParameterString(command, "Key", item.KeyPalette);
			db.AddInParameterString(command, "LocalId", item.LocalId);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "MsgCode", item.MsgCode);
		}

	
	}
}

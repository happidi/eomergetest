﻿using System.Data.Common;
using System.Globalization;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.DataLayer.BrokerDB {

	/// <summary>
	/// Database broker for RESTDataAcquisitionEndpoint business entity.
	/// </summary>
	public class RESTDataAcquisitionEndpointBrokerDB : DataAcquisitionEndpointBrokerDB<RESTDataAcquisitionEndpoint> {

		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, RESTDataAcquisitionEndpoint item) {
			db.AddInParameterString(command, "Key", item.KeyDataAcquisitionEndpoint);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "LocalId", item.LocalId);
			db.AddInParameterString(command, "Name", item.Name);
			db.AddInParameterString(command, "Description", item.Description);
			db.AddInParameterString(command, "KeyDataAcquisitionContainer", item.KeyDataAcquisitionContainer);
			db.AddInParameterString(command, "Path", item.Path);
			db.AddInParameterString(command, "QueryString", item.QueryString);
			db.AddInParameterString(command, "Unit", item.Unit);
		}


		/// <summary>
		/// Populates a RESTDataAcquisitionEndpoint business entity from a DataSourceDecorator.
		/// </summary>
		/// <param name="source">The data source.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated RESTDataAcquisitionEndpoint business entity.</returns>
		protected override RESTDataAcquisitionEndpoint FillObject(DataSourceDecorator source, string prefixColumn) {
			return new RESTDataAcquisitionEndpoint {
				KeyDataAcquisitionEndpoint = source.Field<int>(prefixColumn + "KeyDataAcquisitionEndpoint").ToString(CultureInfo.InvariantCulture),
				KeyDataAcquisitionContainer = source.Field<int>(prefixColumn + "KeyDataAcquisitionContainer").ToString(CultureInfo.InvariantCulture),
				LocalId = source.Field<string>(prefixColumn + "LocalId"),
				Name = source.Field<string>(prefixColumn + "Name"),
				Description = source.Field<string>(prefixColumn + "Description"),
				Path = source.Field<string>(prefixColumn + "Path"),
				TitleContainer = source.Field<string>(prefixColumn + "TitleContainer"),
				QueryString = source.Field<string>(prefixColumn + "QueryString"),
				ProviderType = source.Field<string>(prefixColumn + "ProviderType"),
				Unit = source.Field<string>(prefixColumn + "Unit")
			};
		}

		//    /// <summary>
		//    /// Saves the RESTDataAcquisitionEndpoint  for a specific container.
		//    /// </summary>
		//    /// <param name="entity">The entity.</param>
		//    /// <param name="endpoints">The endpoints.</param>
		//    /// <returns></returns>
		//    public List<RESTDataAcquisitionEndpoint> SaveRESTDataAcquisitionContainertRESTDataAcquisitionEndpoint(RESTDataAcquisitionContainer entity, CrudStore<RESTDataAcquisitionEndpoint> endpoints)
		//    {
		//        DbCommand command = db.GetStoredProcCommand(const_proc_struxurewaredataacquisitioncontainerstruxurewaredataacquisitionendpoint);
		//        command.CommandTimeout = 600;
		//        List<RESTDataAcquisitionEndpoint> results;

		//        if (endpoints == null || endpoints.IsEmpty())
		//            return null;
		//        string inserts = null;
		//        string deletes = null;

		//        if (endpoints.create != null) {
		//            inserts = Helper.BuildXml(endpoints.create.Select(p => p.KeyDataAcquisitionEndpoint).ToArray());
		//        }

		//        if (endpoints.destroy != null) {
		//            deletes = Helper.BuildXml(endpoints.destroy.Select(p => p.KeyDataAcquisitionEndpoint).ToArray());
		//        }

		//        db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
		//        db.AddInParameterString(command, "KeyDataAcquisitionContainer", entity.KeyDataAcquisitionContainer);
		//        db.AddInParameterString(command, "Inserts", inserts);
		//        db.AddInParameterString(command, "Deletes", deletes);
		//        using (IDataReader reader = db.ExecuteReader(command)) {
		//            results = FillObjects(reader);
		//        }
		//        return results;
		//    }
	}
}
﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Vizelia.FOL.Providers;


namespace Vizelia.FOL.DataLayer.BrokerDB {
	/// <summary>
	/// Tenant broker for Job business entity.
	/// </summary>
	public class TenantBrokerDB : BaseBrokerDB<Tenant> {
		private const string const_protocol_string = "://";

		/// <summary>
		/// The prefix of stored procedures.
		/// </summary>
		protected override string const_proc_prefix {
			get { return AppModule.Tenancy; }
		}

		/// <summary>
		/// Builds the save params.
		/// </summary>
		/// <param name="command">The command.</param>
		/// <param name="item">The item.</param>
		protected override void BuildSaveParams(DbCommand command, Tenant item) {
			db.AddInParameterString(command, "Key", item.KeyTenant);
			db.AddInParameterBool(command, "IsInitialized", item.IsInitialized);
			db.AddInParameterString(command, "AdminEmail", item.AdminEmail);
			db.AddInParameterString(command, "Url", item.Url);
			// This is just for compatibility with other stored procedures.
			db.AddInParameterString(command, "Application", "N/A");
		}

		/// <summary>
		/// Fills the object.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="prefixColumn">The prefix column.</param>
		/// <returns></returns>
		protected override Tenant FillObject(DataSourceDecorator source, string prefixColumn) {
			return new Tenant {
				KeyTenant = source.Field<string>(prefixColumn + "KeyTenant"),
				IsInitialized = source.Field<bool>(prefixColumn + "IsInitialized"),
				AdminEmail = source.Field<string>(prefixColumn + "AdminEmail"),
				Url = source.Field<string>(prefixColumn + "Url")
			};

		}

		/// <summary>
		/// Determines whether the object brokered by this component is tenant aware or not.
		/// </summary>
		/// <returns>
		///   <c>true</c> if the object brokered by this component is tenant aware; otherwise, <c>false</c>.
		/// </returns>
		protected override bool IsTenantAwareObject() {
			return false;
		}

		/// <summary>
		/// Gets the item.
		/// </summary>
		/// <param name="Key">The key.</param>
		/// <returns></returns>
		public override Tenant GetItem(string Key) {
			Tenant dbTenant = base.GetItem(Key);
			return dbTenant;
			/*
			Tenant iisTenant;

			try {
				iisTenant = TenantProvisioningService.GetTenant(Key);
			}
			catch (UnauthorizedAccessException) {
				iisTenant = null;
			}

			Tenant newTenant = MergeTenantData(dbTenant, iisTenant);

			return newTenant;
			 */
		}

		/// <summary>
		/// Gets the list.
		/// </summary>
		/// <param name="Keys">The keys.</param>
		/// <returns></returns>
		public override List<Tenant> GetList(string[] Keys) {
			List<Tenant> dbTenants = base.GetList(Keys);

			/*
			List<Tenant> iisTenants = GetAllNoPaging().Where(t => Keys.Contains(t.KeyTenant)).ToList();
			var mergedTenants = MergeTenantsData(dbTenants, iisTenants);
			return mergedTenants;
			 */
			return dbTenants;
		}
		/*
	/// <summary>
	/// Merges the tenant data.
	/// </summary>
	/// <param name="dbTenant">The db tenant.</param>
	/// <param name="iisTenant">The IIS tenant.</param>
	/// <returns></returns>
	private static Tenant MergeTenantData(Tenant dbTenant, Tenant iisTenant) {
		if (iisTenant == null) return dbTenant;
		if (dbTenant == null) return null;

		var newTenant = new Tenant {
			KeyTenant = dbTenant.KeyTenant,
			Name = iisTenant.Name,
			Url = iisTenant.Url,
			AdminEmail = dbTenant.AdminEmail,
			IsInitialized = dbTenant.IsInitialized,
			HostingContainer = iisTenant.HostingContainer,
			IsIsolated = iisTenant.IsIsolated
		};

		return newTenant;
	}

	
/// <summary>
/// Merges the tenants data.
/// </summary>
/// <param name="dbTenants">The db tenants.</param>
/// <param name="iisTenants">The IIS tenants.</param>
/// <returns></returns>
private static List<Tenant> MergeTenantsData(IEnumerable<Tenant> dbTenants, IEnumerable<Tenant> iisTenants) {

	var tenantPairs = from dbTenant in dbTenants
					  let iisTenant = iisTenants.FirstOrDefault(t => t.KeyTenant == dbTenant.KeyTenant)
					  let merged = MergeTenantData(dbTenant, iisTenant)
					  select merged;

	List<Tenant> mergedTenants = tenantPairs.ToList();

	return mergedTenants;
}

	
protected override List<Tenant> GetAllNoPaging() {
	List<Tenant> dbTenants = base.GetAllNoPaging();
	List<Tenant> iisTenants = TenantProvisioningService.GetTenants();
	List<Tenant> mergedTenants = MergeTenantsData(dbTenants, iisTenants);

	return mergedTenants;
}


		/// <summary>
		/// Gets all generic paging.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="total">The total.</param>
		/// <returns></returns>
		protected override List<Tenant> GetAllGenericPaging(PagingParameter paging, out int total) {
			List<Tenant> dbTenants = base.GetAllGenericPaging(paging, out total);
			List<Tenant> iisTenants = GetAllNoPaging();
			List<Tenant> mergedTenants = MergeTenantsData(dbTenants, iisTenants);

			return mergedTenants;
		}
		*/
		/// <summary>
		/// Creates the specified item.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <returns></returns>
		public override Tenant Create(Tenant item) {
			//if (!item.IsIsolated)
			//	item.Url = Helper.GetFrontendUrl();
			if (base.GetItem(item.KeyTenant) != null) {
				throw new VizeliaException("Tenant already exists.");
			}

			CoerceTenantUrl(item);

			Tenant dbTenant = base.Create(item);
			Tenant iisTenant = TenantProvisioningService.CreateTenant(item);

			try {
				TenantHelper.RunInDifferentTenantContext(item.Name, () => ReportingService.CreateFolder(string.Empty));
			}
			catch (Exception e) {
				TracingService.Write(TraceEntrySeverity.Error, "Could not create tenant Reporting Services folder: " + e);
			}

			//Tenant mergedTenant = MergeTenantData(dbTenant, iisTenant);
			return dbTenant;
		}

		/// <summary>
		/// Updates the specified item.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <returns></returns>
		public override Tenant Update(Tenant item) {
			//if (!item.IsIsolated)
			//	item.Url = Helper.GetFrontendUrl();
			CoerceTenantUrl(item);

			Tenant dbTenant = base.Update(item);

			try {
				Tenant iisTenant = TenantProvisioningService.UpdateTenant(item);
			}
			catch (UnauthorizedAccessException) {
				// Do nothing.
			}

			//Tenant mergedTenant = MergeTenantData(dbTenant, iisTenant);

			return dbTenant;
		}

		/// <summary>
		/// Coerces the tenant URL by stripping the protocol prefix from it and adding a '/' in the end.
		/// </summary>
		/// <param name="item">The item.</param>
		private void CoerceTenantUrl(Tenant item) {
			if (item.Url == null) {
				item.Url = string.Empty;
			}

			string url;
			int indexOfProtocol = item.Url.IndexOf(const_protocol_string, StringComparison.Ordinal);

			if (indexOfProtocol != -1) {
				url = item.Url.Substring(indexOfProtocol + const_protocol_string.Length);
			}
			else {
				url = item.Url;
			}

			url = url.EndsWith("/") ? url : (url + "/");
			item.Url = url;
		}

		/// <summary>
		/// Deletes the specified item.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <returns></returns>
		public override bool Delete(Tenant item) {
			try {
				TenantHelper.RunInDifferentTenantContext(item.Name, () => ReportingService.DeleteItem(string.Empty));
			}
			catch (Exception e) {
				TracingService.Write(TraceEntrySeverity.Error, "Could not delete tenant Reporting Services folder: " + e);
			}

			var iisSuccess = TenantProvisioningService.DeleteTenant(item);
			bool dbSuccess = Delete(item.KeyTenant);
			bool totalSuccess = dbSuccess && iisSuccess;

			return totalSuccess;
		}

		/// <summary>
		/// Gets the item by local id.
		/// </summary>
		/// <param name="localId">The local id.</param>
		/// <returns></returns>
		public override Tenant GetItemByLocalId(string localId) {
			throw new NotImplementedException();
		}

		/// <summary>
		/// Updates the batch.
		/// </summary>
		/// <param name="keys">The keys.</param>
		/// <param name="item">The item.</param>
		/// <returns></returns>
		public override int UpdateBatch(string[] keys, Tenant item) {
			throw new NotImplementedException();
		}

		/// <summary>
		/// Deletes the specified key.
		/// </summary>
		/// <param name="Key">The key.</param>
		/// <returns></returns>
		public override bool Delete(string Key) {
			Tenant item = GetItem(Key);
			DeleteTenant(Key);
			bool iisSuccess = TenantProvisioningService.DeleteTenant(item);
			SchedulerService.DeleteAllJobs(item.Name);

			return iisSuccess;
		}

		/// <summary>
		/// Workflows the delete instance.
		/// </summary>
		/// <param name="instanceId">The instance id.</param>
		/// <returns></returns>
		public override bool WorkflowDeleteInstance(Guid instanceId) {
			throw new NotImplementedException();
		}

		/// <summary>
		/// Deletes all tenant data.
		/// </summary>
		/// <param name="keyTenant">The key tenant.</param>
		private void DeleteTenant(string keyTenant) {
			using (var t = Helper.CreateTransactionScope()) {
				try {
					DbCommand command = db.GetStoredProcCommand(const_proc_deleteitem);
					command.CommandTimeout = 60 * 4; // 4 minutes
					db.AddInParameterString(command, "Application", keyTenant);
					db.ExecuteNonQuery(command);
					t.Complete();
				}
				catch (SqlException ex) {
					throw SqlExceptionWrap(ex);
				}
			}
		}
	}
}
﻿using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.DataLayer.BrokerDB {

	/// <summary>
	/// Database broker for ChartHistoricalAnalysis business entity.
	/// </summary>
	public class ChartHistoricalAnalysisBrokerDB : BaseBrokerDB<ChartHistoricalAnalysis> {

		/// <summary>
		/// The name of the stored procedure that retreives a list of ChartHistoricalAnalysis for a specific chart.
		/// </summary>
		protected const string const_proc_getlistbychart = AppModule.Energy + "_ChartHistoricalAnalysisGetListByKeyChart";

		/// <summary>
		/// The name of the stored procedure that retreives a list of ChartHistoricalAnalysis for a specific chart.
		/// </summary>
		protected const string const_proc_getallpagingbykeychart = AppModule.Energy + "_ChartHistoricalAnalysisGetAllPagingByKeyChart";

		/// <summary>
		/// The name of the stored procedure that saves a list of ChartHistoricalAnalysis for a specific chart.
		/// </summary>
		protected const string const_proc_charthistoricalanalysissave = AppModule.Energy + "_ChartChartHistoricalAnalysisSave";

		/// <summary>
		/// The prefix of stored procedures of that provider. 
		/// </summary>
		protected override string const_proc_prefix {
			get {
				return AppModule.Energy;
			}
		}

		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, ChartHistoricalAnalysis item) {
			db.AddInParameterString(command, "Key", item.KeyChartHistoricalAnalysis);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "LocalId", item.LocalId);
			db.AddInParameterString(command, "Name", item.Name);
			db.AddInParameterString(command, "KeyChart", item.KeyChart);
			db.AddInParameterInt(command, "Frequency", item.Frequency);
			db.AddInParameterString(command, "Interval", item.Interval.ToString());
			db.AddInParameterBool(command, "ExtraChartArea", item.ExtraChartArea);
			db.AddInParameterString(command, "DataSerieType", item.DataSerieType.ToString());
			db.AddInParameterBool(command, "Enabled", item.Enabled);
			db.AddInParameterBool(command, "XAxisVisible", item.XAxisVisible);
			db.AddInParameterBool(command, "SetXAxisMin", item.SetXAxisMin);
			db.AddInParameterBool(command, "SetXAxisMax", item.SetXAxisMax);
			db.AddInParameterBool(command, "IncludeInMainTimeline", item.IncludeInMainTimeline);
		}

		/// <summary>
		/// Populates a ChartHistoricalAnalysis business entity from a DataSourceDecorator.
		/// </summary>
		/// <param name="source">The data source.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated ChartHistoricalAnalysis business entity.</returns>
		protected override ChartHistoricalAnalysis FillObject(DataSourceDecorator source, string prefixColumn) {
			return new ChartHistoricalAnalysis {
				KeyChartHistoricalAnalysis = source.Field<int>(prefixColumn + "KeyChartHistoricalAnalysis").ToString(CultureInfo.InvariantCulture),
				LocalId = source.Field<string>(prefixColumn + "LocalId"),
				KeyChart = source.Field<int>(prefixColumn + "KeyChart").ToString(CultureInfo.InvariantCulture),
				Name = source.Field<string>(prefixColumn + "Name"),
				Frequency = source.Field<int>(prefixColumn + "Frequency"),
				Interval = source.Field<string>(prefixColumn + "Interval").ParseAsEnum<AxisTimeInterval>(),
				ExtraChartArea = source.Field<bool>(prefixColumn + "ExtraChartArea"),
				DataSerieType = source.Field<string>(prefixColumn + "DataSerieType").ParseAsEnum<DataSerieType>(),
				Enabled = source.Field<bool>(prefixColumn + "Enabled"),
				XAxisVisible = source.Field<bool>(prefixColumn + "XAxisVisible"),
				SetXAxisMin = source.Field<bool?>(prefixColumn + "SetXAxisMin") ?? true,
				SetXAxisMax = source.Field<bool?>(prefixColumn + "SetXAxisMax") ?? true,
				IncludeInMainTimeline = source.Field<bool?>(prefixColumn + "IncludeInMainTimeline") ?? false,
			};
		}

		/// <summary>
		/// Get the list of saved ChartHistoricalAnalysis from a Chart
		/// </summary>
		/// <param name="KeyChart">The key of the chart.</param>
		public List<ChartHistoricalAnalysis> GetListByKeyChart(string KeyChart) {
			List<ChartHistoricalAnalysis> results;
			DbCommand command = db.GetStoredProcCommand(const_proc_getlistbychart);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyChart", KeyChart);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}

		/// <summary>
		/// Gets all the ChartHistoricalAnalysis for a specific chart.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyChart">The Key of the chart.</param>
		/// <param name="total">The total number of items.</param>
		/// <returns>The list of items.</returns>
		public List<ChartHistoricalAnalysis> GetAllPagingByKeyChart(PagingParameter paging, string KeyChart, out int total) {
			DbCommand command = db.GetStoredProcCommand(const_proc_getallpagingbykeychart);
			command.CommandTimeout = 600;
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyChart", KeyChart);
			db.AddInParameterInt(command, "start", paging.start);
			db.AddInParameterInt(command, "limit", paging.limit);
			DataSet ds = db.ExecuteDataSet(command);
			if (ds.Tables[0].Rows.Count > 0)
				total = ds.Tables[0].Rows[0].Field<int>("TotalRecord");
			else
				total = 0;
			return FillObjects(ds);
		}

		/// <summary>
		/// Saves the chart ChartHistoricalAnalysis.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="charthistoricalanalysis">The ChartHistoricalAnalysis store.</param>
		/// <returns></returns>
		public List<ChartHistoricalAnalysis> SaveChartChartHistoricalAnalysis(Chart entity, CrudStore<ChartHistoricalAnalysis> charthistoricalanalysis) {
			DbCommand command = db.GetStoredProcCommand(const_proc_charthistoricalanalysissave);
			command.CommandTimeout = 600;
			List<ChartHistoricalAnalysis> results;

			if (charthistoricalanalysis == null || charthistoricalanalysis.IsEmpty())
				return null;
			string inserts = null;
			string deletes = null;


			if (charthistoricalanalysis.destroy != null) {
				deletes = Helper.BuildXml(charthistoricalanalysis.destroy.Select(p => p.KeyChartHistoricalAnalysis).ToArray());
			}

			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyChart", entity.KeyChart);
			db.AddInParameterString(command, "Inserts", inserts);
			db.AddInParameterString(command, "Deletes", deletes);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}
	}
}
﻿using System.Data.Common;
using System.Globalization;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.DataLayer.BrokerDB {

	/// <summary>
	/// Database broker for StruxureWareDataAcquisitionEndpoint business entity.
	/// </summary>
	public class StruxureWareDataAcquisitionEndpointBrokerDB : DataAcquisitionEndpointBrokerDB<StruxureWareDataAcquisitionEndpoint> {
		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, StruxureWareDataAcquisitionEndpoint item) {
			db.AddInParameterString(command, "Key", item.KeyDataAcquisitionEndpoint);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "LocalId", item.LocalId);
			db.AddInParameterString(command, "ServerLocalId", item.ServerLocalId);
			db.AddInParameterString(command, "Name", item.Name);
			db.AddInParameterString(command, "Description", item.Description);
			db.AddInParameterString(command, "KeyDataAcquisitionContainer", item.KeyDataAcquisitionContainer);
			db.AddInParameterString(command, "Path", item.Path);
		}


		/// <summary>
		/// Populates a StruxureWareDataAcquisitionEndpoint business entity from a DataSourceDecorator.
		/// </summary>
		/// <param name="source">The data source.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated StruxureWareDataAcquisitionEndpoint business entity.</returns>
		protected override StruxureWareDataAcquisitionEndpoint FillObject(DataSourceDecorator source, string prefixColumn) {
			return new StruxureWareDataAcquisitionEndpoint {
				KeyDataAcquisitionEndpoint = source.Field<int>(prefixColumn + "KeyDataAcquisitionEndpoint").ToString(CultureInfo.InvariantCulture),
				KeyDataAcquisitionContainer = source.Field<int>(prefixColumn + "KeyDataAcquisitionContainer").ToString(CultureInfo.InvariantCulture),
				LocalId = source.Field<string>(prefixColumn + "LocalId"),
				ServerLocalId = source.Field<string>(prefixColumn + "ServerLocalId"),
				Name = source.Field<string>(prefixColumn + "Name"),
				Description = source.Field<string>(prefixColumn + "Description"),
				Path = source.Field<string>(prefixColumn + "Path"),
				TitleContainer = source.Field<string>(prefixColumn + "TitleContainer")
			};
		}

		//    /// <summary>
		//    /// Saves the StruxureWareDataAcquisitionEndpoint  for a specific container.
		//    /// </summary>
		//    /// <param name="entity">The entity.</param>
		//    /// <param name="endpoints">The endpoints.</param>
		//    /// <returns></returns>
		//    public List<StruxureWareDataAcquisitionEndpoint> SaveStruxureWareDataAcquisitionContainertStruxureWareDataAcquisitionEndpoint(StruxureWareDataAcquisitionContainer entity, CrudStore<StruxureWareDataAcquisitionEndpoint> endpoints)
		//    {
		//        DbCommand command = db.GetStoredProcCommand(const_proc_struxurewaredataacquisitioncontainerstruxurewaredataacquisitionendpoint);
		//        command.CommandTimeout = 600;
		//        List<StruxureWareDataAcquisitionEndpoint> results;

		//        if (endpoints == null || endpoints.IsEmpty())
		//            return null;
		//        string inserts = null;
		//        string deletes = null;

		//        if (endpoints.create != null) {
		//            inserts = Helper.BuildXml(endpoints.create.Select(p => p.KeyDataAcquisitionEndpoint).ToArray());
		//        }

		//        if (endpoints.destroy != null) {
		//            deletes = Helper.BuildXml(endpoints.destroy.Select(p => p.KeyDataAcquisitionEndpoint).ToArray());
		//        }

		//        db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
		//        db.AddInParameterString(command, "KeyDataAcquisitionContainer", entity.KeyDataAcquisitionContainer);
		//        db.AddInParameterString(command, "Inserts", inserts);
		//        db.AddInParameterString(command, "Deletes", deletes);
		//        using (IDataReader reader = db.ExecuteReader(command)) {
		//            results = FillObjects(reader);
		//        }
		//        return results;
		//    }
	}
}
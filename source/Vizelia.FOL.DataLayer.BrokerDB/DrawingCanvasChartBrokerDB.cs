﻿using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.DataLayer.BrokerDB {

	/// <summary>
	/// Database broker for DrawingCanvasChart business entity.
	/// </summary>
	public class DrawingCanvasChartBrokerDB : BaseBrokerDB<DrawingCanvasChart> {

		/// <summary>
		/// The name of the stored procedure that retreives a list of DrawingCanvasChart for a specific DrawingCanvas.
		/// </summary>
		protected const string const_proc_getlistbykeydrawingcanvas = AppModule.Energy + "_DrawingCanvasChartGetListByKeyDrawingCanvas";

		/// <summary>
		/// The name of the stored procedure that retreives a list of DrawingCanvasChart for a specific DrawingCanvas.
		/// </summary>
		protected const string const_proc_getallpagingbykeydrawingcanvas = AppModule.Energy + "_DrawingCanvasChartGetAllPagingByKeyDrawingCanvas";

		/// <summary>
		/// The name of the stored procedure that saves a list of meters for a specific Portal.
		/// </summary>
		protected const string const_proc_drawingcanvassave = AppModule.Energy + "_DrawingCanvasDrawingCanvasChartSave";
		/// <summary>
		/// The prefix of stored procedures of that provider. 
		/// </summary>
		protected override string const_proc_prefix {
			get {
				return AppModule.Energy;
			}
		}

		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, DrawingCanvasChart item) {
			db.AddInParameterString(command, "Key", item.KeyDrawingCanvasChart);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "LocalId", item.LocalId);
			db.AddInParameterString(command, "KeyDrawingCanvas", item.KeyDrawingCanvas);
			db.AddInParameterString(command, "KeyChart", item.KeyChart);
			db.AddInParameterString(command, "Title", item.Title);
			db.AddInParameterInt(command, "Width", item.Width);
			db.AddInParameterInt(command, "Height", item.Height);
			db.AddInParameterInt(command, "X", item.X);
			db.AddInParameterInt(command, "Y", item.Y);
			db.AddInParameterInt(command, "ZIndex", item.ZIndex);
			db.AddInParameterBool(command, "DisableAllSliders", item.DisableAllSliders);
		}


		/// <summary>
		/// Populates a DrawingCanvasChart business entity from a DataSourceDecorator.
		/// </summary>
		/// <param name="source">The data source.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated DrawingCanvasChart business entity.</returns>
		protected override DrawingCanvasChart FillObject(DataSourceDecorator source, string prefixColumn) {
			return new DrawingCanvasChart {
				Application = ContextHelper.ApplicationName,
				KeyDrawingCanvasChart = source.Field<int>(prefixColumn + "KeyDrawingCanvasChart").ToString(CultureInfo.InvariantCulture),
				LocalId = source.Field<string>(prefixColumn + "LocalId"),
				KeyDrawingCanvas = source.Field<int>(prefixColumn + "KeyDrawingCanvas").ToString(CultureInfo.InvariantCulture),
				KeyChart = source.Field<int>(prefixColumn + "KeyChart").ToString(CultureInfo.InvariantCulture),
				Title = source.Field<string>(prefixColumn + "Title"),
				Width = source.Field<int>(prefixColumn + "Width"),
				Height = source.Field<int>(prefixColumn + "Height"),
				X = source.Field<int>(prefixColumn + "X"),
				Y = source.Field<int>(prefixColumn + "Y"),
				ZIndex = source.Field<int>(prefixColumn + "ZIndex"),
				ChartTitle = source.Field<string>(prefixColumn + "ChartTitle"),
				DisableAllSliders = source.Field<bool?>(prefixColumn + "DisableAllSliders") ?? false,
                KeyDynamicDisplay = source.Field<string>(prefixColumn + "KeyDynamicDisplay")
			};
		}

		/// <summary>
		/// Get the list of saved DrawingCanvas Chart from a KeyDrawingCanvas.
		/// </summary>
		/// <param name="KeyDrawingCanvas">The key of the  DrawingCanvas.</param>
		public List<DrawingCanvasChart> GetListByKeyDrawingCanvas(string KeyDrawingCanvas) {
			List<DrawingCanvasChart> results;
			DbCommand command = db.GetStoredProcCommand(const_proc_getlistbykeydrawingcanvas);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyDrawingCanvas", KeyDrawingCanvas);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}

		/// <summary>
		/// Gets a store of DrawingCanvas Chart for a specific DrawingCanvas.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyDrawingCanvas">The Key of the DrawingCanvas.</param>
		/// <param name="total">The total.</param>
		public List<DrawingCanvasChart> GetAllPagingByKeyDrawingCanvas(PagingParameter paging, string KeyDrawingCanvas, out int total) {
			DbCommand command = db.GetStoredProcCommand(const_proc_getallpagingbykeydrawingcanvas);
			command.CommandTimeout = 600;
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyDrawingCanvas", KeyDrawingCanvas);
			db.AddInParameterInt(command, "start", paging.start);
			db.AddInParameterInt(command, "limit", paging.limit);
			DataSet ds = db.ExecuteDataSet(command);
			if (ds.Tables[0].Rows.Count > 0)
				total = ds.Tables[0].Rows[0].Field<int>("TotalRecord");
			else
				total = 0;
			return FillObjects(ds);
		}

		/// <summary>
		/// Saves the DrawingCanvas Charts.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="charts">The DrawingCanvasChart store.</param>
		/// <returns></returns>
		public List<DrawingCanvasChart> SaveDrawingCanvasDrawingCanvasChart(DrawingCanvas entity, CrudStore<DrawingCanvasChart> charts) {
			DbCommand command = db.GetStoredProcCommand(const_proc_drawingcanvassave);
			command.CommandTimeout = 600;
			List<DrawingCanvasChart> results;

			if (charts == null || charts.IsEmpty())
				return null;
			string inserts = null;
			string deletes = null;


			if (charts.destroy != null) {
				deletes = Helper.BuildXml(charts.destroy.Select(p => p.KeyDrawingCanvasChart).ToArray());
			}

			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyDrawingCanvas", entity.KeyDrawingCanvas);
			db.AddInParameterString(command, "Inserts", inserts);
			db.AddInParameterString(command, "Deletes", deletes);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}
	}
}
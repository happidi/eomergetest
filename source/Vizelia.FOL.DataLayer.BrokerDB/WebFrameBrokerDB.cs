﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Web.Security;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Vizelia.FOL.DataLayer.Interfaces;

namespace Vizelia.FOL.DataLayer.BrokerDB {

	/// <summary>
	/// Database broker for WebFrame business entity.
	/// </summary>
	public class WebFrameBrokerDB : BaseBrokerDB<WebFrame> {
		/// <summary>
		/// The prefix of stored procedures of that provider. 
		/// </summary>
		protected override string const_proc_prefix {
			get {
				return AppModule.Energy;
			}
		}


		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, WebFrame item) {
			db.AddInParameterString(command, "Key", item.KeyWebFrame);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "LocalId", item.LocalId);
			db.AddInParameterString(command, "Title", item.Title);
			db.AddInParameterString(command, "Url", item.Url);
			db.AddInParameterString(command, "Body", item.Body);
			db.AddInParameterBool(command, "OpenOnStartup", item.OpenOnStartup);
			db.AddInParameterBool(command, "IsFavorite", item.IsFavorite);
		}


		/// <summary>
		/// Populates a WebFrame business entity from a DataSourceDecorator.
		/// </summary>
		/// <param name="source">The data source.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated WebFrame business entity.</returns>
		protected override WebFrame FillObject(DataSourceDecorator source, string prefixColumn) {
			return new WebFrame {
				KeyWebFrame = source.Field<int>(prefixColumn + "KeyWebFrame").ToString(),
				LocalId = source.Field<string>(prefixColumn + "LocalId"),
				Title = source.Field<string>(prefixColumn + "Title"),
				Url = source.Field<string>(prefixColumn + "Url"),
				Body = source.Field<string>(prefixColumn + "Body"),
				OpenOnStartup = source.Field<bool?>(prefixColumn + "OpenOnStartup") ?? false,
				IsFavorite = source.Field<bool?>(prefixColumn + "IsFavorite") ?? false
			};
		}
	}
}
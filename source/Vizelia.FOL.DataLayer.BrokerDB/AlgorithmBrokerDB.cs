﻿using System.Data.Common;
using System.Web.Security;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.DataLayer.BrokerDB {

	/// <summary>
	/// Database broker for Algorithm business entity.
	/// </summary>
	public class AlgorithmBrokerDB : BaseBrokerDB<Algorithm> {
		/// <summary>
		/// The prefix of stored procedures of that provider. 
		/// </summary>
		protected override string const_proc_prefix {
			get {
				return AppModule.Energy;
			}
		}

		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, Algorithm item) {
			db.AddInParameterString(command, "Key", item.KeyAlgorithm);
			db.AddInParameterString(command, "Application", Membership.ApplicationName);
			db.AddInParameterString(command, "Name", item.Name);
			db.AddInParameterString(command, "Description", item.Description);
			db.AddInParameterString(command, "Code", item.Code);
			db.AddInParameterString(command, "Language", item.Language.ParseAsString());
			db.AddInParameterString(command, "Source", item.Source.ParseAsString());
			db.AddInParameterString(command, "KeyBinary", item.KeyBinary);
			db.AddInParameterString(command, "UniqueId", item.UniqueId);
		}


		/// <summary>
		/// Populates a Algorithm business entity from a DataSourceDecorator.
		/// </summary>
		/// <param name="source">The data source.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated Algorithm business entity.</returns>
		protected override Algorithm FillObject(DataSourceDecorator source, string prefixColumn) {
			return new Algorithm {
				KeyAlgorithm = source.Field<int>(prefixColumn + "KeyAlgorithm").ToString(),
				Name = source.Field<string>(prefixColumn + "Name"),
				Description = source.Field<string>(prefixColumn + "Description"),
				Code = source.Field<string>(prefixColumn + "Code"),
				Language = (source.Field<string>(prefixColumn + "Language") ?? "Python").ParseAsEnum<AlgorithmLanguage>(),
				Source = (source.Field<string>(prefixColumn + "Source")).ParseAsEnum<AlgorithmSource>(),
				KeyBinary = source.Field<string>(prefixColumn + "KeyBinary"),
				UniqueId = source.Field<string>(prefixColumn + "UniqueId")
			};
		}
	}
}
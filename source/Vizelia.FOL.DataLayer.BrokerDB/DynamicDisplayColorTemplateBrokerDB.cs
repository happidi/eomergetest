﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Vizelia.FOL.BusinessEntities;
using System.Data;
using System.Data.Common;
using Vizelia.FOL.DataLayer.Interfaces;
using System.Xml.Linq;
using Vizelia.FOL.Common;
using System.Web.Security;

namespace Vizelia.FOL.DataLayer.BrokerDB {
	/// <summary>
	/// Database broker for DynamicDisplayColorTemplate business entity.
	/// </summary>
	public class DynamicDisplayColorTemplateBrokerDB : BaseBrokerDB<DynamicDisplayColorTemplate> {
		
		/// <summary>
		/// Gets the const_proc_prefix.
		/// </summary>
		/// <value>The const_proc_prefix.</value>
		protected override string const_proc_prefix {
			get {
				return AppModule.Energy;
			}
		}


		/// <summary>
		/// Populates a DynamicDisplayColorTemplate business entity from a DataSourceDecorator.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated DynamicDisplay business entity.</returns>
		protected override DynamicDisplayColorTemplate FillObject(DataSourceDecorator source, string prefixColumn) {
			return new DynamicDisplayColorTemplate {
				KeyDynamicDisplayColorTemplate = source.Field<int>(prefixColumn + "KeyDynamicDisplayColorTemplate").ToString(),
				Name = source.Field<string>(prefixColumn + "Name"),
				LocalId = source.Field<string>(prefixColumn + "LocalId"),
				StartColorR = source.Field<int>(prefixColumn + "StartColorR"),
				StartColorG = source.Field<int>(prefixColumn + "StartColorG"),
				StartColorB = source.Field<int>(prefixColumn + "StartColorB"),
				EndColorR = source.Field<int>(prefixColumn + "EndColorR"),
				EndColorG = source.Field<int>(prefixColumn + "EndColorG"),
				EndColorB = source.Field<int>(prefixColumn + "EndColorB"),
				BorderColorR = source.Field<int>(prefixColumn + "BorderColorR"),
				BorderColorG = source.Field<int>(prefixColumn + "BorderColorG"),
				BorderColorB = source.Field<int>(prefixColumn + "BorderColorB")
			};
		}

		

		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, DynamicDisplayColorTemplate item) {
			db.AddInParameterString(command, "Key", item.KeyDynamicDisplayColorTemplate);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "Name", item.Name);
			db.AddInParameterString(command, "LocalId", item.LocalId);
			db.AddInParameterInt(command, "StartColorR", item.StartColorR);
			db.AddInParameterInt(command, "StartColorG", item.StartColorG);
			db.AddInParameterInt(command, "StartColorB", item.StartColorB);
			db.AddInParameterInt(command, "EndColorR", item.EndColorR);
			db.AddInParameterInt(command, "EndColorG", item.EndColorG);
			db.AddInParameterInt(command, "EndColorB", item.EndColorB);
			db.AddInParameterInt(command, "BorderColorR", item.BorderColorR);
			db.AddInParameterInt(command, "BorderColorG", item.BorderColorG);
			db.AddInParameterInt(command, "BorderColorB", item.BorderColorB);
		}

	
	}
}

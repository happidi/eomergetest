﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Web.Security;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Vizelia.FOL.DataLayer.Interfaces;

namespace Vizelia.FOL.DataLayer.BrokerDB {

	/// <summary>
	/// Database broker for ChartDrillDown business entity.
	/// </summary>
	public class ChartDrillDownBrokerDB : BaseBrokerDB<ChartDrillDown> {

		/// <summary>
		/// The name of the stored procedure that retreives the existing ChartDrillDown for a specific Chart.
		/// </summary>
		protected const string const_proc_getitembykeychart = AppModule.Energy + "_ChartDrillDownGetItemByKeyChart";

		/// <summary>
		/// The prefix of stored procedures of that provider. 
		/// </summary>
		protected override string const_proc_prefix {
			get {
				return AppModule.Energy;
			}
		}

		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, ChartDrillDown item) {
			db.AddInParameterString(command, "Key", item.KeyChartDrillDown);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyChart", item.KeyChart);
			db.AddInParameterBool(command, "Enabled", item.Enabled);
			db.AddInParameterDateTime(command, "StartDate", item.StartDate);
			db.AddInParameterDateTime(command, "EndDate", item.EndDate);
			db.AddInParameterInt(command, "ClassificationLevel", item.ClassificationLevel);
			db.AddInParameterString(command, "Localisation", item.Localisation.ParseAsString());
			db.AddInParameterInt(command, "LocalisationSiteLevel", item.LocalisationSiteLevel);
			db.AddInParameterString(command, "TimeInterval", item.TimeInterval.ParseAsString());
			db.AddInParameterString(command, "KeyLocation", item.KeyLocation);
			db.AddInParameterString(command, "KeyClassificationItem", item.KeyClassificationItem);
		}


		/// <summary>
		/// Populates a Chart business entity from a DataSourceDecorator.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated Chart business entity.</returns>
		protected override ChartDrillDown FillObject(DataSourceDecorator source, string prefixColumn) {
			return new ChartDrillDown {
				KeyChartDrillDown = source.Field<int>(prefixColumn + "KeyChartDrillDown").ToString(),
				KeyChart = source.Field<int>(prefixColumn + "KeyChart").ToString(),
				StartDate = source.Field<DateTime?>(prefixColumn + "StartDate"),
				EndDate = source.Field<DateTime?>(prefixColumn + "EndDate"),
				TimeInterval = source.Field<string>(prefixColumn + "TimeInterval").ParseAsEnum<AxisTimeInterval>(),
				Localisation = source.Field<string>(prefixColumn + "Localisation").ParseAsEnum<ChartLocalisation>(),
				LocalisationSiteLevel = source.Field<int>(prefixColumn + "LocalisationSiteLevel"),
				ClassificationLevel = source.Field<int>(prefixColumn + "ClassificationLevel"),
				Enabled = source.Field<bool?>(prefixColumn + "Enabled") ?? false,
				KeyLocation = source.Field<string>(prefixColumn + "KeyLocation"),
				KeyClassificationItem = source.Field<string>(prefixColumn + "KeyClassificationItem")
			};
		}


		/// <summary>
		/// Get the ChartDrillDown entity of a Chart
		/// </summary>
		/// <param name="KeyChart">The key of the chart.</param>
		public ChartDrillDown GetItemByKeyChart(string KeyChart) {
			ChartDrillDown item = null;

			DbCommand command = db.GetStoredProcCommand(const_proc_getitembykeychart);
			db.AddInParameterString(command, "KeyChart", KeyChart);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);

			using (IDataReader reader = db.ExecuteReader(command)) {
				if (reader.Read()) {
					item = FillObject(reader);
				}
			}
			return item;
		}

	}
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.DataLayer.BrokerDB {
	/// <summary>
	/// Base data layer class for business entity broker.
	/// </summary>
	/// <typeparam name="T">The business entity.</typeparam>
	public abstract class BaseBrokerDBGeneric<T> : MarshalByRefObject
		where T : BaseBusinessEntity, new() {
		#region protected variables

		/// <summary>
		/// The name of the id attribute of the business entity.
		/// </summary>
		protected string idProperty;

		/// <summary>
		/// The name of the business entity.
		/// </summary>
		protected virtual string BusinessEntityName {
			// the name of the business entity.
			get { return typeof(T).Name; }
		}

		/// <summary>
		/// True if the entity is localized;
		/// </summary>
		protected virtual bool IsLocalized {
			get { return false; }
		}

		/// <summary>
		/// Gets a value indicating whether this should be processed for hierarachical objects.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if this should be processed for hierarachical objects; otherwise, <c>false</c>.
		/// </value>
		public bool IsHierarchical { get; set; }

		#endregion

		#region Constructor

		/// <summary>
		/// Public ctor.
		/// </summary>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
		protected BaseBrokerDBGeneric() {
			// we initiate the idProperty of the business entity, so we can use it when saving data.
			idProperty = Helper.GetPropertyNameId(typeof(T));
			if (String.IsNullOrEmpty(idProperty))
				throw new ArgumentException("Could not retreive property id for business entity " + BusinessEntityName);
		}

		#endregion

		#region Read methods

		/// <summary>
		/// Gets an item.
		/// </summary>
		/// <param name="Key">The key of the item.</param>
		/// <returns>The item.</returns>
		public abstract T GetItem(string Key);

		/// <summary>
		/// Gets an item by its LocalId.
		/// </summary>
		/// <param name="localId">The LocalId of the item.</param>
		/// <returns>The item.</returns>
		public abstract T GetItemByLocalId(string localId);

		/// <summary>
		/// Gets children for an item.
		/// </summary>
		/// <param name="Key">The Key of the parent item.</param>
		/// <returns>The list of children.</returns>
		public virtual List<T> GetChildren(string Key) {
			return null;
		}

		/// <summary>
		/// Gets a list of items.
		/// </summary>
		/// <param name="Keys">An array of keys.</param>
		/// <returns>The list of items corresponding to the provided keys.</returns>
		public abstract List<T> GetList(string[] Keys);



		/// <summary>
		/// Gets all the items.
		/// Depending on the parameters, paging AND filter will occur in Memory or in DataBase.
		/// </summary>
		/// <param name="paging">Paging parameters</param>
		/// <param name="location">Indicates if paging AND filter occurs in Memory or in DataBase.</param>
		/// <param name="total">The total number of items.</param>
		/// <param name="useSecurableFilter">if set to <c>true</c> we use the securable filter, to return only the items with a Creator equals to the current user.</param>
		/// <returns>
		/// The list of items.
		/// </returns>
		public List<T> GetAll(PagingParameter paging, PagingLocation location, out int total, bool useSecurableFilter = true) {
			try {
				List<T> results;
				// When paging.limit == -1 or any negative value, this indicates we just want to return an empty result.
				if (paging != null && paging.limit < 0) {
					total = 0;
					return new List<T>();
				}
				if (paging == null) {
					paging = new PagingParameter();
				}
				paging.ExtractInnerJoinFilters();
				if (useSecurableFilter) {
					BuildSecurityFilterParams(paging);
				}
				if (location == PagingLocation.Memory) {
					results = GetAllCache(this.UseCache);
					results = ExecuteFilter(results, paging);
					total = results.Count;

					if (paging.innerjoinfilters != null && paging.innerjoinfilters.Count > 0) {
						var queryjoin = from p in results select p;
						foreach (InnerJoinFilter filter in paging.innerjoinfilters) {
							string field = filter.field;
							List<string> values = filter.data.values;
							queryjoin = from p in queryjoin
										join k in values
											on p.GetPropertyValueString(field, false, false) equals k
										select p;
						}
						results = queryjoin.ToList();
						total = results.Count;
					}

					results = results.Rank(paging.sort, paging.dir, paging.start, paging.limit);
				}
				else {
					results = GetAllGenericPaging(paging, out total);
				}
				return this.PopulateReference(results);
			}
			catch (SqlException) {
				throw new ApplicationException("The database could not perform the query");
			}
		}

		/// <summary>
		/// Populates references from a list of business entities.
		/// </summary>
		/// <param name="results"></param>
		/// <returns></returns>
		public List<T> PopulateReference(List<T> results) {
			return results;
		}

		/// <summary>
		/// Gets all the items either from cache or from a call to database.
		/// In the case we want an entity to use cache we should 
		/// 1 - Override UseCache property of the entityBrokerDB.
		/// 2 - Add a [Serializable] attribute to the entity class.
		/// </summary>
		/// <param name="useCache">if set to <c>true</c> and cache exists the use cache.</param>
		/// <returns></returns>
		private List<T> GetAllCache(bool useCache) {
			List<T> results;
			string cacheKey = BuildCacheKey();
			if (useCache) {
				object data = CacheService.GetData(cacheKey);
				if (data != null) {
					try {
						results = (List<T>)data;
					}
					// in case of an error we get results from database.
					catch {
						results = GetAllNoPaging();
					}
				}
				else {
					results = GetAllNoPaging();
				}
			}
			else {
				results = GetAllNoPaging();
			}
			if (useCache)
				CacheService.Add(cacheKey, results, CacheDuration);
			return results;
		}

		/// <summary>
		/// Gets all the items from database into memory. 
		/// Uses const_proc_getall proc to retreive the items.
		/// </summary>
		/// <returns>The list of items.</returns>
		protected abstract List<T> GetAllNoPaging();

		/// <summary>
		/// Gets all the items from the server.
		/// Uses the generic paging proc to retreive the items. The name of this proc can be overrided if necessary.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="total">The total number of items.</param>
		/// <returns>The list of items.</returns>
		protected abstract List<T> GetAllGenericPaging(PagingParameter paging, out int total);

		/// <summary>
		/// Builds the UDF parameter JoinFilterType from a list of InnerJoinFilter so it can be passed to Core_GenericPaging
		/// </summary>
		/// <param name="filters"></param>
		/// <returns></returns>
		protected DataTable BuildJoinFilterType(List<InnerJoinFilter> filters) {
			DataTable table = new DataTable();
			//DataColumn dcID = new DataColumn("id", typeof(int)); 
			//dcID.AutoIncrement = true; 
			//dcID.AutoIncrementSeed = 1; 
			//dcID.AutoIncrementStep = 1;                   
			//table.Columns.Add(dcID);
			table.Columns.Add("field", typeof(string));
			table.Columns.Add("values", typeof(string));
			table.Columns.Add("joinType", typeof(string));
			foreach (InnerJoinFilter filter in filters) {
				if (filter.data.values != null && filter.data.values.Count > 0) {
					DataRow dr = table.NewRow();
					dr["field"] = filter.field;
					dr["values"] = Helper.BuildXml(filter.data.values.ToArray());
					dr["joinType"] = filter.joinType;
					table.Rows.Add(dr);
				}
			}
			return table;
		}

		#endregion

		#region Create methods

		/// <summary>
		/// Creates (inserts) the provided item to database.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns>The item created.</returns>
		public abstract T Create(T item);

		#endregion

		#region Update methods

		/// <summary>
		/// Updates the provided item to database.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns>The item updated.</returns>
		public abstract T Update(T item);

		/// <summary>
		/// Update batch (mass update) a series of items based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public abstract int UpdateBatch(string[] keys, T item);

		#endregion

		#region Delete methods

		/// <summary>
		/// Deletes the provided item from database.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns>True if successfull, false otherwise.</returns>
		public abstract bool Delete(T item);

		/// <summary>
		/// Deletes the provided item from database.
		/// </summary>
		/// <param name="Key">The key of the item to delete.</param>
		/// <returns>True if successfull, false otherwise.</returns>
		public abstract bool Delete(string Key);

		/// <summary>
		/// Deletes a workflow instance from persistence and tracking database.
		/// </summary>
		/// <param name="instanceId"></param>
		/// <returns></returns>
		public abstract bool WorkflowDeleteInstance(Guid instanceId);


		#endregion

		#region Cache methods

		/// <summary>
		/// Gets a value indicating whether the broker should use cache.
		/// </summary>
		/// <value><c>true</c> if use cache; otherwise, <c>false</c>.</value>
		public virtual bool UseCache {
			get { return false; }
		}

		/// <summary>
		/// The cache duration in minutes.
		/// </summary>
		public virtual int CacheDuration {
			get { return 20; }
		}

		/// <summary>
		/// Buillds the cache key that should be used to retreive the items.
		/// </summary>
		/// <returns></returns>
		public virtual string BuildCacheKey() {
			return ContextHelper.ApplicationName + '_' + typeof(T).FullName;
		}


		#endregion

		#region Helper methods

		/// <summary>
		/// Filters (in memory) the list of result with provided paging parameters.
		/// Expects paging.query + paging.filter and paging.filters.
		/// </summary>
		/// <param name="results">The list of business entities to filter.</param>
		/// <param name="paging">The paging parameters.</param>
		/// <returns>The filtered list of business entities.</returns>
		private static List<T> ExecuteFilter(List<T> results, PagingParameter paging) {
			return results.AsQueryable().Filter(paging).ToList();
		}

		/// <summary>
		/// Builds a SQL where clause given colums names (filter) and search value (query).
		/// </summary>
		/// <param name="filter">A string representing the columns names (or composition) to search in.
		/// <example>
		/// filter = "FirstName LastName FirstName+LastName";
		/// </example>
		/// </param>
		/// <param name="query">The value that should be searched in filter.</param>
		/// <returns>The SQL where clause.</returns>
		protected static string BuildWhereClause(string filter, string query) {
			var txtFilter = filter.Replace(".", "_").SafeSqlLiteral();
			string[] filters = txtFilter.Split(' ');
			if (filters.Any(p => p.WordCount() > 1))
				throw new ApplicationException("The parameters of the query are incorrect");
			var filterQuery = from f in filters
							  select String.Format(" [{0}] like \'%{1}%\' COLLATE French_CI_AI", f.Replace("+", "] + \' \' + ["), query.SafeSqlLiteral());
			return String.Join(" or ", filterQuery.ToArray());
		}

		/// <summary>
		/// Builds a SQL where clause given a GridFilter list.
		/// </summary>
		/// <param name="filters">The grid fitler list.</param>
		/// <returns></returns>
		protected static string BuildWhereClause(List<GridFilter> filters) {
			// we ignore filters with no value.
			var queryFilter = from p in filters
							  where !String.IsNullOrEmpty(p.data.value)
							  select p;

			StringBuilder strBuilder = new StringBuilder("1 = 1");

			foreach (GridFilter filter in queryFilter) {
				// The temporary variable in the loop is required to avoid the outer variable trap, 
				// where the same variable is captured for each iteration of the foreach loop.
				// for more info see : http://www.albahari.com/nutshell/predicatebuilder.aspx
				string value = filter.data.value.SafeSqlLiteral();
				filter.field = filter.field.SafeSqlLiteral();
				// we check here if the field is a simple field or more likely an expression (containing a space) to decide if bracket should be applied.
				string field = filter.field.Contains(" ") ? filter.field : String.Format("[{0}]", filter.field);
				field = field.Replace(".", "_");
				const string const_case_sensitivity = " COLLATE French_CI_AI";
				switch (filter.data.type) {
					case "string":
						switch (filter.data.comparison) {
							case "start":
								strBuilder.Append(String.Format(" and {0} like \'%{1}\'" + const_case_sensitivity, field, value));
								break;

							case "end":
								strBuilder.Append(String.Format(" and {0} like \'{1}%\'" + const_case_sensitivity, field, value));
								break;

							case "eq":
								strBuilder.Append(String.Format(" and {0} = \'{1}\'" + const_case_sensitivity, field, value));
								break;

							case "ne":
								strBuilder.Append(String.Format(" and {0} <> \'{1}\'" + const_case_sensitivity, field, value));
								break;

							default:
								strBuilder.Append(String.Format(" and {0} like \'%{1}%\'" + const_case_sensitivity, field, value));
								break;
						}
						break;

					case "boolean":
						strBuilder.Append(String.Format(" and {0} = {1}", field, bool.Parse(value) ? 1 : 0));
						break;

					case "date":

						var strDate = DateTime.Parse(value, CultureInfo.InvariantCulture).ToUniversalTime().ToString("s");
						var strDateNextDay = DateTime.Parse(value, CultureInfo.InvariantCulture).ToUniversalTime().AddDays(1).ToString("s");
						switch (filter.data.comparison) {

							case "eq":
								strBuilder.Append(String.Format(" and {0} BETWEEN \'{1}\'  and \'{2}\'", field, strDate, strDateNextDay));

								break;
							case "ne":
								strBuilder.Append(String.Format(" and {0} <> \'{1}\'", field, strDate));
								break;
							case "lt":
								strBuilder.Append(String.Format(" and {0} <= \'{1}\'", field, strDate));
								break;
							case "gt":
								strBuilder.Append(String.Format(" and {0} >= \'{1}\'", field, strDate));
								break;

							default:
								strBuilder.Append(String.Format(" and {0} = \'{1}\'", field, strDate));
								break;
						}
						break;

					case "numeric":
						switch (filter.data.comparison) {
							case "eq":
								strBuilder.Append(String.Format(" and {0} = \'{1}\'", field, float.Parse(value).ToString("f3", CultureInfo.InvariantCulture)));
								break;
							case "ne":
								strBuilder.Append(String.Format(" and {0} <> \'{1}\'", field, float.Parse(value).ToString("f3", CultureInfo.InvariantCulture)));
								break;
							case "lt":
								strBuilder.Append(String.Format(" and {0} <= \'{1}\'", field, float.Parse(value).ToString("f3", CultureInfo.InvariantCulture)));
								break;
							case "gt":
								strBuilder.Append(String.Format(" and {0} >= \'{1}\'", field, float.Parse(value).ToString("f3", CultureInfo.InvariantCulture)));
								break;
							default:
								strBuilder.Append(String.Format(" and {0} = \'{1}\'", field, float.Parse(value).ToString("f3", CultureInfo.InvariantCulture)));
								break;
						}
						break;

					case "int":
						switch (filter.data.comparison) {
							case "eq":
								strBuilder.Append(String.Format(" and {0} = \'{1}\'", field, ((int)float.Parse(value)).ToString()));
								break;
							case "ne":
								strBuilder.Append(String.Format(" and {0} <> \'{1}\'", field, ((int)float.Parse(value)).ToString()));
								break;
							case "lt":
								strBuilder.Append(String.Format(" and {0} <= \'{1}\'", field, ((int)float.Parse(value)).ToString()));
								break;
							case "gt":
								strBuilder.Append(String.Format(" and {0} >= \'{1}\'", field, ((int)float.Parse(value)).ToString()));
								break;
							default:
								strBuilder.Append(String.Format(" and {0} = \'{1}\'", field, ((int)float.Parse(value)).ToString()));
								break;
						}
						break;

					case "list":
						var list = filter.data.value.Split(',');
						if (list.Count() > 0) {
							var s = from p in list
									select "\'" + p.Replace("'", "''") + "\'";
							//String.Join(",", s.ToArray());
							strBuilder.Append(String.Format(" and {0} in ({1}" + const_case_sensitivity + ")", field, String.Join(const_case_sensitivity + ",", s.ToArray())));
						}
						break;
				}
			}
			return strBuilder.ToString();
		}

		/// <summary>
		/// Sends the audit action.
		/// </summary>
		/// <param name="parametersDictionary">The parameters dictionary.</param>
		public abstract void SaveAuditAction(Dictionary<string, object> parametersDictionary);

		#endregion

		#region   Filter (Security) methods

		/// <summary>
		/// we loop on the entity,
		/// for each Property that has a FilterType Attribute
		/// we go and add a paging filter
		/// </summary>
		/// <param name="paging">The paging.</param>
		protected void BuildSecurityFilterParams(PagingParameter paging) {
			// TODO : In memory cache for reflection
			var allPropertiesOnT = typeof(T).GetProperties();
			foreach (PropertyInfo pinfo in allPropertiesOnT) {
				object[] filterTypeAttributes = pinfo.GetCustomAttributes(typeof(FilterTypeAttribute), false);
				foreach (object obj in filterTypeAttributes) {
					FilterTypeAttribute filterTypeAttribute = obj as FilterTypeAttribute;
					if (filterTypeAttribute == null)
						continue;

					List<Filter> xmlFilters =
						FilterHelper.GetFilter(filterTypeAttribute.FilterBusinessEntityType).Where(
							filter => filter.Type != FilterType.Ascendant).ToList();
					if (xmlFilters.Count == 0 && !filterTypeAttribute.ShowAllIfEmpty) {
						xmlFilters.Add(new Filter(" "));
					}
					//if the paging filter already exist on the same field, we dont need to add the security filter in order to speed up the process
					if (paging.filter == pinfo.Name) {
						xmlFilters.Clear();
						//TODO : Check that the paging.query is contained in the security list and otherwise return a security exception.
					}
					else {
						//we look for existing filters on the same attribute
						var existingFilters = new List<InnerJoinFilter>();

						if (paging.innerjoinfilters != null)
							existingFilters = paging.innerjoinfilters.Where(filter => filter.field == pinfo.Name).ToList();

						//if there are none, we can add the security ones
						List<string> allowedKeys = xmlFilters.Select(f => f.Key).Distinct().ToList();

						if (existingFilters.Count == 0) {
							paging.AddInnerJoinFilter(pinfo.Name, allowedKeys, "innerjoin");
						}
						else {
							//otherwise we only keep in the existing filters, the one that are in the security filter, and " ".
							allowedKeys.Add(" ");

							foreach (var filter in existingFilters) {
								filter.data.values = filter.data.values.Intersect(allowedKeys).ToList();

								if (filter.data.values.Count == 0 && !filterTypeAttribute.ShowAllIfEmpty) {
									filter.data.values.Add(" ");
								}
							}
						}
					}
				}
			}
		}

		/// <summary>
		/// Add the key of the created item to the Filter if the entity is Securable.
		/// </summary>
		/// <typeparam name="TFilter">the type of the Filter</typeparam>
		/// <param name="key">The key.</param>
		/// <param name="item">The item.</param>
		protected void AddCreatedItemToSecurityFilter<TFilter>(string key, TFilter item) {
			if (item is ILocation) {
				var currentFilter = FilterHelper.GetFilter<Location>();
				if (currentFilter.Count > 0) {
					FilterHelper.AddFilter<Location>(new Filter(key));
				}
			}
			else if (item is ClassificationItem) {
				var currentFilter = FilterHelper.GetFilter<ClassificationItem>();
				if (currentFilter.Count > 0) {
					FilterHelper.AddFilter<ClassificationItem>(new Filter(key));
				}
			}
			else if (Helper.IsSecurableType(typeof(TFilter))) {
				// we have to check here that the object created is for the current actor. In case of a copy (ie portaltemplate) we are creating object for other users...
				if (((ISecurableObject)item).Creator == Helper.GetCurrentActor()) {
					FilterHelper.AddFilter<TFilter>(new Filter(key));
				}
			}
		}

		#endregion

	}
}

﻿using System.Data.Common;
using System.Web.Security;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.DataLayer.BrokerDB {
	/// <summary>
	/// Database broker for Furniture business entity.
	/// </summary>
	public class FurnitureBrokerDB : BaseBrokerDB<Furniture> {
		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, Furniture item) {
			db.AddInParameterString(command, "Key", item.KeyFurniture);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "LocalId", item.LocalId);
			db.AddInParameterString(command, "GlobalId", item.GlobalId);
			db.AddInParameterString(command, "KeyLocation", item.KeyLocation);
			db.AddInParameterString(command, "ObjectType", item.ObjectType);
			db.AddInParameterString(command, "Name", item.Name);
			db.AddInParameterString(command, "Description", item.Description);
			db.AddInParameterString(command, "Tag", item.Tag);
			db.AddInParameterString(command, "KeyClassificationItem", item.KeyClassificationItem);
            db.AddInParameterBool(command, "IsMappingInProgress", base.IsAsynchronousSave);
		}

		/// <summary>
		/// Populates a Furniture business entity from a IDataReader.
		/// </summary>
		/// <param name="source">The reader.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated Furniture business entity.</returns>
		protected override Furniture FillObject(DataSourceDecorator source, string prefixColumn) {
			var furniture = new Furniture {
				KeyFurniture = source.Field<string>(prefixColumn + "KeyFurniture"),
				LocalId = source.Field<string>(prefixColumn + "LocalId"),
				GlobalId = source.Field<string>(prefixColumn + "GlobalId"),
				KeyLocation = source.Field<string>(prefixColumn + "KeyLocation"),
				LocationLevel = source.Field<int?>("LocationLevel"),
				LocationLongPath = source.Field<string>(prefixColumn + "LocationLongPath"),
				LocationShortPath = source.Field<string>(prefixColumn + "LocationShortPath"),
				LocationName = source.Field<string>(prefixColumn + "LocationName"),
				LocationTypeName = source.Field<string>(prefixColumn + "LocationTypeName").ParseAsEnum<HierarchySpatialTypeName>(),
				ObjectType = source.Field<string>(prefixColumn + "ObjectType"),
				Name = source.Field<string>(prefixColumn + "Name"),
				Description = source.Field<string>(prefixColumn + "Description"),
				Tag = source.Field<string>(prefixColumn + "Tag"),
				KeyClassificationItem = source.Field<string>(prefixColumn + "KeyClassificationItem"),
				KeyClassificationItemPath = source.Field<string>(prefixColumn + "KeyClassificationItemPath"),
				ClassificationItemLongPath = source.Field<string>(prefixColumn + "ClassificationItemLongPath"),
				ClassificationItemLocalIdPath = source.Field<string>(prefixColumn + "ClassificationItemLocalIdPath"),
				ClassificationItemTitle = source.Field<string>(prefixColumn + "ClassificationItemTitle"),
				ClassificationItemLevel = source.Field<int>(prefixColumn + "ClassificationItemLevel")
			};
			return furniture;
		}


        /// <summary>
        /// Builds the additional delete params.
        /// </summary>
        /// <param name="command">The command.</param>
        protected override void BuildAdditionalDeleteParams(DbCommand command) {
            db.AddInParameterBool(command, "IsMappingInProgress", base.IsAsynchronousSave);
        }
	}
}
﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Web.Security;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Vizelia.FOL.DataLayer.Interfaces;

namespace Vizelia.FOL.DataLayer.BrokerDB {

	/// <summary>
	/// Database broker for WorkflowInstanceDefinition business entity.
	/// </summary>
	public class WorkflowInstanceDefinitionBrokerDB : BaseBrokerDB<WorkflowInstanceDefinition> {

		/// <summary>
		/// The name of the stored procedure that retreives the item by its instanceId.
		/// </summary>
		protected const string const_proc_getitembyinstanceid = AppModule.Core + "_WorkflowInstanceDefinitionGetItemByInstanceId";

		/// <summary>
		/// The name of the stored procedure that detachs an instance from its workflow.
		/// </summary>
		protected const string const_proc_workflowdetachinstance = AppModule.Core + "_WorkflowDetachInstance";

		/// <summary>
		/// The name of the stored procedure that retreives the list of pending workflows.
		/// </summary>
		protected const string const_proc_workflowgetlistpending = AppModule.Core + "_WorkflowGetListPending";

		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, WorkflowInstanceDefinition item) {
			db.AddInParameterString(command, "Key", item.KeyWorkflowInstanceDefinition);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterGuid(command, "InstanceId", item.InstanceId);
			db.AddInParameterString(command, "InitialState", item.InitialState);
			db.AddInParameterString(command, "Xaml", item.Xaml);
			db.AddInParameterString(command, "XamlDesign", item.XamlDesign);
			db.AddInParameterBool(command, "HasWorkflow", item.HasWorkflow);
			db.AddInParameterString(command, "WorkflowAssemblyQualifiedName", item.WorkflowAssemblyQualifiedName);
		}


		/// <summary>
		/// Populates a WorkflowInstanceDefinition business entity from a DataSourceDecorator.
		/// </summary>
		/// <param name="source">The data source.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated WorkflowInstanceDefinition business entity.</returns>
		protected override WorkflowInstanceDefinition FillObject(DataSourceDecorator source, string prefixColumn) {
			return new WorkflowInstanceDefinition {
				KeyWorkflowInstanceDefinition = source.Field<int>(prefixColumn + "KeyWorkflowInstanceDefinition").ToString(),
				InstanceId = source.Field<Guid>(prefixColumn + "InstanceId"),
				InitialState = source.Field<string>(prefixColumn + "InitialState"),
				Xaml = source.Field<string>(prefixColumn + "Xaml"),
				XamlDesign = source.Field<string>(prefixColumn + "XamlDesign"),
				HasWorkflow = source.Field<bool>(prefixColumn + "HasWorkflow"),
				WorkflowAssemblyQualifiedName = source.Field<string>(prefixColumn + "WorkflowAssemblyQualifiedName")
			};
		}

		/// <summary>
		/// Gets an item by its instanceId.
		/// </summary>
		/// <param name="instanceId">The instanceId of the item.</param>
		/// <returns>The item.</returns>
		public WorkflowInstanceDefinition GetItemByInstanceId(Guid instanceId) {
			WorkflowInstanceDefinition item = default(WorkflowInstanceDefinition);
			DbCommand command = db.GetStoredProcCommand(const_proc_getitembyinstanceid);
			db.AddInParameterGuid(command, "InstanceId", instanceId);
			using (IDataReader reader = db.ExecuteReader(command)) {
				if (reader.Read()) {
					item = FillObject(reader);
				}
			}
			return item;
		}


		/// <summary>
		/// Detaches the instance from workflow.
		/// </summary>
		/// <param name="instanceId">The instance id.</param>
		public void WorkflowDetachInstance(Guid instanceId) {
			DbCommand command = db.GetStoredProcCommand(const_proc_workflowdetachinstance);
			db.AddInParameterGuid(command, "InstanceId", instanceId);
			db.ExecuteNonQuery(command);
		}


		/// <summary>
		/// Gets the list workflows pending.
		/// </summary>
		/// <returns></returns>
		public List<Guid> GetListPendingWorkflows() {
			List<Guid> retVal = new List<Guid>();
			DbCommand command = db.GetStoredProcCommand(const_proc_workflowgetlistpending);
			using (var decorator = new DataReaderDecorator(db.ExecuteReader(command))) {
				while (decorator.Read()) {
					retVal.Add(decorator.Field<Guid>("Id"));
				}
			}
			return retVal;
		}
	}
}
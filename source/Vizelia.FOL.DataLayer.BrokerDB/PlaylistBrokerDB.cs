﻿using Vizelia.FOL.BusinessEntities;
using System.Data.Common;
using System.Web.Security;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.DataLayer.BrokerDB {
	/// <summary>
	/// Database broker for Playlist business entity.
	/// </summary>
	public class PlaylistBrokerDB : BaseBrokerDB<Playlist> {
		
		/// <summary>
		/// Gets the const_proc_prefix.
		/// </summary>
		/// <value>The const_proc_prefix.</value>
		protected override string const_proc_prefix {
			get {
				return AppModule.Energy;
			}
		}


		/// <summary>
		/// Populates a Playlist business entity from a DataSourceDecorator.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated Playlist business entity.</returns>
		protected override Playlist FillObject(DataSourceDecorator source, string prefixColumn) {
			return new Playlist {
                Application = ContextHelper.ApplicationName,
				KeyPlaylist = source.Field<int>(prefixColumn + "KeyPlaylist").ToString(),
				LocalId = source.Field<string>(prefixColumn + "LocalId"),
				Name = source.Field<string>(prefixColumn + "Name"),
				KeyLocalizationCulture =source.Field<string>(prefixColumn + "KeyLocalizationCulture"),
				Width  = source.Field<int>(prefixColumn + "Width"),
				Height = source.Field<int>(prefixColumn + "Height"),
				VirtualFilePath = source.Field<string>(prefixColumn + "VirtualFilePath")
			};
		}

		

		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, Playlist item) {
			db.AddInParameterString(command, "Key", item.KeyPlaylist);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "LocalId", item.LocalId);
			db.AddInParameterString(command, "KeyLocalizationCulture", item.KeyLocalizationCulture);
			db.AddInParameterString(command, "Name", item.Name);
			db.AddInParameterInt(command, "Width", item.Width);
			db.AddInParameterInt(command, "Height", item.Height);
		}

	
	}
}

﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.DataLayer.BrokerDB {
	/// <summary>
	/// Database broker for CalendarEvent business entity.
	/// </summary>
	public class CalendarEventBrokerDB : BaseBrokerDB<CalendarEvent>
	{
		/// <summary>
		/// Builds the save params.
		/// </summary>
		/// <param name="command">The command.</param>
		/// <param name="item">The item.</param>
		protected override void BuildSaveParams(DbCommand command, CalendarEvent item)
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Fills the object.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="prefixColumn">The prefix column.</param>
		/// <returns></returns>
		protected override CalendarEvent FillObject(DataSourceDecorator source, string prefixColumn)
		{
			throw new NotImplementedException();
		}
	}
}

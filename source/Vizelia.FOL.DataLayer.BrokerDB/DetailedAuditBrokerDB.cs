﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.DataLayer.BrokerDB {
	/// <summary>
	/// The Broker DB for entity DetailedAudit.
	/// </summary>
	public class DetailedAuditBrokerDB : BaseAuditBrokerDB<DetailedAudit> {
		/// <summary>
		/// Fills the object.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="prefixColumn">The prefix column.</param>
		/// <returns></returns>
		protected override DetailedAudit FillObject(DataSourceDecorator source, string prefixColumn) {
			return new DetailedAudit {
				AuditId = source.Field<long>("AuditId").ToString(CultureInfo.InvariantCulture),
				ModifiedBy = source.Field<string>("ModifiedBy"),
				ChangeTime = source.Field<DateTime>("ChangeTime"),
				ChangedFields = GetChangedFields(source)
			};
		}

		private List<AuditValues> GetChangedFields(DataSourceDecorator source) {
			var retVal = new List<AuditValues>();
			var changedFields = source.Field<string>("ChangedFields").Split(',');
			var i = 0;

			var columnName = string.Format("FieldName{0}", i);
			while (source.FieldExists(columnName)) {
				var fieldName = source.Field<string>(columnName);
				if (!string.IsNullOrWhiteSpace(fieldName) && changedFields.Contains(fieldName)) {
					if (EntityProperties.Any(x => x.Key == fieldName)) {
						var auditValues = new AuditValues {
							FieldName = EntityProperties.First(x => x.Key == fieldName).Value,
							OldValue = source.Field<string>(string.Format("OldValue{0}", i)),
							NewValue = source.Field<string>(string.Format("NewValue{0}", i))
						};

						if (fieldName == "AcquisitionDateTime") {
							auditValues.OldValue = ParseDateValue(auditValues.OldValue as string);
							auditValues.NewValue = ParseDateValue(auditValues.NewValue as string);
						}

						retVal.Add(auditValues);
					}
				}
				i++;
				columnName = string.Format("FieldName{0}", i);
			}
			return retVal;
		}

		private static DateTime? ParseDateValue(string dateValue) {
			if (dateValue == null) {
				return null;
			}

			DateTime date;
			var parseSucceed = DateTime.TryParse(dateValue, DateTimeFormatInfo.CurrentInfo, DateTimeStyles.AssumeUniversal, out date);
			return parseSucceed ? (DateTime?)date.ToUniversalTime() : null;
		}
	}
}

﻿using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.DataLayer.BrokerDB {

	/// <summary>
	/// Database broker for DataSerie business entity.
	/// </summary>
	public class StatisticalSerieBrokerDB : BaseBrokerDB<StatisticalSerie> {

		/// <summary>
		/// The name of the stored procedure that retreives a list of dataseries for a specific chart.
		/// </summary>
		protected const string const_proc_getlistbychart = AppModule.Energy + "_StatisticalSerieGetListByChart";

		/// <summary>
		/// The name of the stored procedure that retreives a list of dataseries for a specific chart.
		/// </summary>
		protected const string const_proc_getallpagingbykeychart = AppModule.Energy + "_StatisticalSerieGetAllPagingByKeyChart";

		/// <summary>
		/// The name of the stored procedure that saves a list of dataseries for a specific chart.
		/// </summary>
		protected const string const_proc_chartdataseriessave = AppModule.Energy + "_ChartStatisticalSeriesSave";

		/// <summary>
		/// Gets the const_proc_prefix.
		/// </summary>
		/// <value>The const_proc_prefix.</value>
		protected override string const_proc_prefix {
			get {
				return AppModule.Energy;
			}
		}

		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, StatisticalSerie item) {
			db.AddInParameterString(command, "Key", item.KeyStatisticalSerie);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "LocalId", item.LocalId);
			db.AddInParameterString(command, "KeyChart", item.KeyChart);
			db.AddInParameterBool(command, "IncludeHiddenSeries", item.IncludeHiddenSeries);
			db.AddInParameterBool(command, "RunAfterCalculatedSeries", item.RunAfterCalculatedSeries);
			db.AddInParameterString(command, "KeyClassificationItem", item.KeyClassificationItem);
			db.AddInParameterString(command, "Operator", item.Operator.ParseAsString());
		}

		/// <summary>
		/// Populates a StatisticalSerie business entity from a DataSourceDecorator.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated StatisticalSerie business entity.</returns>
		protected override StatisticalSerie FillObject(DataSourceDecorator source, string prefixColumn) {
			return new StatisticalSerie {
				KeyStatisticalSerie = source.Field<int>(prefixColumn + "KeyStatisticalSerie").ToString(CultureInfo.InvariantCulture),
				LocalId = source.Field<string>(prefixColumn + "LocalId"),
				KeyChart = source.Field<int>(prefixColumn + "KeyChart").ToString(CultureInfo.InvariantCulture),
				IncludeHiddenSeries = source.Field<bool>(prefixColumn + "IncludeHiddenSeries"),
				RunAfterCalculatedSeries = source.Field<bool>(prefixColumn + "RunAfterCalculatedSeries"),
				KeyClassificationItem = source.Field<string>(prefixColumn + "KeyClassificationItem"),
				KeyClassificationItemPath = source.Field<string>(prefixColumn + "KeyClassificationItemPath"),
				ClassificationItemLongPath = source.Field<string>(prefixColumn + "ClassificationItemLongPath"),
				ClassificationItemLocalIdPath = source.Field<string>(prefixColumn + "ClassificationItemLocalIdPath"),
				ClassificationItemTitle = source.Field<string>(prefixColumn + "ClassificationItemTitle"),
				Operator = source.Field<string>(prefixColumn + "Operator").ParseAsEnum<MathematicOperator>(),
				ClassificationItemColorR = source.Field<int>(prefixColumn + "ClassificationItemColorR"),
				ClassificationItemColorG = source.Field<int>(prefixColumn + "ClassificationItemColorG"),
				ClassificationItemColorB = source.Field<int>(prefixColumn + "ClassificationItemColorB"),
			};
		}

		/// <summary>
		/// Get the list of saved StatisticalSerie from a Chart
		/// </summary>
		/// <param name="KeyChart">The key of the chart.</param>
		public List<StatisticalSerie> GetListByKeyChart(string KeyChart) {
			List<StatisticalSerie> results;
			DbCommand command = db.GetStoredProcCommand(const_proc_getlistbychart);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyChart", KeyChart);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}

		/// <summary>
		/// Gets all the StatisticalSerie for a specific chart.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyChart">The Key of the chart.</param>
		/// <param name="total">The total number of items.</param>
		/// <returns>The list of items.</returns>
		public List<StatisticalSerie> GetAllPagingByKeyChart(PagingParameter paging, string KeyChart, out int total) {
			DbCommand command = db.GetStoredProcCommand(const_proc_getallpagingbykeychart);
			command.CommandTimeout = 600;
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyChart", KeyChart);
			db.AddInParameterInt(command, "start", paging.start);
			db.AddInParameterInt(command, "limit", paging.limit);
			DataSet ds = db.ExecuteDataSet(command);
			if (ds.Tables[0].Rows.Count > 0)
				total = ds.Tables[0].Rows[0].Field<int>("TotalRecord");
			else
				total = 0;
			return FillObjects(ds);
		}

		/// <summary>
		/// Saves the chart StatisticalSerie.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="statisticalseries">The Statisticalseries.</param>
		/// <returns></returns>
		public List<StatisticalSerie> SaveChartStatisticalSeries(Chart entity, CrudStore<StatisticalSerie> statisticalseries) {
			DbCommand command = db.GetStoredProcCommand(const_proc_chartdataseriessave);
			command.CommandTimeout = 600;
			List<StatisticalSerie> results;

			if (statisticalseries == null || statisticalseries.IsEmpty())
				return null;
			string deletes = null;

			if (statisticalseries.destroy != null) {
				deletes = Helper.BuildXml(statisticalseries.destroy.Select(p => p.KeyStatisticalSerie).ToArray());
			}

			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyChart", entity.KeyChart);
			db.AddInParameterString(command, "Inserts", null);
			db.AddInParameterString(command, "Deletes", deletes);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}
	}
}
﻿using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.DataLayer.BrokerDB {

	/// <summary>
	/// Database broker for Portlet business entity.
	/// </summary>
	public class PortletBrokerDB : BaseBrokerDB<Portlet> {

		/// <summary>
		/// The name of the stored procedure that retreives a list of Porlets for a specific Portal Column.
		/// </summary>
		protected const string const_proc_getlistbyportalcolumn = AppModule.Energy + "_PortletGetListByKeyPortalColumn";

		/// <summary>
		/// The name of the stored procedure that retreives a list of Porlets for a specific Portal Column.
		/// </summary>
		protected const string const_proc_getallpagingbykeyportalcolumn = AppModule.Energy + "_PortletGetAllPagingByKeyPortalColumn";

		/// <summary>
		/// The name of the stored procedure that saves a list of portlets for a specific Portal column.
		/// </summary>
		protected const string const_proc_portalcolumnsave = AppModule.Energy + "_PortalColumnPortletSave";

		/// <summary>
		/// The name of the stored procedure that retreives a list of Porlets for a specific Portal Column.
		/// </summary>
		protected const string const_proc_getlistbyentity = AppModule.Energy + "_PortletGetListByKeyEntity";

		/// <summary>
		/// The name of the stored procedure that retreives a list of Porlets for a specific Portal Column.
		/// </summary>
		protected const string const_proc_getallpagingbykeyentity = AppModule.Energy + "_PortletGetAllPagingByKeyEntity";

		/// <summary>
		/// The prefix of stored procedures of that provider. 
		/// </summary>
		protected override string const_proc_prefix {
			get {
				return AppModule.Energy;
			}
		}

		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, Portlet item) {
			db.AddInParameterString(command, "Key", item.KeyPortlet);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyPortalColumn", item.KeyPortalColumn);
			db.AddInParameterString(command, "LocalId", item.LocalId);
			db.AddInParameterString(command, "KeyEntity", item.KeyEntity);
			db.AddInParameterString(command, "TypeEntity", item.TypeEntity);
			db.AddInParameterString(command, "Title", item.Title);
			db.AddInParameterBool(command, "Collapsed", item.Collapsed);
			db.AddInParameterBool(command, "HeaderAndToolbarVisible", item.HeaderAndToolbarVisible);
			db.AddInParameterInt(command, "Flex", item.Flex);
			db.AddInParameterInt(command, "Order", item.Order);
			db.AddInParameterInt(command, "FixedHeight", item.FixedHeight);
			db.AddInParameterInt(command, "RefreshFrequency", item.RefreshFrequency);
			db.AddInParameterString(command, "AssociatedPortalTabKey", item.AssociatedPortalTabKey);
			db.AddInParameterString(command, "RefreshAnimationDirection", item.RefreshAnimationDirection);
		}

		/// <summary>
		/// Populates a Portlet business entity from a DataSourceDecorator.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated Portlet business entity.</returns>
		protected override Portlet FillObject(DataSourceDecorator source, string prefixColumn) {
			var retVal = new Portlet {
				KeyPortlet = source.Field<int>(prefixColumn + "KeyPortlet").ToString(CultureInfo.InvariantCulture),
				KeyPortalColumn = source.Field<int>(prefixColumn + "KeyPortalColumn").ToString(CultureInfo.InvariantCulture),
				KeyPortalTab = source.Field<int>(prefixColumn + "KeyPortalTab").ToString(CultureInfo.InvariantCulture),
				KeyPortalWindow = source.Field<int>(prefixColumn + "KeyPortalWindow").ToString(CultureInfo.InvariantCulture),
				KeyEntity = source.Field<string>(prefixColumn + "KeyEntity"),
				LocalId = source.Field<string>(prefixColumn + "LocalId"),
				TypeEntity = source.Field<string>(prefixColumn + "TypeEntity"),
				Title = source.Field<string>(prefixColumn + "Title"),
				Collapsed = source.Field<bool>(prefixColumn + "Collapsed"),
				HeaderAndToolbarVisible = source.Field<bool>(prefixColumn + "HeaderAndToolbarVisible"),
				Flex = source.Field<int>(prefixColumn + "Flex"),
				Order = source.Field<int>(prefixColumn + "Order"),
				FixedHeight = source.Field<int>(prefixColumn + "FixedHeight"),
				RefreshFrequency = source.Field<int?>(prefixColumn + "RefreshFrequency"),
				PortalWindowTitle = source.Field<string>(prefixColumn + "PortalWindowTitle"),
				PortalTabTitle = source.Field<string>(prefixColumn + "PortalTabTitle"),
				PortalWindowIconCls = source.Field<string>(prefixColumn + "PortalWindowIconCls"),
				AssociatedPortalTabKey = source.Field<string>(prefixColumn + "AssociatedPortalTabKey"),
				TitleEntity = source.Field<string>(prefixColumn + "TitleEntity"),
				RefreshAnimationDirection = source.Field<string>(prefixColumn + "RefreshAnimationDirection"),
			};
			return retVal;
		}

		/// <summary>
		/// Get the list of saved Portlets from a Portal Column.
		/// </summary>
		/// <param name="KeyPortalColumn">The key of the portal column.</param>
		public List<Portlet> GetListByKeyPortalColumn(string KeyPortalColumn) {
			List<Portlet> results;
			DbCommand command = db.GetStoredProcCommand(const_proc_getlistbyportalcolumn);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyPortalColumn", KeyPortalColumn);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}

		/// <summary>
		/// Gets all the Portlets for a specific Portal Column.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyPortalColumn">The Key of the portal column.</param>
		/// <param name="total">The total number of items.</param>
		/// <returns>The list of items.</returns>
		public List<Portlet> GetAllPagingByKeyPortalColumn(PagingParameter paging, string KeyPortalColumn, out int total) {
			DbCommand command = db.GetStoredProcCommand(const_proc_getallpagingbykeyportalcolumn);
			command.CommandTimeout = 600;
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyPortalColumn", KeyPortalColumn);
			db.AddInParameterInt(command, "start", paging.start);
			db.AddInParameterInt(command, "limit", paging.limit);
			DataSet ds = db.ExecuteDataSet(command);
			if (ds.Tables[0].Rows.Count > 0)
				total = ds.Tables[0].Rows[0].Field<int>("TotalRecord");
			else
				total = 0;
			return FillObjects(ds);
		}

		/// <summary>
		/// Get the list of saved Portlets for a specific Entity.
		/// </summary>
		/// <param name="KeyEntity">The key entity.</param>
		/// <param name="TypeEntity">The type entity.</param>
		/// <returns></returns>
		public List<Portlet> GetListByKeyEntity(string KeyEntity, string TypeEntity) {
			List<Portlet> results;
			DbCommand command = db.GetStoredProcCommand(const_proc_getlistbyentity);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyEntity", KeyEntity);
			db.AddInParameterString(command, "TypeEntity", TypeEntity);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}

		/// <summary>
		/// Gets all the Portlets for a specific Entity.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyEntity">The key entity.</param>
		/// <param name="TypeEntity">The type entity.</param>
		/// <param name="total">The total number of items.</param>
		/// <returns>
		/// The list of items.
		/// </returns>
		public List<Portlet> GetAllPagingByKeyEntity(PagingParameter paging, string KeyEntity, string TypeEntity, out int total) {
			DbCommand command = db.GetStoredProcCommand(const_proc_getallpagingbykeyentity);
			command.CommandTimeout = 600;
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyEntity", KeyEntity);
			db.AddInParameterString(command, "TypeEntity", TypeEntity);
			db.AddInParameterInt(command, "start", paging.start);
			db.AddInParameterInt(command, "limit", paging.limit);
			DataSet ds = db.ExecuteDataSet(command);
			if (ds.Tables[0].Rows.Count > 0)
				total = ds.Tables[0].Rows[0].Field<int>("TotalRecord");
			else
				total = 0;
			return FillObjects(ds);
		}

		/// <summary>
		/// Saves the portal column Portlets.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="portlet">The Portlet store.</param>
		/// <returns></returns>
		public List<Portlet> SavePortalColumnPortlet(PortalColumn entity, CrudStore<Portlet> portlet) {
			DbCommand command = db.GetStoredProcCommand(const_proc_portalcolumnsave);
			command.CommandTimeout = 600;
			List<Portlet> results;

			if (portlet == null || portlet.IsEmpty())
				return null;
			string inserts = null;
			string deletes = null;

			/*if (dataseries.create != null) {
				inserts = Helper.BuildXml(dataseries.create.Select(p => p.KeyDataSerie).ToArray());
			}*/

			if (portlet.destroy != null) {
				deletes = Helper.BuildXml(portlet.destroy.Select(p => p.KeyPortlet).ToArray());
			}

			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyPortalColumn", entity.KeyPortalColumn);
			db.AddInParameterString(command, "Inserts", inserts);
			db.AddInParameterString(command, "Deletes", deletes);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}
	}
}
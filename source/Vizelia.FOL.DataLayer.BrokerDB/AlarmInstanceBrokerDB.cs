﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.DataLayer.BrokerDB {

	/// <summary>
	/// Database broker for AlarmInstance business entity.
	/// </summary>
	public class AlarmInstanceBrokerDB : BaseBrokerDB<AlarmInstance> {

		/// <summary>
		/// The name of the stored procedure that retreives a list of  AlarmInstance for a specific  Alarm definition.
		/// </summary>
		protected const string const_proc_getlistbykeyalarmdefinition = AppModule.Energy + "_AlarmInstanceGetListByKeyAlarmDefinition";

		/// <summary>
		/// The name of the stored procedure that retreives a list of  AlarmInstance for a specific Chart (that are coming from a AlarmDefinition based on this Chart).
		/// </summary>
		protected const string const_proc_getlistbykeychart = AppModule.Energy + "_AlarmInstanceGetListByKeyChart";

		/// <summary>
		/// The name of the stored procedure that retreives a list of saved AlarmInstance for a specific Chart (that are coming from a cross of the Chart AlarmDefinition Classification and Spatial Filter).
		/// </summary>
		protected const string const_proc_getlistasdatasourcebykeychart = AppModule.Energy + "_AlarmInstanceGetListAsDataSourceByKeyChart";

		/// <summary>
		/// The name of the stored procedure that retreives a list of  AlarmInstance for a specific MeterValidationRule.
		/// </summary>
		protected const string const_proc_getlistbykeymetervalidationrule = AppModule.Energy + "_AlarmInstanceGetListByKeyMeterValidationRule";

		/// <summary>
		/// The name of the stored procedure that retreives a list of  AlarmInstance for a specific MeterData.
		/// </summary>
		protected const string const_proc_getlistbykeymeterdata = AppModule.Energy + "_AlarmInstanceGetListByKeyMeterData";

		/// <summary>
		/// The name of the stored procedure that retreives a list of  AlarmInstance for a specific Meter.
		/// </summary>
		protected const string const_proc_getlistbykeymeter = AppModule.Energy + "_AlarmInstanceGetListByKeyMeter";

		/// <summary>
		/// The name of the stored procedure that retreives a list of  AlarmInstance for a Alarm definition.
		/// </summary>
		protected const string const_proc_getallpagingbykeyalarmdefinition = AppModule.Energy + "_AlarmInstanceGetAllPagingByKeyAlarmDefinition";

		/// <summary>
		/// The name of the stored procedure that retreives a list of  AlarmInstance for a MeterValidationRule.
		/// </summary>
		protected const string const_proc_getallpagingbykeymetervalidationrule = AppModule.Energy + "_AlarmInstanceGetAllPagingByKeyMeterValidationRule";

		/// <summary>
		/// The name of the stored procedure that retreives a list of  AlarmInstance for a MeterData.
		/// </summary>
		protected const string const_proc_getallpagingbykeymeterdata = AppModule.Energy + "_AlarmInstanceGetAllPagingByKeyMeterData";

		/// <summary>
		/// The name of the stored procedure that retreives a list of  AlarmInstance for a Meter.
		/// </summary>
		protected const string const_proc_getallpagingbykeymeter = AppModule.Energy + "_AlarmInstanceGetAllPagingByKeyMeter";

		/// <summary>
		/// The name of the stored procedure that retreives a list of  AlarmInstance for a Alarm Table.
		/// </summary>
		protected const string const_proc_getallpagingbykeyalarmtable = AppModule.Energy + "_AlarmInstanceGetAllPagingByKeyAlarmTable";

		/// <summary>
		/// The name of the stored procedure that retreives a list of new created  AlarmInstance for a specific Location.
		/// </summary>
		protected const string const_proc_getlistbykeylocation = AppModule.Energy + "_AlarmInstanceGetListByKeyLocation";

		/// <summary>
		/// The name of the stored procedure that retreives the latest AlarmInstances.
		/// </summary>
		protected const string const_proc_getlatest = AppModule.Energy + "_AlarmInstanceGetLatestPaging";

		/// <summary>
		/// The name of the stored procedure that retreives a list of  AlarmInstance for a specific  Alarm definition.
		/// </summary>
		protected const string const_proc_deleteall = AppModule.Energy + "_AlarmInstanceDeleteAll";

		/// <summary>
		///  The name of the stored procedure that deletes a limited number of rows
		/// </summary>
		protected const string const_proc_deletelimitednumberrows = AppModule.Energy + "_AlarmInstanceDeleteLimitedNumberRows";

		/// <summary>
		///  The name of the stored procedure that retrieves the instances count for specific sources
		/// </summary>
		protected const string const_proc_getcountbykeysource = AppModule.Energy + "_AlarmInstanceGetCountByKeySource";        

		/// <summary>
		/// The prefix of stored procedures of that provider. 
		/// </summary>
		protected override string const_proc_prefix {
			get {
				return AppModule.Energy;
			}
		}

		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, AlarmInstance item) {
			db.AddInParameterString(command, "Key", item.KeyAlarmInstance);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "LocalId", item.LocalId);
			db.AddInParameterString(command, "Description", item.Description);
			db.AddInParameterDateTime(command, "CreateDateTime", DateTime.UtcNow);

			db.AddInParameterDateTime(command, "InstanceDateTime", item.InstanceDateTime);
			db.AddInParameterString(command, "KeyAlarmDefinition", item.KeyAlarmDefinition);
			db.AddInParameterString(command, "KeyMeterValidationRule", item.KeyMeterValidationRule);

			db.AddInParameterString(command, "KeyChart", item.KeyChart);
			db.AddInParameterString(command, "DataSerieLocalId", item.DataSerieLocalId);
			db.AddInParameterString(command, "KeyDataSerie", item.KeyDataSerie);

			db.AddInParameterString(command, "KeyMeter", item.KeyMeter);

			db.AddInParameterString(command, "Status", item.Status.ParseAsString());
			db.AddInParameterDouble(command, "Value", item.Value);

			db.AddInParameterString(command, "KeyActionRequest", item.KeyActionRequest);

			db.AddInParameterString(command, "KeyMeterData", item.KeyMeterData);
			db.AddInParameterDateTime(command, "MeterDataAcquisitionDateTimeStart", item.MeterDataAcquisitionDateTimeStart);
			db.AddInParameterDateTime(command, "MeterDataAcquisitionDateTimeEnd", item.MeterDataAcquisitionDateTimeEnd);
			db.AddInParameterString(command, "MeterValidationRuleAlarmType", item.MeterValidationRuleAlarmType.ParseAsString());

			db.AddInParameterBool(command, "EmailSent", item.EmailSent);
		}

		/// <summary>
		/// Populates a AlarmInstance business entity from a DataSourceDecorator.
		/// </summary>
		/// <param name="source">The data source.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated AlarmInstance business entity.</returns>
		protected override AlarmInstance FillObject(DataSourceDecorator source, string prefixColumn) {
			var keyMeterData = source.Field<long?>(prefixColumn + "KeyMeterData");
			var retVal = new AlarmInstance {
				KeyAlarmInstance = source.Field<int>(prefixColumn + "KeyAlarmInstance").ToString(CultureInfo.InvariantCulture),
				LocalId = source.Field<string>(prefixColumn + "LocalId"),
				Description = source.Field<string>(prefixColumn + "Description"),
				Title = source.Field<string>(prefixColumn + "Title"),
				InstanceDateTime = source.Field<DateTime>(prefixColumn + "InstanceDateTime"),
				CreateDateTime = source.Field<DateTime>(prefixColumn + "CreateDateTime"),


				KeyChart = source.Field<string>(prefixColumn + "KeyChart"),
				KeyDataSerie = source.Field<string>(prefixColumn + "KeyDataSerie"),
				DataSerieLocalId = source.Field<string>(prefixColumn + "DataSerieLocalId"),
				Name = source.Field<string>(prefixColumn + "Name"),

				KeyMeter = source.Field<string>(prefixColumn + "KeyMeter"),

				KeyMeterData = keyMeterData.HasValue ? keyMeterData.Value.ToString(CultureInfo.InvariantCulture) : null,
				MeterDataAcquisitionDateTimeStart = source.Field<DateTime?>(prefixColumn + "MeterDataAcquisitionDateTimeStart"),
				MeterDataAcquisitionDateTimeEnd = source.Field<DateTime?>(prefixColumn + "MeterDataAcquisitionDateTimeEnd"),
				MeterValidationRuleAlarmType = (source.Field<string>(prefixColumn + "MeterValidationRuleAlarmType") ?? "None").ParseAsEnum<MeterValidationRuleAlarmType>(),

				Status = source.Field<string>(prefixColumn + "Status").ParseAsEnum<AlarmInstanceStatus>(),
				Value = source.Field<double>(prefixColumn + "Value"),
				Threshold = source.Field<double?>(prefixColumn + "Threshold"),
				Threshold2 = source.Field<double?>(prefixColumn + "Threshold2"),

				KeyActionRequest = source.Field<string>(prefixColumn + "KeyActionRequest"),

				EmailSent = source.Field<bool?>(prefixColumn + "EmailSent"),

				KeyLocation = source.Field<string>(prefixColumn + "KeyLocation"),
				LocationLevel = source.Field<int?>("LocationLevel"),
				LocationLongPath = source.Field<string>(prefixColumn + "LocationLongPath"),
				LocationShortPath = source.Field<string>(prefixColumn + "LocationShortPath"),
				LocationName = source.Field<string>(prefixColumn + "LocationName"),
			};

			string chartTimeZoneId = source.Field<string>(prefixColumn + "ChartTimeZoneId");
			if (!String.IsNullOrEmpty(chartTimeZoneId)) {
				TimeZoneInfo timeZoneInfo = CalendarHelper.GetTimeZoneFromId(chartTimeZoneId);
				retVal.ChartInstanceDateTime =
					string.Format(retVal.InstanceDateTime.ToString(DateTimeHelper.DateTimeFormat(true)) + " ({0}{1})",
						timeZoneInfo.BaseUtcOffset.CompareTo(TimeSpan.Zero) > 0 ? "+" : "", timeZoneInfo.BaseUtcOffset);
				
				retVal.UTCInstanceDateTime = DateTime.SpecifyKind(retVal.InstanceDateTime, DateTimeKind.Unspecified);
				retVal.UTCInstanceDateTime = TimeZoneInfo.ConvertTimeToUtc((DateTime)retVal.UTCInstanceDateTime, timeZoneInfo);
			}

			var locationTypeName = source.Field<string>(prefixColumn + "LocationTypeName");
			retVal.LocationTypeName = string.IsNullOrEmpty(locationTypeName) ? HierarchySpatialTypeName.None : locationTypeName.ParseAsEnum<HierarchySpatialTypeName>();


			var keyAlarmDefinition = source.Field<int?>(prefixColumn + "KeyAlarmDefinition");
			if (keyAlarmDefinition.HasValue) {
				retVal.KeyAlarmDefinition = keyAlarmDefinition.Value.ToString(CultureInfo.InvariantCulture);
				retVal.KeyClassificationItem = source.Field<string>(prefixColumn + "KeyClassificationItem");
				retVal.KeyClassificationItemPath = source.Field<string>(prefixColumn + "KeyClassificationItemPath");
				retVal.ClassificationItemLongPath = source.Field<string>(prefixColumn + "ClassificationItemLongPath");
				retVal.ClassificationItemLocalIdPath = source.Field<string>(prefixColumn + "ClassificationItemLocalIdPath");
				retVal.ClassificationItemTitle = source.Field<string>(prefixColumn + "ClassificationItemTitle");
				retVal.ClassificationItemLevel = source.Field<int>(prefixColumn + "ClassificationItemLevel");
				retVal.ClassificationItemColorR = source.Field<int>(prefixColumn + "ClassificationItemColorR");
				retVal.ClassificationItemColorG = source.Field<int>(prefixColumn + "ClassificationItemColorG");
				retVal.ClassificationItemColorB = source.Field<int>(prefixColumn + "ClassificationItemColorB");
				retVal.KeyAlarmDefinitionLocation = source.Field<string>(prefixColumn + "KeyAlarmDefinitionLocation");
			}
			var keyMeterValidation = source.Field<int?>(prefixColumn + "KeyMeterValidationRule");
			if (keyMeterValidation.HasValue)
				retVal.KeyMeterValidationRule = keyMeterValidation.Value.ToString(CultureInfo.InvariantCulture);

			return retVal;

		}

		/// <summary>
		/// Get the list of saved AlarmInstance for a specific AlarmDefinition .
		/// </summary>
		/// <param name="KeyAlarmDefinition">The key of the alarm definition.</param>
		public List<AlarmInstance> GetListByKeyAlarmDefinition(string KeyAlarmDefinition) {
			List<AlarmInstance> results;
			DbCommand command = db.GetStoredProcCommand(const_proc_getlistbykeyalarmdefinition);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyAlarmDefinition", KeyAlarmDefinition);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}

		/// <summary>
		/// Get the list of saved AlarmInstance for a specific MeterValidationRule .
		/// </summary>
		/// <param name="KeyMeterValidationRule">The key meter validation rule.</param>
		/// <returns></returns>
		public List<AlarmInstance> GetListByKeyMeterValidationRule(string KeyMeterValidationRule) {
			List<AlarmInstance> results;
			DbCommand command = db.GetStoredProcCommand(const_proc_getlistbykeymetervalidationrule);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyMeterValidationRule", KeyMeterValidationRule);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}

		/// <summary>
		/// Get the list of saved AlarmInstance for a specific MeterData .
		/// </summary>
		/// <param name="KeyMeterData">The key meter data.</param>
		/// <returns></returns>
		public List<AlarmInstance> GetListByKeyMeterData(string KeyMeterData) {
			List<AlarmInstance> results;
			DbCommand command = db.GetStoredProcCommand(const_proc_getlistbykeymeterdata);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyMeterData", KeyMeterData);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}

		/// <summary>
		/// Get the list of saved AlarmInstance for a specific Meter .
		/// </summary>
		/// <param name="KeyMeter">The key meter.</param>
		/// <returns></returns>
		public List<AlarmInstance> GetListByKeyMeter(string KeyMeter) {
			List<AlarmInstance> results;
			DbCommand command = db.GetStoredProcCommand(const_proc_getlistbykeymeter);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyMeter", KeyMeter);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}

		/// <summary>
		/// Get the list of saved AlarmInstance for a specific Chart.
		/// </summary>
		/// <param name="KeyChart">The key chart.</param>
		/// <returns></returns>
		public List<AlarmInstance> GetListByKeyChart(string KeyChart) {
			List<AlarmInstance> results;
			DbCommand command = db.GetStoredProcCommand(const_proc_getlistbykeychart);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyChart", KeyChart);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}

		/// <summary>
		/// Get the list of saved AlarmInstance for a specific Chart (that are coming from a cross of the Chart AlarmDefinition Classification and Spatial Filter).
		/// </summary>
		/// <param name="KeyChart">The key chart.</param>
		/// <param name="startDate">The start date.</param>
		/// <param name="endDate">The end date.</param>
		/// <param name="timeInterval">The time interval.</param>
		/// <param name="timeZone">The time zone.</param>
		/// <returns></returns>
		public List<AlarmInstanceAsDataSource> GetListAsDataSourceByKeyChart(string KeyChart, DateTime startDate, DateTime endDate, AxisTimeInterval timeInterval, TimeZoneInfo timeZone) {
			List<AlarmInstanceAsDataSource> results = new List<AlarmInstanceAsDataSource>();
			DbCommand command = db.GetStoredProcCommand(const_proc_getlistasdatasourcebykeychart);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyChart", KeyChart);
			db.AddInParameterDateTime(command, "StartDate", startDate);
			db.AddInParameterDateTime(command, "EndDate", endDate);
			AddProjectLocationFilter(command, "ProjectLocationFilter");

			using (IDataReader reader = db.ExecuteReader(command)) {
				while (reader.Read()) {
					var item = new AlarmInstanceAsDataSource {
						InstanceDateTime = DateTime.SpecifyKind((DateTime)reader["InstanceDateTime"], DateTimeKind.Utc).ConvertTimeFromUtc(timeZone).UpdateByInterval(timeInterval),
						Value = (int)reader["Value"],
						KeyAlarmDefinitionLocation = (string)reader["KeyAlarmDefinitionLocation"],
						KeyClassificationItem = (string)reader["KeyClassificationItem"],
					};
					results.Add(item);
				}
			}
			//We consolidate the AlermInstances by TimeInterval to minize data exchange with EA.
			var consolidatedAlarmInstances =
					(from ai in results
					 group ai by new {
						 ai.InstanceDateTime,
						 ai.KeyAlarmDefinitionLocation,
						 ai.KeyClassificationItem,
					 } into cai
					 select new AlarmInstanceAsDataSource {
						 InstanceDateTime = cai.Key.InstanceDateTime.ConvertTimeToUtc(timeZone),
						 KeyAlarmDefinitionLocation = cai.Key.KeyAlarmDefinitionLocation,
						 KeyClassificationItem = cai.Key.KeyClassificationItem,
						 Value = cai.Sum(i => i.Value)
					 }).ToList();

			return consolidatedAlarmInstances;
		}


		/// <summary>
		/// Gets all the AlarmInstance for a specific AlarmDefinition.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyAlarmDefinition">The Key of the AlarmDefinition.</param>
		/// <param name="total">The total number of items.</param>
		/// <returns>The list of items.</returns>
		public List<AlarmInstance> GetAllPagingByKeyAlarmDefinition(PagingParameter paging, string KeyAlarmDefinition, out int total) {
			DbCommand command = db.GetStoredProcCommand(const_proc_getallpagingbykeyalarmdefinition);
			command.CommandTimeout = 600;
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyAlarmDefinition", KeyAlarmDefinition);
			db.AddInParameterInt(command, "start", paging.start);
			db.AddInParameterInt(command, "limit", paging.limit);

			AddPagingParameterOrderBy(paging, command);
			if (paging != null)
				paging.ExtractInnerJoinFilters();
			AddPagingParameterFilter(paging, command);

			DataSet ds = db.ExecuteDataSet(command);
			if (ds.Tables[0].Rows.Count > 0)
				total = ds.Tables[0].Rows[0].Field<int>("TotalRecord");
			else
				total = 0;
			return FillObjects(ds);
		}

		/// <summary>
		/// Gets all the AlarmInstance for a specific AlarmTable.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyAlarmTable">The Key of the AlarmDefinition.</param>
		/// <param name="total">The total number of items.</param>
		/// <returns>
		/// The list of items.
		/// </returns>
		public List<AlarmInstance> GetAllPagingByKeyAlarmTable(PagingParameter paging, string KeyAlarmTable, out int total) {
			DbCommand command = db.GetStoredProcCommand(const_proc_getallpagingbykeyalarmtable);
			command.CommandTimeout = 600;
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyAlarmTable", KeyAlarmTable);
			db.AddInParameterInt(command, "start", paging.start);
			db.AddInParameterInt(command, "limit", paging.limit);

			AddPagingParameterOrderBy(paging, command);
			paging.ExtractInnerJoinFilters();
			AddPagingParameterFilter(paging, command);

			DataSet ds = db.ExecuteDataSet(command);
			if (ds.Tables[0].Rows.Count > 0)
				total = ds.Tables[0].Rows[0].Field<int>("TotalRecord");
			else
				total = 0;
			return FillObjects(ds);
		}

		/// <summary>
		/// Gets all the AlarmInstance for a specific MeterValidationRule.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyMeterValidationRule">The key meter validation rule.</param>
		/// <param name="total">The total number of items.</param>
		/// <returns>
		/// The list of items.
		/// </returns>
		public List<AlarmInstance> GetAllPagingByKeyMeterValidationRule(PagingParameter paging, string KeyMeterValidationRule, out int total) {
			DbCommand command = db.GetStoredProcCommand(const_proc_getallpagingbykeymetervalidationrule);
			command.CommandTimeout = 600;
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyMeterValidationRule", KeyMeterValidationRule);
			db.AddInParameterInt(command, "start", paging.start);
			db.AddInParameterInt(command, "limit", paging.limit);

			AddPagingParameterOrderBy(paging, command);
			paging.ExtractInnerJoinFilters();
			AddPagingParameterFilter(paging, command);

			DataSet ds = db.ExecuteDataSet(command);
			if (ds.Tables[0].Rows.Count > 0)
				total = ds.Tables[0].Rows[0].Field<int>("TotalRecord");
			else
				total = 0;
			return FillObjects(ds);
		}

		/// <summary>
		/// Gets all the AlarmInstance for a specific Meter.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyMeter">The key meter.</param>
		/// <param name="total">The total number of items.</param>
		/// <returns>
		/// The list of items.
		/// </returns>
		public List<AlarmInstance> GetAllPagingByKeyMeter(PagingParameter paging, string KeyMeter, out int total) {
			DbCommand command = db.GetStoredProcCommand(const_proc_getallpagingbykeymeter);
			command.CommandTimeout = 600;
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyMeter", KeyMeter);
			db.AddInParameterInt(command, "start", paging.start);
			db.AddInParameterInt(command, "limit", paging.limit);

			AddPagingParameterOrderBy(paging, command);
			if (paging != null)
				paging.ExtractInnerJoinFilters();
			AddPagingParameterFilter(paging, command);

			DataSet ds = db.ExecuteDataSet(command);
			if (ds.Tables[0].Rows.Count > 0)
				total = ds.Tables[0].Rows[0].Field<int>("TotalRecord");
			else
				total = 0;
			return FillObjects(ds);
		}

		/// <summary>
		/// Gets all the AlarmInstance for a specific MeterData.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyMeterData">The key meter data.</param>
		/// <param name="total">The total number of items.</param>
		/// <returns>
		/// The list of items.
		/// </returns>
		public List<AlarmInstance> GetAllPagingByKeyMeterData(PagingParameter paging, string KeyMeterData, out int total) {
			DbCommand command = db.GetStoredProcCommand(const_proc_getallpagingbykeymeterdata);
			command.CommandTimeout = 600;
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyMeterData", KeyMeterData);
			db.AddInParameterInt(command, "start", paging.start);
			db.AddInParameterInt(command, "limit", paging.limit);

			AddPagingParameterOrderBy(paging, command);
			paging.ExtractInnerJoinFilters();
			AddPagingParameterFilter(paging, command);

			DataSet ds = db.ExecuteDataSet(command);
			if (ds.Tables[0].Rows.Count > 0)
				total = ds.Tables[0].Rows[0].Field<int>("TotalRecord");
			else
				total = 0;
			return FillObjects(ds);
		}

		/// <summary>
		/// Get the list of opened AlarmInstance for a specific Location .
		/// </summary>
		/// <param name="KeyLocation">The key of the location.</param>
		/// <param name="KeyClassificationItem">The key classification item.</param>
		/// <returns></returns>
		public List<AlarmInstance> GetOpenListByKeyLocation(string KeyLocation, string KeyClassificationItem) {
			List<AlarmInstance> results;
			DbCommand command = db.GetStoredProcCommand(const_proc_getlistbykeylocation);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "Location", Helper.BuildXml(new string[] { KeyLocation }));
			db.AddInParameterString(command, "ClassificationItem", Helper.BuildXml(new string[] { KeyClassificationItem }));
			db.AddInParameterString(command, "Status", AlarmInstanceStatus.Open.ParseAsString());

			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}

		/// <summary>
		/// Get the latest AlarmInstances with an InstanceDatetime newer than Now - seconds.
		/// </summary>
		/// <param name="seconds">The seconds.</param>
		/// <param name="paging">The paging.</param>
		/// <param name="total">The total.</param>
		/// <returns></returns>
		public List<AlarmInstance> GetLatest(int seconds, PagingParameter paging, out int total) {
			DbCommand command = db.GetStoredProcCommand(const_proc_getlatest);
			command.CommandTimeout = 600;
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterInt(command, "Seconds", seconds);
			db.AddInParameterString(command, "Creator", Helper.GetCurrentActor());
			db.AddInParameterInt(command, "start", paging.start);
			db.AddInParameterInt(command, "limit", paging.limit);
			DataSet ds = db.ExecuteDataSet(command);
			if (ds.Tables[0].Rows.Count > 0)
				total = ds.Tables[0].Rows[0].Field<int>("TotalRecord");
			else
				total = 0;
			return FillObjects(ds);
		}

		/// <summary>
		/// Deletes the provided item from database.
		/// </summary>
		/// <param name="KeyAlarmDefinition">The key alarm definition.</param>
		/// <param name="KeyMeterValidationRule">The key meter validation rule.</param>
		/// <returns>
		/// True if successfull, false otherwise.
		/// </returns>
		public bool DeleteAll(string KeyAlarmDefinition, string KeyMeterValidationRule) {
			using (DbCommand command = db.GetStoredProcCommand(const_proc_deleteall)) {
				db.AddInParameterString(command, "KeyAlarmDefinition", KeyAlarmDefinition);
				db.AddInParameterString(command, "KeyMeterValidationRule", KeyMeterValidationRule);
				try {
					int result = db.ExecuteNonQuery(command);
					return result > 0;
				}
				catch (System.Data.SqlClient.SqlException ex) {
					throw SqlExceptionWrap(ex);
				}
			}
		}

		/// <summary>
		/// Deletes the instances by key source.
		/// </summary>
		/// <param name="NumberOfRowsToDelete">The number of rows to delete.</param>
		/// <param name="SourceKey">The source key.</param>
		/// <param name="InstanceSource">The instance source.</param>
		/// <returns></returns>
		/// <exception cref="System.NotImplementedException"></exception>
		public int DeleteLimitedNumberRowsByKeySource(int NumberOfRowsToDelete, string SourceKey, AlarmInstanceSource InstanceSource) {
			var sourceKeyFieldName = InstanceSource.Equals(AlarmInstanceSource.AlarmDefinition) ? "KeyAlarmDefinition" : "KeyMeterValidationRule";
			using (DbCommand command = db.GetStoredProcCommand(const_proc_deletelimitednumberrows)) {
				db.AddInParameterInt(command, "NumberRowsToDelete", NumberOfRowsToDelete);
				db.AddInParameterInt(command, sourceKeyFieldName, int.Parse(SourceKey));
				try {
					int result = db.ExecuteNonQuery(command);
					return result;
				}
				catch (System.Data.SqlClient.SqlException ex) {
					throw SqlExceptionWrap(ex);
				}
			}
		}

		/// <summary>
		/// Gets the count by key source.
		/// </summary>
		/// <param name="SourceKeysList">The source keys list.</param>
		/// <param name="InstanceSource">The instance source.</param>
		/// <returns></returns>
		public int GetCountByKeySource(IEnumerable<string> SourceKeysList, AlarmInstanceSource InstanceSource) {
			using (DbCommand command = db.GetStoredProcCommand(const_proc_getcountbykeysource)) {
				db.AddInParameterXml(command, "SourceKeys", Helper.BuildXml(SourceKeysList.ToArray()));    
				db.AddInParameterInt(command, "SourceType", InstanceSource.ParseAsInt());
				db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
				db.AddOutParameterInt(command, "RetCount");
				try {

					db.ExecuteNonQuery(command);
					int result = Convert.ToInt32(db.GetParameterValue(command, "RetCount")); ;
					return result;
				}
				catch (System.Data.SqlClient.SqlException ex) {
					throw SqlExceptionWrap(ex);
				}
			}
		}
	}
}
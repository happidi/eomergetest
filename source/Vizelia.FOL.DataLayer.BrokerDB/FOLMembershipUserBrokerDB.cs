﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web.Security;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.DataLayer.BrokerDB {
	/// <summary>
	/// The brokerDB for FOLMembershipUser.
	/// </summary>
	public class FOLMembershipUserBrokerDB : BaseBrokerDB<FOLMembershipUser> {
		private readonly OccupantBrokerDB m_OccupantBrokerDB = new OccupantBrokerDB();

		/// <summary>
		/// The name of the stored procedure that retreives a paged store of FOLMembershipUsers for a specific PortalTemplate.
		/// </summary>
		protected const string const_proc_getallpagingbykeyportaltemplate = AppModule.Energy + "_FOLMembershipUserGetAllPagingByKeyPortalTemplate";
		/// <summary>
		/// The name of the stored procedure that retreives a list of FOLMembershipUsers for a specific PortalTemplate.
		/// </summary>
		protected const string const_proc_getlistbykeyportaltemplate = AppModule.Energy + "_FOLMembershipUserGetListByKeyPortalTemplate";
		/// <summary>
		/// The name of the stored procedure that saves a list of FOLMembershipUsers for a specific portaltemplate.
		/// </summary>
		protected const string const_proc_portaltemplatefolmembershipusersave = AppModule.Energy + "_PortalTemplateFOLMembershipUserSave";

		/// <summary>
		/// Gets the name of the business entity.
		/// </summary>
		/// <value>
		/// The name of the business entity.
		/// </value>
		protected override string BusinessEntityName {
			get {
				return "UsersDetail";
			}
		}
		/// <summary>
		/// Gets the const_proc_prefix.
		/// </summary>
		protected override string const_proc_prefix {
			get {
				return AppModule.Membership;
			}
		}

		/// <summary>
		/// Builds the save params.
		/// </summary>
		/// <param name="command">The command.</param>
		/// <param name="item">The item.</param>
		protected override void BuildSaveParams(DbCommand command, FOLMembershipUser item) {
			throw new NotImplementedException();
		}

		/// <summary>
		/// Fills the object.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="prefixColumn">The prefix column.</param>
		/// <returns></returns>
		protected override FOLMembershipUser FillObject(DataSourceDecorator source, string prefixColumn) {
			return new FOLMembershipUser {
				KeyUser = source.Field<string>("KeyUser"),
				Email = source.Field<string>("Email"),
				Comment = source.Field<string>("Comment"),
				UserName = source.Field<string>("UserName"),
				IsApproved = source.Field<bool>("IsApproved"),
				IsOnline = IsOnline(source.Field<DateTime>("LastActivityDate")),
				LastLoginDate = source.Field<DateTime>("LastLoginDate"),
				IsLockedOut = source.Field<bool>("IsLockedOut"),
				LocalId = source.Field<string>("UserName"),
				ProviderUserKey = new FOLProviderUserKey {
					Guid = source.Field<Guid?>("UserId"),
					KeyOccupant = source.Field<string>("KeyOccupant"),
					KeyUser = source.Field<string>("KeyUser"),
					FirstName = source.Field<string>("FirstName"),
					LastName = source.Field<string>("LastName"),
                    Occupant = m_OccupantBrokerDB.FillObjectFromUser(source, ""),
                    KeyLocation = source.Field<string>(prefixColumn + "KeyLocation"),
                    UserLocationName = source.Field<string>(prefixColumn + "UserLocationName"),
                    UserLocationLevel = source.Field<int?>(prefixColumn + "UserLocationLevel"),
                    UserLocationLocalIdPath = source.Field<string>(prefixColumn + "UserLocationLocalIdPath"),
                    UserLocationLongPath = source.Field<string>(prefixColumn + "UserLocationLongPath"),
                    UserLocationShortPath = source.Field<string>(prefixColumn + "UserLocationShortPath"),
                    UserLocationTypeName = source.Field<string>(prefixColumn + "UserLocationTypeName").ParseAsEnum<HierarchySpatialTypeName>(),
                    ProjectsCount = source.Field<int>(prefixColumn + "ProjectsCount")
				},
				ApplicationName = source.Field<string>("App")
			};

		}

		/// <summary>
		/// Determines whether the specified last activity date is online.
		/// </summary>
		/// <param name="lastActivityDate">The last activity date.</param>
		/// <returns>
		///   <c>true</c> if the specified last activity date is online; otherwise, <c>false</c>.
		/// </returns>
		private static bool IsOnline(DateTime lastActivityDate) {
			TimeSpan value = new TimeSpan(0, Membership.UserIsOnlineTimeWindow, 0);
			DateTime t = DateTime.UtcNow.Subtract(value);
			return lastActivityDate.ToUniversalTime() > t;
		}

		/// <summary>
		/// Gets a list of FOLMembershipUsers for a specific PortalTemplate.
		/// </summary>
		/// <param name="KeyPortalTemplate">The Key PortalTemplate.</param>
		/// <returns></returns>
		public List<FOLMembershipUser> GetListByKeyPortalTemplate(string KeyPortalTemplate) {
			List<FOLMembershipUser> results;
			DbCommand command = db.GetStoredProcCommand(const_proc_getlistbykeyportaltemplate);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyPortalTemplate", KeyPortalTemplate);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}

		/// <summary>
		/// Gets all the FOLMembershipUsers for a specific PortalTemplate.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyPortalTemplate">The Key of the PortalTemplate.</param>
		/// <param name="total">The total number of items.</param>
		/// <returns>The list of items.</returns>
		public List<FOLMembershipUser> GetAllPagingByKeyPortalTemplate(PagingParameter paging, string KeyPortalTemplate, out int total) {
			DbCommand command = db.GetStoredProcCommand(const_proc_getallpagingbykeyportaltemplate);
			command.CommandTimeout = 600;
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyPortalTemplate", KeyPortalTemplate);
			db.AddInParameterInt(command, "start", paging.start);
			db.AddInParameterInt(command, "limit", paging.limit);
			DataSet ds = db.ExecuteDataSet(command);
			if (ds.Tables[0].Rows.Count > 0)
				total = ds.Tables[0].Rows[0].Field<int>("TotalRecord");
			else
				total = 0;
			return FillObjects(ds);
		}


		/// <summary>
		/// Saves the PortalTemplate FOLMembershipUsers.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="users">The users.</param>
		/// <returns></returns>
		public List<FOLMembershipUser> SaveFOLMembershipUserPortalTemplate(PortalTemplate entity, CrudStore<FOLMembershipUser> users) {
			DbCommand command = db.GetStoredProcCommand(const_proc_portaltemplatefolmembershipusersave);
			command.CommandTimeout = 600;
			List<FOLMembershipUser> results;

			if (users == null || users.IsEmpty())
				return null;
			string inserts = null;
			string deletes = null;

			if (users.create != null) {
				inserts = Helper.BuildXml(users.create.Where(p => p.ProviderUserKey.KeyUser != Helper.GetCurrentActor()).Select(p => p.ProviderUserKey.KeyUser).ToArray());
			}

			if (users.destroy != null) {
				deletes = Helper.BuildXml(users.destroy.Select(p => p.ProviderUserKey.KeyUser).ToArray());
			}

			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyPortalTemplate", entity.KeyPortalTemplate);
			db.AddInParameterString(command, "Inserts", inserts);
			db.AddInParameterString(command, "Deletes", deletes);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}

		/// <summary>
		/// Allows to query the entire users table and not only for the current tenant
		/// </summary>
		public bool? IsTenantAware;

		/// <summary>
		/// Determines whether [is tenant aware object].
		/// </summary>
		/// <returns>
		///   <c>true</c> if [is tenant aware object]; otherwise, <c>false</c>.
		/// </returns>
		protected override bool IsTenantAwareObject() {
			return IsTenantAware.HasValue ? IsTenantAware.Value : base.IsTenantAwareObject();
		}
	}
}
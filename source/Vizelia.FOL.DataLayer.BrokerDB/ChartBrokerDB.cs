﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.DataLayer.BrokerDB {

	/// <summary>
	/// Database broker for Chart business entity.
	/// </summary>
	public class ChartBrokerDB : BaseBrokerDB<Chart> {

		/// <summary>
		/// The name of the stored procedure that retreives a paged store of charts for a specific ChartScheduler.
		/// </summary>
		protected const string const_proc_getallpagingbykeychartscheduler = AppModule.Energy + "_ChartGetAllPagingByKeyChartScheduler";

		/// <summary>
		/// The name of the stored procedure that retreives a list of charts for a specific ChartScheduler.
		/// </summary>
		protected const string const_proc_getlistbykeychartscheduler = AppModule.Energy + "_ChartGetListByKeyChartScheduler";

		/// <summary>
		/// The name of the stored procedure that saves a list of charts for a specific chartscheduler.
		/// </summary>
		protected const string const_proc_chartschedulerchartsave = AppModule.Energy + "_ChartSchedulerChartSave";

		/// <summary>
		/// The name of the stored procedure that retreives a paged store of charts for a specific AlarmDefinition.
		/// </summary>
		protected const string const_proc_getallpagingbykeyalarmdefinition = AppModule.Energy + "_ChartGetAllPagingByKeyAlarmDefinition";

		/// <summary>
		/// The name of the stored procedure that retreives a list of charts for a specific AlarmDefinition.
		/// </summary>
		protected const string const_proc_getlistbykeyalarmdefinition = AppModule.Energy + "_ChartGetListByKeyAlarmDefinition";

		/// <summary>
		/// The name of the stored procedure that saves a list of charts for a specific alarm definition.
		/// </summary>
		protected const string const_proc_alarmdefinitionchartsave = AppModule.Energy + "_AlarmDefinitionChartSave";

		/// <summary>
		/// The name of the stored procedure that retreives a list of chart's keys for a specific DynamicDisplay.
		/// </summary>
		protected const string const_proc_getkeyslistbykeydynamicdisplay = AppModule.Energy + "_ChartGetKeysListByKeyDynamicDisplay";

		/// <summary>
		/// The name of the stored procedure that retreives a paged store of charts for a specific Classification Item KPI.
		/// </summary>
		protected const string const_proc_getallpagingbykeyclassificationitemkpi = AppModule.Energy + "_ChartGetAllPagingByKeyClassificationItemKPI";

		internal const string const_zwsp = "\u200B"; // zero-width 

		/// <summary>
		/// The prefix of stored procedures of that provider. 
		/// </summary>
		protected override string const_proc_prefix {
			get {
				return AppModule.Energy;
			}
		}

		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, Chart item) {


			db.AddInParameterString(command, "Key", item.KeyChart);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "LocalId", item.LocalId);
			db.AddInParameterString(command, "Title", item.Title);
			db.AddInParameterString(command, "Description", item.Description);
			db.AddInParameterDateTime(command, "StartDate", item.StartDate);
			db.AddInParameterDateTime(command, "EndDate", item.EndDate);
			db.AddInParameterString(command, "ChartType", item.ChartType.ParseAsString());
			db.AddInParameterString(command, "DataSerieType", item.DataSerieType.ParseAsString());
			db.AddInParameterString(command, "GaugeType", item.GaugeType.ParseAsString());
			db.AddInParameterString(command, "TimeInterval", item.TimeInterval.ParseAsString());
			db.AddInParameterString(command, "Localisation", item.Localisation.ParseAsString());
			db.AddInParameterInt(command, "LocalisationSiteLevel", item.LocalisationSiteLevel);
			db.AddInParameterInt(command, "ClassificationLevel", item.ClassificationLevel);
			db.AddInParameterString(command, "DisplayShaddingEffect", item.DisplayShaddingEffect.ParseAsString());
			db.AddInParameterBool(command, "Display3D", item.Display3D);
			db.AddInParameterBool(command, "DisplayValues", item.DisplayValues);
			db.AddInParameterBool(command, "DisplayStartEndDate", item.DisplayStartEndDate);
			db.AddInParameterBool(command, "DisplayStackSeries", item.DisplayStackSeries);
			db.AddInParameterBool(command, "DisplayLegendVisible", item.DisplayLegendVisible);
			db.AddInParameterBool(command, "DisplayClassifications", item.DisplayClassifications);
			db.AddInParameterInt(command, "DisplayStartEndDateFontSize", item.DisplayStartEndDateFontSize);
			db.AddInParameterInt(command, "DisplayMarkerSize", item.DisplayMarkerSize);
			db.AddInParameterInt(command, "DisplayLineThickness", item.DisplayLineThickness);
			db.AddInParameterInt(command, "DisplayTransparency", item.DisplayTransparency);
			db.AddInParameterInt(command, "DisplayLegendFontSize", item.DisplayLegendFontSize);
			db.AddInParameterInt(command, "DisplayXAxisFontSize", item.DisplayXAxisFontSize);
			db.AddInParameterInt(command, "DisplayLabelTruncationLength", item.DisplayLabelTruncationLength);
			db.AddInParameterInt(command, "DisplayFontSize", item.DisplayFontSize);
			db.AddInParameterInt(command, "DisplayDecimalPrecision", item.DisplayDecimalPrecision);
			db.AddInParameterString(command, "DisplayFontFamily", item.DisplayFontFamily);
			db.AddInParameterString(command, "DisplayMode", item.DisplayMode.ParseAsString());
			db.AddInParameterString(command, "KeyPalette", item.KeyPalette);
			db.AddInParameterBool(command, "DynamicTimeScaleEnabled", item.DynamicTimeScaleEnabled);
			db.AddInParameterInt(command, "DynamicTimeScaleFrequency", item.DynamicTimeScaleFrequency);
			db.AddInParameterString(command, "DynamicTimeScaleInterval", item.DynamicTimeScaleInterval.ParseAsString());
			db.AddInParameterBool(command, "DynamicTimeScaleCalendar", item.DynamicTimeScaleCalendar);
			db.AddInParameterBool(command, "DynamicTimeScaleCalendarEndToNow", item.DynamicTimeScaleCalendarEndToNow);
			db.AddInParameterBool(command, "DynamicTimeScaleCompleteInterval", item.DynamicTimeScaleCompleteInterval);
			db.AddInParameterString(command, "NightStartTime", item.NightStartTime);
			db.AddInParameterString(command, "NightEndTime", item.NightEndTime);
			db.AddInParameterInt(command, "NightColorR", item.NightColorR);
			db.AddInParameterInt(command, "NightColorG", item.NightColorG);
			db.AddInParameterInt(command, "NightColorB", item.NightColorB);
			db.AddInParameterString(command, "KeyDynamicDisplay", item.KeyDynamicDisplay);
			db.AddInParameterBool(command, "UseDynamicDisplay", item.UseDynamicDisplay);
			db.AddInParameterString(command, "KeyCalendarEventCategory", item.KeyCalendarEventCategory);
			db.AddInParameterString(command, "CalendarMode", item.CalendarMode.ParseAsString());
			db.AddInParameterBool(command, "LockFilterSpatial", item.LockFilterSpatial);
			db.AddInParameterString(command, "SpecificAnalysis", item.SpecificAnalysis.ParseAsString());
			db.AddInParameterString(command, "HeatMapMaxColor", item.HeatMapMaxColor);
			db.AddInParameterString(command, "HeatMapMinColor", item.HeatMapMinColor);
			db.AddInParameterString(command, "XAxisFormat", item.XAxisFormat);
			db.AddInParameterString(command, "CorrelationXSerieLocalId", item.CorrelationXSerieLocalId);
			db.AddInParameterString(command, "CorrelationYSerieLocalId", item.CorrelationYSerieLocalId);
			db.AddInParameterString(command, "CorrelationXSerieLocalId2", item.CorrelationXSerieLocalId2);
			db.AddInParameterString(command, "CorrelationYSerieLocalId2", item.CorrelationYSerieLocalId2);
			db.AddInParameterString(command, "DynamicDisplayMainText", RemoveString(item.DynamicDisplayMainText, const_zwsp));
			db.AddInParameterString(command, "DynamicDisplayHeaderText", RemoveString(item.DynamicDisplayHeaderText, const_zwsp));
			db.AddInParameterString(command, "DynamicDisplayFooterText", RemoveString(item.DynamicDisplayFooterText, const_zwsp));
			db.AddInParameterString(command, "DataGridPercentage", item.DataGridPercentage.ParseAsString());
			db.AddInParameterString(command, "TimeZoneId", item.TimeZoneId);
			db.AddInParameterString(command, "DifferenceHighlightSerie1LocalId", item.DifferenceHighlightSerie1LocalId);
			db.AddInParameterString(command, "DifferenceHighlightSerie2LocalId", item.DifferenceHighlightSerie2LocalId);
			db.AddInParameterInt(command, "DifferenceHighlightSerieThreshold", item.DifferenceHighlightSerieThreshold);
			db.AddInParameterBool(command, "EnergyCertificateDisplayElements", item.EnergyCertificateDisplayElements);
			db.AddInParameterDouble(command, "EnergyCertificateTarget", item.EnergyCertificateTarget);
			db.AddInParameterString(command, "KeyEnergyCertificate", item.KeyEnergyCertificate);
			db.AddInParameterDouble(command, "MicroChartTarget", item.MicroChartTarget);
			db.AddInParameterDouble(command, "MicroChartTargetElement", item.MicroChartTargetElement);
			db.AddInParameterBool(command, "MicroChartDisplayElements", item.MicroChartDisplayElements);
			db.AddInParameterString(command, "DisplayLegendSort", item.DisplayLegendSort.ParseAsString());
			db.AddInParameterString(command, "DisplayChartAreaBackgroundStart", item.DisplayChartAreaBackgroundStart);
			db.AddInParameterString(command, "DisplayChartAreaBackgroundEnd", item.DisplayChartAreaBackgroundEnd);
			db.AddInParameterString(command, "DisplayBackground", item.DisplayBackground);
			db.AddInParameterInt(command, "CustomGridRowCount", item.CustomGridRowCount);
			db.AddInParameterInt(command, "CustomGridColumnCount", item.CustomGridColumnCount);
			db.AddInParameterBool(command, "CustomGridHideLines", item.CustomGridHideLines);
			db.AddInParameterString(command, "ChartAreaPicture", item.ChartAreaPicture);
			db.AddInParameterString(command, "LimitSeriesNumber", item.LimitSeriesNumber.ParseAsString());
			db.AddInParameterString(command, "CalendarViewTimeInterval", item.CalendarViewTimeInterval.ParseAsString());
			db.AddInParameterInt(command, "MaxDataPointPerDataSerie", item.MaxDataPointPerDataSerie);
			db.AddInParameterBool(command, "GridTranspose", item.GridTranspose);
			db.AddInParameterString(command, "DynamicDisplayHeaderPicture", item.DynamicDisplayHeaderPicture);
			db.AddInParameterString(command, "DynamicDisplayMainPicture", item.DynamicDisplayMainPicture);
			db.AddInParameterString(command, "DynamicDisplayFooterPicture", item.DynamicDisplayFooterPicture);
			db.AddInParameterBool(command, "IsFavorite", item.IsFavorite);
			db.AddInParameterBool(command, "OpenOnStartup", item.OpenOnStartup);
			db.AddInParameterBool(command, "UseClassificationPath", item.UseClassificationPath);
			db.AddInParameterBool(command, "UseSpatialPath", item.UseSpatialPath);
			db.AddInParameterBool(command, "HideFullSpatialPath", item.HideFullSpatialPath);
			db.AddInParameterInt(command, "XAxisStartDayOfWeek", item.XAxisStartDayOfWeek);
			db.AddInParameterBool(command, "IsKPI", item.IsKPI);
			db.AddInParameterString(command, "DisplayFontColor", item.DisplayFontColor);
			db.AddInParameterBool(command, "KPIDisableClassificationLevelSlider", item.KPIDisableClassificationLevelSlider);
			db.AddInParameterBool(command, "KPIDisableDisplayModeSlider", item.KPIDisableDisplayModeSlider);
			db.AddInParameterBool(command, "KPIDisableLimitSeriesNumberSlider", item.KPIDisableLimitSeriesNumberSlider);
			db.AddInParameterBool(command, "KPIDisableLocalisationSlider", item.KPIDisableLocalisationSlider);
			db.AddInParameterBool(command, "KPIDisableMeterClassificationSelection", item.KPIDisableMeterClassificationSelection);
			db.AddInParameterBool(command, "KPIDisableMeterSelection", item.KPIDisableMeterSelection);
			db.AddInParameterBool(command, "KPIDisableSpatialSelection", item.KPIDisableSpatialSelection);
			db.AddInParameterBool(command, "KPIDisableTimeIntervalSlider", item.KPIDisableTimeIntervalSlider);
			db.AddInParameterBool(command, "KPIDisableDateRangeMenu", item.KPIDisableDateRangeMenu);
			db.AddInParameterBool(command, "YAxisFullStacked", item.YAxisFullStacked);
			db.AddInParameterBool(command, "DrillDownEnabled", item.DrillDownEnabled);
			db.AddInParameterString(command, "DrillDownMode", item.DrillDownMode.ParseAsString());
			db.AddInParameterString(command, "KeyClassificationItemKPI", item.KeyClassificationItemKPI);

			db.AddInParameterString(command, "KPIId", item.KPIId);
			db.AddInParameterString(command, "KPISetupInstructions", item.KPISetupInstructions);
			db.AddInParameterString(command, "KPIBuild", item.KPIBuild);
			db.AddInParameterString(command, "KeyKPIPreviewPicture", item.KeyKPIPreviewPicture);
			db.AddInParameterDateTime(command, "KPICopyDate", item.KPICopyDate);


			db.AddInParameterBool(command, "UseDataAcquisitionEndPoint", item.UseDataAcquisitionEndPoint);

			db.AddInParameterString(command, "LegendPosition", item.LegendPosition.ParseAsString());
			db.AddInParameterString(command, "InfoIconPosition", item.InfoIconPosition.ParseAsString());

			db.AddInParameterBool(command, "DisplayLegendPlain", item.DisplayLegendPlain);
			db.AddInParameterInt(command, "MaxMeter", item.MaxMeter);

			db.AddInParameterBool(command, "HideMeterDataValidity", item.HideMeterDataValidity);
			db.AddInParameterBool(command, "HideDatagridInExport", item.HideDatagridInExport);

			db.AddInParameterInt(command, "GridTransposeClassificationWidth", item.GridTransposeClassificationWidth);
			db.AddInParameterInt(command, "GridTransposeLocationWidth", item.GridTransposeLocationWidth);
			db.AddInParameterBool(command, "GridHideUnit", item.GridHideUnit);
			db.AddInParameterBool(command, "GridAggregateByCalculatedSerieClassification", item.GridAggregateByCalculatedSerieClassification);
			db.AddInParameterBool(command, "DetailedStatusIconEnabled", item.DetailedStatusIconEnabled);

            db.AddInParameterString(command, "DynamicDisplayKeyLocalizationCulture", item.DynamicDisplayKeyLocalizationCulture);

		}

		private static string RemoveString(string value, string stringToRemove) {
			return value != null ? value.Replace(stringToRemove, string.Empty) : null;
		}

		/// <summary>
		/// Populates a Chart business entity from a DataSourceDecorator.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated Chart business entity.</returns>
		protected override Chart FillObject(DataSourceDecorator source, string prefixColumn) {
			return new Chart {
				KeyChart = source.Field<int>(prefixColumn + "KeyChart").ToString(CultureInfo.InvariantCulture),
				Application = ContextHelper.ApplicationName,
				Title = source.Field<string>(prefixColumn + "Title"),
				LocalId = source.Field<string>(prefixColumn + "LocalId"),
				Description = source.Field<string>(prefixColumn + "Description"),
				StartDate = source.Field<DateTime>(prefixColumn + "StartDate"),
				EndDate = source.Field<DateTime>(prefixColumn + "EndDate"),
				ChartType = source.Field<string>(prefixColumn + "ChartType").ParseAsEnum<ChartType>(),
				DataSerieType = source.Field<string>(prefixColumn + "DataSerieType").ParseAsEnum<DataSerieType>(),
				GaugeType = source.Field<string>(prefixColumn + "GaugeType").ParseAsEnum<GaugeType>(),
				TimeInterval = source.Field<string>(prefixColumn + "TimeInterval").ParseAsEnum<AxisTimeInterval>(),
				Localisation = source.Field<string>(prefixColumn + "Localisation").ParseAsEnum<ChartLocalisation>(),
				LocalisationSiteLevel = source.Field<int>(prefixColumn + "LocalisationSiteLevel"),
				ClassificationLevel = source.Field<int>(prefixColumn + "ClassificationLevel"),
				DisplayShaddingEffect = source.Field<string>(prefixColumn + "DisplayShaddingEffect").ParseAsEnum<ChartShaddingEffect>(),
				Display3D = source.Field<bool>(prefixColumn + "Display3D"),
				DisplayValues = source.Field<bool>(prefixColumn + "DisplayValues"),
				DisplayStartEndDate = source.Field<bool>(prefixColumn + "DisplayStartEndDate"),
				DisplayStackSeries = source.Field<bool>(prefixColumn + "DisplayStackSeries"),
				DisplayLegendVisible = source.Field<bool>(prefixColumn + "DisplayLegendVisible"),
				DisplayStartEndDateFontSize = source.Field<int>(prefixColumn + "DisplayStartEndDateFontSize"),
				DisplayMarkerSize = source.Field<int>(prefixColumn + "DisplayMarkerSize"),
				DisplayLineThickness = source.Field<int>(prefixColumn + "DisplayLineThickness"),
				DisplayTransparency = source.Field<int>(prefixColumn + "DisplayTransparency"),
				DisplayLegendFontSize = source.Field<int>(prefixColumn + "DisplayLegendFontSize"),
				DisplayFontSize = source.Field<int>(prefixColumn + "DisplayFontSize"),
				DisplayXAxisFontSize = source.Field<int?>(prefixColumn + "DisplayXAxisFontSize"),
				DisplayLabelTruncationLength = source.Field<int?>(prefixColumn + "DisplayLabelTruncationLength"),
				DisplayFontFamily = source.Field<string>(prefixColumn + "DisplayFontFamily"),
				DisplayDecimalPrecision = source.Field<int>(prefixColumn + "DisplayDecimalPrecision"),
				DisplayMode = source.Field<string>(prefixColumn + "DisplayMode").ParseAsEnum<ChartDisplayMode>(),
				KeyPalette = source.Field<string>(prefixColumn + "KeyPalette"),
				DynamicTimeScaleEnabled = source.Field<bool>(prefixColumn + "DynamicTimeScaleEnabled"),
				DynamicTimeScaleFrequency = source.Field<int>(prefixColumn + "DynamicTimeScaleFrequency"),
				DynamicTimeScaleInterval = source.Field<string>(prefixColumn + "DynamicTimeScaleInterval").ParseAsEnum<AxisTimeInterval>(),
				DynamicTimeScaleCalendar = source.Field<bool>(prefixColumn + "DynamicTimeScaleCalendar"),
				DynamicTimeScaleCalendarEndToNow = source.Field<bool?>(prefixColumn + "DynamicTimeScaleCalendarEndToNow") ?? false,
				DynamicTimeScaleCompleteInterval = source.Field<bool>(prefixColumn + "DynamicTimeScaleCompleteInterval"),
				NightStartTime = source.Field<string>(prefixColumn + "NightStartTime"),
				NightEndTime = source.Field<string>(prefixColumn + "NightEndTime"),
				NightColorR = source.Field<int?>(prefixColumn + "NightColorR"),
				NightColorG = source.Field<int?>(prefixColumn + "NightColorG"),
				NightColorB = source.Field<int?>(prefixColumn + "NightColorB"),
				KeyDynamicDisplay = source.Field<string>(prefixColumn + "KeyDynamicDisplay"),
				UseDynamicDisplay = source.Field<bool>(prefixColumn + "UseDynamicDisplay"),
				KeyCalendarEventCategory = source.Field<string>(prefixColumn + "KeyCalendarEventCategory"),
				CalendarMode = source.Field<string>(prefixColumn + "CalendarMode").ParseAsEnum<ChartCalendarMode>(),
				LockFilterSpatial = source.Field<bool>(prefixColumn + "LockFilterSpatial"),
				SpecificAnalysis = source.Field<string>(prefixColumn + "SpecificAnalysis").ParseAsEnum<ChartSpecificAnalysis>(),
				HeatMapMaxColor = source.Field<string>(prefixColumn + "HeatMapMaxColor"),
				HeatMapMinColor = source.Field<string>(prefixColumn + "HeatMapMinColor"),
				XAxisFormat = source.Field<string>(prefixColumn + "XAxisFormat"),
				CorrelationXSerieLocalId = source.Field<string>(prefixColumn + "CorrelationXSerieLocalId"),
				CorrelationYSerieLocalId = source.Field<string>(prefixColumn + "CorrelationYSerieLocalId"),
				CorrelationXSerieLocalId2 = source.Field<string>(prefixColumn + "CorrelationXSerieLocalId2"),
				CorrelationYSerieLocalId2 = source.Field<string>(prefixColumn + "CorrelationYSerieLocalId2"),
				DynamicDisplayMainText = RemoveString(source.Field<string>(prefixColumn + "DynamicDisplayMainText"), const_zwsp),
				DynamicDisplayHeaderText = RemoveString(source.Field<string>(prefixColumn + "DynamicDisplayHeaderText"), const_zwsp),
				DynamicDisplayFooterText = RemoveString(source.Field<string>(prefixColumn + "DynamicDisplayFooterText"), const_zwsp),
				DataGridPercentage = source.Field<string>(prefixColumn + "DataGridPercentage").ParseAsEnum<ChartDataGridPercentage>(),
				TimeZoneId = source.Field<string>(prefixColumn + "TimeZoneId"),
				DifferenceHighlightSerie1LocalId = source.Field<string>(prefixColumn + "DifferenceHighlightSerie1LocalId"),
				DifferenceHighlightSerie2LocalId = source.Field<string>(prefixColumn + "DifferenceHighlightSerie2LocalId"),
				DifferenceHighlightSerieThreshold = source.Field<int>(prefixColumn + "DifferenceHighlightSerieThreshold"),
				EnergyCertificateDisplayElements = source.Field<bool>(prefixColumn + "EnergyCertificateDisplayElements"),
				EnergyCertificateTarget = source.Field<double?>(prefixColumn + "EnergyCertificateTarget"),
				KeyEnergyCertificate = source.Field<string>(prefixColumn + "KeyEnergyCertificate"),
				MicroChartDisplayElements = source.Field<bool>(prefixColumn + "MicroChartDisplayElements"),
				MicroChartTarget = source.Field<double?>(prefixColumn + "MicroChartTarget"),
				MicroChartTargetElement = source.Field<double?>(prefixColumn + "MicroChartTargetElement"),
				DisplayLegendSort = source.Field<string>(prefixColumn + "DisplayLegendSort").ParseAsEnum<ChartLegendSort>(),
				DisplayChartAreaBackgroundStart = source.Field<string>(prefixColumn + "DisplayChartAreaBackgroundStart"),
				DisplayChartAreaBackgroundEnd = source.Field<string>(prefixColumn + "DisplayChartAreaBackgroundEnd"),
				DisplayBackground = source.Field<string>(prefixColumn + "DisplayBackground"),
				DisplayClassifications = source.Field<bool?>(prefixColumn + "DisplayClassifications") ?? true,
				CustomGridRowCount = source.Field<int>(prefixColumn + "CustomGridRowCount"),
				CustomGridColumnCount = source.Field<int>(prefixColumn + "CustomGridColumnCount"),
				CustomGridHideLines = source.Field<bool>(prefixColumn + "CustomGridHideLines"),
				ChartAreaPicture = source.Field<string>(prefixColumn + "ChartAreaPicture"),
				LimitSeriesNumber = source.Field<string>(prefixColumn + "LimitSeriesNumber").ParseAsEnum<ChartLimitSeriesNumber>(),
				CalendarViewTimeInterval = source.Field<string>(prefixColumn + "CalendarViewTimeInterval").ParseAsEnum<AxisTimeInterval>(),
				MaxDataPointPerDataSerie = source.Field<int>(prefixColumn + "MaxDataPointPerDataSerie"),
				GridTranspose = source.Field<bool>(prefixColumn + "GridTranspose"),
				DynamicDisplayHeaderPicture = source.Field<string>(prefixColumn + "DynamicDisplayHeaderPicture"),
				DynamicDisplayMainPicture = source.Field<string>(prefixColumn + "DynamicDisplayMainPicture"),
				DynamicDisplayFooterPicture = source.Field<string>(prefixColumn + "DynamicDisplayFooterPicture"),
				IsFavorite = source.Field<bool?>(prefixColumn + "IsFavorite") ?? false,
				OpenOnStartup = source.Field<bool?>(prefixColumn + "OpenOnStartup") ?? false,
				UseSpatialPath = source.Field<bool?>(prefixColumn + "UseSpatialPath") ?? false,
				HideFullSpatialPath = source.Field<bool?>(prefixColumn + "HideFullSpatialPath") ?? false,
				UseClassificationPath = source.Field<bool?>(prefixColumn + "UseClassificationPath") ?? true,
				XAxisStartDayOfWeek = source.Field<int?>(prefixColumn + "XAxisStartDayOfWeek") ?? 1,
				IsKPI = source.Field<bool?>(prefixColumn + "IsKPI") ?? false,
				DisplayFontColor = source.Field<string>(prefixColumn + "DisplayFontColor"),
				YAxisFullStacked = source.Field<bool?>(prefixColumn + "YAxisFullStacked") ?? false,
				KPIDisableClassificationLevelSlider = source.Field<bool?>(prefixColumn + "KPIDisableClassificationLevelSlider") ?? false,
				KPIDisableDisplayModeSlider = source.Field<bool?>(prefixColumn + "KPIDisableDisplayModeSlider") ?? false,
				KPIDisableLimitSeriesNumberSlider = source.Field<bool?>(prefixColumn + "KPIDisableLimitSeriesNumberSlider") ?? false,
				KPIDisableLocalisationSlider = source.Field<bool?>(prefixColumn + "KPIDisableLocalisationSlider") ?? false,
				KPIDisableMeterClassificationSelection = source.Field<bool?>(prefixColumn + "KPIDisableMeterClassificationSelection") ?? false,
				KPIDisableMeterSelection = source.Field<bool?>(prefixColumn + "KPIDisableMeterSelection") ?? false,
				KPIDisableSpatialSelection = source.Field<bool?>(prefixColumn + "KPIDisableSpatialSelection") ?? false,
				KPIDisableTimeIntervalSlider = source.Field<bool?>(prefixColumn + "KPIDisableTimeIntervalSlider") ?? false,
				KPIDisableDateRangeMenu = source.Field<bool?>(prefixColumn + "KPIDisableDateRangeMenu") ?? false,
				VirtualFilePath = source.Field<string>(prefixColumn + "VirtualFilePath"),
				DrillDownEnabled = source.Field<bool?>(prefixColumn + "DrillDownEnabled") ?? false,
				DrillDownMode = (source.Field<string>(prefixColumn + "DrillDownMode") ?? "Autonomous").ParseAsEnum<ChartDrillDownMode>(),
				UseDataAcquisitionEndPoint = source.Field<bool?>(prefixColumn + "UseDataAcquisitionEndPoint") ?? false,

				KeyClassificationItemKPI = source.Field<string>(prefixColumn + "KeyClassificationItemKPI"),
				KeyClassificationItemPathKPI = source.Field<string>(prefixColumn + "KeyClassificationItemPathKPI"),
				ClassificationItemKPILongPath = source.Field<string>(prefixColumn + "ClassificationItemKPILongPath"),
				ClassificationItemKPILocalIdPath = source.Field<string>(prefixColumn + "ClassificationItemKPILocalIdPath"),
				ClassificationItemKPITitle = source.Field<string>(prefixColumn + "ClassificationItemKPITitle"),
				ClassificationItemKPILevel = source.Field<int?>(prefixColumn + "ClassificationItemKPILevel") ?? 0,

				KPIId = source.Field<string>(prefixColumn + "KPIId"),
				KPISetupInstructions = source.Field<string>(prefixColumn + "KPISetupInstructions"),
				KPICopyDate = source.Field<DateTime?>(prefixColumn + "KPICopyDate"),
				KeyKPIPreviewPicture = source.Field<string>(prefixColumn + "KeyKPIPreviewPicture"),
				TenantKeyKPIPreviewPicture = source.Field<string>(prefixColumn + "TenantKeyKPIPreviewPicture"),
				KPIBuild = source.Field<string>(prefixColumn + "KPIBuild"),

				LegendPosition = (source.Field<string>(prefixColumn + "LegendPosition") ?? "ChartTitle").ParseAsEnum<ChartLegendPosition>(),
				InfoIconPosition = (source.Field<string>(prefixColumn + "InfoIconPosition") ?? "BottomRight").ParseAsEnum<ChartInfoIconPosition>(),

				DisplayLegendPlain = source.Field<bool?>(prefixColumn + "DisplayLegendPlain") ?? false,
				MaxMeter = source.Field<int?>(prefixColumn + "MaxMeter") ?? 100,

				UsedCount = source.Field<int?>(prefixColumn + "UsedCount") ?? 0,
				HideMeterDataValidity = source.Field<bool?>(prefixColumn + "HideMeterDataValidity") ?? false,
				HideDatagridInExport = source.Field<bool?>(prefixColumn + "HideDatagridInExport") ?? false,

				GridHideUnit = source.Field<bool?>(prefixColumn + "GridHideUnit") ?? false,
				GridAggregateByCalculatedSerieClassification = source.Field<bool?>(prefixColumn + "GridAggregateByCalculatedSerieClassification") ?? true,
				GridTransposeClassificationWidth = source.Field<int?>(prefixColumn + "GridTransposeClassificationWidth") ?? 150,
				GridTransposeLocationWidth = source.Field<int?>(prefixColumn + "GridTransposeLocationWidth") ?? 120,

				AlgorithmsCount = source.Field<int?>(prefixColumn + "AlgorithmsCount") ?? 0,
				
				DetailedStatusIconEnabled = source.Field<bool>(prefixColumn + "DetailedStatusIconEnabled"),
                DynamicDisplayKeyLocalizationCulture = source.Field<string>(prefixColumn + "DynamicDisplayKeyLocalizationCulture")
			};
		}

		/// <summary>
		/// Gets a list of Charts for a specific ChartScheduler.
		/// </summary>
		/// <param name="KeyChartScheduler">The Key ChartScheduler.</param>
		/// <returns></returns>
		public List<Chart> GetListByKeyChartScheduler(string KeyChartScheduler) {
			List<Chart> results;
			DbCommand command = db.GetStoredProcCommand(const_proc_getlistbykeychartscheduler);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyChartScheduler", KeyChartScheduler);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}

		/// <summary>
		/// Gets all the charts for a specific ChartScheduler.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyChartScheduler">The Key of the ChartScheduler.</param>
		/// <param name="total">The total number of items.</param>
		/// <returns>The list of items.</returns>
		public List<Chart> GetAllPagingByKeyChartScheduler(PagingParameter paging, string KeyChartScheduler, out int total) {
			DbCommand command = db.GetStoredProcCommand(const_proc_getallpagingbykeychartscheduler);
			command.CommandTimeout = 600;
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyChartScheduler", KeyChartScheduler);
			db.AddInParameterInt(command, "start", paging.start);
			db.AddInParameterInt(command, "limit", paging.limit);
			DataSet ds = db.ExecuteDataSet(command);
			if (ds.Tables[0].Rows.Count > 0)
				total = ds.Tables[0].Rows[0].Field<int>("TotalRecord");
			else
				total = 0;
			return FillObjects(ds);
		}


		/// <summary>
		/// Saves the ChartScheduler Charts.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="charts">The charts.</param>
		/// <returns></returns>
		public List<Chart> SaveChartChartScheduler(ChartScheduler entity, CrudStore<Chart> charts) {
			DbCommand command = db.GetStoredProcCommand(const_proc_chartschedulerchartsave);
			command.CommandTimeout = 600;
			List<Chart> results;

			if (charts == null || charts.IsEmpty())
				return null;
			string inserts = null;
			string deletes = null;

			if (charts.create != null) {
				inserts = Helper.BuildXml(charts.create.Select(p => p.KeyChart).ToArray());
			}

			if (charts.destroy != null) {
				deletes = Helper.BuildXml(charts.destroy.Select(p => p.KeyChart).ToArray());
			}

			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyChartScheduler", entity.KeyChartScheduler);
			db.AddInParameterString(command, "Inserts", inserts);
			db.AddInParameterString(command, "Deletes", deletes);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}


		/// <summary>
		/// Gets a list of Charts for a specific AlarmDefinition.
		/// </summary>
		/// <param name="KeyAlarmDefinition">The key alarm definition.</param>
		/// <returns></returns>
		public List<Chart> GetListByKeyAlarmDefinition(string KeyAlarmDefinition) {
			List<Chart> results;
			DbCommand command = db.GetStoredProcCommand(const_proc_getlistbykeyalarmdefinition);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyAlarmDefinition", KeyAlarmDefinition);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}

		/// <summary>
		/// Gets all the charts for a specific AlarmDefinition.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyAlarmDefinition">The Key of the AlarmDefinition.</param>
		/// <param name="total">The total number of items.</param>
		/// <returns>The list of items.</returns>
		public List<Chart> GetAllPagingByKeyAlarmDefinition(PagingParameter paging, string KeyAlarmDefinition, out int total) {
			DbCommand command = db.GetStoredProcCommand(const_proc_getallpagingbykeyalarmdefinition);
			command.CommandTimeout = 600;
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyAlarmDefinition", KeyAlarmDefinition);
			db.AddInParameterInt(command, "start", paging.start);
			db.AddInParameterInt(command, "limit", paging.limit);
			DataSet ds = db.ExecuteDataSet(command);
			if (ds.Tables[0].Rows.Count > 0)
				total = ds.Tables[0].Rows[0].Field<int>("TotalRecord");
			else
				total = 0;
			return FillObjects(ds);
		}


		/// <summary>
		/// Saves the AlarmDefinition Charts.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="charts">The charts.</param>
		/// <returns></returns>
		public List<Chart> SaveChartAlarmDefinition(AlarmDefinition entity, CrudStore<Chart> charts) {
			DbCommand command = db.GetStoredProcCommand(const_proc_alarmdefinitionchartsave);
			command.CommandTimeout = 600;
			List<Chart> results;

			if (charts == null || charts.IsEmpty())
				return null;
			string inserts = null;
			string deletes = null;

			if (charts.create != null) {
				inserts = Helper.BuildXml(charts.create.Select(p => p.KeyChart).ToArray());
			}

			if (charts.destroy != null) {
				deletes = Helper.BuildXml(charts.destroy.Select(p => p.KeyChart).ToArray());
			}

			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyAlarmDefinition", entity.KeyAlarmDefinition);
			db.AddInParameterString(command, "Inserts", inserts);
			db.AddInParameterString(command, "Deletes", deletes);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}

		/// <summary>
		/// Gets all the chart's keys that are using the specified dynamic display.
		/// </summary>
		/// <param name="KeyDynamicDisplay">The key dynamic display.</param>
		/// <returns></returns>
		public List<string> GetKeysByKeyDynamicDisplay(string KeyDynamicDisplay) {
			var retVal = new List<string>();

			DbCommand command = db.GetStoredProcCommand(const_proc_getkeyslistbykeydynamicdisplay);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyDynamicDisplay", KeyDynamicDisplay);

			using (IDataReader reader = db.ExecuteReader(command)) {
				while (reader.Read()) {
					retVal.Add(reader["KeyChart"].ToString());
				}
			}
			return retVal;
		}



		/// <summary>
		/// Gets all paging by key classification item KPI.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="KeyClassificationItem">The key classification item.</param>
		/// <param name="roles">The roles.</param>
		/// <param name="total">The total.</param>
		/// <returns></returns>
		public List<Chart> GetAllPagingByKeyClassificationItemKPI(PagingParameter paging, string KeyClassificationItem, List<AuthorizationItem> roles, out int total) {
			DbCommand command = db.GetStoredProcCommand(const_proc_getallpagingbykeyclassificationitemkpi);
			command.CommandTimeout = 600;

			string rolesXML = null;

			if (roles != null) {
				rolesXML = Helper.BuildXml(roles.Select(p => p.Name).ToArray());
			}

			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyClassificationItem", KeyClassificationItem);
			db.AddInParameterString(command, "Roles", rolesXML);
			db.AddInParameterInt(command, "start", paging.start);
			db.AddInParameterInt(command, "limit", paging.limit);

			AddPagingParameterOrderBy(paging, command);
			if (paging != null)
				paging.ExtractInnerJoinFilters();
			AddPagingParameterFilter(paging, command);

			DataSet ds = db.ExecuteDataSet(command);
			if (ds.Tables[0].Rows.Count > 0)
				total = ds.Tables[0].Rows[0].Field<int>("TotalRecord");
			else
				total = 0;
			return FillObjects(ds);
		}

		/// <summary>
		/// Determines whether the current user has the needed filters to see and modify the given object.
		/// </summary>
		/// <param name="item">The item to check on.</param>
		/// <returns>
		///   <c>true</c> if the current user has the needed filters to see and modify the given object; otherwise, <c>false</c>.
		/// </returns>
		protected override bool IsUserAllowed(Chart item) {
			// Specifc case for KPI chart which are shared without the need of putting them in projects.
			if (item.IsKPI) {
				return true;
			}
			else {
				return base.IsUserAllowed(item);
			}
		}
	}
}

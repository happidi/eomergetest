﻿using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.DataLayer.BrokerDB {

	/// <summary>
	/// Database broker for PlaylistPage business entity.
	/// </summary>
	public class PlaylistPageBrokerDB : BaseBrokerDB<PlaylistPage> {

		/// <summary>
		/// The name of the stored procedure that retreives a list of Portal Tabs for a specific Portal.
		/// </summary>
		protected const string const_proc_getlistbyplaylist = AppModule.Energy + "_PlaylistPageGetListByKeyPlaylist";

		/// <summary>
		/// The name of the stored procedure that retreives a list of Portal Tabs for a specific Portal.
		/// </summary>
		protected const string const_proc_getallpagingbykeyplaylist = AppModule.Energy + "_PlaylistPageGetAllPagingByKeyPlaylist";

		/// <summary>
		/// The name of the stored procedure that saves a list of meters for a specific Portal.
		/// </summary>
		protected const string const_proc_playlistpagesave = AppModule.Energy + "_PlaylistPlaylistPageSave";

		/// <summary>
		/// The prefix of stored procedures of that provider. 
		/// </summary>
		protected override string const_proc_prefix {
			get {
				return AppModule.Energy;
			}
		}

		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, PlaylistPage item) {
			db.AddInParameterString(command, "Key", item.KeyPlaylistPage);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "LocalId", item.LocalId);
			db.AddInParameterString(command, "KeyPlaylist", item.KeyPlaylist);
			db.AddInParameterString(command, "KeyPortalTab", item.KeyPortalTab);
			db.AddInParameterInt(command, "Order", item.Order);
			db.AddInParameterInt(command, "Duration", item.Duration);
			db.AddInParameterString(command, "KeyDocument", item.KeyDocument);
		}

		/// <summary>
		/// Populates a PlaylistPage business entity from a DataSourceDecorator.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated PlaylistPage business entity.</returns>
		protected override PlaylistPage FillObject(DataSourceDecorator source, string prefixColumn) {
			var retVal = new PlaylistPage {
				KeyPlaylistPage = source.Field<int>(prefixColumn + "KeyPlaylistPage").ToString(CultureInfo.InvariantCulture),
				KeyPlaylist = source.Field<int>(prefixColumn + "KeyPlaylist").ToString(CultureInfo.InvariantCulture),
				LocalId = source.Field<string>(prefixColumn + "LocalId"),
				KeyPortalTab = source.Field<int>(prefixColumn + "KeyPortalTab").ToString(CultureInfo.InvariantCulture),
				PortalTabTitle = source.Field<string>(prefixColumn + "PortalTabTitle"),
				PortalWindowTitle = source.Field<string>(prefixColumn + "PortalWindowTitle"),
				Order = source.Field<int>(prefixColumn + "Order"),
				Duration = source.Field<int>(prefixColumn + "Duration"),
				PlaylistHeight = source.Field<int>(prefixColumn + "PlaylistHeight"),
				PlaylistWidth = source.Field<int>(prefixColumn + "PlaylistWidth")
			};

			var keyDocument = source.Field<int?>(prefixColumn + "KeyDocument");
			if (keyDocument.HasValue) {
				retVal.KeyDocument = keyDocument.Value.ToString(CultureInfo.InvariantCulture);
			}
			return retVal;
		}


		/// <summary>
		/// Get the list of saved PlaylistPage from a Playlist.
		/// </summary>
		/// <param name="KeyPlaylist">The key of the portal window.</param>
		public List<PlaylistPage> GetListByKeyPlaylist(string KeyPlaylist) {
			List<PlaylistPage> results;
			DbCommand command = db.GetStoredProcCommand(const_proc_getlistbyplaylist);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyPlaylist", KeyPlaylist);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}

		/// <summary>
		/// Gets all the PlaylistPage for a specific Portal.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyPlaylist">The Key of the portal window.</param>
		/// <param name="total">The total number of items.</param>
		/// <returns>The list of items.</returns>
		public List<PlaylistPage> GetAllPagingByKeyPlaylist(PagingParameter paging, string KeyPlaylist, out int total) {
			DbCommand command = db.GetStoredProcCommand(const_proc_getallpagingbykeyplaylist);
			command.CommandTimeout = 600;
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyPlaylist", KeyPlaylist);
			db.AddInParameterInt(command, "start", paging.start);
			db.AddInParameterInt(command, "limit", paging.limit);
			DataSet ds = db.ExecuteDataSet(command);
			if (ds.Tables[0].Rows.Count > 0)
				total = ds.Tables[0].Rows[0].Field<int>("TotalRecord");
			else
				total = 0;
			return FillObjects(ds);
		}

		/// <summary>
		/// Saves the portal PlaylistPages.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="PlaylistPage">The PlaylistPage store.</param>
		/// <returns></returns>
		public List<PlaylistPage> SavePlaylistPlaylistPage(Playlist entity, CrudStore<PlaylistPage> PlaylistPage) {
			DbCommand command = db.GetStoredProcCommand(const_proc_playlistpagesave);
			command.CommandTimeout = 600;
			List<PlaylistPage> results;

			if (PlaylistPage == null || PlaylistPage.IsEmpty())
				return null;
			string deletes = null;


			if (PlaylistPage.destroy != null) {
				deletes = Helper.BuildXml(PlaylistPage.destroy.Select(p => p.KeyPlaylistPage).ToArray());
			}

			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyPlaylist", entity.KeyPlaylist);
			db.AddInParameterString(command, "Inserts", null);
			db.AddInParameterString(command, "Deletes", deletes);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}
	}
}
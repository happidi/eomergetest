﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Vizelia.FOL.BusinessEntities;
using System.Data;
using Vizelia.FOL.Common;
using System.Data.Common;
using Vizelia.FOL.DataLayer.Interfaces;
using System.Web.Security;

namespace Vizelia.FOL.DataLayer.BrokerDB {

	/// <summary>
	/// Database broker for Task business entity.
	/// </summary>
	public class TaskBrokerDB : BaseBrokerDB<Task> {

		/// <summary>
		/// The name of the stored procedure that retreives a list of tasks  for a specified ActionRequest.
		/// </summary>
		const string const_proc_getlistbykeyactionrequest = AppModule.ServiceDesk + "_TaskGetListByKeyActionRequest";

		/// <summary>
		/// The name of the stored procedure that retreives a paged list of tasks  for a specified ActionRequest.
		/// </summary>
		const string const_proc_getallpagingbykeyactionrequest = AppModule.ServiceDesk + "_TaskGetAllPagingByKeyActionRequest";
		

		/// <summary>
		/// The name of the stored procedure that retreives the item by its instanceId.
		/// </summary>
		protected const string const_proc_getitembyinstanceid = AppModule.ServiceDesk + "_TaskGetItemByInstanceId";


		/// <summary>
		/// Gets a value indicating whether this instance is localized.
		/// </summary>
		/// <remarks></remarks>
		protected override bool IsLocalized {
			get {
				return true;
			}
		}

		/// <summary>
		/// Gets the const_proc_prefix.
		/// </summary>
		/// <value>The const_proc_prefix.</value>
		protected override string const_proc_prefix {
			get {
				return AppModule.ServiceDesk;
			}
		}

		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, Task item) {
			db.AddInParameterString(command, "Key", item.KeyTask);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyActionRequest", item.KeyActionRequest);
			db.AddInParameterString(command, "Name", item.Name);
			db.AddInParameterInt(command, "Priority", item.Priority);
			db.AddInParameterString(command, "Description", item.Description);
			
			db.AddInParameterString(command, "KeyClassificationItem", item.KeyClassificationItem);
			db.AddInParameterString(command, "KeyActor", item.KeyActor);
			db.AddInParameterDateTime(command, "ActualStart", item.ActualStart);
			db.AddInParameterDateTime(command, "ScheduleStart", item.ScheduleStart);
			db.AddInParameterDateTime(command, "ActualFinish", item.ActualFinish);
			db.AddInParameterInt(command, "Duration", item.Duration);

		}

		/// <summary>
		/// Populates a Task business entity from a DataSourceDecorator.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated Task business entity.</returns>
		protected override Task FillObject(DataSourceDecorator source, string prefixColumn) {
			return new Task {
				KeyTask = source.Field<string>(prefixColumn + "KeyTask"),
				KeyActionRequest = source.Field<string>(prefixColumn + "KeyActionRequest"),
				Name = source.Field<string>(prefixColumn + "Name"),
				Description = source.Field<string>(prefixColumn + "Description"),
				State = source.Field<string>(prefixColumn + "State"),
				Priority = source.Field<int?>(prefixColumn + "Priority"),
				Duration = source.Field<int?>(prefixColumn + "Duration"),
				ActualStart = source.Field<DateTime?>(prefixColumn + "ActualStart"),
				ScheduleStart = source.Field<DateTime?>(prefixColumn + "ScheduleStart"),
				ActualFinish = source.Field<DateTime?>(prefixColumn + "ActualFinish"),
				KeyActor = source.Field<string>(prefixColumn + "KeyActor"),
				ActorFirstName = source.Field<string>(prefixColumn + "ActorFirstName"),
				ActorLastName = source.Field<string>(prefixColumn + "ActorLastName"),
				ActorMail = source.Field<string>(prefixColumn + "ActorMail"),
				KeyClassificationItem = source.Field<string>(prefixColumn + "KeyClassificationItem"),
				ClassificationLongPath = source.Field<string>(prefixColumn + "ClassificationLongPath"),
				KeyLocation = source.Field<string>(prefixColumn + "KeyLocation")
			};
		}

		/// <summary>
		/// Gets the list of tasks by  key ActionRequest item.
		/// </summary>
		/// <param name="keyActionRequest">The key ActionRequest item.</param>
		/// <returns></returns>
		public List<Task> GetListByKeyActionRequest(string keyActionRequest) {
			List<Task> result = null;
			DbCommand command = db.GetStoredProcCommand(const_proc_getlistbykeyactionrequest);
			db.AddInParameterString(command, "KeyActionRequest", keyActionRequest);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "Culture", Helper.GetCurrentCultureShort());
			using (IDataReader reader = db.ExecuteReader(command)) {
				result = FillObjects(reader);
			}
			return result;
		}

		/// <summary>
		/// Gets all taks paged by key action request.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="KeyActionRequest">The key action request.</param>
		/// <param name="total">The total.</param>
		/// <returns></returns>
		public List<Task> GetAllPagingByKeyActionRequest(PagingParameter paging, string KeyActionRequest, out int total) {

			DbCommand command = db.GetStoredProcCommand(const_proc_getallpagingbykeyactionrequest);
			command.CommandTimeout = 600;
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyActionRequest", KeyActionRequest);
			db.AddInParameterString(command, "Culture", Helper.GetCurrentCultureShort());
			db.AddInParameterInt(command, "start", paging.start);
			db.AddInParameterInt(command, "limit", paging.limit);
			AddPagingParameterOrderBy(paging, command);
			AddPagingParameterFilter(paging, command);

			DataSet ds = db.ExecuteDataSet(command);
			if (ds.Tables[0].Rows.Count > 0)
				total = ds.Tables[0].Rows[0].Field<int>("TotalRecord");
			else
				total = 0;
			return FillObjects(ds);

		}
		/// <summary>
		/// Gets an item by its instanceId.
		/// </summary>
		/// <param name="instanceId">The instanceId of the item.</param>
		/// <returns>The item.</returns>
		public Task GetItemByInstanceId(Guid instanceId) {
			Task item = default(Task);
			DbCommand command = db.GetStoredProcCommand(const_proc_getitembyinstanceid);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterGuid(command, "InstanceId", instanceId);
			db.AddInParameterString(command, "Culture", Helper.GetCurrentCultureShort());
			using (IDataReader reader = db.ExecuteReader(command)) {
				if (reader.Read()) {
					item = FillObject(reader);
				}
			}
			// populates the references.
			this.PopulateReference(new List<Task> { item });
			return item;
		}


	}
}

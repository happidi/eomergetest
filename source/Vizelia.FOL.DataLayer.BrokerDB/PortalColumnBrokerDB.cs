﻿using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.DataLayer.BrokerDB {

	/// <summary>
	/// Database broker for PortalColumn business entity.
	/// </summary>
	public class PortalColumnBrokerDB : BaseBrokerDB<PortalColumn> {

		/// <summary>
		/// The name of the stored procedure that retreives a list of Portal Columns for a specific Portal tab.
		/// </summary>
		protected const string const_proc_getlistbykeyportaltab = AppModule.Energy + "_PortalColumnGetListByKeyPortalTab";

		/// <summary>
		/// The name of the stored procedure that retreives a list of Portal Columns for a specific Portal tab.
		/// </summary>
		protected const string const_proc_getallpagingbykeyportaltab = AppModule.Energy + "_PortalColumnGetAllPagingByKeyPortalTab";

		/// <summary>
		/// The name of the stored procedure that saves a list of columns for a specific Portal tab.
		/// </summary>
		protected const string const_proc_portalcolumnsave = AppModule.Energy + "_PortalTabPortalColumnSave";

		/// <summary>
		/// The prefix of stored procedures of that provider. 
		/// </summary>
		/// 
		/// <summary>
		/// The prefix of stored procedures of that provider. 
		/// </summary>
		protected override string const_proc_prefix {
			get {
				return AppModule.Energy;
			}
		}

		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, PortalColumn item) {
			db.AddInParameterString(command, "Key", item.KeyPortalColumn);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyPortalTab", item.KeyPortalTab);
			db.AddInParameterString(command, "LocalId", item.LocalId);
			db.AddInParameterInt(command, "Flex", item.Flex);
			db.AddInParameterInt(command, "Order", item.Order);
			db.AddInParameterString(command, "Type", item.Type.ParseAsString());

		}

		/// <summary>
		/// Populates a PortalColumn business entity from a DataSourceDecorator.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated PortalColumn business entity.</returns>
		protected override PortalColumn FillObject(DataSourceDecorator source, string prefixColumn) {
			return new PortalColumn {
				KeyPortalColumn = source.Field<int>(prefixColumn + "KeyPortalColumn").ToString(CultureInfo.InvariantCulture),
				KeyPortalTab = source.Field<int>(prefixColumn + "KeyPortalTab").ToString(CultureInfo.InvariantCulture),
				KeyPortalWindow = source.Field<int>(prefixColumn + "KeyPortalWindow").ToString(CultureInfo.InvariantCulture),
				LocalId = source.Field<string>(prefixColumn + "LocalId"),
				Flex = source.Field<int>(prefixColumn + "Flex"),
				Order = source.Field<int>(prefixColumn + "Order"),
				PortalTabTitle = source.Field<string>(prefixColumn + "PortalTabTitle"),
				PortalWindowTitle = source.Field<string>(prefixColumn + "PortalWindowTitle"),
				PortalWindowIconCls = source.Field<string>(prefixColumn + "PortalWindowIconCls"),
				Type = source.Field<string>(prefixColumn + "Type").ParseAsEnum<PortalColumnType>()
			};
		}

		/// <summary>
		/// Get the list of saved PortalColumns from a Portal Tab.
		/// </summary>
		/// <param name="KeyPortalTab">The key of the portal tab.</param>
		public List<PortalColumn> GetListByKeyPortalTab(string KeyPortalTab) {
			List<PortalColumn> results;
			DbCommand command = db.GetStoredProcCommand(const_proc_getlistbykeyportaltab);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyPortalTab", KeyPortalTab);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}

		/// <summary>
		/// Gets all the PortalColumns for a specific Portal Tab.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyPortalTab">The Key of the portal tab.</param>
		/// <param name="total">The total number of items.</param>
		/// <returns>The list of items.</returns>
		public List<PortalColumn> GetAllPagingByKeyPortalTab(PagingParameter paging, string KeyPortalTab, out int total) {
			DbCommand command = db.GetStoredProcCommand(const_proc_getallpagingbykeyportaltab);
			command.CommandTimeout = 600;
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyPortalTab", KeyPortalTab);
			db.AddInParameterInt(command, "start", paging.start);
			db.AddInParameterInt(command, "limit", paging.limit);
			DataSet ds = db.ExecuteDataSet(command);
			if (ds.Tables[0].Rows.Count > 0)
				total = ds.Tables[0].Rows[0].Field<int>("TotalRecord");
			else
				total = 0;
			return FillObjects(ds);
		}

		/// <summary>
		/// Saves the portal tab  PortalColumns.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="portalcolumn">The PortalColumn store.</param>
		/// <returns></returns>
		public List<PortalColumn> SavePortalTabPortalColumn(PortalTab entity, CrudStore<PortalColumn> portalcolumn) {
			DbCommand command = db.GetStoredProcCommand(const_proc_portalcolumnsave);
			command.CommandTimeout = 600;
			List<PortalColumn> results;

			if (portalcolumn == null || portalcolumn.IsEmpty())
				return null;
			string inserts = null;
			string deletes = null;

			/*if (dataseries.create != null) {
				inserts = Helper.BuildXml(dataseries.create.Select(p => p.KeyDataSerie).ToArray());
			}*/

			if (portalcolumn.destroy != null) {
				deletes = Helper.BuildXml(portalcolumn.destroy.Select(p => p.KeyPortalColumn).ToArray());
			}

			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyPortalTab", entity.KeyPortalTab);
			db.AddInParameterString(command, "Inserts", inserts);
			db.AddInParameterString(command, "Deletes", deletes);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}
	}
}

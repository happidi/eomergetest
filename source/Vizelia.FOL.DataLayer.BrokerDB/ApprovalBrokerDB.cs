﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web.Security;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.DataLayer.BrokerDB {

	
	
	/// <summary>
	/// Database broker for Approval business entity.
	/// </summary>
	public class ApprovalBrokerDB : BaseBrokerDB<Approval> {

		/// <summary>
		/// The name of the stored procedure that retreives a list of approvals  for a specified ActionRequest.
		/// </summary>
		const string const_proc_getlistbykeyactionrequest = AppModule.ServiceDesk + "_ApprovalGetListByKeyActionRequest";


		/// <summary>
		/// Gets the const_proc_prefix.
		/// </summary>
		/// <value>The const_proc_prefix.</value>
		protected override string const_proc_prefix {
			get {
				return AppModule.ServiceDesk;
			}
		}

		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, Approval item) {
			throw new NotImplementedException();
		}

		/// <summary>
		/// Populates a Approval business entity from a DataSourceDecorator.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated Approval business entity.</returns>
		protected override Approval FillObject(DataSourceDecorator source, string prefixColumn) {
			return new Approval {
				KeyApproval = source.Field<string>(prefixColumn + "KeyApproval"),
				Name = source.Field<string>(prefixColumn + "Name"),
				Description = source.Field<string>(prefixColumn + "Description"),
				History = source.Field<string>(prefixColumn + "History"),
				Identifier = source.Field<string>(prefixColumn + "Identifier"),
				KeyActionRequest = source.Field<string>(prefixColumn + "KeyActionRequest"),
				KeyActor = source.Field<string>(prefixColumn + "KeyActor"),
				ApprovalDateTime = source.Field<DateTime?>(prefixColumn + "ApprovalDateTime"),
				Level = source.Field<string>(prefixColumn + "Level"),
				Status = source.Field<string>(prefixColumn + "Status"),
				ApproverFirstName = source.Field<string>(prefixColumn + "ApproverFirstName"),
				ApproverLastName = source.Field<string>(prefixColumn + "ApproverLastName"),
				ApproverMail = source.Field<string>(prefixColumn + "ApproverMail")
			};
		}



		/// <summary>
		/// Gets the list of approvals by  key ActionRequest item.
		/// </summary>
		/// <param name="keyActionRequest">The key ActionRequest item.</param>
		/// <returns></returns>
		public List<Approval> GetListByKeyActionRequest(string keyActionRequest) {
			List<Approval> result = null;
			DbCommand command = db.GetStoredProcCommand(const_proc_getlistbykeyactionrequest);
			db.AddInParameterString(command, "KeyActionRequest", keyActionRequest);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			using (IDataReader reader = db.ExecuteReader(command)) {
				result = FillObjects(reader);
			}
			return result;
		}

	}
}

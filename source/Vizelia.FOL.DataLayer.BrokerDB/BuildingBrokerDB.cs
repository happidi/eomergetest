﻿using Vizelia.FOL.BusinessEntities;
using System.Data.Common;
using System.Web.Security;
using System.Collections.Generic;
using System.Data;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.DataLayer.BrokerDB {

	/// <summary>
	/// Database broker for Building business entity.
	/// </summary>
	public class BuildingBrokerDB : BaseBrokerDB<Building> {

		/// <summary>
		/// The name of the stored procedure that retreives a list of building for a specific map.
		/// </summary>
		protected const string const_proc_getlistbymap = AppModule.Energy + "_BuildingGetListByKeyMap";


		/// <summary>
		/// Populates a Building business entity from a DataSourceDecorator.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated Building business entity.</returns>
		protected override Building FillObject(DataSourceDecorator source, string prefixColumn) {
			return new Building {
				KeyBuilding = source.Field<string>("KeyBuilding"),
				LocalId = source.Field<string>(prefixColumn + "LocalId"),
				ObjectType = source.Field<string>(prefixColumn + "ObjectType"),
				Name = source.Field<string>(prefixColumn + "Name"),
				Description = source.Field<string>(prefixColumn + "Description"),
				KeySite = source.Field<string>(prefixColumn + "KeySite")
			};
		}

		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, Building item) {
			db.AddInParameterString(command, "Key", item.KeyBuilding);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeySite", item.KeySite);
			db.AddInParameterString(command, "ObjectType", item.ObjectType);
			db.AddInParameterString(command, "Name", item.Name);
			db.AddInParameterString(command, "Description", item.Description);
            db.AddInParameterBool(command, "IsMappingInProgress", base.IsAsynchronousSave);
			string localId = !string.IsNullOrWhiteSpace(item.LocalId) ? item.LocalId : null;
			db.AddInParameterString(command, "LocalId", localId);
		}


		/// <summary>
		/// Get the list of Building from a Map
		/// </summary>
		/// <param name="KeyMap">The key of the Map.</param>
		public List<Building> GetListByKeyMap(string KeyMap) {
			List<Building> results;
			DbCommand command = db.GetStoredProcCommand(const_proc_getlistbymap);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyMap", KeyMap);
			AddSecurityFilterParam<Location>(command, "ProjectLocationFilter");
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}


        /// <summary>
        /// Builds the additional delete params.
        /// </summary>
        /// <param name="command">The command.</param>
        protected override void BuildAdditionalDeleteParams(DbCommand command) {
            db.AddInParameterBool(command, "IsMappingInProgress", base.IsAsynchronousSave);
        }
	}
}

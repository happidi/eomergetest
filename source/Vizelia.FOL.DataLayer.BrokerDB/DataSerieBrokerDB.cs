﻿using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.DataLayer.BrokerDB {

	/// <summary>
	/// Database broker for DataSerie business entity.
	/// </summary>
	public class DataSerieBrokerDB : BaseBrokerDB<DataSerie> {

		/// <summary>
		/// The name of the stored procedure that retreives a list of dataseries for a specific chart.
		/// </summary>
		protected const string const_proc_getlistbychart = AppModule.Energy + "_DataSerieGetListByKeyChart";

		/// <summary>
		/// The name of the stored procedure that retreives a list of meters for a specific chart.
		/// </summary>
		protected const string const_proc_getallpagingbykeychart = AppModule.Energy + "_DataSerieGetAllPagingByKeyChart";

		/// <summary>
		/// The name of the stored procedure that saves a list of meters for a specific chart.
		/// </summary>
		protected const string const_proc_chartdataseriessave = AppModule.Energy + "_ChartDataSeriesSave";

		/// <summary>
		/// Gets the const_proc_prefix.
		/// </summary>
		/// <value>The const_proc_prefix.</value>
		protected override string const_proc_prefix {
			get {
				return AppModule.Energy;
			}
		}

		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, DataSerie item) {
			db.AddInParameterString(command, "Key", item.KeyDataSerie);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "LocalId", item.LocalId);
			db.AddInParameterString(command, "KeyChart", item.KeyChart);
			db.AddInParameterString(command, "KeyClassificationItem", item.KeyClassificationItem);
			db.AddInParameterString(command, "Name", item.Name);
			db.AddInParameterString(command, "Type", item.Type.ParseAsString());
			db.AddInParameterString(command, "Unit", item.Unit);
			db.AddInParameterInt(command, "DisplayTransparency", item.DisplayTransparency);
			db.AddInParameterString(command, "DisplayLegendEntry", item.DisplayLegendEntry.ParseAsString());
			db.AddInParameterInt(command, "ColorR", item.ColorR);
			db.AddInParameterInt(command, "ColorG", item.ColorG);
			db.AddInParameterInt(command, "ColorB", item.ColorB);
			db.AddInParameterString(command, "KeyYAxis", item.KeyYAxis);
			db.AddInParameterDouble(command, "Ratio", item.Ratio);
			db.AddInParameterBool(command, "Hidden", item.Hidden);
			db.AddInParameterBool(command, "HideLegendEntry", item.HideLegendEntry);
			db.AddInParameterDouble(command, "LimitElementMinValue", item.LimitElementMinValue);
			db.AddInParameterDouble(command, "LimitElementMaxValue", item.LimitElementMaxValue);
			db.AddInParameterDouble(command, "LimitByLegendEntryMinValue", item.LimitByLegendEntryMinValue);
			db.AddInParameterDouble(command, "LimitByLegendEntryMaxValue", item.LimitByLegendEntryMaxValue);
			db.AddInParameterDouble(command, "Target", item.Target);
			db.AddInParameterDouble(command, "TargetElement", item.TargetElement);
			db.AddInParameterBool(command, "LimitElementCount", item.LimitElementCount);
			db.AddInParameterBool(command, "LimitElementRawData", item.LimitElementRawData);
			db.AddInParameterBool(command, "HideLabel", item.HideLabel);
			db.AddInParameterString(command, "MarkerType", item.MarkerType.ParseAsString());
			db.AddInParameterString(command, "LineDashStyle", item.LineDashStyle.ParseAsString());
			db.AddInParameterInt(command, "MarkerSize", item.MarkerSize);
			db.AddInParameterInt(command, "LineWidth", item.LineWidth);
			db.AddInParameterInt(command, "ZIndex", item.ZIndex);
		}

		/// <summary>
		/// Populates a DataSerie business entity from a DataSourceDecorator.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated DataSerie business entity.</returns>
		protected override DataSerie FillObject(DataSourceDecorator source, string prefixColumn) {
			return new DataSerie {
				KeyDataSerie = source.Field<int>(prefixColumn + "KeyDataSerie").ToString(CultureInfo.InvariantCulture),
				KeyChart = source.Field<int>(prefixColumn + "KeyChart").ToString(CultureInfo.InvariantCulture),
				KeyClassificationItem = source.Field<string>(prefixColumn + "KeyClassificationItem"),
				KeyClassificationItemPath = source.Field<string>(prefixColumn + "KeyClassificationItemPath"),
				ClassificationItemLongPath = source.Field<string>(prefixColumn + "ClassificationItemLongPath"),
				ClassificationItemLocalIdPath = source.Field<string>(prefixColumn + "ClassificationItemLocalIdPath"),
				ClassificationItemTitle = source.Field<string>(prefixColumn + "ClassificationItemTitle"),
				LocalId = source.Field<string>(prefixColumn + "LocalId"),
				Name = source.Field<string>(prefixColumn + "Name"),
				Type = source.Field<string>(prefixColumn + "Type").ParseAsEnum<DataSerieType>(),
				Unit = source.Field<string>(prefixColumn + "Unit"),
				DisplayLegendEntry = source.Field<string>(prefixColumn + "DisplayLegendEntry").ParseAsEnum<DataSerieLegendEntry>(),
				DisplayTransparency = source.Field<int?>(prefixColumn + "DisplayTransparency"),
				ColorR = source.Field<int?>(prefixColumn + "ColorR"),
				ColorG = source.Field<int?>(prefixColumn + "ColorG"),
				ColorB = source.Field<int?>(prefixColumn + "ColorB"),
				KeyYAxis = source.Field<string>(prefixColumn + "KeyYAxis"),
				YAxisName = source.Field<string>(prefixColumn + "YAxisName"),
				Ratio = source.Field<double?>(prefixColumn + "Ratio"),
				Hidden = source.Field<bool?>(prefixColumn + "Hidden") ?? false,
				HideLegendEntry = source.Field<bool?>(prefixColumn + "HideLegendEntry") ?? false,
				LimitElementMinValue = source.Field<double?>(prefixColumn + "LimitElementMinValue"),
				LimitElementMaxValue = source.Field<double?>(prefixColumn + "LimitElementMaxValue"),
				Target = source.Field<double?>(prefixColumn + "Target"),
				LimitElementCount = source.Field<bool?>(prefixColumn + "LimitElementCount") ?? false,
				LimitElementRawData = source.Field<bool?>(prefixColumn + "LimitElementRawData") ?? false,
				TargetElement = source.Field<double?>(prefixColumn + "TargetElement"),
				HideLabel = source.Field<bool?>(prefixColumn + "HideLabel") ?? false,
				LimitByLegendEntryMinValue = source.Field<double?>(prefixColumn + "LimitByLegendEntryMinValue"),
				LimitByLegendEntryMaxValue = source.Field<double?>(prefixColumn + "LimitByLegendEntryMaxValue"),
				MarkerType = (source.Field<string>(prefixColumn + "MarkerType") ?? "Circle").ParseAsEnum<MarkerType>(),
				LineDashStyle = (source.Field<string>(prefixColumn + "LineDashStyle") ?? "Solid").ParseAsEnum<LineDashStyle>(),
				LineWidth = source.Field<int?>(prefixColumn + "LineWidth"),
				MarkerSize = source.Field<int?>(prefixColumn + "MarkerSize"),
				ZIndex = source.Field<int?>(prefixColumn + "ZIndex"),
			};
		}

		/// <summary>
		/// Get the list of saved DataSerie from a Chart
		/// </summary>
		/// <param name="KeyChart">The key of the chart.</param>
		public List<DataSerie> GetListByKeyChart(string KeyChart) {
			List<DataSerie> results;
			DbCommand command = db.GetStoredProcCommand(const_proc_getlistbychart);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyChart", KeyChart);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}

		/// <summary>
		/// Gets all the dataserie for a specific chart.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyChart">The Key of the chart.</param>
		/// <param name="total">The total number of items.</param>
		/// <returns>The list of items.</returns>
		public List<DataSerie> GetAllPagingByKeyChart(PagingParameter paging, string KeyChart, out int total) {
			DbCommand command = db.GetStoredProcCommand(const_proc_getallpagingbykeychart);
			command.CommandTimeout = 600;
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyChart", KeyChart);
			db.AddInParameterInt(command, "start", paging.start);
			db.AddInParameterInt(command, "limit", paging.limit);


			AddPagingParameterOrderBy(paging, command);

			if (paging != null)
				paging.ExtractInnerJoinFilters();
			AddPagingParameterFilter(paging, command);



			DataSet ds = db.ExecuteDataSet(command);
			if (ds.Tables[0].Rows.Count > 0)
				total = ds.Tables[0].Rows[0].Field<int>("TotalRecord");
			else
				total = 0;
			return FillObjects(ds);
		}


		/// <summary>
		/// Saves the chart dataseries.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="dataseries">The dataseries.</param>
		/// <returns></returns>
		public List<DataSerie> SaveChartDataSeries(Chart entity, CrudStore<DataSerie> dataseries) {
			DbCommand command = db.GetStoredProcCommand(const_proc_chartdataseriessave);
			command.CommandTimeout = 600;
			List<DataSerie> results;

			if (dataseries == null || dataseries.IsEmpty())
				return null;
			string inserts = null;
			string deletes = null;

			/*if (dataseries.create != null) {
				inserts = Helper.BuildXml(dataseries.create.Select(p => p.KeyDataSerie).ToArray());
			}*/

			if (dataseries.destroy != null) {
				deletes = Helper.BuildXml(dataseries.destroy.Select(p => p.KeyDataSerie).ToArray());
			}

			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyChart", entity.KeyChart);
			db.AddInParameterString(command, "Inserts", inserts);
			db.AddInParameterString(command, "Deletes", deletes);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}
	}
}
﻿using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Globalization;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.DataLayer.BrokerDB {

	/// <summary>
	/// Database broker for DynamicDisplayImage business entity.
	/// </summary>
	public class DynamicDisplayImageBrokerDB : BaseBrokerDB<DynamicDisplayImage> {

		/// <summary>
		/// The name of the stored procedure that retreives a list of DynamicDisplayImage for a specific type.
		/// </summary>
		protected const string const_proc_getallpagingbykeydrawingcanvas = AppModule.Energy + "_DynamicDisplayImageGetAllPagingByType";

		/// <summary>
		/// The prefix of stored procedures of that provider. 
		/// </summary>
		protected override string const_proc_prefix {
			get {
				return AppModule.Energy;
			}
		}

		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, DynamicDisplayImage item) {
			db.AddInParameterString(command, "Key", item.KeyDynamicDisplayImage);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "Name", item.Name);
			db.AddInParameterString(command, "KeyImage", item.KeyImage);
			db.AddInParameterString(command, "Type", item.Type.ParseAsString());
		}


		/// <summary>
		/// Populates a DynamicDisplayImage business entity from a DataSourceDecorator.
		/// </summary>
		/// <param name="source">The data source.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated DynamicDisplayImage business entity.</returns>
		protected override DynamicDisplayImage FillObject(DataSourceDecorator source, string prefixColumn) {
			return new DynamicDisplayImage {
				Application = ContextHelper.ApplicationName,
				KeyDynamicDisplayImage = source.Field<int>(prefixColumn + "KeyDynamicDisplayImage").ToString(CultureInfo.InvariantCulture),
				Name = source.Field<string>(prefixColumn + "Name"),
				KeyImage = source.Field<string>(prefixColumn + "KeyImage"),
				Type = (source.Field<string>(prefixColumn + "Type") ?? "Image").ParseAsEnum<DynamicDisplayImageType>()
			};
		}

		/// <summary>
		/// Gets a store of DrawingCanvas Image for a specific DrawingCanvas.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="type">The type.</param>
		/// <param name="total">The total.</param>
		/// <returns></returns>
		public List<DynamicDisplayImage> GetAllPagingByType(PagingParameter paging, DynamicDisplayImageType type, out int total) {
			DbCommand command = db.GetStoredProcCommand(const_proc_getallpagingbykeydrawingcanvas);
			command.CommandTimeout = 600;
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "Type", type.ParseAsString());

			AddPagingParameterFilter(paging, command);


			db.AddInParameterInt(command, "start", paging.start);
			db.AddInParameterInt(command, "limit", paging.limit);
			DataSet ds = db.ExecuteDataSet(command);
			if (ds.Tables[0].Rows.Count > 0)
				total = ds.Tables[0].Rows[0].Field<int>("TotalRecord");
			else
				total = 0;
			return FillObjects(ds);
		}
	}
}
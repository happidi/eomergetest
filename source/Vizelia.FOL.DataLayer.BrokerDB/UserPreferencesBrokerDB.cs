﻿using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.DataLayer.BrokerDB {
	/// <summary>
	/// The broker DB for UserPreferences
	/// </summary>
	public class UserPreferencesBrokerDB : BaseBrokerDB<UserPreferences>{

		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(System.Data.Common.DbCommand command, UserPreferences item) {
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyUser", item.KeyUser);
			db.AddInParameterString(command, "CustomDesktopBackground", item.CustomDesktopBackground);
			db.AddInParameterString(command, "DesktopBackground", item.DesktopBackground);
			db.AddInParameterString(command, "DesktopTextColor", item.DesktopTextColor);
			db.AddInParameterInt(command, "GridsPageSize", item.GridsPageSize);
			db.AddInParameterString(command, "DefaultChartKPI", item.DefaultChartKPI);
			db.AddInParameterBool(command, "PreloadCharts", item.PreloadCharts);
			db.AddInParameterString(command, "TimeZoneId", item.TimeZoneId);
		}
		
		/// <summary>
		/// Populates a T business entity from a IDataReader.
		/// </summary>
		/// <param name="source">The reader.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>
		/// The populated T business entity.
		/// </returns>
		protected override UserPreferences FillObject(DataSourceDecorator source, string prefixColumn) {
			return new UserPreferences {
				Application = ContextHelper.ApplicationName,
				KeyUser = source.Field<string>(prefixColumn + "KeyUser"),
				GridsPageSize = source.Field<int>(prefixColumn + "GridsPageSize"),
				CustomDesktopBackground = source.Field<string>(prefixColumn + "CustomDesktopBackground"),
				DesktopBackground = source.Field<string>(prefixColumn + "DesktopBackground"),
				DesktopTextColor = source.Field<string>(prefixColumn + "DesktopTextColor"),
				DefaultChartKPI = source.Field<string>(prefixColumn + "DefaultChartKPI"),
				TimeZoneId = source.Field<string>(prefixColumn + "TimeZoneId"),
				PreloadCharts = source.Field<bool>(prefixColumn + "PreloadCharts"),
				LocalId = source.Field<string>(prefixColumn + "LocalId"),
			};
		}
	}
}
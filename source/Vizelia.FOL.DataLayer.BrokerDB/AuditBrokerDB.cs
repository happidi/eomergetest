﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.DataLayer.BrokerDB {
	/// <summary>
	/// Database broker for BaseAuditEntity business entity.
	/// </summary>
	public class AuditBrokerDB : BaseAuditBrokerDB<Audit> {
		/// <summary>
		/// Gets or sets the type of the entity.
		/// </summary>
		/// <value>
		/// The type of the entity.
		/// </value>
		public Type EntityType { get; set; }

		/// <summary>
		/// Fills the object.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="prefixColumn">The prefix column.</param>
		/// <returns></returns>
		protected override Audit FillObject(DataSourceDecorator source, string prefixColumn) {
			var dmlType = source.Field<string>(prefixColumn + "DMLType");
			var retVal = new Audit {
				AuditId = source.Field<long>(prefixColumn + "AuditId").ToString(CultureInfo.InvariantCulture),
				ModifiedBy = source.Field<string>(prefixColumn + "ModifiedBy"),
				ChangeTime = source.Field<DateTime>(prefixColumn + "ChangeTime"),
				ChangedFields = source.Field<string>(prefixColumn + "ChangedFields"),
				DMLType = dmlType,
				Operation = source.Field<int>(prefixColumn + "Operation").ParseAsEnum<AuditOperation>(),
				Entity = GetEntity(source, dmlType)
			};
			return retVal;
		}

		private List<SimpleListElementGeneric<object>> GetEntity(DataSourceDecorator source, string dmlType) {
			var entity = new List<SimpleListElementGeneric<object>>();

			var i = 0;
			var columnName = string.Format("FieldName{0}", i);
			while (source.FieldExists(columnName)) {
				var fieldName = source.Field<string>(columnName);
				if (!string.IsNullOrWhiteSpace(fieldName)) {
					if (EntityProperties.Any(x => x.Key == fieldName)) {
						var valueType = dmlType.Equals("D") ? "OldValue" : "NewValue";
						var element = new SimpleListElementGeneric<object> {
							Id = fieldName,
							Value = source.Field<string>(string.Format("{0}{1}", valueType, i))
						};
						if (fieldName == "AcquisitionDateTime") {
							DateTime date;
							var parseSucceed = DateTime.TryParse(element.Value as string, DateTimeFormatInfo.CurrentInfo, DateTimeStyles.AssumeUniversal, out date);
							if (parseSucceed) {
								var dateElement = new SimpleListElementGeneric<object> { Id = fieldName, Value = date };
								entity.Add(dateElement);
							}
						}
						else {
							entity.Add(element);
						}
					}
				}
				i++;
				columnName = string.Format("FieldName{0}", i);
			}
			return entity;
		}
	}
}

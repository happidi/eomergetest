﻿using System.Data.Common;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.DataLayer.BrokerDB {

	/// <summary>
	/// Database broker for MappingTask business entity.
	/// </summary>
	public class MappingTaskBrokerDB : BaseBrokerDB<MappingTask> {

		/// <summary>
		/// The prefix of stored procedures of that provider. 
		/// </summary>
		protected override string const_proc_prefix {
			get {
				return AppModule.Mapping;
			}
		}

		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, MappingTask item) {
			db.AddInParameterString(command, "Key", item.KeyMappingTask);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "Name", item.Name);
			db.AddInParameterString(command, "Description", item.Description);
			db.AddInParameterInt(command, "FileSourceType", (int)item.FileSourceType);
			db.AddInParameterString(command, "Username", item.Username);
			db.AddInParameterInt(command, "FtpSecurityType", (int)item.FtpSecurityType);
			db.AddInParameterBool(command, "UseCustomCredentials", item.UseCustomCredentials);
			db.AddInParameterString(command, "Address", item.Address);
			db.AddInParameterString(command, "Emails", item.Emails);
			db.AddInParameterString(command, "PrivateKey", item.PrivateKey);
			db.AddInParameterInt(command, "SftpAuthenticationMode", (int)item.SftpAuthenticationMode);
			db.AddInParameterString(command, "AutoCreatedMetersKeyLocation", item.AutoCreatedMetersKeyLocation);
			db.AddInParameterInt(command, "AutoCreateMeterOptions", (int)item.AutoCreateMeterOptions);

			// Encrypt the password
			string passwordIV;
			var encryptedPassword = EncryptText(item.Password, out passwordIV);
			db.AddInParameterString(command, "Password", encryptedPassword);
			db.AddInParameterString(command, "PasswordIV", passwordIV);

			// Encrypt the private key password
			string privateKeyPasswordIV;
			var encryptedPrivateKeyPassword = EncryptText(item.PrivateKeyPassword, out privateKeyPasswordIV);
			db.AddInParameterString(command, "PrivateKeyPassword", encryptedPrivateKeyPassword);
			db.AddInParameterString(command, "PrivateKeyPasswordIV", privateKeyPasswordIV);
		}

		private static string EncryptText(string text, out string initializationVector) {
			string encryptedText;

			if (!string.IsNullOrWhiteSpace(text)) {
				encryptedText = CryptoHelper.Encrypt(text, out initializationVector);
			}
			else {
				encryptedText = null;
				initializationVector = null;
			}

			return encryptedText;
		}

		/// <summary>
		/// Populates a MappingTask business entity from a DataSourceDecorator.
		/// </summary>
		/// <param name="source">The data source.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated MappingTask business entity.</returns>
		protected override MappingTask FillObject(DataSourceDecorator source, string prefixColumn) {
			// Decrypt the password
			string encryptedPassword = source.Field<string>(prefixColumn + "Password");
			string initializationVector = source.Field<string>(prefixColumn + "PasswordIV");
			string password = string.Empty;

			if (!string.IsNullOrWhiteSpace(encryptedPassword) && !string.IsNullOrWhiteSpace(initializationVector)) {
				password = CryptoHelper.Decrypt(encryptedPassword, initializationVector);
			}

			// Decrypt the certificatePassword
			string encryptedPrivateKeyPassword = source.Field<string>(prefixColumn + "PrivateKeyPassword");
			string privateKeyIV = source.Field<string>(prefixColumn + "PrivateKeyPasswordIV");
			string privateKeyPassword = string.Empty;

			if (!string.IsNullOrWhiteSpace(encryptedPrivateKeyPassword) && !string.IsNullOrWhiteSpace(privateKeyIV)) {
				privateKeyPassword = CryptoHelper.Decrypt(encryptedPrivateKeyPassword, privateKeyIV);
			}

			return new MappingTask {
				KeyMappingTask = source.Field<int>(prefixColumn + "KeyMappingTask").ToString(),
				Name = source.Field<string>(prefixColumn + "Name"),
				Description = source.Field<string>(prefixColumn + "Description"),
				FileSourceType = source.Field<int>(prefixColumn + "FileSourceType").ParseAsEnum<FileSourceType>(),
				Username = source.Field<string>(prefixColumn + "Username"),
				Password = password,
				PrivateKey = source.Field<string>(prefixColumn + "PrivateKey"),
				PrivateKeyPassword = privateKeyPassword,
				FtpSecurityType = source.Field<int>(prefixColumn + "FtpSecurityType").ParseAsEnum<FtpSecurityType>(),
				SftpAuthenticationMode = source.Field<int>(prefixColumn + "SftpAuthenticationMode").ParseAsEnum<SftpAuthenticationMode>(),
				UseCustomCredentials = source.Field<bool>(prefixColumn + "UseCustomCredentials"),
				Address = source.Field<string>(prefixColumn + "Address"),
				Emails = source.Field<string>(prefixColumn + "Emails"),
				AutoCreatedMetersKeyLocation = source.Field<string>(prefixColumn + "AutoCreatedMetersKeyLocation"),
				AutoCreatedMetersLocationLongPath = source.Field<string>(prefixColumn + "AutoCreatedMetersLocationLongPath"),
				AutoCreateMeterOptions = source.Field<int>(prefixColumn + "AutoCreateMeterOptions").ParseAsEnum<AutoCreateMeterOptions>()
			};
		}
	}
}
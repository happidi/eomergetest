﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.DataLayer.BrokerDB {
	/// <summary>
	/// Database broker for a list element business entity.
	/// </summary>
	public class ListElementBrokerDB : BaseBrokerDB<ListElement> {
		/// <summary>
		/// Gets or sets the name of the list.
		/// </summary>
		/// <value>
		/// The name of the list.
		/// </value>
		public string ListName { get; set; }

		/// <summary>
		/// The name of the stored procedure that retreives a list of element for a pset list.
		/// </summary>
		protected const string const_proc_getpsetlist = AppModule.Core + "_ListElementPsetGetAll";

		/// <summary>
		/// Gets all the element of the list.
		/// </summary>
		/// <returns></returns>
		protected override List<ListElement> GetAllNoPaging() {
			List<ListElement> results;
			DbCommand command = db.GetStoredProcCommand(const_proc_getall);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "ListName", ListName);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}

		/// <summary>
		/// Gets all the element from a pset list.
		/// </summary>
		/// <returns></returns>
		public List<ListElement> GetPsetAll() {
			List<ListElement> results;
			DbCommand command = db.GetStoredProcCommand(const_proc_getpsetlist);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "ListName", ListName);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}


		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, ListElement item) {
			db.AddInParameterString(command, "Key", item.Id);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "ListName", ListName);
			db.AddInParameterString(command, "MsgCode", item.MsgCode);
			db.AddInParameterString(command, "Value", item.Value);
		}

		/// <summary>
		/// Populates a ListElement business entity from a DataSourceDecorator.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated ListElement business entity.</returns>
		protected override ListElement FillObject(DataSourceDecorator source, string prefixColumn) {
			var listElement = new ListElement {
				Id = source.Field<int>(prefixColumn + "Id").ToString(),
				Value = source.Field<string>(prefixColumn + "Value"),
				MsgCode = source.Field<string>(prefixColumn + "MsgCode")
			};
			if (source.FieldExists(prefixColumn + "IconCls")) {

				listElement.IconCls = source.Field<string>(prefixColumn + "IconCls");
			}

			return listElement;
		}

		/// <summary>
		/// Gets the item by local id.
		/// The override is necessary because of .read() in FillObjects that is redondant with .read() in sub method GetItemByLocalId.
		/// </summary>
		/// <param name="localId">The local id.</param>
		/// <returns></returns>
		public override ListElement GetItemByLocalId(string localId) {
			ListElement item = null;
			DbCommand command = db.GetStoredProcCommand(const_proc_getitembylocalid);
			var parameters = localId.Split(new[] { "ListName=", "MsgCode=" }, StringSplitOptions.RemoveEmptyEntries);
			var listName = parameters[0];
			var msgCode = parameters[1];
			db.AddInParameterString(command, "ListName", listName);
			db.AddInParameterString(command, "MsgCode", msgCode);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			using (IDataReader reader = db.ExecuteReader(command)) {
				if (reader.Read()) {
					item = FillObject(reader);
				}
			}
			return item;
		}
	}
}

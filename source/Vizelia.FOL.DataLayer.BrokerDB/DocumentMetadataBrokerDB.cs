﻿using System.Data.Common;
using System.Web.Security;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.DataLayer.BrokerDB {

	/// <summary>
	/// Database broker for DocumentMetadata business entity.
	/// </summary>
	public class DocumentMetadataBrokerDB : BaseBrokerDB<DocumentMetadata> {



		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, DocumentMetadata item) {
			db.AddInParameterString(command, "Key", item.KeyDocument);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterInt(command, "DocumentContentsLength", item.DocumentContentsLength);
			db.AddInParameterString(command, "DocumentName", item.DocumentName);
			db.AddInParameterString(command, "DocumentType", item.DocumentType);
		}

		/// <summary>
		/// Populates a T business entity from a IDataReader.
		/// </summary>
		/// <param name="source">The reader.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated T business entity.</returns>
		protected override DocumentMetadata FillObject(DataSourceDecorator source, string prefixColumn) {
			var documentMetadata = new DocumentMetadata {
				KeyDocument = source.Field<int>(prefixColumn + "KeyDocument").ToString(),
				DocumentContentsLength = source.Field<int>(prefixColumn + "DocumentContentsLength"),
				DocumentName = source.Field<string>(prefixColumn + "DocumentName"),
				DocumentType = source.Field<string>(prefixColumn + "DocumentType"),
			};

			return documentMetadata;
		}

	}
}
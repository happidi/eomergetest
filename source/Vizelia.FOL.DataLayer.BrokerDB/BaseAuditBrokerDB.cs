﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.DataLayer.BrokerDB {
	/// <summary>
	/// A Base Broker DB for DataAcquisitionEndpoint entities.
	/// </summary>
	/// <typeparam name="TAudit">The type of the audit.</typeparam>
	public abstract class BaseAuditBrokerDB<TAudit> : BaseBrokerDB<TAudit> where TAudit : BaseBusinessEntity, new() {

		/// <summary>
		/// Gets or sets the entity properties.
		/// </summary>
		/// <value>
		/// The entity properties.
		/// </value>
		internal List<KeyValuePair<string, string>> EntityProperties { get; set; }

		/// <summary>
		/// Gets the name of the business entity.
		/// </summary>
		/// <value>
		/// The name of the business entity.
		/// </value>
		protected override string BusinessEntityName {
			get {
				return "Audit";
			}
		}

		/// <summary>
		/// Determines whether [is tenant aware object].
		/// </summary>
		/// <returns>
		///   <c>true</c> if [is tenant aware object]; otherwise, <c>false</c>.
		/// </returns>
		protected override bool IsTenantAwareObject() {
			return false;
		}

		/// <summary>
		/// Builds the save params.
		/// </summary>
		/// <param name="command">The command.</param>
		/// <param name="item">The item.</param>
		protected override void BuildSaveParams(DbCommand command, TAudit item) {
			throw new NotImplementedException();
		}

		/// <summary>
		/// Loads the properties tree.
		/// </summary>
		/// <param name="type">The type.</param>
		public List<KeyValuePair<string, string>> LoadPropertiesTree(Type type) {
			EntityProperties = GetAuditPropertiesTree(type);
			return EntityProperties;
		}

		/// <summary>
		/// Gets the audit properties tree.
		/// </summary>
		/// <param name="type">The type.</param>
		/// <param name="namePrefix">The name prefix.</param>
		/// <returns></returns>
		protected List<KeyValuePair<string, string>> GetAuditPropertiesTree(Type type, string namePrefix = "") {
			var retVal = new List<KeyValuePair<string, string>>();
			foreach (var propertyInfo in type.GetProperties()) {
				// recursive into properties that marked with AuditedClassAttribute Attribute
				if (propertyInfo.HasAttribute<AuditedClassAttribute>()) {
					retVal.AddRange(GetAuditPropertiesTree(propertyInfo.PropertyType, propertyInfo.Name + "_"));
				}
				// take properties that marked with Information Attribute
				else if (propertyInfo.HasAttribute<InformationAttribute>()) {
					var propertyName = namePrefix + propertyInfo.Name;
					if (propertyInfo.HasAttribute<AuditKeyAttribute>()) {
						propertyName += "_ToString";
					}
					retVal.Add(new KeyValuePair<string, string>(propertyName, Helper.GetPropertyLocalizedName(type, propertyInfo)));
				}
			}
			return retVal;
		}
	}
}
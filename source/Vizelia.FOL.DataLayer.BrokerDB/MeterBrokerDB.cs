﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.DataLayer.BrokerDB {
	/// <summary>
	/// Database broker for Meter business entity.
	/// </summary>
	public class MeterBrokerDB : BaseBrokerDB<Meter> {
		#region MeterDataExportTask
		/// <summary>
		/// The name of the stored procedure that retreives the complete list of meters for a specific MeterDataExportTask.
		/// </summary>
		protected const string const_proc_getcompletelistbykeyMeterDataExportTask = AppModule.Energy + "_MeterGetListByKeyObject";
		/// <summary>
		/// The name of the stored procedure that retreives a list of meters directly attached to a MeterDataExportTask.
		/// </summary>
		protected const string const_proc_getallpagingbykeyMeterDataExportTask = AppModule.Energy + "_MeterGetAllPagingByKeyMeterDataExportTask";
		/// <summary>
		/// The name of the stored procedure that saves a list of meters for a specific MeterDataExportTask.
		/// </summary>
		protected const string const_proc_MeterDataExportTaskmeterssave = AppModule.Energy + "_MeterDataExportTaskMetersSave";

		#endregion
	
		#region Chart
		/// <summary>
		/// The name of the stored procedure that retreives the complete list of meters for a specific chart.
		/// </summary>
		protected const string const_proc_getcompletelistbykeychart = AppModule.Energy + "_MeterGetListByKeyObject";
		/// <summary>
		/// The name of the stored procedure that retreives a list of meters directly attached to a chart.
		/// </summary>
		protected const string const_proc_getallpagingbykeychart = AppModule.Energy + "_MeterGetAllPagingByKeyChart";
		/// <summary>
		/// The name of the stored procedure that saves a list of meters for a specific chart.
		/// </summary>
		protected const string const_proc_chartmeterssave = AppModule.Energy + "_ChartMetersSave";
		#endregion
		#region AlarmDefinition
		/// <summary>
		/// The name of the stored procedure that saves a list of meters for a specific AlarmDefinition.
		/// </summary>
		protected const string const_proc_alarmdefinitionmeterssave = AppModule.Energy + "_AlarmDefinitionMetersSave";
		/// <summary>
		/// The name of the stored procedure that retreives the complete list of meters for a specific AlarmDefinition.
		/// </summary>
		protected const string const_proc_getcompletelistbykeyalarmdefinition = AppModule.Energy + "_MeterGetListByKeyObject";
		/// <summary>
		/// The name of the stored procedure that retreives a list of meters directly attached to an alarm definition.
		/// </summary>
		protected const string const_proc_getallpagingbykeyalarmdefinition = AppModule.Energy + "_MeterGetAllPagingByKeyAlarmDefinition";
		#endregion
		#region MeterValidationRule
		/// <summary>
		/// The name of the stored procedure that saves a list of meters for a specific MeterValidationRule.
		/// </summary>
		protected const string const_proc_metervalidationrulemeterssave = AppModule.Energy + "_MeterValidationRuleMetersSave";
		/// <summary>
		/// The name of the stored procedure that retreives a list of meters directly attached to an alarm definition.
		/// </summary>
		protected const string const_proc_getallpagingbykeymetervalidationrule = AppModule.Energy + "_MeterGetAllPagingByKeyMeterValidationRule";
		/// <summary>
		/// The name of the stored procedure that retreives the complete list of meters for a specific AlarmDefinition.
		/// </summary>
		protected const string const_proc_getcompletelistbykeymetervalidationrule = AppModule.Energy + "_MeterGetListByKeyObject";
		#endregion
		#region Misc
		/// <summary>
		/// The name of the stored procedure that retrieves a list of meters from a list of Locations and ClassificationItems.
		/// </summary>
		protected const string const_proc_getlistbylocationandclassification = AppModule.Energy + "_MeterGetListByLocationAndClassification";
		/// <summary>
		/// The name of the stored procedure that retreives a list of meters for a specific location.
		/// </summary>
		protected const string const_proc_getlistbykeylocation = AppModule.Energy + "_MeterGetListByKeyLocation";
		/// <summary>
		/// The name of the stored procedure that retrieves all meter that have an endpoint attached.
		/// </summary>
		protected const string const_proc_getallwithendpoint = AppModule.Energy + "_MeterGetAllWithEndpoint";

		/// <summary>
		/// The name of the stored procedure  gets a list of localIds by a given list od localIds
		/// </summary>
		protected const string const_proc_meterGetLocalIdsByLocalIds = AppModule.Energy + "_MeterGetLocalIdsByLocalIds";

		#endregion

		/// <summary>
		/// Gets the const_proc_prefix.
		/// </summary>
		/// <value>The const_proc_prefix.</value>
		protected override string const_proc_prefix {
			get {
				return AppModule.Energy;
			}
		}


		/// <summary>
		/// Populates a Meter business entity from a DataSourceDecorator.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated Meter business entity.</returns>
		protected override Meter FillObject(DataSourceDecorator source, string prefixColumn) {
			var retVal = new Meter {
				KeyMeter = source.Field<string>(prefixColumn + "KeyMeter"),
				Application = ContextHelper.ApplicationName,
				LocalId = source.Field<string>(prefixColumn + "LocalId"),
				Factor = source.Field<double>(prefixColumn + "Factor"),
				Offset = source.Field<double?>(prefixColumn + "Offset") ?? 0,
				Name = source.Field<string>(prefixColumn + "Name"),
				Description = source.Field<string>(prefixColumn + "Description"),
				KeyLocation = source.Field<string>(prefixColumn + "KeyLocation"),
				KeyLocationPath = source.Field<string>(prefixColumn + "KeyLocationPath"),
				LocationLevel = source.Field<int?>("LocationLevel"),
				LocationLongPath = source.Field<string>(prefixColumn + "LocationLongPath"),
				LocationShortPath = source.Field<string>(prefixColumn + "LocationShortPath"),
				LocationName = source.Field<string>(prefixColumn + "LocationName"),
				LocationTypeName = source.Field<string>(prefixColumn + "LocationTypeName").ParseAsEnum<HierarchySpatialTypeName>(),
				GroupOperation = source.Field<string>(prefixColumn + "GroupOperation").ParseAsEnum<MathematicOperator>(),
				UnitInput = source.Field<string>(prefixColumn + "UnitInput"),
				UnitOutput = source.Field<string>(prefixColumn + "UnitOutput"),
				IsAccumulated = source.Field<bool?>(prefixColumn + "IsAccumulated") ?? false,
				IsAcquisitionDateTimeEndInterval = source.Field<bool?>(prefixColumn + "IsAcquisitionDateTimeEndInterval") ?? false,
				RolloverValue = source.Field<double?>(prefixColumn + "RolloverValue"),
				FrequencyInput = source.Field<int?>(prefixColumn + "FrequencyInput"),
				EndpointFrequency = source.Field<int?>(prefixColumn + "EndpointFrequency"),
				KeyEndpoint = source.Field<string>(prefixColumn + "KeyEndpoint"),
				EndpointType = source.Field<string>(prefixColumn + "EndpointType"),
				CalibrationDateTime = source.Field<DateTime?>(prefixColumn + "CalibrationDateTime"),
				CalibrationValue = source.Field<double?>(prefixColumn + "CalibrationValue"),
				AcceptMobileInput = source.Field<bool?>(prefixColumn + "AcceptMobileInput") ?? false,
				KeyClassificationItem = source.Field<string>(prefixColumn + "KeyClassificationItem"),
				KeyClassificationItemPath = source.Field<string>(prefixColumn + "KeyClassificationItemPath"),
				ClassificationItemLongPath = source.Field<string>(prefixColumn + "ClassificationItemLongPath"),
				ClassificationItemLocalIdPath = source.Field<string>(prefixColumn + "ClassificationItemLocalIdPath"),
				ClassificationItemTitle = source.Field<string>(prefixColumn + "ClassificationItemTitle"),
				ClassificationItemLevel = source.Field<int>(prefixColumn + "ClassificationItemLevel"),
				ClassificationItemColorR = source.Field<int>(prefixColumn + "ClassificationItemColorR"),
				ClassificationItemColorG = source.Field<int>(prefixColumn + "ClassificationItemColorG"),
				ClassificationItemColorB = source.Field<int>(prefixColumn + "ClassificationItemColorB"),
				MeterOperationTimeScaleInterval = source.Field<string>(prefixColumn + "MeterOperationTimeScaleInterval").ParseAsEnum<AxisTimeInterval>(),
				ManualInputTimeInterval = source.Field<string>(prefixColumn + "ManualInputTimeInterval").ParseAsEnum<ManualInputTimeInterval>(),
				KeyMeterOperationResult = source.Field<string>(prefixColumn + "KeyMeterOperationResult"),
				DegreeDayType = (source.Field<string>(prefixColumn + "DegreeDayType") ?? "None").ParseAsEnum<DegreeDayType>(),
				DegreeDayBase = source.Field<double?>(prefixColumn + "DegreeDayBase"),
				TimeZoneId = source.Field<string>(prefixColumn + "TimeZoneId"),
				AlarmInstanceFromMeterValidationRuleCount = source.Field<int>(prefixColumn + "AlarmInstanceFromMeterValidationRuleCount"),
				MeterDataCount = source.Field<long?>(prefixColumn + "MeterDataCount") ?? 0,
				AcquisitionDateTimeMin = source.Field<DateTime?>(prefixColumn + "AcquisitionDateTimeMin"),
				AcquisitionDateTimeMax = source.Field<DateTime?>(prefixColumn + "AcquisitionDateTimeMax"),
				KeyCalendarEventCategory = source.Field<string>(prefixColumn + "KeyCalendarEventCategory"),
				CalendarEventCategoryTitle = source.Field<string>(prefixColumn + "CalendarEventCategoryTitle"),
				CalendarMode = (source.Field<string>(prefixColumn + "CalendarMode") ?? "Include").ParseAsEnum<ChartCalendarMode>(),
				PsetName = source.Field<string>(prefixColumn + "PsetName"),
				AttributeName = source.Field<string>(prefixColumn + "AttributeName"),
				KeyPropertySingleValue = source.Field<string>(prefixColumn + "KeyPropertySingleValue"),
				UsageName = source.Field<string>(prefixColumn + "UsageName"),
				PsetMsgCode = source.Field<string>(prefixColumn + "PsetMsgCode"),
				AttributeMsgCode = source.Field<string>(prefixColumn + "AttributeMsgCode"),
				PsetFactorOperator = (source.Field<string>(prefixColumn + "PsetFactorOperator") ?? "Multiply").ParseAsEnum<ArithmeticOperator>(),
				LastRawValue = source.Field<double?>(prefixColumn + "LastRawValue"),
				LastValidity = ((int)(source.Field<byte?>(prefixColumn + "LastValidity") ?? 0)).ParseAsEnum<MeterDataValidity>()

			};

			if (string.IsNullOrEmpty(retVal.PsetMsgCode) == false && string.IsNullOrEmpty(retVal.AttributeMsgCode) == false) {
				retVal.PropertySingleValueLongPath = string.Format("{0} / {1}", Helper.LocalizeText(retVal.PsetMsgCode), Helper.LocalizeText(retVal.AttributeMsgCode));
			}
			return retVal;
		}



		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, Meter item) {
			db.AddInParameterString(command, "Key", item.KeyMeter);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterDouble(command, "Factor", item.Factor);
			db.AddInParameterDouble(command, "Offset", item.Offset);
			db.AddInParameterString(command, "Name", item.Name);
			db.AddInParameterString(command, "Description", item.Description);
			db.AddInParameterString(command, "KeyLocation", item.KeyLocation);
			db.AddInParameterString(command, "GroupOperation", item.GroupOperation.ParseAsString());
			db.AddInParameterString(command, "UnitInput", item.UnitInput);
			db.AddInParameterString(command, "UnitOutput", item.UnitOutput);
			db.AddInParameterBool(command, "IsAccumulated", item.IsAccumulated);
			db.AddInParameterDouble(command, "RolloverValue", item.RolloverValue);
			db.AddInParameterInt(command, "FrequencyInput", item.FrequencyInput);
			db.AddInParameterInt(command, "EndpointFrequency", item.EndpointFrequency);
			db.AddInParameterString(command, "KeyEndpoint", item.KeyEndpoint);
			db.AddInParameterString(command, "EndpointType", item.EndpointType);
			db.AddInParameterDateTime(command, "CalibrationDateTime", item.CalibrationDateTime);
			db.AddInParameterDouble(command, "CalibrationValue", item.CalibrationValue);
			db.AddInParameterBool(command, "AcceptMobileInput", item.AcceptMobileInput);
			db.AddInParameterString(command, "KeyClassificationItem", item.KeyClassificationItem);
			db.AddInParameterString(command, "KeyMeterOperationResult", item.KeyMeterOperationResult);
			db.AddInParameterString(command, "MeterOperationTimeScaleInterval", item.MeterOperationTimeScaleInterval.ParseAsString());
			db.AddInParameterString(command, "ManualInputTimeInterval", item.ManualInputTimeInterval.ParseAsString());
			db.AddInParameterBool(command, "IsAcquisitionDateTimeEndInterval", item.IsAcquisitionDateTimeEndInterval);
			db.AddInParameterString(command, "DegreeDayType", item.DegreeDayType.ParseAsString());
			db.AddInParameterDouble(command, "DegreeDayBase", item.DegreeDayBase);
			db.AddInParameterString(command, "TimeZoneId", item.TimeZoneId);
			db.AddInParameterString(command, "KeyCalendarEventCategory", item.KeyCalendarEventCategory);
			db.AddInParameterString(command, "CalendarEventCategoryTitle", item.CalendarEventCategoryTitle);
			db.AddInParameterString(command, "CalendarMode", item.CalendarMode.ParseAsString());
			db.AddInParameterString(command, "PsetName", item.PsetName);
			db.AddInParameterString(command, "AttributeName", item.AttributeName);
			db.AddInParameterString(command, "PsetFactorOperator", item.PsetFactorOperator.ParseAsString());


			string localId = !string.IsNullOrWhiteSpace(item.LocalId) ? item.LocalId : null;
			db.AddInParameterString(command, "LocalId", localId);
		}
		#region Location
		/// <summary>
		/// Gets a list of Meters for a specific location.
		/// </summary>
		/// <param name="KeyLocation">The Key location.</param>
		/// <returns></returns>
		public List<Meter> GetListByKeyLocation(string KeyLocation) {
			List<Meter> results;
			DbCommand command = db.GetStoredProcCommand(const_proc_getlistbykeylocation);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyLocation", KeyLocation);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}
		/// <summary>
		/// Gets the list of Meters by locations and classificatons.
		/// </summary>
		/// <param name="Locations">The locations.</param>
		/// <param name="ClassificationItems">The classification items.</param>
		/// <returns></returns>
		public List<Meter> GetListByLocationsAndClassificatons(string[] Locations, string[] ClassificationItems) {
			List<Meter> results;
			DbCommand command = db.GetStoredProcCommand(const_proc_getlistbylocationandclassification);
			db.AddInParameterXml(command, "Locations", Helper.BuildXml(Locations));
			db.AddInParameterXml(command, "Classifications", Helper.BuildXml(ClassificationItems));
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}
		#endregion

		#region EndPoint
		/// <summary>
		/// Retrieves all Meters that have an Endpoint attached.
		/// </summary>
		/// <returns></returns>
		public List<Meter> GetAllWithEndpoint() {
			List<Meter> results;
			DbCommand command = db.GetStoredProcCommand(const_proc_getallwithendpoint);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}
		#endregion

		#region KeyChart
		/// <summary>
		/// Gets all the meters for a specific chart.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="keyChart">The Key of the chart.</param>
		/// <param name="total">The total number of items.</param>
		/// <returns>The list of items.</returns>
		public List<Meter> GetAllPagingByKeyChart(PagingParameter paging, string keyChart, out int total) {
			return GetAllPagingByKeyObject(const_proc_getallpagingbykeychart, paging, keyChart, "KeyChart", out total);
		}

		/// <summary>
		/// Returns the complete list of Meter for a specific Chart based on Meter Classification Filter, Spatial Filter, Meters instances.
		/// </summary>
		/// <param name="keyChart">The Key of the chart.</param>
		/// <param name="keyMeterAndLocationOnly">if set to <c>true</c> [key meter and location only].</param>
		/// <param name="filterSpatialKeys">The filter spatial keys.</param>
		/// <param name="metersClassificationsKeys">The meters classifications keys.</param>
		/// <param name="metersKeys">The meters keys.</param>
		/// <returns></returns>
		public List<Meter> GetCompleteListByKeyChart(string keyChart, bool keyMeterAndLocationOnly, IEnumerable<string> filterSpatialKeys = null, 
				IEnumerable<string> metersClassificationsKeys = null, IEnumerable<string> metersKeys = null) {

			List<Meter> results = GetCompleteListByKeyObject(const_proc_getcompletelistbykeychart, keyChart, "KeyChart", keyMeterAndLocationOnly, 
				filterSpatialKeys, metersClassificationsKeys, metersKeys);
			return results;
		}

		/// <summary>
		/// Saves the chart meters.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="meters">The meters.</param>
		/// <returns></returns>
		public List<Meter> SaveChartMeters(Chart entity, CrudStore<Meter> meters) {
			return SaveObjectMeters(const_proc_chartmeterssave, entity.KeyChart, "KeyChart", meters);
		}

		#endregion

		#region MeterDataExportTask
		/// <summary>
		/// Gets all the meters for a specific MeterDataExportTask.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyMeterDataExportTask">The Key of the MeterDataExportTask.</param>
		/// <param name="total">The total number of items.</param>
		/// <returns>The list of items.</returns>
		public List<Meter> GetAllPagingByKeyMeterDataExportTask(PagingParameter paging, string KeyMeterDataExportTask, out int total) {
			return GetAllPagingByKeyObject(const_proc_getallpagingbykeyMeterDataExportTask, paging, KeyMeterDataExportTask, "KeyMeterDataExportTask", out total);
		}

		/// <summary>
		/// Returns the complete list of Meter for a specific MeterDataExportTask based on Meter Classification Filter, Spatial Filter, Meters instances.
		/// </summary>
		/// <param name="KeyMeterDataExportTask">The Key of the MeterDataExportTask.</param>
		/// <returns></returns>
		public List<Meter> GetCompleteListByKeyMeterDataExportTask(string KeyMeterDataExportTask) {
			List<Meter> results = GetCompleteListByKeyObject(const_proc_getcompletelistbykeyMeterDataExportTask, KeyMeterDataExportTask, "KeyMeterDataExportTask");
			return results;
		}

		/// <summary>
		/// Saves the MeterDataExportTask meters.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="meters">The meters.</param>
		/// <returns></returns>
		public List<Meter> SaveMeterDataExportTaskMeters(MeterDataExportTask entity, CrudStore<Meter> meters) {
			return SaveObjectMeters(const_proc_MeterDataExportTaskmeterssave, entity.KeyMeterDataExportTask, "KeyMeterDataExportTask", meters);
		}

		#endregion

		#region AlarmDefinition
		/// <summary>
		/// Gets all the meters for a specific AlarmDefinition.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyAlarmDefinition">The Key of the AlarmDefinition.</param>
		/// <param name="total">The total number of items.</param>
		/// <returns>The list of items.</returns>
		public List<Meter> GetAllPagingByKeyAlarmDefinition(PagingParameter paging, string KeyAlarmDefinition, out int total) {
			return GetAllPagingByKeyObject(const_proc_getallpagingbykeyalarmdefinition, paging, KeyAlarmDefinition, "KeyAlarmDefinition", out total);
		}
		/// <summary>
		/// Returns the complete list of Meter for a specific AlarmDefinition based on Meter Classification Filter, Spatial Filter, Meters instances.
		/// </summary>
		/// <param name="KeyAlarmDefinition">The Key of the AlarmDefinition.</param>
		/// <returns></returns>
		public List<Meter> GetCompleteListByKeyAlarmDefinition(string KeyAlarmDefinition) {
			List<Meter> results = GetCompleteListByKeyObject(const_proc_getcompletelistbykeyalarmdefinition, KeyAlarmDefinition, "KeyAlarmDefinition");
			return results;
		}
		/// <summary>
		/// Saves the AlarmDefinition meters.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="meters">The meters.</param>
		/// <returns></returns>
		public List<Meter> SaveAlarmDefinitionMeters(AlarmDefinition entity, CrudStore<Meter> meters) {
			return SaveObjectMeters(const_proc_alarmdefinitionmeterssave, entity.KeyAlarmDefinition, "KeyAlarmDefinition", meters);
		}
		#endregion

		#region MeterValidationRule
		/// <summary>
		/// Gets all the meters for a specific MeterValidationRule.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyMeterValidationRule">The Key of the MeterValidationRule.</param>
		/// <param name="total">The total number of items.</param>
		/// <returns>The list of items.</returns>
		public List<Meter> GetAllPagingByKeyMeterValidationRule(PagingParameter paging, string KeyMeterValidationRule, out int total) {
			return GetAllPagingByKeyObject(const_proc_getallpagingbykeymetervalidationrule, paging, KeyMeterValidationRule, "KeyMeterValidationRule", out total);
		}
		/// <summary>
		/// Returns the complete list of Meter for a specific MeterValidationRule based on Meter Classification Filter, Spatial Filter, Meters instances.
		/// </summary>
		/// <param name="KeyMeterValidationRule">The Key of the MeterValidationRule.</param>
		/// <returns></returns>
		public List<Meter> GetCompleteListByKeyMeterValidationRule(string KeyMeterValidationRule) {
			List<Meter> results = GetCompleteListByKeyObject(const_proc_getcompletelistbykeymetervalidationrule, KeyMeterValidationRule, "KeyMeterValidationRule");
			return results;
		}


		/// <summary>
		/// Saves the MeterValidationRule meters.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="meters">The meters.</param>
		/// <returns></returns>
		public List<Meter> SaveMeterValidationRuleMeters(MeterValidationRule entity, CrudStore<Meter> meters) {
			return SaveObjectMeters(const_proc_metervalidationrulemeterssave, entity.KeyMeterValidationRule, "KeyMeterValidationRule", meters);
		}
		#endregion



		/// <summary>
		/// Gets all the meters for a specific chart or alarm.
		/// </summary>
		/// <param name="proc">The proc.</param>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="keyObject">The key object.</param>
		/// <param name="fieldKey">The field key.</param>
		/// <param name="total">The total number of items.</param>
		/// <returns>
		/// The list of items.
		/// </returns>
		private List<Meter> GetAllPagingByKeyObject(string proc, PagingParameter paging, string keyObject, string fieldKey, out int total) {
			DbCommand command = db.GetStoredProcCommand(proc);
			command.CommandTimeout = 600;
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, fieldKey, keyObject);
			db.AddInParameterInt(command, "start", paging.start);
			db.AddInParameterInt(command, "limit", paging.limit);
			DataSet ds = db.ExecuteDataSet(command);
			if (ds.Tables[0].Rows.Count > 0)
				total = ds.Tables[0].Rows[0].Field<int>("TotalRecord");
			else
				total = 0;
			return FillObjects(ds);
		}

		/// <summary>
		/// Gets the complete list of Meter for a specific chart or alarm.
		/// </summary>
		/// <param name="proc">The proc.</param>
		/// <param name="keyObject">The key object.</param>
		/// <param name="fieldKey">The field key.</param>
		/// <param name="keyMeterAndLocationOnly">if set to <c>true</c> [key meter and location only].</param>
		/// <param name="filterSpatialKeys">The filter spatial keys.</param>
		/// <param name="metersClassificationsKeys">The meters classifications keys.</param>
		/// <param name="metersKeys">The meters keys.</param>
		/// <returns></returns>
		private List<Meter> GetCompleteListByKeyObject(string proc, string keyObject, string fieldKey, bool keyMeterAndLocationOnly = false,
				IEnumerable<string> filterSpatialKeys = null, IEnumerable<string> metersClassificationsKeys = null, IEnumerable<string> metersKeys = null) {

			List<Meter> results;
			DbCommand command = db.GetStoredProcCommand(proc);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, fieldKey, keyObject);
			db.AddInParameterBool(command, "KeyMeterAndLocationOnly", keyMeterAndLocationOnly);
			AddProjectLocationFilter(command, "ProjectLocationFilter");
			if (filterSpatialKeys != null)
				db.AddInParameterString(command, "FilterSpatial", Helper.BuildXml(filterSpatialKeys.ToArray()));
			if (metersClassificationsKeys != null)
				db.AddInParameterString(command, "MeterClassifications", Helper.BuildXml(metersClassificationsKeys.ToArray()));
			if (metersKeys != null)
				db.AddInParameterString(command, "DirectLinkedMeters", Helper.BuildXml(metersKeys.ToArray()));

			using (var reader = db.ExecuteReader(command)) {
				if (keyMeterAndLocationOnly) {
					results = new List<Meter>();
					while (reader.Read()) {
						results.Add(new Meter {
							KeyMeter = reader["KeyMeter"].ToString(),
							KeyLocation = reader["KeyLocation"].ToString()
						});
					}
				}
				else {
					results = FillObjects(reader);
				}
			}
			return results;
		}

		/// <summary>
		/// Saves the Object meters.
		/// </summary>
		/// <param name="proc">The proc.</param>
		/// <param name="keyObject">The key object.</param>
		/// <param name="fieldKey">The field key.</param>
		/// <param name="meters">The meters.</param>
		/// <returns></returns>
		private List<Meter> SaveObjectMeters(string proc, string keyObject, string fieldKey, CrudStore<Meter> meters) {
			DbCommand command = db.GetStoredProcCommand(proc);
			command.CommandTimeout = 600;
			List<Meter> results;

			if (meters == null || meters.IsEmpty())
				return null;
			string inserts = null;
			string deletes = null;

			if (meters.create != null) {
				inserts = Helper.BuildXml(meters.create.Select(p => p.KeyMeter).ToArray());
			}

			if (meters.destroy != null) {
				deletes = Helper.BuildXml(meters.destroy.Select(p => p.KeyMeter).ToArray());
			}

			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, fieldKey, keyObject);
			db.AddInParameterString(command, "Inserts", inserts);
			db.AddInParameterString(command, "Deletes", deletes);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}

		/// <summary>
		/// Gets the meter local ids by a list of local ids.
		/// </summary>
		/// <param name="localIds">The local ids.</param>
		/// <returns></returns>
		public HashSet<string> GetMeterLocalIdsByLocalIds(IEnumerable<string> localIds){
			DbCommand command = db.GetStoredProcCommand(const_proc_meterGetLocalIdsByLocalIds);
			db.AddInParameterXml(command, "LocalIds", Helper.BuildXml(localIds.ToArray()));
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);

			DataSet ds = db.ExecuteDataSet(command);
			DataTable localIdsTable = ds.Tables[0];

			if (localIdsTable.Rows.Count == 0){
				return new HashSet<string>();
			}

			var localIdsFromDb = new HashSet<string>();
			foreach (DataRow dataRow in localIdsTable.Rows){
				localIdsFromDb.Add(dataRow.Field<string>("LocalId"));
			}

			return localIdsFromDb;
		}


		/// <summary>
		/// Adds the paging parameter filter.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="command">The command.</param>
		protected override void AddPagingParameterFilter(PagingParameter paging, DbCommand command) {
			// Change type of LastValidity value from the name of the enum to its int value.
			MeterHelper.ChangeValidityFieldStringValueToInt(paging, "LastValidity");
			// Perform original logic.
			base.AddPagingParameterFilter(paging, command);
		}


	}
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Web.Security;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Vizelia.FOL.DataLayer.Interfaces;

namespace Vizelia.FOL.DataLayer.BrokerDB {

	/// <summary>
	/// Database broker for VirtualFile business entity.
	/// </summary>
	public class VirtualFileBrokerDB : BaseBrokerDB<VirtualFile> {

		/// <summary>
		/// The name of the stored procedure that retreives a VirtualFile by its path.
		/// </summary>
		protected const string const_proc_getitembypath = AppModule.Energy + "_VirtualFileGetItemByPath";

		/// <summary>
		/// The name of the stored procedure that retreives a VirtualFile for a specific Chart.
		/// </summary>
		protected const string const_proc_getitembykeyentity = AppModule.Energy + "_VirtualFileGetItemByKeyEntity";

		/// <summary>
		/// The prefix of stored procedures of that provider. 
		/// </summary>
		protected override string const_proc_prefix {
			get {
				return AppModule.Energy;
			}
		}
		

		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, VirtualFile item) {
			db.AddInParameterString(command, "Key", item.KeyVirtualFile);
			db.AddInParameterString(command, "LocalId", item.LocalId);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyEntity", item.KeyEntity);
			db.AddInParameterString(command, "Path", item.Path);
			db.AddInParameterString(command, "EntityType", item.EntityType);
			db.AddInParameterInt(command, "Width", item.Width);
			db.AddInParameterInt(command, "Height", item.Height);
			db.AddInParameterInt(command, "MaxRecordCount", item.MaxRecordCount);
		}

		/// <summary>
		/// Populates a VirtualFile business entity from a DataSourceDecorator.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated Portlet business entity.</returns>
		protected override VirtualFile FillObject(DataSourceDecorator source, string prefixColumn) {
			return new VirtualFile {
                Application = ContextHelper.ApplicationName,
				KeyVirtualFile = source.Field<int>(prefixColumn + "KeyVirtualFile").ToString(),
				LocalId = source.Field<string>(prefixColumn + "LocalId"),
				KeyEntity = source.Field<string>(prefixColumn + "KeyEntity").ToString(),
				EntityType = source.Field<string>(prefixColumn + "EntityType"),
				Path = source.Field<string>(prefixColumn + "Path"),
				Width = source.Field<int>(prefixColumn + "Width"),
				Height = source.Field<int>(prefixColumn + "Height"),
				MaxRecordCount=source.Field<int?>(prefixColumn+"MaxRecordCount")??0
			};
		}


		/// <summary>
		///Check if a VirtualFile with this path exists.
		/// </summary>
		/// <param name="path">the path.</param>
		/// <returns></returns>
		public bool PathExists(string path) {
			DbCommand command = db.GetStoredProcCommand(const_proc_getitembypath);
			db.AddInParameterString(command, "Path", path);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			using (IDataReader reader = db.ExecuteReader(command)) {
				if (reader.Read()) {
					return true;
				}
			}
			return false;
		}

		/// <summary>
		/// Returns a VirtualFile from a Path.
		/// </summary>
		/// <param name="path">the path.</param>
		/// <returns></returns>
		public VirtualFile GetItemByPath(string path) {
			VirtualFile retVal = null;
			DbCommand command = db.GetStoredProcCommand(const_proc_getitembypath);
			db.AddInParameterString(command, "Path", path);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			using (IDataReader reader = db.ExecuteReader(command)) {
				if (reader.Read()) {
					retVal = FillObject(reader);
				}
			}
			return retVal;
		}

		/// <summary>
		/// Returns a VirtualFile from an Entity.
		/// </summary>
		/// <param name="KeyEntity">The key entity.</param>
		/// <param name="entityType">Type of the entity.</param>
		/// <returns></returns>
		public VirtualFile GetItemByKeyEntity(string KeyEntity,Type entityType) {
			VirtualFile retVal = null;
			DbCommand command = db.GetStoredProcCommand(const_proc_getitembykeyentity);
			db.AddInParameterString(command, "KeyEntity", KeyEntity);
			db.AddInParameterString(command, "EntityType", entityType.FullName);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			using (IDataReader reader = db.ExecuteReader(command)) {
				if (reader.Read()) {
					retVal = FillObject(reader);
				}
			}
			return retVal;
		}
	}
}
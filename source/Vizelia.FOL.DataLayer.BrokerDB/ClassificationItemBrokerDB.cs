﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web.Security;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Vizelia.FOL.DataLayer.Interfaces;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.DataLayer.BrokerDB {
	/// <summary>
	/// Database broker for ClassificationItem business entity.
	/// </summary>
	public class ClassificationItemBrokerDB : BaseBrokerDB<ClassificationItem> {
		const string const_proc_clean_classificationitem_pset = AppModule.Core + "_ClassificationItemCleanPsetValue";
		const string const_proc_cleanall_classificationitem_pset = AppModule.Core + "_ClassificationItemCleanAllPsetValue";

		/// <summary>
		/// The name of the stored procedure that gets all found classificaition by key word
		/// </summary>
		protected const string const_proc_getallpagingbykeword = AppModule.Core + "_HierarchyClassificationItemGetAllPaging";

		const string const_proc_getallpagingbykeymail = AppModule.ServiceDesk + "_ClassificationItemGetAllPagingByKeyMail";
		const string const_proc_getallpagingbykeyobject = AppModule.Core + "_ClassificationItemGetAllPagingByKeyObject";

		const string const_proc_getlistrelationship = AppModule.Core + "_ClassificationItemGetListRelationship";


		const string const_proc_getall_root = AppModule.Core + "_ClassificationItemGetAllRoot";
		/// <summary>
		/// The name of the stored procedure that retrieve a classificationitem with the hierarchy fields.
		/// </summary>
		const string const_proc_getitemfromhierarchy = AppModule.Core + "_ClassificationItemGetItemFromHierarchy";

		/// <summary>
		/// The name of the stored procedure that saves a classification filter for a specific mail.
		/// </summary>
		const string const_proc_mailclassificationssave = AppModule.ServiceDesk + "_MailClassificationsSave";

		/// <summary>
		/// The name of the stored procedure that gets a workflow assembly definition for a specific classification.
		/// </summary>
		const string const_proc_getworkflowassemblydefinitionbyclassification = AppModule.ServiceDesk + "_WorkflowAssemblyDefinitionGetItemByClassification";

		/// <summary>
		/// The name of the stored procedure that gets a workflow assembly definition for a specific classification in ascendant.
		/// </summary>
		const string const_proc_getworkflowassemblydefinitionbyclassificationascendant = AppModule.ServiceDesk + "_WorkflowAssemblyDefinitionGetItemByClassificationAscendant";

		/// <summary>
		/// The name of the stored procedure that saves a workflow assembly definition for a specific classification.
		/// </summary>
		const string const_proc_workflowassemblydefinitionclassification_saveitem = AppModule.ServiceDesk + "_WorkflowAssemblyDefinitionClassificationSaveItem";

		/// <summary>
		/// The name of the stored procedure that deletes a workflow assembly definition for a specific classification.
		/// </summary>
		const string const_proc_workflowassemblydefinitionclassification_deleteitem = AppModule.ServiceDesk + "_WorkflowAssemblyDefinitionClassificationDeleteItem";

		/// <summary>
		/// The name of the stored procedure that retreives a meter classification filter for a specific chart.
		/// </summary>
		const string const_proc_getallmeterpagingbykeychart = AppModule.Energy + "_FilterMeterClassificationGetAllPagingByKeyChart";

		/// <summary>
		/// The name of the stored procedure that saves a  meter classification filter for a specific chart
		/// </summary>
		const string const_proc_chartfiltermeterclassificationsave = AppModule.Energy + "_ChartFilterMeterClassificationSave";

		/// <summary>
		/// The name of the stored procedure that saves a  meter classification filter for a specific AlarmDefinition
		/// </summary>
		const string const_proc_alarmdefinitionfiltermeterclassificationsave = AppModule.Energy + "_AlarmDefinitionFilterMeterClassificationSave";


		/// <summary>
		/// The name of the stored procedure that saves a  meter classification filter for a specific MeterValidationRule
		/// </summary>
		const string const_proc_metervalidationrulefiltermeterclassificationsave = AppModule.Energy + "_MeterValidationRuleFilterMeterClassificationSave";

		/// <summary>
		/// The name of the stored procedure that saves a  alarm definition classification filter for a specific AlarmTable
		/// </summary>
		const string const_proc_alarmtablefilteralarmdefinitionclassificationsave = AppModule.Energy + "_AlarmTableFilterAlarmDefinitionClassificationSave";

		/// <summary>
		/// The name of the stored procedure that retreives a meter classification filter for a specific AlarmDefinition.
		/// </summary>
		const string const_proc_getallmeterpagingbykeyalarmdefinition = AppModule.Energy + "_FilterMeterClassificationGetAllPagingByKeyAlarmDefinition";

		/// <summary>
		/// The name of the stored procedure that retreives a meter classification filter for a specific MeterValidationRule.
		/// </summary>
		const string const_proc_getallmeterpagingbykeymetervalidationrule = AppModule.Energy + "_FilterMeterClassificationGetAllPagingByKeyMeterValidationRule";

		/// <summary>
		/// The name of the stored procedure that retreives a alarm definition classification filter for a specific AlarmTable.
		/// </summary>
		const string const_proc_getallalarmdefinitionpagingbykeyalarmtable = AppModule.Energy + "_FilterAlarmDefinitionClassificationGetAllPagingByKeyAlarmTable";

		/// <summary>
		/// The name of the stored procedure that retreives a alarm definition classification filter for a specific Chart.
		/// </summary>
		const string const_proc_getallalarmdefinitionpagingbykeychart = AppModule.Energy + "_FilterAlarmDefinitionClassificationGetAllPagingByKeyChart";

		/// <summary>
		/// The name of the stored procedure that retreives an event log classification filter for a specific chart.
		/// </summary>
		const string const_proc_getalleventlogpagingbykeychart = AppModule.Energy + "_FilterEventLogClassificationGetAllPagingByKeyChart";

		/// <summary>
		/// The name of the stored procedure that saves an event log classification filter for a specific chart
		/// </summary>
		const string const_proc_chartfiltereventlogclassificationsave = AppModule.Energy + "_ChartFilterEventLogClassificationSave";

		/// <summary>
		/// The name of the stored procedure that saves an alarmdefinition classification filter for a specific chart
		/// </summary>
		const string const_proc_chartfilteralarmdefinitionclassificationsave = AppModule.Energy + "_ChartFilterAlarmDefinitionClassificationSave";

		/// <summary>
		/// The name of the stored procedure that returns  a list of meter classification child nodes.
		/// </summary>
		const string const_proc_meterclassificationgetchildrenbykeychart = AppModule.Energy + "_MeterClassificationGetChildrenByKeyChart";

		/// <summary>
		/// The name of the stored procedure that returns  a list of meter classification child nodes.
		/// </summary>
		const string const_proc_meterclassificationgetchildrenbykeyalarmdefinition = AppModule.Energy + "_MeterClassificationGetChildrenByKeyAlarmDefinition";

		/// <summary>
		/// The name of the stored procedure that returns  a list of meter classification child nodes.
		/// </summary>
		const string const_proc_meterclassificationgetchildrenbykeymetervalidationrule = AppModule.Energy + "_MeterClassificationGetChildrenByKeyMeterValidationRule";

		/// <summary>
		/// The name of the stored procedure that get the max level of Meter Classification in the hierarchy.
		/// </summary>
		const string const_proc_meterclassificationgetmaxlevel = AppModule.Energy + "_MeterClassificationGetMaxLevel";

		private const string const_type_filterjointype = "dbo.FilterJoinType";

		/// <summary>
		/// Initializes a new instance of the <see cref="ClassificationItemBrokerDB"/> class.
		/// </summary>
		public ClassificationItemBrokerDB() {
			IsHierarchical = true;
		}

		/// <summary>
		/// Populates a ClassificationItem business entity from a DataSourceDecorator.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated ClassificationItem business entity.</returns>
		protected override ClassificationItem FillObject(DataSourceDecorator source, string prefixColumn) {
			ClassificationItem entity = new ClassificationItem {
				KeyClassificationItem = source.Field<string>(prefixColumn + "KeyClassificationItem"),
				LocalId = source.Field<string>(prefixColumn + "LocalId"),
				Title = source.Field<string>(prefixColumn + "Title"),
				Code = source.Field<string>(prefixColumn + "Code"),
				KeyParent = source.Field<string>(prefixColumn + "KeyParent")
			};
			if (source.FieldExists(prefixColumn + "ColorR") && source.FieldExists(prefixColumn + "ColorG") && source.FieldExists(prefixColumn + "ColorB")) {
				entity.ColorR = source.Field<int>(prefixColumn + "ColorR");
				entity.ColorG = source.Field<int>(prefixColumn + "ColorG");
				entity.ColorB = source.Field<int>(prefixColumn + "ColorB");
			}

			if (source.FieldExists(prefixColumn + "LongPath")) {
				entity.LongPath = source.Field<string>(prefixColumn + "LongPath");
			}

			if (source.FieldExists(prefixColumn + "Level")) {
				entity.Level = source.Field<int>(prefixColumn + "Level");
			}

			if (source.FieldExists(prefixColumn + "LocalIdPath")) {
				entity.LocalIdPath = source.Field<string>(prefixColumn + "LocalIdPath");
			}

			if (source.FieldExists(prefixColumn + "Category") && source.FieldExists(prefixColumn + "IconRootOnly") && source.FieldExists(prefixColumn + "IconCls")) {
				entity.Category = source.Field<string>(prefixColumn + "Category");
				entity.IconRootOnly = source.Field<bool>(prefixColumn + "IconRootOnly");
				entity.IconCls = source.Field<string>(prefixColumn + "IconCls");
			}
			else
				SetProperties(entity);

			prefixColumn += "Relation_";
			if (source.FieldExists(prefixColumn + "KeyClassificationItemRelationship")) {
				if (source.Field<string>(prefixColumn + "KeyClassificationItemRelationship") != null) {
					entity.Relation = new ClassificationItemRelationship {
						KeyChildren = source.Field<string>(prefixColumn + "KeyChildren"),
						KeyParent = source.Field<string>(prefixColumn + "KeyParent"),
						KeyClassificationItemRelationship = source.Field<string>(prefixColumn + "KeyClassificationItemRelationship"),
						RelationType = source.Field<string>(prefixColumn + "RelationType"),
						RelationPath = source.Field<string>(prefixColumn + "RelationPath"),
						RelationOrder = source.Field<int?>(prefixColumn + "RelationOrder") ?? 0
					};
				}
			}

			return entity;
		}

		/// <summary>
		/// Sets the calculated properties of the ClassificationItem.
		/// </summary>
		/// <param name="item">The item.</param>
		private void SetProperties(ClassificationItem item) {
			ClassificationItem itemRoot = null;
			try {
				itemRoot = GetRootClassificationItem(item.Code);
			}
			catch { }
			item.Category = itemRoot != null ? itemRoot.Category : null;
			item.IconRootOnly = itemRoot != null && itemRoot.IconRootOnly;
			if (itemRoot != null && (!itemRoot.IconRootOnly || itemRoot.Code == item.Code)) {
				item.IconCls = itemRoot.IconCls;
			}
			else
				item.IconCls = null;
		}

		/// <summary>
		/// Gets the root classification item by the Code of this classification item.
		/// </summary>
		/// <returns></returns>
		private ClassificationItem GetRootClassificationItem(string code) {
			if (code == null) return null;
			// This only works if the list is sorted by code!!
			ClassificationItem root = GetAllClassificationItemDefinition().FirstOrDefault(c => code.StartsWith(c.Code));
			return root;
		}


		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, ClassificationItem item) {
			db.AddInParameterString(command, "Key", item.KeyClassificationItem);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "Title", item.Title);
			db.AddInParameterString(command, "Code", item.Code);
			db.AddInParameterInt(command, "ColorR", item.ColorR);
			db.AddInParameterInt(command, "ColorG", item.ColorG);
			db.AddInParameterInt(command, "ColorB", item.ColorB);
			db.AddInParameterString(command, "KeyParent", item.KeyParent);
			db.AddInParameterBool(command, "IsMappingInProgress", IsAsynchronousSave);
			string localId = !string.IsNullOrWhiteSpace(item.LocalId) ? item.LocalId : null;
			db.AddInParameterString(command, "LocalId", localId);
		}

		/// <summary>
		/// Gets the children.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <param name="path">The path.</param>
		/// <returns></returns>
		public List<ClassificationItem> GetChildren(string key, string path) {
			List<ClassificationItem> results;
			DbCommand command = db.GetStoredProcCommand(const_proc_getchildren);
			db.AddInParameterString(command, "Key", key);
			db.AddInParameterString(command, "Path", path);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}


		/// <summary>
		/// Gets a classification item with hierachy fields.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <returns></returns>
		public ClassificationItem GetItemFromHierarchy(string key) {
			ClassificationItem item = default(ClassificationItem);
			DbCommand command = db.GetStoredProcCommand(const_proc_getitemfromhierarchy);
			db.AddInParameterString(command, "Key", key);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			using (IDataReader reader = db.ExecuteReader(command)) {
				if (reader.Read()) {
					item = FillObject(reader);
				}
			}
			return item;
		}




		/// <summary>
		/// Gets all the priorities for a specific mail.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="keyMail">The Key of the Mail.</param>
		/// <param name="total">The total number of items.</param>
		/// <returns>The list of items.</returns>
		public List<ClassificationItem> GetAllPagingByKeyMail(PagingParameter paging, string keyMail, out int total) {
			DbCommand command = db.GetStoredProcCommand(const_proc_getallpagingbykeymail);
			command.CommandTimeout = 600;
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyMail", keyMail);
			db.AddInParameterInt(command, "start", paging.start);
			db.AddInParameterInt(command, "limit", paging.limit);
			DataSet ds = db.ExecuteDataSet(command);
			total = ds.Tables[0].Rows.Count > 0 ? ds.Tables[0].Rows[0].Field<int>("TotalRecord") : 0;
			return FillObjects(ds);

		}

		/// <summary>
		/// Gets all the classificationitems for a specific object except objects having their own specific function (mail etc...).
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="keyObject">The key object.</param>
		/// <param name="total">The total number of items.</param>
		/// <returns>
		/// The list of items.
		/// </returns>
		public List<ClassificationItem> GetAllPagingByKeyObject(PagingParameter paging, string keyObject, out int total) {
			DbCommand command = db.GetStoredProcCommand(const_proc_getallpagingbykeyobject);
			command.CommandTimeout = 600;
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyObject", keyObject);
			db.AddInParameterInt(command, "start", paging.start);
			db.AddInParameterInt(command, "limit", paging.limit);
			DataSet ds = db.ExecuteDataSet(command);
			total = ds.Tables[0].Rows.Count > 0 ? ds.Tables[0].Rows[0].Field<int>("TotalRecord") : 0;
			return FillObjects(ds);

		}



		/// <summary>
		/// Gets all the filter meter classification for a specific chart.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="keyChart">The Key of the chart.</param>
		/// <param name="total">The total number of items.</param>
		/// <returns>The list of items.</returns>
		public List<ClassificationItem> GetFilterMeterClassificationAllPagingByKeyChart(PagingParameter paging, string keyChart, out int total) {
			return GetFilterClassificationAllPagingByKeyObject(const_proc_getallmeterpagingbykeychart, paging, keyChart, "KeyChart", out total);
		}

		/// <summary>
		/// Gets all the filter meter classification for a specific AlarmDefinition.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="keyAlarmDefinition">The Key of the AlarmDefinition.</param>
		/// <param name="total">The total number of items.</param>
		/// <returns>The list of items.</returns>
		public List<ClassificationItem> GetFilterMeterClassificationAllPagingByKeyAlarmDefinition(PagingParameter paging, string keyAlarmDefinition, out int total) {
			return GetFilterClassificationAllPagingByKeyObject(const_proc_getallmeterpagingbykeyalarmdefinition, paging, keyAlarmDefinition, "KeyAlarmDefinition", out total);
		}

		/// <summary>
		/// Gets all the filter meter classification for a specific MeterValidationRule.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="keyMeterValidationRule">The Key of the MeterValidationRule.</param>
		/// <param name="total">The total number of items.</param>
		/// <returns>The list of items.</returns>
		public List<ClassificationItem> GetFilterMeterClassificationAllPagingByKeyMeterValidationRule(PagingParameter paging, string keyMeterValidationRule, out int total) {
			return GetFilterClassificationAllPagingByKeyObject(const_proc_getallmeterpagingbykeymetervalidationrule, paging, keyMeterValidationRule, "KeyMeterValidationRule", out total);
		}

		/// <summary>
		/// Gets all the filter meter classification for a specific AlarmDefinition.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="keyAlarmTable">The key alarm table.</param>
		/// <param name="total">The total number of items.</param>
		/// <returns>
		/// The list of items.
		/// </returns>
		public List<ClassificationItem> GetFilterAlarmDefinitionClassificationStoreByKeyAlarmTable(PagingParameter paging, string keyAlarmTable, out int total) {
			return GetFilterClassificationAllPagingByKeyObject(const_proc_getallalarmdefinitionpagingbykeyalarmtable, paging, keyAlarmTable, "KeyAlarmTable", out total);
		}


		/// <summary>
		/// Gets all the filter meter classification for a specific Chart.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="keyChart">The key chart.</param>
		/// <param name="total">The total number of items.</param>
		/// <returns>
		/// The list of items.
		/// </returns>
		public List<ClassificationItem> GetFilterAlarmDefinitionClassificationStoreByKeyChart(PagingParameter paging, string keyChart, out int total) {
			return GetFilterClassificationAllPagingByKeyObject(const_proc_getallalarmdefinitionpagingbykeychart, paging, keyChart, "KeyChart", out total);
		}



		/// <summary>
		/// Gets all the filter eventlog classification for a specific chart.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="keyChart">The Key of the chart.</param>
		/// <param name="total">The total number of items.</param>
		/// <returns>The list of items.</returns>
		public List<ClassificationItem> GetFilterEventLogClassificationAllPagingByKeyChart(PagingParameter paging, string keyChart, out int total) {
			return GetFilterClassificationAllPagingByKeyObject(const_proc_getalleventlogpagingbykeychart, paging, keyChart, "KeyChart", out total);
		}

		/// <summary>
		/// Generic function for getting a List of ClassificationItem filter associated to a Chart.
		/// </summary>
		/// <param name="proc">the name of the stored procedure to launch.</param>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="keyObject">The Key of the object.</param>
		/// <param name="fieldKey">The name of the field key.</param>
		/// <param name="total">The total number of items.</param>
		/// <returns>
		/// The list of items.
		/// </returns>
		private List<ClassificationItem> GetFilterClassificationAllPagingByKeyObject(string proc, PagingParameter paging, string keyObject, string fieldKey, out int total) {
			DbCommand command = db.GetStoredProcCommand(proc);
			command.CommandTimeout = 600;
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, fieldKey, keyObject);
			db.AddInParameterInt(command, "start", paging.start);
			db.AddInParameterInt(command, "limit", paging.limit);
			DataSet ds = db.ExecuteDataSet(command);
			total = ds.Tables[0].Rows.Count > 0 ? ds.Tables[0].Rows[0].Field<int>("TotalRecord") : 0;
			return FillObjects(ds);
		}

		/// <summary>
		/// Saves the chart Filter Meter Classification.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="filterMeterClassification">The filter meter classification.</param>
		/// <returns></returns>
		public List<ClassificationItem> SaveChartFilterMeterClassification(Chart entity, CrudStore<ClassificationItem> filterMeterClassification) {
			return SaveObjectFilterClassification(const_proc_chartfiltermeterclassificationsave, entity.KeyChart, "KeyChart", filterMeterClassification);
		}

		/// <summary>
		/// Saves the AlarmTable filter AlarmDefinition classification.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="filterAlarmDefinitionClassification">The filter AlarmDefinition classification.</param>
		/// <returns></returns>
		public List<ClassificationItem> SaveAlarmDefinitionFilterMeterClassification(AlarmDefinition entity, CrudStore<ClassificationItem> filterAlarmDefinitionClassification) {
			return SaveObjectFilterClassification(const_proc_alarmdefinitionfiltermeterclassificationsave, entity.KeyAlarmDefinition, "KeyAlarmDefinition", filterAlarmDefinitionClassification);
		}

		/// <summary>
		/// Saves the AlarmTable filter MeterValidationRule classification.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="filterMeterValidationRuleClassification">The filter MeterValidationRule classification.</param>
		/// <returns></returns>
		public List<ClassificationItem> SaveMeterValidationRuleFilterMeterClassification(MeterValidationRule entity, CrudStore<ClassificationItem> filterMeterValidationRuleClassification) {
			return SaveObjectFilterClassification(const_proc_metervalidationrulefiltermeterclassificationsave, entity.KeyMeterValidationRule, "KeyMeterValidationRule", filterMeterValidationRuleClassification);
		}

		/// <summary>
		/// Saves the AlarmDefinition Filter Meter Classification.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="filterAlarmDefinitionClassification">The filter meter classification.</param>
		/// <returns></returns>
		public List<ClassificationItem> SaveAlarmTableFilterAlarmDefinitionClassification(AlarmTable entity, CrudStore<ClassificationItem> filterAlarmDefinitionClassification) {
			return SaveObjectFilterClassification(const_proc_alarmtablefilteralarmdefinitionclassificationsave, entity.KeyAlarmTable, "KeyAlarmTable", filterAlarmDefinitionClassification);
		}

		/// <summary>
		/// Saves the chart Filter EventLog Classification.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="filterEventLogClassification">The filter eventlog classification.</param>
		/// <returns></returns>
		public List<ClassificationItem> SaveChartFilterEventLogClassification(Chart entity, CrudStore<ClassificationItem> filterEventLogClassification) {
			return SaveObjectFilterClassification(const_proc_chartfiltereventlogclassificationsave, entity.KeyChart, "KeyChart", filterEventLogClassification);
		}

		/// <summary>
		/// Saves the chart Filter AlarmDefinition Classification.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="filterAlarmDefinitionClassification">The filter alarm definition classification.</param>
		/// <returns></returns>
		public List<ClassificationItem> SaveChartFilterAlarmDefinitionClassification(Chart entity, CrudStore<ClassificationItem> filterAlarmDefinitionClassification) {
			return SaveObjectFilterClassification(const_proc_chartfilteralarmdefinitionclassificationsave, entity.KeyChart, "KeyChart", filterAlarmDefinitionClassification);
		}

		/// <summary>
		/// Generic function for saving a Filter store associated to a Chart.
		/// </summary>
		/// <param name="proc">The proc name.</param>
		/// <param name="keyObject">The key object.</param>
		/// <param name="fieldKey">The name of the field key.</param>
		/// <param name="filterClassification">The filter classification.</param>
		/// <returns></returns>
		private List<ClassificationItem> SaveObjectFilterClassification(string proc, string keyObject, string fieldKey, CrudStore<ClassificationItem> filterClassification) {
			DbCommand command = db.GetStoredProcCommand(proc);
			command.CommandTimeout = 600;
			List<ClassificationItem> results;

			if (filterClassification == null || filterClassification.IsEmpty())
				return null;
			string inserts = null;
			string deletes = null;

			if (filterClassification.create != null) {
				inserts = Helper.BuildXml(filterClassification.create.Select(p => p.KeyClassificationItem).ToArray());
			}

			if (filterClassification.destroy != null) {
				deletes = Helper.BuildXml(filterClassification.destroy.Select(p => p.KeyClassificationItem).ToArray());
			}

			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, fieldKey, keyObject);
			db.AddInParameterString(command, "Inserts", inserts);
			db.AddInParameterString(command, "Deletes", deletes);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}

		/// <summary>
		/// Saves the mail priorities.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="classifications">The classifications.</param>
		/// <returns></returns>
		public List<ClassificationItem> SaveMailClassifications(Mail entity, CrudStore<ClassificationItem> classifications) {
			DbCommand command = db.GetStoredProcCommand(const_proc_mailclassificationssave);
			command.CommandTimeout = 600;
			List<ClassificationItem> results;

			if (classifications == null || classifications.IsEmpty())
				return null;
			string inserts = null;
			string deletes = null;

			if (classifications.create != null) {
				inserts = Helper.BuildXml(classifications.create.Select(p => p.KeyClassificationItem).ToArray());
			}

			if (classifications.destroy != null) {
				deletes = Helper.BuildXml(classifications.destroy.Select(p => p.KeyClassificationItem).ToArray());
			}

			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyMail", entity.KeyMail);
			db.AddInParameterString(command, "Inserts", inserts);
			db.AddInParameterString(command, "Deletes", deletes);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}

		/// <summary>
		/// Gets children for an item.
		/// </summary>
		/// <param name="key">The Key of the parent item.</param>
		/// <param name="keyChart">The key chart.</param>
		/// <param name="keyAlarmDefinition">The key alarm definition.</param>
		/// <param name="keyMeterValidationRule">The key meter validation rule.</param>
		/// <param name="existing">if set to <c>true</c>
		/// the method will only return MeterClassifcation for meter that are attached to the chart,
		/// else it will return all available MeterClassification in the spatial filter of the chart.</param>
		/// <param name="filterSpatial">The current filter spatial of the Chart.</param>
		/// <returns></returns>
		public List<ClassificationItem> MeterClassification_GetChildren(string key, string keyChart, string keyAlarmDefinition, string keyMeterValidationRule, bool existing, CrudStore<Location> filterSpatial) {

			string inserts = null;
			string deletes = null;
			// ReSharper disable ExpressionIsAlwaysNull
			if (filterSpatial != null || !filterSpatial.IsEmpty()) {
				// ReSharper restore ExpressionIsAlwaysNull
				// ReSharper disable PossibleNullReferenceException
				if (filterSpatial.create != null) {
					// ReSharper restore PossibleNullReferenceException
					inserts = Helper.BuildXml(filterSpatial.create.Select(p => p.KeyLocation).ToArray());
				}

				if (filterSpatial.destroy != null) {
					deletes = Helper.BuildXml(filterSpatial.destroy.Select(p => p.KeyLocation).ToArray());
				}
			}

			List<ClassificationItem> retVal;

			DbCommand command;
			if (!string.IsNullOrEmpty(keyAlarmDefinition)) {
				command = db.GetStoredProcCommand(const_proc_meterclassificationgetchildrenbykeyalarmdefinition);
				db.AddInParameterString(command, "KeyAlarmDefinition", keyAlarmDefinition);
			}
			else if (!string.IsNullOrEmpty(keyMeterValidationRule)) {
				command = db.GetStoredProcCommand(const_proc_meterclassificationgetchildrenbykeymetervalidationrule);
				db.AddInParameterString(command, "KeyMeterValidationRule", keyMeterValidationRule);
			}
			else {
				command = db.GetStoredProcCommand(const_proc_meterclassificationgetchildrenbykeychart);
				db.AddInParameterString(command, "KeyChart", keyChart);
			}

			db.AddInParameterString(command, "Key", key);
			db.AddInParameterBool(command, "Existing", existing);
			db.AddInParameterString(command, "FilterSpatialInserts", inserts);
			db.AddInParameterString(command, "FilterSpatialDeletes", deletes);
			AddSecurityFilterParam<Location>(command, "ProjectLocationFilter");
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			using (IDataReader reader = db.ExecuteReader(command)) {
				retVal = FillObjects(reader);
			}
			return retVal;

		}

		/// <summary>
		///  Get the max level of site in the Spatial Hierarchy.
		/// </summary>
		/// <returns></returns>
		public int MeterClassification_GetMaxLevel() {
			DbCommand command = db.GetStoredProcCommand(const_proc_meterclassificationgetmaxlevel);
			command.CommandTimeout = 600;
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			DataSet ds = db.ExecuteDataSet(command);
			return ds.Tables[0].Rows.Count > 0 ? ds.Tables[0].Rows[0].Field<int?>("Level") ?? 0 : 0;
		}

		/// <summary>
		/// Creates an association between a ClassificationItem and a WorkflowAssemblyDefinition.
		/// </summary>
		/// <param name="keyClassificationChildren">The Key of the ClassificationItem.</param>
		/// <param name="keyClassificationParent">The Key of the parent ClassificationItem in case the priority is added to a relationship.</param>
		/// <param name="workflowAssemblyShortName">Name of the workflow assembly definition.</param>
		/// <param name="workflowAssemblyQualifiedName">Name of the workflow assembly definition assembly qualified.</param>
		/// <returns></returns>
		public bool AddWorkflowAssemblyDefinitionClassification(string keyClassificationChildren, string keyClassificationParent, string workflowAssemblyShortName, string workflowAssemblyQualifiedName) {
			DbCommand command = db.GetStoredProcCommand(const_proc_workflowassemblydefinitionclassification_saveitem);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyClassificationChildren", keyClassificationChildren);
			db.AddInParameterString(command, "KeyClassificationParent", keyClassificationParent);
			db.AddInParameterString(command, "WorkflowAssemblyShortName", workflowAssemblyShortName);
			db.AddInParameterString(command, "WorkflowAssemblyQualifiedName", workflowAssemblyQualifiedName);

			db.AddOutParameterString(command, "Return");
			db.AddReturnParameter(command);
			db.ExecuteNonQuery(command);

			string result = Convert.ToString(db.GetParameterValue(command, "Return"));
			return result.Length > 0;
		}

		/// <summary>
		/// Deletes an association between a ClassificationItem and a WorkflowAssemblyDefinition.
		/// </summary>
		/// <param name="keyClassificationChildren">The Key of the ClassificationItem.</param>
		/// <param name="keyClassificationParent">The Key of the parent ClassificationItem in case the priority is added to a relationship.</param>
		/// <returns></returns>
		public bool DeleteWorkflowAssemblyDefinitionClassification(string keyClassificationChildren, string keyClassificationParent) {
			DbCommand command = db.GetStoredProcCommand(const_proc_workflowassemblydefinitionclassification_deleteitem);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyClassificationChildren", keyClassificationChildren);
			db.AddInParameterString(command, "KeyClassificationParent", keyClassificationParent);
			db.ExecuteNonQuery(command);
			return true;
		}


		/// <summary>
		/// Gets the WorkflowAssemblyDefinition object assigned to a ClassificationItem.
		/// </summary>
		/// <param name="keyClassificationChildren">The key classification children.</param>
		/// <param name="keyClassificationParent">The key classification parent.</param>
		/// <returns></returns>
		public WorkflowAssemblyDefinition GetWorkflowAssemblyDefinitionByClassification(string keyClassificationChildren, string keyClassificationParent) {
			var retVal = GetWorkflowAssemblyDefinitionByClassificationInternal(const_proc_getworkflowassemblydefinitionbyclassification, keyClassificationChildren, keyClassificationParent);
			return retVal;
		}

		/// <summary>
		/// Gets the WorkflowAssemblyDefinition assigned to a any upper level of ClassificationItem Hierarchy.
		/// </summary>
		/// <param name="keyClassificationChildren">The key classification children.</param>
		/// <param name="keyClassificationParent">The key classification parent.</param>
		/// <returns></returns>
		public WorkflowAssemblyDefinition GetWorkflowAssemblyDefinitionByClassificationAscendant(string keyClassificationChildren, string keyClassificationParent) {
			var retVal = GetWorkflowAssemblyDefinitionByClassificationInternal(const_proc_getworkflowassemblydefinitionbyclassificationascendant, keyClassificationChildren, keyClassificationParent);
			return retVal;
		}

		/// <summary>
		/// Gets a workflow assembly definition given the proc to execute, KeyClasificationChildren and KeyClassificationParent.
		/// </summary>
		/// <param name="proc">The proc.</param>
		/// <param name="keyClassificationChildren">The key classification children.</param>
		/// <param name="keyClassificationParent">The key classification parent.</param>
		/// <returns></returns>
		private WorkflowAssemblyDefinition GetWorkflowAssemblyDefinitionByClassificationInternal(string proc, string keyClassificationChildren, string keyClassificationParent) {
			WorkflowAssemblyDefinition result = default(WorkflowAssemblyDefinition);

			DbCommand command = db.GetStoredProcCommand(proc);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyClassificationChildren", keyClassificationChildren);
			db.AddInParameterString(command, "KeyClassificationParent", keyClassificationParent);
			using (IDataReader reader = db.ExecuteReader(command)) {
				if (reader.Read()) {
					result = new WorkflowAssemblyDefinition {
						AssemblyShortName = reader.GetValue<string>("WorkflowAssemblyShortName"),
						AssemblyQualifiedName = reader.GetValue<string>("WorkflowAssemblyQualifiedName")
					};
				}
			}
			return result;
		}

		/// <summary>
		/// Return the complete list of ClassificationItem.
		/// </summary>
		/// <param name="keyClassificationItemPath">The key classification item path (optional).</param>
		/// <returns></returns>
		public List<ClassificationItem> GetAll(string keyClassificationItemPath) {

			List<ClassificationItem> retVal;
			DbCommand command = db.GetStoredProcCommand(const_proc_generic_paging);
			command.CommandTimeout = 600;
			db.AddInParameterString(command, "datasrc", "HierarchyClassificationItem");

			string whereClause = string.Format("App = '{0}'", ContextHelper.ApplicationName);

			if (!string.IsNullOrEmpty(keyClassificationItemPath)) {
				string keyClassificationItemFilter = string.Format(" AND KeyClassificationItemPath LIKE '%{0}%'", keyClassificationItemPath);
				whereClause += keyClassificationItemFilter;
			}

			db.AddInParameterString(command, "filter", whereClause);

			using (IDataReader reader = db.ExecuteReader(command)) {
				retVal = FillObjects(reader);
			}
			return retVal;
		}



		/// <summary>
		/// Gets all the classificaion items found by key word.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="total">The total number of items.</param>
		/// <returns>
		/// The list of items.
		/// </returns>
		public List<ClassificationItem> GetAll(PagingParameter paging, out int total) {
			DbCommand command = db.GetStoredProcCommand(const_proc_getallpagingbykeword);
			command.CommandTimeout = 600;
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "@Keyword", paging.query);
			db.AddInParameterInt(command, "start", paging.start);
			db.AddInParameterInt(command, "limit", paging.limit);
			DataSet ds = db.ExecuteDataSet(command);
			if (ds.Tables[0].Rows.Count > 0)
				total = ds.Tables[0].Rows[0].Field<int>("TotalRecord");
			else
				total = 0;
			return FillObjects(ds);

		}

		/// <summary>
		/// Gets all the classificaion items from their relationships.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="total">The total number of items.</param>
		/// <returns>
		/// The list of items.
		/// </returns>
		public List<ClassificationItem> GetListRelationship(PagingParameter paging, out int total) {
			DbCommand command = db.GetStoredProcCommand(const_proc_getlistrelationship);
			command.CommandTimeout = 600;
			db.AddInParameterString(command, "Application", Membership.ApplicationName);
			db.AddInParameterTable(command, "filterjoin", BuildJoinFilterType(paging.innerjoinfilters), const_type_filterjointype);
			db.AddInParameterInt(command, "start", paging.start);
			db.AddInParameterInt(command, "limit", paging.limit);
			DataSet ds = db.ExecuteDataSet(command);
			if (ds.Tables[0].Rows.Count > 0)
				total = ds.Tables[0].Rows[0].Field<int>("TotalRecord");
			else
				total = 0;
			return FillObjects(ds);

		}



		/// <summary>
		/// Cleans the pset values for the given classification item and psets
		/// </summary>
		/// <param name="keyclassificationItemXml">The keyclassification item XML.</param>
		/// <param name="keyPset">The key pset.</param>
		/// <param name="psetAttributeListKeysXml">The pset attribute list keys XML.</param>
		public void ClassificationItem_CleanPsetValue(string keyclassificationItemXml, string keyPset, string psetAttributeListKeysXml) {

			DbCommand command = db.GetStoredProcCommand(const_proc_clean_classificationitem_pset);
			command.CommandTimeout = 600;
			db.AddInParameterXml(command, "KeyClassificationItem", keyclassificationItemXml);
			db.AddInParameterString(command, "KeyPset", keyPset);
			db.AddInParameterXml(command, "KeysPsetAttributes", psetAttributeListKeysXml);

			db.ExecuteNonQuery(command);
		}

		/// <summary>
		/// Clean all pset value for a Classification Item.
		/// </summary>
		/// <param name="keyclassificationItemXml">The keyclassification item XML.</param>
		public void ClassificationItem_CleanAllPsetValue(string keyclassificationItemXml) {
			DbCommand command = db.GetStoredProcCommand(const_proc_cleanall_classificationitem_pset);
			command.CommandTimeout = 600;
			db.AddInParameterXml(command, "KeyClassificationItemToClean", keyclassificationItemXml);

			db.ExecuteNonQuery(command);
		}


		/// <summary>
		/// Builds the additional delete params.
		/// </summary>
		/// <param name="command">The command.</param>
		protected override void BuildAdditionalDeleteParams(DbCommand command) {
			db.AddInParameterBool(command, "IsMappingInProgress", IsAsynchronousSave);
		}


		/// <summary>
		/// Gets all the classification items which are root nodes issued from ClassificationDefinition.
		/// The values are cached per ApplicationName to optimize performance.
		/// </summary>
		/// <returns></returns>
		public List<ClassificationItem> GetAllClassificationItemDefinition() {
			return CacheService.Get(CacheKey.const_cache_classifications, () => {
				// need a new db connection because we are executing a new DataReader inside FillObject that already have an open DataReader
				var innerdb = Helper.CreateInstance<DataAccess, IDataAccess>();
				List<ClassificationItem> retVal;
				DbCommand command = innerdb.GetStoredProcCommand(const_proc_getall_root);
				innerdb.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
				using (IDataReader reader = innerdb.ExecuteReader(command)) {
					retVal = FillObjects(reader);
				}

				return retVal;
			});
		}
	}
}

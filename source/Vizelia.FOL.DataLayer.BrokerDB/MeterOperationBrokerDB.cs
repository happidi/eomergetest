﻿using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.DataLayer.BrokerDB {
	/// <summary>
	/// Database broker for MeterOperation business entity.
	/// </summary>
	public class MeterOperationBrokerDB : BaseBrokerDB<MeterOperation> {

		/// <summary>
		/// The name of the stored procedure that retreives a list of meteroperations for a specific meter.
		/// </summary>
		protected const string const_proc_getlistbymeter = AppModule.Energy + "_MeterOperationGetListByKeyMeter";


		/// <summary>
		/// The name of the stored procedure that retreives a list of meteroperations for a specific meter.
		/// </summary>
		protected const string const_proc_getallpagingbykeymeter = AppModule.Energy + "_MeterOperationGetAllPagingByKeyMeter";


		/// <summary>
		/// The name of the stored procedure that saves a list of meteroperations for a specific meter.
		/// </summary>
		protected const string const_proc_metermeteroperationssave = AppModule.Energy + "_MeterMeterOperationsSave";


		/// <summary>
		/// Gets the const_proc_prefix.
		/// </summary>
		/// <value>The const_proc_prefix.</value>
		protected override string const_proc_prefix {
			get {
				return AppModule.Energy;
			}
		}


		/// <summary>
		/// Populates a MeterOperation business entity from a DataSourceDecorator.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated MeterOperation business entity.</returns>
		protected override MeterOperation FillObject(DataSourceDecorator source, string prefixColumn) {
			var keyMeterOperation1 = source.Field<int?>(prefixColumn + "KeyMeterOperation1");
			var keyMeterOperation2 = source.Field<int?>(prefixColumn + "KeyMeterOperation2");
			var retVal = new MeterOperation {
				KeyMeterOperation = source.Field<int>(prefixColumn + "KeyMeterOperation").ToString(CultureInfo.InvariantCulture),
				LocalId = source.Field<string>(prefixColumn + "LocalId"),
				KeyMeter = source.Field<string>(prefixColumn + "KeyMeter"),
				Order = source.Field<int>(prefixColumn + "Order"),
				Factor1 = source.Field<double>(prefixColumn + "Factor1"),
				Function1 = source.Field<string>(prefixColumn + "Function1").ParseAsEnum<AlgebricFunction>(),
				Offset1 = source.Field<double?>(prefixColumn + "Offset1"),
				KeyMeter1 = source.Field<string>(prefixColumn + "KeyMeter1"),
				NameMeter1 = source.Field<string>(prefixColumn + "NameMeter1"),
				KeyMeterOperation1 = keyMeterOperation1.HasValue ? keyMeterOperation1.Value.ToString(CultureInfo.InvariantCulture) : null,
				LocalIdMeterOperation1 = source.Field<string>(prefixColumn + "LocalIdMeterOperation1"),
				Operator = source.Field<string>(prefixColumn + "Operator").ParseAsEnum<ArithmeticOperator>(),
				Factor2 = source.Field<double>(prefixColumn + "Factor2"),
				Function2 = source.Field<string>(prefixColumn + "Function2").ParseAsEnum<AlgebricFunction>(),
				Offset2 = source.Field<double?>(prefixColumn + "Offset2"),
				KeyMeter2 = source.Field<string>(prefixColumn + "KeyMeter2"),
				NameMeter2 = source.Field<string>(prefixColumn + "NameMeter2"),
				KeyMeterOperation2 = keyMeterOperation2.HasValue ? keyMeterOperation2.Value.ToString(CultureInfo.InvariantCulture) : null,
				LocalIdMeterOperation2 = source.Field<string>(prefixColumn + "LocalIdMeterOperation2")
			};
			return retVal;
		}

		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, MeterOperation item) {
			db.AddInParameterString(command, "Key", item.KeyMeterOperation);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyMeter", item.KeyMeter);
			db.AddInParameterInt(command, "Order", item.Order);
			db.AddInParameterDouble(command, "Factor1", item.Factor1);
			db.AddInParameterString(command, "Function1", item.Function1.ParseAsString());
			db.AddInParameterDouble(command, "Offset1", item.Offset1);
			db.AddInParameterString(command, "KeyMeter1", item.KeyMeter1);
			db.AddInParameterString(command, "KeyMeterOperation1", item.KeyMeterOperation1);
			db.AddInParameterString(command, "Operator", item.Operator.ParseAsString());
			db.AddInParameterDouble(command, "Factor2", item.Factor2);
			db.AddInParameterString(command, "Function2", item.Function2.ParseAsString());
			db.AddInParameterDouble(command, "Offset2", item.Offset2);
			db.AddInParameterString(command, "KeyMeter2", item.KeyMeter2);
			db.AddInParameterString(command, "KeyMeterOperation2", item.KeyMeterOperation2);

			string localId = !string.IsNullOrWhiteSpace(item.LocalId) ? item.LocalId : null;
			db.AddInParameterString(command, "LocalId", localId);
		}


		/// <summary>
		/// Get the list of saved MeterOperation from a Meter
		/// </summary>
		/// <param name="KeyMeter">The key of the meter.</param>
		public List<MeterOperation> GetListByKeyMeter(string KeyMeter) {
			List<MeterOperation> results;
			DbCommand command = db.GetStoredProcCommand(const_proc_getlistbymeter);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyMeter", KeyMeter);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}

		/// <summary>
		/// Gets all the MeterOperation for a specific meter.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyMeter">The Key of the meter.</param>
		/// <param name="KeyMeterOperation">The key meter operation to exclude.</param>
		/// <param name="order">The order if the current meter operation in order to filter available results. 0 to disable filtering.</param>
		/// <param name="total">The total number of items.</param>
		/// <returns>
		/// The list of items.
		/// </returns>
		public List<MeterOperation> GetAllPagingByKeyMeter(PagingParameter paging, string KeyMeter, string KeyMeterOperation, int order, out int total) {
			DbCommand command = db.GetStoredProcCommand(const_proc_getallpagingbykeymeter);
			command.CommandTimeout = 600;
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyMeter", KeyMeter);
			db.AddInParameterString(command, "KeyMeterOperation", KeyMeterOperation);
			db.AddInParameterInt(command, "Order", order);
			db.AddInParameterInt(command, "start", paging.start);
			db.AddInParameterInt(command, "limit", paging.limit);
			DataSet ds = db.ExecuteDataSet(command);
			if (ds.Tables[0].Rows.Count > 0)
				total = ds.Tables[0].Rows[0].Field<int>("TotalRecord");
			else
				total = 0;
			return FillObjects(ds);
		}


		/// <summary>
		/// Saves the meter MeterOperation.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="meterOperations">The meter operations.</param>
		/// <returns></returns>
		public List<MeterOperation> SaveMeterMeterOperations(Meter entity, CrudStore<MeterOperation> meterOperations) {
			DbCommand command = db.GetStoredProcCommand(const_proc_metermeteroperationssave);
			command.CommandTimeout = 600;
			List<MeterOperation> results;

			if (meterOperations == null || meterOperations.IsEmpty())
				return null;
			string deletes = null;

			if (meterOperations.destroy != null) {
				deletes = Helper.BuildXml(meterOperations.destroy.Select(p => p.KeyMeterOperation.ToString()).ToArray());
			}

			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyMeter", entity.KeyMeter);
			db.AddInParameterString(command, "Inserts", null);
			db.AddInParameterString(command, "Deletes", deletes);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}

	}
}
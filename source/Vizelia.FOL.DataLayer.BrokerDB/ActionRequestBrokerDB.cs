﻿using System;
using System.Collections.Generic;
using Vizelia.FOL.BusinessEntities;
using System.Data;
using System.Data.Common;
using System.Web.Security;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.DataLayer.BrokerDB {

	/// <summary>
	/// Database broker for ActionRequest business entity.
	/// </summary>
	public class ActionRequestBrokerDB : BaseBrokerDB<ActionRequest> {


		/// <summary>
		/// The name of the stored procedure that retreives the item by its instanceId.
		/// </summary>
		protected const string const_proc_getitembyinstanceid = AppModule.ServiceDesk + "_ActionRequestGetItemByInstanceId";


		/// <summary>
		/// Gets a value indicating whether this instance is localized.
		/// </summary>
		/// <remarks></remarks>
		protected override bool IsLocalized {
			get {
				return true;
			}
		}

		/// <summary>
		/// Gets the const_proc_prefix.
		/// </summary>
		/// <value>The const_proc_prefix.</value>
		protected override string const_proc_prefix {
			get {
				return AppModule.ServiceDesk;
			}
		}



		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, ActionRequest item) {
			db.AddInParameterString(command, "Key", item.KeyActionRequest);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterInt(command, "PriorityID", item.PriorityID);
			db.AddInParameterString(command, "Description", item.Description);
			db.AddInParameterString(command, "KeyLocation", item.KeyLocation);
			db.AddInParameterString(command, "HasBeenCreatedByUser", "");
			db.AddInParameterString(command, "Requestor", item.Requestor);
			db.AddInParameterString(command, "ClassificationKeyParent", item.ClassificationKeyParent);
			db.AddInParameterString(command, "ClassificationKeyChildren", item.ClassificationKeyChildren);
			db.AddInParameterDateTime(command, "ActualStart", item.ActualStart);
			db.AddInParameterDateTime(command, "ScheduleStart", item.ScheduleStart);
			db.AddInParameterDateTime(command, "CloseDateTime", item.CloseDateTime);
			db.AddInParameterString(command, "ApprovalDescription", "");
			db.AddInParameterString(command, "ApprovalLevel", "");
			db.AddInParameterString(command, "KeyApprover", item.KeyApprover);

			string localId = !string.IsNullOrWhiteSpace(item.LocalId) ? item.LocalId : null;
			db.AddInParameterString(command, "LocalId", localId);
		}

		/// <summary>
		/// Populates a ActionRequest business entity from a DataSourceDecorator.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated ActionRequest business entity.</returns>
		protected override ActionRequest FillObject(DataSourceDecorator source, string prefixColumn) {
			return new ActionRequest {
				KeyActionRequest = source.Field<string>(prefixColumn + "KeyActionRequest"),
				RequestID = source.Field<string>(prefixColumn + "RequestID"),
				LocalId = source.Field<string>(prefixColumn + "LocalId"),
				ClassificationKeyParent = source.Field<string>(prefixColumn + "ClassificationKeyParent"),
				ClassificationKeyChildren = source.Field<string>(prefixColumn + "ClassificationKeyChildren"),
				ActualStart = source.Field<DateTime?>(prefixColumn + "ActualStart"),
				ScheduleStart = source.Field<DateTime?>(prefixColumn + "ScheduleStart"),
				CloseDateTime = source.Field<DateTime?>(prefixColumn + "CloseDateTime"),
				PriorityID = source.Field<int>(prefixColumn + "PriorityID"),
				Description = source.Field<string>(prefixColumn + "Description"),
				Requestor = source.Field<string>(prefixColumn + "Requestor"),
				KeyLocation = source.Field<string>(prefixColumn + "KeyLocation"),
				PriorityValue = source.Field<int>(prefixColumn + "PriorityValue"),
				PriorityMsgCode = source.Field<string>(prefixColumn + "PriorityMsgCode"),
				RequestorFirstName = source.Field<string>(prefixColumn + "RequestorFirstName"),
				RequestorLastName = source.Field<string>(prefixColumn + "RequestorLastName"),
				RequestorMail = source.Field<string>(prefixColumn + "RequestorMail"),
				KeyApprover = source.Field<string>(prefixColumn + "KeyApprover"),
				ApproverFirstName = source.Field<string>(prefixColumn + "ApproverFirstName"),
				ApproverLastName = source.Field<string>(prefixColumn + "ApproverLastName"),
				ApproverMail = source.Field<string>(prefixColumn + "ApproverMail"),
				LocationLevel = source.Field<int?>("LocationLevel"),
				LocationLongPath = source.Field<string>(prefixColumn + "LocationLongPath"),
				LocationShortPath = source.Field<string>(prefixColumn + "LocationShortPath"),
				LocationName = source.Field<string>(prefixColumn + "LocationName"),
				LocationTypeName = source.Field<string>(prefixColumn + "LocationTypeName").ParseAsEnum<HierarchySpatialTypeName>(),
				ClassificationLongPath = source.Field<string>(prefixColumn + "ClassificationLongPath")
			};
		}


		/// <summary>
		/// Gets an item by its instanceId.
		/// </summary>
		/// <param name="instanceId">The instanceId of the item.</param>
		/// <returns>The item.</returns>
		public ActionRequest GetItemByInstanceId(Guid instanceId) {
			ActionRequest item = default(ActionRequest);
			DbCommand command = db.GetStoredProcCommand(const_proc_getitembyinstanceid);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterGuid(command, "InstanceId", instanceId);
			db.AddInParameterString(command, "Culture", Helper.GetCurrentCultureShort());
			using (var decorator = new DataReaderDecorator(db.ExecuteReader(command))) {
				while (decorator.Read()) {
					item = FillObjectInternal(decorator, "");
				}
			}
			// populates the references.
			this.PopulateReference(new List<ActionRequest> { item });
			return item;
		}






	}
}

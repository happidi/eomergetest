﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web.Security;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Vizelia.FOL.DataLayer.Interfaces;

namespace Vizelia.FOL.DataLayer.BrokerDB {
	/// <summary>
	/// Database broker for MeterData business entity.
	/// </summary>
	public class MeterDataBrokerDB : BaseBrokerDB<MeterData> {
		const string const_type_meterdatacrud = "dbo.MeterDataCrud";

		#region Deprecated
		///// <summary>
		///// The name of the stored procedure that retreives a list of meter data for a specific chart.
		///// </summary>
		//protected const string const_proc_getlistbymeters = AppModule.Energy + "_MeterDataGetListByMeters";

		///// <summary>
		///// The name of the stored procedure that retreives a list of meter data for a specific meter.
		///// </summary>
		//protected const string const_proc_getallpagingbykeymeter = AppModule.Energy + "_MeterDataGetAllPagingByKeyMeter";
		#endregion
		/// <summary>
		/// The name of the stored procedure that retreives a list of meter data for a specific date range.
		/// </summary>
		protected const string const_proc_getallbydaterange = AppModule.Energy + "_MeterDataGetAllByDateRange";

		/// <summary>
		/// The name of the stored procedure that retreives a list of meter data for a specific date range.
		/// </summary>
		protected const string const_proc_getlastbykeymeter = AppModule.Energy + "_MeterDataGetLastByKeyMeter";

		/// <summary>
		/// The name of the stored procedure that imports a list of meter data
		/// </summary>
		protected const string const_proc_importbulk = AppModule.Energy + "_MeterDataImportBulk";

		/// <summary>
		/// The name of the stored procedure that deletes a list of meter data
		/// </summary>
		protected const string const_proc_deletebulk = AppModule.Energy + "_MeterDataDeleteBulk";

		/// <summary>
		/// The name of the stored procedure that deletes a list of meter data by date range
		/// </summary>
		protected const string const_proc_deletebydaterangebulk = AppModule.Energy + "_MeterDataDeleteByDateRangeBulk";


		/// <summary>
		/// Initializes a new instance of the <see cref="MeterDataBrokerDB"/> class.
		/// </summary>
		public MeterDataBrokerDB()
			: base() {
			db = Helper.CreateInstance<DataAccess, IDataAccess>("dns_vizelia_meter_data");
			const_proc_generic_updatebatch = String.Format("{0}_{1}UpdateBatch", const_proc_prefix, BusinessEntityName);
		}

		/// <summary>
		/// Gets the const_proc_prefix.
		/// </summary>
		/// <value>The const_proc_prefix.</value>
		protected override string const_proc_prefix {
			get {
				return AppModule.Energy;
			}
		}


		/// <summary>
		/// Populates a MeterData business entity from a DataSourceDecorator.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated MeterData business entity.</returns>
		protected override MeterData FillObject(DataSourceDecorator source, string prefixColumn) {
			return new MeterData {
				KeyMeterData = source.Field<long>(prefixColumn + "KeyMeterData").ToString(), 
				KeyMeter = source.Field<string>(prefixColumn + "KeyMeter"),
				//MeterLocalId = source.Field<string>(prefixColumn + "MeterLocalId"),
				AcquisitionDateTime = source.Field<DateTime>(prefixColumn + "AcquisitionDateTime"),
				Value = source.Field<double>(prefixColumn + "Value"),
				Validity = ((int)(source.Field<byte?>(prefixColumn + "Validity") ?? 0)).ParseAsEnum<MeterDataValidity>()
				//Source = source.Field<string>(prefixColumn + "Source")
			};
		}

		/// <summary>
		/// Builds the additional update batch params.
		/// </summary>
		/// <param name="command">The command.</param>
		protected override void BuildAdditionalUpdateBatchParams(DbCommand command) {
			db.AddInParameterString(command, "UserName", Helper.GetCurrentUserName());
		}

		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, MeterData item) {
			db.AddInParameterString(command, "Key", item.KeyMeterData);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyMeter", item.KeyMeter);
			db.AddInParameterDateTime(command, "AcquisitionDateTime", item.AcquisitionDateTime);
			db.AddInParameterDouble(command, "Value", item.Value);
			db.AddInParameterString(command, "Source", "");			//item.Source
			db.AddInParameterInt(command, "Validity", item.Validity.ParseAsInt());
			db.AddInParameterString(command, "UserName", Helper.GetCurrentUserName());
		}

		/// <summary>
		/// Gets the complete list of MD (optimized expression of MeterData) grouped by KeyMeter as int, filtered by date range.
		/// </summary>
		/// <param name="startDate">The start date.</param>
		/// <param name="endDate">The end date.</param>
		/// <param name="KeyMeters">The key meters.</param>
		/// <returns></returns>
		public ConcurrentDictionary<int, SortedList<DateTime, MD>> GetAllByDateRange(DateTime? startDate, DateTime? endDate, string[] KeyMeters = null) {
			//return MeterHelper.GenerateMeterData(1000000, 30000000);
			var command = db.GetStoredProcCommand(const_proc_getallbydaterange);
			command.CommandTimeout = 0;
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			if (startDate.HasValue && startDate.Value > DateTime.MinValue && startDate.Value < DateTime.MaxValue) {
				db.AddInParameterDateTime(command, "StartDate", startDate);
			}
			else {
				;
			}
			if (endDate.HasValue && endDate.Value > DateTime.MinValue && endDate.Value < DateTime.MaxValue) {
				db.AddInParameterDateTime(command, "EndDate", endDate);
			}
			else {
				;
			}
			string metersXML = null;

			if (KeyMeters != null) {
				metersXML = Helper.BuildXml(KeyMeters);
			}
			db.AddInParameterString(command, "KeyMeters", metersXML);
			
			var unsortedDictionary = new Dictionary<int, Dictionary<DateTime, MD>>();

			using (IDataReader reader = db.ExecuteReader(command)) {
				while (reader.Read()) {
					var keyMeter = MeterHelper.ConvertMeterKeyToInt(reader["KeyMeter"].ToString());
					if (!unsortedDictionary.ContainsKey(keyMeter)) {
						unsortedDictionary.Add(keyMeter, new Dictionary<DateTime, MD>());
					}
					MD item = new MD {
						AcquisitionDateTime = DateTime.SpecifyKind((DateTime)reader["AcquisitionDateTime"], DateTimeKind.Utc),
						Value = (double)reader["Value"],
						KeyMeter = keyMeter,
						KeyMeterData = (long)reader["KeyMeterData"], 
						Validity = 0
					};

					if (reader["Validity"] != System.DBNull.Value) {
						item.Validity = (byte)reader["Validity"];
					}

					if (!unsortedDictionary[keyMeter].ContainsKey(item.AcquisitionDateTime)) {
						unsortedDictionary[keyMeter].Add(item.AcquisitionDateTime, item);
					}
				}
			}


			var retVal = new ConcurrentDictionary<int, SortedList<DateTime, MD>>(unsortedDictionary.AsParallel()
										   .Select(item => new KeyValuePair<int, SortedList<DateTime, MD>>(item.Key, new SortedList<DateTime, MD>(item.Value))));
			//.ToDictionary(v => v.Key, v => v.Value);
			return retVal;
		}


		/// <summary>
		/// Gets the last meter data by key meter.
		/// </summary>
		/// <param name="KeyMeter">The key meter.</param>
		/// <returns></returns>
		public MeterData GetLastByKeyMeter(string KeyMeter) {
			MeterData meterdata = default(MeterData);
			DbCommand command = db.GetStoredProcCommand(const_proc_getlastbykeymeter);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyMeter", KeyMeter);
			using (IDataReader reader = db.ExecuteReader(command)) {
				if (reader.Read()) {
					meterdata = FillObject(reader);
				}
			}
			return meterdata;
		}

		/// <summary>
		/// Bulk imports Meter Data.
		/// </summary>
		/// <param name="meterDatas">The meter datas.</param>
		/// <returns>
		/// A list of items MeterData objects which their KeyMeterData is an indication of insertion successs.
		/// If the KeyMeterData is equal to -1, then insertion has failed.
		/// </returns>
		public IEnumerable<MeterDataBulkOperationResult> ImportBulk(List<MeterData> meterDatas) {
			var retVal = PerformBulkCommand(meterDatas, const_proc_importbulk);
			return retVal;
		}

		/// <summary>
		/// Bulk deletes Meter Data.
		/// </summary>
		/// <param name="meterDatas">The meter datas.</param>
		public IEnumerable<MeterDataBulkOperationResult> DeleteBulk(List<MeterData> meterDatas) {
			var retVal = PerformBulkCommand(meterDatas, const_proc_deletebulk);
			return retVal;
		}

		/// <summary>
		/// Deletes the meter data by date range as a bulk operation.
		/// </summary>
		/// <param name="keyMeter">The key meter.</param>
		/// <param name="startDate">The start date.</param>
		/// <param name="endDate">The end date.</param>
		/// <param name="pagingLimit">The paging limit.</param>
		/// <returns>
		/// The list of deleted meter data items.
		/// </returns>
		public List<MeterData> DeleteByDateRangeBulk(string keyMeter, DateTime startDate, DateTime endDate, int pagingLimit) {
			DbCommand command = db.GetStoredProcCommand(const_proc_deletebydaterangebulk);
			db.AddInParameterString(command, "Key", keyMeter);
			db.AddInParameterDateTime(command, "StartDate", startDate);
			db.AddInParameterDateTime(command, "EndDate", endDate);
			db.AddInParameterInt(command, "PagingLimit", pagingLimit);
			db.AddInParameterString(command, "Application", Membership.ApplicationName);
			db.AddInParameterString(command, "UserName", Helper.GetCurrentUserName());
			command.CommandTimeout = 200;

			using (IDataReader bulkOutputReader = db.ExecuteReader(command)) {
				List<MeterData> deletedItems = FillObjects(bulkOutputReader);
				return deletedItems;
			}
		}

		/// <summary>
		/// Builds the additional delete params.
		/// </summary>
		/// <param name="command">The command.</param>
		protected override void BuildAdditionalDeleteParams(DbCommand command) {
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "UserName", Helper.GetCurrentUserName());
		}

		/// <summary>
		/// Performs the bulk command and returns MeterData entities as a result.
		/// </summary>
		/// <param name="meterDatas">The meter datas.</param>
		/// <param name="storedProcedureName">Name of the stored procedure.</param>
		private IEnumerable<MeterDataBulkOperationResult> PerformBulkCommand(IEnumerable<MeterData> meterDatas, string storedProcedureName) {
			List<MeterData> meterDatasWithValueNaN;
			DataTable mdTable = ConvertToTable(meterDatas, out meterDatasWithValueNaN);

			DbCommand command = db.GetStoredProcCommand(storedProcedureName);
			db.AddInParameterTable(command, "Items", mdTable, const_type_meterdatacrud);
			db.AddInParameterString(command, "Application", Membership.ApplicationName);
			db.AddInParameterString(command, "UserName", Helper.GetCurrentUserName());
			command.CommandTimeout = 200;

			using (IDataReader bulkOutput = db.ExecuteReader(command)) {
				var results = GetBulkOperationResults(bulkOutput, meterDatasWithValueNaN);
				return results;
			}
		}

		/// <summary>
		/// Gets the import report.
		/// </summary>
		/// <param name="bulkOutputReader">The bulk output reader.</param>
		/// <param name="meterDatasWithValueNaN">The list of meter datas with value NaN.</param>
		/// <returns></returns>
		private IEnumerable<MeterDataBulkOperationResult> GetBulkOperationResults(IDataReader bulkOutputReader, List<MeterData> meterDatasWithValueNaN) {
			var report = new List<MeterDataBulkOperationResult>();
			bool isUpdateFieldExists = bulkOutputReader.FieldExists("IsUpdate");

			using (var readerDecorator = new DataReaderDecorator(bulkOutputReader)) {
				while (bulkOutputReader.Read()) {
					MeterData meterData = FillObject(readerDecorator, string.Empty);

					bool success = bulkOutputReader.GetValue<bool>("Success");
					var resultCode = success
										? MeterDataBulkOperationResultCode.Success
										: MeterDataBulkOperationResultCode.ResolveMeterLocalIdFailed;

					string meterLocalId = bulkOutputReader.GetValue<string>("MeterLocalId");
					string crudAction;

					if (isUpdateFieldExists) {
						bool isUpdate = bulkOutputReader.GetValue<bool>("IsUpdate");
						crudAction = isUpdate ? CrudAction.Update : CrudAction.Create;
					}
					else {
						crudAction = CrudAction.Destroy;
					}

					var result = new MeterDataBulkOperationResult(resultCode, crudAction, meterLocalId, meterData);
					report.Add(result);
				}
			}

			report.AddRange(
				meterDatasWithValueNaN.Select(
					meterData => new MeterDataBulkOperationResult(MeterDataBulkOperationResultCode.ValueNaN, CrudAction.Create, meterData.KeyMeter, meterData)));

			return report;
		}

		/// <summary>
		/// Converts the Meter Data items to a data table.
		/// </summary>
		/// <param name="meterDatas">The meter datas.</param>
		/// <param name="NaNMeterDatas">The output list of meter datas with value NaN.</param>
		/// <returns></returns>
		private static DataTable ConvertToTable(IEnumerable<MeterData> meterDatas, out List<MeterData> NaNMeterDatas) {
			var mdTable = new DataTable();
			mdTable.Columns.Add("RowNum", typeof(int)).AutoIncrement = true;
			mdTable.Columns.Add("KeyMeter", typeof(string));
			mdTable.Columns.Add("AcquisitionDateTime", typeof(DateTime));
			mdTable.Columns.Add("Value", typeof(double));
			mdTable.Columns.Add("Validity", typeof(int));

			var meterDatasWithValueNaN = new List<MeterData>();

			foreach (MeterData md in meterDatas) {
				if (double.IsNaN(md.Value)) {
					//TracingService.Write(TraceEntrySeverity.Warning, "Encountered meter data with NaN value, skipping it.");
					meterDatasWithValueNaN.Add(md);
				}
				else {
					var row = mdTable.NewRow();
					row["KeyMeter"] = md.KeyMeter;
					row["AcquisitionDateTime"] = md.AcquisitionDateTime.ToUniversalTime();
					row["Value"] = md.Value;
					row["Validity"] = md.Validity;

					mdTable.Rows.Add(row);
				}
			}

			NaNMeterDatas = meterDatasWithValueNaN;
			return mdTable;
		}
	}

	/// <summary>
	/// An enum indicating the result code of the bulk MeterData operation.
	/// </summary>
	public enum MeterDataBulkOperationResultCode {
		/// <summary>
		/// The operation succeeded.
		/// </summary>
		Success,
		/// <summary>
		/// The operation failed because the meter local id could not be resolved.
		/// </summary>
		ResolveMeterLocalIdFailed,
		/// <summary>
		/// The operation failed because the value of the meter data is NaN.
		/// </summary>
		ValueNaN
	}

	/// <summary>
	/// A class indicating the result of the bulk MeterData operation.
	/// </summary>
	public class MeterDataBulkOperationResult {
		/// <summary>
		/// Initializes a new instance of the <see cref="MeterDataBulkOperationResult"/> class.
		/// </summary>
		/// <param name="resultCode">The result code.</param>
		/// <param name="crudAction">The crud action.</param>
		/// <param name="meterLocalId">The meter local id.</param>
		/// <param name="meterData">The meter data.</param>
		public MeterDataBulkOperationResult(MeterDataBulkOperationResultCode resultCode, string crudAction, string meterLocalId, MeterData meterData) {
			ResultCode = resultCode;
			CrudAction = crudAction;
			MeterLocalId = meterLocalId;
			MeterData = meterData;
		}

		/// <summary>
		/// Gets or sets the result code.
		/// </summary>
		/// <value>
		/// The result code.
		/// </value>
		public MeterDataBulkOperationResultCode ResultCode { get; set; }

		/// <summary>
		/// Gets or sets the crud action.
		/// </summary>
		/// <value>
		/// The crud action.
		/// </value>
		public string CrudAction { get; set; }

		/// <summary>
		/// Gets or sets the meter local id.
		/// </summary>
		/// <value>
		/// The meter local id.
		/// </value>
		public string MeterLocalId { get; set; }

		/// <summary>
		/// Gets or sets the meter data.
		/// </summary>
		/// <value>
		/// The meter data.
		/// </value>
		public MeterData MeterData { get; set; }
	}
}

﻿using Vizelia.FOL.BusinessEntities;
using System.Data.Common;
using System.Web.Security;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.DataLayer.BrokerDB {
	/// <summary>
	/// Database broker for CalendarEventCategory business entity.
	/// </summary>
	public class CalendarEventCategoryBrokerDB : BaseBrokerDB<CalendarEventCategory> {
		
		/// <summary>
		/// Gets the const_proc_prefix.
		/// </summary>
		/// <value>The const_proc_prefix.</value>
		protected override string const_proc_prefix {
			get {
				return AppModule.Core;
			}
		}


		/// <summary>
		/// Populates a CalendarEventCategory business entity from a DataSourceDecorator.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated CalendarEventCategory business entity.</returns>
		protected override CalendarEventCategory FillObject(DataSourceDecorator source, string prefixColumn) {
			return new CalendarEventCategory {
				KeyCalendarEventCategory = source.Field<int>(prefixColumn + "KeyCalendarEventCategory").ToString(),
				LocalId = source.Field<string>(prefixColumn + "LocalId"),
				MsgCode = source.Field<string>(prefixColumn + "MsgCode"),
				ColorR = source.Field<int>(prefixColumn + "ColorR"),
				ColorG = source.Field<int>(prefixColumn + "ColorG"),
				ColorB = source.Field<int>(prefixColumn + "ColorB"),
				ColorId = source.Field<string>(prefixColumn + "ColorId")
			};
		}


		
		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, CalendarEventCategory item) {
			db.AddInParameterString(command, "Key", item.KeyCalendarEventCategory);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "LocalId", item.LocalId);
			db.AddInParameterString(command, "MsgCode", item.MsgCode);
			db.AddInParameterInt(command, "ColorR", item.ColorR);
			db.AddInParameterInt(command, "ColorG", item.ColorG);
			db.AddInParameterInt(command, "ColorB", item.ColorB);
			db.AddInParameterString(command, "ColorId", item.ColorId);
		}

	
	}
}

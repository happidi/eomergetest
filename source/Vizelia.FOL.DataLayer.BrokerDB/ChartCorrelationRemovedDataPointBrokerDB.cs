﻿using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Globalization;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.DataLayer.BrokerDB {

	/// <summary>
	/// Database broker for ChartCorrelationRemovedDataPoint business entity.
	/// </summary>
	public class ChartCorrelationRemovedDataPointBrokerDB : BaseBrokerDB<ChartCorrelationRemovedDataPoint> {

		/// <summary>
		/// The name of the stored procedure that retreives a list of ChartAxis for a specific chart.
		/// </summary>
		protected const string const_proc_getlistbychart = AppModule.Energy + "_ChartCorrelationRemovedDataPointGetListByKeyChart";

		/// <summary>
		/// The name of the stored procedure that retreives a list of ChartAxis for a specific chart.
		/// </summary>
		protected const string const_proc_getallpagingbykeychart = AppModule.Energy + "_ChartCorrelationRemovedDataPointGetAllPagingByKeyChart";

		/// <summary>
		/// Gets the const_proc_prefix.
		/// </summary>
		/// <value>The const_proc_prefix.</value>
		protected override string const_proc_prefix {
			get {
				return AppModule.Energy;
			}
		}

		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, ChartCorrelationRemovedDataPoint item) {
			db.AddInParameterString(command, "Key", item.KeyChartCorrelationRemovedDataPoint);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyChart", item.KeyChart);
			db.AddInParameterDouble(command, "XValue", item.XValue);
			db.AddInParameterDouble(command, "YValue", item.YValue);
			db.AddInParameterString(command, "LocalId", item.LocalId);
		}

		/// <summary>
		/// Populates a ChartCorrelationRemovedDataPoint business entity from a DataSourceDecorator.
		/// </summary>
		/// <param name="source">The data source.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated ChartCorrelationRemovedDataPoint business entity.</returns>
		protected override ChartCorrelationRemovedDataPoint FillObject(DataSourceDecorator source, string prefixColumn) {
			return new ChartCorrelationRemovedDataPoint {
				KeyChartCorrelationRemovedDataPoint = source.Field<int>(prefixColumn + "KeyChartCorrelationRemovedDataPoint").ToString(CultureInfo.InvariantCulture),
				KeyChart = source.Field<int>(prefixColumn + "KeyChart").ToString(CultureInfo.InvariantCulture),
				XValue = source.Field<double>(prefixColumn + "XValue"),
				YValue = source.Field<double>(prefixColumn + "YValue"),
				LocalId = source.Field<string>(prefixColumn + "LocalId")
			};
		}

		/// <summary>
		/// Get the list of saved ChartCorrelationRemovedDataPoint from a Chart
		/// </summary>
		/// <param name="KeyChart">The key of the chart.</param>
		public List<ChartCorrelationRemovedDataPoint> GetListByKeyChart(string KeyChart) {
			List<ChartCorrelationRemovedDataPoint> results;
			DbCommand command = db.GetStoredProcCommand(const_proc_getlistbychart);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyChart", KeyChart);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}

		/// <summary>
		/// Gets all the ChartCorrelationRemovedDataPoint for a specific chart.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyChart">The Key of the chart.</param>
		/// <param name="total">The total number of items.</param>
		/// <returns>The list of items.</returns>
		public List<ChartCorrelationRemovedDataPoint> GetAllPagingByKeyChart(PagingParameter paging, string KeyChart, out int total) {
			DbCommand command = db.GetStoredProcCommand(const_proc_getallpagingbykeychart);
			command.CommandTimeout = 600;
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyChart", KeyChart);
			db.AddInParameterInt(command, "start", paging.start);
			db.AddInParameterInt(command, "limit", paging.limit);
			DataSet ds = db.ExecuteDataSet(command);
			if (ds.Tables[0].Rows.Count > 0)
				total = ds.Tables[0].Rows[0].Field<int>("TotalRecord");
			else
				total = 0;
			return FillObjects(ds);
		}
	}
}
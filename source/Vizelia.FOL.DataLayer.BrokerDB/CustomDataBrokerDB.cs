﻿using System.Data.Common;
using System.Globalization;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.DataLayer.BrokerDB {
	/// <summary>
	/// The broker DB for business entity CustomData
	/// </summary>
	public class CustomDataBrokerDB : BaseBrokerDB<CustomData> {
		/// <summary>
		/// The name of the stored procedure that deletes all prference for a specific user.
		/// </summary>
		const string const_proc_deleteall = AppModule.Core + "_CustomDataDeleteAll";

		/// <summary>
		/// Builds the save params.
		/// </summary>
		/// <param name="command">The command.</param>
		/// <param name="item">The item.</param>
		protected override void BuildSaveParams(DbCommand command, CustomData item) {
			db.AddInParameterString(command, "Key", item.KeyCustomData);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "ContainerName", item.ContainerName);
			db.AddInParameterString(command, "KeyUser", item.KeyUser);
			db.AddInParameterString(command, "DataKey", item.DataKey);
			db.AddInParameterString(command, "DataValue", item.DataValue);
		}

		/// <summary>
		/// Fills the object.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="prefixColumn">The prefix column.</param>
		/// <returns></returns>
		protected override CustomData FillObject(DataSourceDecorator source, string prefixColumn) {
			return new CustomData {
				KeyCustomData = source.Field<int>(prefixColumn + "KeyCustomData").ToString(CultureInfo.InvariantCulture),
				ContainerName = source.Field<string>(prefixColumn + "ContainerName"),
				KeyUser = source.Field<string>(prefixColumn + "KeyUser"),
				DataKey = source.Field<string>(prefixColumn + "DataKey"),
				DataValue = source.Field<string>(prefixColumn + "DataValue")
			};
		}

		/// <summary>
		/// Deletes all data.
		/// </summary>
		/// <param name="containerName">Name of the container.</param>
		/// <param name="keyUser">The key user.</param>
		/// <returns></returns>
		public bool DeleteAll(string containerName, string keyUser) {
			DbCommand command = db.GetStoredProcCommand(const_proc_deleteall);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "ContainerName", containerName);
			db.AddInParameterString(command, "KeyUser", keyUser);
			db.ExecuteNonQuery(command);
			return true;
		}
	}
}
﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Web.Security;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Vizelia.FOL.DataLayer.Interfaces;

namespace Vizelia.FOL.DataLayer.BrokerDB {

	/// <summary>
	/// Database broker for PortalTemplate business entity.
	/// </summary>
	public class PortalTemplateBrokerDB : BaseBrokerDB<PortalTemplate> {

		/// <summary>
		/// The prefix of stored procedures of that provider. 
		/// </summary>
		protected override string const_proc_prefix {
			get {
				return AppModule.Energy;
			}
		}

        /// <summary>
        /// The name of the stored procedure that retreives the list of new users by key portal template.
        /// </summary>
        protected const string const_proc_folmembershipusergetnewusersbykeyportaltemplate = AppModule.Energy + "_FOLMembershipUserGetNewUsersByKeyPortalTemplate";

		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, PortalTemplate item) {
			db.AddInParameterString(command, "Key", item.KeyPortalTemplate);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "LocalId", item.LocalId);
			db.AddInParameterString(command, "Title", item.Title);
			db.AddInParameterString(command, "Description", item.Description);
			db.AddInParameterBool(command, "AutoUpdate", item.AutoUpdate);
			db.AddInParameterString(command, "TimeZoneId", item.TimeZoneId);
			db.AddInParameterInt(command, "NumberOfColumns", item.NumberOfColumns);
			db.AddInParameterInt(command, "StartX", item.StartX);
			db.AddInParameterInt(command, "StartY", item.StartY);
		}


		/// <summary>
		/// Populates a PortalTemplate business entity from a DataSourceDecorator.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated PortalTemplate business entity.</returns>
		protected override PortalTemplate FillObject(DataSourceDecorator source, string prefixColumn) {
			return new PortalTemplate() {
				KeyPortalTemplate = source.Field<int>(prefixColumn + "KeyPortalTemplate").ToString(),
				Title = source.Field<string>(prefixColumn + "Title"),
				LocalId = source.Field<string>(prefixColumn + "LocalId"),
				Description = source.Field<string>(prefixColumn + "Description"),
				AutoUpdate = source.Field<bool>(prefixColumn + "AutoUpdate"),
				TimeZoneId=source.Field<string>(prefixColumn+"TimeZoneId"),
				NumberOfColumns = source.Field<int?>(prefixColumn + "NumberOfColumns"),
				StartX = source.Field<int?>(prefixColumn + "StartX"),
				StartY = source.Field<int?>(prefixColumn + "StartY"),
			};
		}

        /// <summary>
        /// Gets a list of new user keys by key portal template.
        /// </summary>
        /// <param name="keyPortalTemplate">The keyPortalTemplate.</param>
        /// <returns></returns>
        public List<string> FOLMembershipUser_GetNewUserKeysByKeyPortalTemplate(int keyPortalTemplate) {
            var results = new List<string>(); ;
            DbCommand command = db.GetStoredProcCommand(const_proc_folmembershipusergetnewusersbykeyportaltemplate);
            db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
            db.AddInParameterInt(command, "KeyPortalTemplate", keyPortalTemplate);
            using (IDataReader reader = db.ExecuteReader(command))
            {
                while (reader.Read())
                {
                    results.Add(reader.GetString(0));
                }
            }
            return results;
        }
	}
}

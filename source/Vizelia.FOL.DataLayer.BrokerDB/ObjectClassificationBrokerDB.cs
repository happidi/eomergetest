﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using System.Data;
using Vizelia.FOL.DataLayer.Interfaces;
using System.Data.Common;
using System.Web.Security;

namespace Vizelia.FOL.DataLayer.BrokerDB {
	/// <summary>
	/// Database broker for ObjectClassification business entity.
	/// </summary>
	public class ObjectClassificationBrokerDB : BaseBrokerDB<ObjectClassification> {

		const string const_proc_getitembyvalue = AppModule.Core + "_ObjectClassificationGetItemByValues";
		const string const_proc_getlistbykeyobject = AppModule.Core+ "_ObjectClassificationGetListByKeyObject";
		const string const_proc_getlistbykeyclassificationitem = AppModule.Core + "_ObjectClassificationGetListByKeyClassificationItem";

		/// <summary>
		/// Populates a ObjectClassification business entity from a DataSourceDecorator.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated ObjectClassification business entity.</returns>
		protected override ObjectClassification FillObject(DataSourceDecorator source, string prefixColumn) {
			return new ObjectClassification {
				KeyClassificationReference = source.Field<string>(prefixColumn + "_KeyClassificationReference"),
				KeyObject = source.Field<string>(prefixColumn + "KeyObject"),
				KeyClassificationItem = source.Field<string>(prefixColumn + "KeyClassificationItem"),
				Location = source.Field<string>(prefixColumn + "Location"),
				TypeName = source.Field<string>(prefixColumn + "TypeName")
			};
		}


	

		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams( DbCommand command, ObjectClassification item) {
			db.AddInParameterString(command, "Key", item.KeyClassificationReference);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyObject", item.KeyObject);
			db.AddInParameterString(command, "KeyClassificationItem", item.KeyClassificationItem);
		}

		/// <summary>
		/// Gets an ObjectClassification from its values.
		/// </summary>
		/// <param name="item">The item containing the values.</param>
		/// <returns>A ObjectClassification if values were found in database, else null.</returns>
		public ObjectClassification GetItemByValues(ObjectClassification item) {
			ObjectClassification result = null;
			DbCommand command = db.GetStoredProcCommand(const_proc_getitembyvalue);
			db.AddInParameterString(command, "KeyObject", item.KeyObject);
			db.AddInParameterString(command, "KeyClassificationItem", item.KeyClassificationItem);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			using (IDataReader reader = db.ExecuteReader(command)) {
				if (reader.Read()) {
					result = FillObject(reader);
				}
			}
			return result;
		}


		/// <summary>
		/// Gets a list of ObjectClassification from the KeyObject and optionally the Location of the Classification.
		/// This is usefull for deleting all assignments to an object without having to get the list before deleting.
		/// </summary>
		/// <param name="item">The item containing the values.</param>
		/// <returns>A ObjectClassification if values were found in database, else null.</returns>
		public List<ObjectClassification> GetListByKeyObject(ObjectClassification item) {
			List<ObjectClassification> result = null;
			DbCommand command = db.GetStoredProcCommand(const_proc_getlistbykeyobject);
			db.AddInParameterString(command, "KeyObject", item.KeyObject);
			db.AddInParameterString(command, "Location", item.Location);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			using (IDataReader reader = db.ExecuteReader(command)) {
				result = FillObjects(reader);
			}
			return result;
		}


		/// <summary>
		/// Gets the list of assigned object by key classification item.
		/// </summary>
		/// <param name="KeyClassificationItem">The key classification item.</param>
		/// <returns></returns>
		public List<ObjectClassification> GetListByKeyClassificationItem(string KeyClassificationItem) {
			List<ObjectClassification> result = null;
			DbCommand command = db.GetStoredProcCommand(const_proc_getlistbykeyclassificationitem);
			db.AddInParameterString(command, "KeyClassificationItem", KeyClassificationItem);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			using (IDataReader reader = db.ExecuteReader(command)) {
				result = FillObjects(reader);
			}
			return result;
		}

		
	}
}

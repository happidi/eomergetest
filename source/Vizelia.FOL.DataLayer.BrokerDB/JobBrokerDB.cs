﻿using System;
using System.Collections.Generic;
using System.Linq;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.DataLayer.BrokerDB {
	/// <summary>
	/// Database broker for Building business entity.
	/// </summary>
	public class JobBrokerDB : BaseBrokerDBServiceProvider<Job> {

		/// <summary>
		/// Gets the item.
		/// </summary>
		/// <param name="Key">The key.</param>
		/// <returns></returns>
		public override Job GetItem(string Key) {
			return SchedulerService.GetJob(Key);
		}

		/// <summary>
		/// Gets the list.
		/// </summary>
		/// <param name="Keys">The keys.</param>
		/// <returns></returns>
		public override List<Job> GetList(string[] Keys) {
			throw new System.NotImplementedException();
		}

		/// <summary>
		/// Gets all no paging.
		/// </summary>
		/// <returns></returns>
		protected override List<Job> GetAllNoPaging() {
			List<Job> jobs = SchedulerService.GetJobs();
			PopulateJobsSeverity(jobs);
			return jobs;
		}

		private void PopulateJobsSeverity(List<Job> jobs) {
			List<string> callerKeys = jobs.SelectMany(j => j.Steps.Select(s => s.KeyStep)).ToList();

			List<Tuple<string, TraceEntrySeverity>> severities = GetJobsLastExecutionSeverity(callerKeys);

			foreach (Job job in jobs) {
				var matchingTuple = severities.FirstOrDefault(sev => job.Steps.Select(step => step.KeyStep).Contains(sev.Item1));
				if (matchingTuple != null) {
					//job.LastRunSeverity = matchingTuple.Item2;
				}
			}
		}

		private List<Tuple<string, TraceEntrySeverity>> GetJobsLastExecutionSeverity(List<string> callerKeys) {
			return new List<Tuple<string, TraceEntrySeverity>>();
		}

		/// <summary>
		/// Gets all generic paging.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="total">The total.</param>
		/// <returns></returns>
		protected override List<Job> GetAllGenericPaging(PagingParameter paging, out int total) {
			throw new System.NotImplementedException();
		}

		/// <summary>
		/// Creates the specified item.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <returns></returns>
		public override Job Create(Job item) {
			return SchedulerService.CreateJob(item);
		}

		/// <summary>
		/// Updates the specified item.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <returns></returns>
		public override Job Update(Job item) {
			return SchedulerService.UpdateJob(item);
		}

		/// <summary>
		/// Deletes the specified item.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <returns></returns>
		public override bool Delete(Job item) {
			return SchedulerService.DeleteJob(item);
		}

		/// <summary>
		/// Gets the item by local id.
		/// </summary>
		/// <param name="localId">The local id.</param>
		/// <returns></returns>
		public override Job GetItemByLocalId(string localId) {
			throw new System.NotImplementedException();
		}

		/// <summary>
		/// Updates the batch.
		/// </summary>
		/// <param name="keys">The keys.</param>
		/// <param name="item">The item.</param>
		/// <returns></returns>
		public override int UpdateBatch(string[] keys, Job item) {
			throw new System.NotImplementedException();
		}

		/// <summary>
		/// Workflows the delete instance.
		/// </summary>
		/// <param name="instanceId">The instance id.</param>
		/// <returns></returns>
		public override bool WorkflowDeleteInstance(System.Guid instanceId) {
			throw new System.NotImplementedException();
		}

		/// <summary>
		/// Sends the audit action.
		/// </summary>
		/// <param name="parametersDictionary">The parameters dictionary.</param>
		public override void SaveAuditAction(Dictionary<string, object> parametersDictionary) {
			throw new NotImplementedException();
		}

		/// <summary>
		/// Deletes the specified key.
		/// </summary>
		/// <param name="Key">The key.</param>
		/// <returns></returns>
		public override bool Delete(string Key) {
			return SchedulerService.DeleteJob(new Job { KeyJob = Key });
		}
	}
}
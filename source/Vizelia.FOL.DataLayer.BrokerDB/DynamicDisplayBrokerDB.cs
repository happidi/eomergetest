﻿using System.Data.Common;
using System.Globalization;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;


namespace Vizelia.FOL.DataLayer.BrokerDB {
	/// <summary>
	/// Database broker for DynamicDisplay business entity.
	/// </summary>
	public class DynamicDisplayBrokerDB : BaseBrokerDB<DynamicDisplay> {

		internal const string const_zwsp = "\u200B"; // zero-width 

		/// <summary>
		/// Gets the const_proc_prefix.
		/// </summary>
		/// <value>The const_proc_prefix.</value>
		protected override string const_proc_prefix {
			get {
				return AppModule.Energy;
			}
		}

		/// <summary>
		/// Populates a DynamicDisplay business entity from a DataSourceDecorator.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated DynamicDisplay business entity.</returns>
		protected override DynamicDisplay FillObject(DataSourceDecorator source, string prefixColumn) {
			return new DynamicDisplay {
				Application = ContextHelper.ApplicationName,
				KeyDynamicDisplay = source.Field<int>(prefixColumn + "KeyDynamicDisplay").ToString(CultureInfo.InvariantCulture),
				LocalId = source.Field<string>(prefixColumn + "LocalId"),
				Name = source.Field<string>(prefixColumn + "Name"),
				Description = source.Field<string>(prefixColumn + "Description"),
				Type = source.Field<string>(prefixColumn + "Type").ParseAsEnum<DynamicDisplayType>(),
				ColorTemplate = source.Field<string>(prefixColumn + "ColorTemplate"),
				KeyCustomColorTemplate = source.Field<string>(prefixColumn + "KeyCustomColorTemplate"),
				IsCustomColorTemplate = source.Field<bool?>(prefixColumn + "IsCustomColorTemplate") ?? false,
				RoundedCorners = source.Field<bool>(prefixColumn + "RoundedCorners"),

				HeaderVisible = source.Field<bool>(prefixColumn + "HeaderVisible"),
				HeaderPicture = source.Field<string>(prefixColumn + "HeaderPicture"),
				HeaderText = RemoveString(source.Field<string>(prefixColumn + "HeaderText"),const_zwsp),
				HeaderEmptyText = RemoveString(source.Field<string>(prefixColumn + "HeaderEmptyText"),const_zwsp),
				HeaderTextDefaultFontFamily = source.Field<string>(prefixColumn + "HeaderTextDefaultFontFamily"),
				HeaderTextDefaultFontColor = source.Field<string>(prefixColumn + "HeaderTextDefaultFontColor"),
				HeaderTextDefaultFontSize = source.Field<string>(prefixColumn + "HeaderTextDefaultFontSize"),

				MainChartVisible = source.Field<bool>(prefixColumn + "MainChartVisible"),
				MainPicture = source.Field<string>(prefixColumn + "MainPicture"),
				MainPictureSize = source.Field<int>(prefixColumn + "MainPictureSize"),
				MainText = RemoveString(source.Field<string>(prefixColumn + "MainText"),const_zwsp),
				MainEmptyText = RemoveString(source.Field<string>(prefixColumn + "MainEmptyText"),const_zwsp),
				MainTextDefaultFontFamily = source.Field<string>(prefixColumn + "MainTextDefaultFontFamily"),
				MainTextDefaultFontColor = source.Field<string>(prefixColumn + "MainTextDefaultFontColor"),
				MainTextDefaultFontSize = source.Field<string>(prefixColumn + "MainTextDefaultFontSize"),

				FooterVisible = source.Field<bool>(prefixColumn + "FooterVisible"),
				FooterPicture = source.Field<string>(prefixColumn + "FooterPicture"),
				FooterText = RemoveString(source.Field<string>(prefixColumn + "FooterText"), const_zwsp),
				FooterEmptyText = RemoveString(source.Field<string>(prefixColumn + "FooterEmptyText"),const_zwsp),
				FooterTextDefaultFontFamily = source.Field<string>(prefixColumn + "FooterTextDefaultFontFamily"),
				FooterTextDefaultFontColor = source.Field<string>(prefixColumn + "FooterTextDefaultFontColor"),
				FooterTextDefaultFontSize = source.Field<string>(prefixColumn + "FooterTextDefaultFontSize"),

				BogusChartType = source.Field<string>(prefixColumn + "BogusChartType").ParseAsEnum<ChartType>()
			};
		}



		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, DynamicDisplay item) {
			db.AddInParameterString(command, "Key", item.KeyDynamicDisplay);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "LocalId", item.LocalId);
			db.AddInParameterString(command, "Name", item.Name);
			db.AddInParameterString(command, "Description", item.Description);
			db.AddInParameterString(command, "Type", item.Type.ParseAsString());
			db.AddInParameterString(command, "ColorTemplate", item.ColorTemplate);
			db.AddInParameterString(command, "KeyCustomColorTemplate", item.KeyCustomColorTemplate);
			db.AddInParameterBool(command, "IsCustomColorTemplate", item.IsCustomColorTemplate);
			db.AddInParameterBool(command, "RoundedCorners", item.RoundedCorners);

			db.AddInParameterBool(command, "HeaderVisible", item.HeaderVisible);
			db.AddInParameterString(command, "HeaderPicture", item.HeaderPicture);
			db.AddInParameterString(command, "HeaderText", RemoveString(item.HeaderText,const_zwsp));
			db.AddInParameterString(command, "HeaderEmptyText", RemoveString(item.HeaderEmptyText,const_zwsp));
			db.AddInParameterString(command, "HeaderTextDefaultFontFamily", item.HeaderTextDefaultFontFamily);
			db.AddInParameterString(command, "HeaderTextDefaultFontSize", item.HeaderTextDefaultFontSize);
			db.AddInParameterString(command, "HeaderTextDefaultFontColor", item.HeaderTextDefaultFontColor);

			db.AddInParameterBool(command, "MainChartVisible", item.MainChartVisible);
			db.AddInParameterString(command, "MainPicture", item.MainPicture);
			db.AddInParameterInt(command, "MainPictureSize", item.MainPictureSize);
			db.AddInParameterString(command, "MainText", RemoveString(item.MainText,const_zwsp));
			db.AddInParameterString(command, "MainEmptyText", RemoveString(item.MainEmptyText,const_zwsp));
			db.AddInParameterString(command, "MainTextDefaultFontFamily", item.MainTextDefaultFontFamily);
			db.AddInParameterString(command, "MainTextDefaultFontSize", item.MainTextDefaultFontSize);
			db.AddInParameterString(command, "MainTextDefaultFontColor", item.MainTextDefaultFontColor);

			db.AddInParameterBool(command, "FooterVisible", item.FooterVisible);
			db.AddInParameterString(command, "FooterPicture", item.FooterPicture);
			db.AddInParameterString(command, "FooterText", RemoveString(item.FooterText,const_zwsp));
			db.AddInParameterString(command, "FooterEmptyText", RemoveString(item.FooterEmptyText,const_zwsp));
			db.AddInParameterString(command, "FooterTextDefaultFontFamily", item.FooterTextDefaultFontFamily);
			db.AddInParameterString(command, "FooterTextDefaultFontSize", item.FooterTextDefaultFontSize);
			db.AddInParameterString(command, "FooterTextDefaultFontColor", item.FooterTextDefaultFontColor);

			db.AddInParameterString(command, "BogusChartType", item.BogusChartType.ParseAsString());
		}

		private static string RemoveString(string value, string stringToRemove) {
			return value != null ? value.Replace(stringToRemove, string.Empty) : null;
		}
	}
}
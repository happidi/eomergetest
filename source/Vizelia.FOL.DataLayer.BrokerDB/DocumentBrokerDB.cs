﻿using System.Data.Common;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.DataLayer.BrokerDB {

	/// <summary>
	/// Database broker for Document business entity.
	/// </summary>
	public class DocumentBrokerDB : BaseBrokerDB<Document> {

		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, Document item) {
			db.AddInParameterString(command, "Key", item.KeyDocument);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterInt(command, "DocumentContentsLength", item.DocumentContentsLength);
			db.AddInParameterString(command, "DocumentName", item.DocumentName);
			db.AddInParameterString(command, "DocumentType", item.DocumentType);
			db.AddInParameterByteArray(command, "DocumentContents", item.DocumentContents);
		}

		/// <summary>
		/// Populates a Document business entity from a IDataReader.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated Document business entity.</returns>
		protected override Document FillObject(DataSourceDecorator source, string prefixColumn) {
			var document = new Document {
				KeyDocument = source.Field<int>(prefixColumn + "KeyDocument").ToString(),
				DocumentContentsLength = source.Field<int>(prefixColumn + "DocumentContentsLength"),
				DocumentName = source.Field<string>(prefixColumn + "DocumentName"),
				DocumentType = source.Field<string>(prefixColumn + "DocumentType"),
				DocumentContents =  source.Field<byte[]>(prefixColumn + "DocumentContents")
			};
			
			return document;
		}

	}
}
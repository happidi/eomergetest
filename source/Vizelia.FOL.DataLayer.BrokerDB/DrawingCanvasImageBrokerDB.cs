﻿using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.DataLayer.BrokerDB {

	/// <summary>
	/// Database broker for DrawingCanvasImage business entity.
	/// </summary>
	public class DrawingCanvasImageBrokerDB : BaseBrokerDB<DrawingCanvasImage> {


		/// <summary>
		/// The name of the stored procedure that retreives a list of DrawingCanvasImage for a specific DrawingCanvas.
		/// </summary>
		protected const string const_proc_getlistbykeydrawingcanvas = AppModule.Energy + "_DrawingCanvasImageGetListByKeyDrawingCanvas";

		/// <summary>
		/// The name of the stored procedure that retreives a list of DrawingCanvasImage for a specific DrawingCanvas.
		/// </summary>
		protected const string const_proc_getallpagingbykeydrawingcanvas = AppModule.Energy + "_DrawingCanvasImageGetAllPagingByKeyDrawingCanvas";

		/// <summary>
		/// The name of the stored procedure that saves a list of meters for a specific Portal.
		/// </summary>
		protected const string const_proc_drawingcanvassave = AppModule.Energy + "_DrawingCanvasDrawingCanvasImageSave";

		/// <summary>
		/// The prefix of stored procedures of that provider. 
		/// </summary>
		protected override string const_proc_prefix {
			get {
				return AppModule.Energy;
			}
		}


		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, DrawingCanvasImage item) {
			db.AddInParameterString(command, "Key", item.KeyDrawingCanvasImage);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "LocalId", item.LocalId);
			db.AddInParameterString(command, "KeyDrawingCanvas", item.KeyDrawingCanvas);
			db.AddInParameterString(command, "KeyImage", item.KeyImage);
			db.AddInParameterString(command, "Title", item.Title);
			db.AddInParameterInt(command, "Width", item.Width);
			db.AddInParameterInt(command, "Height", item.Height);
			db.AddInParameterInt(command, "X", item.X);
			db.AddInParameterInt(command, "Y", item.Y);
			db.AddInParameterInt(command, "ZIndex", item.ZIndex);
		}


		/// <summary>
		/// Populates a DrawingCanvasImage business entity from a DataSourceDecorator.
		/// </summary>
		/// <param name="source">The data source.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated DrawingCanvasImage business entity.</returns>
		protected override DrawingCanvasImage FillObject(DataSourceDecorator source, string prefixColumn) {
			return new DrawingCanvasImage {
				Application = ContextHelper.ApplicationName,
				KeyDrawingCanvasImage = source.Field<int>(prefixColumn + "KeyDrawingCanvasImage").ToString(CultureInfo.InvariantCulture),
				LocalId = source.Field<string>(prefixColumn + "LocalId"),
				KeyDrawingCanvas = source.Field<int>(prefixColumn + "KeyDrawingCanvas").ToString(CultureInfo.InvariantCulture),
				KeyImage = source.Field<int>(prefixColumn + "KeyImage").ToString(CultureInfo.InvariantCulture),
				Title = source.Field<string>(prefixColumn + "Title"),
				Width = source.Field<int>(prefixColumn + "Width"),
				Height = source.Field<int>(prefixColumn + "Height"),
				X = source.Field<int>(prefixColumn + "X"),
				Y = source.Field<int>(prefixColumn + "Y"),
				ZIndex = source.Field<int>(prefixColumn + "ZIndex"),
				ImageTitle = source.Field<string>(prefixColumn + "ImageTitle")
			};
		}

		/// <summary>
		/// Get the list of saved DrawingCanvas Image from a KeyDrawingCanvas.
		/// </summary>
		/// <param name="KeyDrawingCanvas">The key of the  DrawingCanvas.</param>
		public List<DrawingCanvasImage> GetListByKeyDrawingCanvas(string KeyDrawingCanvas) {
			List<DrawingCanvasImage> results;
			DbCommand command = db.GetStoredProcCommand(const_proc_getlistbykeydrawingcanvas);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyDrawingCanvas", KeyDrawingCanvas);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}

		/// <summary>
		/// Gets a store of DrawingCanvas Image for a specific DrawingCanvas.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyDrawingCanvas">The Key of the DrawingCanvas.</param>
		/// <param name="total">The total.</param>
		public List<DrawingCanvasImage> GetAllPagingByKeyDrawingCanvas(PagingParameter paging, string KeyDrawingCanvas, out int total) {
			DbCommand command = db.GetStoredProcCommand(const_proc_getallpagingbykeydrawingcanvas);
			command.CommandTimeout = 600;
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyDrawingCanvas", KeyDrawingCanvas);
			db.AddInParameterInt(command, "start", paging.start);
			db.AddInParameterInt(command, "limit", paging.limit);
			DataSet ds = db.ExecuteDataSet(command);
			if (ds.Tables[0].Rows.Count > 0)
				total = ds.Tables[0].Rows[0].Field<int>("TotalRecord");
			else
				total = 0;
			return FillObjects(ds);
		}

		/// <summary>
		/// Saves the DrawingCanvas Images.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="images">The DrawingCanvasImage store.</param>
		/// <returns></returns>
		public List<DrawingCanvasImage> SaveDrawingCanvasDrawingCanvasImage(DrawingCanvas entity, CrudStore<DrawingCanvasImage> images) {
			DbCommand command = db.GetStoredProcCommand(const_proc_drawingcanvassave);
			command.CommandTimeout = 600;
			List<DrawingCanvasImage> results;

			if (images == null || images.IsEmpty())
				return null;
			string inserts = null;
			string deletes = null;


			if (images.destroy != null) {
				deletes = Helper.BuildXml(images.destroy.Select(p => p.KeyDrawingCanvasImage).ToArray());
			}

			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyDrawingCanvas", entity.KeyDrawingCanvas);
			db.AddInParameterString(command, "Inserts", inserts);
			db.AddInParameterString(command, "Deletes", deletes);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}
	}
}
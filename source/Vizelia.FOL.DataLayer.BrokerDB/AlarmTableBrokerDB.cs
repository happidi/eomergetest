﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Web.Security;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Vizelia.FOL.DataLayer.Interfaces;

namespace Vizelia.FOL.DataLayer.BrokerDB {

	/// <summary>
	/// Database broker for AlarmTable business entity.
	/// </summary>
	public class AlarmTableBrokerDB : BaseBrokerDB<AlarmTable> {

		/// <summary>
		/// The prefix of stored procedures of that provider. 
		/// </summary>
		protected override string const_proc_prefix {
			get { return AppModule.Energy; }
		}


		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, AlarmTable item) {
			db.AddInParameterString(command, "Key", item.KeyAlarmTable);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "LocalId", item.LocalId);
			db.AddInParameterString(command, "Title", item.Title);
			db.AddInParameterBool(command, "OpenOnStartup", item.OpenOnStartup);
			db.AddInParameterBool(command, "IsFavorite", item.IsFavorite);
			db.AddInParameterString(command, "AlarmInstanceStatus", item.AlarmInstanceStatus.ParseAsString());
		}


		/// <summary>
		/// Populates a AlarmTable business entity from a DataSourceDecorator.
		/// </summary>
		/// <param name="source">The data source.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated AlarmTable business entity.</returns>
		protected override AlarmTable FillObject(DataSourceDecorator source, string prefixColumn) {
			var retVal=new AlarmTable {
				KeyAlarmTable = source.Field<int>(prefixColumn + "KeyAlarmTable").ToString(),
				LocalId = source.Field<string>(prefixColumn + "LocalId"),
				Title = source.Field<string>(prefixColumn + "Title"),
				OpenOnStartup = source.Field<bool?>(prefixColumn + "OpenOnStartup") ?? false,
				IsFavorite = source.Field<bool?>(prefixColumn + "IsFavorite") ?? false,
				AlarmInstanceStatus=AlarmInstanceStatus.Open
			};

			if (string.IsNullOrEmpty(source.Field<string>(prefixColumn + "AlarmInstanceStatus"))==false){
				retVal.AlarmInstanceStatus = source.Field<string>(prefixColumn + "AlarmInstanceStatus").ParseAsEnum<AlarmInstanceStatus>();
			}

			return retVal;
			
		}
	}
}
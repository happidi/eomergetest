﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.DataLayer.BrokerDB {
	/// <summary>
	/// Broker DB for Report Subscription Business Entity.
	/// </summary>
	public class ReportSubscriptionBrokerDB : BaseBrokerDB<ReportSubscription> {

		/// <summary>
		/// Creates the specified item.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <returns></returns>
		public override ReportSubscription Create(ReportSubscription item) {
			ReportingService.CreateSubscription(item);
			return item;
		}

		/// <summary>
		/// Updates the specified item.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <returns></returns>
		public override ReportSubscription Update(ReportSubscription item) {
			throw new NotImplementedException();
			//return base.Update(item);
		}

		/// <summary>
		/// Deletes the specified item.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <returns></returns>
		public override bool Delete(ReportSubscription item) {
			return Delete(item.KeyReportSubscription);
		}

		/// <summary>
		/// Deletes the specified key.
		/// </summary>
		/// <param name="Key">The key.</param>
		/// <returns></returns>
		public override bool Delete(string Key) {
			ReportingService.DeleteSubscription(Key);
			return true;
		}

		/// <summary>
		/// Workflows the delete instance.
		/// </summary>
		/// <param name="instanceId">The instance id.</param>
		/// <returns></returns>
		public override bool WorkflowDeleteInstance(Guid instanceId) {
			throw new NotSupportedException();
		}

		/// <summary>
		/// Updates the batch.
		/// </summary>
		/// <param name="keys">The keys.</param>
		/// <param name="item">The item.</param>
		/// <returns></returns>
		public override int UpdateBatch(string[] keys, ReportSubscription item) {
			throw new NotSupportedException();
		}

		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, ReportSubscription item) {
			throw new System.NotImplementedException();
		}

		/// <summary>
		/// Populates a T business entity from a IDataReader.
		/// </summary>
		/// <param name="source">The reader.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>
		/// The populated T business entity.
		/// </returns>
		protected override ReportSubscription FillObject(DataSourceDecorator source, string prefixColumn) {
			throw new System.NotImplementedException();
		}

		/// <summary>
		/// Gets all by report path.
		/// </summary>
		/// <param name="reportPath">The report path.</param>
		/// <param name="paging">The paging.</param>
		/// <param name="totalCount">The total count.</param>
		/// <returns></returns>
		public List<ReportSubscription> GetAllByReportPath(string reportPath, PagingParameter paging, out int totalCount) {
			return ReportingService.GetSubscriptions(reportPath, Helper.GetCurrentActor(), paging, out totalCount);
		}
	}
}
﻿using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.DataLayer.BrokerDB {

	/// <summary>
	/// Database broker for FilterSpatialPset business entity.
	/// </summary>
	public class FilterSpatialPsetBrokerDB : BaseBrokerDB<FilterSpatialPset> {

		/// <summary>
		/// The name of the stored procedure that retreives a list of FilterSpatialPset for a specific chart.
		/// </summary>
		protected const string const_proc_getlistbychart = AppModule.Energy + "_FilterSpatialPsetGetListByKeyChart";

		/// <summary>
		/// The name of the stored procedure that retreives a list of FilterSpatialPset for a specific chart.
		/// </summary>
		protected const string const_proc_getallpagingbykeychart = AppModule.Energy + "_FilterSpatialPsetGetAllPagingByKeyChart";

		/// <summary>
		/// The name of the stored procedure that saves a list of FilterSpatialPset for a specific chart.
		/// </summary>
		protected const string const_proc_chartfilterspatialpsetsave = AppModule.Energy + "_ChartFilterSpatialPsetSave";

		/// <summary>
		/// The name of the stored procedure that retreives a list of FilterSpatialPset for a specific chart.
		/// </summary>
		protected const string const_proc_getlistbyalarmdefinition = AppModule.Energy + "_FilterSpatialPsetGetListByKeyAlarmDefinition";

		/// <summary>
		/// The name of the stored procedure that retreives a list of FilterSpatialPset for a specific chart.
		/// </summary>
		protected const string const_proc_getallpagingbykeyalarmdefinition = AppModule.Energy + "_FilterSpatialPsetGetAllPagingByKeyAlarmDefinition";

		/// <summary>
		/// The name of the stored procedure that saves a list of FilterSpatialPset for a specific chart.
		/// </summary>
		protected const string const_proc_alarmdefinitionfilterspatialpsetsave = AppModule.Energy + "_AlarmDefinitionFilterSpatialPsetSave";

		/// <summary>
		/// Gets the const_proc_prefix.
		/// </summary>
		/// <value>The const_proc_prefix.</value>
		protected override string const_proc_prefix {
			get {
				return AppModule.Energy;
			}
		}

		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, FilterSpatialPset item) {
			db.AddInParameterString(command, "Key", item.KeyFilterSpatialPset);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "LocalId", item.LocalId);
			db.AddInParameterInt(command, "Order", item.Order);
			db.AddInParameterString(command, "KeyChart", item.KeyChart);
			db.AddInParameterString(command, "KeyAlarmDefinition", item.KeyAlarmDefinition);
			db.AddInParameterString(command, "PsetName", item.PsetName);
			db.AddInParameterString(command, "AttributeName", item.AttributeName);
			db.AddInParameterString(command, "Value", item.Value);
			db.AddInParameterString(command, "Value2", item.Value2);
			db.AddInParameterString(command, "Condition", item.Condition.ParseAsString());
			db.AddInParameterString(command, "Operator", item.Operator.ParseAsString());
		}

		/// <summary>
		/// Populates a FilterSpatialPset business entity from a DataSourceDecorator.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated FilterSpatialPset business entity.</returns>
		protected override FilterSpatialPset FillObject(DataSourceDecorator source, string prefixColumn) {
			var retVal = new FilterSpatialPset {
				KeyFilterSpatialPset = source.Field<int>(prefixColumn + "KeyFilterSpatialPset").ToString(CultureInfo.InvariantCulture),
				LocalId = source.Field<string>(prefixColumn + "LocalId"),
				Order = source.Field<int>(prefixColumn + "Order"),
				PsetName = source.Field<string>(prefixColumn + "PsetName"),
				AttributeName = source.Field<string>(prefixColumn + "AttributeName"),
				Value = source.Field<string>(prefixColumn + "Value"),
				Value2 = source.Field<string>(prefixColumn + "Value2"),
				Condition = source.Field<string>(prefixColumn + "Condition").ParseAsEnum<FilterCondition>(),
				Operator = source.Field<string>(prefixColumn + "Operator").ParseAsEnum<LogicalOperator>(),
				UsageName = source.Field<string>(prefixColumn + "UsageName"),
				PsetMsgCode = source.Field<string>(prefixColumn + "PsetMsgCode"),
				AttributeMsgCode = source.Field<string>(prefixColumn + "AttributeMsgCode")
			};
			var keyChart = source.Field<int?>(prefixColumn + "KeyChart");
			if (keyChart.HasValue)
				retVal.KeyChart = keyChart.Value.ToString(CultureInfo.InvariantCulture);
			var keyAlarmDefinition = source.Field<int?>(prefixColumn + "KeyAlarmDefinition");
			if (keyAlarmDefinition.HasValue)
				retVal.KeyAlarmDefinition = keyAlarmDefinition.Value.ToString(CultureInfo.InvariantCulture);

			retVal.PropertySingleValueLongPath = string.Format("{0} / {1}", Helper.LocalizeText(retVal.PsetMsgCode),
															   Helper.LocalizeText(retVal.AttributeMsgCode));
			return retVal;
		}
		/// <summary>
		/// Get the list of saved FilterSpatialPset from a Chart
		/// </summary>
		/// <param name="KeyChart">The key of the Chart.</param>
		public List<FilterSpatialPset> GetListByKeyChart(string KeyChart) {
			return GetListByKeyObject(const_proc_getlistbychart, KeyChart, "KeyChart");
		}
		/// <summary>
		/// Get the list of saved FilterSpatialPset from a AlarmDefinition
		/// </summary>
		/// <param name="KeyAlarmDefinition">The key of the KeyAlarmDefinition.</param>
		public List<FilterSpatialPset> GetListByKeyAlarmDefinition(string KeyAlarmDefinition) {
			return GetListByKeyObject(const_proc_getlistbyalarmdefinition, KeyAlarmDefinition, "KeyChart");
		}

		/// <summary>
		/// Get the list of saved FilterSpatialPset from a Object
		/// </summary>
		/// <param name="proc">The proc.</param>
		/// <param name="KeyObject">The key object.</param>
		/// <param name="fieldKey">The field key.</param>
		/// <returns></returns>
		private List<FilterSpatialPset> GetListByKeyObject(string proc, string KeyObject, string fieldKey) {
			List<FilterSpatialPset> results;
			DbCommand command = db.GetStoredProcCommand(const_proc_getlistbychart);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, fieldKey, KeyObject);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}

		/// <summary>
		/// Gets all the FilterSpatialPset for a specific chart.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyChart">The Key of the chart.</param>
		/// <param name="total">The total number of items.</param>
		/// <returns>The list of items.</returns>
		public List<FilterSpatialPset> GetAllPagingByKeyChart(PagingParameter paging, string KeyChart, out int total) {
			return GetAllPagingByKeyObject(paging, const_proc_getallpagingbykeychart, KeyChart, "KeyChart", out total);
		}

		/// <summary>
		/// Gets all the FilterSpatialPset for a specific AlarmDefinition.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyAlarmDefinition">The key alarm definition.</param>
		/// <param name="total">The total number of items.</param>
		/// <returns>
		/// The list of items.
		/// </returns>
		public List<FilterSpatialPset> GetAllPagingByKeyAlarmDefinition(PagingParameter paging, string KeyAlarmDefinition, out int total) {
			return GetAllPagingByKeyObject(paging, const_proc_getallpagingbykeyalarmdefinition, KeyAlarmDefinition, "KeyAlarmDefinition", out total);
		}

		/// <summary>
		/// Gets all the FilterSpatialPset for a specific Object.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="proc">The proc.</param>
		/// <param name="KeyObject">The key object.</param>
		/// <param name="fieldKey">The field key.</param>
		/// <param name="total">The total number of items.</param>
		/// <returns>
		/// The list of items.
		/// </returns>
		private List<FilterSpatialPset> GetAllPagingByKeyObject(PagingParameter paging, string proc, string KeyObject, string fieldKey, out int total) {
			DbCommand command = db.GetStoredProcCommand(proc);
			command.CommandTimeout = 600;
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, fieldKey, KeyObject);
			db.AddInParameterInt(command, "start", paging.start);
			db.AddInParameterInt(command, "limit", paging.limit);
			DataSet ds = db.ExecuteDataSet(command);
			if (ds.Tables[0].Rows.Count > 0)
				total = ds.Tables[0].Rows[0].Field<int>("TotalRecord");
			else
				total = 0;
			return FillObjects(ds);
		}


		/// <summary>
		/// Saves the chart FilterSpatialPset.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="psetFilters">The FilterSpatialPset store.</param>
		/// <returns></returns>
		public List<FilterSpatialPset> SaveChartFilterSpatialPset(Chart entity, CrudStore<FilterSpatialPset> psetFilters) {
			return SaveObjectFilterSpatialPset(const_proc_chartfilterspatialpsetsave, entity.KeyChart, "KeyChart", psetFilters);
		}

		/// <summary>
		/// Saves the AlarmDefinition FilterSpatialPset.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="psetFilters">The FilterSpatialPset store.</param>
		/// <returns></returns>
		public List<FilterSpatialPset> SaveAlarmDefinitionFilterSpatialPset(AlarmDefinition entity, CrudStore<FilterSpatialPset> psetFilters) {
			return SaveObjectFilterSpatialPset(const_proc_alarmdefinitionfilterspatialpsetsave, entity.KeyAlarmDefinition, "KeyAlarmDefinition", psetFilters);
		}

		/// <summary>
		/// Saves the chart FilterSpatialPset.
		/// </summary>
		/// <param name="proc">The proc.</param>
		/// <param name="KeyObject">The key object.</param>
		/// <param name="fieldKey">The field key.</param>
		/// <param name="psetFilters">The FilterSpatialPset store.</param>
		/// <returns></returns>
		private List<FilterSpatialPset> SaveObjectFilterSpatialPset(string proc, string KeyObject, string fieldKey, CrudStore<FilterSpatialPset> psetFilters) {
			DbCommand command = db.GetStoredProcCommand(proc);
			command.CommandTimeout = 600;
			List<FilterSpatialPset> results;

			if (psetFilters == null || psetFilters.IsEmpty())
				return null;
			string inserts = null;
			string deletes = null;


			if (psetFilters.destroy != null) {
				deletes = Helper.BuildXml(psetFilters.destroy.Select(p => p.KeyFilterSpatialPset).ToArray());
			}

			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, fieldKey, KeyObject);
			db.AddInParameterString(command, "Inserts", inserts);
			db.AddInParameterString(command, "Deletes", deletes);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}
	}
}
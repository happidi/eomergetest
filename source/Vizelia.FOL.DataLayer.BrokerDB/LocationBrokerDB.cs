﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.DataLayer.BrokerDB {

	/// <summary>
	/// Database broker for Location business entity.
	/// </summary>
	public class LocationBrokerDB : BaseBrokerDB<Location> {
		/// <summary>
		/// Gets the name of the entity source.
		/// </summary>
		/// <value>
		/// The name of the entity source.
		/// </value>
		protected override string EntitySourceName {
			get {
				return "HierarchySpatial";
			}
		}

		/// <summary>
		/// Gets the entity key name in database.
		/// </summary>
		/// <value>
		/// The entity key name in database.
		/// </value>
		protected override string EntityKeyNameInDB {
			get {
				return "KeyChildren";
			}
		}

		/// <summary>
		/// The name of the stored procedure that gets all locations by key word
		/// </summary>
		protected const string const_proc_getallpagingbykeword = AppModule.Core + "_HierarchySpatialGetAllPaging";


		/// <summary>
		/// The name of the stored procedure that retreives a list of locations for a specific map.
		/// </summary>
		protected const string const_proc_getlistbymap = AppModule.Energy + "_LocationGetListByKeyMap";




		#region Chart
		/// <summary>
		/// The name of the stored procedure that saves a spatial filter for a specific chart
		/// </summary>
		protected const string const_proc_chartfilterspatialsave = AppModule.Energy + "_ChartFilterSpatialSave";
		/// <summary>
		/// The name of the stored procedure that retreives a spatial filter for a specific chart.
		/// </summary>
		protected const string const_proc_getallpagingbykeychart = AppModule.Energy + "_FilterSpatialGetAllPagingByKeyChart";
		/// <summary>
		/// The name of the stored procedure that retreives a list of location based on pset filters for a specific chart.
		/// </summary>
		protected const string const_proc_getpsetfilterallpagingbykeychart = AppModule.Energy + "_FilterSpatialPsetGetLocationAllPagingByKeyChart";
		#endregion
		
		#region AlarmDefinition
		/// <summary>
		/// The name of the stored procedure that saves a spatial filter for a specific AlarmDefinition
		/// </summary>
		protected const string const_proc_alarmdefinitionfilterspatialsave = AppModule.Energy + "_AlarmDefinitionFilterSpatialSave";
		/// <summary>
		/// The name of the stored procedure that retreives a spatial filter for a specific AlarmDefinition.
		/// </summary>
		protected const string const_proc_getallpagingbykeyalarmdefinition = AppModule.Energy + "_FilterSpatialGetAllPagingByKeyAlarmDefinition";
		/// <summary>
		/// The name of the stored procedure that retreives a list of location based on pset filters for a specific AlarmDefinition.
		/// </summary>
		protected const string const_proc_getpsetfilterallpagingbykeyalarmdefinition = AppModule.Energy + "_FilterSpatialPsetGetLocationAllPagingByKeyAlarmDefinition";
		#endregion
		#region MeterValidationRule
		/// <summary>
		/// The name of the stored procedure that saves a spatial filter for a specific MeterValidationRule
		/// </summary>
		protected const string const_proc_metervalidationrulefilterspatialsave = AppModule.Energy + "_MeterValidationRuleFilterSpatialSave";
		/// <summary>
		/// The name of the stored procedure that retreives a spatial filter for a specific MeterValidationRule.
		/// </summary>
		protected const string const_proc_getallpagingbykeymetervalidationrule = AppModule.Energy + "_FilterSpatialGetAllPagingByKeyMeterValidationRule";

		#endregion
		#region AlarmTable
		/// <summary>
		/// The name of the stored procedure that saves a spatial filter for a specific alarmtable.
		/// </summary>
		protected const string const_proc_alarmtablefilterspatialsave = AppModule.Energy + "_AlarmTableFilterSpatialSave";
		/// <summary>
		/// The name of the stored procedure that retreives a spatial filter for a specific alarmtable.
		/// </summary>
		protected const string const_proc_getallpagingbykeyalarmtable = AppModule.Energy + "_FilterSpatialGetAllPagingByKeyAlarmTable";
		#endregion
		#region Mail
		/// <summary>
		/// The name of the stored procedure that retreives a spatial filter for a specific mail.
		/// </summary>
		const string const_proc_getallpagingbykeymail = AppModule.ServiceDesk + "_LocationGetAllPagingByKeyMail";
		/// <summary>
		/// The name of the stored procedure that saves a spatial filter for a specific mail.
		/// </summary>
		const string const_proc_maillocationssave = AppModule.ServiceDesk + "_MailLocationsSave";
		#endregion
		#region PortalTemplate
		/// <summary>
		/// The name of the stored procedure that saves a spatial filter for a specific PortalTemplate
		/// </summary>
		protected const string const_proc_portaltemplatefilterspatialsave = AppModule.Energy + "_PortalTemplateFilterSpatialSave";
		/// <summary>
		/// The name of the stored procedure that retreives a spatial filter for a specific PortalTemplate.
		/// </summary>
		protected const string const_proc_getallpagingbykeyportaltemplate = AppModule.Energy + "_FilterSpatialGetAllPagingByKeyPortalTemplate";
		#endregion

		/// <summary>
		/// Builds the paraLocations for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append paraLocations to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, Location item) {
			throw new NotImplementedException();
		}

		/// <summary>
		/// Populates a Location business entity from a DataSourceDecorator.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated ActionRequest business entity.</returns>
		protected override Location FillObject(DataSourceDecorator source, string prefixColumn) {
			var elevaltion = prefixColumn + "Elevation";
			var areaValue = prefixColumn + "AreaValue";
			var keyClassificationItem = prefixColumn + "KeyClassificationItem";
			return new Location {
				KeyLocation = source.Field<string>(prefixColumn + "KeyChildren"),
				KeyParent = source.Field<string>(prefixColumn + "KeyParent"),
				KeyLocationPath = source.Field<string>(prefixColumn + "KeyLocationPath"),
				LocalId = source.Field<string>(prefixColumn + "LocalId"),
				LocalIdPath = source.Field<string>(prefixColumn + "LocalIdPath"),
				LongPath = source.Field<string>(prefixColumn + "LongPath"),
				ShortPath = source.Field<string>(prefixColumn + "ShortPath"),
				IfcType = source.Field<string>(prefixColumn + "TypeName"),
				TypeName = source.Field<string>(prefixColumn + "TypeName").ParseAsEnum<HierarchySpatialTypeName>(),
				Level = source.Field<int>(prefixColumn + "Level"),
				Name = source.Field<string>(prefixColumn + "Name"),
				Description = source.Field<string>(prefixColumn + "Description"),
				Elevation = source.FieldExists(elevaltion) ? source.Field<double?>(elevaltion) : null,
				AreaValue = source.FieldExists(areaValue) ? source.Field<double?>(areaValue).GetValueOrDefault() : default(double),
				KeyClassificationItem = source.FieldExists(keyClassificationItem) ? source.Field<string>(keyClassificationItem) : null
			};
		}

		/// <summary>
		/// Gets all the Location entities..
		/// </summary>
		/// <returns></returns>
		public List<Location> GetAll() {
			var retVal = GetAllNoPaging();
			return retVal;
		}

		/// <summary>
		/// Get the list of Building from a Map
		/// </summary>
		/// <param name="keyMap">The key of the Map.</param>
		public List<Location> GetListByKeyMap(string keyMap) {
			List<Location> results;
			DbCommand command = db.GetStoredProcCommand(const_proc_getlistbymap);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyMap", keyMap);
			AddSecurityFilterParam<Location>(command, "ProjectLocationFilter");
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}


		/// <summary>
		/// Gets all the locations for a specific mail.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="keyMail">The Key of the Mail.</param>
		/// <param name="total">The total number of items.</param>
		/// <returns>The list of items.</returns>
		public List<Location> GetAllPagingByKeyMail(PagingParameter paging, string keyMail, out int total) {
			DbCommand command = db.GetStoredProcCommand(const_proc_getallpagingbykeymail);
			command.CommandTimeout = 600;
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyMail", keyMail);
			db.AddInParameterInt(command, "start", paging.start);
			db.AddInParameterInt(command, "limit", paging.limit);
			DataSet ds = db.ExecuteDataSet(command);
			total = ds.Tables[0].Rows.Count > 0 ? ds.Tables[0].Rows[0].Field<int>("TotalRecord") : 0;
			return FillObjects(ds);
		}

		/// <summary>
		/// Gets all the locations for a specific key word.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="keyword">The keyword.</param>
		/// <param name="total">The total number of items.</param>
		/// <returns>
		/// The list of items.
		/// </returns>
		public List<Location> GetAllPagingByKeyword(PagingParameter paging, string keyword, out int total) {
			DbCommand command = db.GetStoredProcCommand(const_proc_getallpagingbykeword);
			command.CommandTimeout = 600;
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "Keyword", keyword);
			AddProjectLocationFilter(command, "ProjectLocationFilter");
			db.AddInParameterInt(command, "start", paging.start);
			db.AddInParameterInt(command, "limit", paging.limit);
			DataSet ds = db.ExecuteDataSet(command);
			total = ds.Tables[0].Rows.Count > 0 ? ds.Tables[0].Rows[0].Field<int>("TotalRecord") : 0;
			return FillObjects(ds);
		}

		/// <summary>
		/// Gets all the filter spatial for a specific chart.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="keyChart">The Key of the chart.</param>
		/// <param name="total">The total number of items.</param>
		/// <returns>The list of items.</returns>
		public List<Location> GetFilterSpatialAllPagingByKeyChart(PagingParameter paging, string keyChart, out int total) {
			return GetFilterSpatialAllPagingByKeyObject(const_proc_getallpagingbykeychart, paging, keyChart, "KeyChart", out total);
		}

		/// <summary>
		/// Gets all the filter spatial for a specific AlarmDefinition.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="keyAlarmDefinition">The Key of the AlarmDefinition.</param>
		/// <param name="total">The total number of items.</param>
		/// <returns>The list of items.</returns>
		public List<Location> GetFilterSpatialAllPagingByKeyAlarmDefinition(PagingParameter paging, string keyAlarmDefinition, out int total) {
			return GetFilterSpatialAllPagingByKeyObject(const_proc_getallpagingbykeyalarmdefinition, paging, keyAlarmDefinition, "KeyAlarmDefinition", out total);
		}

		/// <summary>
		/// Gets all the filter spatial for a specific MeterValidationRule.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="keyMeterValidationRule">The Key of the MeterValidationRule.</param>
		/// <param name="total">The total number of items.</param>
		/// <returns>The list of items.</returns>
		public List<Location> GetFilterSpatialAllPagingByKeyMeterValidationRule(PagingParameter paging, string keyMeterValidationRule, out int total) {
			return GetFilterSpatialAllPagingByKeyObject(const_proc_getallpagingbykeymetervalidationrule, paging, keyMeterValidationRule, "KeyMeterValidationRule", out total);
		}

		/// <summary>
		/// Gets all the filter spatial for a specific AlarmTable.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="keyAlarmTable">The Key of the AlarmTable.</param>
		/// <param name="total">The total number of items.</param>
		/// <returns>The list of items.</returns>
		public List<Location> GetFilterSpatialAllPagingByKeyAlarmTable(PagingParameter paging, string keyAlarmTable, out int total) {
			return GetFilterSpatialAllPagingByKeyObject(const_proc_getallpagingbykeyalarmtable, paging, keyAlarmTable, "KeyAlarmTable", out total);
		}

		/// <summary>
		/// Gets all the filter spatial for a specific PortalTemplate.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="keyPortalTemplate">The Key of the PortalTemplate.</param>
		/// <param name="total">The total number of items.</param>
		/// <returns>The list of items.</returns>
		public List<Location> GetFilterSpatialAllPagingByKeyPortalTemplate(PagingParameter paging, string keyPortalTemplate, out int total) {
			return GetFilterSpatialAllPagingByKeyObject(const_proc_getallpagingbykeyportaltemplate, paging, keyPortalTemplate, "KeyPortalTemplate", out total);
		}

		/// <summary>
		/// Gets all the filter spatial for a specific object.
		/// </summary>
		/// <param name="proc">The proc.</param>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="keyObject">The key object.</param>
		/// <param name="fieldKey">The field key.</param>
		/// <param name="total">The total number of items.</param>
		/// <returns>
		/// The list of items.
		/// </returns>
		private List<Location> GetFilterSpatialAllPagingByKeyObject(string proc, PagingParameter paging, string keyObject, string fieldKey, out int total) {
			DbCommand command = db.GetStoredProcCommand(proc);
			command.CommandTimeout = 600;
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, fieldKey, keyObject);
			db.AddInParameterInt(command, "start", paging.start);
			db.AddInParameterInt(command, "limit", paging.limit);
			DataSet ds = db.ExecuteDataSet(command);
			total = ds.Tables[0].Rows.Count > 0 ? ds.Tables[0].Rows[0].Field<int>("TotalRecord") : 0;
			return FillObjects(ds);
		}

		/// <summary>
		/// Saves the chart Filter Spatial.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="filterSpatial">The filter spatial.</param>
		/// <returns></returns>
		public List<Location> SaveChartFilterSpatial(Chart entity, CrudStore<Location> filterSpatial) {
			return SaveObjectFilterSpatial(const_proc_chartfilterspatialsave, entity.KeyChart, "KeyChart", filterSpatial);
		}

		/// <summary>
		/// Saves the AlarmDefinition Filter Spatial.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="filterSpatial">The filter spatial.</param>
		/// <returns></returns>
		public List<Location> SaveAlarmDefinitionFilterSpatial(AlarmDefinition entity, CrudStore<Location> filterSpatial) {
			return SaveObjectFilterSpatial(const_proc_alarmdefinitionfilterspatialsave, entity.KeyAlarmDefinition, "KeyAlarmDefinition", filterSpatial);
		}

		/// <summary>
		/// Saves the MeterValidationRule Filter Spatial.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="filterSpatial">The filter spatial.</param>
		/// <returns></returns>
		public List<Location> SaveMeterValidationRuleFilterSpatial(MeterValidationRule entity, CrudStore<Location> filterSpatial) {
			return SaveObjectFilterSpatial(const_proc_metervalidationrulefilterspatialsave, entity.KeyMeterValidationRule, "KeyMeterValidationRule", filterSpatial);
		}

		/// <summary>
		/// Saves the AlarmTable Filter Spatial.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="filterSpatial">The filter spatial.</param>
		/// <returns></returns>
		public List<Location> SaveAlarmTableFilterSpatial(AlarmTable entity, CrudStore<Location> filterSpatial) {
			return SaveObjectFilterSpatial(const_proc_alarmtablefilterspatialsave, entity.KeyAlarmTable, "KeyAlarmTable", filterSpatial);
		}

		/// <summary>
		/// Saves the PortalTemplate Filter Spatial.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="filterSpatial">The filter spatial.</param>
		/// <returns></returns>
		public List<Location> SavePortalTemplateFilterSpatial(PortalTemplate entity, CrudStore<Location> filterSpatial) {
			return SaveObjectFilterSpatial(const_proc_portaltemplatefilterspatialsave, entity.KeyPortalTemplate, "KeyPortalTemplate", filterSpatial);
		}

		/// <summary>
		/// Saves the chart Filter Spatial.
		/// </summary>
		/// <param name="proc">The proc.</param>
		/// <param name="keyObject">The key object.</param>
		/// <param name="fieldKey">The field key.</param>
		/// <param name="filterSpatial">The filter spatial.</param>
		/// <returns></returns>
		private List<Location> SaveObjectFilterSpatial(string proc, string keyObject, string fieldKey, CrudStore<Location> filterSpatial) {


			DbCommand command = db.GetStoredProcCommand(proc);
			command.CommandTimeout = 600;
			List<Location> results;

			if (filterSpatial == null)
				return null;
			string inserts = null;
			string deletes = null;

			if (filterSpatial.create != null) {
				// We ensure here that the client only sent locations to which the user has direct or descendant permission
				// We only check if the location filter has entries. if location filter count is zero, the user has access to everything.
				var filterLocationAll = FilterHelper.GetFilter<Location>();
				if (filterLocationAll.Count > 0) {
					List<Filter> filterLocation = filterLocationAll.Where(filter => filter.Type != FilterType.Ascendant).ToList();
					List<string> locations = (from p in filterSpatial.create
											  join q in filterLocation
												 on p.KeyLocation equals q.Key
											  select p.KeyLocation).ToList();
					if (filterSpatial.create.Count > locations.Count)
						throw new Exception("Unauthorized location");
				}
				inserts = Helper.BuildXml(filterSpatial.create.Select(p => p.KeyLocation).ToArray());

			}

			if (filterSpatial.destroy != null) {
				deletes = Helper.BuildXml(filterSpatial.destroy.Select(p => p.KeyLocation).ToArray());
			}

			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, fieldKey, keyObject);
			db.AddInParameterString(command, "Inserts", inserts);
			db.AddInParameterString(command, "Deletes", deletes);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}


		/// <summary>
		/// Gets all the filter spatial based on pset filters for a specific chart.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="keyChart">The Key of the chart.</param>
		/// <param name="total">The total number of items.</param>
		/// <returns>The list of items.</returns>
		public List<Location> GetFilterSpatialPsetAllPagingByKeyChart(PagingParameter paging, string keyChart, out int total) {
			return GetFilterSpatialPsetAllPagingByKeyObject(paging, const_proc_getpsetfilterallpagingbykeychart, keyChart, "KeyChart", out total);
		}

		/// <summary>
		/// Gets all the filter spatial based on pset filters for a specific AlarmDefinition.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="keyAlarmDefinition">The Key of the AlarmDefinition.</param>
		/// <param name="total">The total number of items.</param>
		/// <returns>The list of items.</returns>
		public List<Location> GetFilterSpatialPsetAllPagingByKeyAlarmDefinition(PagingParameter paging, string keyAlarmDefinition, out int total) {
			return GetFilterSpatialPsetAllPagingByKeyObject(paging, const_proc_getpsetfilterallpagingbykeyalarmdefinition, keyAlarmDefinition, "KeyAlarmDefinition", out total);
		}


		/// <summary>
		/// Gets all the filter spatial based on pset filters for a specific chart.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="proc">The proc.</param>
		/// <param name="keyObject">The key object.</param>
		/// <param name="fieldKey">The field key.</param>
		/// <param name="total">The total number of items.</param>
		/// <returns>
		/// The list of items.
		/// </returns>
		private List<Location> GetFilterSpatialPsetAllPagingByKeyObject(PagingParameter paging, string proc, string keyObject, string fieldKey, out int total) {
			DbCommand command = db.GetStoredProcCommand(proc);
			command.CommandTimeout = 600;
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, fieldKey, keyObject);
			db.AddInParameterInt(command, "start", paging.start);
			db.AddInParameterInt(command, "limit", paging.limit);
			DataSet ds = db.ExecuteDataSet(command);
			if (ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Columns.Contains("TotalRecord"))
				total = ds.Tables[0].Rows[0].Field<int>("TotalRecord");
			else
				total = 0;
			return FillObjects(ds);
		}

		/// <summary>
		/// Saves the mail locations.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="locations">The locations.</param>
		/// <returns></returns>
		public List<Location> SaveMailLocations(Mail entity, CrudStore<Location> locations) {
			DbCommand command = db.GetStoredProcCommand(const_proc_maillocationssave);
			command.CommandTimeout = 600;
			List<Location> results;

			if (locations == null || locations.IsEmpty())
				return null;
			string inserts = null;
			string deletes = null;

			if (locations.create != null) {
				inserts = Helper.BuildXml(locations.create.Select(p => p.KeyLocation).ToArray());
			}

			if (locations.destroy != null) {
				deletes = Helper.BuildXml(locations.destroy.Select(p => p.KeyLocation).ToArray());
			}

			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyMail", entity.KeyMail);
			db.AddInParameterString(command, "Inserts", inserts);
			db.AddInParameterString(command, "Deletes", deletes);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}

		/// <summary>
		/// Gets list of locations by local id's.
		/// </summary>
		/// <param name="localIds"></param>
		/// <returns></returns>
		public List<Location> GetListByLocalIds(string[] localIds) {
			List<Location> results;
			DbCommand command = db.GetStoredProcCommand("Core_LocationGetListByLocalIds");
			var localIdsDictionary = SplitLocalIds(localIds);

			db.AddInParameterXml(command, "Keys", Helper.BuildXml(localIdsDictionary));
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			if (IsLocalized)
				db.AddInParameterString(command, "Culture", Helper.GetCurrentCultureShort());
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}

		/// <summary>
		/// Splitting localIds string array in format sample "IfcSite:21" to KeyValuePair of (key=21, value=IfcSite)
		/// </summary>
		/// <param name="localIds"></param>
		/// <returns></returns>
		private Dictionary<string, string> SplitLocalIds(IEnumerable<string> localIds) {
			const string defaultHierarchyLevel = "IfcSite";

			var splittedDictionary = new Dictionary<string, string>();
			foreach (var localId in localIds) {
				var localIdsDictionary = localId.Split(':');

				if (localIdsDictionary.Length == 1) {
					if (!splittedDictionary.ContainsKey(localIdsDictionary[0])) {
						splittedDictionary.Add(localIdsDictionary[0], defaultHierarchyLevel);
					}
				}
				if (localIdsDictionary.Length == 2) {
					if (!splittedDictionary.ContainsKey(localIdsDictionary[1])) {
						splittedDictionary.Add(localIdsDictionary[1], localIdsDictionary[0]);
					}
				}
			}
			return splittedDictionary;

		}
	}
}
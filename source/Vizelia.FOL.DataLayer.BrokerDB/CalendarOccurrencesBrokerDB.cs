﻿using System;
using System.Data.Common;
using System.Data.SqlClient;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.DataLayer.BrokerDB {

	/// <summary>
	/// Database broker for CalendarOccurrences business entity.
	/// </summary>
	public class CalendarOccurrencesBrokerDB : BaseBrokerDB<CalendarOccurrences> {

		/// <summary>
		/// Stored procedure for deleting calendar occurrences items.
		/// </summary>
		protected const string const_proc_calendaroccurences_delete_item_by_keycalendar = AppModule.Core + "_CalendarOccurrencesDeleteItemByKeyCalendar";

		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, CalendarOccurrences item) {
			db.AddInParameterString(command, "Key", item.KeyCalendarOccurrences);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterByteArray(command, "CalendarOccurrencesData", CompressionHelper.Compress(item.CalendarOccurrencesData));
			db.AddInParameterDateTime(command, "StartDate", item.StartDate);
			db.AddInParameterDateTime(command, "EndDate", item.EndDate);
			db.AddInParameterString(command, "Timezone", item.TimeZone);
			db.AddInParameterString(command, "KeyCalendar", item.KeyCalendar);
			db.AddInParameterString(command, "iCalData", item.iCalData);
		}

		/// <summary>
		/// Populates a CalendarOccurrences business entity from a IDataReader.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated Document business entity.</returns>
		protected override CalendarOccurrences FillObject(DataSourceDecorator source, string prefixColumn) {
			var calendarOccurences = new CalendarOccurrences {
				KeyCalendarOccurrences = source.Field<int>(prefixColumn + "KeyCalendarOccurrences").ToString(),
				CalendarOccurrencesData = CompressionHelper.Decompress(source.Field<byte[]>(prefixColumn + "CalendarOccurrencesData")),
				StartDate = source.Field<DateTime>(prefixColumn + "StartDate"),
				EndDate = source.Field<DateTime>(prefixColumn + "EndDate"),
				TimeZone = source.Field<string>(prefixColumn + "Timezone"),
				KeyCalendar = source.Field<string>(prefixColumn + "KeyCalendar"),
				iCalData = source.Field<string>(prefixColumn + "iCalData"),
			};

			return calendarOccurences;
		}

		/// <summary>
		/// Deletes the CalendarOccurrences by key calendar.
		/// </summary>
		/// <param name="keyObject">The key object.</param>
		public void DeleteByKeyCalendar(string keyObject) {
			using (DbCommand command = db.GetStoredProcCommand(const_proc_calendaroccurences_delete_item_by_keycalendar)) {
				db.AddInParameterString(command,"KeyCalendar",keyObject);
				db.AddReturnParameter(command);
				try {
					db.ExecuteNonQuery(command);
					
				}
				catch (SqlException ex) {
					throw SqlExceptionWrap(ex);
				}
			}
		}
	}
}
﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common.Localization;
using Vizelia.FOL.Infrastructure;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.DataLayer.BrokerDB {
	/// <summary>
	/// Database broker for LocalizationResource business entity.
	/// </summary>
	public class LocalizationResourceBrokerDB : BaseBrokerDB<LocalizationResource> {
		/// <summary>
		/// Builds the save parameters.
		/// </summary>
		/// <param name="command">The command.</param>
		/// <param name="item">The item.</param>
		/// <exception cref="System.NotImplementedException"></exception>
		protected override void BuildSaveParams(DbCommand command, LocalizationResource item) {
			throw new NotImplementedException();
		}

		/// <summary>
		/// Fills the object.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="prefixColumn">The prefix column.</param>
		/// <returns></returns>
		protected override LocalizationResource FillObject(DataSourceDecorator source, string prefixColumn) {
			return new LocalizationResource {
				Id = source.Field<int>(prefixColumn + "Id").ToString(CultureInfo.InvariantCulture),
				Culture = source.Field<string>(prefixColumn + "Culture"),
				Key = source.Field<string>(prefixColumn + "Key"),
				Value = source.Field<string>(prefixColumn + "Value"),
				IsUserDefined = true
			};
		}

		/// <summary>
		/// Gets the item by local id.
		/// </summary>
		/// <param name="localId">The local id.</param>
		/// <returns></returns>
		public override LocalizationResource GetItemByLocalId(string localId) {
			var value = DatabaseResourceHelper.GetString(localId);
			LocalizationResource retVal = null;
			if (!string.IsNullOrWhiteSpace(value))
				retVal = new LocalizationResource { Key = localId, Value = value };
			return retVal;
		}

		/// <summary>
		/// Updates the batch.
		/// </summary>
		/// <param name="keys">The keys.</param>
		/// <param name="item">The item.</param>
		/// <returns></returns>
		public override int UpdateBatch(string[] keys, LocalizationResource item) {
			throw new NotImplementedException();
		}

		/// <summary>
		/// Deletes the specified key.
		/// </summary>
		/// <param name="Key">The key.</param>
		/// <returns></returns>
		public override bool Delete(string Key) {
			throw new NotImplementedException();
		}

		/// <summary>
		/// Creates the specified item.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <returns></returns>
		public override LocalizationResource Create(LocalizationResource item) {
			var id = DatabaseResourceHelper.AddResource(item.Key, item.Value, item.Culture);
			item.Id = id.ToString(CultureInfo.InvariantCulture);
			List<string> cultures = DatabaseResourceHelper.GetSupportedCultures();
			cultures.Remove(item.Culture);
			// we then translate for each culture and save again
			foreach (string languageTo in cultures) {
				try {
					var translated = TranslationHelper.TranslateText(item.Value, item.Culture, languageTo);
					DatabaseResourceHelper.AddResource(item.Key, translated, languageTo);
					ExternalCrudNotificationService.SendNotification(new LocalizationResource { Key = item.Key, Culture = languageTo, Value = translated}, CrudAction.Create);
				}
				catch { }
			}

			return item;
		}

		/// <summary>
		/// Updates the specified item.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <returns></returns>
		public override LocalizationResource Update(LocalizationResource item) {
			DatabaseResourceHelper.UpdateResource(item.Key, item.Value, item.Culture);
			return item;
		}
	}
}

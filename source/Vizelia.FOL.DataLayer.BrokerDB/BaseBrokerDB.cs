﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Localization;
using Vizelia.FOL.DataLayer.Interfaces;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.DataLayer.BrokerDB {
	/// <summary>
	/// Base data layer class for business entity broker using database access.
	/// </summary>
	/// <typeparam name="T">The business entity.</typeparam>
	public abstract class BaseBrokerDB<T> : BaseBrokerDBGeneric<T> where T : BaseBusinessEntity, new() {
		private const string const_auditmodifiedby = "AuditModifiedBy";
		private const string const_audittimelastmodified = "AuditTimeLastModified";
		private const string const_auditmodifiedbylogin = "AuditModifiedByLogin";
		private const string const_auditmodifiedbyfirstname = "AuditModifiedByFirstName";
		private const string const_auditmodifiedbylastname = "AuditModifiedByLastName";
		/// <summary>
		/// the return param.
		/// </summary>
		protected const string const_commandreturnparam = "Return";
		private const string const_type_filterjointype = "dbo.FilterJoinType";

		#region protected variables

		/// <summary>
		/// The inner database object.
		/// </summary>
		protected IDataAccess db;

		/// <summary>
		/// The prefix of stored procedures of that provider. 
		/// Can be overrided if needed.
		/// </summary>
		protected virtual string const_proc_prefix {
			get { return AppModule.Core; }
		}

		/// <summary>
		/// Gets a value indicating whether this instance is asynchronous save.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if this instance is asynchronous save; otherwise, <c>false</c>.
		/// </value>
		protected bool IsAsynchronousSave {
			get { return ExternalCrudNotificationService.IsAsynchronousMode; }
		}

		/// <summary>
		/// The name of the stored procedure that retreives an item (Core_BusinessEntityGetItem).
		/// </summary>
		protected string const_proc_getitem;

		/// <summary>
		/// The name of the stored procedure that retreives an item by its LocalId (Core_BusinessEntityGetItemByLocalId).
		/// </summary>
		protected string const_proc_getitembylocalid;

		/// <summary>
		/// The name of the stored procedure that deletes an item (Core_BusinessEntityDeleteItem).
		/// </summary>
		protected string const_proc_deleteitem;

		/// <summary>
		/// The name of the stored procedure that retreives children of an item (Core_BusinessEntityGetChildren).
		/// </summary>
		protected string const_proc_getchildren;

		/// <summary>
		/// The name of the stored procedure that updates an item (Core_BusinessEntityUpdateItem).
		/// </summary>
		protected string const_proc_updateitem;

		/// <summary>
		/// The name of the stored procedure that creates an item (Core_BusinessEntityCreateItem).
		/// </summary>
		protected string const_proc_createitem;

		/// <summary>
		/// The name of the stored procedure that retreives all items (Core_BusinessEntityGetAll).
		/// </summary>
		protected string const_proc_getall;

		/// <summary>
		/// The name of the stored procedure that retreives a list of items by Keys (Core_BusinessEntityGetList).
		/// </summary>
		protected string const_proc_getlist;

		/// <summary>
		/// The name of the stored procedure that populates references from a list of items by Keys (Core_BusinessEntityPopulateReference).
		/// </summary>
		protected string const_proc_populatereference;

		/// <summary>
		/// The name of the stored procedure that does generic paging (Core_GenericPaging).
		/// </summary>
		protected const string const_proc_generic_paging = AppModule.Core + "_GenericPaging";

		/// <summary>
		/// The name of the stored procedure that does generic update batch (Core_GenericUpdateBatch).
		/// </summary>
		protected string const_proc_generic_updatebatch = AppModule.Core + "_GenericUpdateBatch";

		/// <summary>
		/// The name of the stored procedure that deletes a worklow instance from Tracking and Persistence databases.
		/// </summary>
		protected const string const_proc_worklow_deleteinstance = AppModule.Core + "_WorkflowDeleteInstance";


		#endregion

		#region Constructor

		/// <summary>
		/// Public ctor.
		/// </summary>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
		protected BaseBrokerDB()
			: base() {
			// we initiate the proc const.
			const_proc_createitem = String.Format("{0}_{1}SaveItem", const_proc_prefix, BusinessEntityName);
			const_proc_updateitem = String.Format("{0}_{1}SaveItem", const_proc_prefix, BusinessEntityName);
			const_proc_deleteitem = String.Format("{0}_{1}DeleteItem", const_proc_prefix, BusinessEntityName);
			const_proc_getitem = String.Format("{0}_{1}GetItem", const_proc_prefix, BusinessEntityName);
			const_proc_getitembylocalid = String.Format("{0}_{1}GetItemByLocalId", const_proc_prefix, BusinessEntityName);
			const_proc_getchildren = String.Format("{0}_{1}GetChildren", const_proc_prefix, BusinessEntityName);
			const_proc_getall = String.Format("{0}_{1}GetAll", const_proc_prefix, BusinessEntityName);
			const_proc_getlist = String.Format("{0}_{1}GetList", const_proc_prefix, BusinessEntityName);
			const_proc_populatereference = String.Format("{0}_{1}PopulateReference", const_proc_prefix, BusinessEntityName);
			db = Helper.CreateInstance<DataAccess, IDataAccess>();
		}

		#endregion

		#region Build save params

		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected abstract void BuildSaveParams(DbCommand command, T item);

		/// <summary>
		/// Builds the save params internal.
		/// </summary>
		/// <param name="command">The command.</param>
		/// <param name="item">The item.</param>
		internal void BuildSaveParamsInternal(DbCommand command, T item) {
			BuildSaveParams(command, item);
			BuildSaveParamsForIAuditEntity(command, item);
			BuildSaveParamsForIWorkflowEntity(command, item);
			BuildSaveParamsForISecurableObject(command, item);
			db.AddOutParameterString(command, const_commandreturnparam);
		}

		/// <summary>
		/// Builds the save params for IAuditEntity.
		/// </summary>
		/// <param name="command">The command.</param>
		/// <param name="item">The item.</param>
		private void BuildSaveParamsForIAuditEntity(DbCommand command, T item) {
			IAuditEntity entity = item as IAuditEntity;
			if (entity == null)
				return;

			entity.AuditTimeLastModified = DateTime.UtcNow;
			entity.AuditModifiedBy = Helper.GetCurrentActor();

			db.AddInParameterDateTime(command, const_audittimelastmodified, entity.AuditTimeLastModified);
			db.AddInParameterString(command, const_auditmodifiedby, entity.AuditModifiedBy);
		}

		/// <summary>
		/// Builds the save params for ISecurableObject.
		/// </summary>
		/// <param name="command">The command.</param>
		/// <param name="item">The item.</param>
		private void BuildSaveParamsForISecurableObject(DbCommand command, T item) {
			ISecurableObject entity = item as ISecurableObject;

			if (entity == null)
				return;
			if (string.IsNullOrEmpty(entity.Creator))
				entity.Creator = Helper.GetCurrentActor();
			db.AddInParameterString(command, "Creator", entity.Creator);
		}

		/// <summary>
		/// Builds the save params for an IWorkflowEntity.
		/// </summary>
		/// <param name="command">The command.</param>
		/// <param name="item">The item.</param>
		private void BuildSaveParamsForIWorkflowEntity(DbCommand command, T item) {
			IWorkflowEntity entity = item as IWorkflowEntity;
			if (entity == null)
				return;
			db.AddInParameterGuid(command, "InstanceId", entity.InstanceId);
			db.AddInParameterString(command, "State", entity.State);
		}

		#endregion

		#region FillObject region

		/// <summary>
		/// Populates a T business entity from a IDataReader.
		/// </summary>
		/// <param name="source">The reader.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>
		/// The populated T business entity.
		/// </returns>
		protected abstract T FillObject(DataSourceDecorator source, string prefixColumn);

		/// <summary>
		/// Fills the object internal.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="prefixColumn">The prefix column.</param>
		/// <returns></returns>
		protected T FillObjectInternal(DataSourceDecorator source, string prefixColumn) {
			ValidateObjectBelongsToCurrentTenant(source);

			T retVal = this.FillObject(source, prefixColumn);
			retVal = this.FillObjectIWorkflowEntity(retVal, source, prefixColumn);
			retVal = this.FillObjectIAuditEntity(retVal, source, prefixColumn);
			retVal = this.FillObjectISecurableObject(retVal, source, prefixColumn);

			return retVal;
		}

		/// <summary>
		/// Validates the retreived object belongs to the current tenant.
		/// </summary>
		/// <param name="source">The source.</param>
		private void ValidateObjectBelongsToCurrentTenant(DataSourceDecorator source) {
			if (IsTenantAwareObject() && source.FieldExists("App")) {
				string objectTenant = source.Field<string>("App");

				if (!ContextHelper.ApplicationName.Equals(objectTenant)) {
					StackTrace callStack = new StackTrace();
					TracingService.Write("Item of different tenant retreived, examine query. /n" + TraceEntrySeverity.Error, callStack.ToString());
					throw new VizeliaException("Item of different tenant retreived, examine query.");
				}
			}
		}

		/// <summary>
		/// Fills the object if it implements IWorkflowEntity.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <param name="source">The source.</param>
		/// <param name="prefixColumn">The prefix column.</param>
		/// <returns></returns>
		private T FillObjectIWorkflowEntity(T item, DataSourceDecorator source, string prefixColumn) {
			IWorkflowEntity entity = item as IWorkflowEntity;
			if (entity == null)
				return item;
			entity.State = source.Field<string>(prefixColumn + "State");
			entity.StateLabel = source.Field<string>(prefixColumn + "StateLabel");
			entity.InstanceId = source.Field<Guid>(prefixColumn + "InstanceId");
			return item;
		}

		/// <summary>
		/// Fills the object if it implements IAuditEntity.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <param name="source">The source.</param>
		/// <param name="prefixColumn">The prefix column.</param>
		/// <returns></returns>
		private T FillObjectIAuditEntity(T item, DataSourceDecorator source, string prefixColumn) {
			IAuditEntity entity = item as IAuditEntity;
			if (entity == null)
				return item;

			entity.AuditModifiedBy = source.Field<string>(prefixColumn + const_auditmodifiedby);
			entity.AuditTimeLastModified = source.Field<DateTime?>(prefixColumn + const_audittimelastmodified);
			entity.AuditModifiedByLogin = source.Field<string>(prefixColumn + const_auditmodifiedbylogin);
			entity.AuditModifiedByFirstName = source.Field<string>(prefixColumn + const_auditmodifiedbyfirstname);
			entity.AuditModifiedByLastName = source.Field<string>(prefixColumn + const_auditmodifiedbylastname);

			return item;
		}

		/// <summary>
		/// Fills the object if it implements ISecurableObject.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <param name="source">The source.</param>
		/// <param name="prefixColumn">The prefix column.</param>
		/// <returns></returns>
		private T FillObjectISecurableObject(T item, DataSourceDecorator source, string prefixColumn) {
			ISecurableObject entity = item as ISecurableObject;

			if (entity == null)
				return item;
			if (source.FieldExists(prefixColumn + "Creator"))
				entity.Creator = source.Field<string>(prefixColumn + "Creator");

			return item;
		}

		/// <summary>
		/// Populates a T business entity from a IDataReader.
		/// </summary>
		/// <param name="reader">The reader.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated T business entity.</returns>
		protected T FillObject(IDataReader reader, string prefixColumn) {
			return this.FillObjectInternal(new DataReaderDecorator(reader), prefixColumn);
		}

		/// <summary>
		/// Populates an existing T business entity from a IDataReader. Used when populating references.
		/// </summary>
		/// <param name="item">The business entity.</param>
		/// <param name="reader">The reader.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		protected virtual void FillObject(T item, IDataReader reader, string prefixColumn) { }

		/// <summary>
		/// Populates a T business entity from a DataRow
		/// </summary>
		/// <param name="row">The row</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated T business entity.</returns>
		protected T FillObject(DataRow row, string prefixColumn) {
			return this.FillObjectInternal(new DataRowDecorator(row), prefixColumn);
		}

		/// <summary>
		/// Populates a T business entity from a IDataReader.
		/// </summary>
		/// <param name="reader">The reader.</param>
		/// <returns>The populated T business entity.</returns>
		protected T FillObject(IDataReader reader) {
			return FillObject(reader, "");
		}

		/// <summary>
		/// Populates a T business entity from a DataRow.
		/// </summary>
		/// <param name="row">The row.</param>
		/// <returns>The populated T business entity.</returns>
		protected T FillObject(DataRow row) {
			return FillObject(row, "");
		}

		/// <summary>
		/// Populates a list of T business entity from a IDataReader.
		/// </summary>
		/// <param name="reader">The reader.</param>
		/// <returns>The list of populated T business entities.</returns>
		protected virtual List<T> FillObjects(IDataReader reader) {
			List<T> tempList = new List<T>();
			while (reader.Read()) {
				tempList.Add(FillObject(reader));
			}
			return tempList;
		}

		/// <summary>
		/// Populates a list of T business entity from a IDataReader.
		/// </summary>
		/// <param name="results">A list of entities that need to be completed by the information in the reader.</param>
		/// <param name="reader">The reader.</param>
		/// <returns>The list of populated T business entities.</returns>
		protected virtual List<T> FillObjects(List<T> results, IDataReader reader) {
			while (reader.Read()) {
				T item = results.Find(p => p.GetPropertyValueString(idProperty, false, false) == reader.GetValue<string>(idProperty));
				FillObject(item, reader, "");
			}
			return results;
		}

		/// <summary>
		/// Populates a list of T business entity from a Dataset.
		/// </summary>
		/// <param name="ds">The dataset.</param>
		/// <returns>The list of populated T business entities.</returns>
		protected List<T> FillObjects(DataSet ds) {
			return (from DataRow row in ds.Tables[0].Rows select FillObject(row)).ToList();
		}

		#endregion

		#region Read methods

		/// <summary>
		/// Gets an item.
		/// </summary>
		/// <param name="Key">The key of the item.</param>
		/// <returns>The item.</returns>
		public override T GetItem(string Key) {
			T item = default(T);
			DbCommand command = db.GetStoredProcCommand(const_proc_getitem);
			db.AddInParameterString(command, "Key", Key);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			if (IsLocalized)
				db.AddInParameterString(command, "Culture", Helper.GetCurrentCultureShort());
			using (IDataReader reader = db.ExecuteReader(command)) {
				if (reader.Read()) {
					item = FillObject(reader);
				}
			}
			// populates the references.
			this.PopulateReference(new List<T> { item });
			return item;
		}

		/// <summary>
		/// Gets an item by its LocalId.
		/// </summary>
		/// <param name="localId">The LocalId of the item.</param>
		/// <returns>The item.</returns>
		public override T GetItemByLocalId(string localId) {
			// We do these checks here because for now EntLib Validation isn't wrapping this in the mapping process.
			if (localId == null) {
				throw new ArgumentNullException("localId");
			}

			if (localId.Length > 255) {
				throw new ArgumentException(string.Format("localId '{0}' is longer than 255 characters", localId));
			}

			T item = default(T);
			DbCommand command = db.GetStoredProcCommand(const_proc_getitembylocalid);
			db.AddInParameterString(command, "LocalId", localId);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			if (IsLocalized)
				db.AddInParameterString(command, "Culture", Helper.GetCurrentCultureShort());
			using (IDataReader reader = db.ExecuteReader(command)) {
				if (reader.Read()) {
					item = FillObject(reader);
				}
			}
			// populates the references.
			this.PopulateReference(new List<T> { item });
			return item;
		}

		/// <summary>
		/// Gets children for an item.
		/// </summary>
		/// <param name="Key">The Key of the parent item.</param>
		/// <returns>The list of children.</returns>
		public override List<T> GetChildren(string Key) {
			List<T> results;
			DbCommand command = db.GetStoredProcCommand(const_proc_getchildren);
			db.AddInParameterString(command, "Key", Key);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			if (IsLocalized)
				db.AddInParameterString(command, "Culture", Helper.GetCurrentCultureShort());
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}

		/// <summary>
		/// Gets a list of items.
		/// </summary>
		/// <param name="Keys">An array of keys.</param>
		/// <returns>The list of items corresponding to the provided keys.</returns>
		public override List<T> GetList(string[] Keys) {
			return GetList(Keys, const_proc_getlist);
		}

		/// <summary>
		/// Gets a list of items.
		/// </summary>
		/// <param name="Keys">The keys.</param>
		/// <param name="procedureName">Name of the stored procedure in database.</param>
		/// <returns></returns>
		public List<T> GetList(string[] Keys, string procedureName) {
			List<T> results;
			DbCommand command = db.GetStoredProcCommand(procedureName);
			db.AddInParameterXml(command, "Keys", Helper.BuildXml(Keys));
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			if (IsLocalized)
				db.AddInParameterString(command, "Culture", Helper.GetCurrentCultureShort());
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}



		/// <summary>
		/// Gets all the items from database into memory. 
		/// Uses const_proc_getall proc to retreive the items.
		/// </summary>
		/// <returns>The list of items.</returns>
		protected override List<T> GetAllNoPaging() {
			List<T> results;
			DbCommand command = db.GetStoredProcCommand(const_proc_getall);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}

		/// <summary>
		/// Gets the name of the entity source.
		/// </summary>
		/// <value>
		/// The name of the entity source.
		/// </value>
		protected virtual string EntitySourceName {
			get {
				return IsHierarchical ? "Hierarchy" + BusinessEntityName : BusinessEntityName;
			}
		}

		/// <summary>
		/// Gets the entity key name in database.
		/// </summary>
		/// <value>
		/// The entity key name in database.
		/// </value>
		protected virtual string EntityKeyNameInDB {
			get {
				return Helper.GetPropertyNameId(typeof(T));
			}
		}
		/// <summary>
		/// Gets all the items from the server.
		/// Uses the generic paging proc to retreive the items. The name of this proc can be overrided if necessary.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="total">The total number of items.</param>
		/// <returns>The list of items.</returns>
		protected override List<T> GetAllGenericPaging(PagingParameter paging, out int total) {
			DbCommand command = db.GetStoredProcCommand(const_proc_generic_paging);
			command.CommandTimeout = 600;
			//db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			if (IsLocalized)
				db.AddInParameterString(command, "datasrcCTE", String.Format("SELECT * FROM dbo.{0}Localized('{1}','{2}')", EntitySourceName, ContextHelper.ApplicationName, Helper.GetCurrentCultureShort()));
			else
				db.AddInParameterString(command, "datasrc", EntitySourceName);

			AddPagingParameterOrderBy(paging, command);

			db.AddInParameterInt(command, "start", paging.start);
			db.AddInParameterInt(command, "limit", paging.limit);

			AddPagingParameterFilter(paging, command);

			db.AddInParameterString(command, "KeyEntity", EntityKeyNameInDB);

			List<T> retVal = new List<T>();
			total = 0;
			using (IDataReader reader = db.ExecuteReader(command)) {
				while (reader.Read()) {
					if (total <= 0)
						total = reader.GetValue<int>("total");
					retVal.Add(FillObject(reader));
				}
			}
			return retVal;
		}

		/// <summary>
		/// Adds the paging parameter order by.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="command">The command.</param>
		protected void AddPagingParameterOrderBy(PagingParameter paging, DbCommand command) {
			if (paging.sort != null && paging.dir != null) {
				if (!paging.sort.IsOrderByValid() || paging.dir.WordCount() > 1)
					throw new ApplicationException("The parameters of the query are incorrect");
				if ((paging.sort.ContainsAny(";", "-")) || paging.dir.ContainsAny(";", ",", "-"))
					throw new ApplicationException("The parameters of the query are incorrect");

				db.AddInParameterString(command, "orderBy", paging.sort.SafeSqlLiteral() + " " + paging.dir.Replace(".", "_").SafeSqlLiteral());
			}
		}



		/// <summary>
		/// Adds the paging parameter filter.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="command">The command.</param>
		protected virtual void AddPagingParameterFilter(PagingParameter paging, DbCommand command) {
			string whereClause = "";

			if (!String.IsNullOrEmpty(paging.query) && !String.IsNullOrEmpty(paging.filter)) {
				whereClause = BuildWhereClause(paging.filter.Replace(".", "_"), paging.query);
			}
			if (paging.filters != null) {
				var filterWhereClause = BuildWhereClause(paging.filters);
				if (!string.IsNullOrWhiteSpace(whereClause) && !string.IsNullOrWhiteSpace(filterWhereClause))
					whereClause = "(" + whereClause + ") AND (" + filterWhereClause + ") ";
				else
					whereClause = filterWhereClause;
			}
			if (paging.innerjoinfilters != null) {
				db.AddInParameterTable(command, "filterjoin", BuildJoinFilterType(paging.innerjoinfilters), const_type_filterjointype);
			}

			string completeWhereClause;

			if (IsTenantAwareObject()) {
				completeWhereClause = "App = '" + ContextHelper.ApplicationName + "'";

				if (!string.IsNullOrWhiteSpace(whereClause)) {
					completeWhereClause += (" AND (" + whereClause + ")");
				}
			}
			else {
				completeWhereClause = whereClause;
			}

			db.AddInParameterString(command, "filter", completeWhereClause);
		}


		/// <summary>
		/// Adds the specified type of Security Filter as an xml param (used to filter for security in GetList brokers for example)
		/// </summary>
		/// <param name="command">The command.</param>
		/// <param name="parameterName">Name of the parameter.</param>
		public void AddSecurityFilterParam<H>(DbCommand command, string parameterName) {
			string xmlFilter = null;
			var filter = FilterHelper.GetFilter<H>();
			if (filter.Count > 0) {
				xmlFilter = Helper.BuildXml(filter.Select(f => f.Key).ToArray());
			}
			db.AddInParameterString(command, parameterName, xmlFilter);
		}

		/// <summary>
		/// Determines whether the object brokered by this component is tenant aware or not.
		/// </summary>
		/// <returns>
		///   <c>true</c> if the object brokered by this component is tenant aware; otherwise, <c>false</c>.
		/// </returns>
		protected virtual bool IsTenantAwareObject() {
			return true;
		}

		#endregion

		#region Create methods

		/// <summary>
		/// Creates (inserts) the provided item to database.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns>The item created.</returns>
		public override T Create(T item) {
			string key;
			using (DbCommand command = db.GetStoredProcCommand(const_proc_createitem)) {
				BuildSaveParamsInternal(command, item);
				db.AddReturnParameter(command);
				try {
					db.ExecuteNonQuery(command);
					int num = db.GetReturnParameter(command);
					if (num != 0)
						throw new VizeliaDatabaseException(num);
					key = Convert.ToString(db.GetParameterValue(command, const_commandreturnparam));
				}
				catch (SqlException ex) {
					throw SqlExceptionWrap(ex);
				}
			}

			T retval = GetItem(key);
			ThrowOnNullSavedItem(retval);
			AddCreatedItemToSecurityFilter(key, retval);

			return retval;
		}

		/// <summary>
		/// Wraps the SQL exception.
		/// </summary>
		/// <param name="ex">The ex.</param>
		protected Exception SqlExceptionWrap(SqlException ex) {
			if (ex.Message.StartsWith("error_"))
				return new VizeliaDatabaseException(Langue.ResourceManager.GetString(ex.Message), ex.Number);

			Exception wrappingException;
			switch (ex.Number) {
				case 2601:
					// Case of an attempt to create a duplicate LocalId mapping_ table
					wrappingException = new VizeliaDatabaseException(100);
					break;
				case 4060:
					// Invalid Database
					wrappingException = new VizeliaException(Langue.error_database_invalid_database + " " + Langue.error_operation, ex);
					break;
				case 18456:
					// Login Failed
					wrappingException = new VizeliaException(Langue.error_database_login_failed + " " + Langue.error_operation, ex);
					break;
				case 547:
					// ForeignKey Violation
					wrappingException = new VizeliaException(Langue.error_database_foreignkey_violation + " " + Langue.error_operation, ex);
					break;
				case 2627:
					// Unique Index/Constraint Violation
					wrappingException = new VizeliaDatabaseException(Langue.error_database_unique_constraint_violation + " " + Langue.error_operation, 101);
					break;
				default:
					// throw a general DAL Exception
					wrappingException = new VizeliaException(Langue.error_database_generic + " " + Langue.error_operation, ex);
					break;
			}
			return wrappingException;
		}


		/// <summary>
		/// Throws on null saved item.
		/// This can be overridden to blank implementation on classes
		/// that have different logic on creation.
		/// </summary>
		/// <param name="item">The item.</param>
		protected virtual void ThrowOnNullSavedItem(T item) {
			if (item == null) {
				throw new VizeliaException(Langue.msg_save_item_error_null_returned);
			}
		}

		#endregion

		#region Update methods

		/// <summary>
		/// Updates the provided item to database.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns>The item updated.</returns>
		public override T Update(T item) {
			if (IsUserAllowed(item)) {
				return DoUpdate(item);
			}

			throw new VizeliaSecurityException(Langue.msg_access_denied_update);
		}

		/// <summary>
		/// Determines whether the current user has the needed filters to see and modify the given object.
		/// </summary>
		/// <param name="item">The item to check on.</param>
		/// <returns>
		///   <c>true</c> if the current user has the needed filters to see and modify the given object; otherwise, <c>false</c>.
		/// </returns>
		protected virtual bool IsUserAllowed(T item) {
			// Get the filter attributes of the type. Cached in memory to save reflection work.
			string key = string.Format("FilterAttributesForType:{0}", typeof(T).Name);
			List<Tuple<PropertyInfo, FilterTypeAttribute>> attributes = CacheService.Get(key, GetFilterAttributes, 0, CacheLocation.Memory);

			// Perform filtering logic.
			foreach (Tuple<PropertyInfo, FilterTypeAttribute> tuple in attributes) {
				List<Filter> xmlFilters = FilterHelper.GetFilter(tuple.Item2.FilterBusinessEntityType).Where(filter => filter.Type != FilterType.Ascendant).ToList();
				if ((xmlFilters.Count == 0 && tuple.Item2.ShowAllIfEmpty) || (xmlFilters.Any(filter => filter.Key.ToString() == item.GetPropertyValue(tuple.Item1.Name).ToString()))) {
					return true;
				}

			}

			//If the entity has no filter properties it means there are no filters on it which means everyone has access to 
			//all of them.
			bool isUserAllowed = !attributes.Any();
			if (!isUserAllowed) {
				TracingService.Write(TraceEntrySeverity.Warning, "Access denied when trying to update item. Item Type:" + item.GetType() + " Item Key:" + item.GetKey() + " User Key:" + Helper.GetCurrentActor(), "ItemAccessDenied", item.GetType().ToString());
			}
			return isUserAllowed;
		}

		/// <summary>
		/// Gets the filter attributes of type T together with the properties that contain them, as tuples.
		/// </summary>
		/// <returns></returns>
		private static List<Tuple<PropertyInfo, FilterTypeAttribute>> GetFilterAttributes() {
			var allPropertiesOnT = typeof(T).GetProperties();
			var attributes = new List<Tuple<PropertyInfo, FilterTypeAttribute>>();

			foreach (PropertyInfo pinfo in allPropertiesOnT) {
				IEnumerable<FilterTypeAttribute> filterTypeAttributes = pinfo.GetCustomAttributes(typeof(FilterTypeAttribute), false).OfType<FilterTypeAttribute>();
				attributes.AddRange(
					filterTypeAttributes.Select(
						filterTypeAttribute => new Tuple<PropertyInfo, FilterTypeAttribute>(pinfo, filterTypeAttribute)));
			}

			return attributes;
		}

		/// <summary>
		/// Does the update.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <returns></returns>
		protected virtual T DoUpdate(T item) {
			using (DbCommand command = db.GetStoredProcCommand(const_proc_updateitem)) {
				BuildSaveParamsInternal(command, item);
				string result;
				try {
					db.ExecuteNonQuery(command);
					result = Convert.ToString(db.GetParameterValue(command, const_commandreturnparam));
				}
				catch (SqlException ex) {
					throw SqlExceptionWrap(ex);
				}
				T retVal = GetItem(result);
				ThrowOnNullSavedItem(retVal);

				return retVal;
			}
		}

		/// <summary>
		/// Update batch (mass update) a series of items based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public override int UpdateBatch(string[] keys, T item) {
			var newKeys = new List<string>();

			foreach (var key in keys) {
				var itemToUpdate = GetItem(key);
				if (IsUserAllowed(itemToUpdate)) {
					newKeys.Add(itemToUpdate.GetKey());
				}
				//var allPropertiesOnT = typeof(T).GetProperties();
				//foreach (PropertyInfo pinfo in allPropertiesOnT) {
				//    object[] filterTypeAttributes = pinfo.GetCustomAttributes(typeof(FilterTypeAttribute), false);
				//    foreach (object obj in filterTypeAttributes) {
				//        FilterTypeAttribute filterTypeAttribute = obj as FilterTypeAttribute;
				//        if (filterTypeAttribute == null)
				//            continue;

				//        List<Filter> xmlFilters =
				//            FilterHelper.GetFilter(filterTypeAttribute.FilterBusinessEntityType).Where(
				//                filter => filter.Type != FilterType.Ascendant).ToList();
				//        if ((xmlFilters.Any(filter => filter.Key.ToString() == itemToUpdate.GetPropertyValue(pinfo.Name).ToString())) ||
				//            (xmlFilters.Count == 0 && filterTypeAttribute.ShowAllIfEmpty)) {
				//            newKeys.Add(item.GetKey());
				//        }
				//    }
				//}
			}

			keys = newKeys.ToArray();

			using (DbCommand command = db.GetStoredProcCommand(const_proc_generic_updatebatch)) {
				db.AddInParameterXml(command, "Keys", Helper.BuildXml(keys));
				db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
				db.AddInParameterString(command, "datasrc", typeof(T).Name);
				db.AddInParameterString(command, "Key", Helper.GetPropertyNameId(typeof(T)));
				db.AddInParameterString(command, "updatelist", String.Join(",", item.BuildSQLUpdateBatchInstruction().ToArray()));
				BuildAdditionalUpdateBatchParams(command);
				db.AddOutParameterString(command, "total");
				db.ExecuteNonQuery(command);
				int result = Convert.ToInt32(db.GetParameterValue(command, "total"));
				return result;
			}
		}

		#endregion

		#region Delete methods

		/// <summary>
		/// Deletes the provided item from database.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns>True if successfull, false otherwise.</returns>
		public override bool Delete(T item) {
			return IsUserAllowed(item) && Delete(item.GetPropertyValue(idProperty).ToString());
		}

		/// <summary>
		/// Builds the parameters for the delete proc.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		protected virtual void BuildAdditionalDeleteParams(DbCommand command) {

		}

		/// <summary>
		/// Builds the parameters for the updatebatch proc.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		protected virtual void BuildAdditionalUpdateBatchParams(DbCommand command) {

		}

		/// <summary>
		/// Deletes the provided item from database.
		/// </summary>
		/// <param name="Key">The key of the item to delete.</param>
		/// <returns>True if successfull, false otherwise.</returns>
		public override bool Delete(string Key) {
			using (DbCommand command = db.GetStoredProcCommand(const_proc_deleteitem)) {
				db.AddInParameterString(command, "Key", Key);
				BuildAdditionalDeleteParams(command);
				try {
					int result = db.ExecuteNonQuery(command);
					return result > 0;
				}
				catch (SqlException ex) {
					throw SqlExceptionWrap(ex);
				}
			}
		}

		/// <summary>
		/// Deletes a workflow instance from persistence and tracking database.
		/// </summary>
		/// <param name="instanceId"></param>
		/// <returns></returns>
		public override bool WorkflowDeleteInstance(Guid instanceId) {
			try {
				DbCommand command = db.GetStoredProcCommand(const_proc_worklow_deleteinstance);
				db.AddInParameterGuid(command, "InstanceId", instanceId);
				int result = db.ExecuteNonQuery(command);
				return result > 0;
			}
			catch (SqlException ex) {
				throw SqlExceptionWrap(ex);
			}
		}

		#endregion

		#region Project Location Filter
		/// <summary>
		/// Adds the project location filter.
		/// </summary>
		/// <param name="command">The command.</param>
		/// <param name="parameterName">Name of the parameter.</param>
		public void AddProjectLocationFilter(DbCommand command, string parameterName) {
			string projectLocationFilter = null;
			var locationFilter = FilterHelper.GetFilter<Location>();
			if ((locationFilter == null || locationFilter.Count == 0)) {
				//we set it from the EA side...
				locationFilter = ContextHelper.Get("LocationFilter") as List<Filter>;
			}
			if (locationFilter != null && locationFilter.Count > 0) {
				projectLocationFilter = Helper.BuildXml(locationFilter.Where(filter => filter.Type != FilterType.Ascendant).Select(filter => filter.Key).ToArray());
			}
			db.AddInParameterString(command, parameterName, projectLocationFilter);
		}
		#endregion

		/// <summary>
		/// Sends the audit action.
		/// </summary>
		/// <param name="parametersDictionary">The parameters dictionary.</param>
		public override void SaveAuditAction(Dictionary<string, object> parametersDictionary) {
			using (var auditCommand = db.GetStoredProcCommand("Audit_SaveAction")) {
				foreach (var parameter in parametersDictionary) {
					if (parameter.Key == "ChangeTime") {
						db.AddInParameterDateTime(auditCommand, parameter.Key, (DateTime)parameter.Value);
					}
					else {
						db.AddInParameterString(auditCommand, parameter.Key, (string)parameter.Value);
					}
				}
				db.ExecuteNonQuery(auditCommand);
			}
		}
	}
}

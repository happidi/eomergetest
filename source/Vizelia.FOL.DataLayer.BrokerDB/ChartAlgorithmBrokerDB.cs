﻿using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web.Security;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.DataLayer.BrokerDB {

	/// <summary>
	/// Database broker for ChartAlgorithm business entity.
	/// </summary>
	public class ChartAlgorithmBrokerDB : BaseBrokerDB<ChartAlgorithm> {

		/// <summary>
		/// The name of the stored procedure that retreives a list of dataseries for a specific chart.
		/// </summary>
		protected const string const_proc_getlistbychart = AppModule.Energy + "_ChartAlgorithmGetListByKeyChart";


		/// <summary>
		/// The name of the stored procedure that retreives a list of meters for a specific chart.
		/// </summary>
		protected const string const_proc_getallpagingbykeychart = AppModule.Energy + "_ChartAlgorithmGetAllPagingByKeyChart";


		/// <summary>
		/// The name of the stored procedure that saves a list of meters for a specific chart.
		/// </summary>
		protected const string const_proc_chartdataseriessave = AppModule.Energy + "_ChartChartAlgorithmSave";


		/// <summary>
		/// The prefix of stored procedures of that provider. 
		/// </summary>
		protected override string const_proc_prefix {
			get {
				return AppModule.Energy;
			}
		}
		

		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, ChartAlgorithm item) {
			// TODO : Add here the parameters for the save procedures
			db.AddInParameterString(command, "Key", item.KeyChartAlgorithm);
			db.AddInParameterString(command, "Application", Membership.ApplicationName);
			db.AddInParameterString(command, "KeyChart", item.KeyChart);
			db.AddInParameterString(command, "KeyAlgorithm", item.KeyAlgorithm);
			db.AddInParameterInt(command, "Order", item.Order);
		}


		/// <summary>
		/// Populates a ChartAlgorithm business entity from a DataSourceDecorator.
		/// </summary>
		/// <param name="source">The data source.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated ChartAlgorithm business entity.</returns>
		protected override ChartAlgorithm FillObject(DataSourceDecorator source, string prefixColumn) {
			return new ChartAlgorithm {
				KeyChartAlgorithm = source.Field<int>(prefixColumn + "KeyChartAlgorithm").ToString(),
				KeyChart = source.Field<int>(prefixColumn + "KeyChart").ToString(),
				KeyAlgorithm = source.Field<string>(prefixColumn + "KeyAlgorithm"),
				Order = source.Field<int>(prefixColumn + "Order"),
				AlgorithmName = source.Field<string>(prefixColumn + "AlgorithmName")
			};
		}

		/// <summary>
		/// Get the list of saved ChartAlgorithm from a Chart
		/// </summary>
		/// <param name="KeyChart">The key of the chart.</param>
		public List<ChartAlgorithm> GetListByKeyChart(string KeyChart) {
			List<ChartAlgorithm> results;
			DbCommand command = db.GetStoredProcCommand(const_proc_getlistbychart);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyChart", KeyChart);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}

		/// <summary>
		/// Gets all the ChartAlgorithm for a specific chart.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyChart">The Key of the chart.</param>
		/// <param name="total">The total number of items.</param>
		/// <returns>The list of items.</returns>
		public List<ChartAlgorithm> GetAllPagingByKeyChart(PagingParameter paging, string KeyChart, out int total) {
			DbCommand command = db.GetStoredProcCommand(const_proc_getallpagingbykeychart);
			command.CommandTimeout = 600;
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyChart", KeyChart);
			db.AddInParameterInt(command, "start", paging.start);
			db.AddInParameterInt(command, "limit", paging.limit);

			/*AddPagingParameterOrderBy(paging, command);

			if (paging != null)
				paging.ExtractInnerJoinFilters();
			AddPagingParameterFilter(paging, command);
			*/

			DataSet ds = db.ExecuteDataSet(command);
			if (ds.Tables[0].Rows.Count > 0)
				total = ds.Tables[0].Rows[0].Field<int>("TotalRecord");
			else
				total = 0;
			return FillObjects(ds);
		}


		/// <summary>
		/// Saves the chart dataseries.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="algorithms">The algorithms.</param>
		/// <returns></returns>
		public List<ChartAlgorithm> SaveChartChartAlgorithm(Chart entity, CrudStore<ChartAlgorithm> algorithms) {
			DbCommand command = db.GetStoredProcCommand(const_proc_chartdataseriessave);
			command.CommandTimeout = 600;
			List<ChartAlgorithm> results;

			if (algorithms == null || algorithms.IsEmpty())
				return null;
			string inserts = null;
			string deletes = null;

			
			if (algorithms.destroy != null) {
				deletes = Helper.BuildXml(algorithms.destroy.Select(p => p.KeyChartAlgorithm).ToArray());
			}

			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyChart", entity.KeyChart);
			db.AddInParameterString(command, "Inserts", inserts);
			db.AddInParameterString(command, "Deletes", deletes);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}
	}
}
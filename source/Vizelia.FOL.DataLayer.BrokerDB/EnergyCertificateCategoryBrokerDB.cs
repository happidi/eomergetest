﻿using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.DataLayer.BrokerDB {

	/// <summary>
	/// Database broker for EnergyCertificateCategory business entity.
	/// </summary>
	public class EnergyCertificateCategoryBrokerDB : BaseBrokerDB<EnergyCertificateCategory> {

		/// <summary>
		/// The name of the stored procedure that retreives a list of Portal Tabs for a specific Portal.
		/// </summary>
		protected const string const_proc_getlistbyenergycertificate = AppModule.Energy + "_EnergyCertificateCategoryGetListByKeyEnergyCertificate";

		/// <summary>
		/// The name of the stored procedure that retreives a list of Portal Tabs for a specific Portal.
		/// </summary>
		protected const string const_proc_getallpagingbykeyenergycertificate = AppModule.Energy + "_EnergyCertificateCategoryGetAllPagingByKeyEnergyCertificate";


		/// <summary>
		/// The name of the stored procedure that saves a list of meters for a specific Portal.
		/// </summary>
		protected const string const_proc_energycertificatecategorysave = AppModule.Energy + "_EnergyCertificateEnergyCertificateCategorySave";

		/// <summary>
		/// The prefix of stored procedures of that provider. 
		/// </summary>
		protected override string const_proc_prefix {
			get {
				return AppModule.Energy;
			}
		}

		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, EnergyCertificateCategory item) {
			db.AddInParameterString(command, "Key", item.KeyEnergyCertificateCategory);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "LocalId", item.LocalId);
			db.AddInParameterString(command, "KeyEnergyCertificate", item.KeyEnergyCertificate);
			db.AddInParameterString(command, "Name", item.Name);
			db.AddInParameterDouble(command, "MinValue", item.MinValue);
			db.AddInParameterDouble(command, "MaxValue", item.MaxValue);
		}

		/// <summary>
		/// Populates a EnergyCertificateCategory business entity from a DataSourceDecorator.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated EnergyCertificateCategory business entity.</returns>
		protected override EnergyCertificateCategory FillObject(DataSourceDecorator source, string prefixColumn) {
			var retVal = new EnergyCertificateCategory {
				KeyEnergyCertificateCategory = source.Field<int>(prefixColumn + "KeyEnergyCertificateCategory").ToString(CultureInfo.InvariantCulture),
				KeyEnergyCertificate = source.Field<int>(prefixColumn + "KeyEnergyCertificate").ToString(CultureInfo.InvariantCulture),
				LocalId = source.Field<string>(prefixColumn + "LocalId"),
				Name = source.Field<string>(prefixColumn + "Name"),
				MinValue = source.Field<double?>(prefixColumn + "MinValue"),
				MaxValue = source.Field<double?>(prefixColumn + "MaxValue")
			};
			return retVal;
		}

		/// <summary>
		/// Get the list of saved EnergyCertificateCategory from a EnergyCertificate.
		/// </summary>
		/// <param name="KeyEnergyCertificate">The key of the portal window.</param>
		public List<EnergyCertificateCategory> GetListByKeyEnergyCertificate(string KeyEnergyCertificate) {
			List<EnergyCertificateCategory> results;
			DbCommand command = db.GetStoredProcCommand(const_proc_getlistbyenergycertificate);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyEnergyCertificate", KeyEnergyCertificate);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}

		/// <summary>
		/// Gets all the EnergyCertificateCategory for a specific Portal.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyEnergyCertificate">The Key of the portal window.</param>
		/// <param name="total">The total number of items.</param>
		/// <returns>The list of items.</returns>
		public List<EnergyCertificateCategory> GetAllPagingByKeyEnergyCertificate(PagingParameter paging, string KeyEnergyCertificate, out int total) {
			DbCommand command = db.GetStoredProcCommand(const_proc_getallpagingbykeyenergycertificate);
			command.CommandTimeout = 600;
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyEnergyCertificate", KeyEnergyCertificate);
			db.AddInParameterInt(command, "start", paging.start);
			db.AddInParameterInt(command, "limit", paging.limit);
			DataSet ds = db.ExecuteDataSet(command);
			if (ds.Tables[0].Rows.Count > 0)
				total = ds.Tables[0].Rows[0].Field<int>("TotalRecord");
			else
				total = 0;
			return FillObjects(ds);
		}

		/// <summary>
		/// Saves the portal EnergyCertificateCategorys.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="EnergyCertificateCategory">The EnergyCertificateCategory store.</param>
		/// <returns></returns>
		public List<EnergyCertificateCategory> SaveEnergyCertificateEnergyCertificateCategory(EnergyCertificate entity, CrudStore<EnergyCertificateCategory> EnergyCertificateCategory) {
			DbCommand command = db.GetStoredProcCommand(const_proc_energycertificatecategorysave);
			command.CommandTimeout = 600;
			List<EnergyCertificateCategory> results;

			if (EnergyCertificateCategory == null || EnergyCertificateCategory.IsEmpty())
				return null;
			string deletes = null;

			if (EnergyCertificateCategory.destroy != null) {
				deletes = Helper.BuildXml(EnergyCertificateCategory.destroy.Select(p => p.KeyEnergyCertificateCategory).ToArray());
			}

			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyEnergyCertificate", entity.KeyEnergyCertificate);
			db.AddInParameterString(command, "Inserts", null);
			db.AddInParameterString(command, "Deletes", deletes);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}
	}
}
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.DataLayer.BrokerDB {
	/// <summary>
	/// A Base Broker DB for DataAcquisitionEndpoint entities.
	/// </summary>
	/// <typeparam name="TEndpoint">The type of the endpoint.</typeparam>
	public abstract class DataAcquisitionEndpointBrokerDB<TEndpoint> : BaseBrokerDB<TEndpoint> where TEndpoint : DataAcquisitionEndpoint, new() {
		/// <summary>
		/// The name of the stored procedure that retreives a list of RESTDataAcquisitionEndpoint for a specific container.
		/// </summary>
		private static readonly string const_proc_getlistbydataacquisitioncontainer = AppModule.Energy + "_" + typeof(TEndpoint).Name + "GetListByKeyDataAcquisitionContainer";

		/// <summary>
		/// The name of the stored procedure that retreives a list of RESTDataAcquisitionEndpoint for a specific container.
		/// </summary>
		private static readonly string const_proc_getallpagingbykeydataacquisitioncontainer = AppModule.Energy + "_" + typeof(TEndpoint).Name + "GetAllPagingByKeyDataAcquisitionContainer";

		/// <summary>
		/// The prefix of stored procedures of that provider. 
		/// </summary>
		protected override string const_proc_prefix {
			get {
				return AppModule.Energy;
			}
		}

		/// <summary>
		/// Get the list of saved TEndpoint from a container
		/// </summary>
		/// <param name="KeyDataAcquisitionContainer">The key data acquisition container.</param>
		/// <returns></returns>
		public List<TEndpoint> GetListByKeyDataAcquisitionContainer(string KeyDataAcquisitionContainer) {
			List<TEndpoint> results;
			DbCommand command = db.GetStoredProcCommand(const_proc_getlistbydataacquisitioncontainer);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "keyDataAcquisitionContainer", KeyDataAcquisitionContainer);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}

		/// <summary>
		/// Gets all paging by key data acquisition container.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="KeyDataAcquisitionContainer">The key data acquisition container.</param>
		/// <param name="total">The total.</param>
		/// <returns></returns>
		public List<TEndpoint> GetAllPagingByKeyDataAcquisitionContainer(PagingParameter paging, string KeyDataAcquisitionContainer, out int total) {
			DbCommand command = db.GetStoredProcCommand(const_proc_getallpagingbykeydataacquisitioncontainer);
			command.CommandTimeout = 600;
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyDataAcquisitionContainer", KeyDataAcquisitionContainer);
			db.AddInParameterInt(command, "start", paging.start);
			db.AddInParameterInt(command, "limit", paging.limit);
			DataSet ds = db.ExecuteDataSet(command);
			if (ds.Tables[0].Rows.Count > 0)
				total = ds.Tables[0].Rows[0].Field<int>("TotalRecord");
			else
				total = 0;
			return FillObjects(ds);
		}


	}
}
﻿using Vizelia.FOL.BusinessEntities;
using System.Data;
using System.Data.Common;
using System.Web.Security;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.DataLayer.BrokerDB {
	/// <summary>
	/// Database broker for Site business entity.
	/// </summary>
	public class SiteBrokerDB : BaseBrokerDB<Site> {

		/// <summary>
		/// The name of the stored procedure that get the max level of sites in the spatial hierarchy.
		/// </summary>
		protected const string const_proc_getmaxlevel = AppModule.Core + "_SiteGetMaxLevel";


		/// <summary>
		/// Populates a Site business entity from a DataSourceDecorator.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated Site business entity.</returns>
		protected override Site FillObject(DataSourceDecorator source, string prefixColumn) {
			return new Site {
				KeySite = source.Field<string>(prefixColumn + "KeySite"),
				LocalId = source.Field<string>(prefixColumn + "LocalId"),
				ObjectType = source.Field<string>(prefixColumn + "ObjectType"),
				Name = source.Field<string>(prefixColumn + "Name"),
				Description = source.Field<string>(prefixColumn + "Description"),
				KeySiteParent = source.Field<string>(prefixColumn + "KeySiteParent")
			};
		}

		
		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, Site item) {
			db.AddInParameterString(command, "Key", item.KeySite);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeySiteParent", item.KeySiteParent);
			db.AddInParameterString(command, "ObjectType", item.ObjectType);
			db.AddInParameterString(command, "Name", item.Name);
			db.AddInParameterString(command, "Description", item.Description);
            db.AddInParameterBool(command, "IsMappingInProgress", base.IsAsynchronousSave);
			string localId = !string.IsNullOrWhiteSpace(item.LocalId) ? item.LocalId : null;
			db.AddInParameterString(command, "LocalId", localId);
		}


		/// <summary>
		///  Get the max level of site in the Spatial Hierarchy.
		/// </summary>
		/// <returns></returns>
		public int GetMaxLevel() {
			DbCommand command = db.GetStoredProcCommand(const_proc_getmaxlevel);
			command.CommandTimeout = 600;
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			DataSet ds = db.ExecuteDataSet(command);
			if (ds.Tables[0].Rows.Count > 0)
				return  ds.Tables[0].Rows[0].Field<int>("Level");
			else
				return  0;
		}


        /// <summary>
        /// Builds the additional delete params.
        /// </summary>
        /// <param name="command">The command.</param>
        protected override void BuildAdditionalDeleteParams(DbCommand command) {
            db.AddInParameterBool(command, "IsMappingInProgress", base.IsAsynchronousSave);
        }

	}
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Globalization;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.DataLayer.BrokerDB {

	/// <summary>
	/// Database broker for PsetAttributeHistorical business entity.
	/// </summary>
	public class PsetAttributeHistoricalBrokerDB : BaseBrokerDB<PsetAttributeHistorical> {

		/// <summary>
		/// The name of the stored procedure that retreives a list of PsetAttributeHistorical for a specific Object.
		/// </summary>
		protected const string const_proc_getlistbykeyobject = AppModule.Core + "_PsetAttributeHistoricalGetListByKeyObject";

		/// <summary>
		/// The name of the stored procedure that retreives a list of PsetAttributeHistorical for a specific meter.
		/// </summary>
		protected const string const_proc_getallpagingbykeyobject = AppModule.Core + "_PsetAttributeHistoricalGetAllPagingByKeyObject";

		/// <summary>
		/// The name of the stored procedure that retreives a list of meter data for a specific date range.
		/// </summary>
		protected const string const_proc_getlastbykeyobject = AppModule.Core + "_PsetAttributeHistoricalGetLastByKeyObject";

		/// <summary>
		/// The name of the stored procedure that retreives an item by its name.
		/// </summary>
		const string const_proc_getitembyname = AppModule.Core + "_PsetAttributeHistoricalGetItemByName";

		private const string const_proc_LatestHistoricalPsetGetValuesByLocationKey = AppModule.Core + "_LatestHistoricalPsetGetValuesByLocationKey";

		private const string const_proc_LatestHistoricalPsetGetValuesOfLocationsByPset = AppModule.Core + "_LatestHistoricalPsetGetValuesOfLocationsByPset";

		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, PsetAttributeHistorical item) {
			db.AddInParameterString(command, "Key", item.KeyPsetAttributeHistorical);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyPropertySingleValue", item.KeyPropertySingleValue);
			db.AddInParameterString(command, "KeyObject", item.KeyObject);
			db.AddInParameterDateTime(command, "AttributeAcquisitionDateTime", item.AttributeAcquisitionDateTime);
			db.AddInParameterDouble(command, "AttributeValue", item.AttributeValue);
			db.AddInParameterString(command, "AttributeComment", item.AttributeComment);
		}

		/// <summary>
		/// Populates a PsetAttributeHistorical business entity from a DataSourceDecorator.
		/// </summary>
		/// <param name="source">The data source.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated PsetAttributeHistorical business entity.</returns>
		protected override PsetAttributeHistorical FillObject(DataSourceDecorator source, string prefixColumn) {
			return new PsetAttributeHistorical {
				KeyPsetAttributeHistorical = source.Field<int>(prefixColumn + "KeyPsetAttributeHistorical").ToString(CultureInfo.InvariantCulture),
				KeyPropertySingleValue = source.Field<string>(prefixColumn + "KeyPropertySingleValue"),
				KeyObject = source.Field<string>(prefixColumn + "KeyObject"),
				AttributeAcquisitionDateTime = source.Field<DateTime>(prefixColumn + "AttributeAcquisitionDateTime"),
				AttributeValue = source.Field<double>(prefixColumn + "AttributeValue"),
				AttributeComment = source.Field<string>(prefixColumn + "AttributeComment")
			};
		}

		/// <summary>
		/// Gets the last  data by key object.
		/// </summary>
		/// <param name="keyObject">The key object.</param>
		/// <param name="keyPropertySingleValue">KeyPropertySingleValue.</param>
		/// <returns></returns>
		public PsetAttributeHistorical GetLastByKeyObject(string keyObject, string keyPropertySingleValue) {
			PsetAttributeHistorical data = default(PsetAttributeHistorical);
			DbCommand command = db.GetStoredProcCommand(const_proc_getlastbykeyobject);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyObject", keyObject);
			db.AddInParameterString(command, "KeyPropertySingleValue", keyPropertySingleValue);
			using (IDataReader reader = db.ExecuteReader(command)) {
				if (reader.Read()) {
					data = FillObject(reader);
				}
			}
			return data;
		}

		/// <summary>
		/// Gets all the PsetAttributeHistorical for a specific Object.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="keyObject">The key object.</param>
		/// <param name="keyPropertySingleValue">The KeyPropertySingleValue.</param>
		/// <param name="total">The total number of items.</param>
		/// <returns>
		/// The list of items.
		/// </returns>
		public List<PsetAttributeHistorical> GetAllPagingByKeyObject(PagingParameter paging, string keyObject, string keyPropertySingleValue, out int total) {
			DbCommand command = db.GetStoredProcCommand(const_proc_getallpagingbykeyobject);
			command.CommandTimeout = 600;
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyObject", keyObject);
			db.AddInParameterString(command, "KeyPropertySingleValue", keyPropertySingleValue);
			db.AddInParameterInt(command, "start", paging.start);
			db.AddInParameterInt(command, "limit", paging.limit);
			DataSet ds = db.ExecuteDataSet(command);
			if (ds.Tables[0].Rows.Count > 0)
				total = ds.Tables[0].Rows[0].Field<int>("TotalRecord");
			else
				total = 0;
			return FillObjects(ds);
		}

		/// <summary>
		/// Gets all the PsetAttributeHistorical entities..
		/// </summary>
		/// <returns></returns>
		public List<PsetAttributeHistorical> GetAll() {
			var retVal = GetAllNoPaging();
			return retVal;
		}

		/// <summary>
		/// Gets the item by local id.
		/// The override is necessary because of .read() in FillObjects that is redondant with .read() in sub method GetItemByLocalId.
		/// </summary>
		/// <param name="localId">The local id.</param>
		/// <returns></returns>
		public override PsetAttributeHistorical GetItemByLocalId(string localId) {
			PsetAttributeHistorical item = null;
			DbCommand command = db.GetStoredProcCommand(const_proc_getitembyname);
			var parameters = localId.Split(new[] { "PsetName=", "PsetAttributeName=", "LocationId=", "LocationTypeName=", "AcquisitionDateTime=" }, StringSplitOptions.RemoveEmptyEntries);
			var psetName = parameters[0];
			var psetAttributeName = parameters[1];
			var locationId = parameters[2];
			var locationTypeName = parameters[3];
			var acquisitionDatatime = DateTime.Parse(parameters[4]);
			db.AddInParameterString(command, "PsetName", psetName);
			db.AddInParameterString(command, "PsetAttributeName", psetAttributeName);
			db.AddInParameterString(command, "LocationId", locationId);
			db.AddInParameterString(command, "LocationTypeName", locationTypeName);
			db.AddInParameterDateTime(command, "AcquisitionDateTime", acquisitionDatatime);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			using (IDataReader reader = db.ExecuteReader(command)) {
				if (reader.Read()) {
					item = FillObject(reader);
				}
			}
			return item;
		}

		/// <summary>
		/// Return the LatestHistoricalPset values by location Key
		/// </summary>
		/// <param name="keyObject">The location key</param>
		/// <returns></returns>
		public List<LatestHistoricalPset> LatestHistoricalPsetGetValuesByLocationKey(string keyObject) {
			DbCommand command = db.GetStoredProcCommand(const_proc_LatestHistoricalPsetGetValuesByLocationKey);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyObject", keyObject);
			using (IDataReader reader = db.ExecuteReader(command)) {
				var results = FillLastestHistoricalPset(new DataReaderDecorator(reader));
				return results;
			}

		}

        /// <summary>
        /// LatestHistoricalPsetGetValuesOfLocationsByPset.
        /// </summary>
        /// <param name="KeyPropertySet">The key property set.</param>
        /// <returns></returns>
		public List<LatestHistoricalPset> LatestHistoricalPsetGetValuesOfLocationsByPset(string KeyPropertySet) {
			DbCommand command = db.GetStoredProcCommand(const_proc_LatestHistoricalPsetGetValuesOfLocationsByPset);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyPropertySet", KeyPropertySet);
			using (IDataReader reader = db.ExecuteReader(command)) {
				var results = FillLastestHistoricalPset(new DataReaderDecorator(reader));
				return results;
			}

		}

		private List<LatestHistoricalPset> FillLastestHistoricalPset(DataReaderDecorator source) {
			List<LatestHistoricalPset> results = new List<LatestHistoricalPset>();

			using (source) {
				while (source.Read()) {
					results.Add(new LatestHistoricalPset {
						AttributeAcquisitionDateTime = source.Field<DateTime>("AttributeAcquisitionDateTime"),
						AttributeMsgCode = source.Field<string>("AttributeMsgCode"),
						AttributeValue = source.Field<double>("AttributeValue"),
						PsetMsgCode = source.Field<string>("PsetMsgCode"),
						KeyObject = source.Field<string>("KeyObject"),
						ObjectName = source.Field<string>("ObjectName"),
						KeyPropertySet = source.Field<string>("KeyPropertySet")
					});
				}
			}

			return results;
		}
	}
}
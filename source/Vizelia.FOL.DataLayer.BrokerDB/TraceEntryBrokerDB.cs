﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Web.Security;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using System.Linq;

namespace Vizelia.FOL.DataLayer.BrokerDB {

	/// <summary>
	/// Database broker for TraceEntry business entity.
	/// </summary>
	public class TraceEntryBrokerDB : BaseBrokerDB<TraceEntry> {
		private const string const_proc_purge = AppModule.Core + "_TraceEntryPurge";
		private const string const_proc_getstoreoflastjobbykeyjob = AppModule.Core + "_TraceEntryGetStoreOfLastJobByKeyJob";
		private const string const_proc_getstorebyjobruninstancerequestid = AppModule.Core + "_TraceEntryGetStoreByJobRunInstanceRequestId";
        private const string const_proc_getstorebykeyjob = AppModule.Core + "_JobInstanceGetStoreByKeyJob";

		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, TraceEntry item) {
			throw new NotSupportedException();
		}


		/// <summary>
		/// Populates a TraceEntry business entity from a DataSourceDecorator.
		/// </summary>
		/// <param name="source">The data source.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated TraceEntry business entity.</returns>
		protected override TraceEntry FillObject(DataSourceDecorator source, string prefixColumn) {
			return new TraceEntry {
				KeyTraceEntry = source.Field<string>(prefixColumn + "KeyTraceEntry"),
				Timestamp = source.Field<DateTime>(prefixColumn + "Timestamp"),
				MachineName = source.Field<string>(prefixColumn + "MachineName"),
				Title = source.Field<string>(prefixColumn + "Title"),
				Priority = source.Field<int>(prefixColumn + "Priority"),
				ProcessID = source.Field<string>(prefixColumn + "ProcessID"),
				ProcessName = source.Field<string>(prefixColumn + "ProcessName"),
				RelatedKeyEntity = source.Field<string>(prefixColumn + "RelatedKeyEntity"),
				Message = source.Field<string>(prefixColumn + "Message"),
				MethodName = source.Field<string>(prefixColumn + "MethodName"),
				Severity = source.Field<string>(prefixColumn + "Severity").ParseAsEnum<TraceEntrySeverity>(),
				TraceCategory = source.Field<string>(prefixColumn + "TraceCategory"),
				RequestId = source.Field<string>(prefixColumn + "RequestId"),
				JobRunInstanceRequestId = source.Field<string>(prefixColumn + "JobRunInstanceRequestId"),
				KeyJob = source.Field<string>(prefixColumn + "KeyJob")
			};
		}

		/// <summary>
		/// Purges the server logs older than the specified days to keep logs.
		/// </summary>
		/// <param name="daysToKeepLogs">The days to keep logs.</param>
		public void Purge(int daysToKeepLogs) {
			try {
				DbCommand command = db.GetStoredProcCommand(const_proc_purge);
				command.CommandTimeout = 60*30; // 30 minutes.
				db.AddInParameterInt(command, "@DaysToPurge", daysToKeepLogs);
				db.AddInParameterString(command, "Application", Membership.ApplicationName);
				db.ExecuteNonQuery(command);
			}
			catch (SqlException ex) {
				throw SqlExceptionWrap(ex);
			}

		}
		
		/// <summary>
		/// Gets the store of last job run by key job.
		/// </summary>
		/// <param name="jobKeys">The key job.</param>
        public Dictionary<string, List<TraceEntry>> GetLastJobRunByKeys(IEnumerable<string> jobKeys)
        {
            try
            {
                DbCommand command = db.GetStoredProcCommand(const_proc_getstoreoflastjobbykeyjob);
                command.CommandTimeout = 60 * 5;

                db.AddInParameterXml(command, "SourceKeys", Helper.BuildXml(jobKeys.ToArray()));
                db.AddInParameterString(command, "Application", Membership.ApplicationName);

                var retVal = new Dictionary<string, List<TraceEntry>>();
                using (IDataReader reader = db.ExecuteReader(command))
                {
                    while (reader.Read())
                    {
                        TraceEntry newRow = FillObject(reader);
                        if (!retVal.ContainsKey(newRow.KeyJob))
                        {
                            retVal[newRow.KeyJob] = new List<TraceEntry>();
                        }
                        retVal[newRow.KeyJob].Add(newRow);
                    }
                }
                return retVal;
            }
            catch (SqlException ex)
            {
                throw SqlExceptionWrap(ex);
            }

        }
		/// <summary>
		/// Gets the store by job run instance request id.
		/// </summary>
		/// <param name="jobRunInstanceRequestId">The job run instance request id.</param>
		/// <returns></returns>
		public JsonStore<TraceEntry> GetStoreByJobRunInstanceRequestId(string jobRunInstanceRequestId) {
			try {
				DbCommand command = db.GetStoredProcCommand(const_proc_getstorebyjobruninstancerequestid);
				command.CommandTimeout = 60 * 5;
				db.AddInParameterString(command, "@JobRunInstanceRequestId", jobRunInstanceRequestId);
				db.AddInParameterString(command, "Application", Membership.ApplicationName);

				int count = 0;
				var retVal = new List<TraceEntry>();
				using (IDataReader reader = db.ExecuteReader(command)) {
					while (reader.Read()) {
						count++;
						retVal.Add(FillObject(reader));
					}
				}
				return retVal.ToJsonStore(count);
			}
			catch (SqlException ex) {
				throw SqlExceptionWrap(ex);
			}
		}


        /// <summary>
        /// Gets a by job run instance request id. Fills the object for job instance. The job instance is really just an object that keeps an aggregation of TraceEntry based 
        /// on a specific Job
        /// </summary>
        /// <param name="paging"></param>
        /// <param name="keyJob">The job run instance request id.</param>
        /// <returns></returns>
        public JsonStore<JobInstance> GetStoreByKeyJob(PagingParameter paging, string keyJob)
        {
            try
            {
                DbCommand command = db.GetStoredProcCommand(const_proc_getstorebykeyjob);
                command.CommandTimeout = 60 * 5;
                db.AddInParameterString(command, "@KeyJob", keyJob);
                db.AddInParameterString(command, "@Application", Membership.ApplicationName);

                db.AddInParameterString(command, "@Sort", paging.sort.SafeSqlLiteral());
                db.AddInParameterInt(command, "@Start", paging.start);
                db.AddInParameterInt(command, "@Limit", paging.limit);
                db.AddInParameterString(command, "@Direction", paging.dir.SafeSqlLiteral());
                db.AddOutParameterInt(command, "@Count");

                var retVal = new List<JobInstance>();
                using (IDataReader reader = db.ExecuteReader(command))
                {
                    while (reader.Read())
                    {
                        retVal.Add(FillObjectForJobInstance(new DataReaderDecorator(reader)));
                    }
                }
                int result = Convert.ToInt32(db.GetParameterValue(command, "@Count"));
                return retVal.ToJsonStore(result);
            }
            catch (SqlException ex)
            {
                throw SqlExceptionWrap(ex);
            }
        }

        /// <summary>
        /// Fills the object for job instance. The job instance is really just an object that keeps an aggregation of TraceEntry based 
        /// on a specific Job
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="prefixColumn">The prefix column.</param>
        /// <returns></returns>
        protected JobInstance FillObjectForJobInstance(DataSourceDecorator source, string prefixColumn = "")
        {
            return new JobInstance
            {
                EndDate = source.Field<DateTime>(prefixColumn + "EndDate"),
                StartDate = source.Field<DateTime>(prefixColumn + "StartDate"),
                JobRunInstanceRequestId = source.Field<string>(prefixColumn + "JobRunInstanceRequestId"),
                KeyJobInstance = source.Field<long>(prefixColumn + "rownum").ToString(),
                Severity = source.Field<string>(prefixColumn + "Severity").ParseAsEnum<TraceEntrySeverity>()
            };
        }
        
	}
}
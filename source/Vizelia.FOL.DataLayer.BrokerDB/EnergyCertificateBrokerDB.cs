﻿using Vizelia.FOL.BusinessEntities;
using System.Data.Common;
using System.Web.Security;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.DataLayer.BrokerDB {
	/// <summary>
	/// Database broker for EnergyCertificate business entity.
	/// </summary>
	public class EnergyCertificateBrokerDB : BaseBrokerDB<EnergyCertificate> {
		
		/// <summary>
		/// Gets the const_proc_prefix.
		/// </summary>
		/// <value>The const_proc_prefix.</value>
		protected override string const_proc_prefix {
			get {
				return AppModule.Energy;
			}
		}


		/// <summary>
		/// Populates a EnergyCertificate business entity from a DataSourceDecorator.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated EnergyCertificate business entity.</returns>
		protected override EnergyCertificate FillObject(DataSourceDecorator source, string prefixColumn) {
			return new EnergyCertificate {
				KeyEnergyCertificate = source.Field<int>(prefixColumn + "KeyEnergyCertificate").ToString(),
				LocalId = source.Field<string>(prefixColumn + "LocalId"),
				Name = source.Field<string>(prefixColumn + "Name"),
				StartColor = source.Field<string>(prefixColumn + "StartColor"),
				EndColor = source.Field<string>(prefixColumn + "EndColor"),			
			};
		}

		

		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, EnergyCertificate item) {
			db.AddInParameterString(command, "Key", item.KeyEnergyCertificate);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "LocalId", item.LocalId);
			db.AddInParameterString(command, "Name", item.Name);
			db.AddInParameterString(command, "StartColor", item.StartColor);
			db.AddInParameterString(command, "EndColor", item.EndColor);
		}

	
	}
}

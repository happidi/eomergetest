﻿using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.DataLayer.BrokerDB {

	/// <summary>
	/// Database broker for ChartCustomGridCell business entity.
	/// </summary>
	public class ChartCustomGridCellBrokerDB : BaseBrokerDB<ChartCustomGridCell> {

		/// <summary>
		/// The name of the stored procedure that retreives a list of ChartCustomGridCell for a specific chart.
		/// </summary>
		protected const string const_proc_getlistbychart = AppModule.Energy + "_ChartCustomGridCellGetListByKeyChart";

		/// <summary>
		/// The name of the stored procedure that retreives a list of ChartCustomGridCell for a specific chart.
		/// </summary>
		protected const string const_proc_getallpagingbykeychart = AppModule.Energy + "_ChartCustomGridCellGetAllPagingByKeyChart";

		/// <summary>
		/// The name of the stored procedure that saves a list of ChartCustomGridCell for a specific chart.
		/// </summary>
		protected const string const_proc_chartcustomgridcellsave = AppModule.Energy + "_ChartChartCustomGridCellSave";

		/// <summary>
		/// The prefix of stored procedures of that provider. 
		/// </summary>
		protected override string const_proc_prefix {
			get {
				return AppModule.Energy;
			}
		}

		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, ChartCustomGridCell item) {
			if (item.Tag == null)
				item.Tag = string.Empty;
			db.AddInParameterString(command, "Key", item.KeyChartCustomGridCell);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyChart", item.KeyChart);
			db.AddInParameterInt(command, "ColumnIndex", item.ColumnIndex);
			db.AddInParameterInt(command, "RowIndex", item.RowIndex);
			db.AddInParameterString(command, "Tag", item.Tag);
		}

		/// <summary>
		/// Populates a ChartCustomGridCell business entity from a DataSourceDecorator.
		/// </summary>
		/// <param name="source">The data source.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated ChartCustomGridCell business entity.</returns>
		protected override ChartCustomGridCell FillObject(DataSourceDecorator source, string prefixColumn) {
			return new ChartCustomGridCell {
				KeyChartCustomGridCell = source.Field<int>(prefixColumn + "KeyChartCustomGridCell").ToString(CultureInfo.InvariantCulture),
				KeyChart = source.Field<int>(prefixColumn + "KeyChart").ToString(CultureInfo.InvariantCulture),
				ColumnIndex = source.Field<int>(prefixColumn + "ColumnIndex"),
				RowIndex = source.Field<int>(prefixColumn + "RowIndex"),
				Tag = source.Field<string>(prefixColumn + "Tag"),
			};
		}

		/// <summary>
		/// Get the list of saved ChartCustomGridCell from a Chart
		/// </summary>
		/// <param name="KeyChart">The key of the chart.</param>
		public List<ChartCustomGridCell> GetListByKeyChart(string KeyChart) {
			List<ChartCustomGridCell> results;
			DbCommand command = db.GetStoredProcCommand(const_proc_getlistbychart);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyChart", KeyChart);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}

		/// <summary>
		/// Gets all the ChartCustomGridCell for a specific chart.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyChart">The Key of the chart.</param>
		/// <param name="total">The total number of items.</param>
		/// <returns>The list of items.</returns>
		public List<ChartCustomGridCell> GetAllPagingByKeyChart(PagingParameter paging, string KeyChart, out int total) {
			DbCommand command = db.GetStoredProcCommand(const_proc_getallpagingbykeychart);
			command.CommandTimeout = 600;
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyChart", KeyChart);
			db.AddInParameterInt(command, "start", paging.start);
			db.AddInParameterInt(command, "limit", paging.limit);
			DataSet ds = db.ExecuteDataSet(command);
			if (ds.Tables[0].Rows.Count > 0)
				total = ds.Tables[0].Rows[0].Field<int>("TotalRecord");
			else
				total = 0;
			return FillObjects(ds);
		}

		/// <summary>
		/// Saves the chart ChartCustomGridCell.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="chartcustomgridcell">The chartcustomgridcell.</param>
		/// <returns></returns>
		public List<ChartCustomGridCell> SaveChartChartCustomGridCell(Chart entity, CrudStore<ChartCustomGridCell> chartcustomgridcell) {
			DbCommand command = db.GetStoredProcCommand(const_proc_chartcustomgridcellsave);
			command.CommandTimeout = 600;
			List<ChartCustomGridCell> results;

			if (chartcustomgridcell == null || chartcustomgridcell.IsEmpty())
				return null;
			string inserts = null;
			string deletes = null;


			if (chartcustomgridcell.destroy != null) {
				deletes = Helper.BuildXml(chartcustomgridcell.destroy.Select(p => p.KeyChartCustomGridCell).ToArray());
			}

			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyChart", entity.KeyChart);
			db.AddInParameterString(command, "Inserts", inserts);
			db.AddInParameterString(command, "Deletes", deletes);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}
	}
}
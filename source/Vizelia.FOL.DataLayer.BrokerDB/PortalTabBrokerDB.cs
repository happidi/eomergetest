﻿using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.DataLayer.BrokerDB {

	/// <summary>
	/// Database broker for PortalTab business entity.
	/// </summary>
	public class PortalTabBrokerDB : BaseBrokerDB<PortalTab> {

		/// <summary>
		/// The name of the stored procedure that retreives a list of Portal Tabs for a specific Portal.
		/// </summary>
		protected const string const_proc_getlistbyportalwindow = AppModule.Energy + "_PortalTabGetListByKeyPortalWindow";

		/// <summary>
		/// The name of the stored procedure that retreives a list of Portal Tabs for a specific Portal.
		/// </summary>
		protected const string const_proc_getallpagingbykeyportalwindow = AppModule.Energy + "_PortalTabGetAllPagingByKeyPortalWindow";

		/// <summary>
		/// The name of the stored procedure that saves a list of meters for a specific Portal.
		/// </summary>
		protected const string const_proc_portaltabsave = AppModule.Energy + "_PortalWindowPortalTabSave";

		/// <summary>
		/// The prefix of stored procedures of that provider. 
		/// </summary>
		protected override string const_proc_prefix {
			get {
				return AppModule.Energy;
			}
		}

		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, PortalTab item) {
			db.AddInParameterString(command, "Key", item.KeyPortalTab);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "LocalId", item.LocalId);
			db.AddInParameterString(command, "KeyPortalWindow", item.KeyPortalWindow);
			db.AddInParameterString(command, "Title", item.Title);
			db.AddInParameterInt(command, "Order", item.Order);
			db.AddInParameterString(command, "BackgroundCls", item.BackgroundCls);
			db.AddInParameterString(command, "ColumnConfig", item.ColumnConfig.ParseAsString());
			db.AddInParameterInt(command, "HeaderHeight", item.HeaderHeight);
			db.AddInParameterInt(command, "FooterHeight", item.FooterHeight);
		}

		/// <summary>
		/// Populates a PortalTab business entity from a DataSourceDecorator.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated PortalTab business entity.</returns>
		protected override PortalTab FillObject(DataSourceDecorator source, string prefixColumn) {
			return new PortalTab {
				KeyPortalTab = source.Field<int>(prefixColumn + "KeyPortalTab").ToString(CultureInfo.InvariantCulture),
				KeyPortalWindow = source.Field<int>(prefixColumn + "KeyPortalWindow").ToString(CultureInfo.InvariantCulture),
				Title = source.Field<string>(prefixColumn + "Title"),
				LocalId = source.Field<string>(prefixColumn + "LocalId"),
				PortalWindowTitle = source.Field<string>(prefixColumn + "PortalWindowTitle"),
				PortalWindowIconCls = source.Field<string>(prefixColumn + "PortalWindowIconCls"),
				PortalWindowKeyImage = source.Field<string>(prefixColumn + "PortalWindowKeyImage"),
				Order = source.Field<int>(prefixColumn + "Order"),
				BackgroundCls = source.Field<string>(prefixColumn + "BackgroundCls"),
				ColumnConfig = source.Field<string>(prefixColumn + "ColumnConfig").ParseAsEnum<PortalTabColumnConfig>(),
				HeaderHeight = source.Field<int?>(prefixColumn + "HeaderHeight"),
				FooterHeight = source.Field<int?>(prefixColumn + "FooterHeight"),
			};
		}



		/// <summary>
		/// Get the list of saved PortalTab from a Portal
		/// </summary>
		/// <param name="KeyPortalWindow">The key of the portal window.</param>
		public List<PortalTab> GetListByKeyPortalWindow(string KeyPortalWindow) {
			List<PortalTab> results;
			DbCommand command = db.GetStoredProcCommand(const_proc_getlistbyportalwindow);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyPortalWindow", KeyPortalWindow);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}

		/// <summary>
		/// Gets all the PortalTab for a specific Portal.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyPortalWindow">The Key of the portal window.</param>
		/// <param name="total">The total number of items.</param>
		/// <returns>The list of items.</returns>
		public List<PortalTab> GetAllPagingByKeyPortalWindow(PagingParameter paging, string KeyPortalWindow, out int total) {
			DbCommand command = db.GetStoredProcCommand(const_proc_getallpagingbykeyportalwindow);
			command.CommandTimeout = 600;
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyPortalWindow", KeyPortalWindow);
			db.AddInParameterInt(command, "start", paging.start);
			db.AddInParameterInt(command, "limit", paging.limit);
			DataSet ds = db.ExecuteDataSet(command);
			if (ds.Tables[0].Rows.Count > 0)
				total = ds.Tables[0].Rows[0].Field<int>("TotalRecord");
			else
				total = 0;
			return FillObjects(ds);
		}


		/// <summary>
		/// Saves the portal PortalTabs.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="portaltab">The PortalTab store.</param>
		/// <returns></returns>
		public List<PortalTab> SavePortalPortalTab(PortalWindow entity, CrudStore<PortalTab> portaltab) {
			DbCommand command = db.GetStoredProcCommand(const_proc_portaltabsave);
			command.CommandTimeout = 600;
			List<PortalTab> results;

			if (portaltab == null || portaltab.IsEmpty())
				return null;
			string inserts = null;
			string deletes = null;

			/*if (dataseries.create != null) {
				inserts = Helper.BuildXml(dataseries.create.Select(p => p.KeyDataSerie).ToArray());
			}*/

			if (portaltab.destroy != null) {
				deletes = Helper.BuildXml(portaltab.destroy.Select(p => p.KeyPortalTab).ToArray());
			}

			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyPortalWindow", entity.KeyPortalWindow);
			db.AddInParameterString(command, "Inserts", inserts);
			db.AddInParameterString(command, "Deletes", deletes);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}
	}
}
﻿using System.Data.Common;
using System.Globalization;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.DataLayer.BrokerDB {

	/// <summary>
	/// Database broker for RESTDataAcquisitionContainer business entity.
	/// </summary>
	public class RESTDataAcquisitionContainerBrokerDB : BaseBrokerDB<RESTDataAcquisitionContainer> {

		/// <summary>
		/// The prefix of stored procedures of that provider. 
		/// </summary>
		protected override string const_proc_prefix {
			get {
				return AppModule.Energy;
			}
		}


		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, RESTDataAcquisitionContainer item) {
			db.AddInParameterString(command, "Key", item.KeyDataAcquisitionContainer);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "LocalId", item.LocalId);
			db.AddInParameterString(command, "Title", item.Title);
			db.AddInParameterString(command, "Url", item.Url);
			db.AddInParameterString(command, "ProviderType", item.ProviderType);
			db.AddInParameterString(command, "XPathMeterId", item.XPathMeterId);
			db.AddInParameterString(command, "XPathAcquisitionDateTime", item.XPathAcquisitionDateTime);
			db.AddInParameterString(command, "XPathValue", item.XPathValue);
			db.AddInParameterString(command, "AttributeAcquisitionDateTime", item.AttributeAcquisitionDateTime);
			db.AddInParameterString(command, "AttributeMeterId", item.AttributeMeterId);
			db.AddInParameterString(command, "AttributeValue", item.AttributeValue);
			db.AddInParameterString(command, "FormatAcquisitionDateTime", item.FormatAcquisitionDateTime);
			db.AddInParameterString(command, "KeyLocalizationCulture", item.KeyLocalizationCulture);
			db.AddInParameterString(command, "CustomData", item.CustomData);

		}


		/// <summary>
		/// Populates a RESTDataAcquisitionContainer business entity from a DataSourceDecorator.
		/// </summary>
		/// <param name="source">The data source.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated RESTDataAcquisitionContainer business entity.</returns>
		protected override RESTDataAcquisitionContainer FillObject(DataSourceDecorator source, string prefixColumn) {
			var container = new RESTDataAcquisitionContainer {
				KeyDataAcquisitionContainer = source.Field<int>(prefixColumn + "KeyDataAcquisitionContainer").ToString(CultureInfo.InvariantCulture),
				LocalId = source.Field<string>(prefixColumn + "LocalId"),
				Title = source.Field<string>(prefixColumn + "Title"),
				Url = source.Field<string>(prefixColumn + "Url"),
				ProviderType = source.Field<string>(prefixColumn + "ProviderType"),
				XPathMeterId = source.Field<string>(prefixColumn + "XPathMeterId"),
				XPathAcquisitionDateTime = source.Field<string>(prefixColumn + "XPathAcquisitionDateTime"),
				XPathValue = source.Field<string>(prefixColumn + "XPathValue"),
				AttributeAcquisitionDateTime = source.Field<string>(prefixColumn + "AttributeAcquisitionDateTime"),
				AttributeMeterId = source.Field<string>(prefixColumn + "AttributeMeterId"),
				AttributeValue = source.Field<string>(prefixColumn + "AttributeValue"),
				FormatAcquisitionDateTime = source.Field<string>(prefixColumn + "FormatAcquisitionDateTime"),
				KeyLocalizationCulture = source.Field<string>(prefixColumn + "KeyLocalizationCulture"),
				CustomData = source.Field<string>(prefixColumn + "CustomData"),
			};
			DataAcquisitionService.FillObject(container);
			return container;

		}
	}
}
﻿using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.DataLayer.BrokerDB {

	/// <summary>
	/// Database broker for PaletteColor business entity.
	/// </summary>
	public class PaletteColorBrokerDB : BaseBrokerDB<PaletteColor> {

		/// <summary>
		/// The name of the stored procedure that retreives a list of Portal Tabs for a specific Portal.
		/// </summary>
		protected const string const_proc_getlistbypalette = AppModule.Energy + "_PaletteColorGetListByKeyPalette";

		/// <summary>
		/// The name of the stored procedure that retreives a list of Portal Tabs for a specific Portal.
		/// </summary>
		protected const string const_proc_getallpagingbykeypalette = AppModule.Energy + "_PaletteColorGetAllPagingByKeyPalette";


		/// <summary>
		/// The name of the stored procedure that saves a list of meters for a specific Portal.
		/// </summary>
		protected const string const_proc_palettecolorsave = AppModule.Energy + "_PalettePaletteColorSave";

		/// <summary>
		/// The prefix of stored procedures of that provider. 
		/// </summary>
		protected override string const_proc_prefix {
			get {
				return AppModule.Energy;
			}
		}


		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, PaletteColor item) {
			db.AddInParameterString(command, "Key", item.KeyPaletteColor);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "LocalId", item.LocalId);
			db.AddInParameterString(command, "KeyPalette", item.KeyPalette);
			db.AddInParameterInt(command, "ColorR", item.ColorR);
			db.AddInParameterInt(command, "ColorG", item.ColorG);
			db.AddInParameterInt(command, "ColorB", item.ColorB);
			db.AddInParameterInt(command, "Order", item.Order);
		}

		/// <summary>
		/// Populates a PaletteColor business entity from a DataSourceDecorator.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated PaletteColor business entity.</returns>
		protected override PaletteColor FillObject(DataSourceDecorator source, string prefixColumn) {
			return new PaletteColor {
				KeyPaletteColor = source.Field<int>(prefixColumn + "KeyPaletteColor").ToString(CultureInfo.InvariantCulture),
				LocalId = source.Field<string>(prefixColumn + "LocalId"),
				KeyPalette = source.Field<int>(prefixColumn + "KeyPalette").ToString(CultureInfo.InvariantCulture),
				ColorR = source.Field<int>(prefixColumn + "ColorR"),
				ColorG = source.Field<int>(prefixColumn + "ColorG"),
				ColorB = source.Field<int>(prefixColumn + "ColorB"),
				Order = source.Field<int>(prefixColumn + "Order")
			};
		}


		/// <summary>
		/// Get the list of saved PaletteColor from a Playlist
		/// </summary>
		/// <param name="KeyPalette">The key of the portal window.</param>
		public List<PaletteColor> GetListByKeyPalette(string KeyPalette) {
			List<PaletteColor> results;
			DbCommand command = db.GetStoredProcCommand(const_proc_getlistbypalette);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyPalette", KeyPalette);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}

		/// <summary>
		/// Gets all the PaletteColor for a specific Portal.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyPalette">The Key of the portal window.</param>
		/// <param name="total">The total number of items.</param>
		/// <returns>The list of items.</returns>
		public List<PaletteColor> GetAllPagingByKeyPalette(PagingParameter paging, string KeyPalette, out int total) {
			DbCommand command = db.GetStoredProcCommand(const_proc_getallpagingbykeypalette);
			command.CommandTimeout = 600;
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyPalette", KeyPalette);
			db.AddInParameterInt(command, "start", paging.start);
			db.AddInParameterInt(command, "limit", paging.limit);
			DataSet ds = db.ExecuteDataSet(command);
			if (ds.Tables[0].Rows.Count > 0)
				total = ds.Tables[0].Rows[0].Field<int>("TotalRecord");
			else
				total = 0;
			return FillObjects(ds);
		}


		/// <summary>
		/// Saves the portal PaletteColors.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="palettecolor">The PaletteColor store.</param>
		/// <returns></returns>
		public List<PaletteColor> SavePalettePaletteColor(Palette entity, CrudStore<PaletteColor> palettecolor) {
			DbCommand command = db.GetStoredProcCommand(const_proc_palettecolorsave);
			command.CommandTimeout = 600;
			List<PaletteColor> results;

			if (palettecolor == null || palettecolor.IsEmpty())
				return null;
			string inserts = null;
			string deletes = null;

			/*if (dataseries.create != null) {
				inserts = Helper.BuildXml(dataseries.create.Select(p => p.KeyDataSerie).ToArray());
			}*/

			if (palettecolor.destroy != null) {
				deletes = Helper.BuildXml(palettecolor.destroy.Select(p => p.KeyPaletteColor).ToArray());
			}

			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyPalette", entity.KeyPalette);
			db.AddInParameterString(command, "Inserts", inserts);
			db.AddInParameterString(command, "Deletes", deletes);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}
	}
}
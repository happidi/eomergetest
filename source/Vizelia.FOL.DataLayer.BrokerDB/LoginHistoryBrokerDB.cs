﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Web.Security;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Vizelia.FOL.DataLayer.Interfaces;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.DataLayer.BrokerDB {

	/// <summary>
	/// Database broker for Login history entity.
	/// </summary>
	public class LoginHistoryBrokerDB : BaseBrokerDB<LoginHistory> {
		/// <summary>
		/// The name of the stored procedure that finds all workflow action elements associated to a specific mail.
		/// </summary>
		protected const string const_proc_cleanloginhistory = AppModule.Membership + "_CleanLoginHistory";


		/// <summary>
		/// The prefix of stored procedures of that provider. 
		/// </summary>
		protected override string const_proc_prefix {
			get {
				return AppModule.Membership;
			}
		}

		/// <summary>
		/// Populates a LoginHistory business entity from a DataSourceDecorator.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated LoginHistory business entity.</returns>
		protected override LoginHistory FillObject(DataSourceDecorator source, string prefixColumn) {
			return new LoginHistory {
				KeyLoginHistory = source.Field<int>(prefixColumn + "KeyLoginHistory").ToString(),
				UserName = source.Field<string>(prefixColumn + "UserName"),
				IP = source.Field<string>(prefixColumn + "IP"),
				LoginDate = source.Field<DateTimeOffset?>(prefixColumn + "LoginDate").ToDateTime(),
				LogoutDate = source.Field<DateTimeOffset?>(prefixColumn + "LogoutDate").ToDateTime()
			};

		}

		/// <summary>
		/// Gets all the items from database into memory. 
		/// Uses const_proc_getall proc to retreive the items.
		/// </summary>
		/// <returns>The list of items.</returns>
		protected override List<LoginHistory> GetAllNoPaging() {
			CleanLoginHistory();
			return base.GetAllNoPaging();
		}

		/// <summary>
		/// Gets all the items from the server.
		/// Uses the generic paging proc to retreive the items. The name of this proc can be overrided if necessary.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="total">The total number of items.</param>
		/// <returns>The list of items.</returns>
		protected override List<LoginHistory> GetAllGenericPaging(PagingParameter paging, out int total) {
			CleanLoginHistory();
			return base.GetAllGenericPaging(paging, out total);
		}

		/// <summary>
		/// Cleans the login history.
		/// </summary>
		private void CleanLoginHistory() {
			string[] activeSessions = SessionService.GetActiveSessions();
			DbCommand command = db.GetStoredProcCommand(const_proc_cleanloginhistory);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterXml(command, "Keys", Helper.BuildXml(activeSessions));
			db.ExecuteNonQuery(command);
		}


		/// <summary>
		/// Builds the save params.
		/// </summary>
		/// <param name="command">The command.</param>
		/// <param name="item">The item.</param>
		protected override void BuildSaveParams(DbCommand command, LoginHistory item) {
			// this entity is read only
			throw new NotImplementedException();
		}
	}
}
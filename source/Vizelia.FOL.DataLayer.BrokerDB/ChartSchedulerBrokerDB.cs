﻿using System;
using System.Data.Common;
using System.Globalization;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.DataLayer.BrokerDB {

	/// <summary>
	/// Database broker for ChartScheduler business entity.
	/// </summary>
	public class ChartSchedulerBrokerDB : BaseBrokerDB<ChartScheduler> {

		/// <summary>
		/// The prefix of stored procedures of that provider. 
		/// </summary>
		protected override string const_proc_prefix {
			get {
				return AppModule.Energy;
			}
		}

		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, ChartScheduler item) {
			db.AddInParameterString(command, "Key", item.KeyChartScheduler);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "LocalId", item.LocalId);
			db.AddInParameterString(command, "Title", item.Title);
			db.AddInParameterString(command, "Emails", item.Emails);
			db.AddInParameterString(command, "ExportType", item.ExportType.ParseAsString());
			db.AddInParameterString(command, "Body", item.Body);
			db.AddInParameterString(command, "Subject", item.Subject);
			db.AddInParameterString(command, "KeyLocalizationCulture", item.KeyLocalizationCulture);
			db.AddInParameterBool(command, "Enabled", item.Enabled);
			db.AddInParameterString(command, "TimeZoneId", item.TimeZoneId);
			string iCalData = item.ToiCalData();
			db.AddInParameterString(command, "iCalData", iCalData);
		}

		/// <summary>
		/// Populates a ChartScheduler business entity from a DataSourceDecorator.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated ChartScheduler business entity.</returns>
		protected override ChartScheduler FillObject(DataSourceDecorator source, string prefixColumn) {
			CalendarEvent ev = null;
			DateTime? nextOccurrenceDate = null;
			var iCalData = source.Field<string>(prefixColumn + "iCalData");
			var timeZoneId = source.Field<string>(prefixColumn + "TimeZoneId");
			if (string.IsNullOrEmpty(iCalData) == false) {
				ev = iCalData.ToCalendarEvent(timeZoneId, out nextOccurrenceDate);
				ev.TimeZoneId = timeZoneId;
			}

			return new ChartScheduler(ev) {
				KeyChartScheduler = source.Field<int>(prefixColumn + "KeyChartScheduler").ToString(CultureInfo.InvariantCulture),
				LocalId = source.Field<string>(prefixColumn + "LocalId"),
				Title = source.Field<string>(prefixColumn + "Title"),
				Emails = source.Field<string>(prefixColumn + "Emails"),
				ExportType = source.Field<string>(prefixColumn + "ExportType").ParseAsEnum<ExportType>(),
				NextOccurenceDate = nextOccurrenceDate,
				Subject = source.Field<string>(prefixColumn + "Subject"),
				Body = source.Field<string>(prefixColumn + "Body"),
				KeyLocalizationCulture = source.Field<string>(prefixColumn + "KeyLocalizationCulture"),
				Enabled = source.Field<bool?>(prefixColumn + "Enabled") ?? true,
				TimeZoneId = source.Field<string>(prefixColumn + "TimeZoneId")
			};
		}

	}
}

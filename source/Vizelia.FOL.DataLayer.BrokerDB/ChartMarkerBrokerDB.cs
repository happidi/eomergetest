﻿using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.DataLayer.BrokerDB {

	/// <summary>
	/// Database broker for ChartMarker business entity.
	/// </summary>
	public class ChartMarkerBrokerDB : BaseBrokerDB<ChartMarker> {

		/// <summary>
		/// The name of the stored procedure that retreives a list of ChartMarkers for a specific chart.
		/// </summary>
		protected const string const_proc_getlistbychart = AppModule.Energy + "_ChartMarkerGetListByKeyChart";

		/// <summary>
		/// The name of the stored procedure that retreives a list of ChartMarkers for a specific chartaxis.
		/// </summary>
		protected const string const_proc_getlistbychartaxis = AppModule.Energy + "_ChartMarkerGetListByKeyChartAxis";

		/// <summary>
		/// The name of the stored procedure that retreives a list of meters for a specific chart.
		/// </summary>
		protected const string const_proc_getallpagingbykeychart = AppModule.Energy + "_ChartMarkerGetAllPagingByKeyChart";

		/// <summary>
		/// The name of the stored procedure that saves a list of meters for a specific chart.
		/// </summary>
		protected const string const_proc_chartchartmarkerssave = AppModule.Energy + "_ChartChartMarkersSave";

		/// <summary>
		/// Gets the const_proc_prefix.
		/// </summary>
		/// <value>The const_proc_prefix.</value>
		protected override string const_proc_prefix {
			get {
				return AppModule.Energy;
			}
		}

		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, ChartMarker item) {
			db.AddInParameterString(command, "Key", item.KeyChartMarker);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "LocalId", item.LocalId);
			db.AddInParameterString(command, "KeyChartAxis", item.KeyChartAxis);
			db.AddInParameterString(command, "Name", item.Name);
			db.AddInParameterDouble(command, "Min", item.Min);
			db.AddInParameterDouble(command, "Max", item.Max);
			db.AddInParameterInt(command, "ColorR", item.ColorR);
			db.AddInParameterInt(command, "ColorG", item.ColorG);
			db.AddInParameterInt(command, "ColorB", item.ColorB);
		}

		/// <summary>
		/// Populates a ChartMarker business entity from a DataSourceDecorator.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated ChartMarker business entity.</returns>
		protected override ChartMarker FillObject(DataSourceDecorator source, string prefixColumn) {
			return new ChartMarker {
				KeyChartMarker = source.Field<int>(prefixColumn + "KeyChartMarker").ToString(CultureInfo.InvariantCulture),
				LocalId = source.Field<string>(prefixColumn + "LocalId"),
				KeyChartAxis = source.Field<int>(prefixColumn + "KeyChartAxis").ToString(CultureInfo.InvariantCulture),
				ChartAxisName = source.Field<string>(prefixColumn + "ChartAxisName"),
				Name = source.Field<string>(prefixColumn + "Name"),
				Min = source.Field<double?>(prefixColumn + "Min"),
				Max = source.Field<double?>(prefixColumn + "Max"),
				ColorR = source.Field<int?>(prefixColumn + "ColorR"),
				ColorG = source.Field<int?>(prefixColumn + "ColorG"),
				ColorB = source.Field<int?>(prefixColumn + "ColorB")
			};
		}

		/// <summary>
		/// Get the list of saved ChartMarker from a Chart
		/// </summary>
		/// <param name="KeyChart">The key of the chart.</param>
		public List<ChartMarker> GetListByKeyChart(string KeyChart) {
			List<ChartMarker> results;
			DbCommand command = db.GetStoredProcCommand(const_proc_getlistbychart);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyChart", KeyChart);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}

		/// <summary>
		/// Get the list of saved ChartMarker from a ChartAxis
		/// </summary>
		/// <param name="KeyChartAxis">The key of the chart axis.</param>
		public List<ChartMarker> GetListByKeyChartAxis(string KeyChartAxis) {
			List<ChartMarker> results;
			DbCommand command = db.GetStoredProcCommand(const_proc_getlistbychartaxis);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyChartAxis", KeyChartAxis);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}

		/// <summary>
		/// Gets all the ChartMarker for a specific chart.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyChart">The Key of the chart.</param>
		/// <param name="total">The total number of items.</param>
		/// <returns>The list of items.</returns>
		public List<ChartMarker> GetAllPagingByKeyChart(PagingParameter paging, string KeyChart, out int total) {
			DbCommand command = db.GetStoredProcCommand(const_proc_getallpagingbykeychart);
			command.CommandTimeout = 600;
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyChart", KeyChart);
			db.AddInParameterInt(command, "start", paging.start);
			db.AddInParameterInt(command, "limit", paging.limit);
			DataSet ds = db.ExecuteDataSet(command);
			if (ds.Tables[0].Rows.Count > 0)
				total = ds.Tables[0].Rows[0].Field<int>("TotalRecord");
			else
				total = 0;
			return FillObjects(ds);
		}

		/// <summary>
		/// Saves the chart ChartMarkers.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="ChartMarkers">The ChartMarkers.</param>
		/// <returns></returns>
		public List<ChartMarker> SaveChartChartMarkers(Chart entity, CrudStore<ChartMarker> ChartMarkers) {
			DbCommand command = db.GetStoredProcCommand(const_proc_chartchartmarkerssave);
			command.CommandTimeout = 600;
			List<ChartMarker> results;

			if (ChartMarkers == null || ChartMarkers.IsEmpty())
				return null;
			string inserts = null;
			string deletes = null;

			/*if (ChartMarkers.create != null) {
				inserts = Helper.BuildXml(ChartMarkers.create.Select(p => p.KeyChartMarker).ToArray());
			}*/

			if (ChartMarkers.destroy != null) {
				deletes = Helper.BuildXml(ChartMarkers.destroy.Select(p => p.KeyChartMarker).ToArray());
			}

			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyChart", entity.KeyChart);
			db.AddInParameterString(command, "Inserts", inserts);
			db.AddInParameterString(command, "Deletes", deletes);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}
	}
}
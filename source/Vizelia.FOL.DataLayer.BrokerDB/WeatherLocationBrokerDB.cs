﻿using System.Data.Common;
using System.Web.Security;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.DataLayer.BrokerDB {

	/// <summary>
	/// Database broker for WeatherLocation business entity.
	/// </summary>
	public class WeatherLocationBrokerDB : BaseBrokerDB<WeatherLocation> {

		/// <summary>
		/// The prefix of stored procedures of that provider. 
		/// </summary>
		protected override string const_proc_prefix {
			get {
				return AppModule.Energy;
			}
		}

		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, WeatherLocation item) {
			db.AddInParameterString(command, "Key", item.KeyWeatherLocation);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "LocalId", item.LocalId);
			db.AddInParameterString(command, "LocationId", item.LocationId);
			db.AddInParameterString(command, "Title", item.Title);
			db.AddInParameterString(command, "Name", item.Name);
			db.AddInParameterString(command, "Unit", item.Unit.ParseAsString());
			db.AddInParameterString(command, "DisplayFontFamily", item.DisplayFontFamily);
			db.AddInParameterString(command, "TextColor", item.TextColor);
			db.AddInParameterString(command, "MainBackgroundColor", item.MainBackgroundColor);
			db.AddInParameterString(command, "AlternativeBackgroundColor", item.AlternativeBackgroundColor);
			db.AddInParameterString(command, "DisplayType", item.DisplayType.ParseAsString());
			db.AddInParameterBool(command, "OpenOnStartup", item.OpenOnStartup);
			db.AddInParameterBool(command, "IsFavorite", item.IsFavorite);
		}


		/// <summary>
		/// Populates a WeatherLocation business entity from a DataSourceDecorator.
		/// </summary>
		/// <param name="source">The data source.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated WeatherLocation business entity.</returns>
		protected override WeatherLocation FillObject(DataSourceDecorator source, string prefixColumn) {
			return new WeatherLocation {
				KeyWeatherLocation = source.Field<int>(prefixColumn + "KeyWeatherLocation").ToString(),
				LocationId = source.Field<string>(prefixColumn + "LocationId"),
				LocalId = source.Field<string>(prefixColumn + "LocalId"),
				Title = source.Field<string>(prefixColumn + "Title"),
				Name = source.Field<string>(prefixColumn + "Name"),
				Unit = source.Field<string>(prefixColumn + "Unit").ParseAsEnum<WeatherUnit>(),
				DisplayFontFamily = source.Field<string>(prefixColumn + "DisplayFontFamily"),
				TextColor = source.Field<string>(prefixColumn + "TextColor"),
				MainBackgroundColor = source.Field<string>(prefixColumn + "MainBackgroundColor"),
				AlternativeBackgroundColor = source.Field<string>(prefixColumn + "AlternativeBackgroundColor"),
				DisplayType = source.Field<string>(prefixColumn + "DisplayType").ParseAsEnum<WeatherDisplayType>(),
				OpenOnStartup = source.Field<bool?>(prefixColumn + "OpenOnStartup") ?? false,
				IsFavorite = source.Field<bool?>(prefixColumn + "IsFavorite") ?? false
			};
		}
	}
}
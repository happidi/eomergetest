﻿using System.Data.Common;
using System.Globalization;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.DataLayer.BrokerDB {

	/// <summary>
	/// Database broker for Logo business entity.
	/// </summary>
	public class LogoBrokerDB : BaseBrokerDB<Logo> {

		/// <summary>
		/// The prefix of stored procedures of that provider. 
		/// </summary>
		protected override string const_proc_prefix {
			get {
				return AppModule.Core;
			}
		}
		
		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, Logo item) {
			db.AddInParameterString(command, "Key", item.KeyLogo);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyImage", item.KeyImage);
			db.AddInParameterString(command, "KeyKPIPreviewPicture", item.KeyKPIPreviewPicture);
		}

		/// <summary>
		/// Fills the object.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="prefixColumn">The prefix column.</param>
		/// <returns></returns>
		protected override Logo FillObject(DataSourceDecorator source, string prefixColumn) {
			return new Logo {
                Application = ContextHelper.ApplicationName,
				KeyLogo = source.Field<int>(prefixColumn + "KeyLogo").ToString(CultureInfo.InvariantCulture),
				KeyImage = source.Field<string>(prefixColumn + "KeyImage"),
				KeyKPIPreviewPicture = source.Field<string>(prefixColumn + "KeyKPIPreviewPicture")
			};
		}
	}
}
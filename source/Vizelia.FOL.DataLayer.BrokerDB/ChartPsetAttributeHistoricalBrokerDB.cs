﻿using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.DataLayer.BrokerDB {

	/// <summary>
	/// Database broker for ChartPsetAttributeHistorical business entity.
	/// </summary>
	public class ChartPsetAttributeHistoricalBrokerDB : BaseBrokerDB<ChartPsetAttributeHistorical> {

		/// <summary>
		/// The name of the stored procedure that retreives a list of ChartPsetAttributeHistorical for a specific chart.
		/// </summary>
		protected const string const_proc_getlistbychart = AppModule.Energy + "_ChartPsetAttributeHistoricalGetListByKeyChart";

		/// <summary>
		/// The name of the stored procedure that retreives a list of ChartPsetAttributeHistorical for a specific chart.
		/// </summary>
		protected const string const_proc_getallpagingbykeychart = AppModule.Energy + "_ChartPsetAttributeHistoricalGetAllPagingByKeyChart";

		/// <summary>
		/// The name of the stored procedure that saves a list of ChartPsetAttributeHistorical for a specific chart.
		/// </summary>
		protected const string const_proc_chartpsetattributehistoricalsave = AppModule.Energy + "_ChartChartPsetAttributeHistoricalSave";

		/// <summary>
		/// The prefix of stored procedures of that provider. 
		/// </summary>
		protected override string const_proc_prefix {
			get {
				return AppModule.Energy;
			}
		}

		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, ChartPsetAttributeHistorical item) {
			db.AddInParameterString(command, "Key", item.KeyChartPsetAttributeHistorical);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "LocalId", item.LocalId);
			db.AddInParameterString(command, "AttributeName", item.AttributeName);
			db.AddInParameterString(command, "GroupOperation", item.GroupOperation.ParseAsString());
			db.AddInParameterString(command, "KeyChart", item.KeyChart);
			db.AddInParameterString(command, "PsetName", item.PsetName);
			db.AddInParameterBool(command, "FillMissingValues", item.FillMissingValues);
		}

		/// <summary>
		/// Populates a ChartPsetAttributeHistorical business entity from a DataSourceDecorator.
		/// </summary>
		/// <param name="source">The data source.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated ChartPsetAttributeHistorical business entity.</returns>
		protected override ChartPsetAttributeHistorical FillObject(DataSourceDecorator source, string prefixColumn) {
			var retVal = new ChartPsetAttributeHistorical {
				KeyChartPsetAttributeHistorical = source.Field<int>(prefixColumn + "KeyChartPsetAttributeHistorical").ToString(CultureInfo.InvariantCulture),
				LocalId = source.Field<string>(prefixColumn + "LocalId"),
				AttributeName = source.Field<string>(prefixColumn + "AttributeName"),
				GroupOperation = source.Field<string>(prefixColumn + "GroupOperation").ParseAsEnum<MathematicOperator>(),
				KeyChart = source.Field<int>(prefixColumn + "KeyChart").ToString(CultureInfo.InvariantCulture),
				PsetName = source.Field<string>(prefixColumn + "PsetName"),
				PsetMsgCode = source.Field<string>(prefixColumn + "PsetMsgCode"),
				KeyPropertySingleValue = source.Field<string>(prefixColumn + "KeyPropertySingleValue"),
				AttributeMsgCode = source.Field<string>(prefixColumn + "AttributeMsgCode"),
				AttributeLabel = Helper.LocalizeText(source.Field<string>(prefixColumn + "AttributeMsgCode")),
				FillMissingValues = source.Field<bool?>(prefixColumn + "FillMissingValues") ?? false
			};
			retVal.PropertySingleValueLongPath = string.Format("{0} / {1}", Helper.LocalizeText(retVal.PsetMsgCode),
												   Helper.LocalizeText(retVal.AttributeMsgCode));
			return retVal;
		}

		/// <summary>
		/// Get the list of saved ChartPsetAttributeHistorical from a Chart
		/// </summary>
		/// <param name="KeyChart">The key of the chart.</param>
		public List<ChartPsetAttributeHistorical> GetListByKeyChart(string KeyChart) {
			List<ChartPsetAttributeHistorical> results;
			DbCommand command = db.GetStoredProcCommand(const_proc_getlistbychart);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyChart", KeyChart);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}

		/// <summary>
		/// Gets all the ChartPsetAttributeHistorical for a specific chart.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyChart">The Key of the chart.</param>
		/// <param name="total">The total number of items.</param>
		/// <returns>The list of items.</returns>
		public List<ChartPsetAttributeHistorical> GetAllPagingByKeyChart(PagingParameter paging, string KeyChart, out int total) {
			DbCommand command = db.GetStoredProcCommand(const_proc_getallpagingbykeychart);
			command.CommandTimeout = 600;
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyChart", KeyChart);
			db.AddInParameterInt(command, "start", paging.start);
			db.AddInParameterInt(command, "limit", paging.limit);
			DataSet ds = db.ExecuteDataSet(command);
			if (ds.Tables[0].Rows.Count > 0)
				total = ds.Tables[0].Rows[0].Field<int>("TotalRecord");
			else
				total = 0;
			return FillObjects(ds);
		}

		/// <summary>
		/// Saves the chart ChartPsetAttributeHistorical.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="historicals">The ChartPsetAttributeHistorical store.</param>
		/// <returns></returns>
		public List<ChartPsetAttributeHistorical> SaveChartChartPsetAttributeHistorical(Chart entity, CrudStore<ChartPsetAttributeHistorical> historicals) {
			DbCommand command = db.GetStoredProcCommand(const_proc_chartpsetattributehistoricalsave);
			command.CommandTimeout = 600;
			List<ChartPsetAttributeHistorical> results;

			if (historicals == null || historicals.IsEmpty())
				return null;
			string inserts = null;
			string deletes = null;

			if (historicals.destroy != null) {
				deletes = Helper.BuildXml(historicals.destroy.Select(p => p.KeyChartPsetAttributeHistorical).ToArray());
			}

			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyChart", entity.KeyChart);
			db.AddInParameterString(command, "Inserts", inserts);
			db.AddInParameterString(command, "Deletes", deletes);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}
	}
}
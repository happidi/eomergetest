﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.DataLayer.BrokerDB {
	/// <summary>
	/// Database broker for Priority business entity.
	/// </summary>
	public class PriorityBrokerDB : BaseBrokerDB<Priority> {

		const string const_proc_priorityclassification_deleteitem = AppModule.ServiceDesk + "_PriorityClassificationDeleteItem";
		const string const_proc_priorityclassification_saveitem = AppModule.ServiceDesk + "_PriorityClassificationSaveItem";
		const string const_proc_getitembyclassification = AppModule.ServiceDesk + "_PriorityGetItemByClassification";
		const string const_proc_getitembyclassificationascendant = AppModule.ServiceDesk + "_PriorityGetItemByClassificationAscendant";
		const string const_proc_getallpagingbykeymail = AppModule.ServiceDesk + "_PriorityGetAllPagingByKeyMail";
		const string const_proc_mailprioritiessave = AppModule.ServiceDesk + "_MailPrioritiesSave";
		/// <summary>
		/// Gets the const_proc_prefix.
		/// </summary>
		/// <value>The const_proc_prefix.</value>
		protected override string const_proc_prefix {
			get {
				return AppModule.ServiceDesk;
			}
		}

		/// <summary>
		/// Populates a Priority business entity from a DataSourceDecorator.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated Priority business entity.</returns>
		protected override Priority FillObject(DataSourceDecorator source, string prefixColumn) {
			return new Priority {
				KeyPriority = source.Field<int>(prefixColumn + "KeyPriority").ToString(CultureInfo.InvariantCulture),
				LocalId = source.Field<string>(prefixColumn + "LocalId"),
				MsgCode = source.Field<string>(prefixColumn + "MsgCode"),
				Value = source.Field<int>(prefixColumn + "Value")
			};
		}

		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, Priority item) {
			db.AddInParameterString(command, "Key", item.KeyPriority);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterInt(command, "Value", item.Value);
			db.AddInParameterString(command, "MsgCode", item.MsgCode);

			string localId = !string.IsNullOrWhiteSpace(item.LocalId) ? item.LocalId : null;
			db.AddInParameterString(command, "LocalId", localId);
		}

		/// <summary>
		/// Creates an association between a ClassificationItem and a Priority.
		/// </summary>
		/// <param name="KeyClassificationChildren">The Key of the ClassificationItem.</param>
		/// <param name="KeyClassificationParent">The Key of the parent ClassificationItem in case the priority is added to a relationship.</param>
		/// <param name="KeyPriority">The Key of the priority.</param>
		/// <returns></returns>
		public bool AddPriorityClassification(string KeyClassificationChildren, string KeyClassificationParent, string KeyPriority) {
			DbCommand command = db.GetStoredProcCommand(const_proc_priorityclassification_saveitem);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyClassificationChildren", KeyClassificationChildren);
			db.AddInParameterString(command, "KeyClassificationParent", KeyClassificationParent);
			db.AddInParameterString(command, "KeyPriority", KeyPriority);

			db.AddOutParameterString(command, "Return");
			db.AddReturnParameter(command);
			db.ExecuteNonQuery(command);

			string result = Convert.ToString(db.GetParameterValue(command, "Return"));
			return result.Length > 0;
		}

		/// <summary>
		/// Deletes an association between a ClassificationItem and a Priority.
		/// </summary>
		/// <param name="KeyClassificationChildren">The Key of the ClassificationItem.</param>
		/// <param name="KeyClassificationParent">The Key of the parent ClassificationItem in case the priority is added to a relationship.</param>
		/// <returns></returns>
		public bool DeletePriorityClassification(string KeyClassificationChildren, string KeyClassificationParent) {
			DbCommand command = db.GetStoredProcCommand(const_proc_priorityclassification_deleteitem);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyClassificationChildren", KeyClassificationChildren);
			db.AddInParameterString(command, "KeyClassificationParent", KeyClassificationParent);
			db.ExecuteNonQuery(command);
			return true;
		}

		/// <summary>
		/// Gets the priority assigned to a ClassificationItem.
		/// </summary>
		/// <param name="KeyClassificationChildren">The key classification children.</param>
		/// <param name="KeyClassificationParent">The key classification parent.</param>
		/// <returns></returns>
		public Priority GetItemByClassification(string KeyClassificationChildren, string KeyClassificationParent) {
			Priority result = default(Priority);
			DbCommand command = db.GetStoredProcCommand(const_proc_getitembyclassification);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyClassificationChildren", KeyClassificationChildren);
			db.AddInParameterString(command, "KeyClassificationParent", KeyClassificationParent);
			using (IDataReader reader = db.ExecuteReader(command)) {
				if (reader.Read()) {
					result = FillObject(reader);
				}
			}
			return result;
		}

		/// <summary>
		/// Gets the priority assigned to a any upper level of ClassificationItem Hierarchy.
		/// </summary>
		/// <param name="KeyClassificationChildren">The key classification children.</param>
		/// <param name="KeyClassificationParent">The key classification parent.</param>
		/// <returns></returns>
		public Priority GetItemByClassificationAscendant(string KeyClassificationChildren, string KeyClassificationParent) {
			Priority result = default(Priority);

			DbCommand command = db.GetStoredProcCommand(const_proc_getitembyclassificationascendant);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyClassificationChildren", KeyClassificationChildren);
			db.AddInParameterString(command, "KeyClassificationParent", KeyClassificationParent);
			using (IDataReader reader = db.ExecuteReader(command)) {
				if (reader.Read()) {
					result = FillObject(reader);
				}
			}
			return result;
		}

		/// <summary>
		/// Gets all the priorities for a specific mail.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyMail">The Key of the Mail.</param>
		/// <param name="total">The total number of items.</param>
		/// <returns>The list of items.</returns>
		public List<Priority> GetAllPagingByKeyMail(PagingParameter paging, string KeyMail, out int total) {
			DbCommand command = db.GetStoredProcCommand(const_proc_getallpagingbykeymail);
			command.CommandTimeout = 600;
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyMail", KeyMail);
			db.AddInParameterInt(command, "start", paging.start);
			db.AddInParameterInt(command, "limit", paging.limit);
			DataSet ds = db.ExecuteDataSet(command);
			if (ds.Tables[0].Rows.Count > 0)
				total = ds.Tables[0].Rows[0].Field<int>("TotalRecord");
			else
				total = 0;
			return FillObjects(ds);
		}

		/// <summary>
		/// Saves the mail priorities.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="priorities">The priorities.</param>
		/// <returns></returns>
		public List<Priority> SaveMailPriorities(Mail entity, CrudStore<Priority> priorities) {
			DbCommand command = db.GetStoredProcCommand(const_proc_mailprioritiessave);
			command.CommandTimeout = 600;
			List<Priority> results;

			if (priorities == null || priorities.IsEmpty())
				return null;
			string inserts = null;
			string deletes = null;

			if (priorities.create != null) {
				inserts = Helper.BuildXml(priorities.create.Select(p => p.KeyPriority).ToArray());
			}

			if (priorities.destroy != null) {
				deletes = Helper.BuildXml(priorities.destroy.Select(p => p.KeyPriority).ToArray());
			}

			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyMail", entity.KeyMail);
			db.AddInParameterString(command, "Inserts", inserts);
			db.AddInParameterString(command, "Deletes", deletes);
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}
			return results;
		}
	}
}

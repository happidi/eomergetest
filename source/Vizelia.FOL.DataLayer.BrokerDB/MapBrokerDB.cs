﻿using System.Data.Common;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.DataLayer.BrokerDB {

	/// <summary>
	/// Database broker for Map business entity.
	/// </summary>
	public class MapBrokerDB : BaseBrokerDB<Map> {

		/// <summary>
		/// The prefix of stored procedures of that provider. 
		/// </summary>
		protected override string const_proc_prefix {
			get {
				return AppModule.Energy;
			}
		}

		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, Map item) {
			db.AddInParameterString(command, "Key", item.KeyMap);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "LocalId", item.LocalId);
			db.AddInParameterString(command, "Title", item.Title);
			db.AddInParameterInt(command, "Zoom", item.Zoom);
			db.AddInParameterDouble(command, "LocationLatitude", item.LocationLatitude);
			db.AddInParameterDouble(command, "LocationLongitude", item.LocationLongitude);
			db.AddInParameterString(command, "MapType", item.MapType.ParseAsString());
			db.AddInParameterString(command, "PsetName", item.PsetName);
			db.AddInParameterString(command, "AttributeName", item.AttributeName);
			db.AddInParameterBool(command, "ShowDashboard", item.ShowDashboard);
			db.AddInParameterString(command, "KeyClassificationItemAlarmDefinition", item.KeyClassificationItemAlarmDefinition);
			db.AddInParameterBool(command, "OpenOnStartup", item.OpenOnStartup);
			db.AddInParameterBool(command, "IsFavorite", item.IsFavorite);
			db.AddInParameterString(command, "KeyLocation", item.KeyLocation);
			db.AddInParameterBool(command, "UseSpatialIcons", item.UseSpatialIcons);
		}


		/// <summary>
		/// Populates a Map business entity from a DataSourceDecorator.
		/// </summary>
		/// <param name="source">The data source.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated Map business entity.</returns>
		protected override Map FillObject(DataSourceDecorator source, string prefixColumn) {
			var retVal = new Map {
				KeyMap = source.Field<int>(prefixColumn + "KeyMap").ToString(),
				LocalId = source.Field<string>(prefixColumn + "LocalId"),
				Title = source.Field<string>(prefixColumn + "Title"),
				Zoom = source.Field<int>(prefixColumn + "Zoom"),
				LocationLatitude = source.Field<double>(prefixColumn + "LocationLatitude"),
				LocationLongitude = source.Field<double>(prefixColumn + "LocationLongitude"),
				MapType = source.Field<string>(prefixColumn + "MapType").ParseAsEnum<MapType>(),
				PsetName = source.Field<string>(prefixColumn + "PsetName"),
				AttributeName = source.Field<string>(prefixColumn + "AttributeName"),
				ShowDashboard = source.Field<bool>(prefixColumn + "ShowDashboard"),
				KeyClassificationItemAlarmDefinition = source.Field<string>(prefixColumn + "KeyClassificationItemAlarmDefinition"),
				KeyClassificationItemAlarmDefinitionPath = source.Field<string>(prefixColumn + "KeyClassificationItemAlarmDefinitionPath"),
				ClassificationItemAlarmDefinitionLongPath = source.Field<string>(prefixColumn + "ClassificationItemAlarmDefinitionLongPath"),
				ClassificationItemAlarmDefinitionLocalIdPath = source.Field<string>(prefixColumn + "ClassificationItemAlarmDefinitionLocalIdPath"),
				ClassificationItemAlarmDefinitionTitle = source.Field<string>(prefixColumn + "ClassificationItemAlarmDefinitionTitle"),
				ClassificationItemAlarmDefinitionLevel = source.Field<int>(prefixColumn + "ClassificationItemAlarmDefinitionLevel"),
				ClassificationItemAlarmDefinitionColorR = source.Field<int?>(prefixColumn + "ClassificationItemAlarmDefinitionColorR"),
				ClassificationItemAlarmDefinitionColorG = source.Field<int?>(prefixColumn + "ClassificationItemAlarmDefinitionColorG"),
				ClassificationItemAlarmDefinitionColorB = source.Field<int?>(prefixColumn + "ClassificationItemAlarmDefinitionColorB"),
				OpenOnStartup = source.Field<bool?>(prefixColumn + "OpenOnStartup") ?? false,
				IsFavorite = source.Field<bool?>(prefixColumn + "IsFavorite") ?? false,
				KeyLocation = source.Field<string>(prefixColumn + "KeyLocation"),
				LocationLongPath = source.Field<string>(prefixColumn + "LocationLongPath"),
				UseSpatialIcons = source.Field<bool?>(prefixColumn + "UseSpatialIcons") ?? false,
				PsetMsgCode = source.Field<string>(prefixColumn + "PsetMsgCode"),
				AttributeMsgCode = source.Field<string>(prefixColumn + "AttributeMsgCode")
			};
			if (string.IsNullOrEmpty(retVal.PsetMsgCode) == false && string.IsNullOrEmpty(retVal.AttributeMsgCode) == false)
				retVal.PropertySingleValueLongPath = string.Format("{0} / {1}", Helper.LocalizeText(retVal.PsetMsgCode), Helper.LocalizeText(retVal.AttributeMsgCode));

			return retVal;
		}
	}
}
﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Web.Security;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Vizelia.FOL.DataLayer.Interfaces;

namespace Vizelia.FOL.DataLayer.BrokerDB {

	/// <summary>
	/// Database broker for PsetAttributeHistoricalEndpoint business entity.
	/// </summary>
	public class PsetAttributeHistoricalEndpointBrokerDB : BaseBrokerDB<PsetAttributeHistoricalEndpoint> {

		/// <summary>
		/// The name of the stored procedure that retreives a PsetAttributeHistoricalEndpoint for a specific Object.
		/// </summary>
		protected const string const_proc_getitembykeyobject = AppModule.Energy + "_PsetAttributeHistoricalEndpointGetItemByKeyObject";


		/// <summary>
		/// The prefix of stored procedures of that provider. 
		/// </summary>
		protected override string const_proc_prefix {
			get {
				return AppModule.Energy;
			}
		}


		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, PsetAttributeHistoricalEndpoint item) {
			db.AddInParameterString(command, "Key", item.KeyPsetAttributeHistoricalEndpoint);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyEndpoint", item.KeyEndpoint);
			db.AddInParameterString(command, "KeyObject", item.KeyObject);
			db.AddInParameterString(command, "KeyPropertySingleValue", item.KeyPropertySingleValue);
			db.AddInParameterString(command, "EndpointType", item.EndpointType);
			db.AddInParameterInt(command, "EndpointFrequency", item.EndpointFrequency);
		}


		/// <summary>
		/// Populates a PsetAttributeHistoricalEndpoint business entity from a DataSourceDecorator.
		/// </summary>
		/// <param name="source">The data source.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated PsetAttributeHistoricalEndpoint business entity.</returns>
		protected override PsetAttributeHistoricalEndpoint FillObject(DataSourceDecorator source, string prefixColumn) {
			return new PsetAttributeHistoricalEndpoint {
				KeyPsetAttributeHistoricalEndpoint = source.Field<int>(prefixColumn + "KeyPsetAttributeHistoricalEndpoint").ToString(),
				KeyEndpoint = source.Field<string>(prefixColumn + "KeyEndpoint"),
				KeyObject = source.Field<string>(prefixColumn + "KeyObject"),
				KeyPropertySingleValue = source.Field<string>(prefixColumn + "KeyPropertySingleValue"),
				EndpointType = source.Field<string>(prefixColumn + "EndpointType"),
				EndpointFrequency = source.Field<int?>(prefixColumn + "EndpointFrequency"),
			};
		}

		/// <summary>
		/// Get the  PsetAttributeHistoricalEndpoint for a specific Object.
		/// </summary>
		/// <param name="KeyObject">The key object.</param>
		/// <param name="KeyPropertySingleValue">KeyPropertySingleValue.</param>
		/// <returns></returns>
		public PsetAttributeHistoricalEndpoint GetItemByKeyObject(string KeyObject, string KeyPropertySingleValue) {
			PsetAttributeHistoricalEndpoint retVal = default(PsetAttributeHistoricalEndpoint);
			DbCommand command = db.GetStoredProcCommand(const_proc_getitembykeyobject);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyObject", KeyObject);
			db.AddInParameterString(command, "KeyPropertySingleValue", KeyPropertySingleValue);
			using (IDataReader reader = db.ExecuteReader(command)) {
				if (reader.Read()) {
					retVal = FillObject(reader);
				}
			}
			return retVal;
		}

	}
}
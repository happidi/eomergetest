﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Web.Security;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Vizelia.FOL.DataLayer.Interfaces;

namespace Vizelia.FOL.DataLayer.BrokerDB {

	/// <summary>
	/// Database broker for WorkflowInstance business entity.
	/// </summary>
	public class WorkflowInstanceBrokerDB : BaseBrokerDB<WorkflowInstance> {

		/// <summary>
		/// The name of the stored procedure that retreives the item by its instanceId.
		/// </summary>
		protected const string const_proc_getitembyinstanceid = AppModule.Core + "_WorkflowInstanceGetItemByInstanceId";

		
		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, WorkflowInstance item) {
			throw new NotImplementedException();
		}


		/// <summary>
		/// Populates a WorkflowInstance business entity from a DataSourceDecorator.
		/// </summary>
		/// <param name="source">The data source.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated WorkflowInstanceDefinition business entity.</returns>
		protected override WorkflowInstance FillObject(DataSourceDecorator source, string prefixColumn) {
			return new WorkflowInstance {
				KeyWorkflowInstance = source.Field<string>(prefixColumn + "KeyWorkflowInstance"),
				InstanceId = source.Field<Guid>(prefixColumn + "InstanceId"),
				State = source.Field<string>(prefixColumn + "State"),
				ObjectType = source.Field<string>(prefixColumn + "ObjectType")
			};
		}

		/// <summary>
		/// Gets an item by its instanceId.
		/// </summary>
		/// <param name="instanceId">The instanceId of the item.</param>
		/// <returns>The item.</returns>
		public WorkflowInstance GetItemByInstanceId(Guid instanceId) {
			WorkflowInstance item = default(WorkflowInstance);
			DbCommand command = db.GetStoredProcCommand(const_proc_getitembyinstanceid);
			db.AddInParameterGuid(command, "InstanceId", instanceId);
			using (IDataReader reader = db.ExecuteReader(command)) {
				if (reader.Read()) {
					item = FillObject(reader);
				}
			}
			return item;
		}


		
	}
}
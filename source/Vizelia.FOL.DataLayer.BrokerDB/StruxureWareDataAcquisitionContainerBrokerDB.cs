﻿using System.Data.Common;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.DataLayer.BrokerDB {

	/// <summary>
	/// Database broker for StruxureWareDataAcquisitionContainer business entity.
	/// </summary>
	public class StruxureWareDataAcquisitionContainerBrokerDB : BaseBrokerDB<StruxureWareDataAcquisitionContainer> {

		/// <summary>
		/// The prefix of stored procedures of that provider. 
		/// </summary>
		protected override string const_proc_prefix {
			get {
				return AppModule.Energy;
			}
		}


		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, StruxureWareDataAcquisitionContainer item) {
			db.AddInParameterString(command, "Key", item.KeyDataAcquisitionContainer);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "LocalId", item.LocalId);
			db.AddInParameterString(command, "Title", item.Title);
			db.AddInParameterString(command, "IP", item.Url);
			db.AddInParameterString(command, "Username", item.Username);
			db.AddInParameterString(command, "Password", item.Password);
		}


		/// <summary>
		/// Populates a StruxureWareDataAcquisitionContainer business entity from a DataSourceDecorator.
		/// </summary>
		/// <param name="source">The data source.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated StruxureWareDataAcquisitionContainer business entity.</returns>
		protected override StruxureWareDataAcquisitionContainer FillObject(DataSourceDecorator source, string prefixColumn) {
			return new StruxureWareDataAcquisitionContainer {
				KeyDataAcquisitionContainer = source.Field<int>(prefixColumn + "KeyDataAcquisitionContainer").ToString(),
				LocalId = source.Field<string>(prefixColumn + "LocalId"),
				Title = source.Field<string>(prefixColumn + "Title"),
				Url = source.Field<string>(prefixColumn + "IP"),
				Username = source.Field<string>(prefixColumn + "Username"),
				Password = source.Field<string>(prefixColumn + "Password")
			};
		}
	}
}
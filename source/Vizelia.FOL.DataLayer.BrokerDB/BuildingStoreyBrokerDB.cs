﻿using Vizelia.FOL.BusinessEntities;
using System.Data.Common;
using System.Web.Security;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.DataLayer.BrokerDB {
	/// <summary>
	/// Database broker for BuildingStorey business entity.
	/// </summary>
	public class BuildingStoreyBrokerDB : BaseBrokerDB<BuildingStorey> {

		/// <summary>
		/// Populates a BuildingStorey business entity from a DataSourceDecorator.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated BuildingStorey business entity.</returns>
		protected override BuildingStorey FillObject(DataSourceDecorator source, string prefixColumn) {
			return new BuildingStorey {
				KeyBuildingStorey = source.Field<string>(prefixColumn + "KeyBuildingStorey"),
				LocalId = source.Field<string>(prefixColumn + "LocalId"),
				CADFile = source.Field<string>(prefixColumn + "CADFile"),
				ObjectType = source.Field<string>(prefixColumn + "ObjectType"),
				Name = source.Field<string>(prefixColumn + "Name"),
				Elevation = source.Field<double?>(prefixColumn + "Elevation"),
				Description = source.Field<string>(prefixColumn + "Description"),
				KeyBuilding = source.Field<string>(prefixColumn + "KeyBuilding")
			};
		}

		
		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, BuildingStorey item) {
			db.AddInParameterString(command, "Key", item.KeyBuildingStorey);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "CADFile", item.CADFile);
			db.AddInParameterString(command, "KeyBuilding", item.KeyBuilding);
			db.AddInParameterString(command, "ObjectType", item.ObjectType);
			db.AddInParameterString(command, "Name", item.Name);
			db.AddInParameterString(command, "Description", item.Description);
			db.AddInParameterDouble(command, "Elevation", item.Elevation);
            db.AddInParameterBool(command, "IsMappingInProgress", base.IsAsynchronousSave);
			string localId = !string.IsNullOrWhiteSpace(item.LocalId) ? item.LocalId : null;
			db.AddInParameterString(command, "LocalId", localId);
		}


        /// <summary>
        /// Builds the additional delete params.
        /// </summary>
        /// <param name="command">The command.</param>
        protected override void BuildAdditionalDeleteParams(DbCommand command) {
            db.AddInParameterBool(command, "IsMappingInProgress", base.IsAsynchronousSave);
        }
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.DataLayer.BrokerDB {
	/// <summary>
	/// Base abstract brokerdb for brokers using a service provider.
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public abstract class BaseBrokerDBServiceProvider<T> : BaseBrokerDBGeneric<T> where T : BaseBusinessEntity, new() {
		/// <summary>
		/// Gets the item.
		/// </summary>
		/// <param name="Key">The key.</param>
		/// <returns></returns>
		public abstract override T GetItem(string Key);

		/// <summary>
		/// Gets the list.
		/// </summary>
		/// <param name="Keys">The keys.</param>
		/// <returns></returns>
		public abstract override List<T> GetList(string[] Keys);

		/// <summary>
		/// Gets all no paging.
		/// </summary>
		/// <returns></returns>
		protected abstract override List<T> GetAllNoPaging();

		/// <summary>
		/// Gets all generic paging.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="total">The total.</param>
		/// <returns></returns>
		protected abstract override List<T> GetAllGenericPaging(PagingParameter paging, out int total);

		/// <summary>
		/// Creates the specified item.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <returns></returns>
		public abstract override T Create(T item);

		/// <summary>
		/// Updates the specified item.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <returns></returns>
		public abstract override T Update(T item);

		/// <summary>
		/// Deletes the specified item.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <returns></returns>
		public abstract override bool Delete(T item);

		/// <summary>
		/// Gets the item by local id.
		/// </summary>
		/// <param name="localId">The local id.</param>
		/// <returns></returns>
		public abstract override T GetItemByLocalId(string localId);

		/// <summary>
		/// Updates the batch.
		/// </summary>
		/// <param name="keys">The keys.</param>
		/// <param name="item">The item.</param>
		/// <returns></returns>
		public abstract override int UpdateBatch(string[] keys, T item);

		/// <summary>
		/// Workflows the delete instance.
		/// </summary>
		/// <param name="instanceId">The instance id.</param>
		/// <returns></returns>
		public abstract override bool WorkflowDeleteInstance(Guid instanceId);

		/// <summary>
		/// Deletes the specified key.
		/// </summary>
		/// <param name="Key">The key.</param>
		/// <returns></returns>
		public abstract override bool Delete(string Key);
	}
}

﻿using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Globalization;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.DataLayer.BrokerDB {

	/// <summary>
	/// Database broker for Logo business entity.
	/// </summary>
	public class LinkBrokerDB : BaseBrokerDB<Link> {

		/// <summary>
		/// The prefix of stored procedures of that provider. 
		/// </summary>
		protected override string const_proc_prefix {
			get {
				return AppModule.Core;
			}
		}

		/// <summary>
		/// The name of the stored procedure that retreives a paged list of tasks  for a specified ActionRequest.
		/// </summary>
		private const string const_proc_getstoreroot = AppModule.Core + "_LinkGetStoreRoot";

		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, Link item) {
			db.AddInParameterString(command, "Key", item.KeyLink);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyImage", item.KeyImage);
			db.AddInParameterString(command, "LocalId", item.LocalId);
			db.AddInParameterString(command, "LinkValue", item.LinkValue);
			db.AddInParameterString(command, "KeyParent", item.KeyParent);
			db.AddInParameterString(command, "IconCls", string.IsNullOrEmpty(item.KeyImage) == false ? null : item.IconCls);
			db.AddInParameterString(command, "MsgCode", item.MsgCode);
			db.AddInParameterString(command, "GroupNameMsgCode", item.GroupNameMsgCode);
		}

		/// <summary>
		/// Fills the object.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="prefixColumn">The prefix column.</param>
		/// <returns></returns>
		protected override Link FillObject(DataSourceDecorator source, string prefixColumn) {
			return new Link {
				KeyLink = source.Field<int>(prefixColumn + "KeyLink").ToString(CultureInfo.InvariantCulture),
				KeyImage = source.Field<string>(prefixColumn + "KeyImage"),
				KeyParent = source.Field<string>(prefixColumn + "KeyParent"),
				LinkValue = source.Field<string>(prefixColumn + "LinkValue"),
				LocalId = source.Field<string>(prefixColumn + "LocalId"),
				IconCls = source.Field<string>(prefixColumn + "IconCls"),
				MsgCode = source.Field<string>(prefixColumn + "MsgCode"),
				GroupNameMsgCode = source.Field<string>(prefixColumn + "GroupNameMsgCode")
			};
		}

		/// <summary>
		/// Gets the store root only.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="location">The location.</param>
		/// <param name="total">The total.</param>
		public List<Link> GetStoreRootOnly(PagingParameter paging, PagingLocation location, out int total) {
			BuildSecurityFilterParams(paging);
			DbCommand command = db.GetStoredProcCommand(const_proc_getstoreroot);
			command.CommandTimeout = 600;
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterInt(command, "start", paging.start);
			db.AddInParameterInt(command, "limit", paging.limit);
			AddPagingParameterOrderBy(paging, command);
			AddPagingParameterFilter(paging, command);

			DataSet ds = db.ExecuteDataSet(command);
			if (ds.Tables[0].Rows.Count > 0)
				total = ds.Tables[0].Rows[0].Field<int>("TotalRecord");
			else
				total = 0;
			return FillObjects(ds);
		}
	}
}
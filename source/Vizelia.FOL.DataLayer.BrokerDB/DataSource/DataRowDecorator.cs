﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.DataLayer.BrokerDB {
	/// <summary>
	/// An implementation of the DataSource decorator for a IDataReader.
	/// </summary>
	class DataRowDecorator : DataSourceDecorator {
		DataRow _row;
		public DataRowDecorator(DataRow row) {
			_row = row;
		}

		/// <summary>
		/// Gets the value of the specified field.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="fieldName">The name of the field.</param>
		/// <returns></returns>
		protected override T InnerField<T>(string fieldName) {
			return _row.Field<T>(fieldName);
		}

		/// <summary>
		/// Checks if the field the exists.
		/// </summary>
		/// <param name="fieldName">The name of the field.</param>
		/// <returns></returns>
		public override bool FieldExists(string fieldName) {
			return _row.FieldExists(fieldName);
		}


		/// <summary>
		/// Gets the type of the inner source.
		/// </summary>
		/// <returns></returns>
		public override Type GetInnerType() {
			return _row.GetType();
		}
	}
}

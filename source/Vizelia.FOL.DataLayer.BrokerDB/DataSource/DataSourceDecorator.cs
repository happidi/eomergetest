﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Vizelia.FOL.DataLayer.BrokerDB {

	/// <summary>
	/// A DataSource decorator.
	/// Used to expose a shared interface on datasource, for example IDataReader or DataRow
	/// This way we can use a single mapping function when reading the source and feeding the entity.
	/// </summary>
	public abstract class DataSourceDecorator {


		/// <summary>
		/// Gets the value of the specified field.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="fieldName">Name of the field.</param>
		/// <returns></returns>
		public T Field<T>(string fieldName) {
			T retVal = this.InnerField<T>(fieldName);

			if (retVal is DateTime) {
				DateTime date = DateTime.SpecifyKind((DateTime)(object)retVal, DateTimeKind.Utc);
				return (T)(object)date;
			}

			return retVal;
		}

		/// <summary>
		/// Gets the value of the specified field.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="fieldName">The name of the field.</param>
		/// <returns></returns>
		protected abstract T InnerField<T>(string fieldName);

		/// <summary>
		/// Checks if the field the exists.
		/// </summary>
		/// <param name="fieldName">The name of the field.</param>
		/// <returns></returns>
		public abstract bool FieldExists(string fieldName);

		/// <summary>
		/// Gets the type of the inner source.
		/// </summary>
		/// <returns></returns>
		public abstract Type GetInnerType();
		
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.DataLayer.BrokerDB {
	/// <summary>
	/// An implementation of the DataSource decorator for a IDataReader.
	/// </summary>
	class DataReaderDecorator : DataSourceDecorator, IDisposable {

		private IDataReader _reader;

		/// <summary>
		/// Initializes a new instance of the <see cref="DataReaderDecorator"/> class.
		/// </summary>
		/// <param name="reader">The reader.</param>
		public DataReaderDecorator(IDataReader reader) {
			_reader = reader;
		}

		/// <summary>
		/// Gets the value of the specified field.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="fieldName">The name of the field.</param>
		/// <returns></returns>
		protected override T InnerField<T>(string fieldName) {
			return _reader.GetValue<T>(fieldName);
		}

		/// <summary>
		/// Check if the field exists.
		/// </summary>
		/// <param name="name">The name of the field.</param>
		/// <returns></returns>
		public override bool FieldExists(string name) {
			return _reader.FieldExists(name);
		}




		/// <summary>
		/// Gets the type of the inner source.
		/// </summary>
		/// <returns></returns>
		public override Type GetInnerType() {
			return _reader.GetType();
		}


		/// <summary>
		/// Reads this instance.
		/// </summary>
		/// <returns></returns>
		public bool Read() {
			return _reader.Read();
		}


		/// <summary>
		/// Disposes the instance.
		/// </summary>
		public void Dispose() {
			_reader.Dispose();
		}
	}

}

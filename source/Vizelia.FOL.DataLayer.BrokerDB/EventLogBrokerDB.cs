﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web.Security;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.DataLayer.BrokerDB {

	/// <summary>
	/// Database broker for EventLog business entity.
	/// </summary>
	public class EventLogBrokerDB : BaseBrokerDB<EventLog> {

		/// <summary>
		/// The name of the stored procedure that retreives a list of ChartAxis for a specific chart.
		/// </summary>
		protected const string const_proc_getlistbychart = AppModule.Energy + "_EventLogGetListByKeyChart";

		/// <summary>
		/// The prefix of stored procedures of that provider. 
		/// </summary>
		protected override string const_proc_prefix {
			get {
				return AppModule.Energy;
			}
		}
		

		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, EventLog item) {
			db.AddInParameterString(command, "Key", item.KeyEventLog);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "Name", item.Name);
			db.AddInParameterString(command, "LocalId", item.LocalId);
			db.AddInParameterString(command, "Description", item.Description);
			db.AddInParameterDateTime(command, "StartDate", item.StartDate);
			db.AddInParameterDateTime(command, "EndDate", item.EndDate);
			db.AddInParameterString(command, "KeyLocation", item.KeyLocation);
			db.AddInParameterString(command, "KeyClassificationItem", item.KeyClassificationItem);

		}

		/// <summary>
		/// Populates a EventLog business entity from a DataSourceDecorator.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated EventLog business entity.</returns>
		protected override EventLog FillObject(DataSourceDecorator source, string prefixColumn) {
			return new EventLog {
				KeyEventLog = source.Field<int>(prefixColumn + "KeyEventLog").ToString(),
				Name = source.Field<string>(prefixColumn + "Name"),
				LocalId = source.Field<string>(prefixColumn + "LocalId"),
				Description = source.Field<string>(prefixColumn + "Description"),
				StartDate = source.Field<DateTime>(prefixColumn + "StartDate"),
				EndDate = source.Field<DateTime>(prefixColumn + "EndDate"),
				KeyLocation = source.Field<string>(prefixColumn + "KeyLocation"),
				KeyClassificationItem = source.Field<string>(prefixColumn + "KeyClassificationItem"),
				LocationName = source.Field<string>(prefixColumn + "LocationName"),
				LocationTypeName = source.Field<string>(prefixColumn + "LocationTypeName").ParseAsEnum<HierarchySpatialTypeName>(),
				LocationDescription = source.Field<string>(prefixColumn + "LocationDescription"),
				LocationShortPath = source.Field<string>(prefixColumn + "LocationShortPath"),
				LocationLongPath = source.Field<string>(prefixColumn + "LocationLongPath"),
				LocationLevel = source.Field<int>(prefixColumn + "LocationLevel"),
				ClassificationItemTitle = source.Field<string>(prefixColumn + "ClassificationItemTitle"),
				ClassificationItemLongPath = source.Field<string>(prefixColumn + "ClassificationItemLongPath"),
				ClassificationItemColorR = source.Field<int>(prefixColumn + "ClassificationItemColorR"),
				ClassificationItemColorG = source.Field<int>(prefixColumn + "ClassificationItemColorG"),
				ClassificationItemColorB = source.Field<int>(prefixColumn + "ClassificationItemColorB")
			};
		}


		

		/// <summary>
		/// Get the list of EventLog from a Chart
		/// </summary>
		/// <param name="KeyChart">The key of the chart.</param>
		public List<EventLog> GetListByKeyChart(string KeyChart) {
			List<EventLog> retVal;
			DbCommand command = db.GetStoredProcCommand(const_proc_getlistbychart);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterString(command, "KeyChart", KeyChart);
			AddProjectLocationFilter(command, "ProjectLocationFilter");
			
			using (IDataReader reader = db.ExecuteReader(command)) {
				retVal = FillObjects(reader);
			}
			return retVal;
		}
	}
}


﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web.Security;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Vizelia.FOL.DataLayer.Interfaces;
using System.Data.SqlClient;

namespace Vizelia.FOL.DataLayer.BrokerDB {
	/// <summary>
	/// Database broker for MeterDataBinary business entity.
	/// </summary>
	public class MeterDataBinaryBrokerDB : BaseBrokerDB<MeterDataBinary> {


		/// <summary>
		/// The name of the stored procedure that retreives the total count of meter data.
		/// </summary>
		protected const string const_proc_getcount = AppModule.Energy + "_MeterDataBinaryGetCount";

		/// <summary>
		/// The name of the stored procedure that retreives the most requested meters.
		/// </summary>
		protected const string const_proc_getmostrequested = AppModule.Energy + "_MeterDataBinaryGetMostRequested";

		/// <summary>
		/// The name of the stored procedure that increase the request count of a specific meter.
		/// </summary>
		protected const string const_proc_increaserequestcount= AppModule.Energy + "_MeterDataBinaryIncreaseRequestCount";


		

		/// <summary>
		/// Initializes a new instance of the <see cref="MeterDataBrokerDB"/> class.
		/// </summary>
		public MeterDataBinaryBrokerDB()
			: base() {
			db = Helper.CreateInstance<DataAccess, IDataAccess>("dns_vizelia_meter_data");
		}

		/// <summary>
		/// Gets the const_proc_prefix.
		/// </summary>
		/// <value>The const_proc_prefix.</value>
		protected override string const_proc_prefix {
			get {
				return AppModule.Energy;
			}
		}

		/// <summary>
		/// Populates a MeterData business entity from a DataSourceDecorator.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="prefixColumn">The prefix string of the columns in database.</param>
		/// <returns>The populated MeterData business entity.</returns>
		protected override MeterDataBinary FillObject(DataSourceDecorator source, string prefixColumn) {
			return new MeterDataBinary {
				KeyMeter = source.Field<int>(prefixColumn + "KeyMeter"),
				Values = source.Field<byte[]>(prefixColumn + "Values"),
				Count = source.Field<int>(prefixColumn + "Count"),
				Requested = source.Field<int>(prefixColumn + "Requested"),
				//Size = source.Field<double>(prefixColumn + "Requested")
			};
		}

		/// <summary>
		/// Builds the parameters for the insert and update procs.
		/// </summary>
		/// <param name="command">The command to append parameters to.</param>
		/// <param name="item">The item to save.</param>
		protected override void BuildSaveParams(DbCommand command, MeterDataBinary item) {
			db.AddInParameterInt(command, "Key", item.KeyMeter);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterByteArray(command, "Values", item.Values);
			db.AddInParameterDouble(command, "Size", item.Size);
			//db.AddInParameterInt(command, "Requested", item.Requested);
			db.AddInParameterInt(command, "Count", item.Count);
		}


		/// <summary>
		/// Get the total count of meter data inside the table.
		/// </summary>
		/// <returns></returns>
		public int GetCount() {
			DbCommand command = db.GetStoredProcCommand(const_proc_getcount);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			var retVal = 0;
			using (IDataReader reader = db.ExecuteReader(command)) {
				if (reader.Read()) {
					retVal = (int)reader[0];
				}
			}
			return retVal;
		}

		/// <summary>
		/// Gets the most requested meters that feats into the specified memory.
		/// </summary>
		/// <param name="maxMemorySize">Size of the max memory.</param>
		/// <returns></returns>
		public List<MeterDataBinary> GetMostRequested(double maxMemorySize) {
			DbCommand command = db.GetStoredProcCommand(const_proc_getmostrequested);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterDouble(command, "MaxMemorySize", maxMemorySize);

			var results = new List<MeterDataBinary>();
			using (IDataReader reader = db.ExecuteReader(command)) {
				results = FillObjects(reader);
			}

			return results;
		}

		/// <summary>
		/// Increase the requested count of a specific meter.
		/// </summary>
		/// <param name="KeyMeter">The key meter.</param>
		public void IncreaseRequestCount(int KeyMeter) {
			DbCommand command = db.GetStoredProcCommand(const_proc_increaserequestcount);
			db.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			db.AddInParameterInt(command, "Key", KeyMeter);
			db.ExecuteNonQuery(command);	
		}


		/// <summary>	
		///We override no to throw an error because we dont get anything back.
		/// </summary>
		/// <param name="item">The item.</param>
		protected override MeterDataBinary DoUpdate(MeterDataBinary item) {
			using (DbCommand command = db.GetStoredProcCommand(const_proc_updateitem)) {
				BuildSaveParamsInternal(command, item);
				string result;
				try {
					db.ExecuteNonQuery(command);
					result = Convert.ToString(db.GetParameterValue(command, const_commandreturnparam));
				}
				catch (SqlException ex) {
					throw SqlExceptionWrap(ex);
				}
				
				return null;
			}
		}
		
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Activities;
using System.Activities.Statements;
using Vizelia.FOL.BusinessEntities;
using System.Web;
using Microsoft.VisualBasic.Activities;
using System.Activities.Expressions;

namespace Vizelia.FOL.WF.Common {
	/// <summary>
	/// A custom activity that encapsulates a condition expression.
	/// </summary>
	public sealed class ConditionWrapperActivity : Activity<bool> {

		private Activity<bool> _vbExpression;



		/// <summary>
		/// Gets or sets the entity arg.
		/// </summary>
		/// <value>The entity arg.</value>
		/// <remarks></remarks>
		public InArgument<BaseBusinessEntity> EntityArg { get; set; }



		/// <summary>
		/// Initializes a new instance of the <see cref="ConditionWrapperActivity"/> class.
		/// </summary>
		/// <param name="vbExpression">The vb expression.</param>
		/// <remarks></remarks>
		public ConditionWrapperActivity(Activity<bool> vbExpression) {
			_vbExpression = vbExpression;

			var ifActivity = new If(_vbExpression) {
				Then = new Assign<bool> {
					To = new ArgumentReference<bool>("Result"),
					Value = true
				},
				Else = new Assign<bool> {
					To = new ArgumentReference<bool>("Result"),
					Value = false
				}

			};


			Implementation = () => ifActivity;

		}

	}
}

﻿using System;
using System.Collections.Generic;
using Vizelia.FOL.BusinessEntities;
using System.Activities;
using System.Threading;
using System.Configuration;
using System.Activities.DurableInstancing;
using Microsoft.VisualBasic.Activities;
using Vizelia.FOL.Common;
using Vizelia.FOL.Providers;
using System.Web;

namespace Vizelia.FOL.WF.Common {

	/// <summary>
	/// The workflow executor.
	/// </summary>
	internal class WorkflowExecutor {

		private WorkflowApplication _app;
		private IWorkflowEntity _entity;
		private WorkflowResults _result;
		private AutoResetEvent _waitHandle;
		private WorkflowDefinition _workflowDefinition;



		/// <summary>
		/// Initializes a new instance of the <see cref="WorkflowExecutor"/> class.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="workflowDefinition">The workflow definition.</param>
		public WorkflowExecutor(IWorkflowEntity entity, WorkflowDefinition workflowDefinition) {
			_entity = entity;
			_workflowDefinition = workflowDefinition;
		}



		/// <summary>
		/// Gets or sets the result.
		/// </summary>
		/// <value>The result.</value>
		public WorkflowResults Result {
			get {
				return _result;
			}
			set {
				_result = value;
			}
		}



		/// <summary>
		/// Configures the event handlers.
		/// </summary>
		private void ConfigureEventHandlers() {
			_app.Unloaded = WorkflowApplicationOnUnloaded;
			_app.PersistableIdle = WorkflowApplicationOnPersistableIdle;
			_app.Completed = WorkflowApplicationOnCompleted;
			_app.Aborted = WorkflowApplicationOnAborted;
			_app.Idle = WorkflowApplicationOnIdle;
			_app.OnUnhandledException = WorkflowApplicationOnUnhandledException;

		}

		/// <summary>
		/// Configures the extensions.
		/// </summary>
		/// <param name="workflowDefinition">The workflow definition.</param>
		private void ConfigureExtensions(WorkflowDefinition workflowDefinition) {
			if (workflowDefinition.LocalServiceType != null) {
				var localService = WorkflowExtensionService.GetExtension(workflowDefinition.LocalServiceType);
				_app.Extensions.Add(localService);

				SaveEntityTrackingParticipant setp = new SaveEntityTrackingParticipant {
					LocalService = localService,
					CurrentContext = HttpContext.Current,
					ApplicationName = ContextHelper.ApplicationName
				};
				_app.Extensions.Add(setp);
			}

			if (workflowDefinition.WorkflowExtensions != null)
				foreach (var extensionType in workflowDefinition.WorkflowExtensions) {
					_app.Extensions.Add(Activator.CreateInstance(extensionType));
				}

			ConfigureReferences();
		}

		/// <summary>
		/// Configures the references needed by the workflow runtime.
		/// </summary>
		private void ConfigureReferences() {
			if (_workflowDefinition.AssemblyAndNamespaceDictionary != null)
				foreach (var vbReference in _workflowDefinition.AssemblyAndNamespaceDictionary) {
					VisualBasicSettings.Default.ImportReferences.Add(new VisualBasicImportReference { Assembly = vbReference.Key, Import = vbReference.Value });
				}
		}

		/// <summary>
		/// Configures the persistence.
		/// </summary>
		private void ConfigurePersistence() {
			var conn = ConfigurationManager.ConnectionStrings[WorkflowHelper.const_workflow_database].ConnectionString;
			var store = new SqlWorkflowInstanceStore(conn) {
				InstanceLockedExceptionAction = InstanceLockedExceptionAction.AggressiveRetry,
				InstanceCompletionAction = InstanceCompletionAction.DeleteAll,
				HostLockRenewalPeriod = TimeSpan.FromSeconds(5),
				RunnableInstancesDetectionPeriod = TimeSpan.FromSeconds(3)
			};
			_app.InstanceStore = store;
		}

		/// <summary>
		/// Configures the workflow application.
		/// </summary>
		/// <param name="workflowDefinition">The workflow definition.</param>
		/// <param name="workflowInputs">The workflow inputs.</param>
		/// <returns></returns>
		private WorkflowApplication ConfigureWorkflowApplication(WorkflowDefinition workflowDefinition, Dictionary<string, object> workflowInputs) {
			return ConfigureWorkflowApplication(workflowDefinition, workflowInputs, String.Empty);
		}

		/// <summary>
		/// Configures the workflow application.
		/// </summary>
		/// <param name="workflowDefinition">The workflow definition.</param>
		/// <param name="workflowInputs">The workflow inputs.</param>
		/// <param name="initialState">The initial state. If the value is not empty, the wrapper activity will position the initial state to this value.</param>
		/// <returns></returns>
		private WorkflowApplication ConfigureWorkflowApplication(WorkflowDefinition workflowDefinition, Dictionary<string, object> workflowInputs, string initialState) {
			if (workflowDefinition.WorkflowActivity == null)
				workflowDefinition.WorkflowActivity = Activator.CreateInstance(workflowDefinition.WorkflowType) as Activity;
			// String.Empty for original starting state.
			Activity dynamicWrapperWorkflowDefinition = WorkflowHelper.GetDynamicWorkflowDefinition(workflowDefinition.WorkflowActivity, initialState);

			if (workflowInputs != null) {
				_app = new WorkflowApplication(dynamicWrapperWorkflowDefinition, workflowInputs);
			}
			else {
				_app = new WorkflowApplication(dynamicWrapperWorkflowDefinition);
			}

			ConfigurePersistence();
			ConfigureExtensions(workflowDefinition);
			ConfigureEventHandlers();

			return _app;
		}

		/// <summary>
		/// Resumes the specified entity.
		/// </summary>
		/// <param name="eventName">Name of the event.</param>
		/// <returns></returns>
		public WorkflowResults Resume(string eventName) {
			VerifyHttpContext();

			_waitHandle = new AutoResetEvent(false);

			Activity activityDB;
			string xamlDB;
			string xamlDesignDB;
			if (!WorkflowHelper.LoadActivityFromDatabase(_entity.InstanceId, out activityDB, out xamlDB, out xamlDesignDB)) {
				Result = WorkflowResults.CreateExceptionWorkflowResults(_entity.InstanceId, new InvalidWorkflowException(String.Format("Cannot resume the workflow {0} as it does not exist in persistence store", _entity.InstanceId)));
				return Result;
			}
			_workflowDefinition.WorkflowActivity = activityDB;

			_app = ConfigureWorkflowApplication(_workflowDefinition, null);
			_app.Load(_entity.InstanceId);

			var retVal = _app.ResumeBookmark(eventName, new WorkflowResumeArgument {
				Entity = _entity,
				CurrentHttpContext = HttpContext.Current
			});

			if (retVal == BookmarkResumptionResult.NotFound) {
				Result = WorkflowResults.CreateBookmarkNotFoundWorkflowResults(_entity.InstanceId, eventName);
				return Result;
			}
			if (retVal == BookmarkResumptionResult.NotReady) {
				Result = WorkflowResults.CreateBookmarkNotReadyWorkflowResults(_entity.InstanceId, eventName);
				return Result;
			}

			_waitHandle.WaitOne();

			return Result;
		}


		/// <summary>
		/// Resumes the pending workflows
		/// </summary>
		/// <returns></returns>
		public WorkflowResults ResumePending(Guid instanceId) {
			VerifyHttpContext();

			_waitHandle = new AutoResetEvent(false);

			Activity activityDB;
			string xamlDB;
			string xamlDesignDB;
			if (!WorkflowHelper.LoadActivityFromDatabase(instanceId, out activityDB, out xamlDB, out xamlDesignDB)) {
				Result = WorkflowResults.CreateExceptionWorkflowResults(instanceId, new InvalidWorkflowException(String.Format("Cannot resume the workflow {0} as it does not exist in persistence store", instanceId)));
				return Result;
			}
			_workflowDefinition.WorkflowActivity = activityDB;

			ConfigureReferences();

			_app = ConfigureWorkflowApplication(_workflowDefinition, null);
			_app.Load(instanceId);
			_app.Run();

			_waitHandle.WaitOne();

			return Result;
		}



		/// <summary>
		/// Creates and starts a specific workflow instance that has an Entity (BaseBusinessEntity) argument.
		/// </summary>
		/// <returns></returns>
		public WorkflowResults Start() {
			return Start(String.Empty);
		}

		/// <summary>
		/// Creates and starts a specific workflow instance that has an Entity (BaseBusinessEntity) argument.
		/// </summary>
		/// <param name="initialState">The initial state of the wotkflow. If value is not empty the wrapper activity will position the initial state to this value.</param>
		/// <returns></returns>
		public WorkflowResults Start(string initialState) {
			VerifyHttpContext();

			_waitHandle = new AutoResetEvent(false);

			// mandatory Entity argument added to any workflow and also this argument should be added manually as a InArgument of the concrete business entity type.
			var workflowInputs = new Dictionary<string, object>{
			                                            	{WorkflowHelper.const_entity_global_variable, _entity}
														};

			_app = ConfigureWorkflowApplication(_workflowDefinition, workflowInputs, initialState);

			WorkflowHelper.SaveWorkflowInstanceDefinitionToDatabase(_app.Id, _workflowDefinition);

			_app.Run();
			_waitHandle.WaitOne();

			return Result;
		}
		/// <summary>
		/// Verifies the HTTP context.
		/// </summary>
		private void VerifyHttpContext() {
			if (HttpContext.Current == null)
				throw new InvalidOperationException("HttpContext.Current is null");
		}

		/// <summary>
		/// Event handler when the WorkflowApplication raises Aborted.
		/// </summary>
		/// <param name="args"></param>
		private void WorkflowApplicationOnAborted(WorkflowApplicationAbortedEventArgs args) {
			Result = WorkflowResults.CreateAbortedWorkflowResults(args);
			_waitHandle.Set();
		}

		/// <summary>
		/// Event handler when the WorkflowApplication raises Completed.
		/// </summary>
		/// <param name="args"></param>
		private void WorkflowApplicationOnCompleted(WorkflowApplicationCompletedEventArgs args) {
			// mark the instance as non attached to workflow.
			WorkflowHelper.WorkflowDetachInstance(args.InstanceId);
			Result = WorkflowResults.CreateCompletedWorkflowResults(args);
		}

		/// <summary>
		/// Event handler when the WorkflowApplication raises Idle.
		/// </summary>
		/// <param name="args">The <see cref="System.Activities.WorkflowApplicationIdleEventArgs"/> instance containing the event data.</param>
		private void WorkflowApplicationOnIdle(WorkflowApplicationIdleEventArgs args) {
			Result = WorkflowResults.CreateIdleWorkflowResults(args);
		}

		/// <summary>
		/// Event handler when the WorkflowApplication raises PersistableIdle.
		/// </summary>
		/// <param name="args"></param>
		/// <returns></returns>
		private PersistableIdleAction WorkflowApplicationOnPersistableIdle(WorkflowApplicationIdleEventArgs args) {
			return PersistableIdleAction.Unload;
		}

		/// <summary>
		/// Event handler when the WorkflowAppliation raises an handled exception.
		/// </summary>
		/// <param name="args"></param>
		/// <returns></returns>
		private UnhandledExceptionAction WorkflowApplicationOnUnhandledException(WorkflowApplicationUnhandledExceptionEventArgs args) {
			//WorkflowHelper.SetWorkflowResults(args.InstanceId, WorkflowResults.CreateUnhandledExceptiondWorkflowResults(args));
			//return UnhandledExceptionAction.Abort;
			return UnhandledExceptionAction.Abort;
		}

		/// <summary>
		/// Event handler when the WorkflowApplication raises Unloaded.
		/// </summary>
		/// <param name="args"></param>
		private void WorkflowApplicationOnUnloaded(WorkflowApplicationEventArgs args) {
			_waitHandle.Set();
		}
	}
}

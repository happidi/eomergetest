﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Activities;
using System.Activities.Statements;
using Vizelia.FOL.BusinessEntities;
using System.Web;
using Microsoft.VisualBasic.Activities;

namespace Vizelia.FOL.WF.Common {
	/// <summary>
	/// A custom activity that encapsulates a state machine for the following purposes:
	/// 1. Being able to append an Entity argument by default to every state machine
	/// 2. Being able to choose a different Initial State then the one defined in the original Workflow Definition
	/// </summary>
	public class StateMachineWrapperActivity : Activity {
		private StateMachine _stateMachine;

		/// <summary>
		/// Gets or sets the entity.
		/// </summary>
		/// <value>
		/// The main business entity associated with this workflow
		/// </value>
		public InArgument<BaseBusinessEntity> EntityArg { get; set; }


		/// <summary>
		/// States the machine wrapper.
		/// </summary>
		/// <param name="stateMachine">The original state machine.</param>
		/// <param name="currentContext">The current http context.</param>
		public StateMachineWrapperActivity(StateMachine stateMachine, HttpContext currentContext) {
			_stateMachine = stateMachine;

			var sequence = new Sequence {
				Activities = { new HttpContextSetterActivity(currentContext), _stateMachine }
			};

			Implementation = () => sequence;
		}


	}
}

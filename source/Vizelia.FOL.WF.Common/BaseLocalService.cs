﻿using System;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Vizelia.FOL.BusinessLayer.Broker;

namespace Vizelia.FOL.WF.Common {
	/// <summary>
	/// Base class for workflow local services.
	/// </summary>
	/// <typeparam name="TEntity">The type of the entity.</typeparam>
	public abstract class BaseLocalService<TEntity> : IBaseLocalService<TEntity>
		where TEntity : BaseBusinessEntity, IWorkflowEntity, new() {

		/// <summary>
		/// Resumes the pending workflows.
		/// </summary>
		public void ResumePendingWorkflows() {
			WorkflowInstanceDefinitionBroker broker = Helper.CreateInstance<WorkflowInstanceDefinitionBroker>();
			var guids = broker.GetListPendingWorkflows();

			foreach (var instanceId in guids) {
				var entity = GetItemByInstanceId(instanceId);
				if (entity != null) {
					var workflowDefinition = ResolveWorkflowDefinition(entity);
					WorkflowHelper.ResumePending(entity, workflowDefinition);
				}
			}
		}

		/// <summary>
		/// Raises the event.
		/// </summary>
		/// <param name="eventName">The event name.</param>
		/// <param name="entity">The entity.</param>
		/// <returns></returns>
		public FormResponse RaiseEvent(string eventName, TEntity entity) {
			// raises the event on the workflow. The result should not be exposed directly to the outside.
			WorkflowResults wfResult = RaiseEventInternal(eventName, entity);
			// transforms the result from the workflow to an understandable FormResponse result.
			var retVal = BuildResponse(wfResult);
			return retVal;
		}


		/// <summary>
		/// Raises the event.
		/// </summary>
		/// <param name="eventName">The event name.</param>
		/// <param name="entity">The entity.</param>
		/// <returns></returns>
		private WorkflowResults RaiseEventInternal(string eventName, TEntity entity) {
			WorkflowResults result = null;
			try {
				WorkflowDefinition workflowDefinition;
				if (entity.InstanceId == Guid.Empty) {
					workflowDefinition = ResolveWorkflowDefinition(entity);
					if (workflowDefinition == null) {
						return WorkflowResults.CreateWorkflowResolveExceptionWorkflowResults();
					}
					result = WorkflowHelper.Start(entity, workflowDefinition);
					if (!result.Success)
						return result;
					entity = GetItemByInstanceId(entity.InstanceId);
				}
				if (!String.IsNullOrEmpty(eventName)) {
					workflowDefinition = ResolveWorkflowDefinition(entity);
					if (workflowDefinition == null) {
						return WorkflowResults.CreateWorkflowResolveExceptionWorkflowResults();
					}
					result = WorkflowHelper.Resume(entity, workflowDefinition, eventName);
					VerifyResults(result, WorkflowStatus.Running);
				}
				else {
					return result;
				}
			}
			catch (Exception ex) {
				result = WorkflowResults.CreateExceptionWorkflowResults(entity.InstanceId, ex);
			}
			return result;
		}

		/// <summary>
		/// Verifies that a result of a workflow equals a status.
		/// </summary>
		/// <param name="workflowResults">The workflow result to check.</param>
		/// <param name="status">The status expected.</param>
		protected void VerifyResults(WorkflowResults workflowResults, WorkflowStatus status) {
			if (workflowResults.Status != status) {
				if (workflowResults.Exception != null) {
					throw workflowResults.Exception;
				}
				string expected = status.ToString();
				string actual = workflowResults.Status.ToString();
				throw new InvalidOperationException(String.Format("Workflow {0} expected status {1} actual status {2}", workflowResults.InstanceId, expected, actual));
			}
		}

		/// <summary>
		/// Virtual function for retreiving the entity based on it's instanceId.
		/// </summary>
		/// <param name="instanceId"></param>
		/// <returns></returns>
		public abstract TEntity GetItemByInstanceId(Guid instanceId);


		/// <summary>
		/// Virtual function for saving the item in database.
		/// </summary>
		/// <param name="entity">The item.</param>
		/// <returns></returns>
		public abstract TEntity SaveItem(TEntity entity);

		/// <summary>
		/// Virtual function for resolving the Workflow Definition that the local service should run based eventually on the entity properties.
		/// </summary>
		/// <param name="entity">The item.</param>
		/// <returns></returns>
		public abstract WorkflowDefinition ResolveWorkflowDefinition(TEntity entity);

		/// <summary>
		/// Builds a form response from a workflow result.
		/// </summary>
		/// <param name="workflowResults"></param>
		/// <returns></returns>
		protected FormResponse BuildResponse(WorkflowResults workflowResults) {
			FormResponse response = new FormResponse { success = workflowResults.Success };
			if (!workflowResults.Success && workflowResults.Exception != null)
				response.msg = workflowResults.Exception.ToString();
			response.data = GetItemByInstanceId(workflowResults.InstanceId);
			return response;
		}




		/// <summary>
		/// Sets state and update workflow definition.
		/// The trick here is to force a new workflow start while positionning its initial state to the targetStateName.
		/// This will change the workflow instanceId.
		/// </summary>
		/// <param name="targetStateName">The name of the state to transition to.</param>
		/// <param name="entity">The entity</param>
		/// <returns></returns>
		public FormResponse SetState(string targetStateName, TEntity entity) {
			// this will force a Start on the workflow
			WorkflowHelper.WorkflowDetachInstance(entity.InstanceId);
			entity.InstanceId = Guid.Empty;
			var workflowDefinition = ResolveWorkflowDefinition(entity);
			var wfResult = WorkflowHelper.Start(entity, workflowDefinition, targetStateName);
			// transforms the result from the workflow to an understandable FormResponse result.
			var retVal = BuildResponse(wfResult);
			return retVal;
		}
	}
}

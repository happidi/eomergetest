﻿using System;
using System.Activities;
using System.Activities.Statements;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.WF.Common {
	/// <summary>
	/// A class for finding workflows in bin directory.
	/// </summary>
	internal class WorkflowFinder {
		/// <summary>
		/// Gets or sets the path.
		/// </summary>
		/// <value>
		/// The path.
		/// </value>
		public string Path { get; set; }

		/// <summary>
		/// Initializes a new instance of the <see cref="WorkflowFinder"/> class.
		/// </summary>
		public WorkflowFinder() {
			Path = AppDomain.CurrentDomain.BaseDirectory + "bin";
		}

		/// <summary>
		/// Gets the workflows from path.
		/// </summary>
		/// <returns></returns>
		public List<WorkflowAssemblyDefinition> GetWorkflowsFromPath() {
			AppDomain.CurrentDomain.ReflectionOnlyAssemblyResolve += CurrentDomain_ReflectionOnlyAssemblyResolve;
			List<WorkflowAssemblyDefinition> retVal = new List<WorkflowAssemblyDefinition>();
			DirectoryInfo directoryInfo = new DirectoryInfo(Path);
			var filesInfos = directoryInfo.GetFiles("Vizelia*.dll");
			foreach (var fileInfo in filesInfos) {
				try {
					var assembly = Assembly.ReflectionOnlyLoadFrom(fileInfo.FullName);
					if (!assembly.FullName.Contains("Vizelia.FOL.WF.Common")) {
						retVal.AddRange(from type in assembly.GetTypes()
										where type.BaseType != null && type.BaseType.FullName == typeof(Activity).FullName
											  && type.Name != ""
										let entityType = WorkflowHelper.GetWorkflowEntityType(type)
										select new WorkflowAssemblyDefinition {
											AssemblyShortName = type.FullName,
											AssemblyQualifiedName = type.AssemblyQualifiedName,
											EntityShortName = entityType != null ? entityType.Name : null,
											EntityQualifiedName = entityType != null ? entityType.FullName : null
										});
					}

				}
				catch { }
			}

			return retVal;
		}

		/// <summary>
		/// Handles the ReflectionOnlyAssemblyResolve event of the CurrentDomain control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="args">The <see cref="System.ResolveEventArgs"/> instance containing the event data.</param>
		/// <returns></returns>
		private Assembly CurrentDomain_ReflectionOnlyAssemblyResolve(object sender, ResolveEventArgs args) {
			string[] parts = args.Name.Split(',');
			string file = Path + "\\" + parts[0].Trim() + ".dll";
			if (File.Exists(file)) {
				Assembly asm = Assembly.ReflectionOnlyLoadFrom(file);
				return asm;
			}
			return Assembly.ReflectionOnlyLoad(args.Name);
		}
	}
}

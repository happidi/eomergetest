﻿using System;
using System.Activities.Tracking;
using System.Collections.Generic;
using System.Reflection;
using System.Threading;
using System.Web;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.WF.Common {
	/// <summary>
	/// When the tracked activity is a State activity which is executing (hence about to be Idle)
	/// we persist the entity using the appropriate wokrflow Extension
	/// </summary>
	public class SaveEntityTrackingParticipant : TrackingParticipant {

		private Dictionary<Guid, BaseBusinessEntity> _internalEntityCollection;

		/// <summary>
		/// Ctor
		/// </summary>
		public SaveEntityTrackingParticipant() {
			_internalEntityCollection = new Dictionary<Guid, BaseBusinessEntity>();
		}


		/// <summary>
		/// Gets or sets the local service.
		/// </summary>
		/// <value>The local service.</value>
		public object LocalService { get; set; }

		/// <summary>
		/// Gets or sets the current http context.
		/// </summary>
		/// <value>
		/// The current http context.
		/// </value>
		public HttpContext CurrentContext { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether the workflow instance is starting.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if this instance is starting; otherwise, <c>false</c>.
		/// </value>
		public bool IsStarting { get; set; }

		/// <summary>
		/// Gets or sets the name of the application.
		/// </summary>
		/// <value>
		/// The name of the application.
		/// </value>
		public string ApplicationName { get; set; }

		/// <summary>
		/// When implemented in a derived class, used to synchronously process the tracking record.
		/// </summary>
		/// <param name="record">The generated tracking record.</param>
		/// <param name="timeout">The time period after which the provider aborts the attempt.</param>
		protected override void Track(TrackingRecord record, TimeSpan timeout) {

			HttpContext.Current = CurrentContext;
			Thread.CurrentPrincipal = CurrentContext.User;
			ContextHelper.ApplicationName = ApplicationName;

			// we are saving the entity each time we enter a state that is not final.
			// the entity is available in the dictionary _internalEntityCollection because we are always coming from a resume call back from our custom EventHandlerActivity.
			// As it is a pointer to the entity any assign operation in the workflow itself will also be visible here.
			if (record is ActivityStateRecord) {
				var activityStateRecord = record as ActivityStateRecord;
				if (activityStateRecord.State == "Executing" && activityStateRecord.Activity.TypeName == WorkflowHelper.const_typename_internalstate) {
					var instance = activityStateRecord.Activity.GetType().GetProperty("Instance", BindingFlags.Instance | BindingFlags.NonPublic).GetValue(activityStateRecord.Activity, null);
					var internalActivity = instance.GetType().GetProperty("Activity", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public).GetValue(instance, null);
					var isFinal = (bool)internalActivity.GetType().GetProperty("IsFinal").GetValue(internalActivity, null);
					if (!isFinal)
						SaveItem(record.InstanceId, activityStateRecord.Activity.Name);
				}
			}

			if (record is BusinessEntityTrackingRecord) {
				_internalEntityCollection[record.InstanceId] = ((BusinessEntityTrackingRecord)record).Entity;
			}

			else if (record is WorkflowInstanceRecord) {
				var workflowInstanceRecord = record as WorkflowInstanceRecord;
				// when starting the instance we want to save when it will besome idle or completed.
				if (workflowInstanceRecord.State == "Started") {
					IsStarting = true;
				}
				if ((workflowInstanceRecord.State == "Idle" || workflowInstanceRecord.State == "Completed") && this.IsStarting) {
					SaveItem(record.InstanceId, null);
				}

			}

		}

		/// <summary>
		/// Saves the item calling the local service.
		/// </summary>
		/// <param name="instanceId">The instance id.</param>
		/// <param name="state">The state name.</param>
		private void SaveItem(Guid instanceId, string state) {
			if (_internalEntityCollection.ContainsKey(instanceId)) {
				var entity = (IWorkflowEntity)_internalEntityCollection[instanceId];
				if (!String.IsNullOrEmpty(state))
					entity.State = state;
				MethodInfo mi = LocalService.GetType().GetMethod("SaveItem");
				mi.Invoke(LocalService, new[] { entity });
				//LocalService.SaveItem(_internalEntityCollection[record.InstanceId]);
				//_internalEntityCollection.Remove(instanceId);
			}
		}
	}

}



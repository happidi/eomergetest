﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.WF.Common {

	/// <summary>
	/// The interface for the local service.
	/// </summary>
	/// <typeparam name="TEntity"></typeparam>
	public interface IBaseLocalService<TEntity>
		where TEntity : BaseBusinessEntity, IWorkflowEntity, new()
	{
		/// <summary>
		/// 
		/// </summary>
		/// <param name="stateName"></param>
		/// <param name="entity"></param>
		/// <returns></returns>
		FormResponse SetState(string stateName, TEntity entity);

		/// <summary>
		/// Resumes the pending workflows.
		/// </summary>
		void ResumePendingWorkflows();

		/// <summary>
		/// Raises the event.
		/// </summary>
		/// <param name="eventName">Name of the event.</param>
		/// <param name="entity">The entity.</param>
		/// <returns></returns>
		FormResponse RaiseEvent(string eventName, TEntity entity);

		/// <summary>
		/// Saves the item in database.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <returns></returns>
		TEntity SaveItem(TEntity entity);

		/// <summary>
		/// Gets the entity based on it's instanceId.
		/// </summary>
		/// <param name="instanceId"></param>
		/// <returns></returns>
		TEntity GetItemByInstanceId(Guid instanceId);

		/// <summary>
		/// Resolves the Workflow Definition that the local service should run based eventually on the entity properties.
		/// </summary>
		/// <param name="entity">The item.</param>
		/// <returns></returns>
		WorkflowDefinition ResolveWorkflowDefinition(TEntity entity);

	


	}
}

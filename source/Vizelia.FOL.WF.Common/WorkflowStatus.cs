﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Vizelia.FOL.WF.Common {
	/// <summary>
	///  Enum for workflow status. 
	/// </summary>
	public enum WorkflowStatus {

		/// <summary>
		/// Indicates that the workflow instance is completed.
		/// </summary>
		Completed,

		/// <summary>
		/// Indicates that the workflow instance is terminated.
		/// </summary>
		Terminated,

		/// <summary>
		/// Indicates that the workflow instance is aborted.
		/// </summary>
		Aborted,

		/// <summary>
		/// Indicates that the workflow instance is running.
		/// </summary>
		Running
	}
}

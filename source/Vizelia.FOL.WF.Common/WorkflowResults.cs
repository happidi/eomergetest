﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Activities;
using Vizelia.FOL.Common.Localization;

namespace Vizelia.FOL.WF.Common {
	/// <summary>
	/// A wrapper for workflow results.
	/// </summary>
	public class WorkflowResults {

		private Exception _exception;
		private Guid _instanceId;
		private IDictionary<string, object> _outputs;
		private WorkflowStatus _status;
		private bool _success;



		/// <summary>
		/// Initializes a new instance of the <see cref="WorkflowResults"/> class.
		/// </summary>
		private WorkflowResults() {

		}

		/// <summary>
		/// Gets the exception.
		/// </summary>
		/// <value>The exception.</value>
		public Exception Exception {
			get { return _exception; }
		}

		/// <summary>
		/// Gets the instance id.
		/// </summary>
		/// <value>The instance id.</value>
		public Guid InstanceId {
			get { return _instanceId; }
		}

		/// <summary>
		/// Gets the output parameters.
		/// </summary>
		/// <value>The output parameters.</value>
		public IDictionary<string, object> OutputParameters {
			get { return _outputs; }
		}

		/// <summary>
		/// Gets the status.
		/// </summary>
		/// <value>The status.</value>
		public WorkflowStatus Status {
			get { return _status; }
		}

		/// <summary>
		/// Gets or sets a value indicating whether this <see cref="WorkflowResults"/> is success.
		/// </summary>
		/// <value><c>true</c> if success; otherwise, <c>false</c>.</value>
		public bool Success {
			get { return _success; }
		}




		#region FactoryMethods


		/// <summary>
		/// Creates the completed workflow results.
		/// </summary>
		/// <param name="args">The instance containing the event data.</param>
		/// <returns></returns>
		public static WorkflowResults CreateCompletedWorkflowResults(WorkflowApplicationCompletedEventArgs args) {
			WorkflowResults results = new WorkflowResults {
				_instanceId = args.InstanceId,
				_success = true,
				_status = WorkflowStatus.Running,
				_outputs = args.Outputs
			};
			return results;
		}

		/// <summary>
		/// Creates the bookmark not found workflow results.
		/// </summary>
		/// <param name="instanceId">The instance id.</param>
		/// <param name="bookmark">The bookmark.</param>
		/// <returns></returns>
		public static WorkflowResults CreateBookmarkNotFoundWorkflowResults(Guid instanceId, string bookmark) {
			WorkflowResults results = new WorkflowResults {
				_instanceId = instanceId,
				_success = true,
				_status = WorkflowStatus.Aborted,
				_exception = new InvalidOperationException(String.Format("Bookmark '{0}' was not find for workflow instanceID '{1}'", bookmark, instanceId))
			};
			return results;
		}


		/// <summary>
		/// Creates the bookmark not ready workflow results.
		/// </summary>
		/// <param name="instanceId">The instance id.</param>
		/// <param name="bookmark">The bookmark.</param>
		/// <returns></returns>
		public static WorkflowResults CreateBookmarkNotReadyWorkflowResults(Guid instanceId, string bookmark) {
			WorkflowResults results = new WorkflowResults {
				_instanceId = instanceId,
				_success = true,
				_status = WorkflowStatus.Aborted,
				_exception = new InvalidOperationException(String.Format("Bookmark '{0}' was not ready for workflow instanceID '{1}'", bookmark, instanceId))
			};
			return results;
		}

		/// <summary>
		/// Creates the aborted workflow results.
		/// </summary>
		/// <param name="args">The instance containing the event data.</param>
		/// <returns></returns>
		public static WorkflowResults CreateAbortedWorkflowResults(WorkflowApplicationAbortedEventArgs args) {
			WorkflowResults results = new WorkflowResults {
				_instanceId = args.InstanceId,
				_success = false,
				_status = WorkflowStatus.Aborted,
				_exception = args.Reason
			};
			return results;
		}

		/// <summary>
		/// Creates the unhandled exception workflow results.
		/// </summary>
		/// <param name="args">The instance containing the event data.</param>
		/// <returns></returns>
		public static WorkflowResults CreateUnhandledExceptiondWorkflowResults(WorkflowApplicationUnhandledExceptionEventArgs args) {
			WorkflowResults results = new WorkflowResults {
				_instanceId = args.InstanceId,
				_success = false,
				_status = WorkflowStatus.Aborted,
				_exception = args.UnhandledException
			};
			return results;
		}


		/// <summary>
		/// Creates the idle workflow results.
		/// </summary>
		/// <param name="args">The instance containing the event data.</param>
		/// <returns></returns>
		public static WorkflowResults CreateIdleWorkflowResults(WorkflowApplicationIdleEventArgs args) {
			WorkflowResults results = new WorkflowResults {
				_instanceId = args.InstanceId,
				_success = true,
				_status = WorkflowStatus.Running
			};
			return results;
		}

		/// <summary>
		/// Creates the exception workflow results.
		/// </summary>
		/// <param name="instanceId">The instance id.</param>
		/// <param name="ex">The ex.</param>
		/// <returns></returns>
		public static WorkflowResults CreateExceptionWorkflowResults(Guid instanceId, Exception ex) {
			WorkflowResults results = new WorkflowResults {
				_instanceId = instanceId,
				_success = false,
				_status = WorkflowStatus.Aborted,
				_exception = ex
			};
			return results;
		}

		/// <summary>
		/// Creates the workflow resolve exception workflow results.
		/// </summary>
		/// <returns></returns>
		public static WorkflowResults CreateWorkflowResolveExceptionWorkflowResults() {
			WorkflowResults results = new WorkflowResults {
				_success = false,
				_status = WorkflowStatus.Aborted,
				_exception = new Exception(Langue.error_workflow_resolvedefinition)
			};
			return results;

		}
		#endregion
	}
}


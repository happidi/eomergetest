﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Vizelia.FOL.BusinessEntities;
using System.Activities;
using System.Activities.Statements;
using System.Configuration;
using System.Activities.DurableInstancing;
using System.Threading;
using Vizelia.FOL.Common;
using Microsoft.VisualBasic.Activities;
using System.Activities.Hosting;
using System.ComponentModel;
using System.Reflection;
using Vizelia.FOL.Providers;
using System.Web;
using System.Web.Security;
using Vizelia.FOL.BusinessLayer.Broker;
using System.Xaml;
using System.IO;
using System.Activities.XamlIntegration;
using System.Xml;
using WorkflowInstance = Vizelia.FOL.BusinessEntities.WorkflowInstance;

namespace Vizelia.FOL.WF.Common {
	/// <summary>
	/// Singleton Helper for managing the workflow runtime.
	/// </summary>
	public static class WorkflowHelper {

		internal const string const_entity_global_variable = "EntityArg";
		internal const string const_workflow_database = "dns_vizelia_wf";
		internal const string const_typename_internalstate = "System.Activities.Statements.InternalState";


		/// <summary>
		/// Takes an existing workflow definition (type) and returns a definition with a the same starting state as defined in the definition.
		/// Invoking this method instead of running the real definition is required for consistency when not starting with the original initial state.
		/// </summary>
		/// <param name="workflowDefinition"></param>
		/// <returns></returns>
		internal static Activity GetDynamicWorkflowDefinition(Activity workflowDefinition) {
			var retVal = GetDynamicWorkflowDefinition(workflowDefinition, String.Empty);
			return retVal;
		}



		/// <summary>
		/// Takes an existing workflow definition (type) and returns a definition with a different starting state.
		/// Invoking this method instead of running the real definition is required for achieving SetState capability.
		/// </summary>
		/// <param name="workflowDefinitionActivity">The workflow definition activity.</param>
		/// <param name="startingStateName">Name of the starting state.</param>
		/// <returns></returns>
		internal static Activity GetDynamicWorkflowDefinition(Activity workflowDefinitionActivity, string startingStateName) {

			// find the state machine in the original definition
			StateMachine stateMachine = GetStateMachine(workflowDefinitionActivity);
			// WorkflowInspectionServices.GetActivities(workflowDefinitionActivity).OfType<StateMachine>().FirstOrDefault();

			if (stateMachine == null)
				throw new InvalidOperationException("Workflow xaml does not contain a state machine workflow");

			if (!string.IsNullOrEmpty(startingStateName)) {
				// find the state that should be set as the new starting state 
				State startingState = stateMachine.States.Where(s => s.DisplayName == startingStateName).SingleOrDefault();

				if (startingState == default(State)) {
					throw new InvalidOperationException(String.Format("Workflow xaml unable to find starting state called '{0}'", startingStateName));
				}
				// set the new starting state
				stateMachine.InitialState = startingState;
			}

			Activity retVal = new StateMachineWrapperActivity(stateMachine, HttpContext.Current);

			return retVal;
		}


		/// <summary>
		/// Gets the state machine from a type activity.
		/// </summary>
		/// <param name="type">The type.</param>
		/// <returns></returns>
		public static StateMachine GetStateMachine(Type type) {
			Activity inputActivity = Activator.CreateInstance(type) as Activity;
			var retVal = GetStateMachine(inputActivity);
			return retVal;
		}

		/// <summary>
		/// Gets the state machine.
		/// </summary>
		/// <param name="activity">The activity.</param>
		/// <returns></returns>
		public static StateMachine GetStateMachine(Activity activity) {
			var list = WorkflowInspectionServices.GetActivities(activity).ToList();
			return list.OfType<StateMachine>().FirstOrDefault();
		}


		/// <summary>
		/// Gets the state machine events.
		/// </summary>
		/// <param name="instanceId">The instance id.</param>
		/// <param name="workflowDefinition">The workflow definition.</param>
		/// <param name="userName">Name of the user.</param>
		/// <returns></returns>
		public static List<WorkflowActionElement> GetStateMachineEventsTransitions(Guid instanceId, WorkflowDefinition workflowDefinition, string userName) {
			string currentState = GetCurrentState(instanceId);
			var retVal = new List<WorkflowActionElement>();
			if (workflowDefinition == null)
				return retVal;
			try {
				Activity activityFromType = Activator.CreateInstance(workflowDefinition.WorkflowType) as Activity;
				string xamlFromType = GetXamlFromActivity(GetStateMachine(activityFromType));
				string xamlDesignFromType = GetXamlDesign(workflowDefinition);
				Activity activityFromDB;
				string xamlFromDB;
				string xamlDesignFromDB;
				if (!LoadActivityFromDatabase(instanceId, out activityFromDB, out xamlFromDB, out xamlDesignFromDB)
					&& Roles.IsUserInRole("Workflow - Read Write")) {
					// The workflow exists in database but the HasWorkflow field is false (means that workflow completed).
					if (activityFromDB != null) {
						retVal.Add(new WorkflowActionElement { Id = "workflow_restart", MsgCode = "msg_workflow_restart", IconCls = "viz-icon-small-workflow-restart" });
					}
					// The workflow does not exist in database.
					else
						retVal.Add(new WorkflowActionElement { Id = "workflow_update_definition", MsgCode = "msg_workflow_updatedefinition", IconCls = "viz-icon-small-workflow-updatedefinition" });
					return retVal;
				}
				// the workflow exists in database but the definition is different.
				if (xamlFromDB != xamlFromType || xamlDesignFromType != xamlDesignFromDB) {
					retVal.Add(new WorkflowActionElement { Id = "workflow_update_definition", MsgCode = "msg_workflow_updatedefinition", IconCls = "viz-icon-small-workflow-updatedefinition" });
				}

				workflowDefinition.WorkflowActivity = activityFromDB;
				foreach (var vbReference in workflowDefinition.AssemblyAndNamespaceDictionary) {
					VisualBasicSettings.Default.ImportReferences.Add(new VisualBasicImportReference { Assembly = vbReference.Key, Import = vbReference.Value });
				}
				StateMachine stateMachine = GetStateMachine(workflowDefinition.WorkflowActivity);

				var state = (from p in stateMachine.States
							 where p.DisplayName == currentState
							 select p).FirstOrDefault();

				if (state != null)
					foreach (var transition in state.Transitions) {
						string eventName = GetPropertyValueFromTransition(transition, "EventName");
						string msgCode = GetPropertyValueFromTransition(transition, "MsgCode");
						string iconCls = GetPropertyValueFromTransition(transition, "IconCls");
						string preAction = GetPropertyValueFromTransition(transition, "PreAction");
						bool resultCondition = true;
						if (transition.Condition != null) {
							Activity<bool> vbExpression = transition.Condition;
							var conditionWrapper = new ConditionWrapperActivity(vbExpression);
							try {
								resultCondition = WorkflowInvoker.Invoke(conditionWrapper,
									new Dictionary<string, object> { { const_entity_global_variable, new WorkflowContext { InstanceId = instanceId } } });
							}
							catch { }
						}
						if (resultCondition)
							retVal.Add(new WorkflowActionElement {
								Id = eventName,
								Value = eventName,
								AssemblyQualifiedName = workflowDefinition.WorkflowType.FullName,
								IconCls = iconCls,
								MsgCode = msgCode ?? eventName,
								PreAction = preAction
							});
					}
			}
			catch { ;}

			return retVal.Distinct(new BaseBusinessEntityComparer<WorkflowActionElement>()).Where(p => !String.IsNullOrEmpty(p.Id)).Where(p => IsAllowed(p.AssemblyQualifiedName + "." + p.Value)).OrderBy(p => p.Label).ToList();
		}


		/// <summary>
		/// Determines whether the specified operation name is allowed.
		/// </summary>
		/// <param name="operationName">Name of the operation.</param>
		/// <returns>
		///   <c>true</c> if the specified operation name is allowed; otherwise, <c>false</c>.
		/// </returns>
		public static bool IsAllowed(string operationName) {
			var isAllowed = Roles.IsUserInRole(operationName);
			return isAllowed;
		}

		/// <summary>
		/// Gets all possible events from an assembly.
		/// </summary>
		/// <param name="assemblyName">The full name of the assembly.
		/// <example>Vizelia.FOL.Workflow.Services.Machine, Vizelia.FOL.WF.Workflows, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null</example>
		/// </param>
		/// <returns></returns>
		public static List<WorkflowActionElement> GetStateMachineEventsTransitions(string assemblyName) {
			var type = Type.GetType(assemblyName, true);
			var stateMachine = GetStateMachine(type);
			var retVal = (from state in stateMachine.States
						  from transition in state.Transitions
						  let eventName = GetPropertyValueFromTransition(transition, "EventName")
						  let msgCode = GetPropertyValueFromTransition(transition, "MsgCode")
						  let iconCls = GetPropertyValueFromTransition(transition, "IconCls")
						  let preAction = GetPropertyValueFromTransition(transition, "PreAction")
						  select new WorkflowActionElement {
							  Id = eventName,
							  Value = eventName,
							  IconCls = iconCls,
							  MsgCode = msgCode ?? eventName,
							  PreAction = preAction,
							  AssemblyQualifiedName = type.AssemblyQualifiedName,
							  AssemblyShortName = type.FullName
						  });

			return retVal.Distinct(new BaseBusinessEntityComparer<WorkflowActionElement>()).Where(p => !String.IsNullOrEmpty(p.Id)).OrderBy(p => p.Label).ToList();
		}

		/// <summary>
		/// Updates the instance definition.
		/// </summary>
		/// <param name="instanceId">The instance id.</param>
		/// <param name="currentState">State of the current.</param>
		/// <param name="workflowDefinition">The workflow definition.</param>
		/// <returns></returns>
		public static bool UpdateInstanceDefinition(Guid instanceId, string currentState, WorkflowDefinition workflowDefinition) {
			Activity activityFromType = Activator.CreateInstance(workflowDefinition.WorkflowType) as Activity;
			string xamlFromType = GetXamlFromActivity(GetStateMachine(activityFromType));

			return true;
		}


		/// <summary>
		/// Gets the property value from a transition.
		/// </summary>
		/// <param name="transition">The transition.</param>
		/// <param name="propertyName">Name of the property.</param>
		/// <returns></returns>
		private static string GetPropertyValueFromTransition(Transition transition, string propertyName) {
			try {
				PropertyInfo property = transition.Trigger.GetType().GetProperty(propertyName);
				string retVal = (string)property.GetValue(transition.Trigger, null);
				return retVal;
			}
			catch (Exception) {
				return null;
			}
		}

		/// <summary>
		/// Resumes a pending workflow.
		/// </summary>
		internal static void ResumePending(IWorkflowEntity entity, WorkflowDefinition workflowDefinition) {
			var executor = new WorkflowExecutor(entity, workflowDefinition);
			executor.ResumePending(entity.InstanceId);
		}

		/// <summary>
		/// Resumes the pending workfdlows.
		/// </summary>
		public static void ResumePending() {
			var workflowEntityType = typeof(IWorkflowEntity);
			var workflowTypes = (typeof(BaseBusinessEntity)).Assembly.GetTypes().Where(
				p =>
					workflowEntityType.IsAssignableFrom(p) == true &&
					p.IsSubclassOf(typeof(BaseBusinessEntity))
				).ToList();

			foreach (var workflowType in workflowTypes) {
				Type baseLocaService = typeof(IBaseLocalService<>);
				Type genericParameter = baseLocaService.MakeGenericType(workflowType);
				dynamic localService = WorkflowExtensionService.GetExtension(genericParameter);
				if (localService != null)
					localService.ResumePendingWorkflows();
			}

		}


		/// <summary>
		/// Gets the activity from xaml.
		/// </summary>
		/// <param name="xaml">The xaml.</param>
		/// <returns></returns>
		private static Activity GetActivityFromXaml(string xaml) {
			//Assembly asm = Assembly.LoadFile(@"C:\temp\WorkflowFourCompleteMockup\Vizelia.FOL.FrontEnd\bin\Cisco.Workflows.dll");
			//VisualBasicSettings.Default.ImportReferences.Add(new VisualBasicImportReference() { Assembly = asm.FullName, Import = "Cisco.Workflows" });
			var sr = new StringReader(xaml);

			//Change LocalAssembly to where the Activities reside
			var xamlSettings = new XamlXmlReaderSettings();// { LocalAssembly = asm }};

			var xamlReader = ActivityXamlServices.CreateReader(new XamlXmlReader(sr, xamlSettings));
			var result = XamlServices.Load(xamlReader);
			var activity = result as Activity;
			return activity;
		}

		/// <summary>
		/// Gets the xaml from an activity.
		/// </summary>
		/// <param name="activity">The activity.</param>
		/// <returns></returns>
		private static string GetXamlFromActivity(this Activity activity) {
			StringBuilder xaml = new StringBuilder();

			using (XmlWriter xmlWriter = XmlWriter.Create(
				xaml,
				new XmlWriterSettings { Indent = true, OmitXmlDeclaration = true, }))

			using (XamlWriter xamlWriter = new XamlXmlWriter(
				xmlWriter,
				new XamlSchemaContext()))

			using (XamlWriter xamlServicesWriter =
				ActivityXamlServices.CreateBuilderWriter(xamlWriter)) {
				ActivityBuilder activityBuilder = new ActivityBuilder {
					Implementation = activity,
				};
				// we must explicitly add the argument to the persisted Xaml
				activityBuilder.Properties.Add(new DynamicActivityProperty {
					Name = const_entity_global_variable,
					Type = typeof(InArgument<BaseBusinessEntity>)
				});

				XamlServices.Save(xamlServicesWriter, activityBuilder);
			}

			return xaml.ToString();
		}


		/// <summary>
		/// Gets the xaml design.
		/// </summary>
		/// <param name="workflowDefinition">The workflow definition.</param>
		/// <returns></returns>
		private static string GetXamlDesign(WorkflowDefinition workflowDefinition) {
			XmlDocument xdoc = new XmlDocument();
			var xamlFile = AppDomain.CurrentDomain.BaseDirectory + WorkflowViewerService.GetXamlFolder() + "\\" + workflowDefinition.WorkflowType.Name + ".xaml";
			xdoc.Load(xamlFile);
			return xdoc.OuterXml;
		}


		/// <summary>
		/// Loads the activity from database.
		/// </summary>
		/// <param name="instanceId">The instance id.</param>
		/// <param name="activity">The activity from database.</param>
		/// <param name="xaml">The xaml activity from database.</param>
		/// <param name="xamlDesign">The xaml design.</param>
		/// <returns>True if the instance is associated with a workflow, false otherwise.</returns>
		internal static bool LoadActivityFromDatabase(Guid instanceId, out Activity activity, out string xaml, out string xamlDesign) {
			activity = null;
			xaml = null;
			xamlDesign = null;
			WorkflowInstanceDefinitionBroker broker = Helper.CreateInstance<WorkflowInstanceDefinitionBroker>();
			WorkflowInstanceDefinition workflowInstanceDefinition = broker.GetItemByInstanceId(instanceId);
			if (workflowInstanceDefinition == null)
				return false;
			xaml = workflowInstanceDefinition.Xaml;
			xamlDesign = workflowInstanceDefinition.XamlDesign;
			var activityFromXaml = GetActivityFromXaml(xaml);
			activity = activityFromXaml;
			//activity = WorkflowHelper.GetDynamicWorkflowDefinition(activityFromXaml);

			return workflowInstanceDefinition.HasWorkflow;
		}


		/// <summary>
		/// Resumes the specified entity.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="workflowDefinition">The workflow definition.</param>
		/// <param name="eventName">Name of the event.</param>
		/// <returns></returns>
		public static WorkflowResults Resume(IWorkflowEntity entity, WorkflowDefinition workflowDefinition, string eventName) {
			WorkflowExecutor executor = new WorkflowExecutor(entity, workflowDefinition);
			var retVal = executor.Resume(eventName);
			return retVal;
		}


		/// <summary>
		/// Saves the workflow instance definition to database.
		/// </summary>
		/// <param name="instanceId">The instance id.</param>
		/// <param name="workflowDefinition">The workflow definition.</param>
		internal static void SaveWorkflowInstanceDefinitionToDatabase(Guid instanceId, WorkflowDefinition workflowDefinition) {
			WorkflowInstanceDefinitionBroker broker = Helper.CreateInstance<WorkflowInstanceDefinitionBroker>();
			Activity workflowActivity = Activator.CreateInstance(workflowDefinition.WorkflowType) as Activity;
			broker.Save(new WorkflowInstanceDefinition {
				InstanceId = instanceId,
				InitialState = workflowDefinition.InitialState,
				HasWorkflow = true,
				Xaml = GetXamlFromActivity(GetStateMachine(workflowActivity)),
				XamlDesign = GetXamlDesign(workflowDefinition),
				WorkflowAssemblyQualifiedName = workflowDefinition.WorkflowType.AssemblyQualifiedName
			});
		}


		/// <summary>
		/// Detaches the instance from workflow.
		/// </summary>
		/// <param name="instanceId">The instance id.</param>
		internal static void WorkflowDetachInstance(Guid instanceId) {
			WorkflowInstanceDefinitionBroker broker = Helper.CreateInstance<WorkflowInstanceDefinitionBroker>();
			broker.WorkflowDetachInstance(instanceId);
		}


		/// <summary>
		/// Creates and starts a specific workflow instance that has an Entity (BaseBusinessEntity) argument.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="workflowDefinition">The workflow definition.</param>
		/// <returns></returns>
		public static WorkflowResults Start(IWorkflowEntity entity, WorkflowDefinition workflowDefinition) {
			WorkflowExecutor executor = new WorkflowExecutor(entity, workflowDefinition);
			var retVal = executor.Start();
			return retVal;
		}

		/// <summary>
		/// Creates and starts a specific workflow instance that has an Entity (BaseBusinessEntity) argument.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="workflowDefinition">The workflow definition.</param>
		/// <param name="initialState">The initial state.</param>
		/// <returns></returns>
		public static WorkflowResults Start(IWorkflowEntity entity, WorkflowDefinition workflowDefinition, string initialState) {
			WorkflowExecutor executor = new WorkflowExecutor(entity, workflowDefinition);
			var retVal = executor.Start(initialState);
			return retVal;
		}

		/// <summary>
		/// Gets the argument entity.
		/// </summary>
		/// <param name="context">The context.</param>
		/// <returns></returns>
		public static BaseBusinessEntity GetArgumentEntity(WorkflowDataContext context) {
			return GetArgumentValue<BaseBusinessEntity>(const_entity_global_variable, context);
		}

		/// <summary>
		/// Sets the argument entity.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="dataContext">The data context.</param>
		public static void SetArgumentEntity(BaseBusinessEntity entity, WorkflowDataContext dataContext) {
			SetArgumentValue(const_entity_global_variable, entity, dataContext);
		}


		/// <summary>
		/// Gets the argument value.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="name">The name.</param>
		/// <param name="dataContext">The data context.</param>
		/// <returns></returns>
		private static T GetArgumentValue<T>(string name, WorkflowDataContext dataContext) {
			WorkflowDataContext dc = dataContext;
			object value = (from object prop in dc.GetProperties() where ((PropertyDescriptor)prop).Name == name select ((PropertyDescriptor)prop).GetValue(dataContext)).FirstOrDefault();
			return (T)value;
		}

		/// <summary>
		/// Sets the argument value.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="name">The name.</param>
		/// <param name="entity">The entity.</param>
		/// <param name="dataContext">The data context.</param>
		private static void SetArgumentValue<T>(string name, T entity, WorkflowDataContext dataContext)
			//where T : BaseBusinessEntity 
		{
			WorkflowDataContext dc = dataContext;
			PropertyDescriptor property = (from object prop in dc.GetProperties() where ((PropertyDescriptor)prop).Name == name select ((PropertyDescriptor)prop)).FirstOrDefault();
			if (property != null) property.SetValue(dataContext, entity);
		}

		/// <summary>
		/// Gets the current state of a state machine. Recursive code.
		/// 
		/// </summary>
		/// <param name="activity">The activity. This is an object since in the WF4 state machine, a state IS NOT an activity</param>
		/// <returns></returns>
		public static string GetCurrentState(object activity) {
			if (activity == null)
				return null;

			if (activity.GetType().FullName == WorkflowHelper.const_typename_internalstate) {
				var activity1 = activity as Activity;
				if (activity1 != null) return activity1.DisplayName;
			}
			else {
				PropertyInfo pi = activity.GetType().GetProperty("Parent", BindingFlags.NonPublic | BindingFlags.Instance);
				if (pi == null)
					return null;
				object parent = pi.GetValue(activity, null);
				// recursive call
				return GetCurrentState(parent);
			}
			return null;
		}


		/// <summary>
		/// Gets the current state of a workflow.
		/// </summary>
		/// <param name="instanceId">The instance id of the workflow.</param>
		/// <returns></returns>
		public static string GetCurrentState(Guid instanceId) {
			var workflowInstanceBroker = Helper.CreateInstance<WorkflowInstanceBroker>();
			WorkflowInstance workflowInstance = workflowInstanceBroker.GetItemByInstanceId(instanceId);
			return workflowInstance != null ? workflowInstance.State : null;
		}

		/// <summary>
		/// Gets all workflows.
		/// </summary>
		/// <returns></returns>
		public static List<WorkflowAssemblyDefinition> GetAllWorkflows() {
			return (new WorkflowFinder()).GetWorkflowsFromPath();
		}

		/// <summary>
		/// Gets workflow base entity type.
		/// </summary>
		/// <param name="workflowType">Type of the workflow.</param>
		/// <returns></returns>
		public static Type GetWorkflowEntityType(Type workflowType) {
			if (workflowType == null) return null;
			if (workflowType.AssemblyQualifiedName != null) {
				Type type = Type.GetType(workflowType.AssemblyQualifiedName);
				var stateMachine = (Activity)WorkflowHelper.GetStateMachine(type);
				var stateMachineParent = stateMachine.GetType().GetProperty("Parent", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public).GetValue(stateMachine, null);
				var eventArgProperty = stateMachineParent.GetType().GetProperty("EntityArg", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
				if (eventArgProperty != null) {
					var eventArg = eventArgProperty.GetValue(stateMachineParent, null);
					if (eventArg != null)
						return ((System.Activities.Argument)(eventArg)).ArgumentType;
				}
			}
			return null;
		}

	}



}

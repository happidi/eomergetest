﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Activities;
using System.Web;
using System.Threading;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.WF.Common {

	/// <summary>
	/// A custom activity for setting HttpContext across threads.
	/// </summary>
	public class HttpContextSetterActivity : CodeActivity {

		private HttpContext httpContext;
		private string applicationName;

		/// <summary>
		/// Initializes a new instance of the <see cref="HttpContextSetterActivity"/> class.
		/// </summary>
		/// <param name="httpContext">The HTTP context.</param>
		public HttpContextSetterActivity(HttpContext httpContext) {
			this.applicationName = ContextHelper.ApplicationName;
			this.httpContext = httpContext;
		}

		/// <summary>
		/// When implemented in a derived class, performs the execution of the activity.
		/// </summary>
		/// <param name="context">The execution context under which the activity executes.</param>
		protected override void Execute(CodeActivityContext context) {
			ContextHelper.ApplicationName = this.applicationName;
			HttpContext.Current = httpContext;
			Thread.CurrentPrincipal = httpContext.User;
		}
	}

}

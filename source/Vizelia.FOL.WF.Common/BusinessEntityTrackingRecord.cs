﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Activities.Tracking;
using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.WF.Common {
	/// <summary>
	/// The custom record used to pass information from an activity to the TrackingParticipant.
	/// </summary>
	public class BusinessEntityTrackingRecord : CustomTrackingRecord {

		/// <summary>
		/// Ctor.
		/// </summary>
		public BusinessEntityTrackingRecord()
			: base("BusinessEntityTrackingRecord", System.Diagnostics.TraceLevel.Info) {

		}

		/// <summary>
		/// The entity.
		/// </summary>
		public BaseBusinessEntity Entity { get; set; }
	}
}

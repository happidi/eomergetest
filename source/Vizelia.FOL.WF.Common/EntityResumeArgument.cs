﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Used to pass Entity and HttpContext to the Workflow when resuming it.
	/// </summary>
	public class WorkflowResumeArgument {

		/// <summary>
		/// Gets or sets the current HTTP context.
		/// </summary>
		/// <value>The current HTTP context.</value>
		public HttpContext CurrentHttpContext { get; set; }

		/// <summary>
		/// Gets or sets the entity.
		/// </summary>
		/// <value>The entity.</value>
		public IWorkflowEntity Entity { get; set; } 
	
	}
}
		
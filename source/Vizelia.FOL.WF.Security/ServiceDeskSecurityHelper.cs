﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Web;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessLayer.Broker;
using Vizelia.FOL.Providers;
using Vizelia.FOL.Common;
using System.Web.Security;
using System.Security;


namespace Vizelia.FOL.WF.Security {
	/// <summary>
	/// A static class for managing security in ServiceDesk workflows.
	/// </summary>
	public static class ServiceDeskSecurityHelper {

		/// <summary>
		/// Verifies the action security for a specific instance of ActionRequest.
		/// </summary>
		/// <param name="instanceId">The instance id.</param>
		/// <param name="checkIsOwner">if set to <c>true</c> [check is owner].</param>
		/// <param name="action">The action.</param>
		/// <param name="actionRequest">The action request.</param>
		/// <returns></returns>
		public static bool Verify(Guid instanceId, bool checkIsOwner, string action, ActionRequest actionRequest)
		{
			ClassificationItemBroker brokerClassificationItem = Helper.CreateInstance<ClassificationItemBroker>();
			var wflAssembyDef = brokerClassificationItem.GetWorkflowAssemblyDefinitionByClassificationAscendant(actionRequest.ClassificationKeyChildren, actionRequest.ClassificationKeyParent);
			if (wflAssembyDef != null)
			{
				string operationServiceName = wflAssembyDef.AssemblyQualifiedName + "." + action;
				IPrincipal principal = HttpContext.Current.User;
				bool isAllowed = principal.IsInRole(operationServiceName);

				Debugger.Launch();
				return isAllowed;
			}
			return false;
		}


        /// <summary>
        /// Verifies the action security for a specific instance of ActionRequest.
        /// </summary>
        /// <param name="instanceId">The instance id.</param>
        /// <param name="checkIsOwner">if set to <c>true</c> [check is owner].</param>
        /// <param name="action">The action.</param>
        /// <returns></returns>
		public static bool Verify(Guid instanceId, bool checkIsOwner, string action) {

        	ActionRequestBroker actionRequestBroker = Helper.CreateInstance<ActionRequestBroker>();
        	var actionRequest = actionRequestBroker.GetItemByInstanceId(instanceId);
        	bool isVerified;
			if (checkIsOwner) {
				 isVerified = actionRequest.KeyApprover == Helper.GetCurrentActor();
				 if (isVerified == false)
				 	return false;
			}
			
			return true;
		}

	}
}

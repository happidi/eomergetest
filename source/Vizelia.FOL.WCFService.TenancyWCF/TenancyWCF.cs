﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessLayer;
using Vizelia.FOL.BusinessLayer.Interfaces;
using Vizelia.FOL.Common;
using Vizelia.FOL.WCFService.Contracts;

namespace Vizelia.FOL.WCFService {
	/// <summary>
	/// Implementation for Tenancy Services.
	/// </summary>
	[ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple, InstanceContextMode = InstanceContextMode.PerCall, UseSynchronizationContext = false)]
	public class TenancyWCF :BaseModuleWCF, ITenancyWCF {
		private readonly ITenancyBusinessLayer m_TenancyBusinessLayer;

		/// <summary>
		/// Initializes a new instance of the <see cref="TenancyWCF"/> class.
		/// </summary>
		public TenancyWCF() {
			// Policy injection on the business layer instance.
			m_TenancyBusinessLayer = Helper.CreateInstance<TenancyBusinessLayer, ITenancyBusinessLayer>();
		}

		/// <summary>
		/// Deletes an existing business entity Tenant.
		/// </summary>
		/// <param name="keyTenant">The key tenant.</param>
		/// <param name="operationId">The operation id.</param>
		public void Tenant_Delete(string keyTenant, Guid operationId) {
			m_TenancyBusinessLayer.Tenant_Delete(keyTenant, operationId);
		}

		/// <summary>
		/// Creates a new business entity Tenant and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <param name="operationId">The operation id.</param>
		public void Tenant_FormCreate(Tenant item, Guid operationId) {
			m_TenancyBusinessLayer.Tenant_FormCreate(item, operationId);
		}

		/// <summary>
		/// Loads a specific item for the business entity Tenant.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse Tenant_FormLoad(string Key) {
			return m_TenancyBusinessLayer.Tenant_FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity Tenant and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <param name="operationId">The operation id.</param>
		/// <returns></returns>
		public void Tenant_FormUpdate(Tenant item, Guid operationId) {
			m_TenancyBusinessLayer.Tenant_FormUpdate(item, operationId);
		}
		
		/// <summary>
		/// Gets a list for the business entity Tenant. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		public List<Tenant> Tenant_GetAll(PagingParameter paging, PagingLocation location) {
			return m_TenancyBusinessLayer.Tenant_GetAll(paging, location);
		}

		/// <summary>
		/// Gets a specific item for the business entity Tenant.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public Tenant Tenant_GetItem(string Key) {
			return m_TenancyBusinessLayer.Tenant_GetItem(Key);
		}

		/// <summary>
		/// Gets a json store for the business entity Tenant
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<Tenant> Tenant_GetStore(PagingParameter paging, PagingLocation location) {
			return m_TenancyBusinessLayer.Tenant_GetStore(paging, location);
		}

		/// <summary>
		/// Saves a crud store for the business entity Tenant.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<Tenant> Tenant_SaveStore(CrudStore<Tenant> store) {
			return m_TenancyBusinessLayer.Tenant_SaveStore(store);
		}
		
		/// <summary>
		/// Resets the current tenant.
		/// </summary>
		/// <param name="operationId">The operation id.</param>
		/// <param name="adminPassword">The admin password.</param>
		public void ResetTenant(Guid operationId, string adminPassword) {
			m_TenancyBusinessLayer.ResetTenant(operationId, adminPassword);
		}

	}
}

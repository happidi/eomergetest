﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Vizelia.FOL.Utilities.CommandLine {
	/// <summary>
	/// A usage helper.
	/// </summary>
	public static class UsageHelper {

		/// <summary>
		/// Gets the width of the console.
		/// </summary>
		/// <returns></returns>
		private static int GetConsoleWidth() {
			return Console.WindowWidth;
		}

		/// <summary>
		/// Aborts the execution.
		/// </summary>
		public static void Abort(string message) {
			Console.ForegroundColor = ConsoleColor.Red;
			Console.WriteLine("");
			Console.WriteLine(message);
			Console.ForegroundColor = ConsoleColor.Gray;
			Environment.Exit(0);

		}

		

		/// <summary>
		/// Gets the usage string.
		/// </summary>
		/// <param name="strings">The strings.</param>
		/// <returns></returns>
		public static string GetUsageString(IList<ArgumentHelpStrings> strings) {
			int screenWidth = GetConsoleWidth();
			int num2;
			int num = 0;
			foreach (ArgumentHelpStrings strings2 in strings) {
				num = Math.Max(num, strings2.Syntax.Length);
			}
			int num3 = num + 2;
			screenWidth = Math.Max(screenWidth, 15);
			if (screenWidth < (num3 + 10)) {
				num2 = 5;
			}
			else {
				num2 = num3;
			}
			StringBuilder builder = new StringBuilder();
			foreach (ArgumentHelpStrings strings3 in strings) {
				int length = strings3.Syntax.Length;
				builder.Append(strings3.Syntax);
				int currentColumn = length;
				if (length >= num2) {
					builder.Append(Environment.NewLine);
					currentColumn = 0;
				}
				int num6 = screenWidth - num2;
				int startIndex = 0;
				while (startIndex < strings3.Help.Length) {
					builder.Append(' ', num2 - currentColumn);
					currentColumn = num2;
					int num8 = startIndex + num6;
					if (num8 >= strings3.Help.Length) {
						num8 = strings3.Help.Length;
					}
					else {
						num8 = strings3.Help.LastIndexOf(' ', num8 - 1, Math.Min(num8 - startIndex, num6));
						if (num8 <= startIndex) {
							num8 = startIndex + num6;
						}
					}
					builder.Append(strings3.Help, startIndex, num8 - startIndex);
					startIndex = num8;
					AddNewLine(Environment.NewLine, builder, ref currentColumn);
					while ((startIndex < strings3.Help.Length) && (strings3.Help[startIndex] == ' ')) {
						startIndex++;
					}
				}
				builder.Append(Environment.NewLine);
			}
			return builder.ToString();
		}

		/// <summary>
		/// Adds the new line.
		/// </summary>
		/// <param name="newLine">The new line.</param>
		/// <param name="builder">The builder.</param>
		/// <param name="currentColumn">The current column.</param>
		private static void AddNewLine(string newLine, StringBuilder builder, ref int currentColumn) {
			builder.Append(newLine);
			currentColumn = 0;
		}

	}
}

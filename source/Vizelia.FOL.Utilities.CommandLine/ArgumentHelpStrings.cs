﻿namespace Vizelia.FOL.Utilities.CommandLine {
	/// <summary>
	/// The class for Argument help strings.
	/// </summary>
	public class ArgumentHelpStrings {
		/// <summary>
		/// Help information on the argument
		/// </summary>
		private string help;

		/// <summary>
		/// Syntax of a help argument
		/// </summary>
		public string Syntax { get; set; }

		/// <summary>
		/// Gets or sets the help.
		/// </summary>
		/// <value>
		/// The help.
		/// </value>
		public string Help {
			get { return "(" + Type.ToString().ToLower() + ") " + help; }
			set { help = value; }
		} 

		public ArgumentType Type;

		/// <summary>
		/// Initializes a new instance of the <see cref="ArgumentHelpStrings"/> class.
		/// </summary>
		public ArgumentHelpStrings() {
			
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="ArgumentHelpStrings"/> struct.
		/// </summary>
		/// <param name="syntax">The syntax.</param>
		/// <param name="help">The help.</param>
		/// <param name="type">The type of the argument.</param>
		public ArgumentHelpStrings(string syntax, string help, ArgumentType type) {
			Syntax = syntax;
			Help = help;
			Type = type;
		}
	}

	/// <summary>
	/// Argument types
	/// </summary>
	public enum ArgumentType {
		Mandatory,
		Optional
	}
}

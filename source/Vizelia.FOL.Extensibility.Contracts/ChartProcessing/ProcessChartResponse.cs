﻿using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.Extensibility.Contracts.ChartProcessing {
	/// <summary>
	/// The response object of chart processing.
	/// </summary>
	public class ProcessChartResponse {
		/// <summary>
		/// Initializes a new instance of the <see cref="ProcessChartResponse"/> class.
		/// </summary>
		/// <param name="chart">The chart.</param>
		public ProcessChartResponse(Chart chart) {
			Chart = chart;
		}

		/// <summary>
		/// Gets or sets the chart.
		/// </summary>
		/// <value>
		/// The chart.
		/// </value>
		public Chart Chart { get; private set; }
	}
}
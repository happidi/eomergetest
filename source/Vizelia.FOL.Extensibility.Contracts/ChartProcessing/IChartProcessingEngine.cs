﻿namespace Vizelia.FOL.Extensibility.Contracts.ChartProcessing
{
	/// <summary>
	/// An interface abstracting the Chart Processing engine functionality.
	/// </summary>
    public interface IChartProcessingEngine {
		/// <summary>
		/// Processes the chart.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		ProcessChartResponse ProcessChart(ProcessChartRequest request);
	}
}

﻿using System.Collections.Generic;
using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.Extensibility.Contracts.ChartProcessing {
	/// <summary>
	/// The request object of chart processing.
	/// </summary>
	public class ProcessChartRequest {
		/// <summary>
		/// Gets or sets the context.
		/// </summary>
		/// <value>
		/// The context.
		/// </value>
		public EnergyAggregatorContext Context { get; set; }

		/// <summary>
		/// Gets or sets the chart.
		/// </summary>
		/// <value>
		/// The chart.
		/// </value>
		public Chart Chart { get; set; }

		/// <summary>
		/// Gets or sets the meter keys.
		/// </summary>
		/// <value>
		/// The meter keys.
		/// </value>
		public List<int> MeterKeys { get; set; }
		
		/// <summary>
		/// Gets or sets the location keys.
		/// </summary>
		/// <value>
		/// The location keys.
		/// </value>
		public List<string> LocationKeys { get; set; }

		/// <summary>
		/// Gets or sets the Meter ClassificationItem keys.
		/// </summary>
		/// <value>
		/// The Meter ClassificationItem keys.
		/// </value>
		public List<string> MeterClassificationItemKeys { get; set; }

		/// <summary>
		/// Gets or sets the historical analysis.
		/// </summary>
		/// <value>
		/// The historical analysis.
		/// </value>
		public ChartHistoricalAnalysis HistoricalAnalysis { get; set; }
		
		/// <summary>
		/// Gets or sets the location filter.
		/// </summary>
		/// <value>
		/// The location filter.
		/// </value>
		public List<Filter> LocationFilter { get; set; }
		
		/// <summary>
		/// Gets or sets the culture.
		/// </summary>
		/// <value>
		/// The culture.
		/// </value>
		public string Culture { get; set; }
		
		/// <summary>
		/// Gets or sets the alarm definition.
		/// </summary>
		/// <value>
		/// The alarm definition.
		/// </value>
		public AlarmDefinition AlarmDefinition { get; set; }
	}
}
﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Configuration.Provider;
using System.IO;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Configuration;
using System.Web.Security;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessLayer.Broker;
using Vizelia.FOL.BusinessLayer.Interfaces;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Configuration;
using Vizelia.FOL.Common.Localization;
using Vizelia.FOL.DataLayer;
using Vizelia.FOL.DataLayer.Interfaces;
using Vizelia.FOL.Providers;
using Vizelia.FOL.Security;
using Vizelia.FOL.WF.Common;

namespace Vizelia.FOL.BusinessLayer {

	/// <summary>
	/// Business Layer implementation for Authentication 
	/// </summary>
	public class AuthenticationBusinessLayer : IAuthenticationBusinessLayer {
		private readonly ICoreDataAccess m_CoreDataAccess;
		private readonly IFOLSqlMembershipProvider m_ProviderMembership;
		private readonly IFOLRoleProvider m_ProviderRole;

		/// <summary>
		/// Public ctor.
		/// The FOLSqlMembership provider is always the default provider.
		/// Instead of looking at Membership.provider and cast the result we retreive it in the constructor.
		/// We do the same for role provider.
		/// This ensure that we have access to specialized functions of the provider.
		/// </summary>
		public AuthenticationBusinessLayer() {
			m_ProviderMembership = (IFOLSqlMembershipProvider)Membership.Provider;
			m_ProviderRole = (IFOLRoleProvider)Roles.Provider;
			m_CoreDataAccess = Helper.CreateInstance<CoreDataAccess, ICoreDataAccess>();
		}



		/// <summary>
		/// Gets a list of auhorization for a specific application group.
		/// </summary>
		/// <param name="applicationGroupId">The application group id for which to retreive the authorizations.</param>
		/// <returns></returns>
		public List<FOLMembershipUser> ApplicationGroup_Authorization_GetList(string applicationGroupId) {
			var broker = Helper.CreateInstance<FOLMembershipUserBroker>();
			return broker.GetListByApplicationGroupId(applicationGroupId);
		}

		/// <summary>
		/// Gets a store of auhorization for a specific application group.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="applicationGroupId">The application group id for which to retreive the authorizations.</param>
		/// <returns></returns>
		//[PrincipalPermission(SecurityAction.Demand, Role = "Vizelia.FOL.WCFService.AuthenticationWCF.ApplicationGroup_Authorization_GetStore")]
		public JsonStore<FOLMembershipUser> ApplicationGroup_Authorization_GetStore(PagingParameter paging, string applicationGroupId) {
			return ApplicationGroup_Authorization_GetList(applicationGroupId).ToJsonStoreWithFilter(paging);
		}

		/// <summary>
		/// Gets a store of roles for a specific application group.
		/// </summary>
		/// <param name="applicationGroupId">The application group id for which to retreive the authorizations.</param>
		/// <returns></returns>
		//[PrincipalPermission(SecurityAction.Demand, Role = "Vizelia.FOL.WCFService.AuthenticationWCF.ApplicationGroup_Authorization_GetStore")]
		public List<AuthorizationItem> ApplicationGroup_AzManRole_GetList(string applicationGroupId) {
			var broker = Helper.CreateInstance<AuthorizationItemBroker>();
			return broker.AzManRole_GetListByApplicationGroupId(applicationGroupId);
		}

		/// <summary>
		/// Gets a store of roles for a specific application group.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="applicationGroupId">The application group id for which to retreive the authorizations.</param>
		/// <returns></returns>
		//[PrincipalPermission(SecurityAction.Demand, Role = "Vizelia.FOL.WCFService.AuthenticationWCF.ApplicationGroup_Authorization_GetStore")]
		public JsonStore<AuthorizationItem> ApplicationGroup_AzManRole_GetStore(PagingParameter paging, string applicationGroupId) {
			return ApplicationGroup_AzManRole_GetList(applicationGroupId).ToJsonStoreWithFilter(paging);
		}

		/// <summary>
		/// Deletes an ApplicationGroup entity.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		//[PrincipalPermission(SecurityAction.Demand, Role = "Vizelia.FOL.WCFService.AuthenticationWCF.ApplicationGroup_Delete")]
		public bool ApplicationGroup_Delete(ApplicationGroup item) {
			var broker = Helper.CreateInstance<ApplicationGroupBroker>();
			var retval = broker.Delete(item);
			broker.InvalidateStorageCache();
			return retval;
		}

		/// <summary>
		/// Creates a new ApplicationGroup entity.
		/// </summary>
		/// <param name="item">The ApplicationGroup entity to create.</param>
		/// <param name="authorizations">The authorizations.</param>
		/// <param name="roles">The roles.</param>
		/// <returns>A json form response.</returns>
		//[PrincipalPermission(SecurityAction.Demand, Role = "Vizelia.FOL.WCFService.AuthenticationWCF.ApplicationGroup_FormCreate")]
		public FormResponse ApplicationGroup_FormCreate(ApplicationGroup item, CrudStore<FOLMembershipUser> authorizations, CrudStore<AuthorizationItem> roles) {
			FormResponse response;
			var broker = Helper.CreateInstance<ApplicationGroupBroker>();
			var folMembershipUserBroker = Helper.CreateInstance<FOLMembershipUserBroker>();
			var authorizationItemBroker = Helper.CreateInstance<AuthorizationItemBroker>();
			using (var t = Helper.CreateTransactionScope()) {
				response = broker.FormCreate(item);
				if (response.success) {
					try {
						var applicationGroup = (ApplicationGroup)response.data;
						// invalidate cahche after ApplicationGroup creation.
						broker.InvalidateStorageCache();
						folMembershipUserBroker.SaveApplicationGroupFOLMembershipUser(applicationGroup, authorizations);
						authorizationItemBroker.SaveApplicationGroupAuthorizationItem(applicationGroup, roles);
						// invalidate cache after adding users and authorizations.
						broker.InvalidateStorageCache();
						t.Complete();
					}
					catch (Exception ex) {
						response.success = false;
						response.msg = ex.Message;
					}
				}
			}
			return response;
		}

		/// <summary>
		/// Loads a ApplicationGroup entity.
		/// </summary>
		/// <param name="Key">The Key of the ApplicationGroup.</param>
		/// <returns>A json form response.</returns>
		// [PrincipalPermission(SecurityAction.Demand, Role = "Vizelia.FOL.WCFService.AuthenticationWCF.ApplicationGroup_FormLoad")]
		public FormResponse ApplicationGroup_FormLoad(string Key) {
			var broker = Helper.CreateInstance<ApplicationGroupBroker>();
			return broker.FormLoad(Key);
		}

		/// <summary>
		/// Updates an ApplicationGroup entity.
		/// </summary>
		/// <param name="item">The ApplicationGroup entity to update.</param>
		/// <param name="authorizations">The authorizations.</param>
		/// <param name="roles">The roles.</param>
		/// <returns>A json form response.</returns>
		//[PrincipalPermission(SecurityAction.Demand, Role = "Vizelia.FOL.WCFService.AuthenticationWCF.ApplicationGroup_FormUpdate")]
		public FormResponse ApplicationGroup_FormUpdate(ApplicationGroup item, CrudStore<FOLMembershipUser> authorizations, CrudStore<AuthorizationItem> roles) {
			FormResponse response;
			var broker = Helper.CreateInstance<ApplicationGroupBroker>();
			var folMembershipUserBroker = Helper.CreateInstance<FOLMembershipUserBroker>();
			var authorizationItemBroker = Helper.CreateInstance<AuthorizationItemBroker>();
			using (var t = Helper.CreateTransactionScope()) {
				response = broker.FormUpdate(item);
				if (response.success) {
					try {
						var applicationGroup = (ApplicationGroup)response.data;
						folMembershipUserBroker.SaveApplicationGroupFOLMembershipUser(applicationGroup, authorizations);
						authorizationItemBroker.SaveApplicationGroupAuthorizationItem(applicationGroup, roles);
						broker.InvalidateStorageCache();
						t.Complete();
					}
					catch (Exception ex) {
						response.success = false;
						response.msg = ex.Message;
					}
				}
			}
			return response;
		}

		/// <summary>
		/// Gets an ApplicationGroup entity by its name.
		/// </summary>
		/// <param name="name">The name of the ApplicationGroup.</param>
		/// <returns></returns>
		public ApplicationGroup ApplicationGroup_GetByName(string name) {
			var broker = Helper.CreateInstance<ApplicationGroupBroker>();
			return broker.GetItemByLocalId(name);
		}

		/// <summary>
		/// Gets a store of all application groups.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <returns></returns>
		//[PrincipalPermission(SecurityAction.Demand, Role = "Vizelia.FOL.WCFService.AuthenticationWCF.ApplicationGroup_GetStore")]
		public JsonStore<ApplicationGroup> ApplicationGroup_GetStore(PagingParameter paging) {
			var broker = Helper.CreateInstance<ApplicationGroupBroker>();
			return broker.GetStore(paging, PagingLocation.Database);
		}

		/// <summary>
		/// Generates the json structure of the ApplicationGroup treeview.
		/// </summary>
		/// <returns></returns>
		//[PrincipalPermission(SecurityAction.Demand, Role = "Vizelia.FOL.WCFService.AuthenticationWCF.ApplicationGroup_GetTree")]
		public List<TreeNode> ApplicationGroup_GetTree() {
			var broker = Helper.CreateInstance<ApplicationGroupBroker>();
			return broker.GetTree(null);
		}

		/// <summary>
		/// Saves the store of Application Groups.
		/// </summary>
		/// <param name="store">The crud store to save.</param>
		/// <returns></returns>
		//[PrincipalPermission(SecurityAction.Demand, Role = "Vizelia.FOL.WCFService.AuthenticationWCF.ApplicationGroup_SaveStore")]
		public JsonStore<ApplicationGroup> ApplicationGroup_SaveStore(CrudStore<ApplicationGroup> store) {
			var broker = Helper.CreateInstance<ApplicationGroupBroker>();
			var retval = broker.SaveStore(store);
			broker.InvalidateStorageCache();
			return retval;
		}

		/// <summary>
		/// Gets all the authentication providers declared in vizelia section of web.config.
		/// The returned list is used to build a domain combobox in login form.
		/// </summary>
		/// <returns></returns>
		public JsonStore<ListElement> AuthenticationProvider_GetStore() {

			ISSOProvider provider;
			GenericProviderCollection<SSOProvider, ISSOProvider> providers = GenericProviderHelper.LoadProviders<SSOProvider, GenericProviderSection, ISSOProvider>("VizeliaSSOSection", out provider);

			List<SSOProvider> providerss = providers.Cast<SSOProvider>().Where(p => p.IsRunnableFromLogin).ToList();

			List<ListElement> result = new List<ListElement>();
			result.Add(new ListElement() { Id = "Standard Login", IconCls = "viz-icon-small-main", MsgCode = "msg_authentication_standard" });
			result.AddRange(providerss.Select(p => new ListElement() { Id = p.Name, IconCls = p.IconCls, MsgCode = p.MsgCode }).ToList());

			return result.ToJsonStore();
		}

		/// <summary>
		/// Gets an AuthorizationItem by its name.
		/// </summary>
		/// <param name="name">The name of the AuthorizationItem.</param>
		/// <returns></returns>
		public AuthorizationItem AuthorizationItem_GetByName(string name) {
			var broker = Helper.CreateInstance<AuthorizationItemBroker>();
			return broker.GetItemByLocalId(name);
		}

		/// <summary>
		/// Gets a store of all authorizations items of the specific user.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="username">The user name.</param>
		/// <returns></returns>
		//[PrincipalPermission(SecurityAction.Demand, Role = "Vizelia.FOL.WCFService.AuthenticationWCF.AuthorizationItem_GetStore")]
		public JsonStore<AuthorizationItem> AuthorizationItem_GetStore(PagingParameter paging, string username) {
			var broker = Helper.CreateInstance<AuthorizationItemBroker>();
			var retVal = broker.UserAuthorization_GetStore(paging, username);
			retVal.metaData.idProperty = "Name"; // override the id of the record to speed up filtering on client side.
			return retVal;
		}

		/// <summary>
		/// Exports the AzMan configuration as a xml stream file.
		/// </summary>
		/// <returns>The xml stream file.</returns>
		public MemoryStream AzMan_Export() {
			return m_ProviderRole.Export();
		}

		/// <summary>
		/// Exports the AzMan configuration as a xml stream file.
		/// </summary>
		/// <param name="operationId">The operation id.</param>
		public void BeginAzManExport(Guid operationId) {
			LongRunningOperationService.Invoke(operationId, () => {
				LongRunningOperationService.ReportProgress();
				var stream = AzMan_Export();
				var file = new StreamResult(stream, "Configuration", "xml", MimeType.Xml);
				var result = new LongRunningOperationResult(LongRunningOperationStatus.ResultAvailable, file);
				return result;
			});
		}

		/// <summary>
		/// Gets a store of auhorization for a specific application groups.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="filterId">The AzMan filter id for which to retreive the authorizations.</param>
		/// <returns></returns>
		public JsonStore<ApplicationGroup> AzManFilter_Authorization_GetStore(PagingParameter paging, string filterId) {
			var broker = Helper.CreateInstance<ApplicationGroupBroker>();
			return broker.GetListByAzManFilter(filterId).ToJsonStoreWithFilter(paging);
		}

		/// <summary>
		/// Gets a store of user authorization for a specific application groups.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="filterId">The AzMan filter id for which to retreive the authorizations.</param>
		/// <returns></returns>
		public JsonStore<FOLMembershipUser> AzManFilter_UserAuthorization_GetStore(PagingParameter paging, string filterId) {
			var broker = Helper.CreateInstance<FOLMembershipUserBroker>();
			return broker.GetListByAzManFilter(filterId).ToJsonStoreWithFilter(paging);
		}

		/// <summary>
		/// Deletes a AzMan filter item.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns>True if operation was successfull, false otherwise.</returns>
		public bool AzManFilter_Delete(AuthorizationItem item) {
			var broker = Helper.CreateInstance<AuthorizationItemBroker>();
			var retval = broker.Delete(item);
			broker.InvalidateStorageCache();
			return retval;
		}

		/// <summary>
		/// Creates a new AzMan filter item.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <param name="filterSpatial">The list of spatial filter elements.</param>
		/// <param name="filterReport">The filter report.</param>
		/// <param name="filterChart">The filter chart.</param>
		/// <param name="filterAlarmDefinition">The filter alarm definition.</param>
		/// <param name="filterDynamicDisplay">The filter dynamic display.</param>
		/// <param name="filterPlaylist">The filter playlist.</param>
		/// <param name="filterChartScheduler">The filter chart scheduler.</param>
		/// <param name="filterPortalTemplate">The filter portal template.</param>
		/// <param name="filterPortalWindow">The filter portal window.</param>
		/// <param name="filterClassificationItem">The filter classification item.</param>
		/// <param name="authorizations">The list of application groups.</param>
		/// <param name="users">The list of users</param>
		/// <returns></returns>
		public FormResponse AzManFilter_FormCreate(AuthorizationItem item, List<SecurableEntity> filterSpatial, List<SecurableEntity> filterReport,
			List<SecurableEntity> filterChart, List<SecurableEntity> filterAlarmDefinition, List<SecurableEntity> filterDynamicDisplay, List<SecurableEntity> filterPlaylist,
			List<SecurableEntity> filterChartScheduler, List<SecurableEntity> filterPortalTemplate, List<SecurableEntity> filterPortalWindow,
			List<SecurableEntity> filterClassificationItem, CrudStore<ApplicationGroup> authorizations, CrudStore<FOLMembershipUser> users) {
			FormResponse response;
			var broker = Helper.CreateInstance<AuthorizationItemBroker>();
			var folMembershipUserBroker = Helper.CreateInstance<FOLMembershipUserBroker>();
			var applicationGroupBroker = Helper.CreateInstance<ApplicationGroupBroker>();
			using (var t = Helper.CreateTransactionScope()) {
				item.ItemType = "Filter";
				item.FilterSpatial = filterSpatial;
				item.FilterReport = filterReport;
				item.FilterChart = filterChart;
				item.FilterDynamicDisplay = filterDynamicDisplay;
				item.FilterPlaylist = filterPlaylist;
				item.FilterChartScheduler = filterChartScheduler;
				item.FilterPortalTemplate = filterPortalTemplate;
				item.FilterPortalWindow = filterPortalWindow;
				item.FilterClassificationItem = filterClassificationItem;
				item.FilterAlarmDefinition = filterAlarmDefinition;
				response = broker.FormCreate(item);
				if (response.success) {
					try {
						broker.InvalidateStorageCache();
						var authorizationItem = (AuthorizationItem)response.data;
						folMembershipUserBroker.SaveAuthorizationItemFOLMembershipUser(authorizationItem, users);
						applicationGroupBroker.SaveApplicationGroupAuthorizationItem(authorizationItem, authorizations);
						broker.InvalidateStorageCache();
						t.Complete();
					}
					catch (Exception ex) {
						response.success = false;
						response.msg = ex.Message;
					}
				}
			}
			return response;
		}

		/// <summary>
		/// Loads an AzMan filter item.
		/// </summary>
		/// <param name="Key">The key of the item.</param>
		/// <returns></returns>
		public FormResponse AzManFilter_FormLoad(string Key) {
			var broker = Helper.CreateInstance<AuthorizationItemBroker>();
			return broker.FormLoad(Key);
		}

		/// <summary>
		/// Loads an AzMan Task item.
		/// </summary>
		/// <param name="Key">The key of the item.</param>
		/// <returns></returns>
		public FormResponse AzManTask_FormLoad(string Key) {
			var broker = Helper.CreateInstance<AuthorizationItemBroker>();
			return broker.FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing AzMan filter item.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <param name="filterSpatial">The list of spatial filter elements.</param>
		/// <param name="filterReport">The filter report.</param>
		/// <param name="filterChart">The filter chart.</param>
		/// <param name="filterAlarmDefinition">The filter alarm definition.</param>
		/// <param name="filterDynamicDisplay">The filter dynamic display.</param>
		/// <param name="filterPlaylist">The filter playlist.</param>
		/// <param name="filterChartScheduler">The filter chart scheduler.</param>
		/// <param name="filterPortalTemplate">The filter portal template.</param>
		/// <param name="filterPortalWindow">The filter portal window.</param>
		/// <param name="filterClassificationItem">The filter classification item.</param>
		/// <param name="filterLink">The filter link item</param>
		/// <param name="authorizations">The list of application groups.</param>
		/// <param name="users">The list of users</param>
		/// <returns></returns>
		public FormResponse AzManFilter_FormUpdate(AuthorizationItem item, List<SecurableEntity> filterSpatial, List<SecurableEntity> filterReport, List<SecurableEntity> filterChart, List<SecurableEntity> filterAlarmDefinition, List<SecurableEntity> filterDynamicDisplay, List<SecurableEntity> filterPlaylist, List<SecurableEntity> filterChartScheduler, List<SecurableEntity> filterPortalTemplate, List<SecurableEntity> filterPortalWindow, List<SecurableEntity> filterClassificationItem, List<SecurableEntity> filterLink, CrudStore<ApplicationGroup> authorizations, CrudStore<FOLMembershipUser> users) {
			FormResponse response;
			var broker = Helper.CreateInstance<AuthorizationItemBroker>();
			var folMembershipUserBroker = Helper.CreateInstance<FOLMembershipUserBroker>();
			var applicationGroupBroker = Helper.CreateInstance<ApplicationGroupBroker>();
			using (var t = Helper.CreateTransactionScope()) {
				item.ItemType = "Filter";
				item.FilterSpatial = filterSpatial;
				item.FilterReport = filterReport;
				item.FilterChart = filterChart;
				item.FilterDynamicDisplay = filterDynamicDisplay;
				item.FilterPlaylist = filterPlaylist;
				item.FilterChartScheduler = filterChartScheduler;
				item.FilterPortalTemplate = filterPortalTemplate;
				item.FilterPortalWindow = filterPortalWindow;
				item.FilterLink = filterLink;
				item.FilterClassificationItem = filterClassificationItem;
				item.FilterAlarmDefinition = filterAlarmDefinition;
				response = broker.FormUpdate(item);
				if (response.success) {
					try {
						var authorizationItem = (AuthorizationItem)response.data;
						folMembershipUserBroker.SaveAuthorizationItemFOLMembershipUser(authorizationItem, users);
						applicationGroupBroker.SaveApplicationGroupAuthorizationItem(authorizationItem, authorizations);
						broker.InvalidateStorageCache();
						t.Complete();
					}
					catch (Exception ex) {
						response.success = false;
						response.msg = ex.Message;
					}
				}
			}
			return response;
		}

		/// <summary>
		/// Generates the json structure of the AzMan Filter treeview.
		/// </summary>
		/// <param name="Key">The Key of the parent node.</param>
		/// <returns>a list of treenodes in json.</returns>
		public List<TreeNode> AzManFilter_GetTree(string Key) {
			var broker = Helper.CreateInstance<AuthorizationItemBroker>();
			return broker.AzManFilter_GetTree(Key);
		}

		/// <summary>
		/// Gets the store of filters for an AzMan filter element.
		/// </summary>
		/// <param name="paging">The paging parameter</param>
		/// <param name="filterId">The id of the AzMan filter element.</param>
		/// <param name="typeName">Name of the securable business entity.</param>
		/// <returns></returns>
		public JsonStore<SecurableEntity> AzManFilter_Filter_GetStore(PagingParameter paging, string filterId, string typeName) {
			var broker = Helper.CreateInstance<AuthorizationItemBroker>();
			var retVal = broker.Filter_GetList(filterId, false, typeName).ToJsonStoreWithFilter(paging);
			return retVal;
		}

		/// <summary>
		/// Get a store of azman filters, projects by keyword
		/// </summary>
		/// <param name="paging"></param>
		/// <param name="keyword"></param>
		/// <returns></returns>
		public JsonStore<AuthorizationItem> AzManFilter_GetStoreByKeyword(PagingParameter paging, string keyword) {
			int totalRecordCount;
			var list = ((FOLRoleProvider)Roles.Provider).AzManFilter_GetAll(paging, out totalRecordCount);
			var result = list.ToJsonStore(totalRecordCount);
			return result;
		}

		/// <summary>
		/// Gets azman filters by location
		/// </summary>
		/// <param name="paging"></param>
		/// <param name="keyLocation"></param>
		/// <returns></returns>
		public JsonStore<AuthorizationItem> AzManFilter_GetStoreByLocation(PagingParameter paging, string keyLocation) {
			var coreBusinessLayer = Helper.Resolve<ICoreBusinessLayer>();
			
			var parentLocationKeys = string.IsNullOrWhiteSpace(keyLocation) ? null :
				coreBusinessLayer.Location_GetItem(keyLocation).KeyLocationPath.Split('/').
				Where(item => !string.IsNullOrWhiteSpace(item)).
				Select(s => s.Trim()).ToArray();
			
			var list = ((FOLRoleProvider)Roles.Provider).AzManFilter_GetByLocation(parentLocationKeys);
			var result = list.ToJsonStoreWithFilter(paging);
			return result;
		}
		
		/// <summary>
		/// Adds members to an AzManItem.
		/// </summary>
		/// <param name="parent">The AzManItem parent.</param>
		/// <param name="children">The list of AzManItem to add as members.</param>
		/// <returns></returns>
		public bool AzManItem_AddMembers(AuthorizationItem parent, List<AuthorizationItem> children) {
			var broker = Helper.CreateInstance<AuthorizationItemBroker>();
			var retval = broker.SaveAuthorizationItemAuthorizationItem(parent, new CrudStore<AuthorizationItem> { create = children });
			broker.InvalidateStorageCache();
			return retval;
		}

		/// <summary>
		/// Removes members from an AzManItem. 
		/// </summary>
		/// <param name="parent">The AzManItem parent.</param>
		/// <param name="children">The list of AzManItem to remove as members.</param>
		/// <returns></returns>
		public bool AzManItem_RemoveMembers(AuthorizationItem parent, List<AuthorizationItem> children) {
			var broker = Helper.CreateInstance<AuthorizationItemBroker>();
			var retval = broker.SaveAuthorizationItemAuthorizationItem(parent, new CrudStore<AuthorizationItem> { destroy = children });
			broker.InvalidateStorageCache();
			return retval;
		}

		/// <summary>
		/// Creates a new AzMan Operation item.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse AzManOperation_FormCreate(AuthorizationItem item) {
			var broker = Helper.CreateInstance<AuthorizationItemBroker>();
			item.ItemType = "Operation";
			var retVal = broker.FormCreate(item);
			broker.InvalidateStorageCache();
			return retVal;
		}

		/// <summary>
		/// Loads a AzMan Operation item.
		/// </summary>
		/// <param name="Key">The key of the item.</param>
		/// <returns></returns>
		public FormResponse AzManOperation_FormLoad(string Key) {
			var broker = Helper.CreateInstance<AuthorizationItemBroker>();
			var retVal = broker.FormLoad(Key);
			return retVal;
		}

		/// <summary>
		/// Updates an existing AzMan Operation item.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <returns></returns>
		public FormResponse AzManOperation_FormUpdate(AuthorizationItem item) {
			var broker = Helper.CreateInstance<AuthorizationItemBroker>();
			var retVal = broker.FormUpdate(item);
			broker.InvalidateStorageCache();
			return retVal;
		}

		/// <summary>
		/// Gets a store of all operations.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <returns></returns>
		public JsonStore<AuthorizationItem> AzManOperation_GetStore(PagingParameter paging) {
			var broker = Helper.CreateInstance<AuthorizationItemBroker>();
			var operations = broker.Operation_GetList();
			IEnumerable<string> operationNames = GetWorkflowOperationNames(operations);
			broker.AddOperations(operationNames);
			return operations.ToJsonStoreWithFilter(paging);
		}

		/// <summary>
		/// Gets the workflow operation names.
		/// </summary>
		/// <param name="operations">The operations.</param>
		/// <returns>The names of the workflow operations</returns>
		private static IEnumerable<string> GetWorkflowOperationNames(List<AuthorizationItem> operations) {
			List<WorkflowActionElement> events = new List<WorkflowActionElement>();

			var workflows = WorkflowHelper.GetAllWorkflows();
			foreach (var workflowAssemblyDefinition in workflows) {
				events.AddRange(WorkflowHelper.GetStateMachineEventsTransitions(workflowAssemblyDefinition.AssemblyQualifiedName));
			}

			var workflowActions = events.Select(record => record.KeyWorkflowActionElement);

			var actionNames = workflowActions.Select(action => action.Split(',').FirstOrDefault())
				.Where(action => !String.IsNullOrWhiteSpace(action) && action.Contains("_"));

			IEnumerable<string> operationNames = actionNames.Select(action => action.Split('_')[1] + "." + action.Split('_')[0])
				.Where(operation => !(operations.Select(op => op.Name).Contains(operation)));
			return operationNames;
		}

		/// <summary>
		/// Saves a crud store for an AzMan item Operation.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<AuthorizationItem> AzManOperation_SaveStore(CrudStore<AuthorizationItem> store) {
			var broker = Helper.CreateInstance<AuthorizationItemBroker>();
			ApplyItemTypeToCrudStore(store, "Operation");
			var retVal = broker.SaveStore(store);
			broker.InvalidateStorageCache();
			return retVal;
		}

		/// <summary>
		/// Creates a new AzMan Role item.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <param name="filterSpatial">The list of spatial filter elements.</param>
		/// <param name="filterReport">The filter report.</param>
		/// <param name="filterChart">The filter chart.</param>
		/// <param name="filterDynamicDisplay">The filter dynamic display.</param>
		/// <param name="filterPlaylist">The filter playlist.</param>
		/// <param name="filterChartScheduler">The filter chart scheduler.</param>
		/// <param name="filterPortalTemplate">The filter portal template.</param>
		/// <param name="filterPortalWindow">The filter portal window.</param>
		/// <param name="filterClassificationItem">The filter classification item.</param>
		/// <param name="authorizations">The list of application groups.</param>
		/// <returns></returns>
		public FormResponse AzManRole_FormCreate(AuthorizationItem item, List<SecurableEntity> filterSpatial, List<SecurableEntity> filterReport,
			List<SecurableEntity> filterChart, List<SecurableEntity> filterDynamicDisplay, List<SecurableEntity> filterPlaylist,
			List<SecurableEntity> filterChartScheduler, List<SecurableEntity> filterPortalTemplate, List<SecurableEntity> filterPortalWindow,
			List<SecurableEntity> filterClassificationItem, List<ApplicationGroup> authorizations) {
			FormResponse response;
			var broker = Helper.CreateInstance<AuthorizationItemBroker>();
			var applicationGroupBroker = Helper.CreateInstance<ApplicationGroupBroker>();
			using (var t = Helper.CreateTransactionScope()) {
				item.ItemType = "Role";
				item.FilterSpatial = filterSpatial;
				item.FilterReport = filterReport;
				item.FilterChart = filterChart;
				item.FilterDynamicDisplay = filterDynamicDisplay;
				item.FilterPlaylist = filterPlaylist;
				item.FilterChartScheduler = filterChartScheduler;
				item.FilterPortalTemplate = filterPortalTemplate;
				item.FilterPortalWindow = filterPortalWindow;
				item.FilterClassificationItem = filterClassificationItem;
				response = broker.FormCreate(item);
				if (response.success) {
					try {
						var authorizationItem = (AuthorizationItem)response.data;
						applicationGroupBroker.SaveApplicationGroupAuthorizationItem(authorizationItem, new CrudStore<ApplicationGroup> { create = authorizations });
						broker.InvalidateStorageCache();
						t.Complete();
					}
					catch (Exception ex) {
						response.success = false;
						response.msg = ex.Message;
					}
				}
			}
			return response;
		}

		/// <summary>
		/// Loads a AzMan Role item.
		/// </summary>
		/// <param name="Key">The key of the item.</param>
		/// <returns></returns>
		public FormResponse AzManRole_FormLoad(string Key) {
			var broker = Helper.CreateInstance<AuthorizationItemBroker>();
			var retVal = broker.FormLoad(Key);
			return retVal;
		}

		/// <summary>
		/// Deletes a AzMan Role item.
		/// </summary>
		/// <param name="item">The item of Role.</param>
		/// <returns></returns>
		public bool AzManRole_Delete(AuthorizationItem item) {
			var broker = Helper.CreateInstance<AuthorizationItemBroker>();
			var retVal = broker.DeleteRole(item);
			broker.InvalidateStorageCache();
			return retVal;
		}

		/// <summary>
		/// Updates an existing AzMan Role item.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <returns></returns>
		public FormResponse AzManRole_FormUpdate(AuthorizationItem item) {
			var broker = Helper.CreateInstance<AuthorizationItemBroker>();
			var retVal = broker.FormUpdate(item);
			broker.InvalidateStorageCache();
			return retVal;
		}


		/// <summary>
		/// Copies an azman role.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <returns>True if the copy worked, false otherwise.</returns>
		public bool AzManRole_Copy(AuthorizationItem item) {
			var broker = Helper.CreateInstance<AuthorizationItemBroker>();
			var retVal = broker.AzManRole_Copy(item);
			broker.InvalidateStorageCache();
			return retVal;
		}

		/// <summary>
		/// Generates the json structure of the AzMan Role treeview.
		/// </summary>
		/// <param name="Key">The Key of the parent node.</param>
		/// <param name="onlyRoles">if set to <c>true</c> return [only roles].</param>
		/// <returns>
		/// a list of treenodes in json.
		/// </returns>
		public List<TreeNode> AzManRole_GetTree(string Key, bool onlyRoles) {
			var broker = Helper.CreateInstance<AuthorizationItemBroker>();
			var retVal = broker.AzManRole_GetTree(Key);
			if (onlyRoles) {
				retVal = retVal.Where(val => val.entity is AuthorizationItem && (val.entity as AuthorizationItem).ItemType == "Role").ToList();
			}
			return retVal;
		}

		/// <summary>
		/// Gets the azman tree starting from the given item.
		/// </summary>
		/// <param name="key">The key of the azman item.</param>
		/// <returns>
		/// A list of tree nodes
		/// </returns>
		public List<TreeNode> AzManItem_GetAncestorTree(string key) {
			var broker = Helper.CreateInstance<AuthorizationItemBroker>();
			var retVal = broker.AzManItem_GetAncestorTree(key);
			return retVal;
		}

		/// <summary>
		/// Creates a new AzMan Task item.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <param name="operations">The crud store of operations.</param>
		/// <returns></returns>
		public FormResponse AzManTask_FormCreate(AuthorizationItem item, CrudStore<AuthorizationItem> operations) {
			FormResponse response;
			var broker = Helper.CreateInstance<AuthorizationItemBroker>();
			using (var t = Helper.CreateTransactionScope()) {
				item.ItemType = "Task";
				response = broker.FormCreate(item);
				if (response.success) {
					try {
						var authorizationItem = (AuthorizationItem)response.data;
						broker.SaveAuthorizationItemAuthorizationItem(authorizationItem, operations);
						broker.InvalidateStorageCache();
						t.Complete();
					}
					catch (Exception ex) {
						response.success = false;
						response.msg = ex.Message;
					}
				}
			}
			return response;
		}

		/// <summary>
		/// Updates an existing AzMan Task item.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <param name="operations">The crud store of operations.</param>
		/// <returns></returns>
		public FormResponse AzManTask_FormUpdate(AuthorizationItem item, CrudStore<AuthorizationItem> operations) {
			FormResponse response;
			var broker = Helper.CreateInstance<AuthorizationItemBroker>();
			using (var t = Helper.CreateTransactionScope()) {
				response = broker.FormUpdate(item);
				if (response.success) {
					try {
						var authorizationItem = (AuthorizationItem)response.data;
						broker.SaveAuthorizationItemAuthorizationItem(authorizationItem, operations);
						broker.InvalidateStorageCache();
						t.Complete();
					}
					catch (Exception ex) {
						response.success = false;
						response.msg = ex.Message;
					}
				}
			}
			return response;
		}

		/// <summary>
		/// Gets a store of all tasks.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="onlyUITasks">if set to <c>true</c> [only UI tasks].</param>
		/// <returns></returns>
		public JsonStore<AuthorizationItem> AzManTask_GetStore(PagingParameter paging, bool onlyUITasks) {
			var broker = Helper.CreateInstance<AuthorizationItemBroker>();
			var retVal = broker.Task_GetStore(paging, onlyUITasks);
			return retVal;
		}

		/// <summary>
		/// Gets a store of all AzMan Operation for a specific AzMan Task.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyAzManTask">The Key of the AzMan Task.</param>
		/// <returns></returns>
		public JsonStore<AuthorizationItem> AzManOperation_GetStoreByKeyAzManTask(PagingParameter paging, string KeyAzManTask) {
			if (string.IsNullOrEmpty(KeyAzManTask))
				return new JsonStore<AuthorizationItem>();

			var broker = Helper.CreateInstance<AuthorizationItemBroker>();
			var retVal = broker.GetChildren(KeyAzManTask).ToJsonStoreWithFilter(paging);
			return retVal;
		}

		/// <summary>
		/// Saves a crud store for an AzMan item Task.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<AuthorizationItem> AzManTask_SaveStore(CrudStore<AuthorizationItem> store) {
			var broker = Helper.CreateInstance<AuthorizationItemBroker>();
			ApplyItemTypeToCrudStore(store, "Task");
			var retVal = broker.SaveStore(store);
			broker.InvalidateStorageCache();
			return retVal;
		}

		/// <summary>
		/// Applies the item type to crud store.
		/// </summary>
		/// <param name="store">The store.</param>
		/// <param name="itemType">Type of the item.</param>
		private void ApplyItemTypeToCrudStore(CrudStore<AuthorizationItem> store, string itemType) {
			if (store.create != null) {
				foreach (var authorizationItem in store.create) {
					authorizationItem.ItemType = itemType;
				}
			}
			if (store.update != null) {
				foreach (var authorizationItem in store.update) {
					authorizationItem.ItemType = itemType;
				}
			}
			if (store.destroy != null) {
				foreach (var authorizationItem in store.destroy) {
					authorizationItem.ItemType = itemType;
				}
			}
		}
		/// <summary>
		/// Checks user credentials and creates an authentication ticket (cookie) if the credentials are valid.
		/// </summary>
		/// <param name="userName">The user name to be validated.</param>
		/// <param name="password">The password for the specified user.</param>
		/// <param name="createPersistentCookie">A value that indicates whether the authentication ticket remains valid across sessions.</param>
		/// <param name="provider">The provider used to check credentials</param>
		/// <param name="pageId">The page ID.</param>
		/// <param name="userNameLoginAs">The user name login as.</param>
		/// <returns>
		/// true if user credentials are valid; otherwise, false.
		/// </returns>
		public bool Login(string userName, string password, bool createPersistentCookie, string provider, out string pageId, string userNameLoginAs) {
			string parsedApplicationName, parsedUsername;
			string parsedLoginAsApplicationName = null, parsedLoginAsUsername = null;
			ParseApplicationAndUsername(userName, out parsedApplicationName, out parsedUsername);
			if (userNameLoginAs != null) {
				if (userNameLoginAs.Contains("\\")) {
					ParseApplicationAndUsername(userNameLoginAs, out parsedLoginAsApplicationName, out parsedLoginAsUsername);
				}
				else {
					parsedLoginAsUsername = userNameLoginAs;
					parsedLoginAsApplicationName = parsedApplicationName;
				}
			}
			ContextHelper.ApplicationName = parsedApplicationName;
			if (userNameLoginAs != null && parsedApplicationName != parsedLoginAsApplicationName) {
				throw new VizeliaSecurityException(Langue.error_unknown_domain);
			}
			userName = parsedUsername;

			var isUserValid = m_ProviderMembership.ValidateUser(userName, password);
			pageId = string.Empty;
			if (isUserValid) {
				if (userNameLoginAs != null) userName = parsedLoginAsUsername;
				var user = User_GetByUserName(userName);
				if (user == null) {
					throw new VizeliaSecurityException(string.Format(Langue.error_msg_invalid_authentication_username, userName));
				}
				// user name is case insensitive. Yet subsystems depend on its spelling matching the value from the Membership.userName.
				userName = user.UserName;
				DoesUserHaveAnyRoles(userName);
/*				
				DoesUserHaveAnyRoles(parsedUsername);
				if (parsedLoginAsUsername != null) DoesUserHaveAnyRoles(parsedLoginAsUsername);
*/
				LoginValidatedUser(userName, createPersistentCookie, out pageId);
			}
			else if (userNameLoginAs == null)
				SessionService.Abandon();
			return isUserValid;
		}

		/// <summary>
		/// Does the user have any roles.
		/// </summary>
		/// <param name="parsedUsername">The parsed username.</param>
		private static void DoesUserHaveAnyRoles(string parsedUsername) {
			if ((!VizeliaConfiguration.Instance.Authentication.DisablePermissions) &&
				!(Roles.Provider.GetRolesForUser(parsedUsername).Any())) {
				TracingService.Write(TraceEntrySeverity.Error,
									 string.Format("Access Denied for user {0}: Not authorized to any operation.", parsedUsername),
									 "Security", string.Empty);
				throw new VizeliaSecurityException(Langue.msg_access_denied_detailed);
			}
		}

		/// <summary>
		/// Logins the validated user.
		/// </summary>
		/// <param name="userName">Name of the user.</param>
		/// <param name="createPersistentCookie">if set to <c>true</c> [create persistent cookie].</param>
		/// <param name="pageId">The page id.</param>
		private void LoginValidatedUser(string userName, bool createPersistentCookie, out string pageId) {


			string sessionID = SessionService.SetAuthCookie(userName, createPersistentCookie, out pageId);
			// we store the sessionID in context to make it available to inner functions.
			// SessionService.Remove(SessionKey.const_session_roles); // we clear the temporary roles list to ensure getting the correct list from database.


			FOLProfileBase profile = FOLProfileBase.GetProfile(userName);

			profile.entity.IP = Helper.GetUserIP();
			profile.Save();

			// logs the user in LoginHistory
			m_ProviderMembership.AddLoginHistory(userName, sessionID, DateTime.UtcNow, null);

			User_SetFilters(userName, sessionID);
		}

		/// <summary>
		/// Starts an sso login process
		/// </summary>
		/// <param name="providerName">The key.</param>
		/// <returns></returns>
		public void SSOLogin(string providerName) {

			string userName;
			string applicationName;
			string errorRedirectTo;
			var isValidated = SSOService.Validate(out userName, out applicationName, out errorRedirectTo, providerName);
			string redirectTo = errorRedirectTo;
			ContextHelper.ApplicationName = applicationName;
			if (isValidated) {
				bool isUser = IsUser(userName);
				if (isUser && (Roles.Provider.GetRolesForUser(userName).Any())) {
					string pageId;
					LoginValidatedUser(userName, false, out pageId);
					redirectTo = GetLoginPage();
				}
				else if (!Roles.Provider.GetRolesForUser(userName).Any()) {
					TracingService.Write(TraceEntrySeverity.Error, string.Format("Access Denied for user {0}: Not authorized to any operation.", userName), "Security", string.Empty);
				}
			}
			if (!string.IsNullOrWhiteSpace(HttpContext.Current.Request.Url.Query)) {
				HttpContext.Current.Response.AddHeader("SSO", HttpContext.Current.Request.Url.AbsoluteUri.Replace(HttpContext.Current.Request.Url.Query, ""));
			}
			HttpContext.Current.Response.Redirect(redirectTo);
		}

		/// <summary>
		/// Gets the login page.
		/// </summary>
		/// <returns></returns>
		private string GetLoginPage() {
			var url = HttpContext.Current.Request.Url.AbsoluteUri.Replace(HttpContext.Current.Request.Url.PathAndQuery, "");
			return url;
		}



		/// <summary>
		/// Determines whether the specified username is user.
		/// We need this because the sso could return a user that is authenticated but does not have a user in the system.
		/// </summary>
		/// <param name="username">The username.</param>
		/// <returns>
		///   <c>true</c> if the specified username is user; otherwise, <c>false</c>.
		/// </returns>
		private bool IsUser(string username) {
			var user = User_GetByUserName(username);
			return (user != null);
		}



		/// <summary>
		/// Parses the application and username.
		/// </summary>
		/// <param name="loginString">The login string.</param>
		/// <param name="applicationName">Name of the application.</param>
		/// <param name="userName">Name of the user.</param>
		public void ParseApplicationAndUsername(string loginString, out string applicationName, out string userName) {
			switch (TenantProvisioningService.Mode) {
				case MultitenantHostingMode.Isolated:
					ParseApplicationAndUsernameForIsolatedTenant(loginString, out applicationName, out userName);
					break;
				case MultitenantHostingMode.Shared:
					ParseApplicationAndUsernameForSharedTenancy(loginString, out applicationName, out userName);
					break;
				default:
					throw new ArgumentOutOfRangeException("TenantProvisioningService.Mode");
			}

		}

		/// <summary>
		/// Parses the application and username for isolated tenant scenario, in which the tenant has its own URL, 
		/// and the user does not have to specify an application name in the login string.
		/// </summary>
		/// <param name="loginString">The login string.</param>
		/// <param name="applicationName">Name of the application.</param>
		/// <param name="userName">Name of the user.</param>
		private static void ParseApplicationAndUsernameForIsolatedTenant(string loginString, out string applicationName, out string userName) {
			applicationName = VizeliaConfiguration.Instance.Deployment.ApplicationName;
			string[] parts = loginString.Split('\\');

			if (parts.Length == 1) {
				userName = parts[0];
			}
			else if (parts.Length == 2) {
				if (!applicationName.ToLower().Equals(parts[0].ToLower())) {
					throw new VizeliaException(Langue.error_unknown_domain);
				}

				userName = parts[1];
			}
			else {
				// We got too many or too little "\" characters in the string, cannot parse.
				throw new VizeliaException(string.Format(Langue.error_msg_invalid_authentication_username, loginString));
			}
		}

		/// <summary>
		/// Parses the application and username for shared tenancy scenario, in which some tenants may have their own URLs,
		/// and some tenants may share the same URL. In the latter case, the user needs to specify an application name in the login string.
		/// </summary>
		/// <param name="loginString">The login string.</param>
		/// <param name="applicationName">Name of the application.</param>
		/// <param name="userName">Name of the user.</param>
		private static void ParseApplicationAndUsernameForSharedTenancy(string loginString, out string applicationName, out string userName) {
			string[] parts = loginString.Split('\\');

			var tenancyBusinessLayer = Helper.Resolve<ITenancyBusinessLayer>();
			List<Tenant> tenantsForHostHeader = tenancyBusinessLayer.GetTenantsForCurrentRequestHostHeader();
			bool tenantHasItsOwnUrl = tenantsForHostHeader.Count == 1;

			if (tenantHasItsOwnUrl) {
				Tenant tenantForHostHeader = tenantsForHostHeader[0];
				// We found the tenant from the host header. It means this tenant has its own URL,
				// in which you may only login only as a user of that tenant.
				// The login may or may not contain the application name as prefix (it's redundant yet allowed).
				if (parts.Length == 1) {
					// Application name not specified in login string.
					userName = parts[0];
					applicationName = tenantForHostHeader.Name;
				}
				else if (parts.Length == 2) {
					// Application name specified in login string.
					if (tenantForHostHeader.Name.ToLower().Equals(parts[0].ToLower())) {
						userName = parts[1];
						applicationName = tenantForHostHeader.Name;
					}
					else {
						throw new VizeliaException(Langue.error_unknown_domain);
					}
				}
				else {
					// We got too many or too little "\" characters in the string, cannot parse.
					throw new VizeliaException(String.Format(Langue.error_invalid_characters_in_username, "\\"));
				}
			}
			else {
				// We found several tenants corresponding with the host header. It means this is a single website that hosts 
				// multiple tenants, in which you must specify both application name and username in format "Domain\username".
				if (parts.Length == 2) {
					// We got both username and application name.
					// Check that the requested tenant is hosted at the requested URL.
					var tenant = tenantsForHostHeader.FirstOrDefault(t => t.Name.ToLower().Equals(parts[0].ToLower()));
					if (tenant == null) {
						// The tenant has a different URL, you must login from it and not from a multiple tenants host URL.
						throw new VizeliaException(Langue.error_unknown_domain);
					}

					applicationName = tenant.Name;
					userName = parts[1];
				}
				else if (parts.Length == 1) {
					throw new VizeliaException(Langue.error_missing_domain_in_username);
				}
				else {
					// We got too many "\" characters in the string, cannot parse.
					throw new VizeliaException(String.Format(Langue.error_invalid_characters_in_username, "\\"));
				}
			}
		}

		/// <summary>
		/// Deletes an existing business entity LoginHistory.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool LoginHistory_Delete(LoginHistory item) {
			LoginHistoryBroker broker = Helper.CreateInstance<LoginHistoryBroker>();
			return broker.Delete(item);
		}

		/// <summary>
		/// Gets a list for the business entity LoginHistory. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		public List<LoginHistory> LoginHistory_GetAll(PagingParameter paging, PagingLocation location) {
			int total;
			LoginHistoryBroker broker = Helper.CreateInstance<LoginHistoryBroker>();
			return broker.GetAll(paging, location, out total);
		}

		/// <summary>
		/// Gets a json store for the business entity LoginHistory.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<LoginHistory> LoginHistory_GetStore(PagingParameter paging, PagingLocation location) {
			LoginHistoryBroker broker = Helper.CreateInstance<LoginHistoryBroker>();
			return broker.GetStore(paging, location);
		}

		/// <summary>
		/// Saves a crud store for the business entity LoginHistory.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<LoginHistory> LoginHistory_SaveStore(CrudStore<LoginHistory> store) {
			LoginHistoryBroker broker = Helper.CreateInstance<LoginHistoryBroker>();
			return broker.SaveStore(store);
		}

		/// <summary>
		/// Gets a json store for the business entity LoginHistory active.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<LoginHistory> LoginHistoryActive_GetStore(PagingParameter paging, PagingLocation location) {
			LoginHistoryActiveBroker broker = Helper.CreateInstance<LoginHistoryActiveBroker>();
			//LoginHistoryActiveBroker broker = new LoginHistoryActiveBroker();
			return broker.GetStore(paging, location);
		}

		/// <summary>
		/// Checks user credentials and creates an authentication ticket (cookie) if the credentials are valid.
		/// Returns a detail message with the reason for not login the user.
		/// </summary>
		/// <param name="userName">The user name to be validated.</param>
		/// <param name="password">The password for the specified user.</param>
		/// <param name="createPersistentCookie">A value that indicates whether the authentication ticket remains valid across sessions.</param>
		/// <param name="provider">The provider used to check credentials</param>
		/// <param name="userNameLoginAs">The user name login as.</param>
		/// <returns>
		/// true if user credentials are valid; otherwise, false.
		/// </returns>
		public FormResponse LoginResponse(string userName, string password, bool createPersistentCookie, string provider, string userNameLoginAs) {
			//To prevent a sitaution where a client doesn't respect the AllowRememberMe we enforce it in the server side too.
			createPersistentCookie = createPersistentCookie && VizeliaConfiguration.Instance.Authentication.AllowRememberMe;
			FormResponse response = new FormResponse();
			try {
				if (userNameLoginAs != null && !VizeliaConfiguration.Instance.Authentication.AllowLoginAsUser) {
					throw new VizeliaSecurityException(Langue.error_msg_loginasuser_not_allowed);
				}
				if (!LicencingHelper.Validate()) {
					throw new VizeliaSecurityException(Langue.msg_unable_to_validate_licence);
				}
				MembershipProvider currentProvider = Membership.Provider;
				string pageid;
				response.success = Login(userName, password, createPersistentCookie, provider, out pageid, userNameLoginAs);
				response.msg = pageid;
				HttpContext.Current.Response.Headers.Add(SessionService.const_page_id, pageid);
				string parsedApplicationName, parsedUsername;
				ParseApplicationAndUsername(userName, out parsedApplicationName, out parsedUsername);
				if (!response.success) {
					MembershipUser user = currentProvider.GetUser(parsedUsername, false);
					if (user == null) {
						response.msg = String.Format(Langue.error_msg_invalid_authentication_username, userNameLoginAs ?? userName);
					}
					else {
						if (!user.IsApproved)
							response.msg = Langue.error_msg_invalid_authentication_unapproved;
						else if (user.IsLockedOut) {
							response.msg = Langue.error_msg_invalid_authentication_lockedout;
						}
						else {
							response.msg = String.Format("{0}<br>{1}", Langue.error_msg_invalid_authentication,
														 Langue.error_msg_invalid_authentication_case_warning);
						}
					}
				}
			}
			catch (Exception ex) {
				response.msg = ex.Message;
				response.data = new SimpleListElement { Id = ContextHelper.ApplicationName, Value = userName }; //FolMembershipUser doesn't work here for some reason.
				response.success = false;
			}
			//Change password isn't an error, its only caused when the user inputs the correct username and old password.
			if ((!response.success) && (VizeliaConfiguration.Instance.Deployment.ObtuseLoginResponses) && (response.msg != Langue.error_password_expired)) {
				response.msg = Langue.msg_access_denied_generic;
			}
			return response;
		}

		/// <summary>
		/// Deletes from browser the authentication cookie.
		/// </summary>
		public void Logout() {
			string userName = null;
			MembershipUser user = Membership.GetUser();
			if (user != null)
				userName = user.UserName;
			// logs the user in LoginHistory
			m_ProviderMembership.AddLoginHistory(userName, SessionService.GetSessionID(), null, DateTime.UtcNow);
			SessionService.Abandon();
		}

		/// <summary>
		/// Gets all the membership providers declared in the membership section of web.config.
		/// </summary>
		public List<string> MembershipProvider_GetList() {
			List<string> result = new List<string>();

			MembershipSection membership = (MembershipSection)ConfigurationManager.GetSection("system.web/membership");
			if (((membership.DefaultProvider == null) || (membership.Providers == null)) || (membership.Providers.Count < 1)) {
				throw new ProviderException("Could not retreive membership section");
			}
			foreach (ProviderSettings settings in membership.Providers) {
				Type c = Type.GetType(settings.Type, true, true);
				if (!typeof(MembershipProvider).IsAssignableFrom(c)) {
					throw new ArgumentException(String.Format("Provider must implement type {0}", typeof(MembershipProvider)));
				}
				MembershipProvider provider = (MembershipProvider)Activator.CreateInstance(c);
				NameValueCollection parameters = settings.Parameters;
				NameValueCollection config = new NameValueCollection(parameters.Count, StringComparer.Ordinal);
				foreach (string str in parameters) {
					config[str] = parameters[str];
				}
				provider.Initialize(settings.Name, config);
				result.Add(provider.Name + " " + provider.Description);
			}
			return result;
		}


		/// <summary>
		/// Gets the store of application group for a specific user.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="username">The user name.</param>
		/// <returns></returns>
		public JsonStore<ApplicationGroup> User_ApplicationGroup_GetStore(PagingParameter paging, string username) {
			var broker = Helper.CreateInstance<ApplicationGroupBroker>();
			var retVal = broker.GetStoreByUser(username).ToJsonStoreWithFilter(paging);
			return retVal;
		}

		/// <summary>
		/// Changes the password for a membership user.
		/// This method works regardless of the password format (clear, encrypted, hashed).
		/// It requires that requiresPasswordAndQuestion is set to false.
		/// </summary>
		/// <param name="username">The user name.</param>
		/// <param name="newPassword">The new password.</param>
		/// <returns>True if operation was successfull, false otherwise.</returns>
		public bool User_ChangePassword(string username, string newPassword) {
			if (username == newPassword) {
				throw new ArgumentException(Langue.msg_username_same_as_password);
			}
			string parsedApplicationName, parsedUsername;
			if (!username.Contains("\\"))
				username = ContextHelper.ApplicationName + "\\" + username;
			ParseApplicationAndUsername(username, out parsedApplicationName, out parsedUsername);
			username = parsedUsername;
			return m_ProviderMembership.ChangePassword(username, newPassword);
		}

		/// <summary>
		/// Changes the password for a membership user through a form with old password and new password.
		/// </summary>
		/// <param name="username">The user name.</param>
		/// <param name="oldPassword">The old password.</param>
		/// <param name="newPassword">The new password.</param>
		/// <returns></returns>
		public FormResponse User_ChangePasswordForm(string username, string oldPassword, string newPassword) {
			string parsedApplicationName, parsedUsername;
			if (!username.Contains("\\"))
				username = ContextHelper.ApplicationName + "\\" + username;
			ParseApplicationAndUsername(username, out parsedApplicationName, out parsedUsername);
			ContextHelper.ApplicationName = parsedApplicationName;
			username = parsedUsername;
			FormResponse response = new FormResponse { success = false };
			try {
				if (oldPassword == newPassword) {
					throw new ArgumentException(Langue.msg_oldpassword_same_as_newpassword);
				}
				if (username == newPassword) {
					throw new ArgumentException(Langue.msg_username_same_as_password);
				}

				response.success = m_ProviderMembership.ChangePassword(username, oldPassword, newPassword);
				// if response is false, it means that oldPassword is incorrect.
				if (response.success) {
					m_ProviderMembership.SwitchIsForgotPasswordOff(username);
				}
				else {
					response.AddError("oldPassword", Langue.error_msg_invalid_authentication);
				}
			}
			catch (Exception ex) {
				response.msg = ex.Message;
				response.success = false;
			}
			return response;
		}

		/// <summary>
		/// Changes the password format for an existing user.
		/// This is necessary when modifying web.config because existing password are not updated when modifying passwordFormat.
		/// </summary>
		/// <param name="username">The user name.</param>
		/// <returns>True if operation was successfull, false otherwise.</returns>
		public bool User_ChangePasswordFormat(string username) {
			return m_ProviderMembership.ChangePasswordFormat(username);
		}

		/// <summary>
		/// Deletes a user.
		/// </summary>
		/// <param name="user">The user.</param>
		/// <returns>True if operation was successfull, false otherwise.</returns>
		public bool User_Delete(FOLMembershipUser user) {
			return User_Delete(user.UserName);
		}

		/// <summary>
		/// Deletes a user.
		/// </summary>
		/// <param name="username">The user name.</param>
		/// <returns>True if operation was successfull, false otherwise.</returns>
		public bool User_Delete(string username) {
			using (TransactionScope t = Helper.CreateTransactionScope()) {
				var userBroker = Helper.CreateInstance<FOLMembershipUserBroker>();
				var success = userBroker.Delete(username);
				var broker = Helper.CreateInstance<AuthorizationItemBroker>();
				broker.InvalidateStorageCache();
				t.Complete();

				return success;
			}
		}

		/// <summary>
		/// Deletes an list of users.
		/// </summary>
		/// <param name="usernames">The list of user names.</param>
		/// <returns>True if operation was successfull, false otherwise.</returns>
		public bool User_DeleteList(List<string> usernames) {
			return usernames.Select(User_Delete).All(result => result);
		}

		/// <summary>
		/// Creates a new user.
		/// </summary>
		/// <param name="user">The user.</param>
		/// <returns>
		/// A FormResponse with the status of the operation.
		/// </returns>
		public FormResponse User_FormCreate(FOLMembershipUser user) {
			return User_FormCreate(user, user.InitialPassword, user.UserMustChangePasswordAtNextLogon);
		}

		/// <summary>
		/// Creates a new user.
		/// </summary>
		/// <param name="user">The user.</param>
		/// <param name="password">The password.</param>
		/// <param name="UserMustChangePasswordAtNextLogon">if set to <c>true</c> [user must change password at next logon].</param>
		/// <returns>
		/// A FormResponse with the status of the operation.
		/// </returns>
		public FormResponse User_FormCreate(FOLMembershipUser user, string password, bool UserMustChangePasswordAtNextLogon = true) {
			if (user == null)
				throw new ArgumentNullException("user");

			var broker = Helper.CreateInstance<FOLMembershipUserBroker>();
			user.Password = password;
			user.UserMustChangePasswordAtNextLogon = UserMustChangePasswordAtNextLogon;
			FormResponse response = broker.FormCreate(user);
			
			return response;
		}

		/// <summary>
		/// Updates an existing user.
		/// </summary>
		/// <param name="user">The user.</param>
		/// <returns>A FormResponse whith the status of the operation.</returns>
		public FormResponse User_FormUpdate(FOLMembershipUser user) {
			return User_FormUpdate(user, user.InitialPassword);
		}

		/// <summary>
		/// Updates an existing user.
		/// </summary>
		/// <param name="user">The user.</param>
		/// <param name="password">The password. If null the stored password remains unchanged.</param>
		/// <returns>A FormResponse whith the status of the operation.</returns>
		public FormResponse User_FormUpdate(FOLMembershipUser user, string password) {
			if (user == null)
				throw new ArgumentNullException("user");
			if (!String.IsNullOrEmpty(password)) {
				user.Password = password;
			}
            
            // Backward comptability, not overriding the user's prefernces if it wasn't supplied.
            if (user.Preferences != null) {
                Helper.CreateInstance<UserPreferencesBroker>().Save(user.Preferences);
            }

			var usersBroker = Helper.CreateInstance<FOLMembershipUserBroker>();
			var userResponse = usersBroker.FormUpdate(user);
			
			return userResponse;
		}

		/// <summary>
		/// Gets a user.
		/// </summary>
		/// <param name="Key">The key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns>
		/// A FOLMembershipUser
		/// </returns>
		public FOLMembershipUser User_GetItem(string Key, params string[] fields) {
			var broker = Helper.CreateInstance<FOLMembershipUserBroker>();
			return broker.GetItem(Key, fields);
		}

		/// <summary>
		/// Gets a user from it's username.
		/// </summary>
		/// <param name="username">The user name.</param>
		/// <returns>A FOLMembershipUser.</returns>
		public FOLMembershipUser User_GetByUserName(string username) {
			var broker = Helper.CreateInstance<FOLMembershipUserBroker>();
			var user = broker.User_GetByUserName(username);
			user.Preferences = Helper.CreateInstance<UserPreferencesBroker>().GetItem(user.KeyUser);
			return user;
		}

		/// <summary>
		/// Gets the current user.
		/// </summary>
		/// <returns>A FOLMembershipUser.</returns>
		public FOLMembershipUser User_GetCurrent() {
			MembershipUser user = Membership.GetUser();
			if (user == null) {
				return null;
			}

			User_SetFilters(user.UserName, SessionService.GetSessionID());
			var folMembershipUser = new FOLMembershipUser(user);
			folMembershipUser.Preferences = Helper.CreateInstance<UserPreferencesBroker>().GetItem(folMembershipUser.KeyUser);

			return folMembershipUser;
		}

		/// <summary>
		/// Builds and store in session the filter spatial for a specific user.
		/// </summary>
		/// <param name="userName">The user we need to build the filter for.</param>
		/// <param name="sessionID">The session id.</param>
		public void User_SetFilters(string userName, string sessionID) {
			FilterHelper.ClearFilters();
			FilterHelper.InitializeFilters();
			// get all the securable types (we can cache this in memory)
			List<Type> securableTypes = Helper.GetSecurableTypes();
			var user = Membership.GetUser(userName);
			FOLProviderUserKey providerUserKey = (FOLProviderUserKey)user.ProviderUserKey;
			Dictionary<string, List<SecurableEntity>> filtersDict = m_ProviderRole.GetFiltersDictionaryForUser(userName, securableTypes);
			var dictionary = new Dictionary<Type, List<Filter>>();

			foreach (Type securableType in securableTypes) {
				List<SecurableEntity> filter = filtersDict[securableType.FullName];
				List<Filter> queryList = filter.Select(e => new Filter(e.KeySecurable)).ToList();
				if (securableType == typeof(Location)) {
					// TODO : We should access through AuthorizationDataAccess
					// When a filter location exist for the user, in order to avoid that this filter is not coherent with user location, we add to it the user location.
					if (queryList.Count > 0) {
						//var userFOL = new FOLMembershipUser(user);
						var userKeyLocation = providerUserKey.Occupant.KeyLocation;
						if (!String.IsNullOrEmpty(userKeyLocation))
							queryList.Add(new Filter(userKeyLocation));
					}
					List<Filter> spatialHierarchy = m_CoreDataAccess.GetSpatialHierarchy(queryList.Select(q => q.Key).ToArray());
					dictionary.Add(typeof(Location), spatialHierarchy);
				}
				else if (securableType == typeof(ClassificationItem)) {
					if (queryList.Count > 0) {
						List<Filter> classificationItemHierarchy = m_CoreDataAccess.GetClassificationItemHierarchy(queryList.Select(q => q.Key).ToArray());
						dictionary.Add(typeof(ClassificationItem), classificationItemHierarchy);
					}
				}
				else {
					List<string> createdEntitys = m_CoreDataAccess.GetCreatedEntity(providerUserKey.KeyUser, securableType);
					if (createdEntitys.Count > 0)
						queryList.AddRange(createdEntitys.Select(createdEntityKey => new Filter(createdEntityKey)));
					dictionary.AddOrMerge(securableType, queryList);
					//we need to add all of the portlets keys contained in the portalwindows to allow access to the charts.
					if (securableType == typeof(PortalWindow)) {
						var energyBusinessLayer = Helper.Resolve<IEnergyBusinessLayer>();
						filter.ForEach(filterPortalWindow => {
							Dictionary<Type, List<string>> dict = energyBusinessLayer.PortalWindow_GetPortletKeys(filterPortalWindow.KeySecurable);
							foreach (var kvp in dict) {
								if (securableTypes.Contains(kvp.Key)) {
									dictionary.AddOrMerge(kvp.Key, kvp.Value.Select(createdEntityKey => new Filter(createdEntityKey)).ToList(), true);
								}
							}
						});

					}
				}
			}
			FilterHelper.AddFilters(dictionary);
		}

		/// <summary>
		/// Gets the list of users.
		/// </summary>
		/// <returns>The list of users.</returns>
		protected List<FOLMembershipUser> User_GetList() {
			MembershipUserCollection userCollection = Membership.GetAllUsers();
			return (from MembershipUser user in userCollection where user.ProviderUserKey != null select new FOLMembershipUser(Membership.GetUser(user.ProviderUserKey, false))).ToList();
		}

		/// <summary>
		/// Gets the roles for user.
		/// </summary>
		/// <param name="username">The username.</param>
		/// <returns></returns>
		public string[] User_GetRoleList(string username) {
			return Roles.Provider.GetRolesForUser(username);
		}

		/// <summary>
		/// Gets a list of Users.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="location">The location.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public List<FOLMembershipUser> User_GetAll(PagingParameter paging, PagingLocation location, params string[] fields) {
			int total;
			FOLMembershipUserBroker broker = Helper.CreateInstance<FOLMembershipUserBroker>();
			return broker.GetAll(paging, location, out total, fields: fields);
		}

		/// <summary>
		/// Gets the store of users.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="fields">The fields.</param>
		/// <returns>The store of all users.</returns>
		public JsonStore<FOLMembershipUser> User_GetStore(PagingParameter paging, params string[] fields) {
			JsonStore<FOLMembershipUser> users = m_ProviderMembership.GetUsersStore(paging, fields);
			return users;
		}

		/// <summary>
		/// Gets a store of users filtered by locations and roles.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="locations">The list of KeyLocation.</param>
		/// <param name="roles">The list of roles.</param>
		/// <returns>The store of users.</returns>
		public JsonStore<FOLMembershipUser> User_GetStoreFiltered(PagingParameter paging, List<string> locations, List<string> roles) {
			return User_GetFilteredList(locations, roles).ToJsonStoreWithFilter(paging);
		}

		/// <summary>
		/// Gets a list of users filtered by locations and roles.
		/// </summary>
		/// <param name="locations">The list of KeyLocation.</param>
		/// <param name="roles">The list of roles.</param>
		protected List<FOLMembershipUser> User_GetFilteredList(List<string> locations, List<string> roles) {
			return m_ProviderMembership.GetUserListFiltered(locations, roles);
		}

		/// <summary>
		/// Generates the json structure of the User treeview.
		/// </summary>
		/// <returns>a list of treenodes in json.</returns>
		//[PrincipalPermission(SecurityAction.Demand, Role = "op_GetLangue")]
		public List<TreeNode> User_GetTree() {
			return User_GetList().GetTree();
		}

		/// <summary>
		/// Resets the password for a membership user.
		/// </summary>
		/// <param name="username">The user name.</param>
		/// <param name="notify">True to notify the change, false otherwise.</param>
		public void User_ResetPassword(string username, bool notify) {
			var folMembershipUser = User_GetByUserName(username);
			if (folMembershipUser != null) {
				var newPassword = m_ProviderMembership.ResetPassword(username, false);
				if (notify) {
					var mailBroker = Helper.CreateInstance<MailBroker>();
					List<Mail> mails = mailBroker.GetListByClassificationItemLocalId(MailBroker.const_resetpassword);
					var folProviderUserKey = folMembershipUser.ProviderUserKey;
					var occupant = folProviderUserKey != null ? folProviderUserKey.Occupant : new Occupant();
					var mailMessages = MailService.ConvertMailsToMailMessages(new ModelDataResetPassword {
						Occupant = occupant,
						UserName = username,
						NewPassword = newPassword,
						Email = folMembershipUser.Email
					}, mails);
					foreach (var mailMessage in mailMessages) {
						MailService.Send(mailMessage);
					}
				}
			}
		}

		/// <summary>
		/// Resets the password for a membership user that forgot the password.
		/// </summary>
		/// <param name="username">The user name.</param>
		/// <param name="email">The email.</param>
		public FormResponse User_ForgotPassword(string username, string email) {
			var response = new FormResponse { success = true };
			try {
				string parsedUsername = null;
				// Validations start
				if (!string.IsNullOrWhiteSpace(email)) {
					if (!string.IsNullOrWhiteSpace(username)) {
						throw new Exception(Langue.error_msg_forgotpassword_toomanyparameters);
					}
					var userBroker = Helper.CreateInstance<FOLMembershipUserBroker>();
					int totalUsersForGivenEmail;
					var tenancyBusinessLayer = Helper.Resolve<ITenancyBusinessLayer>();
					List<Tenant> tenantsForHostHeader = tenancyBusinessLayer.GetTenantsForCurrentRequestHostHeader();

					var emailFilter = new GridFilter {
						field = "LoweredEmail",
						data = new GridData { comparison = "=", type = "string", value = email.ToLower() }
					};
					var appFilter = new GridFilter {
						field = "App",
						data = new GridData { type = "list", value = tenantsForHostHeader.Aggregate("", (s, tenant) => s + tenant.Name + ',') }
					};
					var filters = new List<GridFilter> { emailFilter, appFilter };
					var users = userBroker.GetAllFromAllTenants(new PagingParameter { filters = filters }, PagingLocation.Database, out totalUsersForGivenEmail, false);
					if (totalUsersForGivenEmail > 1) {
						throw new Exception(Langue.error_msg_forgotpassword_emailofmultipleusers);
					}
					if (totalUsersForGivenEmail == 0) {
						throw new Exception(Langue.error_msg_forgotpassword_wrongparameters);
					}
					var user = users[0];
					username = parsedUsername = user.UserName;
					ContextHelper.ApplicationName = user.ApplicationName;
				}
				if (string.IsNullOrWhiteSpace(username)) {
					throw new Exception(Langue.error_msg_forgotpassword_parametersnotsupplied);
				}
				if (parsedUsername == null) {
					string parsedApplicationName;
					ParseApplicationAndUsername(username, out parsedApplicationName, out parsedUsername);
					ContextHelper.ApplicationName = parsedApplicationName;
				}
				var folMembershipUser = User_GetByUserName(parsedUsername);
				if (folMembershipUser == null) {
					throw new Exception(Langue.error_msg_forgotpassword_wrongparameters);
				}
				// Validations end


				var newPassword = m_ProviderMembership.ResetPassword(parsedUsername, true);
				var mailBroker = Helper.CreateInstance<MailBroker>();
				List<Mail> mails = mailBroker.GetListByClassificationItemLocalId(MailBroker.const_forgotpassword);
				var folProviderUserKey = folMembershipUser.ProviderUserKey;
				var occupant = folProviderUserKey != null ? folProviderUserKey.Occupant : new Occupant();
				var mailMessages = MailService.ConvertMailsToMailMessages(new ModelDataForgotPassword {
					Occupant = occupant,
					UserName = folMembershipUser.UserName,
					NewPassword = newPassword,
					Email = folMembershipUser.Email
				}, mails);
				foreach (var mailMessage in mailMessages) {
					MailService.Send(mailMessage);
				}
			}
			catch (Exception exception) {
				response.success = false;
				response.msg = exception.Message;
			}
			return response;
		}

		/// <summary>
		/// Saves a list of User.
		/// </summary>
		/// <param name="action">The Crud action. <see cref="CrudAction"/></param>
		/// <param name="records">The list of User to save.</param>
		/// <returns>True if the action was successfull, false otherwise.</returns>
		public OperationStatusStore<FOLMembershipUser> User_Save(string action, List<FOLMembershipUser> records) {
			OperationStatusStore<FOLMembershipUser> store = new OperationStatusStore<FOLMembershipUser> { success = true };
			foreach (FOLMembershipUser user in records) {
				MembershipUser result;
				try {
					MembershipCreateStatus status;
					result = Membership.CreateUser(user.UserName, "toto", "thaiat@hotmail.com", "blablabla", "blablabla", true, user.ProviderUserKey, out status);
				}
				catch {
					store.success = false;
					result = null;
				}
				store.records.Add(new FOLMembershipUser(result));
			}
			return store;
		}

		/// <summary>
		/// Saves the store of users.
		/// </summary>
		/// <param name="store">The crud store to save.</param>
		/// <returns></returns>
		public JsonStore<FOLMembershipUser> User_SaveStore(CrudStore<FOLMembershipUser> store) {
			var result = new JsonStore<FOLMembershipUser> { success = true };
			using (TransactionScope t = Helper.CreateTransactionScope()) {
				if (store.destroy != null) {
					//var deletedUsersStore = new CrudStore<FOLMembershipUser> { destroy = store.destroy };

					//var allProjects = m_ProviderRole.AzManFilter_GetTree(" ");
					//foreach (var project in allProjects) {

					//    var authorizationItem = project.entity as AuthorizationItem;
					//    if (authorizationItem != null) {
					//        m_ProviderRole.CreateAzManItemMembers(authorizationItem, deletedUsersStore);
					//    }
					//}

					foreach (var user in store.destroy) {
						m_ProviderRole.RemoveAuthorizationForUser(user.UserName);
						result.success = User_Delete(user.UserName);
						if (result.success)
							continue;
						result.records = null;
						return result;
					}
					m_ProviderRole.InvalidateStorageCache();
					t.Complete();
				}
				return result;
			}
		}

		/// <summary>
		/// Updates the UserName of a user.
		/// </summary>
		/// <param name="oldUserName">The old UserName.</param>
		/// <param name="newUserName">The new UserName.</param>
		/// <returns>True if the action was successfull, false otherwise.</returns>
		public bool User_ChangeUserName(string oldUserName, string newUserName) {
			bool success = ((IFOLSqlMembershipProvider)Membership.Provider).ChangeUserName(oldUserName, newUserName);
			return success;
		}

		/// <summary>
		/// Returns the Chart's KPI AzMan Role store.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="KeyChart">The key chart.</param>
		/// <returns></returns>
		public JsonStore<AuthorizationItem> Chart_AzManRole_GetStore(PagingParameter paging, string KeyChart) {
			return m_ProviderRole.AzManRole_GetStoreByKeyChartKPI(paging, KeyChart);
		}

		/// <summary>
		/// Reset license cache.
		/// </summary>
		public void License_ResetCache() {
			LicencingHelper.ResetCache();
		}
	}
}

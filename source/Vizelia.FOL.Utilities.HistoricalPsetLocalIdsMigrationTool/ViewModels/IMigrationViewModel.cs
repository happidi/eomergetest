﻿using HistoricalPsetLocalIdsMigrationTool.Models;

namespace Vizelia.FOL.Utilities.HistoricalPsetLocalIdsMigrationTool.ViewModels
{
	public interface IMigrationViewModel
	{
		/// <summary>
		/// Sets the selected chart data.
		/// </summary>
		/// <param name="selectedChart">The selected chart.</param>
		void SetSelectedChartData(Chart selectedChart);

		/// <summary>
		/// Sets the selected tenant charts.
		/// </summary>
		/// <param name="application">The application.</param>
		void DisplayChartsForTenant(string application);


		/// <summary>
		/// Fixes the data for chart.
		/// </summary>
		/// <param name="keyChart">The key chart.</param>
		/// <returns></returns>
		void FixDataForChart(int keyChart);

		/// <summary>
		/// Fixes all data.
		/// </summary>
		/// <returns></returns>
		void FixAllData();
	}
}
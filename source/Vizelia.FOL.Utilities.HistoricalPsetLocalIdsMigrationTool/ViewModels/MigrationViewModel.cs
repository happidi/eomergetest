﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using HistoricalPsetLocalIdsMigrationTool;
using HistoricalPsetLocalIdsMigrationTool.Commands;
using HistoricalPsetLocalIdsMigrationTool.Models;
using Microsoft.Practices.EnterpriseLibrary.Common.Utility;
using Vizelia.FOL.Utilities.HistoricalPsetLocalIdsMigrationTool.BusinessLayer;
using Vizelia.FOL.Utilities.HistoricalPsetLocalIdsMigrationTool.Commands;
using Vizelia.FOL.Utilities.HistoricalPsetLocalIdsMigrationTool.DataObjects;

namespace Vizelia.FOL.Utilities.HistoricalPsetLocalIdsMigrationTool.ViewModels {
	public class MigrationViewModel : DependencyObject, IMigrationViewModel
	{
		private const string const_no_data_selected_msg = "No data was selected.";
		private const string const_updating_status_msg = "Executing...";
		private const string const_execution_result_title = "Fix Results Notification.";
		private const string const_no_data_exist_msg = "No invalid data exists.";
		private const string const_get_invalid_data_msg = "Fetching invalid data. This might take a few minutes...";
		private const string const_data_loaded_msg = "Invalid data has been loaded.";
		private const string const_loading_data_failed_msg = "Failed to load invalid data.";
		private const string const_fix_all_completed_msg = "Fix all completed";

		private readonly ProgressUpdater m_ProgressUpdater = ProgressUpdater.Instance;
		private readonly IMigrationBusinessComponent m_MigrationBusinessComponent = new MigrationBusinessComponent();

		public static readonly DependencyProperty IsBusyProperty =
			DependencyProperty.Register("IsBusy", typeof (bool),
				typeof (MigrationViewModel), new FrameworkPropertyMetadata(false));
		
		public static readonly DependencyProperty IsFetchingDataProperty =
			DependencyProperty.Register("IsFetchingData", typeof(bool),
				typeof(MigrationViewModel), new FrameworkPropertyMetadata(false));

		public static readonly DependencyProperty StatusNotificationProperty =
			DependencyProperty.Register("StatusNotification", typeof (string),
				typeof (MigrationViewModel), new FrameworkPropertyMetadata(null));

		public static readonly DependencyProperty TotalCommandsToProcessProperty =
			DependencyProperty.Register("TotalCommandsToProcess", typeof (double),
				typeof (MigrationViewModel), new FrameworkPropertyMetadata(Double.MaxValue));

		public static readonly DependencyProperty NumOfProcessedCommandsProperty =
			DependencyProperty.Register("NumOfProcessedCommands", typeof (double),
				typeof (MigrationViewModel), new FrameworkPropertyMetadata((double) 0));

		public double NumOfProcessedCommands
		{
			get { return (double) GetValue(NumOfProcessedCommandsProperty); }
			set { SetValue(NumOfProcessedCommandsProperty, value); }
		}

		public double TotalCommandsToProcess
		{
			get { return (double) GetValue(TotalCommandsToProcessProperty); }
			set { SetValue(TotalCommandsToProcessProperty, value); }
		}

		public bool IsBusy
		{
			get { return (bool) GetValue(IsBusyProperty); }
			set { SetValue(IsBusyProperty, value); }
		}


		public bool IsFetchingData {
			get { return (bool)GetValue(IsFetchingDataProperty); }
			set { SetValue(IsFetchingDataProperty, value); }
		}

		public string StatusNotification
		{
			get { return (string) GetValue(StatusNotificationProperty); }
			set { SetValue(StatusNotificationProperty, value); }
		}


		private ObservableCollection<HistoricalPsetSerie> m_HistoricalPsetDataSeries =
			new ObservableCollection<HistoricalPsetSerie>();

		private ObservableCollection<HistoricalPsetCalculatedSerie> m_HistoricalPsetCalculatedSeries =
			new ObservableCollection<HistoricalPsetCalculatedSerie>();

		private ObservableCollection<HistoricalPsetDynamicDisplay> m_HistoricalPsetDynamicDisplays =
			new ObservableCollection<HistoricalPsetDynamicDisplay>();

		private ObservableCollection<HistoricalPsetCustomGridCell> m_HistoricalPsetCustomGrids =
			new ObservableCollection<HistoricalPsetCustomGridCell>();

		private ObservableCollection<Chart> m_ChartsData = new ObservableCollection<Chart>();
		private ObservableCollection<Tenant> m_Tenants = new ObservableCollection<Tenant>();

		public ObservableCollection<HistoricalPsetSerie> HistoricalPsetDataSeries
		{
			get { return m_HistoricalPsetDataSeries; }
			set { m_HistoricalPsetDataSeries = value; }
		}

		public ObservableCollection<HistoricalPsetCalculatedSerie> HistoricalPsetCalculatedSeries
		{
			get { return m_HistoricalPsetCalculatedSeries; }
			set { m_HistoricalPsetCalculatedSeries = value; }
		}

		public ObservableCollection<HistoricalPsetDynamicDisplay> HistoricalPsetDynamicDisplays
		{
			get { return m_HistoricalPsetDynamicDisplays; }
			set { m_HistoricalPsetDynamicDisplays = value; }
		}

		public ObservableCollection<HistoricalPsetCustomGridCell> HistoricalPsetCustomGrids
		{
			get { return m_HistoricalPsetCustomGrids; }
			set { m_HistoricalPsetCustomGrids = value; }
		}

		public ObservableCollection<Chart> ChartsData
		{
			get { return m_ChartsData; }
			set { m_ChartsData = value; }
		}

		public ObservableCollection<Tenant> Tenants
		{
			get { return m_Tenants; }
			set { m_Tenants = value; }
		}

		public MigrationViewModel()
		{
			Initialize();
			m_ProgressUpdater.ReportProgress += ProgressUpdaterReportProgress;
		}

		private void ProgressUpdaterReportProgress(object sender, int processedItems)
		{
			NumOfProcessedCommands = processedItems;
		}

		private void Initialize()
		{
			IsBusy = true;
			IsFetchingData = true;
			StatusNotification = const_get_invalid_data_msg;
			

			System.Threading.Tasks.Task<MigrationDto> task =
				System.Threading.Tasks.Task.Factory.StartNew(() => m_MigrationBusinessComponent.GetAllDataForMigration());
			task.ContinueWith(p =>
			{
				IsBusy = false;
				IsFetchingData = false;

				if (task.Status != System.Threading.Tasks.TaskStatus.Faulted)
				{
					MigrationDto migrationData = task.Result;
					if (!migrationData.TenantsDtos.Any()) {
						MessageBox.Show("There is no data to modify. All of the data is valid!");
						return;
					}

					StatusNotification = const_data_loaded_msg;

					InitTenants(migrationData.TenantsDtos, migrationData.ChartsDtos);
					InitCharts(migrationData.ChartsDtos);

					foreach (ChartDto chartDto in migrationData.ChartsDtos)
					{
						InitHistoricalPsetSeriesForChart(chartDto.KeyChart, migrationData.HistoricalPsetSerieDtos);
						InitHistoricalPsetCalculatedSeriesForChart(chartDto.KeyChart, migrationData.HistoricalPsetCalculatedSeriesDtos);
						InitHistoricalPsetDynamicDisplayForChart(chartDto.KeyChart, migrationData.HistoricalPsetDynamicDisplayDtos);
						InitHistoricalPsetCustomGridCellsForChart(chartDto.KeyChart, migrationData.HistoricalPsetCustomGridDtos);
					}
				}
				else
				{
					StatusNotification = const_loading_data_failed_msg;
					if (task.Exception != null)
					{
						MessageBox.Show(task.Exception.Flatten().ToString());
					}
				}

			}, System.Threading.Tasks.TaskScheduler.FromCurrentSynchronizationContext());
		}

		private void UpdateCollectionsAfterFixingData(int keyChart)
		{
			List<HistoricalPsetSerie> seriesFixed =
				HistoricalPsetDataSeries.Where(s => s.KeyChart == keyChart && s.IsSelected).ToList();
			foreach (HistoricalPsetSerie serie in seriesFixed)
			{
				HistoricalPsetDataSeries.Remove(serie);
			}

			List<HistoricalPsetCalculatedSerie> calculatedSeriesFixed =
				HistoricalPsetCalculatedSeries.Where(s => s.KeyChart == keyChart && s.IsSelected).ToList();
			foreach (HistoricalPsetCalculatedSerie serie in calculatedSeriesFixed)
			{
				HistoricalPsetCalculatedSeries.Remove(serie);
			}

			List<HistoricalPsetDynamicDisplay> chartsFixed =
				HistoricalPsetDynamicDisplays.Where(s => s.KeyChart == keyChart && s.IsSelected).ToList();
			foreach (var chart in chartsFixed)
			{
				HistoricalPsetDynamicDisplays.Remove(chart);
			}

			List<HistoricalPsetCustomGridCell> customGridsFixed =
				HistoricalPsetCustomGrids.Where(s => s.KeyChart == keyChart && s.IsSelected).ToList();
			foreach (var gridCell in customGridsFixed)
			{
				HistoricalPsetCustomGrids.Remove(gridCell);
			}

			if (HistoricalPsetDataSeries.All(s => s.KeyChart != keyChart) &&
			    HistoricalPsetCalculatedSeries.All(s => s.KeyChart != keyChart) &&
			    HistoricalPsetDynamicDisplays.All(s => s.KeyChart != keyChart) &&
			    HistoricalPsetCustomGrids.All(s => s.KeyChart != keyChart))
			{
				Chart fixedChart = ChartsData.FirstOrDefault(s => s.KeyChart == keyChart);
				ChartsData.Remove(fixedChart);

				Tenant tenant = Tenants.FirstOrDefault(t => t.Application == fixedChart.Application);
				int tenantInvalidChartsCount = ChartsData.Count(chart => chart.Application == tenant.Application);
				if (tenantInvalidChartsCount == 0)
				{
					Tenants.Remove(tenant);
				}
				else
				{
					tenant.TotalChartsToUpdate = tenantInvalidChartsCount;
				}
			}
			
		}

		private void ClearAllCollections()
		{
			Tenants.Clear();
			ChartsData.Clear();
			HistoricalPsetDataSeries.Clear();
			HistoricalPsetCalculatedSeries.Clear();
			HistoricalPsetDynamicDisplays.Clear();
			HistoricalPsetCustomGrids.Clear();
		}

		private void InitTenants(IEnumerable<TenantDto> tenantDtos, IEnumerable<ChartDto> chartDtos)
		{
			foreach (TenantDto tenantDto in tenantDtos)
			{
				Tenants.Add(new Tenant()
				{
					Application = tenantDto.Application,
					TotalChartsToUpdate = chartDtos.Count(c => c.Application == tenantDto.Application)
				});
			}
		}

		/// <summary>
		/// Sets the selected chart data.
		/// </summary>
		/// <param name="selectedChart">The selected chart.</param>
		public void SetSelectedChartData(Chart selectedChart)
		{
			var selectedCharts = new List<int>() {selectedChart.KeyChart};
			DisplayDataForSelectedCharts(selectedCharts, HistoricalPsetDataSeries);
			DisplayDataForSelectedCharts(selectedCharts, HistoricalPsetCalculatedSeries);
			DisplayDataForSelectedCharts(selectedCharts, HistoricalPsetDynamicDisplays);
			DisplayDataForSelectedCharts(selectedCharts, HistoricalPsetCustomGrids);
		}

		private void DisplayDataForSelectedCharts<TData>(IEnumerable<int> selectedChartKeys,
														 IEnumerable<TData> fixableDataModel) where TData : IFixableDataModel {
			foreach (var item in fixableDataModel) {
				item.IsVisible = selectedChartKeys.Contains(item.KeyChart);
				item.IsSelected = true;
			}
		}

		/// <summary>
		/// Sets the selected tenant charts.
		/// </summary>
		/// <param name="application">The application.</param>
		private void InitCharts(IEnumerable<ChartDto> chartDtos)
		{
			foreach (var chart in chartDtos)
			{
				ChartsData.Add(new Chart()
				{
					Application = chart.Application,
					KeyChart = chart.KeyChart,
					Title = chart.Title
				});
			}
		}

		public void DisplayChartsForTenant(string application)
		{
			foreach (var chart in ChartsData)
			{
				chart.IsVisible = chart.Application == application;
			}
		}

		/// <summary>
		/// Fixes the data for chart.
		/// </summary>
		/// <param name="keyChart">The key chart.</param>
		/// <returns></returns>
		public void FixDataForChart(int keyChart)
		{
			IEnumerable<ICommand> commands = CreateCommandsForChart(keyChart);
			if (!commands.Any())
			{
				MessageBox.Show(const_no_data_selected_msg,
							const_execution_result_title,
							MessageBoxButton.OK,
							MessageBoxImage.Warning);
				return;
			}

			StatusNotification = const_updating_status_msg;
			TotalCommandsToProcess = commands.Count();
			IsBusy = true;

			System.Threading.Tasks.Task<ICommandResult> task = System.Threading.Tasks.Task.Factory.StartNew(() => m_MigrationBusinessComponent.FixData(commands));
			task.ContinueWith(p => {
				IsBusy = false;
				
				ICommandResult result = task.Result;
				if (!result.IsFailure) {
					UpdateCollectionsAfterFixingData(keyChart);
					StatusNotification = "Successfully fixed all the selected data for chart : " + keyChart;
				}
				else {
					MessageBox.Show(string.Format("The following Error occurred while fixing selected data for chart {0} : {1} ",
													keyChart,
													result.ErrorMessage),
													const_execution_result_title,
													MessageBoxButton.OK,
													MessageBoxImage.Error);
				}
				TotalCommandsToProcess = Double.MaxValue;

			}, System.Threading.Tasks.TaskScheduler.FromCurrentSynchronizationContext());
		}


		public void FixAllData()
		{
			if (!Tenants.Any()) {
				MessageBox.Show(const_no_data_exist_msg,
					const_execution_result_title,
					MessageBoxButton.OK,
					MessageBoxImage.Warning);

				return;
			}

			IEnumerable<string> allTenantNames = Tenants.Select(t => t.Application);
			var allTenantsCharts = ChartsData.Where(c => allTenantNames.Contains(c.Application));

			var allCommands = new List<ICommand>();
			foreach (Chart chart in allTenantsCharts)
			{
				HistoricalPsetDataSeries.ForEach(s => s.IsSelected = true);
				HistoricalPsetCalculatedSeries.ForEach(s => s.IsSelected = true);
				HistoricalPsetDynamicDisplays.ForEach(s => s.IsSelected = true);
				HistoricalPsetCustomGrids.ForEach(s => s.IsSelected = true);

				IEnumerable<ICommand> chartCommands = CreateCommandsForChart(chart.KeyChart);
				if (chartCommands.Any())
				{
					allCommands.AddRange(chartCommands);
				}
			}

			TotalCommandsToProcess = allCommands.Count;
			StatusNotification = const_updating_status_msg;
			IsBusy = true;

			System.Threading.Tasks.Task<ICommandResult> task =
				System.Threading.Tasks.Task.Factory.StartNew(() => m_MigrationBusinessComponent.FixData(allCommands));

			task.ContinueWith(p => {
				ICommandResult result = task.Result;
				
				IsBusy = false;
				StatusNotification = const_fix_all_completed_msg;
				if (!result.IsFailure) {
					ClearAllCollections();
					MessageBox.Show("Successfully fixed all of the data!",
					const_execution_result_title,
					MessageBoxButton.OK,
					MessageBoxImage.Information);
				}
				else {
					MessageBox.Show(
					string.Format("The following Error occurred while fixing the data: " + result.ErrorMessage),
					const_execution_result_title,
					MessageBoxButton.OK,
					MessageBoxImage.Error);

				}

				TotalCommandsToProcess = Double.MaxValue;
			}, System.Threading.Tasks.TaskScheduler.FromCurrentSynchronizationContext());

		}

		private IEnumerable<ICommand> CreateCommandsForChart(int keyChart)
		{
			var commands = new List<ICommand>();

			IEnumerable<ICommand> updateDataSeriesCommands = CreateUpdateDataSeriesCommands(keyChart);
			if (updateDataSeriesCommands.Any())
			{
				commands.AddRange(updateDataSeriesCommands);
			}

			IEnumerable<ICommand> updateCalculatedSeriesCommands = CreateUpdateCalculatedSeriesCommands(keyChart);
			if (updateCalculatedSeriesCommands.Any())
			{
				commands.AddRange(updateCalculatedSeriesCommands);
			}

			IEnumerable<ICommand> updateDynamicDisplayCommands = CreateUpdateChartDynamicDisplayCommands(keyChart);
			if (updateDynamicDisplayCommands.Any())
			{
				commands.AddRange(updateDynamicDisplayCommands);
			}

			IEnumerable<ICommand> updateCustomGridCellsCommands = CreateUpdateCustomGridCellCommands(keyChart);
			if (updateCustomGridCellsCommands.Any())
			{
				commands.AddRange(updateCustomGridCellsCommands);
			}
			return commands;
		}

		private IEnumerable<ICommand> CreateUpdateDataSeriesCommands(int keyChart)
		{
			IList<ICommand> commands = new List<ICommand>();
			IEnumerable<HistoricalPsetSerie> chartSelectedDataSeries =
				HistoricalPsetDataSeries.Where(s => s.KeyChart == keyChart && s.IsSelected);
			if (!chartSelectedDataSeries.Any()) {
				return Enumerable.Empty<ICommand>();
			}

			foreach (var selectedSerie in chartSelectedDataSeries)
			{
				var command = new UpdateDataSerieCommand()
				{
					Application = selectedSerie.Application,
					KeyChart = keyChart,
					KeyDataSerie = selectedSerie.KeyDataSerie,
					NewLocalId = selectedSerie.NewLocalId,
					OldLocalId = selectedSerie.LocalId
				};

				commands.Add(command);
			}
			return commands;
		}

		private IEnumerable<ICommand> CreateUpdateCalculatedSeriesCommands(int keyChart) {
			
			IEnumerable<HistoricalPsetCalculatedSerie> chartSelectedCalculatedSeries = HistoricalPsetCalculatedSeries.Where(s => s.KeyChart == keyChart && s.IsSelected);

			if (!chartSelectedCalculatedSeries.Any()) {
				return Enumerable.Empty<ICommand>();
			}

			IList<ICommand> commands = new List<ICommand>();

			string chartApplication = chartSelectedCalculatedSeries.First().Application;

			//Since different historical psets can be combined into one formula, there will be several rows with new formulas according to each pset for the same serie key
			//all of these new formulas should be merged into one
			//For example formula :(Pset1 + Pset2) should be changed to=> (Pset1Name-Pset1AttributeName + Pset2Name-Pset2AttributeName)
			var groupedByKey = from s in chartSelectedCalculatedSeries
              group s by s.KeyCalculatedSerie into g
              select new { KeyCalculatedSerie = g.Key, Data = g.ToList() };

			foreach (var group in groupedByKey)
			{
				var command = new UpdateCalculatedSerieCommand()
				{
					Application = chartApplication,
					KeyChart = keyChart,
					OldFormula = group.Data.First().Formula,
					KeyCalculatedSerie = group.KeyCalculatedSerie
				};

				var firstSelectedSerieNewFormula = group.Data[0].NewFormula;
				if (group.Data.Count == 1)
				{
					command.NewFormula = firstSelectedSerieNewFormula;
					command.OldFormula = group.Data[0].Formula;
				}
				else
				{
					//Merge all the formulas for the same calculated serie key
					string newFormula = firstSelectedSerieNewFormula;

					foreach (var serie in group.Data)
					{
						newFormula = newFormula.Replace(serie.PsetMsgLocalizedValue,
														String.Format("{0}-{1}", serie.PsetName, serie.PsetAttributeName));
					}

					command.NewFormula = newFormula;
				}

				commands.Add(command);
			}

			return commands;
		}

		private IEnumerable<ICommand> CreateUpdateChartDynamicDisplayCommands(int keyChart) {
			
			IEnumerable<HistoricalPsetDynamicDisplay> chartSelectedDynamicDisplays = HistoricalPsetDynamicDisplays.Where(s => s.KeyChart == keyChart && s.IsSelected);

			if (!chartSelectedDynamicDisplays.Any())
			{
				return Enumerable.Empty<ICommand>();
			}

			IList<ICommand> commands = new List<ICommand>();
			string chartApplication = chartSelectedDynamicDisplays.First().Application;

			var groupedByKey = from s in chartSelectedDynamicDisplays
							   group s by s.KeyChart into g
							   select new { keyChart = g.Key, Data = g.ToList() };

			foreach (var group in groupedByKey) {
				var command = new UpdateChartDynamicDisplayCommand() {
					Application = chartApplication,
					KeyChart = group.keyChart,
					OldDynamicDisplayMainText = group.Data.First().DynamicDisplayMainText,
					OldDynamicDisplayHeaderText = group.Data.First().DynamicDisplayHeaderText,
					OldDynamicDisplayFooterText = group.Data.First().DynamicDisplayFooterText,
				};

				var firstSelectedChartNewDynamicDisplayMainText = group.Data[0].NewDynamicDisplayMainText;
				var firstSelectedChartNewDynamicDisplayHeaderText = group.Data[0].NewDynamicDisplayHeaderText;
				var firstSelectedChartNewDynamicDisplayFooterText = group.Data[0].NewDynamicDisplayFooterText;

				if (group.Data.Count == 1) {
					command.NewDynamicDisplayMainText = firstSelectedChartNewDynamicDisplayMainText;
					command.NewDynamicDisplayHeaderText = firstSelectedChartNewDynamicDisplayHeaderText;
					command.NewDynamicDisplayFooterText = firstSelectedChartNewDynamicDisplayFooterText;
				}
				else {
					//Merge all the new values for the same dynamic display 
					string newDynamicDisplayMainText = firstSelectedChartNewDynamicDisplayMainText;
					string newDynamicDisplayHeaderText = firstSelectedChartNewDynamicDisplayHeaderText;
					string newDynamicDisplayFooterText = firstSelectedChartNewDynamicDisplayFooterText;

					foreach (var chart in group.Data) {

						if (!String.IsNullOrWhiteSpace(chart.NewDynamicDisplayMainText))
						{
							newDynamicDisplayMainText = newDynamicDisplayMainText.Replace(chart.PsetMsgLocalizedValue,
														String.Format("{0}-{1}", chart.PsetName, chart.PsetAttributeName));
						}
						if (!String.IsNullOrWhiteSpace(chart.NewDynamicDisplayHeaderText)) {
							newDynamicDisplayHeaderText = newDynamicDisplayHeaderText.Replace(chart.PsetMsgLocalizedValue,
														  String.Format("{0}-{1}", chart.PsetName, chart.PsetAttributeName));
						}

						if (!String.IsNullOrWhiteSpace(chart.NewDynamicDisplayFooterText))
						{
							newDynamicDisplayFooterText = newDynamicDisplayFooterText.Replace(chart.PsetMsgLocalizedValue,
														String.Format("{0}-{1}", chart.PsetName, chart.PsetAttributeName));
						}
					}

					command.NewDynamicDisplayMainText = newDynamicDisplayMainText;
					command.NewDynamicDisplayHeaderText = newDynamicDisplayHeaderText;
					command.NewDynamicDisplayFooterText = newDynamicDisplayFooterText;
				}

				commands.Add(command);
			}

			return commands;
		}

		private IEnumerable<ICommand> CreateUpdateCustomGridCellCommands(int keyChart) {
		
			IEnumerable<HistoricalPsetCustomGridCell> chartSelectedCustomGridCells = HistoricalPsetCustomGrids.Where(s => s.KeyChart == keyChart && s.IsSelected);
			if (!chartSelectedCustomGridCells.Any()) {
				return Enumerable.Empty<ICommand>();
			}

			IList<ICommand> commands = new List<ICommand>();
			string chartApplication = chartSelectedCustomGridCells.First().Application;

			var groupedByKey = from s in chartSelectedCustomGridCells
							   group s by s.KeyChartCustomGridCell into g
							   select new { KeyChartCustomGridCell = g.Key, Data = g.ToList() };

			foreach (var group in groupedByKey) {
				var command = new UpdateCustomGridCellCommand() {
					Application = chartApplication,
					KeyChart = keyChart,
					OldTag = group.Data.First().Tag,
					KeyChartCustomGridCell = group.KeyChartCustomGridCell
				};

				var firstSelectedChartNewTag = group.Data[0].NewTag;
				if (group.Data.Count == 1)
				{
					command.OldTag = group.Data[0].Tag;
					command.NewTag = firstSelectedChartNewTag;
				}
				else {
					//Merge all the tags for the same custom grid cell
					string newTag = firstSelectedChartNewTag;
					string oldTag = String.Empty;

					foreach (var gridCell in group.Data) {

						oldTag = gridCell.Tag + "," + oldTag;

						newTag = newTag.Replace(gridCell.PsetMsgLocalizedValue,
														String.Format("{0}-{1}", gridCell.PsetName, gridCell.PsetAttributeName));
					}
					command.OldTag = oldTag;
					command.NewTag = newTag;
				}
				commands.Add(command);
			}

			return commands;
		}

		private void InitHistoricalPsetSeriesForChart(int keyChart, IEnumerable<HistoricalPsetSerieDto> seriesDtos)
		{
			IEnumerable<HistoricalPsetSerieDto> chartHistoricalPsetSeries = seriesDtos.Where(s => s.KeyChart == keyChart);

			foreach (var serie in chartHistoricalPsetSeries) {
				//There can be a scenario where for the same key, there will be several localIds if two different pset series,
				// With the same pset MSG_CODE are defined on the chart
				// This situation is a bug in the system.
				// In this case, the application logic only uses the first pset serie and ignores all others
				// Here we do the same
				if (HistoricalPsetDataSeries.FirstOrDefault(s => s.KeyDataSerie == serie.KeyDataSerie && s.PsetMsgCode == serie.PsetMsgCode) != null) {
					continue;
				}

				HistoricalPsetDataSeries.Add(new HistoricalPsetSerie() {
					Application = serie.Application,
					KeyChart = serie.KeyChart,
					KeyDataSerie = serie.KeyDataSerie,
					LocalId = serie.LocalId,
					PsetAttributeName = serie.PsetAttributeName,
					PsetMsgCode = serie.PsetMsgCode,
					PsetMsgLocalizedValue = serie.PsetMsgLocalizedValue,
					PsetName = serie.PsetName,
					PsetMsgCulture = serie.PsetMsgCulture
				});
			}
			RemoveDuplicateSeriesForSameKeyAndLocalValueIfExist(keyChart, HistoricalPsetDataSeries);
		}

		private void InitHistoricalPsetCalculatedSeriesForChart(int keyChart, IEnumerable<HistoricalPsetCalculatedSerieDto> calculatedSerieDtos)
		{
			IEnumerable<HistoricalPsetCalculatedSerieDto> chartHistoricalPsetCalculatedSerie = calculatedSerieDtos.Where(s => s.KeyChart == keyChart);

			foreach (var serie in chartHistoricalPsetCalculatedSerie)
			{
				
				if (HistoricalPsetCalculatedSeries.FirstOrDefault(s => s.KeyCalculatedSerie == serie.KeyCalculatedSerie &&
																	   s.PsetMsgCode == serie.PsetMsgCode) != null) {
					continue;
				}

				HistoricalPsetCalculatedSeries.Add(new HistoricalPsetCalculatedSerie()
				{
					Application = serie.Application,
					KeyChart = serie.KeyChart,
					KeyCalculatedSerie = serie.KeyCalculatedSerie,
					Formula = serie.Formula,
					PsetAttributeName = serie.PsetAttributeName,
					PsetMsgCode = serie.PsetMsgCode,
					PsetMsgLocalizedValue = serie.PsetMsgLocalizedValue,
					PsetName = serie.PsetName,
					PsetMsgCulture = serie.PsetMsgCulture
				});
			}

			RemoveDuplicateSeriesForSameKeyAndLocalValueIfExist(keyChart, HistoricalPsetCalculatedSeries);
		}

		private void InitHistoricalPsetDynamicDisplayForChart(int keyChart, IEnumerable<HistoricalPsetDynamicDisplayDto> chartDynamicDisplayDtos) {

			IEnumerable<HistoricalPsetDynamicDisplayDto> chartHistoricalPsetDynamicDisplays = chartDynamicDisplayDtos.Where(s => s.KeyChart == keyChart);

			foreach (var serie in chartHistoricalPsetDynamicDisplays) {

				if (HistoricalPsetDynamicDisplays.FirstOrDefault(s => s.KeyChart == serie.KeyChart && s.PsetMsgCode == serie.PsetMsgCode) != null) {
					continue;
				}

				HistoricalPsetDynamicDisplays.Add(new HistoricalPsetDynamicDisplay() {
					Application = serie.Application,
					KeyChart = serie.KeyChart,
					DynamicDisplayMainText = serie.DynamicDisplayMainText,
					DynamicDisplayHeaderText = serie.DynamicDisplayHeaderText,
					DynamicDisplayFooterText = serie.DynamicDisplayFooterText,
					PsetAttributeName = serie.PsetAttributeName,
					PsetMsgCode = serie.PsetMsgCode,
					PsetMsgLocalizedValue = serie.PsetMsgLocalizedValue,
					PsetName = serie.PsetName,
					PsetMsgCulture = serie.PsetMsgCulture
				});
			}
		}

		private void InitHistoricalPsetCustomGridCellsForChart(int keyChart, IEnumerable<HistoricalPsetCustomGridDto> customGridDtos) {

			IEnumerable<HistoricalPsetCustomGridDto> chartHistoricalPsetCustomGridCells = customGridDtos.Where(s => s.KeyChart == keyChart);
			foreach (var cell in chartHistoricalPsetCustomGridCells)
			{
				if (
					HistoricalPsetCustomGrids.FirstOrDefault(
						s => s.KeyChartCustomGridCell == cell.KeyChartCustomGridCell && s.PsetMsgCode == cell.PsetMsgCode) != null)
				{
					continue;
				}

				HistoricalPsetCustomGrids.Add(new HistoricalPsetCustomGridCell()
				{
					Application = cell.Application,
					KeyChart = cell.KeyChart,
					KeyChartCustomGridCell = cell.KeyChartCustomGridCell,
					Tag = cell.Tag,
					PsetAttributeName = cell.PsetAttributeName,
					PsetMsgCode = cell.PsetMsgCode,
					PsetMsgLocalizedValue = cell.PsetMsgLocalizedValue,
					PsetName = cell.PsetName,
					PsetMsgCulture = cell.PsetMsgCulture
				});
			}

			RemoveDuplicateSeriesForSameKeyAndLocalValueIfExist(keyChart, HistoricalPsetCustomGrids);
		}


		/// <summary>
		/// Its possible that different pset seires with the same Localized value but DIFFERNET msg_code exist on the chart
		/// This can create duplicate result for the same data serie key
		/// In this case, we remove the duplicates by choosing another serie on the chart,
		/// taking its culture and removing all the series for the same key which are not in that culture
		/// </summary>
		/// <param name="keyChart">The key chart.</param>
		private void RemoveDuplicateSeriesForSameKeyAndLocalValueIfExist<TData>(int keyChart,IList<TData> collectionToFix)  where TData:IFixableDataModel{
			
			//group all data by key and Local Value 
			var groupedByKeyAndMsgValue = from s in collectionToFix
										  group s by new { Key = s.EntityKey, s.PsetMsgLocalizedValue }
											  into g
											  where g.Count() > 1
											  select new { GroupKey = g.Key, Data = g.ToList() };

			if (!groupedByKeyAndMsgValue.Any()) {
				return;
			}

			foreach (var group in groupedByKeyAndMsgValue) {
				TData existingSerieOnChart =
					collectionToFix.FirstOrDefault(s => s.KeyChart == keyChart &&
											      groupedByKeyAndMsgValue.All(g => g.GroupKey.Key != s.EntityKey));

				if (existingSerieOnChart != null) {
					List<TData> otherCulturesSeries =
						group.Data.Where(s => s.PsetMsgCulture != existingSerieOnChart.PsetMsgCulture).ToList();
					
					foreach (TData serie in otherCulturesSeries) {
						collectionToFix.Remove(serie);
					}
				}
			}
		}

	}
}
	

﻿using System;
using System.Windows;

namespace Vizelia.FOL.Utilities.HistoricalPsetLocalIdsMigrationTool {

	/// <summary>
	/// Responsible for updating progress
	/// </summary>
	public class ProgressUpdater {
		private static ProgressUpdater m_Instance;

		private ProgressUpdater() { }

		public static ProgressUpdater Instance {
			get {
				if (m_Instance == null) {
					m_Instance = new ProgressUpdater();
				}
				return m_Instance;
			}
		}


		public event EventHandler<int> ReportProgress;

		protected void OnReportProgress(int e)
		{
			EventHandler<int> handler = ReportProgress;
			if (handler != null)
			{
				Application.Current.Dispatcher.Invoke(handler, this, e);
			}
			
		}

		/// <summary>
		/// Raises the report progress event.
		/// </summary>
		/// <param name="processedItemsCount">The processed items count.</param>
		public void RaiseReportProgress(int processedItemsCount)
		{
			OnReportProgress(processedItemsCount);
		}
	}
}

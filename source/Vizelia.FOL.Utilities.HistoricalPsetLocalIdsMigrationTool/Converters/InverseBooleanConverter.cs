﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Vizelia.FOL.Utilities.HistoricalPsetLocalIdsMigrationTool.Converters {
	public class InverseBooleanConverter : IValueConverter{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			bool? boolValue =  value as bool?;
			if (boolValue.HasValue)
			{
				return !boolValue;
			}

			return null;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}

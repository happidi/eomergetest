﻿namespace Vizelia.FOL.Utilities.HistoricalPsetLocalIdsMigrationTool.Commands
{
	public class UpdateChartDynamicDisplayCommand : BaseCommand
	{
		public string NewDynamicDisplayMainText { get; set; }

		public string NewDynamicDisplayFooterText { get; set; }

		public string NewDynamicDisplayHeaderText { get; set; }


		public string OldDynamicDisplayMainText { get; set; }

		public string OldDynamicDisplayFooterText { get; set; }

		public string OldDynamicDisplayHeaderText { get; set; }

		public override string TargetTableName {
			get { return "Chart"; }
		}

		public override string ToString()
		{
			return base.ToString() + string.Format("OldDynamicDisplayMainText:{0}. NewDynamicDisplayMainText:{1}." +
			                                       " OldDynamicDisplayHeaderText:{2}. NewDynamicDisplayHeaderText:{3}." +
												   " OldDynamicDisplayFooterText:{4}. NewDynamicDisplayFooterText:{5}",
													OldDynamicDisplayMainText,NewDynamicDisplayMainText,
													OldDynamicDisplayHeaderText,NewDynamicDisplayHeaderText,
													OldDynamicDisplayFooterText, NewDynamicDisplayFooterText);
		}
	}
}
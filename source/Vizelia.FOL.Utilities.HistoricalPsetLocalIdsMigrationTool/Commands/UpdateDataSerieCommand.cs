﻿namespace Vizelia.FOL.Utilities.HistoricalPsetLocalIdsMigrationTool.Commands
{
	public class UpdateDataSerieCommand : BaseCommand
	{
		public int KeyDataSerie { get; set; }

		public string NewLocalId { get; set; }

		public string OldLocalId { get; set; }

		public override string TargetTableName {
			get { return "DataSerie"; }
		}

		public override string ToString()
		{
			return base.ToString() +
				   string.Format("KeyDataSerie:{0}.OldLocalId: {1}. NewLocalId: {2}", KeyDataSerie,OldLocalId, NewLocalId);
		}
	}
}
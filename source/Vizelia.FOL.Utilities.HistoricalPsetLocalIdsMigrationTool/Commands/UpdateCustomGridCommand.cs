﻿namespace Vizelia.FOL.Utilities.HistoricalPsetLocalIdsMigrationTool.Commands
{
	public class UpdateCustomGridCellCommand : BaseCommand
	{
		public int KeyChartCustomGridCell { get; set; }

		public string NewTag { get; set; }

		public string OldTag { get; set; }

		public override string TargetTableName {
			get { return "ChartCustomGridCell"; }
		}

		public override string ToString()
		{
			return base.ToString() +
			       string.Format("KeyChartCustomGridCell:{0}. OldTag:{1}. NewTag:{2}. ", KeyChartCustomGridCell, OldTag, NewTag);
		}
	}
}
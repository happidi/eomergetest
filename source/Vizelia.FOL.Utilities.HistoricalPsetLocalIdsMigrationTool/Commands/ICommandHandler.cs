﻿using HistoricalPsetLocalIdsMigrationTool.Commands;

namespace Vizelia.FOL.Utilities.HistoricalPsetLocalIdsMigrationTool.Commands {

	public interface ICommandHandler {

		ICommandResult Execute(ICommand command);
	}


	public interface ICommandHandler<TCommand> :ICommandHandler where TCommand: ICommand
	{

		ICommandResult Execute(TCommand command);
	}
}

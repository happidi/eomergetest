﻿using HistoricalPsetLocalIdsMigrationTool.Commands;

namespace Vizelia.FOL.Utilities.HistoricalPsetLocalIdsMigrationTool.Commands
{
	public abstract class BaseCommand : ICommand
	{
		public string Application { get; set; }

		public int KeyChart { get; set; }

		public abstract string TargetTableName { get;  }

		public override string ToString()
		{
			return string.Format("Application:{0}. KeyChart:{1}. ", Application, KeyChart);
		}
	}
}
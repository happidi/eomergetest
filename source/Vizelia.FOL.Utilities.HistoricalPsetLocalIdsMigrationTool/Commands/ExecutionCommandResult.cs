﻿using HistoricalPsetLocalIdsMigrationTool.Commands;

namespace Vizelia.FOL.Utilities.HistoricalPsetLocalIdsMigrationTool.Commands
{
	public class ExecutionCommandResult : ICommandResult
	{
		public bool IsFailure { get; set; }

		public string ErrorMessage { get; set; }
	}
}
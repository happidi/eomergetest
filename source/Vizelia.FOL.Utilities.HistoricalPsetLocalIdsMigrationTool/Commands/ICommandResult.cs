namespace Vizelia.FOL.Utilities.HistoricalPsetLocalIdsMigrationTool.Commands
{
	public interface ICommandResult
	{
		bool IsFailure { get; set; }

		string ErrorMessage { get; set; }
	}
}
﻿namespace Vizelia.FOL.Utilities.HistoricalPsetLocalIdsMigrationTool.Commands {
	public interface ICommand
	{
		string Application { get; set; }

		int KeyChart { get; set; }

		string TargetTableName { get; }
	}
}

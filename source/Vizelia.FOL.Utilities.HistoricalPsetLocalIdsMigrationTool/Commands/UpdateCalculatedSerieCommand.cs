﻿using Vizelia.FOL.Utilities.HistoricalPsetLocalIdsMigrationTool.Commands;

namespace HistoricalPsetLocalIdsMigrationTool.Commands
{
	public class UpdateCalculatedSerieCommand : BaseCommand
	{
		public int KeyCalculatedSerie { get; set; }

		public string NewFormula { get; set; }

		public string OldFormula { get; set; }

		public override string TargetTableName {
			get { return "CalculatedSerie"; }
		}

		public override string ToString()
		{
			return base.ToString() +
			       string.Format("KeyCalculatedSerie:{0}.OldFormula: {1}. NewFormula:{2}", KeyCalculatedSerie, OldFormula,
				       NewFormula);
		}
	}
}
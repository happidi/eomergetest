﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using HistoricalPsetLocalIdsMigrationTool.Annotations;

namespace HistoricalPsetLocalIdsMigrationTool.Models {
	public class HistoricalPsetSerie : IFixableDataModel, INotifyPropertyChanged{
		private bool m_IsVisible;
		private bool m_IsSelected;

		public string Application { get; set; }

		public int KeyChart { get; set; }

		public int KeyDataSerie { get; set; }

		public string LocalId { get; set; }

		public string PsetMsgCode { get; set; }

		public int EntityKey {
			get { return KeyDataSerie; }
		}
		public string PsetMsgLocalizedValue { get; set; }

		public string PsetName { get; set; }

		public string PsetAttributeName { get; set; }

		public string PsetMsgCulture { get; set; }

		public string NewLocalId
		{
			get
			{
				if (string.IsNullOrWhiteSpace(LocalId) ||
					string.IsNullOrWhiteSpace(PsetMsgLocalizedValue) ||
					string.IsNullOrWhiteSpace(PsetName) ||
					string.IsNullOrWhiteSpace(PsetAttributeName))
				{
					return null;
				}

				return LocalId.Replace(PsetMsgLocalizedValue, string.Format("{0}-{1}", PsetName, PsetAttributeName));
			}
		
		}

		public bool IsSelected
		{
			get { return m_IsSelected; }
			set {
				if (m_IsSelected != value) {
					m_IsSelected = value;
					RaisePropertyChanged("IsSelected");
				}
			}
		}

		public bool IsVisible
		{
			get { return m_IsVisible; }
			set
			{
				if (m_IsVisible != value)
				{
					m_IsVisible = value;
					RaisePropertyChanged("IsVisible");
				}
			}
		}

		private void RaisePropertyChanged(string property)
		{
			if (PropertyChanged != null)
			{
				PropertyChanged(this,
					new PropertyChangedEventArgs(property));
			}
		}

		public event PropertyChangedEventHandler PropertyChanged;

		[NotifyPropertyChangedInvocator]
		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			PropertyChangedEventHandler handler = PropertyChanged;
			if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using HistoricalPsetLocalIdsMigrationTool.Annotations;

namespace HistoricalPsetLocalIdsMigrationTool.Models {
	public class Chart : INotifyPropertyChanged {

		private bool m_IsVisible;
		
		public string Application { get; set; }

		public int KeyChart { get; set; }

		public string Title { get; set; }

		public override string ToString() {
			return string.Format("{0}-{1}", KeyChart, Title);
		}


		public bool IsVisible {
			get { return m_IsVisible; }
			set {
				if (m_IsVisible != value) {
					m_IsVisible = value;
					RaisePropertyChanged("IsVisible");
				}
			}
		}

		private void RaisePropertyChanged(string property) {
			if (PropertyChanged != null) {
				PropertyChanged(this,
					new PropertyChangedEventArgs(property));
			}
		}

		public event PropertyChangedEventHandler PropertyChanged;

		[NotifyPropertyChangedInvocator]
		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) {
			PropertyChangedEventHandler handler = PropertyChanged;
			if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}

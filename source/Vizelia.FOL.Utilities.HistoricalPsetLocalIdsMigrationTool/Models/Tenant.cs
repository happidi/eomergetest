﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using HistoricalPsetLocalIdsMigrationTool.Annotations;

namespace HistoricalPsetLocalIdsMigrationTool.Models {
	public class Tenant : INotifyPropertyChanged{
		private int m_TotalChartsToUpdate;
		public string Application { get; set; }

		public int TotalChartsToUpdate
		{
			get { return m_TotalChartsToUpdate; }
			set
			{
				if (m_TotalChartsToUpdate != value)
				{
					m_TotalChartsToUpdate = value;
					RaisePropertyChanged("TotalChartsToUpdate");
					RaisePropertyChanged("DisplayValue");
				}
			}
		}

		public string DisplayValue {
			get { return string.Format("{0}(Charts to update: {1})", Application, TotalChartsToUpdate); }
		}


		private void RaisePropertyChanged(string property)
		{
			if (PropertyChanged != null)
			{
				PropertyChanged(this,
					new PropertyChangedEventArgs(property));
			}
		}

		public event PropertyChangedEventHandler PropertyChanged;

		[NotifyPropertyChangedInvocator]
		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			PropertyChangedEventHandler handler = PropertyChanged;
			if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}

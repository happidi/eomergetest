﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HistoricalPsetLocalIdsMigrationTool.Models {
	public interface IFixableDataModel
	{
		string Application { get; set; }

		string PsetMsgCode { get; set; }

		int EntityKey { get; }

		int KeyChart { get; set; }

		string PsetMsgLocalizedValue { get; set; }

		string PsetName { get; set; }

		string PsetAttributeName { get; set; }

		string PsetMsgCulture { get; set; }

		bool IsSelected { get; set; }
		
		bool IsVisible { get; set; }
	}
}

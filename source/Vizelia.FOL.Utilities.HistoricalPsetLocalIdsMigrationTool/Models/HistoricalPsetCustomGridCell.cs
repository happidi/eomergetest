﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using HistoricalPsetLocalIdsMigrationTool.Annotations;

namespace HistoricalPsetLocalIdsMigrationTool.Models {
	public class HistoricalPsetCustomGridCell : IFixableDataModel, INotifyPropertyChanged {
		private bool m_IsVisible;
		private bool m_IsSelected;
		public string Application { get; set; }

		public int KeyChartCustomGridCell { get; set; }

		public int KeyChart { get; set; }

		public string Tag { get; set; }


		public string NewTag {
			get {
				if (string.IsNullOrWhiteSpace(Tag) ||
					string.IsNullOrWhiteSpace(PsetMsgLocalizedValue) ||
					string.IsNullOrWhiteSpace(PsetName) ||
					string.IsNullOrWhiteSpace(PsetAttributeName)) {
					return null;
				}

				return Tag.Replace(PsetMsgLocalizedValue, string.Format("{0}-{1}", PsetName, PsetAttributeName));
			}
		}

		public string PsetMsgCode { get; set; }

		public int EntityKey {
			get { return KeyChartCustomGridCell; }
		}
		public string PsetMsgLocalizedValue { get; set; }

		public string PsetName { get; set; }

		public string PsetAttributeName { get; set; }


		public string PsetMsgCulture { get; set; }



		public bool IsSelected {
			get { return m_IsSelected; }
			set {
				if (m_IsSelected != value) {
					m_IsSelected = value;
					RaisePropertyChanged("IsSelected");
				}
			}
		}

		public bool IsVisible {
			get { return m_IsVisible; }
			set {
				if (m_IsVisible != value) {
					m_IsVisible = value;
					RaisePropertyChanged("IsVisible");
				}
			}
		}

		private void RaisePropertyChanged(string property) {
			if (PropertyChanged != null) {
				PropertyChanged(this,
					new PropertyChangedEventArgs(property));
			}
		}

		public event PropertyChangedEventHandler PropertyChanged;

		[NotifyPropertyChangedInvocator]
		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) {
			PropertyChangedEventHandler handler = PropertyChanged;
			if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}

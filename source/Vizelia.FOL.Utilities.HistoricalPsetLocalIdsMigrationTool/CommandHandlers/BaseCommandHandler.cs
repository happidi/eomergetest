﻿using System;
using System.Transactions;
using HistoricalPsetLocalIdsMigrationTool;
using HistoricalPsetLocalIdsMigrationTool.Commands;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Vizelia.FOL.Providers;
using Vizelia.FOL.Utilities.HistoricalPsetLocalIdsMigrationTool.Commands;

namespace Vizelia.FOL.Utilities.HistoricalPsetLocalIdsMigrationTool.CommandHandlers
{
	public abstract class BaseCommandHandler<TCommand> : ICommandHandler<TCommand> where TCommand : class, ICommand
	{
		public abstract ICommandResult Execute(TCommand command);

		public ICommandResult Execute(ICommand command)
		{
			ICommandResult result = null;
			string keyChartString = command.KeyChart.ToString();
			try
			{
				ContextHelper.ApplicationName = command.Application;
				result = Execute(command as TCommand);

				if (!result.IsFailure)
				{
					string sucessMsg = string.Format("Successfully updated table :{0}. With Entity : {1}.", command.TargetTableName, command);
					TracingService.Write(TraceEntrySeverity.Information, sucessMsg, Constants.const_trace_category_name, keyChartString);
				}
				else
				{
					TracingService.Write(TraceEntrySeverity.Error,
						"The following error has occurred during execution: " + result.ErrorMessage, Constants.const_trace_category_name,
						keyChartString);
				}
			}
			catch (Exception exception)
			{
				if (result == null)
				{
					result = new ExecutionCommandResult();
				}
				result.IsFailure = true;
				string errorMessage =
					string.Format("An exception occurred while executing the update of Table : {0}. With Data {1}. Exception: {2}.",
								  command.TargetTableName, command, exception);

				result.ErrorMessage = errorMessage;

				using (new TransactionScope(TransactionScopeOption.Suppress))
				{
					TracingService.Write(TraceEntrySeverity.Error, errorMessage, Constants.const_trace_category_name, keyChartString);
				}
			}

			return result;
		}
	}
}
﻿using HistoricalPsetLocalIdsMigrationTool.Commands;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.DataLayer.BrokerDB;
using Vizelia.FOL.Utilities.HistoricalPsetLocalIdsMigrationTool.Commands;

namespace Vizelia.FOL.Utilities.HistoricalPsetLocalIdsMigrationTool.CommandHandlers
{
	public class UpdateCalculatedSerieCommandHandler : BaseCommandHandler<UpdateCalculatedSerieCommand>
	{
		readonly CalculatedSerieBrokerDB m_CalculatedSerieBrokerDb = new CalculatedSerieBrokerDB();
		public override ICommandResult Execute(UpdateCalculatedSerieCommand command)
		{ 
			CalculatedSerie serie = m_CalculatedSerieBrokerDb.GetItem(command.KeyCalculatedSerie.ToString());

			serie.Formula = command.NewFormula;
			m_CalculatedSerieBrokerDb.Update(serie);

			return new ExecutionCommandResult();
		}
	}
}
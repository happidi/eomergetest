﻿using System;
using System.Collections.Generic;
using System.Reflection;
using HistoricalPsetLocalIdsMigrationTool.Commands;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Vizelia.FOL.DataLayer.BrokerDB;
using Vizelia.FOL.Providers;
using Vizelia.FOL.Utilities.HistoricalPsetLocalIdsMigrationTool.Commands;

namespace Vizelia.FOL.Utilities.HistoricalPsetLocalIdsMigrationTool.CommandHandlers
{
	public class UpdateChartDynamicDisplayCommandHandler: BaseCommandHandler<UpdateChartDynamicDisplayCommand>
	{
		readonly ChartBrokerDB m_ChartDbBroker = new ChartBrokerDB();

		public UpdateChartDynamicDisplayCommandHandler()
		{
			AddFakeKeyToCacheToEnableChartUpdatePermissions();
		}

		private static void AddFakeKeyToCacheToEnableChartUpdatePermissions()
		{
			Type typeOfChart = typeof (Chart);
			string cacheKey = string.Format("FilterAttributesForType:{0}", typeOfChart.Name);

			PropertyInfo propertyInfo = typeOfChart.GetProperty("KeyChart");
			var filterTypeAttribute = new FilterTypeAttribute(typeOfChart);
			var attributes = new List<Tuple<PropertyInfo, FilterTypeAttribute>>()
			{
				new Tuple<PropertyInfo, FilterTypeAttribute>(propertyInfo, filterTypeAttribute)
			};

			var item = CacheService.GetData(cacheKey);
			if (item == null)
			{
				CacheService.Add(cacheKey, attributes);
			}
		}


		public override ICommandResult Execute(UpdateChartDynamicDisplayCommand command)
		{
			AddFakeKeyToCacheToEnableChartUpdatePermissions();
			Chart chart = m_ChartDbBroker.GetItem(command.KeyChart.ToString());
			
			chart.DynamicDisplayHeaderText = command.NewDynamicDisplayHeaderText;
			chart.DynamicDisplayMainText = command.NewDynamicDisplayMainText;
			chart.DynamicDisplayFooterText = command.NewDynamicDisplayFooterText;
			m_ChartDbBroker.Update(chart);

			return new ExecutionCommandResult();
		}
	}
}
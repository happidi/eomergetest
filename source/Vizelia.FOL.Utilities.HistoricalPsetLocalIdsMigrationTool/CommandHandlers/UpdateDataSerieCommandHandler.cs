﻿using HistoricalPsetLocalIdsMigrationTool.Commands;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.DataLayer.BrokerDB;
using Vizelia.FOL.Utilities.HistoricalPsetLocalIdsMigrationTool.Commands;

namespace Vizelia.FOL.Utilities.HistoricalPsetLocalIdsMigrationTool.CommandHandlers
{
	public class UpdateDataSerieCommandHandler: BaseCommandHandler<UpdateDataSerieCommand> 
	{
		readonly DataSerieBrokerDB m_DataSerieBroker = new DataSerieBrokerDB();

		public override ICommandResult Execute(UpdateDataSerieCommand command)
		{
			DataSerie serie = m_DataSerieBroker.GetItem(command.KeyDataSerie.ToString());

			serie.LocalId = command.NewLocalId;
			m_DataSerieBroker.Update(serie);

			return new ExecutionCommandResult();
		}
	}
}
﻿using HistoricalPsetLocalIdsMigrationTool.Commands;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.DataLayer.BrokerDB;
using Vizelia.FOL.Utilities.HistoricalPsetLocalIdsMigrationTool.Commands;

namespace Vizelia.FOL.Utilities.HistoricalPsetLocalIdsMigrationTool.CommandHandlers
{
	public class UpdateCustomGridCellCommandHandler: BaseCommandHandler<UpdateCustomGridCellCommand>
	{
		private readonly ChartCustomGridCellBrokerDB m_ChartCustomGridCellBrokerDb = new ChartCustomGridCellBrokerDB();

		public override ICommandResult Execute(UpdateCustomGridCellCommand command)
		{
			ChartCustomGridCell customGridCell = m_ChartCustomGridCellBrokerDb.GetItem(command.KeyChartCustomGridCell.ToString());

			customGridCell.Tag = command.NewTag;
			m_ChartCustomGridCellBrokerDb.Update(customGridCell);

			return new ExecutionCommandResult();
		}
	}
}
﻿namespace Vizelia.FOL.Utilities.HistoricalPsetLocalIdsMigrationTool.DataObjects
{
	public class HistoricalPsetCustomGridDto
	{

		public string Application { get; set; }

		public int KeyChartCustomGridCell { get; set; }

		public int KeyChart { get; set; }

		public string Tag { get; set; }

		public string PsetMsgCode { get; set; }

		public string PsetMsgLocalizedValue { get; set; }

		public string PsetName { get; set; }

		public string PsetAttributeName { get; set; }

		public string PsetMsgCulture { get; set; }
	}
}
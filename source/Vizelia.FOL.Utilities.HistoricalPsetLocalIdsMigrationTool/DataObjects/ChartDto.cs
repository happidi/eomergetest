namespace Vizelia.FOL.Utilities.HistoricalPsetLocalIdsMigrationTool.DataObjects
{
	public class ChartDto
	{
		public string Application { get; set; }

		public int KeyChart { get; set; }
		
		public string Title { get; set; }

	}
}
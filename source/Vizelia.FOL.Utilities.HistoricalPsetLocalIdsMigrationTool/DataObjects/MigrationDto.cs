﻿using System.Collections.Generic;

namespace Vizelia.FOL.Utilities.HistoricalPsetLocalIdsMigrationTool.DataObjects
{
	public class MigrationDto
	{

		public MigrationDto()
		{
			TenantsDtos = new List<TenantDto>();
			ChartsDtos = new List<ChartDto>();
			HistoricalPsetSerieDtos = new List<HistoricalPsetSerieDto>();
			HistoricalPsetCalculatedSeriesDtos = new List<HistoricalPsetCalculatedSerieDto>();
			HistoricalPsetDynamicDisplayDtos = new List<HistoricalPsetDynamicDisplayDto>();
			HistoricalPsetCustomGridDtos = new List<HistoricalPsetCustomGridDto>();
		}

		public IEnumerable<HistoricalPsetSerieDto> HistoricalPsetSerieDtos { get; set; }


		public IEnumerable<HistoricalPsetCalculatedSerieDto> HistoricalPsetCalculatedSeriesDtos { get; set; }


		public IEnumerable<HistoricalPsetDynamicDisplayDto> HistoricalPsetDynamicDisplayDtos { get; set; }


		public IEnumerable<HistoricalPsetCustomGridDto> HistoricalPsetCustomGridDtos { get; set; }


		public IEnumerable<ChartDto> ChartsDtos { get; set; }

		public IEnumerable<TenantDto> TenantsDtos { get; set; }
	}
}
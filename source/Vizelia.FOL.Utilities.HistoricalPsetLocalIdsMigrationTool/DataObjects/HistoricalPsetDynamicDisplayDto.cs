﻿namespace Vizelia.FOL.Utilities.HistoricalPsetLocalIdsMigrationTool.DataObjects
{
	public class HistoricalPsetDynamicDisplayDto
	{
		public string Application { get; set; }

		public int KeyChart { get; set; }

		public string DynamicDisplayMainText { get; set; }

		public string DynamicDisplayFooterText { get; set; }

		public string DynamicDisplayHeaderText { get; set; }

		public string PsetMsgCode { get; set; }

		public string PsetMsgLocalizedValue { get; set; }

		public string PsetName { get; set; }

		public string PsetAttributeName { get; set; }

		public string PsetMsgCulture { get; set; }
	}
}
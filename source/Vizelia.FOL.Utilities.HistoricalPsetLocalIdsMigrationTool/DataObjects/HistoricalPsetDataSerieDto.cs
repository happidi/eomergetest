﻿namespace Vizelia.FOL.Utilities.HistoricalPsetLocalIdsMigrationTool.DataObjects {
	public class HistoricalPsetSerieDto {

		public string Application { get; set; }

		public int KeyChart { get; set; }

		public int KeyDataSerie { get; set; }

		public string LocalId { get; set; }

		public string PsetMsgCode { get; set; }

		public string PsetMsgLocalizedValue { get; set; }

		public string PsetName { get; set; }

		public string PsetAttributeName { get; set; }

		public string PsetMsgCulture { get; set; }
	}
}

﻿using System.Windows;
using System.Windows.Controls;
using HistoricalPsetLocalIdsMigrationTool.Commands;
using HistoricalPsetLocalIdsMigrationTool.Models;
using Vizelia.FOL.Utilities.HistoricalPsetLocalIdsMigrationTool.ViewModels;


namespace HistoricalPsetLocalIdsMigrationTool {
	/// <summary>
	/// Interaction logic for MainWindowView.xaml
	/// </summary>
	public partial class MainWindowView : Window {
		readonly IMigrationViewModel m_ViewModel = new MigrationViewModel();
		
		private const string const_fix_all_waring_msg = "The following will fix all of the invalid data in all of the charts in all the tenants. Are you sure you want to proceed?";
		private const string const_fix_all_title = "Fix All Data Warning";
		private const string const_no_data_selected_msg = "No data was selected.";
		private const string const_execution_result_title = "Fix Results Notification.";

		public MainWindowView() {
			InitializeComponent();
			DataContext = m_ViewModel;
		}
		

		private void OnChartsDataSelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			var listBox = sender as ListBox;
			if (listBox == null)
			{
				return;
			}

			var selectedChart = listBox.SelectedItem as Chart;

			if (selectedChart != null)
			{
				m_ViewModel.SetSelectedChartData(selectedChart);
			}
		}

		private void OnTenantsSelectionChanged(object sender, SelectionChangedEventArgs e) {
			var tenantsCombo = sender as ComboBox;
			if (tenantsCombo == null)
			{
				return;
			}


			Tenant selectedTenant = (Tenant)tenantsCombo.SelectedItem;

			if (selectedTenant != null)
			{
				m_ViewModel.DisplayChartsForTenant(selectedTenant.Application);
			}

		}

		private void OnApplyChangesClicked(object sender, RoutedEventArgs e) {
			var selectChart = ChartsList.SelectedItem as Chart;
			if (selectChart == null) {
				MessageBox.Show(const_no_data_selected_msg,
							const_execution_result_title,
							MessageBoxButton.OK,
							MessageBoxImage.Warning);

				return;
			}
		
			m_ViewModel.FixDataForChart(selectChart.KeyChart);
		}

		private void OnFixAllClicked(object sender, RoutedEventArgs e)
		{
			MessageBoxResult confirmationResult = MessageBox.Show(const_fix_all_waring_msg,
				const_fix_all_title,
				MessageBoxButton.OKCancel,
				MessageBoxImage.Hand);

			if (confirmationResult == MessageBoxResult.Cancel)
			{
				return;
			}

			m_ViewModel.FixAllData();
		}
	}
}

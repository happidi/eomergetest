﻿using System.Collections.Generic;
using HistoricalPsetLocalIdsMigrationTool.Commands;
using Vizelia.FOL.Utilities.HistoricalPsetLocalIdsMigrationTool.Commands;
using Vizelia.FOL.Utilities.HistoricalPsetLocalIdsMigrationTool.DataObjects;

namespace Vizelia.FOL.Utilities.HistoricalPsetLocalIdsMigrationTool.BusinessLayer {
	public interface IMigrationBusinessComponent {
		MigrationDto GetAllDataForMigration();

		ICommandResult FixData(IEnumerable<ICommand> commands);
	}
}

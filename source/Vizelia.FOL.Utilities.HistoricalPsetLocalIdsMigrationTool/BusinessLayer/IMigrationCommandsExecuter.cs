﻿using System.Collections.Generic;
using HistoricalPsetLocalIdsMigrationTool.Commands;
using Vizelia.FOL.Utilities.HistoricalPsetLocalIdsMigrationTool.Commands;

namespace Vizelia.FOL.Utilities.HistoricalPsetLocalIdsMigrationTool.BusinessLayer {
	public interface IMigrationCommandsExecuter {
		ICommandResult ExecuteCommands(IList<ICommand> commands);
	}
}

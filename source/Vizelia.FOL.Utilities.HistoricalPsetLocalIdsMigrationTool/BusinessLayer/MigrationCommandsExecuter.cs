﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Transactions;
using HistoricalPsetLocalIdsMigrationTool;
using HistoricalPsetLocalIdsMigrationTool.Commands;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Providers;
using Vizelia.FOL.Utilities.HistoricalPsetLocalIdsMigrationTool.CommandHandlers;
using Vizelia.FOL.Utilities.HistoricalPsetLocalIdsMigrationTool.Commands;

namespace Vizelia.FOL.Utilities.HistoricalPsetLocalIdsMigrationTool.BusinessLayer
{
	public class MigrationCommandsExecuter : IMigrationCommandsExecuter
	{
		private readonly Dictionary<Type, ICommandHandler> m_CommandsHandlersMap = new Dictionary<Type, ICommandHandler>();
		private readonly TimeSpan m_MaxTransationTimeout = TimeSpan.FromMinutes(10);
		private readonly ProgressUpdater m_ProgressUpdater = ProgressUpdater.Instance;

		public MigrationCommandsExecuter()
		{
			CreateCommandsHandlersMapping();
		}

		private void CreateCommandsHandlersMapping()
		{
			m_CommandsHandlersMap.Add(typeof(UpdateDataSerieCommand),new UpdateDataSerieCommandHandler());
			m_CommandsHandlersMap.Add(typeof(UpdateCalculatedSerieCommand), new UpdateCalculatedSerieCommandHandler());
			m_CommandsHandlersMap.Add(typeof(UpdateChartDynamicDisplayCommand), new UpdateChartDynamicDisplayCommandHandler());
			m_CommandsHandlersMap.Add(typeof(UpdateCustomGridCellCommand), new UpdateCustomGridCellCommandHandler());
		}

		private ICommandHandler GetCommandHandler(Type commandType)
		{
			if (!m_CommandsHandlersMap.ContainsKey(commandType))
			{
				throw new InvalidOperationException(
					string.Format("Command of type: {0}, does not have a CommandHandler implementation registered", commandType.Name));
			}

			ICommandHandler commandHandler = m_CommandsHandlersMap[commandType];
			return commandHandler;

		}

		public ICommandResult ExecuteCommands(IList<ICommand> commands)
		{
			Stopwatch stopwatch = Stopwatch.StartNew();
			ICommandResult result = new ExecutionCommandResult();
			using (var ts = new TransactionScope(TransactionScopeOption.Required, m_MaxTransationTimeout))
			{
				try
				{
					for (int i = 0; i < commands.Count(); i++)
					{
						var command = commands[i];
						ICommandHandler commandHandler = GetCommandHandler(command.GetType());
						result = commandHandler.Execute(command);

						if (result.IsFailure)
						{
							break;
						}

						m_ProgressUpdater.RaiseReportProgress(i+1);
					}

					if (!result.IsFailure)
					{
						ts.Complete();
					}

					stopwatch.Stop();
				}
				catch (Exception exception)
				{
					result.IsFailure = true;
					string errorMsg = "The following exception has occurred during commands execution: " + exception;
					result.ErrorMessage = errorMsg;

					using (new TransactionScope(TransactionScopeOption.Suppress)) {
						TracingService.Write(TraceEntrySeverity.Error, errorMsg, Constants.const_trace_category_name, "");
					}
				}
			}
			
			return result;
		}
	}
}
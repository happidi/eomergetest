﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using HistoricalPsetLocalIdsMigrationTool;
using HistoricalPsetLocalIdsMigrationTool.Commands;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.DataLayer;
using Vizelia.FOL.Providers;
using Vizelia.FOL.Utilities.HistoricalPsetLocalIdsMigrationTool.Commands;
using Vizelia.FOL.Utilities.HistoricalPsetLocalIdsMigrationTool.DataObjects;

namespace Vizelia.FOL.Utilities.HistoricalPsetLocalIdsMigrationTool.BusinessLayer
{
	public class MigrationBusinessComponent : IMigrationBusinessComponent
	{
		private readonly IMigrationCommandsExecuter m_MigrationCommandsExecuter = new MigrationCommandsExecuter();

		private const string const_get_all_data_proc_name = "Admin_HistoricalPsetsLocalIdsMigrationToolGetAllDataForMigration";
		private const string const_back_up_data_proc_name = "Admin_HistoricalPsetsLocalIdsMigrationToolBackupTables";
		private const int const_Command_timeout_seconds = 6000;
		private readonly DataAccess m_DataAccess = new DataAccess();

		public MigrationDto GetAllDataForMigration() {

			var migrationData = new MigrationDto();
			try {

				DbCommand command = m_DataAccess.GetStoredProcCommand(const_get_all_data_proc_name);
				command.CommandTimeout = const_Command_timeout_seconds;
				DataSet allData = m_DataAccess.ExecuteDataSet(command);

				if (allData.Tables[0].Rows.Count > 0) {
					migrationData.HistoricalPsetSerieDtos = GetDataSeries(allData.Tables[0]);
				}

				if (allData.Tables[1].Rows.Count > 0) {
					migrationData.HistoricalPsetCalculatedSeriesDtos = GetCalculatedSeriesDtos(allData.Tables[1]);
				}

				if (allData.Tables[2].Rows.Count > 0) {
					migrationData.HistoricalPsetDynamicDisplayDtos = GetDynamicDisplayDtos(allData.Tables[2]);
				}

				if (allData.Tables[3].Rows.Count > 0) {
					migrationData.HistoricalPsetCustomGridDtos = GetCustomGridDtos(allData.Tables[3]);
				}

				if (allData.Tables[4].Rows.Count > 0) {
					migrationData.ChartsDtos = GetChartDtos(allData.Tables[4]);
				}

				if (allData.Tables[5].Rows.Count > 0) {
					migrationData.TenantsDtos = GetTenantDtos(allData.Tables[5]);
				}

			}
			catch (Exception exception)
			{
				string errorMsg = "The following exception has occurred during fetching the data: " + exception;
				TracingService.Write(TraceEntrySeverity.Error, errorMsg, Constants.const_trace_category_name, "");
				throw exception;
			}
	

			return migrationData;
		}

		public ICommandResult FixData(IEnumerable<ICommand> commands)
		{
			ICommandResult result = new ExecutionCommandResult();
			try
			{
				TracingService.Write(TraceEntrySeverity.Information, "Execution Started", Constants.const_trace_category_name, "");

				BackupOriginalData();
				result = m_MigrationCommandsExecuter.ExecuteCommands(commands.ToList());

				TracingService.Write(TraceEntrySeverity.Information, "Execution Completed", Constants.const_trace_category_name, "");

			}
			catch (Exception exception)
			{
				result.IsFailure = true;
				string errorMsg = "The following exception has occurred during execution: " + exception;
				TracingService.Write(TraceEntrySeverity.Error, errorMsg, Constants.const_trace_category_name, "");
			}

			return result;
		}

		private void BackupOriginalData()
		{
			TracingService.Write(TraceEntrySeverity.Information, "Backup of original tables started", Constants.const_trace_category_name, "");
			
			m_DataAccess.ExecuteNonQuery(const_back_up_data_proc_name);

			TracingService.Write(TraceEntrySeverity.Information, "Backup of original tables completed", Constants.const_trace_category_name, "");
		}

		private IEnumerable<ChartDto> GetChartDtos(DataTable dataTable)
		{
			var dtos = new List<ChartDto>();

			foreach (DataRow dataRow in dataTable.Rows.OfType<DataRow>()) {
				dtos.Add(new ChartDto() {
					Application = dataRow.Field<string>("App"),
					KeyChart = dataRow.Field<int>("KeyChart"),
					Title = dataRow.Field<string>("Title"),
				});
			}

			return dtos;
		}

		private IEnumerable<TenantDto> GetTenantDtos(DataTable dataTable) {
			var dtos = new List<TenantDto>();

			foreach (DataRow dataRow in dataTable.Rows.OfType<DataRow>()) {
				dtos.Add(new TenantDto(){
					Application = dataRow.Field<string>("App")}); 
			}
			return dtos;
		}

		private IEnumerable<HistoricalPsetCustomGridDto> GetCustomGridDtos(DataTable dataTable)
		{
			var dtos = new List<HistoricalPsetCustomGridDto>();

			foreach (DataRow dataRow in dataTable.Rows.OfType<DataRow>()) {

				dtos.Add(new HistoricalPsetCustomGridDto() {
					Application = dataRow.Field<string>("App"),
					KeyChartCustomGridCell = dataRow.Field<int>("KeyChartCustomGridCell"),
					KeyChart = dataRow.Field<int>("KeyChart"),
					Tag = dataRow.Field<string>("Tag"),
					PsetAttributeName = dataRow.Field<string>("PsetAttributeName"),
					PsetMsgCode = dataRow.Field<string>("PsetMsgCode"),
					PsetMsgLocalizedValue = dataRow.Field<string>("PsetMsgLocalizedValue"),
					PsetName = dataRow.Field<string>("PsetName"),
					PsetMsgCulture = dataRow.Field<string>("PsetMsgCulture")
				});
			}

			return dtos;
		}

		private IEnumerable<HistoricalPsetDynamicDisplayDto> GetDynamicDisplayDtos(DataTable dataTable)
		{
			var dtos = new List<HistoricalPsetDynamicDisplayDto>();

			foreach (DataRow dataRow in dataTable.Rows.OfType<DataRow>()) {
				dtos.Add(new HistoricalPsetDynamicDisplayDto() {
					Application = dataRow.Field<string>("App"),
					KeyChart = dataRow.Field<int>("KeyChart"),
					DynamicDisplayMainText = dataRow.Field<string>("DynamicDisplayMainText"),
					DynamicDisplayHeaderText = dataRow.Field<string>("DynamicDisplayHeaderText"),
					DynamicDisplayFooterText = dataRow.Field<string>("DynamicDisplayFooterText"),
					PsetAttributeName = dataRow.Field<string>("PsetAttributeName"),
					PsetMsgCode = dataRow.Field<string>("PsetMsgCode"),
					PsetMsgLocalizedValue = dataRow.Field<string>("PsetMsgLocalizedValue"),
					PsetName = dataRow.Field<string>("PsetName"),
					PsetMsgCulture = dataRow.Field<string>("PsetMsgCulture")
				});
			}

			return dtos;
		}

		private IEnumerable<HistoricalPsetCalculatedSerieDto> GetCalculatedSeriesDtos(DataTable dataTable) {
			var dtos = new List<HistoricalPsetCalculatedSerieDto>();

			foreach (DataRow dataRow in dataTable.Rows.OfType<DataRow>()) {
				dtos.Add(new HistoricalPsetCalculatedSerieDto() {
					Application = dataRow.Field<string>("App"),
					KeyChart = dataRow.Field<int>("KeyChart"),
					KeyCalculatedSerie = dataRow.Field<int>("KeyCalculatedSerie"),
					Formula = dataRow.Field<string>("Formula"),
					PsetAttributeName = dataRow.Field<string>("PsetAttributeName"),
					PsetMsgCode = dataRow.Field<string>("PsetMsgCode"),
					PsetMsgLocalizedValue = dataRow.Field<string>("PsetMsgLocalizedValue"),
					PsetName = dataRow.Field<string>("PsetName"),
					PsetMsgCulture = dataRow.Field<string>("PsetMsgCulture")
				});
			}

			return dtos;
		}

		private IEnumerable<HistoricalPsetSerieDto> GetDataSeries(DataTable dataSeriesTable) {
			var dtos = new List<HistoricalPsetSerieDto>();

			foreach (DataRow dataRow in dataSeriesTable.Rows.OfType<DataRow>()) {

				dtos.Add(new HistoricalPsetSerieDto() {
					Application = dataRow.Field<string>("App"),
					KeyChart = dataRow.Field<int>("KeyChart"),
					KeyDataSerie = dataRow.Field<int>("KeyDataSerie"),
					LocalId = dataRow.Field<string>("LocalId"),
					PsetAttributeName = dataRow.Field<string>("PsetAttributeName"),
					PsetMsgCode = dataRow.Field<string>("PsetMsgCode"),
					PsetMsgLocalizedValue = dataRow.Field<string>("PsetMsgLocalizedValue"),
					PsetName = dataRow.Field<string>("PsetName"),
					PsetMsgCulture = dataRow.Field<string>("PsetMsgCulture")
				});
			}

			return dtos;
		}

	}
}
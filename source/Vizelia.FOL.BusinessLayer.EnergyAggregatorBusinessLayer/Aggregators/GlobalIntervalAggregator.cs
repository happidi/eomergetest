using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.BusinessLayer{
	/// <summary>
	/// GlobalIntervalAggregator
	/// </summary>
	public class GlobalIntervalAggregator : BaseTimeIntervalAggregator {
		/// <summary>
		/// Gets the aggregation interval.
		/// </summary>
		/// <value>
		/// The aggregation interval.
		/// </value>
		public override AxisTimeInterval AggregationInterval {
			get { return AxisTimeInterval.Global; }
		}
	}
}
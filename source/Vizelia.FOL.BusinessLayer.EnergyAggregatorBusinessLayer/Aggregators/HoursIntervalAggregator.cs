using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.BusinessLayer{
	/// <summary>
	/// HoursIntervalAggregator
	/// </summary>
	public class HoursIntervalAggregator : BaseTimeIntervalAggregator {
		
		/// <summary>
		/// Gets the aggregation interval.
		/// </summary>
		/// <value>
		/// The aggregation interval.
		/// </value>
		public override AxisTimeInterval AggregationInterval {
			get { return AxisTimeInterval.Hours; }
		}
	}
}
﻿using System.Collections.Generic;

namespace Vizelia.FOL.BusinessLayer{
	
	/// <summary>
	///  Holds the data of the aggregation result
	/// </summary>
	public class AggregationResult{

		/// <summary>
		/// Initializes a new instance of the <see cref="AggregationResult"/> class.
		/// </summary>
		public AggregationResult(){

			Buckets = new List<Bucket>();
		}
		/// <summary>
		/// Gets or sets the buckets with the aggregated data.
		/// </summary>
		/// <value>
		/// The buckets.
		/// </value>
		public IList<Bucket> Buckets { get; set; }

	}
}
﻿using System;
using System.Collections.Generic;
using System.Text;
using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.BusinessLayer{

	/// <summary>
	///  An interface for a data aggregation component
	/// </summary>
	public interface IDataAggregator{
		
		/// <summary>
		/// Aggregates the specified meter data.
		/// </summary>
		/// <param name="meterData">The meter data.</param>
		/// <param name="op">The mathematic operation that should be applied when aggregating the data</param>
		/// <param name="timeZone">The time zone.</param>
		/// <param name="startDayOfWeek">The start day of week.</param>
		/// <returns></returns>
		AggregationResult Aggregate(List<MD> meterData, MathematicOperator op, TimeZoneInfo timeZone = null, int startDayOfWeek = 1);
	}
}
	
	

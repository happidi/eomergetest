using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.BusinessLayer{
	/// <summary>
	/// DaysIntervalAggregator
	/// </summary>
	public class DaysIntervalAggregator : BaseTimeIntervalAggregator {
		/// <summary>
		/// Gets the aggregation interval.
		/// </summary>
		/// <value>
		/// The aggregation interval.
		/// </value>
		public override AxisTimeInterval AggregationInterval {
			get { return AxisTimeInterval.Days; }
		}
	}
}
using System;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessLayer{
	/// <summary>
	/// QuartersIntervalAggregator
	/// </summary>
	public class QuartersIntervalAggregator : BaseTimeIntervalAggregator {
		/// <summary>
		/// Gets the aggregation interval.
		/// </summary>
		/// <value>
		/// The aggregation interval.
		/// </value>
		public override AxisTimeInterval AggregationInterval {
			get { return AxisTimeInterval.Quarters; }

		}

		/// <summary>
		/// Formats the date to a label.
		/// </summary>
		/// <param name="date">The date.</param>
		/// <returns></returns>
		protected override string FormatDateToLabel(DateTime date){
			string label = Helper.ConvertDateTimeToQuarterString(date);

			return label;
		}
	}
}
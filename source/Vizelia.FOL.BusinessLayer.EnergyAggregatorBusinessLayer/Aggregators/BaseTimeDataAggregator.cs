using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessLayer{
	
	/// <summary>
	///  Base class for Time interval aggregator
	/// </summary>
	public abstract class BaseTimeIntervalAggregator : ITimeIntervalAggregator {

		/// <summary>
		/// Aggregates the specified meter data.
		/// </summary>
		/// <param name="meterData">The meter data.</param>
		/// <param name="op">The mathematic operation that should be applied when aggregating the data</param>
		/// <param name="timeZone">The time zone.</param>
		/// <param name="startDayOfWeek">The start day of week.</param>
		/// <returns></returns>
		public AggregationResult Aggregate(List<MD> meterData, MathematicOperator op, TimeZoneInfo timeZone, int startDayOfWeek = 1) {

			var result = new AggregationResult();
			if (meterData == null || !meterData.Any()) {

				return result;
			}
			Dictionary<long, List<MD>> groupedData = GroupDataByTime(meterData, timeZone, startDayOfWeek);

			foreach (var period in groupedData){
				double aggregatedValue;
				switch (op) {
					case MathematicOperator.MIN:
						aggregatedValue = period.Value.Min(md => md.Value);
						break;
					case MathematicOperator.MAX:
						aggregatedValue = period.Value.Max(md => md.Value);
						break;
					case MathematicOperator.AVG:
						aggregatedValue = period.Value.Average(md => md.Value);
						break;
					default:
						aggregatedValue = period.Value.Sum(md => md.Value);
						break;

				}
				var invalidIndex = period.Value.FindIndex(md => md.Validity == 1);
				var estimatedIndex = period.Value.FindIndex(md => md.Validity == 4);

				DateTime date = EnergyAggregatorHelper.ConvertLongToDateTime(period.Key);

				var bucket = new Bucket() {
					Label = FormatDateToLabel(date),
					DateTime = EnergyAggregatorHelper.ConvertLongToDateTime(period.Key),
					Value = aggregatedValue,
					MeterDataValidity = invalidIndex >= 0 ? MeterDataValidity.Invalid : (estimatedIndex >= 0 ? MeterDataValidity.Estimated : MeterDataValidity.Valid)
				};

				result.Buckets.Add(bucket);
			}

			return result;
		}


		/// <summary>
		/// Formats the date to a label.
		/// </summary>
		/// <param name="date">The date.</param>
		/// <returns></returns>
		protected virtual string FormatDateToLabel(DateTime date){

			return date.ToString(AggregationInterval.GetFormat());
		}

		/// <summary>
		/// Groups a list of MD by time interval
		/// (the key of the dictionnary is the datetime expressed as a long for performance).
		/// </summary>
		/// <param name="data">The data.</param>
		/// <param name="timeZone">The time zone.</param>
		/// <param name="startDayOfWeek">The start day of week.</param>
		/// <returns></returns>
		public Dictionary<long, List<MD>> GroupDataByTime(List<MD> data, TimeZoneInfo timeZone = null, int startDayOfWeek = 1) {
			var dataGroupedByTime = new Dictionary<long, List<MD>>();
			object combineLock = new object();
			bool useTimeZone = timeZone != null && timeZone != TimeZoneInfo.Utc;

			var bucketNumber = Environment.ProcessorCount;
			var bucketSize = data.Count / bucketNumber + 1;
			Parallel.For(0, bucketNumber, () => new Dictionary<long, List<MD>>(),
				(currentIndex, loopState, localDictionnary) => {
					                                               for (var j = bucketSize * currentIndex; j < Math.Min(data.Count, bucketSize * (currentIndex + 1)); j++) {

						                                               var md = data[j];
						                                               if (md != null) {
							                                               var acquisitionDateTime = useTimeZone ? md.AcquisitionDateTime.ConvertTimeFromUtc(timeZone) : md.AcquisitionDateTime;

																		   var key = EnergyAggregatorHelper.ConvertDateTimeByTimeIntervalToLong(acquisitionDateTime, AggregationInterval, startDayOfWeek);
							                                               List<MD> list;
							                                               if (!localDictionnary.TryGetValue(key, out list)) {
								                                               list = new List<MD>();
								                                               localDictionnary.Add(key, list);
							                                               }

							                                               list.Add(md);
						                                               }
					                                               }
					                                               return localDictionnary;

				}, localDictionnary => {
					                       lock (combineLock) {
						                       foreach (var t in localDictionnary) {
							                       List<MD> list;
							                       if (!dataGroupedByTime.TryGetValue(t.Key, out list)) {
								                       dataGroupedByTime.Add(t.Key, t.Value);
							                       }
							                       else {
								                       list.AddRange(t.Value);
							                       }
						                       }
					                       }
				});
			return dataGroupedByTime;
		}


		/// <summary>
		/// Gets the aggregation interval.
		/// </summary>
		/// <value>
		/// The aggregation interval.
		/// </value>
		public abstract AxisTimeInterval AggregationInterval { get; }

	}
}
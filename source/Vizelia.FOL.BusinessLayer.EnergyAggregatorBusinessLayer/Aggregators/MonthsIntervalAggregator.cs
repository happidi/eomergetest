using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.BusinessLayer{
	/// <summary>
	/// MonthsIntervalAggregator
	/// </summary>
	public class MonthsIntervalAggregator : BaseTimeIntervalAggregator {
		/// <summary>
		/// Gets the aggregation interval.
		/// </summary>
		/// <value>
		/// The aggregation interval.
		/// </value>
		public override AxisTimeInterval AggregationInterval {
			get { return AxisTimeInterval.Months; }
		}
	}
}
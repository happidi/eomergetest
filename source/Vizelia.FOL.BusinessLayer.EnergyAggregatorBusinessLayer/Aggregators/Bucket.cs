﻿using System;
using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.BusinessLayer{
	
	/// <summary>
	///  Represents a single aggregated result
	/// </summary>
	public class Bucket {

		/// <summary>
		/// Gets or sets the label.
		/// </summary>
		public string Label { get; set; }

		/// <summary>
		/// Gets or sets the value.
		/// </summary>
		public double Value { get; set; }

		/// <summary>
		/// Gets or sets the date time of the bucket.
		/// Relevant for buckets that are a result of time based aggregation
		/// </summary>
		public DateTime? DateTime { get; set; }

		/// <summary>
		/// Gets or sets the meter data validity.
		/// </summary>
		public MeterDataValidity MeterDataValidity { get; set; }
	}
}
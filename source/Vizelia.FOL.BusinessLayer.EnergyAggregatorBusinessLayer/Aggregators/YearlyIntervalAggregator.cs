using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.BusinessLayer{
	/// <summary>
	///  YearlyIntervalAggregator 
	/// </summary>
	public class YearlyIntervalAggregator : BaseTimeIntervalAggregator {
		/// <summary>
		/// Gets the aggregation interval.
		/// </summary>
		/// <value>
		/// The aggregation interval.
		/// </value>
		public override AxisTimeInterval AggregationInterval {
			get { return AxisTimeInterval.Years; }
		}
	}
}
﻿using System;
using System.Collections.Generic;
using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.BusinessLayer{
	
	/// <summary>
	/// Interface for time based data aggregation
	/// </summary>
	public interface ITimeIntervalAggregator : IDataAggregator {

		/// <summary>
		/// Gets the aggregation interval.
		/// </summary>
		/// <value>
		/// The aggregation interval.
		/// </value>
		AxisTimeInterval AggregationInterval { get; }

		/// <summary>
		/// Groups the data by time.
		/// </summary>
		/// <param name="data">The data.</param>
		/// <param name="timeZone">The time zone.</param>
		/// <param name="startDayOfWeek">The start day of week.</param>
		/// <returns></returns>
		Dictionary<long, List<MD>> GroupDataByTime(List<MD> data, TimeZoneInfo timeZone = null, int startDayOfWeek = 1);
	}
}
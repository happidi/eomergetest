using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.BusinessLayer{
	/// <summary>
	/// WeeklyIntervalAggregator
	/// </summary>
	public class WeeklyIntervalAggregator : BaseTimeIntervalAggregator {
		/// <summary>
		/// Gets the aggregation interval.
		/// </summary>
		/// <value>
		/// The aggregation interval.
		/// </value>
		public override AxisTimeInterval AggregationInterval {
			get { return AxisTimeInterval.Weeks; }
		}
	}
}
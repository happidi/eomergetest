using System;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessLayer{
	/// <summary>
	/// SemestersIntervalAggregator
	/// </summary>
	public class SemestersIntervalAggregator : BaseTimeIntervalAggregator {
		/// <summary>
		/// Gets the aggregation interval.
		/// </summary>
		/// <value>
		/// The aggregation interval.
		/// </value>
		public override AxisTimeInterval AggregationInterval {
			get { return AxisTimeInterval.Semesters; }
		}


		/// <summary>
		/// Formats the date to a label.
		/// </summary>
		/// <param name="date">The date.</param>
		/// <returns></returns>
		protected override string FormatDateToLabel(DateTime date) {
			string label = Helper.ConvertDateTimeToSemesterString(date);

			return label;

		}
	}
}
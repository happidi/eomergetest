﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Vizelia.FOL.BusinessEntities;
using System.IO;
using System.Threading.Tasks;
using System.Threading;
using System.Diagnostics;
using System.Runtime.InteropServices;
using Vizelia.FOL.BusinessLayer;

namespace Vizelia.FOL.Common {
	/// <summary>
	/// Energy Aggregator Helper
	/// </summary>
	public static class EnergyAggregatorHelper {
		

		/// <summary>
		/// Cleanups the memory.
		/// </summary>
		public static void CleanupMemory() {

			Thread t = new Thread(() => {
				Thread.Sleep(10000);
				GC.Collect(2, GCCollectionMode.Forced);

				GC.Collect(GC.MaxGeneration);
				GC.WaitForPendingFinalizers();
				SetProcessWorkingSetSize(Process.GetCurrentProcess().Handle, (UIntPtr)0xFFFFFFFF, (UIntPtr)0xFFFFFFFF);

			});
			t.Start();
		}

		/// <summary>
		/// Sets the size of the process working set.
		/// </summary>
		/// <param name="process">The process.</param>
		/// <param name="minimumWorkingSetSize">Minimum size of the working set.</param>
		/// <param name="maximumWorkingSetSize">Maximum size of the working set.</param>
		/// <returns></returns>
		[DllImport("kernel32.dll")]
		[return: MarshalAs(UnmanagedType.Bool)]
		private extern static bool SetProcessWorkingSetSize(IntPtr process, UIntPtr minimumWorkingSetSize, UIntPtr maximumWorkingSetSize);


		private static void AddBogusMeters(Vizelia.FOL.BusinessLayer.EnergyAggregatorState Entry, int count) {
			var maxMeterKey = Entry.Meters.Keys.Max() + 1;
			var maxMeterDataKey = 1;
			//if (tenantContainer.Entry.MeterDatas != null && tenantContainer.Entry.MeterDatas.GetMeterDataCount() > 0)
			//	maxMeterDataKey = tenantContainer.Entry.MeterDatas.Values.Max(list => list.Max(kvp => kvp.Value.KeyMeterData)) + 1;
			var classification = Entry.ClassificationItemHierarchy.Where(kvp => kvp.Value.Category.ToUpper() == "METER").FirstOrDefault().Value;
			var location = Entry.SpatialHierarchy.Where(kvp => kvp.Value.Level == 0).FirstOrDefault().Value;

			for (var i = 0; i < count; i++) {
				var meter = new Meter() {
					KeyClassificationItem = classification.KeyClassificationItem,
					ClassificationItemLevel = classification.Level,
					ClassificationItemColorB = classification.ColorB,
					ClassificationItemColorG = classification.ColorG,
					ClassificationItemColorR = classification.ColorR,
					ClassificationItemLocalIdPath = classification.LocalIdPath,
					ClassificationItemLongPath = classification.LongPath,
					ClassificationItemTitle = classification.Title,


					KeyLocation = location.KeyLocation,
					KeyLocationPath = location.KeyLocationPath,
					LocationLevel = location.Level,
					LocationLongPath = location.LongPath,
					LocationName = location.Name,
					LocationShortPath = location.ShortPath,
					LocationTypeName = location.TypeName,

					KeyMeter = MeterHelper.ConvertMeterKeyToString(maxMeterKey),
					Description = "Bogus",
					Name = "Meter " + i.ToString(),
					Factor = 1
				};

				Entry.Meters[maxMeterKey] = meter;

				var data = MeterHelper.GenerateMeterData(1, 0, maxMeterKey, maxMeterDataKey);
				Entry.MeterDatas.Add(maxMeterKey, data.Values.First().ToDictionary(kvp => kvp.Key, kvp => kvp.Value));

				maxMeterKey += 1;
				maxMeterDataKey += data.Count;
			}
		}

		/// <summary>
		/// Return the current DateTime grouped by the timeinterval as a long .(i.e. 20110101000000).
		/// This is quicker than creating a new DateTime and is used for performances reasons.
		/// </summary>
		/// <param name="d">The DateTime</param>
		/// <param name="interval">The time interval.</param>
		/// <param name="startDayOfWeek">The start day of week.</param>
		/// <returns></returns>
		public static long ConvertDateTimeByTimeIntervalToLong(DateTime d, AxisTimeInterval interval, int startDayOfWeek = 1) {
			long retVal = 0;
			
			switch (interval) {
				case AxisTimeInterval.Seconds:
					retVal = d.Second + d.Minute * 100 + d.Hour * 10000 + d.Day * 1000000 + d.Month * 100000000 + d.Year * 10000000000;
					break;
				case AxisTimeInterval.Minutes:
					retVal = d.Minute * 100 + d.Hour * 10000 + d.Day * 1000000 + d.Month * 100000000 + d.Year * 10000000000;
					break;
				case AxisTimeInterval.Hours:
					retVal = d.Hour * 10000 + d.Day * 1000000 + d.Month * 100000000 + d.Year * 10000000000;
					break;
				case AxisTimeInterval.Days:
					retVal = d.Day * 1000000 + d.Month * 100000000 + d.Year * 10000000000;
					break;
				case AxisTimeInterval.Weeks:
					DateTime nd;
					//here we aggregate to the monday + adjust to the correct start day of the week.
					switch (d.DayOfWeek) {
						case DayOfWeek.Tuesday:
							nd = d.AddDays(-1 + startDayOfWeek - 1);
							break;
						case DayOfWeek.Wednesday:
							nd = d.AddDays(-2 + startDayOfWeek - 1);
							break;
						case DayOfWeek.Thursday:
							nd = d.AddDays(-3 + startDayOfWeek - 1);
							break;
						case DayOfWeek.Friday:
							nd = d.AddDays(-4 + startDayOfWeek - 1);
							break;
						case DayOfWeek.Saturday:
							nd = d.AddDays(-5 + startDayOfWeek - 1);
							break;
						case DayOfWeek.Sunday:
							nd = d.AddDays(-6 + startDayOfWeek - 1);
							break;
						case DayOfWeek.Monday:
							nd = d.AddDays(0 + startDayOfWeek - 1);
							break;
						default:
							throw new ArgumentOutOfRangeException("d.dayOfWeek");
					}
					retVal = nd.Day * 1000000 + nd.Month * 100000000 + nd.Year * 10000000000;
					break;

				case AxisTimeInterval.Months:
					retVal = 1 * 1000000 + d.Month * 100000000 + d.Year * 10000000000;
					break;
				case AxisTimeInterval.Years:
					retVal = 1 * 1000000 + 1 * 100000000 + d.Year * 10000000000;
					break;
				case AxisTimeInterval.Quarters:
					var q = (d.Month - 1) / 3 + 1;//quarter number : 1, 2, 3 or 4
					var m = 3 * (q - 1) + 1; // converted back to month number.
					retVal = 1 * 1000000 + m * 100000000 + d.Year * 10000000000;
					break;
				case AxisTimeInterval.Semesters:
					var s = (d.Month - 1) / 6 + 1;//Semesters number : 1 or 2
					var mo = 6 * (s - 1) + 1; // converted back to month number.
					retVal = 1 * 1000000 + mo * 100000000 + d.Year * 10000000000;
					break;
				case AxisTimeInterval.Global:
				case AxisTimeInterval.None:
					retVal = ConvertDateTimeByTimeIntervalToLong(Helper.DateTimeNow, AxisTimeInterval.Minutes);
					break;
			}
			return retVal;
		}

		/// <summary>
		/// Gets the date time.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <returns></returns>
		public static DateTime ConvertLongToDateTime(long key){
			int year = Convert.ToInt32(key/10000000000);
			key = key - year*10000000000;

			int month = Convert.ToInt32(key/100000000);
			key = key - month*100000000;

			int day = Convert.ToInt32(key/1000000);
			key = key - day*1000000;

			int hour = Convert.ToInt32(key/10000);
			key = key - hour*10000;

			int minute = Convert.ToInt32(key/100);
			key = key - minute*100;

			int second = Convert.ToInt32(key);

			return new DateTime(year, month, day, hour, minute, second);
		}


		/// <summary>
		/// Gets the appropriate time aggregator.
		/// </summary>
		/// <param name="interval">The interval.</param>
		/// <returns></returns>
		/// <exception cref="System.ArgumentOutOfRangeException">interval</exception>
		public static ITimeIntervalAggregator GetTimeAggregator(AxisTimeInterval interval){
			switch (interval) {
				case AxisTimeInterval.None:
				case AxisTimeInterval.Global:
					return new GlobalIntervalAggregator();
				case AxisTimeInterval.Years:
					return new YearlyIntervalAggregator();
				case AxisTimeInterval.Semesters:
					return new SemestersIntervalAggregator();
				case AxisTimeInterval.Quarters:
					return new QuartersIntervalAggregator();
				case AxisTimeInterval.Months:
					return new MonthsIntervalAggregator();
				case AxisTimeInterval.Weeks:
					return new WeeklyIntervalAggregator();
				case AxisTimeInterval.Days:
					return new DaysIntervalAggregator();
				case AxisTimeInterval.Hours:
					return new HoursIntervalAggregator();
				case AxisTimeInterval.Minutes:
					return new MinutesIntervalAggregator();
				case AxisTimeInterval.Seconds:
					return new SecondsIntervalAggregator();
				default:
					throw new ArgumentOutOfRangeException("interval");
			}
		}

	}
}

﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using DDay.iCal;
using IronPython.Hosting;
using IronPython.Runtime.Operations;
using Microsoft.Scripting;
using Microsoft.Scripting.Hosting;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessLayer.Broker;
using Vizelia.FOL.BusinessLayer.Interfaces;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Configuration;
using Vizelia.FOL.Common.Localization;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.BusinessLayer {

	/// <summary>
	/// Business Layer implementation for Energy.
	/// </summary>
	public class EnergyAggregatorBusinessLayer : IEnergyAggregatorBusinessLayer {

		#region Private Variables
		private const string const_classification_name_placeholder = "{ClassificationName}";
		private static TenantContainer<EnergyAggregatorState> tenantContainer = new TenantContainer<EnergyAggregatorState>();
		private static readonly object generallock = new object();
		private static Dictionary<string, object> lockdictionary = new Dictionary<string, object>();
		private static readonly object chartprocesslock = new object();
		private readonly int m_ProcessorCount = Environment.ProcessorCount;

		#endregion

		#region  AlarmDefinition_Process
		/// <summary>
		/// Process all the enabled AlarmDefinition to generate the AlarmInstances.
		/// </summary>
		/// <param name="context">The context.</param>
		/// <param name="alarms">The alarms.</param>
		/// <param name="culture">The culture.</param>
		/// <returns></returns>
		public Dictionary<AlarmDefinition, List<AlarmInstance>> AlarmDefinition_Process(EnergyAggregatorContext context, List<AlarmDefinition> alarms, string culture = null) {
			Helper.SetUICulture(culture);
			var retVal = new Dictionary<AlarmDefinition, List<AlarmInstance>>();
			var instances = new List<AlarmInstance>();

			instances.AddRange(alarms.SelectMany(a => AlarmDefinition_ProcessSingle(context, a)));
			//in order to limit the serialisation of the alarm instances we group the alarm instances by alarm definition;

			var groups = instances.GroupBy(i => i.AlarmDefinition);
			foreach (var group in groups) {
				var alarmDefinition = group.Key;
				foreach (var instance in group) {
					instance.AlarmDefinition = null;
				}
				retVal.Add(alarmDefinition, new List<AlarmInstance>(group));
			}
			return retVal;
		}
		#endregion

		#region  MeterValidationRule_Process
		/// <summary>
		/// Process all the enabled MeterValidationRule to generate the AlarmInstances.
		/// </summary>
		/// <param name="context">The context.</param>
		/// <param name="rules">The rules.</param>
		/// <param name="culture">The culture.</param>
		/// <returns></returns>
		public Dictionary<MeterValidationRule, List<AlarmInstance>> MeterValidationRule_Process(EnergyAggregatorContext context, List<MeterValidationRule> rules, string culture = null) {
			Helper.SetUICulture(culture);
			var retVal = new Dictionary<MeterValidationRule, List<AlarmInstance>>();
			var instances = new List<AlarmInstance>();

			//rules.ForEach(rule => instances.AddRange(MeterValidationRule_ProcessSingle(rule)));
			foreach (var rule in rules) {
				instances.AddRange(MeterValidationRule_ProcessSingle(rule));
			}

			var groups = instances.GroupBy(i => i.MeterValidationRule);
			foreach (var group in groups) {
				var metervalidationrule = group.Key;
				foreach (var instance in group) {
					instance.MeterValidationRule = null;
				}
				retVal.Add(metervalidationrule, new List<AlarmInstance>(group));
			}
			return retVal;
		}
		#endregion

		#region InitializeData
		/// <summary>
		/// Initializes the data.
		/// </summary>
		/// <param name="tenantDateRangeList">The tenant date range list.</param>
		public void Initialize(List<EnergyAggregatorContext> tenantDateRangeList) {
			//TracingService.Write(TraceEntrySeverity.Information, "Validating licence", "Licencing");

			//TracingService.Write(TraceEntrySeverity.Information, "Licence is valid", "Licencing");

			var action = new Action(() => {
				foreach (var context in tenantDateRangeList) {
					lock (generallock) {
						if (!lockdictionary.ContainsKey(context.ApplicationName))
							lockdictionary.Add(context.ApplicationName, new object());
					}

					lock (lockdictionary[context.ApplicationName]) {
						TenantHelper.RunInDifferentTenantContext(context.ApplicationName, () => {

							var coreBusinessLayer = Helper.Resolve<ICoreBusinessLayer>();
							var energyBusinessLayer = Helper.Resolve<IEnergyBusinessLayer>();

							using (TracingService.StartTracing("EnergyAggregator", context.ApplicationName)) {
								try {

									if (!LicencingHelper.Validate()) {
										TracingService.Write(TraceEntrySeverity.Error, "Licence is invalid", "Licencing");
										return;
									}

									if (this.IsInitialized(context) == false) {

										//We need to force the localization provider to init, so we use the VizeliaResourceManager
										LocalizationService.Init();

										TracingService.Write(TraceEntrySeverity.Information, "Start InitializeData", "Information");
										tenantContainer.Entry = new EnergyAggregatorState();
										tenantContainer.Entry.LoadingMeterData = true;
										tenantContainer.Entry.StartDate = context.StartDate;
										tenantContainer.Entry.EndDate = context.EndDate;
										tenantContainer.Entry.SpatialHierarchy = new ConcurrentDictionary<string, Location>(coreBusinessLayer.Location_GetDictionary());

										tenantContainer.Entry.ClassificationItemHierarchy = new ConcurrentDictionary<string, ClassificationItem>(coreBusinessLayer.ClassificationItem_GetDictionary(""));//(meterClassificationFilter
										tenantContainer.Entry.SiteMaxLevel = coreBusinessLayer.Site_GetMaxLevel();
										tenantContainer.Entry.Meters = new ConcurrentDictionary<int, Meter>(energyBusinessLayer.Meter_GetDictionary());
										tenantContainer.Entry.HistoricalData = new ConcurrentDictionary<string, PsetAttributeHistorical>(coreBusinessLayer.PsetAttributeHistorical_GetDictionary());

										TracingService.Write(TraceEntrySeverity.Information, "Start InitCalendarEventOccurences", "Information");

										var calendars = coreBusinessLayer.CalendarEvent_GetAll();
										Stopwatch swCal = new Stopwatch();
										swCal.Start();
										InitCalendarEventOccurences(calendars);

										swCal.Stop();
										TracingService.Write(string.Format("End InitCalendarEventOccurences :{0} seconds, {1} data in Memory", (swCal.ElapsedMilliseconds / 1000).ToString(), GetCalendarOccurrencesCount()), "Information");

										TracingService.Write(TraceEntrySeverity.Information, string.Format("InitMeterData starting from {0} to {1}", tenantContainer.Entry.StartDate.ToString("s"), tenantContainer.Entry.EndDate.ToString("s")), "Information");
										Stopwatch sw = new Stopwatch();
										sw.Start();
										this.InitMeterData();
										sw.Stop();
										TracingService.Write(string.Format("InitMeterData end :{0} seconds, {1} data in Memory", (sw.ElapsedMilliseconds / 1000).ToString(), this.GetMeterDataCount(context).ToString()), "Information");

										tenantContainer.Entry.IsInitialized = true;
										TracingService.Write(TraceEntrySeverity.Information, "End InitializeData", "Information");
									}
								}
								catch (SystemException e) {
									TracingService.Write(e);
								}
							}
						});
					}
				}
			});
			if (tenantDateRangeList.Any(context => !context.StartInSeperateThread)) {
				action();
			}
			else {
				Helper.StartNewThread(action);
			}
		}


		#endregion

		#region Chart_Process


		/// <summary>
		/// Process a collection of Charts.
		/// </summary>
		/// <param name="context">The context.</param>
		/// <param name="Keys">The keys.</param>
		/// <param name="locationFilter">The location filter.</param>
		/// <param name="culture">The culture.</param>
		/// <returns></returns>
		public List<Chart> Chart_Process(EnergyAggregatorContext context, List<string> Keys, List<Filter> locationFilter, string culture) {
			Helper.SetUICulture(culture);
			
			GCLatencyMode oldMode = GCSettings.LatencyMode;
			var retVal = new List<Chart>();
			try {
				GCSettings.LatencyMode = GCLatencyMode.LowLatency;
				foreach (var key in Keys) {
					var chart = Chart_Process(context, key, locationFilter, culture);
					retVal.Add(chart);
				}
			}
			finally {
				GCSettings.LatencyMode = oldMode;
			}
			return retVal;
		}

		/// <summary>
		/// Process a single Chart.
		/// </summary>
		/// <param name="context">The context.</param>
		/// <param name="key">The key of the Chart.</param>
		/// <param name="locationFilter">The location filter.</param>
		/// <param name="culture">The culture.</param>
		/// <param name="chart">The chart.</param>
		/// <returns></returns>
		public Chart Chart_Process(EnergyAggregatorContext context, string key, List<Filter> locationFilter, string culture, Chart chart = null) {

			Helper.SetUICulture(culture);
			
			var stopwatch = new Stopwatch();
			var perf = new Dictionary<string, double>();
			var energyBusinessLayer = Helper.Resolve<IEnergyBusinessLayer>();

			ContextHelper.Add("LocationFilter", locationFilter);
			var isAnalytics = chart != null;

			if (!isAnalytics) {
				stopwatch.Start();
				chart = energyBusinessLayer.Chart_GetItem(key, "*");
				stopwatch.Stop();
				perf.Add("Chart_GetItem", stopwatch.ElapsedMilliseconds);
			}

			if (!isAnalytics || chart.Meters.Count == 0) {
				stopwatch.Restart();
				chart.Meters = energyBusinessLayer.Chart_GetMeters(key, true);
				stopwatch.Stop();
				perf.Add("Chart_GetMeters", stopwatch.ElapsedMilliseconds);
			}

			if (!isAnalytics ||  chart.FilterSpatial.Count == 0) {
				stopwatch.Restart();
				chart.FilterSpatial = energyBusinessLayer.Chart_GetLocations(key, chart.Meters);
				stopwatch.Stop();
				perf.Add("Chart_GetLocationKeys", stopwatch.ElapsedMilliseconds);
			}

			if (chart.DrillDownEnabled && chart.DrillDown != null && chart.DrillDownMode != ChartDrillDownMode.Disabled) {
				energyBusinessLayer.ChartDrillDown_Apply(chart, chart.Meters, chart.FilterSpatial);
			}
			var metersKeys = chart.Meters.Select(m => MeterHelper.ConvertMeterKeyToInt(m.KeyMeter)).ToList();
			var locationsKeys = chart.FilterSpatial.Select(l => l.KeyLocation).ToList();

			stopwatch.Restart();
			chart.HistoricalAnalysis = Chart_GetItemWithDataHistorical(energyBusinessLayer, context, chart, metersKeys, locationsKeys, locationFilter, culture);
			stopwatch.Stop();
			perf.Add("Chart_GetItemWithDataHistorical", stopwatch.ElapsedMilliseconds);

			stopwatch.Restart();
			chart = Chart_Process(context, chart, metersKeys, locationsKeys, null, locationFilter, culture);
			stopwatch.Stop();
			perf.Add("Chart_Process", stopwatch.ElapsedMilliseconds);

			stopwatch.Restart();
			if (!chart.IsEnergyAggregatorLoadingMeterData) {
				energyBusinessLayer.Chart_FixScriptChanges(chart);
				energyBusinessLayer.Chart_AssociateAlarmInstance(chart);

				//we use this in the info icon, but we dont put the meters in the chart before otherwise the serialization is too slow.
				chart.MetersCount = chart.Meters.Count;
			}

			stopwatch.Stop();
			perf.Add("Chart_FixScriptChanges", stopwatch.ElapsedMilliseconds);
			return chart;
		}

		/// <summary>
		/// Returns the list of extra Charts generated by the associated Historical Analysis.
		/// </summary>
		/// <param name="energyBusinessLayer">The energy business layer.</param>
		/// <param name="context">The context.</param>
		/// <param name="chart">The chart.</param>
		/// <param name="meterKeys">The meter keys.</param>
		/// <param name="locationKeys">The location keys.</param>
		/// <param name="locationFilter">The location filter.</param>
		/// <param name="culture">The culture.</param>
		/// <returns></returns>
		private Dictionary<ChartHistoricalAnalysis, DataSerieCollection> Chart_GetItemWithDataHistorical(IEnergyBusinessLayer energyBusinessLayer, EnergyAggregatorContext context, Chart chart, List<int> meterKeys, List<string> locationKeys, List<Filter> locationFilter, string culture) {
			var retVal = new Dictionary<ChartHistoricalAnalysis, DataSerieCollection>();
			var existingSeries = chart.Series;
			foreach (var historicalAnalysis in chart.HistoricalAnalysisDefinitions.Where(historicalAnalysis => historicalAnalysis.Enabled)) {
				ChartHelper.UpdateChartHistoricalAnalysisDates(chart, historicalAnalysis);
				var tmpChart = Chart_Process(context, chart, meterKeys, locationKeys, historicalAnalysis, locationFilter, culture);
				//we propagete the RunSuccessfully option
				chart.CalculatedSeries = tmpChart.CalculatedSeries;
				energyBusinessLayer.Chart_ApplySortOrder(tmpChart);
				var series = tmpChart.Series;
				if (historicalAnalysis.IncludeInMainTimeline == false)
					energyBusinessLayer.Chart_ApplyPalette(tmpChart);
				retVal.Add(historicalAnalysis, series);

				chart.Series = existingSeries;
			}
			return retVal;
		}

		/// <summary>
		/// Process the Chart and adds returns the Series collecion.
		/// </summary>
		/// <param name="context">The context.</param>
		/// <param name="chart">The chart.</param>
		/// <param name="meterKeys">The meter keys.</param>
		/// <param name="locationKeys">The location keys.</param>
		/// <param name="historicalAnalysis">The historical analysis.</param>
		/// <param name="locationFilter">The location filter.</param>
		/// <param name="culture">The culture.</param>
		/// <param name="alarm">The alarm (just in case a chart is assigned to an alarm definition and we need to apply the alarm calendar as well).</param>
		/// <returns></returns>
		public Chart Chart_Process(EnergyAggregatorContext context, Chart chart, List<int> meterKeys, List<string> locationKeys, ChartHistoricalAnalysis historicalAnalysis, List<Filter> locationFilter, string culture = null, AlarmDefinition alarm = null) {

			Helper.SetUICulture(culture);

			Chart retVal = null;
			#region Check Init
			if (!this.IsInitialized(context) || this.IsLoadingMeterData(context) || tenantContainer.Entry.MeterDatas == null) {
				chart.IsEnergyAggregatorLoadingMeterData = true;
				return chart;
			}
			#endregion

			var meters = new List<Meter>();
			var locations = new List<Location>();
			FetchMetersAndLocations(meterKeys, locationKeys, out meters, out locations);

			retVal = Chart_Process(context, chart, meters, locations, historicalAnalysis, locationFilter, culture, alarm);

			return retVal;
		}
		/// <summary>
		/// Process the Chart and adds returns the Series collecion.
		/// </summary>
		/// <param name="context">The context.</param>
		/// <param name="chart">The chart.</param>
		/// <param name="meters">The meters.</param>
		/// <param name="locations">The locations.</param>
		/// <param name="historicalAnalysis">The historical analysis.</param>
		/// <param name="locationFilter">The location filter.</param>
		/// <param name="culture">The culture.</param>
		/// <param name="alarm">The alarm (just in case a chart is assigned to an alarm definition and we need to apply the alarm calendar as well).</param>
		/// <returns></returns>
		protected Chart Chart_Process(EnergyAggregatorContext context, Chart chart, List<Meter> meters, List<Location> locations, ChartHistoricalAnalysis historicalAnalysis, List<Filter> locationFilter, string culture = null, AlarmDefinition alarm = null) {
			DataSerieCollection retVal = null;
			int meterdataCount = 0;
		    var warningsList = new List<string>();
			var errorsList = new List<string>();
			//lock (chartprocesslock) 
			{
				
				CheckEntityLongPath();
				//To prevent duplicates element when the chart is called in historical analysis mode, for example.
				//DataSerieHelper.CleanData(chart.Series);
				//chart.Series = new DataSerieCollection();
				chart.IsEnergyAggregatorLoadingMeterData = false;
				try {
					#region StopWatch
					var sw = new Stopwatch();
					var performances = new Dictionary<string, double>();
					#endregion
					#region StartDate and EndDate
					var startDateValue = (historicalAnalysis != null && historicalAnalysis.StartDate.HasValue) ? historicalAnalysis.StartDate.Value : chart.StartDate;
					var endDateValue = (historicalAnalysis != null && historicalAnalysis.EndDate.HasValue) ? historicalAnalysis.EndDate.Value : chart.EndDate;
					#endregion
					#region GetMeterDataRealAndVirtual
					sw.Restart();
					if (chart.MaxMeter == 0) {
						chart.MaxMeter = Chart.const_max_meter_default_value;
					}
					meters = meters.OrderBy(m => m.Name).Take(chart.MaxMeter).ToList();
					meters = GetMeterDataRealAndVirtual(meters, startDateValue, endDateValue, false, true, chart.TimeInterval, chart.CalendarEventCategory, chart.CalendarMode, chart.UseDataAcquisitionEndPoint, errors: errorsList);
					meterdataCount = meters.Sum(m => m.MDData == null ? 0 : m.MDData.Count);
					sw.Stop();
					performances.Add("GetMeterDataRealAndVirtual", sw.Elapsed.TotalSeconds);
					#endregion
					#region Alarm calendar filtering
					if (alarm != null && alarm.CalendarEventCategory != null) {
						FilterByCalendar(meters, startDateValue, endDateValue, alarm.CalendarEventCategory, alarm.CalendarMode);//, alarm.GetTimeZone()
					}
					#endregion
					#region HistoricalPsetCreateMeters
					sw.Restart();
					HistoricalPsetCreateMeters(chart, locations, meters, startDateValue, endDateValue, chart.TimeInterval, locationFilter);
					sw.Stop();
					performances.Add("HistoricalPsetCreateMeters", sw.Elapsed.TotalSeconds);
					#endregion
					#region AlarmInstancesCreateMeters
					sw.Restart();
					AlarmInstancesCreateMeters(chart, meters);
					sw.Stop();
					performances.Add("AlarmInstancesCreateMeters", sw.Elapsed.TotalSeconds);
					#endregion
					#region GroupByLocalisationAndClassification
					sw.Restart();
					var series = GroupByLocalisationAndClassification(meters, chart, historicalAnalysis);
					sw.Stop();
					performances.Add("GroupByLocalisation", sw.Elapsed.TotalSeconds);
					#endregion
					#region OffsetHistorical
					sw.Restart();
					if (historicalAnalysis == null) {
						OffsetHistorical(chart.HistoricalAnalysis, series);
					}
					sw.Stop();
					performances.Add("OffsetHistorical", sw.Elapsed.TotalSeconds);
					#endregion
					#region Limit Raw Data
					LimitElements(series, true);
					#endregion
					#region Apply Pset tRatio
					sw.Restart();
					series = ApplyPsetRatios(series, chart.TimeInterval, startDateValue, endDateValue, chart.HideMeterDataValidity, chart.GetTimeZone());
					sw.Stop();
					performances.Add("ApplyPsetRatio", sw.Elapsed.TotalSeconds);
					#endregion
					#region Group By Time Interval
					sw.Restart();
					GroupListDataSerieByTimeInterval(series, chart.TimeInterval, chart.HideMeterDataValidity, chart.GetTimeZone(), chart.XAxisStartDayOfWeek);
					sw.Stop();
					performances.Add("GroupByTimeInterval", sw.Elapsed.TotalSeconds);
					#endregion
					#region Apply DataSerie Calculation
					sw.Restart();
					ApplyDataSerieCalculation(series);
					sw.Stop();
					performances.Add("ApplyDataSerieCalculation", sw.Elapsed.TotalSeconds);
					#endregion
					#region Generate StatisticalSeries
					GenerateStatisticalSeries(chart.StatisticalSeries, series, chart.Series, false, historicalAnalysis,chart.KeyChart);
					#endregion
					#region Limit & Color Elements
					LimitElements(series, false);
					ColorElements(series);
					#endregion
					#region Generate CalculatedSeries
					sw.Restart();
                    warningsList.AddRange(GenerateCalculatedSeries(chart.KeyChart, chart.UseSpatialPath, chart.UseClassificationPath, chart.CalculatedSeries, series, chart.Series, historicalAnalysis));
					sw.Stop();
					performances.Add("GenerateCalculatedSerie", sw.Elapsed.TotalSeconds);
					#endregion
					#region Generate StatisticalSeries
                    GenerateStatisticalSeries(chart.StatisticalSeries, series, chart.Series, true, historicalAnalysis, chart.KeyChart);
					#endregion
					#region Sort Points
					sw.Restart();
					SortPoints(series);
					sw.Stop();
					performances.Add("SortPoints", sw.Elapsed.TotalSeconds);
					#endregion
					#region Apply LocationName (For Aggregation = None)
					sw.Restart();
					ApplyLocationName(chart.TimeInterval, series);
					sw.Stop();
					performances.Add("ApplyLocationName", sw.Elapsed.TotalSeconds);
					#endregion
					#region Specific Analysis
					sw.Restart();
					SpecificAnalysis(chart, series);
					sw.Stop();
					performances.Add("SpecificAnalysis", sw.Elapsed.TotalSeconds);
					#endregion
					#region RunScripts
					if (chart.Algorithms != null && historicalAnalysis == null) {
						RunPython(chart, series, chart.Algorithms.Where(script => script.Algorithm != null && script.Algorithm.Language == AlgorithmLanguage.Python && script.Algorithm.Source == AlgorithmSource.Editable).ToList());
						RunCSharp(context, ref chart, ref series, chart.Algorithms.Where(script => script.Algorithm == null || script.Algorithm.Language == AlgorithmLanguage.CSharp || script.Algorithm.Source == AlgorithmSource.ExternalLibrary || script.Algorithm.Source == AlgorithmSource.BuiltIn).ToList());
					}
					#endregion
					#region retVal
					retVal = new DataSerieCollection(series);
					#endregion
					#region Memory Cleanup
					meters = null;
					//EnergyAggregatorHelper.CleanupMemory();
					#endregion
					#region Performance Debug
					//if the total of the function took more than 5 sec we write it to the trace.
					TracePerformances(chart, meterdataCount, performances, 5);
					#endregion
					#region Validate Max Meters
					//var energyBusinessLayer = Helper.Resolve<IEnergyBusinessLayer>();
					int resultingMetersCount = chart.ResultingMeters.Count; //energyBusinessLayer.GetResultingMetersCount(chart);
					var validator = new MaxMeterValidator(chart, resultingMetersCount);
					validator.ValidateFormResponse();

					#endregion
				}
				catch (SystemException e) {
					#region Trace error
					TracingService.Write(e);
					#endregion
				}
			}
			chart.Series = retVal;
            chart.Warnings.AddRange(warningsList);
			chart.Errors.AddRange(errorsList);
			return chart;
		}

		/// <summary>
		/// Traces the performances.
		/// </summary>
		/// <param name="chart">The chart.</param>
		/// <param name="meterdataCount">The meterdata count.</param>
		/// <param name="performances">The performances.</param>
		/// <param name="thresholdInSeconds">The threshold in seconds.</param>
		private static void TracePerformances(Chart chart, int meterdataCount, Dictionary<string, double> performances, double thresholdInSeconds) {
			var totalSeconds = performances.Sum(kvp => kvp.Value);
			if (totalSeconds > thresholdInSeconds) {
				var message = new StringBuilder();
				if (chart != null) {
					message.AppendLine(string.Format("Chart: {1} {2} for Tenant {0}", ContextHelper.ApplicationName, chart.KeyChart, chart.Title ?? ""));
					message.AppendLine(string.Format("Provider: {0}", tenantContainer.Entry.MeterDatas.GetType().Name));
					if (meterdataCount > 0)
						message.AppendLine(string.Format("MeterData Count: {0}", meterdataCount.ToString("N")));
					message.AppendLine(string.Format("Total Processed Time: {0} seconds", totalSeconds.ToString("N")));
					message.AppendLine();
				}
				foreach (var kvp in performances) {
					message.AppendLine(string.Format("{0}: {1} sec", kvp.Key, kvp.Value.ToString("N")));
				}
				using (TracingService.StartTracing("Chart_Process", chart != null ? chart.KeyChart : "")) {
					TracingService.Write(TraceEntrySeverity.Warning, message.ToString());
				}
			}
		}
		#endregion

		#region Chart_GetDataSerieLocalId
		/// <summary>
		/// Return a list of available Series name for a specific Chart.
		/// </summary>
		/// <param name="context">The context.</param>
		/// <param name="chart">The chart.</param>
		/// <param name="meterKeys">The meter keys.</param>
		/// <param name="locationKeys">The location keys.</param>
		/// <param name="analysis">The historical analysis.</param>
		/// <param name="locationFilter">The location filter.</param>
		/// <param name="includeExisting">True to include existing serie, false to return only series to be created.</param>
		/// <param name="culture">The culture.</param>
		/// <returns></returns>
		public List<ListElement> Chart_GetDataSerieLocalId(EnergyAggregatorContext context, Chart chart, List<int> meterKeys, List<string> locationKeys, ChartHistoricalAnalysis analysis, List<Filter> locationFilter, bool includeExisting, string culture = null) {
			if (!this.IsInitialized(context)) {
				return new List<ListElement>();
			}

			var meters = new List<Meter>();
			var locations = new List<Location>();
			FetchMetersAndLocations(meterKeys, locationKeys, out meters, out locations);

			return Chart_GetDataSerieLocalId(chart, meters, locations, analysis, locationFilter, includeExisting, culture);
		}

		/// <summary>
		/// Return a list of available Series name for a specific Chart.
		/// </summary>
		/// <param name="chart">The chart.</param>
		/// <param name="meters">The meters.</param>
		/// <param name="locations">The locations.</param>
		/// <param name="analysis">The historical analysis.</param>
		/// <param name="locationFilter">The location filter.</param>
		/// <param name="includeExisting">True to include existing serie, false to return only series to be created.</param>
		/// <param name="culture">The culture.</param>
		/// <returns></returns>
		private List<ListElement> Chart_GetDataSerieLocalId(Chart chart, List<Meter> meters, List<Location> locations, ChartHistoricalAnalysis analysis, List<Filter> locationFilter, bool includeExisting, string culture = null) {
			Helper.SetUICulture(culture);
			CheckEntityLongPath();
			var existingSeries = chart.Series;
			
			#region SplitByEvent Mode
			
			//we need to generate DataByCalendarEvenT if we are on SplitByEvent mode.
			if (chart.CalendarEventCategory != null && chart.CalendarMode == ChartCalendarMode.SplitByEvent) {
				var category = chart.CalendarEventCategory;

				foreach (var meter in meters) {
					List<VizEventOccurence> occurrences;

					//We first check if a calendar has been defined on the meter level.
					tenantContainer.Entry.CalendarEventOccurrences.TryGetValue(meter.KeyMeter, out occurrences);

					var keyLocation = meter.KeyLocation;
					var isCategoryInCalendar = false;
					while (!isCategoryInCalendar && !string.IsNullOrWhiteSpace(keyLocation)) {
							tenantContainer.Entry.CalendarEventOccurrences.TryGetValue(keyLocation, out occurrences);

						keyLocation = tenantContainer.Entry.SpatialHierarchy[keyLocation].KeyParent;
						isCategoryInCalendar = CalendarContainsCategory(occurrences, category);
							}

					if (!isCategoryInCalendar) {
						occurrences = null;
								}

					meter.DataByCalendarEvent = new Dictionary<string, List<MD>>();

					if (occurrences != null) {
						foreach (var occ in occurrences) {
							if (occ.Category == category.LocalId && !(occ.StartDate < chart.StartDate.ToUniversalTime()) && !(occ.EndDate > chart.EndDate.ToUniversalTime())) {
								var eventType = occ.Summary;
								if (!meter.DataByCalendarEvent.ContainsKey(eventType))
									meter.DataByCalendarEvent.Add(eventType, new List<MD>());
							}
						}
					}
				}
			}
			#endregion
			#region HistoricalPsetCreateMeters
			HistoricalPsetCreateMeters(chart, locations, meters, chart.StartDate, chart.EndDate, chart.TimeInterval, locationFilter);
			#endregion
			#region AlarmInstancesCreateMeters
			AlarmInstancesCreateMeters(chart, meters);
			#endregion
			#region GroupByLocalisationAndClassification
			var series = GroupByLocalisationAndClassification(meters, chart, analysis);
			var retVal = series.Select(serie => new ListElement {
				Id = serie.LocalId,
				Value = serie.LocalId,
				MsgCode = serie.Name,
				IconCls = Helper.GetAttributeValue<IconClsAttribute>(typeof(DataSerie))
			}).ToList();

			#endregion
			#region Sort by Name
			retVal = retVal.OrderBy(el => el.MsgCode).ToList();
			#endregion
			#region Calculated series
			//we add the calculated series here.
			if (chart.CalculatedSeries != null) {
				var updateCalculations = UpdateCalculatedSeriesFromClassificationItem(chart.CalculatedSeries, series, chart.UseSpatialPath);
				if (analysis != null) {
					updateCalculations.ForEach(c => {
						c.LocalId += string.Format(" - {0}", analysis.LocalId);
						c.Name += string.Format(" - {0}", analysis.Name);
					});
				}
				var calc = new List<ListElement>();
				updateCalculations.ForEach(calcul => {
					if ((includeExisting == true || !existingSeries.ContainsLocalid(calcul.LocalId)) && (calcul.RunOnlyInMainTimeline == false || analysis == null)) {
						calc.Add(new ListElement() {
							Id = calcul.LocalId,
							Value = calcul.LocalId,
							MsgCode = calcul.Name,
							IconCls = Helper.GetAttributeValue<IconClsAttribute>(typeof(CalculatedSerie))
						});
					}
				});
				calc = calc.OrderBy(el => el.MsgCode).ToList();
				if (calc.Count > 0) {
					retVal.AddRange(calc);
				}
			}
			#endregion
			#region Statistical series
			//we add the Statistical series here.
			if (chart.StatisticalSeries != null) {
				chart.StatisticalSeries.ForEach(statistical => {
					var classification = tenantContainer.Entry.ClassificationItemHierarchy[statistical.KeyClassificationItem];
					var statisticalLocalId = String.Format("{0} - {1}", statistical.Operator.ToString(), classification.Title);
					var statisticalName = String.Format("{0} - {1}", statistical.Operator.GetLocalizedText(), classification.Title);
					if (analysis != null) {
						statisticalLocalId += string.Format(" - {0}", String.IsNullOrEmpty(analysis.Name) ? analysis.KeyChart : analysis.Name);
						statisticalName += string.Format(" - {0}", String.IsNullOrEmpty(analysis.Name) ? analysis.KeyChart : analysis.Name);
					}
					retVal.Add(new ListElement() {
						Id = statisticalLocalId,//"Statistical " + classification.LocalIdPath + statistical.LocalId,
						Value = statisticalLocalId,
						MsgCode = statisticalName,
						IconCls = Helper.GetAttributeValue<IconClsAttribute>(typeof(StatisticalSerie))
					});
				});

			}
			#endregion
			#region Algorithms
			if (chart.Algorithms != null && chart.Algorithms.Count > 0) {
				var scripts = chart.Algorithms.Where(script => script.Algorithm == null || script.Algorithm.Language == AlgorithmLanguage.CSharp);
				foreach (ChartAlgorithm script in scripts) {
					AlgorithmInstance<IAnalyticsExtensibility> instance;

					List<string> errors;
					instance = CreateAlgorithmInstance(script, out errors);

					if (errors == null || errors.Count == 0) {
						try {
							retVal.AddRange(instance.Algorithm.GetDataSeriesLocalId());
						}
						catch (Exception e) {
							TracingService.Write(e);
						}
					}
				}
			}

			#endregion
			#region Filter to display only the non existing series (except if include existing is true)
			retVal = retVal.Distinct().ToList();
			var filteredretVal = new List<ListElement>();
			if (includeExisting == true)
				filteredretVal = retVal;
			else {
				//Add the available series localid and match it against the existing series if includeExisting is false.
				retVal.ForEach(list => {
					if (!existingSeries.ContainsLocalid(list.Value))
						filteredretVal.Add(list);
				});
			}
			#endregion
			return filteredretVal;
		}
		#endregion

		#region MeterData_GetStoreFromMeter
		/// <summary>
		/// Gets a json store for the business entity MeterData.
		/// </summary>
		/// <param name="context">The context.</param>
		/// <param name="meter">The meter.</param>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="startDate">The start date.</param>
		/// <param name="endDate">The end date.</param>
		/// <param name="rawData">if set to <c>true</c> [raw data].</param>
		/// <returns>
		/// A json store.
		/// </returns>
		public JsonStore<MeterData> MeterData_GetStoreFromMeter(EnergyAggregatorContext context, Meter meter,
			PagingParameter paging, DateTime? startDate, DateTime? endDate, bool rawData = false) {
			if (paging == null) {
				throw new ArgumentNullException("paging");
			}

			var retVal = new JsonStore<MeterData>();
			if (meter != null && !IsLoadingMeterData(context)) {
				var meters = new List<Meter> { meter };
				meters = GetMeterDataRealAndVirtual(meters, null, null, rawData, false);

				if (meters.Count > 0 && meters[0].MDData != null) {
					//We filter before converting back to meter data

					MeterHelper.ChangeValidityFieldStringValueToInt(paging, "Validity");

					var tmpJsonStore = meters[0].MDData.ToJsonStoreWithFilter(paging);
					retVal.recordCount = tmpJsonStore.recordCount;
					foreach (var md in tmpJsonStore.records) {
						retVal.records.Add((MeterData)md);
					}
				}
			}
			return retVal;
		}

		/// <summary>
		/// Get the last meter data for a specific Meter (sorted by AcquisitionDateTime).
		/// </summary>
		/// <param name="context">The context.</param>
		/// <param name="meter">The meter.</param>
		/// <returns></returns>
		public MeterData MeterData_GetLastFromMeter(EnergyAggregatorContext context, Meter meter) {
			if (meter == null) {
				throw new ArgumentNullException("meter");
			}

			MeterData lastMeterData = null;
			GetMeterDataRealAndVirtual(new List<Meter> { meter }, null, null, true);
			if (meter.MDData != null && meter.MDData.Count > 0) {
				
				//Since virtual meter data can represent data rolled up by time, it may not be ordered due to the rollup
				if (meter.IsVirtual)
				{
					meter.MDData = meter.MDData.OrderBy(md => md.AcquisitionDateTime).ToList();
				}
				lastMeterData = (MeterData) meter.MDData.Last();
			}

			return lastMeterData;
		}
		#endregion

		#region Calendar_GetEventsTitle
		/// <summary>
		/// Returns the list of existing events title from a specific calendar category.
		/// </summary>
		/// <param name="context">The context.</param>
		/// <param name="category">The category.</param>
		/// <param name="meter">The meter.</param>
		/// <returns></returns>
		public List<string> Calendar_GetEventsTitle(EnergyAggregatorContext context, CalendarEventCategory category, Meter meter) {
			List<string> retVal = new List<string>();

			#region We go up the Spatial Hierarchy to find all occurences for this category
			List<VizEventOccurence> allOccurrences = new List<VizEventOccurence>();
			List<VizEventOccurence> occurrences = new List<VizEventOccurence>();
			var keyLocation = meter.KeyMeter;
			//We first check if a calendar has been defined on the meter level.
			tenantContainer.Entry.CalendarEventOccurrences.TryGetValue(keyLocation, out occurrences);
			if (CalendarContainsCategory(occurrences, category)) {
				allOccurrences.AddRange(occurrences);
				keyLocation = null;
			}
			else {
				keyLocation = meter.KeyLocation;
			}

			while (keyLocation != null) {
				tenantContainer.Entry.CalendarEventOccurrences.TryGetValue(keyLocation, out occurrences);
				if (CalendarContainsCategory(occurrences, category)) {
					allOccurrences.AddRange(occurrences);
					keyLocation = null;
				}
				else {
					Location location;
					if (tenantContainer.Entry.SpatialHierarchy.TryGetValue(keyLocation, out location)) {
						keyLocation = location.KeyParent;
					}
					else {
						keyLocation = null;
					}
				}
			}
			#endregion


			foreach (var occ in allOccurrences) {
				if (occ.Category == category.LocalId) {
					retVal.Add(occ.Summary);
				}
			}

			retVal = retVal.Distinct().ToList();
			return retVal;
		}
		#endregion

		#region IsCrudNotificationInProgress
		/// <summary>
		/// Determines whether [is crud notification in progress].
		/// </summary>
		/// <param name="context">The context.</param>
		/// <returns>
		///   <c>true</c> if [is crud notification in progress]; otherwise, <c>false</c>.
		/// </returns>
		public bool IsCrudNotificationInProgress(EnergyAggregatorContext context) {
			return tenantContainer.Entry.CrudNotificationInProgress > 0;
		}
		#endregion

		#region Privates
		#region FetchMetersAndLocations
		private void FetchMetersAndLocations(List<int> meterKeys, List<string> locationKeys, out  List<Meter> meters, out  List<Location> locations) {
			var tmpMeters = new ConcurrentBag<Meter>();
			var tmpLocations = new ConcurrentBag<Location>();

			var metersDic = tenantContainer.Entry.Meters;
			var locationsDic = tenantContainer.Entry.SpatialHierarchy;
			Parallel.ForEach(meterKeys, (key) => {
				Meter m = null;
				if (metersDic.TryGetValue(key, out m)) {
					tmpMeters.Add(m.Clone());//.Clone()
				}
			});

			Parallel.ForEach(locationKeys, (key) => {
				Location l = null;
				if (locationsDic.TryGetValue(key, out l))
					tmpLocations.Add(l);
			});
			meters = tmpMeters.ToList();
			locations = tmpLocations.ToList();
		}
		#endregion

		#region GetMeterDataRealAndVirtual
		/// <summary>
		/// Gets the meter data real and virtual and applies calendar filtering.
		/// </summary>
		/// <param name="meters">The meters.</param>
		/// <param name="startDate">The start date.</param>
		/// <param name="endDate">The end date.</param>
		/// <param name="rawData">if set to <c>true</c> we return raw data.(optional)</param>
		/// <param name="fixInterval">if set to <c>true</c> we fix interval data based on the TimeInterval. (optional)</param>
		/// <param name="timeInterval">The time interval.(optional)</param>
		/// <param name="eventCategory">The event category (optional).</param>
		/// <param name="calendarMode">The calendar mode.</param>
		/// <param name="useDataAcquisitionEndPoint">if set to <c>true</c> we use the Meter EndPoint (if defined) to get realtime values instead of going to the historical values.</param>
        /// <param name="errors">The errors.</param>
		/// <returns></returns>
		private List<Meter> GetMeterDataRealAndVirtual(List<Meter> meters, DateTime? startDate, DateTime? endDate, bool rawData = false, bool fixInterval = false, AxisTimeInterval timeInterval = AxisTimeInterval.None, CalendarEventCategory eventCategory = null, ChartCalendarMode calendarMode = ChartCalendarMode.Include, bool useDataAcquisitionEndPoint = false, List<string> errors = null) {
			var sw = new Stopwatch();
			var performances = new Dictionary<string, double>();

			sw.Start();
			var originalMeterList = new List<Meter>(meters);
			meters = LoadMetersUsedInOperation(meters);
			sw.Stop();
			performances.Add("LoadMetersUsedInOperation", sw.Elapsed.TotalSeconds);

			sw.Restart();
			FilterByMeterAndDateRange(meters, startDate.HasValue ? startDate.Value : DateTime.MinValue, endDate.HasValue ? endDate.Value : DateTime.MaxValue, rawData, useDataAcquisitionEndPoint);
			sw.Stop();
			performances.Add("FilterByMeterAndDateRange", sw.Elapsed.TotalSeconds);

			sw.Restart();
			ApplyMeterOperations(meters, errors);
			sw.Stop();
			performances.Add("ApplyMeterOperations", sw.Elapsed.TotalSeconds);

			//The reason we do this is that when you expand the virtual meters you get the real meters they are based on, which you then use to get the meterdata from the real meters that are part of the virtual meters.
			//but after we get the meter data, we dont need them in the meterlist so we return only to the meters (real and virtual) specified in the chart.
			meters = originalMeterList;
			//meters = FilterMeterList(meters, originalMeterList);
			sw.Restart();
			if (fixInterval) {
				// we fix the interval for meters that have End of TimeInterval option. 
				//The virtual meters that have meters with end of time interval options have already been treated.
				FixRollupForMeterIsAcquisitionDateTimeEndInterval(meters, timeInterval);
			}
			sw.Stop();
			performances.Add("FixRollupForMeterIsAcquisitionDateTimeEndInterval", sw.Elapsed.TotalSeconds);

			sw.Restart();
			ApplyPsetHistoricalFactor(meters);
			sw.Stop();
			performances.Add("ApplyPsetHistoricalFactor", sw.Elapsed.TotalSeconds);


			sw.Restart();
			ApplyDegreeDays(meters, rawData);
			sw.Stop();
			performances.Add("ApplyDegreeDays", sw.Elapsed.TotalSeconds);

			sw.Restart();
			ApplyVirtualMeterCalendar(meters);
			sw.Stop();
			performances.Add("ApplyVirtualMeterCalendar", sw.Elapsed.TotalSeconds);

			//move the calendar filtering after the rollup fix
			sw.Restart();
			if (eventCategory != null && startDate.HasValue && endDate.HasValue) {
				//timeZone = (timeZone ?? TimeZoneInfo.FindSystemTimeZoneById("UTC"));
				FilterByCalendar(meters, startDate.Value, endDate.Value, eventCategory, calendarMode);//, timeZone
			}
			sw.Stop();
			performances.Add("FilterByCalendar", sw.Elapsed.TotalSeconds);
			TracePerformances(null, 0, performances, 4);
			return meters;
		}
		#endregion

		#region CrudNotifications
		/// <summary>
		/// Update, delete or create the base business entities that are stored in memory needs.
		/// </summary>
		/// <param name="context">The context.</param>
		/// <param name="notifications">The notifications.</param>
		public void CrudNotification(EnergyAggregatorContext context, List<CrudInformation> notifications) {
			var action = new Action(() => {
				tenantContainer.Entry.CrudNotificationInProgressIncrement();
				try {
					var meterDataCruds = new Dictionary<int, List<CrudInformation>>();
					foreach (var notification in notifications) {
						if (notification.Entity.GetType() == typeof(MeterData)) {
							var keyMeter = MeterHelper.ConvertMeterKeyToInt(((MeterData)notification.Entity).KeyMeter);
							List<CrudInformation> list = null;
							if (!meterDataCruds.TryGetValue(keyMeter, out list)) {
								list = new List<CrudInformation>();
								meterDataCruds.Add(keyMeter, list);
							}
							list.Add(notification);
						}
						else {
							UpdateData(notification.Entity, notification.CrudAction);
						}
					}

					if (meterDataCruds.Count > 0) {
						foreach (var kvp in meterDataCruds) {
							UpdateMeterData(kvp.Key, kvp.Value);
						}
					}
				}
				catch (Exception e) {
					TracingService.Write(e);
				}
				finally {
					tenantContainer.Entry.CrudNotificationInProgressDecrement();
				}
			});
			Helper.StartNewThread(action);
		}

		/// <summary>
		/// Update, delete or create the base business entities that are stored in memory needs.
		/// </summary>
		/// <param name="notification">The notification.</param>
		public void CrudNotification(CrudInformation notification) {
			tenantContainer.Entry.CrudNotificationInProgressIncrement();
			try {
				UpdateData(notification.Entity, notification.CrudAction);
			}
			catch (Exception e) {
				TracingService.Write(e);
			}
			finally {
				tenantContainer.Entry.CrudNotificationInProgressDecrement();
			}
		}

		/// <summary>
		/// Updates the data.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="crudAction">The crud action.</param>
		/// <returns></returns>
		private bool UpdateData(BaseBusinessEntity entity, string crudAction) {
			if (entity == null)
				return false;

			bool updated = false;

			#region MeterData
			var meterData = entity as MeterData;
			if (meterData != null && tenantContainer.Entry.MeterDatas != null) {
				updated = UpdateMeterData(crudAction, meterData);
			}
			#endregion
			#region iCalData
			else {
				var iCalEntity = entity as iCalEntity;
				if (iCalEntity != null) {
					DeleteCalendarEventOccurences(iCalEntity.KeyObject);
					if (crudAction != CrudAction.Destroy) {
						CreateOrUpdateCalendarEventOccurences(iCalEntity.KeyObject, iCalEntity.iCalData, false, null, iCalEntity.TimeZoneId);
					}
				}
			}
			#endregion
			#region Generic

			var algorithm = entity as Algorithm;
			if (algorithm != null) {
				UpdateAlgorithm(algorithm);
			}

			var meter = entity as Meter;
			if (meter != null && tenantContainer.Entry.Meters != null) {
				var key = MeterHelper.ConvertMeterKeyToInt(meter.KeyMeter);
				CrudNotification_UpdateDictionary(key, meter, crudAction, tenantContainer.Entry.Meters);
			}

			var classification = entity as ClassificationItem;
			if (classification != null && tenantContainer.Entry.ClassificationItemHierarchy != null) {
				CrudNotification_UpdateDictionary(classification.KeyClassificationItem, classification, crudAction, tenantContainer.Entry.ClassificationItemHierarchy);
			}

			var ilocationEntity = entity as ILocation;
			if (ilocationEntity != null && tenantContainer.Entry.SpatialHierarchy != null) {
				var location = ilocationEntity.GetLocation();
				CrudNotification_UpdateDictionary(location.KeyLocation, location, crudAction, tenantContainer.Entry.SpatialHierarchy);
			}

			var locationEntity = entity as Location;
			if (locationEntity  != null && tenantContainer.Entry.SpatialHierarchy != null) {
				CrudNotification_UpdateDictionary(locationEntity.KeyLocation, locationEntity, crudAction, tenantContainer.Entry.SpatialHierarchy);
			}

			var historical = entity as PsetAttributeHistorical;
			if (historical != null && tenantContainer.Entry.HistoricalData != null) {
				UpdatePsetAttributeHistorical(crudAction, historical);
			}

			var localizationResource = entity as LocalizationResource;
			if (localizationResource != null) {
				UpdateLocalizationResource(crudAction, localizationResource);
			}

			#endregion

			return updated;
		}

		private void UpdateAlgorithm(Algorithm algorithm) {
			// Remove the algorithm if it exists, it will be recreated on usage.
			AlgorithmInstance<IAnalyticsExtensibility> removed;
			tenantContainer.Entry.AlgorithmInstances.TryRemove(algorithm.KeyAlgorithm, out removed);

			if (removed != null) {
				removed.Dispose();
			}
		}

		/// <summary>
		/// Updates the localization resource cache. Unlike the front-end behavior, when a resource is added the cache is modified
		/// directly instead of being re-populated from a a database query.
		/// </summary>
		/// <param name="crudAction">The crud action.</param>
		/// <param name="localizationResource">The localization resource.</param>
		private void UpdateLocalizationResource(string crudAction, LocalizationResource localizationResource) {
			//Debug.WriteLine(string.Format("UpdateLocalizationResource: crudAction={3} key={0}, value={1}, culture={2}", localizationResource.Key, localizationResource.Value, localizationResource.Culture, crudAction));
			switch (crudAction) {
				case CrudAction.Create:
				case CrudAction.Update:
					LocalizationService.AddLocalizedTextToCache(localizationResource.Key, localizationResource.Culture, localizationResource.Value);
					break;

				case CrudAction.Destroy:
					LocalizationService.RemoveLocalizedTextFromCache(localizationResource.Key, localizationResource.Culture);
					break;
			}
		}

		/// <summary>
		/// Update the dictionnary with the crud the notification.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <typeparam name="K"></typeparam>
		/// <param name="key">The key.</param>
		/// <param name="entity">The entity.</param>
		/// <param name="crudAction">The crud action.</param>
		/// <param name="dictionary">The dictionary.</param>
		private void CrudNotification_UpdateDictionary<T, K>(T key, K entity, string crudAction, IDictionary<T, K> dictionary) where K : BaseBusinessEntity {
			switch (crudAction) {
				case CrudAction.Create:
				case CrudAction.Update:
					// This will overwrite possible existing value.
					dictionary[key] = entity;
					break;

				case CrudAction.Destroy:
					dictionary.Remove(key);
					break;
			}

		}

		/// <summary>
		/// Updates the meter data.
		/// </summary>
		/// <param name="keyMeter">The key meter.</param>
		/// <param name="list">The list.</param>
		/// <returns></returns>
		private static bool UpdateMeterData(int keyMeter, List<CrudInformation> list) {
			var data = tenantContainer.Entry.MeterDatas.GetOrAdd(keyMeter, i => new Dictionary<DateTime, MD>());
			foreach (var notification in list) {
				var meterData = notification.Entity as MeterData;
				if (meterData != null) {
					if (!(meterData.AcquisitionDateTime >= tenantContainer.Entry.StartDate && meterData.AcquisitionDateTime < tenantContainer.Entry.EndDate)) {
						break;
					}
					meterData.AcquisitionDateTime = meterData.AcquisitionDateTime.ToUniversalTime();
					MD md = (MD)meterData;
					// The lock is at the meter level. Other meters can still be read and updated at the same time.
					lock (data) {
						switch (notification.CrudAction) {
							case CrudAction.Create:
								data.Remove(md.AcquisitionDateTime);
								data.Add(md.AcquisitionDateTime, md);
								break;

							case CrudAction.Update:
								RemovePreviousMeterDataItem(md, data);
								data.Add(md.AcquisitionDateTime, md);
								break;

							case CrudAction.Destroy:
								RemovePreviousMeterDataItem(md, data);
								break;
						}
					}
				}
			}

			return true;
		}


		/// <summary>
		/// Updates the meter data.
		/// </summary>
		/// <param name="crudAction">The crud action.</param>
		/// <param name="meterData">The meter data.</param>
		/// <returns></returns>
		private static bool UpdateMeterData(string crudAction, MeterData meterData) {
			if (!(meterData.AcquisitionDateTime >= tenantContainer.Entry.StartDate && meterData.AcquisitionDateTime < tenantContainer.Entry.EndDate)) {
				return false;
			}

			meterData.AcquisitionDateTime = meterData.AcquisitionDateTime.ToUniversalTime();

			int keyMeter = MeterHelper.ConvertMeterKeyToInt(meterData.KeyMeter);
			var data = tenantContainer.Entry.MeterDatas.GetOrAdd(keyMeter, i => new Dictionary<DateTime, MD>());
			MD md = (MD)meterData;

			// The lock is at the meter level. Other meters can still be read and updated at the same time.
			lock (data) {
				switch (crudAction) {
					case CrudAction.Create:
						data.Remove(md.AcquisitionDateTime);
						data.Add(md.AcquisitionDateTime, md);
						break;

					case CrudAction.Update:
						RemovePreviousMeterDataItem(md, data);
						data.Add(md.AcquisitionDateTime, md);
						break;

					case CrudAction.Destroy:
						RemovePreviousMeterDataItem(md, data);
						break;
				}
			}
			tenantContainer.Entry.MeterDatas[keyMeter] = data;

			return true;
		}

		/// <summary>
		/// Removes the previous meter data item.
		/// </summary>
		/// <param name="meterData">The meter data.</param>
		/// <param name="data">The data.</param>
		private static void RemovePreviousMeterDataItem(MD meterData, IDictionary<DateTime, MD> data) {
			MD existingItem;
			bool foundPreviousItem = false;

			//Here we remove old point that might have the same AcquisitionDateTime
			if (data.TryGetValue(meterData.AcquisitionDateTime, out existingItem)) {
				data.Remove(meterData.AcquisitionDateTime);
				foundPreviousItem = existingItem.KeyMeterData.Equals(meterData.KeyMeterData);
			}

			if (!foundPreviousItem) {
				// This is a case in which the item has been updated and has been set a new AcquisitionDateTime.
				// We need to find where it was.
				var originalEntry = data.FirstOrDefault(kvp => kvp.Value.KeyMeterData == meterData.KeyMeterData);

				if (originalEntry.Value != null) {
					data.Remove(originalEntry.Key);
				}
			}
		}

		/// <summary>
		/// Updates the pset attribute historical.
		/// </summary>
		/// <param name="crudAction">The crud action.</param>
		/// <param name="pset">The pset.</param>
		/// <returns></returns>
		private static bool UpdatePsetAttributeHistorical(string crudAction, PsetAttributeHistorical pset) {
			var dictionary = tenantContainer.Entry.HistoricalData;
			switch (crudAction) {
				case CrudAction.Create:
				case CrudAction.Update:
					RemovePreviousPsetAttributeHistorical(pset, dictionary);
					dictionary[pset.KeyPsetAttributeHistorical] = pset;
					break;

				case CrudAction.Destroy:
					dictionary.TryRemove(pset.KeyPsetAttributeHistorical, out pset);
					break;
			}
			return true;
		}

		/// <summary>
		/// Removes the previous pset attribute historical.
		/// </summary>
		/// <param name="pset">The pset.</param>
		/// <param name="data">The data.</param>
		private static void RemovePreviousPsetAttributeHistorical(PsetAttributeHistorical pset, IDictionary<string, PsetAttributeHistorical> data) {
			var toRemove = data.Values.Where(p => (p.KeyPsetAttributeHistorical == pset.KeyPsetAttributeHistorical) || (p.KeyObject == pset.KeyObject && p.KeyPropertySingleValue == pset.KeyPropertySingleValue && p.AttributeAcquisitionDateTime == pset.AttributeAcquisitionDateTime)).ToList();
			if (toRemove != null && toRemove.Count > 0) {
				toRemove.ForEach(p => data.Remove(p.KeyPsetAttributeHistorical));
			}
		}

		/// <summary>
		/// Updates the classification item if the LongPath is empty
		/// </summary>
		/// <param name="item">The item.</param>
		private void UpdateClassificationItem(ClassificationItem item) {
			if (item == null)
				return;
			if (String.IsNullOrEmpty(item.LongPath)) {
				IEnergyBusinessLayer energyBusinessLayer = Helper.Resolve<IEnergyBusinessLayer>();
				var newitem = energyBusinessLayer.ClassificationItem_GetItem(item.KeyClassificationItem);
				//if we find the entity in the db we update it
				CrudNotification(newitem != null ? new CrudInformation { Entity = newitem, CrudAction = CrudAction.Update } : new CrudInformation { Entity = item, CrudAction = CrudAction.Destroy });
			}
		}

		/// <summary>
		/// Updates the location and site max level.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <param name="siteLevel">The site level.</param>
		/// <returns></returns>
		private bool UpdateLocationAndSiteMaxLevel(Location item, out int siteLevel) {
			siteLevel = -1;
			if (item == null)
				return false;
			if (item.TypeName == HierarchySpatialTypeName.IfcSite) {
				siteLevel = item.Level;
			}
			if (String.IsNullOrEmpty(item.LongPath)) {
				//var EnergyBusinessLayer = new EnergyBusinessLayer();
				var energyBusinessLayer = Helper.Resolve<IEnergyBusinessLayer>();
				var newitem = energyBusinessLayer.Location_GetItem(item.KeyLocation);
				//if we find the entity in the db we update it
				CrudNotification(newitem != null ? new CrudInformation { Entity = newitem, CrudAction = CrudAction.Update } : new CrudInformation { Entity = item, CrudAction = CrudAction.Destroy });
				return true;
			}
			return false;
		}

		/// <summary>
		/// Checks the classification items and location long pathes.
		/// </summary>
		private void CheckEntityLongPath() {
			foreach (ClassificationItem item in tenantContainer.Entry.ClassificationItemHierarchy.Values) {
				UpdateClassificationItem(item);
			}
			//var updateSiteMaxLevel = false;
			var realSiteMaxLevel = 0;
			foreach (Location item in tenantContainer.Entry.SpatialHierarchy.Values) {
				int siteLevel;
				UpdateLocationAndSiteMaxLevel(item, out siteLevel);
				realSiteMaxLevel = Math.Max(siteLevel, realSiteMaxLevel);
			}
			tenantContainer.Entry.SiteMaxLevel = realSiteMaxLevel;
		}
		#endregion

		#region Init Data
		/// <summary>
		/// Determines whether this instance is initialized with Data.
		/// </summary>
		/// <returns>
		///   <c>true</c> if this instance is initialized; otherwise, <c>false</c>.
		/// </returns>
		public bool IsInitialized(EnergyAggregatorContext context) {
			return tenantContainer.Entry.IsInitialized;
		}

		/// <summary>
		/// Set the IsInitialized property
		/// </summary>
		/// <param name="isInitialized"></param>
		public void SetIsInitialized(bool isInitialized) {
			tenantContainer.Entry.IsInitialized = isInitialized;
		}

		/// <summary>
		/// Determines whether the Aggregator is loading meter data.
		/// </summary>
		public bool IsLoadingMeterData(EnergyAggregatorContext context) {
			return tenantContainer.Entry.LoadingMeterData;
		}

		/// <summary>
		/// Gets the meter data count.
		/// </summary>
		/// <returns></returns>
		public int GetMeterDataCount(EnergyAggregatorContext context) {
			var retVal = 0;
			if (tenantContainer.Entry.MeterDatas != null) {
				// From MSDN: The enumerator returned from the dictionary is safe to use concurrently with reads and writes to the dictionary, 
				// however it does not represent a moment-in-time snapshot of the dictionary. 
				// The contents exposed through the enumerator may contain modifications made to the dictionary after GetEnumerator was called.
				try {
					// This is a bit dangerous as the Value needs to be locked in order to be counted.
					// In order to increase performance, we don't lock it here, since it would mean we lock every meter.
					retVal = tenantContainer.Entry.MeterDatas.GetMeterDataCount();
				}
				catch {
					retVal = -1; // indicate an error.
				}
			}
			return retVal;
		}

		/// <summary>
		/// Gets the calendar occurrence count.
		/// </summary>
		/// <returns></returns>
		private int GetCalendarOccurrencesCount() {
			var retVal = 0;
			if (tenantContainer.Entry.CalendarEventOccurrences != null) {
				// From MSDN: The enumerator returned from the dictionary is safe to use concurrently with reads and writes to the dictionary, 
				// however it does not represent a moment-in-time snapshot of the dictionary. 
				// The contents exposed through the enumerator may contain modifications made to the dictionary after GetEnumerator was called.
				try {
					// This is a bit dangerous as the Value needs to be locked in order to be counted.
					// In order to increase performance, we don't lock it here, since it would mean we lock every meter.
					retVal = tenantContainer.Entry.CalendarEventOccurrences.Values.Select(x => x.Count).Sum();
				}
				catch {
					retVal = -1; // indicate an error.
				}
			}
			return retVal;
		}

		/// <summary>
		/// Check if the list of MeterData is empty and loads all the MeterData into memory.
		/// </summary>
		private void InitMeterData() {
			//Debugger.Launch();
			tenantContainer.Entry.LoadingMeterData = true;
			try {

				tenantContainer.Entry.MeterDatas = MeterDataService.GetStorage();
				tenantContainer.Entry.MeterDatas.Init();

				if (tenantContainer.Entry.MeterDatas.GetMeterDataCount() == 0) {
					var energyBusinessLayer = Helper.Resolve<IEnergyBusinessLayer>();
					//PagingParameter paging = new PagingParameter { start = 0, limit = 0, sort = "AcquisitionDateTime" };

					if (tenantContainer.Entry.MeterDatas.GetType().Name == "InMemoryMeterDataStorage") {
						ConcurrentDictionary<int, SortedList<DateTime, MD>> meterDatas = energyBusinessLayer.MeterData_GetAllByDateRange(tenantContainer.Entry.StartDate, tenantContainer.Entry.EndDate);
						tenantContainer.Entry.MeterDatas.AddRange(meterDatas);
					}
					else {
						var meters = tenantContainer.Entry.Meters.Values.ToList();
						var storage = tenantContainer.Entry.MeterDatas;
						var startDate = tenantContainer.Entry.StartDate;
						var endDate = tenantContainer.Entry.EndDate;

						int index = 0;
						int count = 10;

						while (index < meters.Count) {
							var keys = meters.GetRange(index, Math.Min(count, meters.Count - index)).Select(m => m.KeyMeter).ToArray();
							var meterDatas = energyBusinessLayer.MeterData_GetAllByDateRange(startDate, endDate, keys);
							storage.AddRange(meterDatas);
							index += count;
						}
					}
				}
			}
			catch (SystemException e) {
				TracingService.Write(e);
			}
			tenantContainer.Entry.LoadingMeterData = false;
			//EnergyAggregatorHelper.CleanupMemory();
		}
		#endregion

		#region Calendar Event Init
		/// <summary>
		/// Inits the calendar event occurences.
		/// </summary>
		/// <param name="calendars">The calendars.</param>
		private void InitCalendarEventOccurences(Dictionary<string, Tuple<string, string>> calendars) {
			tenantContainer.Entry.CalendarEventOccurrences = new ConcurrentDictionary<string, List<VizEventOccurence>>();
			try {
				//Getting all the calendars up front
				DateTime startDate = tenantContainer.Entry.StartDate.AddDays(-1);
				DateTime endDate = tenantContainer.Entry.EndDate.AddDays(-1);

				if (startDate > endDate) {
					var tempDate = endDate;
					endDate = startDate;
					startDate = tempDate;
				}

				var validCals = calendars.Where(cal => ((!String.IsNullOrEmpty(cal.Value.Item1)) && ((tenantContainer.Entry.SpatialHierarchy.ContainsKey(cal.Key) || tenantContainer.Entry.Meters.ContainsKey(MeterHelper.ConvertMeterKeyToInt(cal.Key)))))).ToList();

				var calendarOccurencesBroker = Helper.Resolve<CalendarOccurrencesBroker>();
				//Preloading all exisitng calendarOccurences so we dont do n calls to the DB
				List<CalendarOccurrences> allCalendarOccurences = calendarOccurencesBroker.GetAll();
				var occurranceList = validCals.Select((cal) => new KeyValuePair<string, IList<VizEventOccurence>>(cal.Key, GetOccurrences(cal.Key, cal.Value.Item1, startDate, endDate, cal.Value.Item2, allCalendarOccurences))).ToList();
				foreach (var keyValuePair in occurranceList) {
					CreateOrUpdateCalendarEventOccurences(keyValuePair.Key, calendars[keyValuePair.Key].Item1, true, keyValuePair.Value, calendars[keyValuePair.Key].Item2);
				}
			}
			catch (Exception e) {
				TracingService.Write(e);
			}
		}

		/// <summary>
		/// Inits the or update calendar event.
		/// </summary>
		/// <param name="keyObject">The key object.</param>
		/// <param name="iCalData">The i cal data.</param>
		/// <param name="isInitialLoad">if set to <c>true</c> [is initial load].</param>
		/// <param name="occurrences">The occurrences.</param>
		/// <param name="timeZoneId">The time zone id.</param>
		private void CreateOrUpdateCalendarEventOccurences(string keyObject, string iCalData, bool isInitialLoad, IList<VizEventOccurence> occurrences, string timeZoneId) {
			// init of the tenantContainer.Entry.CalendarEventOccurrences dictionary
			List<VizEventOccurence> eventOccurrences;
			tenantContainer.Entry.CalendarEventOccurrences.TryGetValue(keyObject, out eventOccurrences);
			if ((!isInitialLoad) || (eventOccurrences == null)) {
				if (occurrences == null)
					occurrences = GetOccurrences(keyObject, iCalData, tenantContainer.Entry.StartDate.AddDays(-1), tenantContainer.Entry.EndDate.AddDays(1), timeZoneId);
				if (occurrences.Count > 0) {
					List<VizEventOccurence> removed;
					tenantContainer.Entry.CalendarEventOccurrences.TryRemove(keyObject, out removed);
					tenantContainer.Entry.CalendarEventOccurrences[keyObject] = occurrences.ToList();
				}
			}
		}


		/// <summary>
		/// Gets the occurrences.
		/// </summary>
		/// <param name="keyCalendar">The key calendar.</param>
		/// <param name="iCalData">The icaldata.</param>
		/// <param name="startDate">The start date.</param>
		/// <param name="endDate">The end date.</param>
		/// <param name="timeZoneId">The time zone id.</param>
		/// <param name="occurrencesesFromDB">The occurenceses from DB. They can be preloaded for all the calendars from the outside and passed in. Or this paramater can be left null and the method will get the occurrences for each calendar</param>
		/// <returns></returns>
		private static IList<VizEventOccurence> GetOccurrences(string keyCalendar, string iCalData, DateTime startDate, DateTime endDate, string timeZoneId, List<CalendarOccurrences> occurrencesesFromDB = null) {
			IDateTime actualStartDate = CalendarHelper.Deserialize(iCalData).Events.Select(ev => ev.Start).Min();
			if (actualStartDate.Value > startDate) {
				startDate = actualStartDate.Value;
			}
			if (occurrencesesFromDB == null) {
				occurrencesesFromDB = LoadOccurrencesesForCalendar(keyCalendar);
			}

			IList<VizEventOccurence> vizEventOccurences = GetVizEventOccurencesFromPreloadedOccurrences(keyCalendar, iCalData, startDate, endDate, timeZoneId, occurrencesesFromDB);
			if (vizEventOccurences != null)
				return vizEventOccurences;

			vizEventOccurences = ComputeOccurrencesParallel(iCalData, startDate, endDate, timeZoneId);
			SaveOccurencesToDB(keyCalendar, iCalData, startDate, endDate, timeZoneId, vizEventOccurences);
			return vizEventOccurences;

		}

		/// <summary>
		/// Gets the occurrenceses the from db.
		/// </summary>
		/// <param name="keyCalendar">The key calendar.</param>
		/// <returns></returns>
		private static List<CalendarOccurrences> LoadOccurrencesesForCalendar(string keyCalendar) {
			var broker = Helper.CreateInstance<CalendarOccurrencesBroker>();
			var filters = new List<GridFilter> { new GridFilter { field = "KeyCalendar", data = new GridData { type = "string", comparison = "eq", value = keyCalendar } } };
			var pagingParameter = new PagingParameter { filters = filters };
			int total;
			var occurrencesesFromDB = broker.GetAll(pagingParameter, PagingLocation.Database, out total);
			return occurrencesesFromDB;
		}

		/// <summary>
		/// Saves the occurences to DB.
		/// </summary>
		/// <param name="keyCalendar">The key calendar.</param>
		/// <param name="iCalData"> </param>
		/// <param name="startDate">The start date.</param>
		/// <param name="endDate">The end date.</param>
		/// <param name="timeZoneId">The time zone id.</param>
		/// <param name="occurences">The occurences.</param>
		private static void SaveOccurencesToDB(string keyCalendar, string iCalData, DateTime startDate, DateTime endDate, string timeZoneId, IList<VizEventOccurence> occurences) {
			var calendarOccurrences = new CalendarOccurrences() {
				CalendarOccurrencesData = occurences.ToByteArray(),
				TimeZone = timeZoneId,
				EndDate = endDate,
				StartDate = startDate,
				KeyCalendar = keyCalendar,
				iCalData = iCalData
			};
			var broker = Helper.Resolve<CalendarOccurrencesBroker>();
			//calendarOccurrences.CalendarOccurrencesData = CompressionHelper.Compress(calendarOccurrences.CalendarOccurrencesData);
			broker.Save(calendarOccurrences);
		}

		/// <summary>
		/// Gets the occurrences from the provided cache, if it exists.
		/// </summary>
		/// <param name="keyCalendar">The key to the calendar.</param>
		/// <param name="iCalData"> </param>
		/// <param name="startDate">The start date.</param>
		/// <param name="endDate">The end date.</param>
		/// <param name="timeZoneId">The time zone id.</param>
		/// <param name="occurrencesFromCache">The occurrences from cache.</param>
		/// <returns>
		/// The occurrences if they are in the db, otherwise it returns null
		/// </returns>
		private static List<VizEventOccurence> GetVizEventOccurencesFromPreloadedOccurrences(string keyCalendar, string iCalData, DateTime startDate, DateTime endDate, string timeZoneId, List<CalendarOccurrences> occurrencesFromCache) {
			CalendarOccurrences calendarOccurrences = occurrencesFromCache.FirstOrDefault(x => x.KeyCalendar == keyCalendar &&
																						   x.StartDate.ToUniversalTime() <= startDate.ToUniversalTime() &&
																						   x.EndDate.ToUniversalTime() >= endDate.ToUniversalTime() &&
																						   x.TimeZone == timeZoneId &&
																						   x.iCalData == iCalData);

			if (calendarOccurrences == null) {
				return null;
			}

			return calendarOccurrences.CalendarOccurrencesData.Deserialize<List<VizEventOccurence>>();
		}


		/// <summary>
		/// Computes the occurrences parallely.
		/// </summary>
		/// <param name="iCalData">The i cal data.</param>
		/// <param name="startDate">The start date.</param>
		/// <param name="endDate">The end date.</param>
		/// <param name="timeZoneId">The time zone id.</param>
		/// <returns></returns>
		private static IList<VizEventOccurence> ComputeOccurrencesParallel(string iCalData, DateTime startDate, DateTime endDate, string timeZoneId) {
			int rangeEnd = Math.Abs(((startDate.Year - endDate.Year) * 12) + startDate.Month - endDate.Month);

			iCalendar[] calendars = new iCalendar[rangeEnd + 1];

			List<int> range = new List<int>(rangeEnd);
			for (int i = 0; i < rangeEnd; i++) {
				range.Add(i);
				calendars[i] = CalendarHelper.Deserialize(iCalData);
			}

			var occurances = range.AsParallel().Select(i => calendars[i].GetOccurrences(startDate.AddMonths(i), startDate.AddMonths(i + 1))).ToList();
			IList<Occurrence> results = new List<Occurrence>(occurances.Count);
			foreach (var occuranceList in occurances) {
				results.AddRange(occuranceList);
			}
			results = results.Distinct().ToList();

			var retVal = new List<VizEventOccurence>(results.Count);
			foreach (var occ in results) {
				var ev = (Event)occ.Source;
				DateTime evStartDate, evEndDate;
				CalendarHelper.DateTimeFromiCal(occ, CalendarHelper.GetTimeZoneFromId(timeZoneId), out evStartDate, out evEndDate);
				var vizEvent = new VizEventOccurence {
					Category = ev.Category(),
					Factor = ev.GetFactor(),
					StartDate = evStartDate,
					EndDate = evEndDate,
					Summary = ev.Summary
				};
				if (ev.IsAllDay)
					vizEvent.EndDate = vizEvent.EndDate.AddSeconds(1);
				retVal.Add(vizEvent);
			}
			return retVal;
		}

		/// <summary>
		/// Inits the or update calendar event.
		/// </summary>
		/// <param name="keyObject">The key object.</param>
		private static void DeleteCalendarEventOccurences(string keyObject) {
			List<VizEventOccurence> removedOccurrences;
			tenantContainer.Entry.CalendarEventOccurrences.TryRemove(keyObject, out removedOccurrences);
		}
		#endregion

		#region FilterByMeterAndDateRange
		/// <summary>
		/// Associate the list of Meter with their corresponding MeterData beetween the specified StartDate and EndDate.
		/// </summary>
		/// <param name="meters">The meters.</param>
		/// <param name="startDate">The start date.</param>
		/// <param name="endDate">The end date.</param>
		/// <param name="rawData">if set to <c>true</c> [raw data].</param>
		/// <param name="useDataAcquisitionEndPoint">if set to <c>true</c> we use the Meter EndPoint (if defined) to get realtime values instead of going to the historical values.</param>
		private void FilterByMeterAndDateRange(List<Meter> meters, DateTime startDate, DateTime endDate, bool rawData = false, bool useDataAcquisitionEndPoint = false) {
			var startDateUniversal = startDate.ToUniversalTime();
			var endDateUniversal = endDate.ToUniversalTime();
			var energyBusinessLayer = Helper.Resolve<IEnergyBusinessLayer>();

			#region Meter not virtual list
			var metersList = new List<Meter>();
			foreach (var meter in meters) {
				//we reset the IsMeterDataRollupFixed flag for each meter when we fetch the data from memory.
				meter.IsMeterDataRollupFixed = false;
				if (meter.IsVirtual == false)
					metersList.Add(meter);
			}
			#endregion

			var bucketNumber = m_ProcessorCount;
			var bucketSize = metersList.Count / bucketNumber + 1;
			var MeterDatas = tenantContainer.Entry.MeterDatas;
			var applicationName = ContextHelper.ApplicationName;

			var swGetData = new Stopwatch();
			var swTransformData = new Stopwatch();
			Parallel.For(0, bucketNumber, i => {
				for (var j = bucketSize * i; j < Math.Min(metersList.Count, bucketSize * (i + 1)); j++) {
					ContextHelper.ApplicationName = applicationName;

					swGetData.Start();
					Meter m = metersList[j];
					int keyMeter = MeterHelper.ConvertMeterKeyToInt(m.KeyMeter);
					List<MD> meterDataList = null;
					#region Real Time DataAcquisitionEndpoint
					if (useDataAcquisitionEndPoint && string.IsNullOrEmpty(m.KeyEndpoint) == false && string.IsNullOrEmpty(m.EndpointType) == false) {
						meterDataList = new List<MD>();
						var values = energyBusinessLayer.DataAcquisitionEndpoint_Process(m.EndpointType, m.KeyEndpoint);
						values.ForEach(value => {
							if (double.IsNaN(value.Value) == false && value.AcquisitionDateTime >= startDateUniversal && value.AcquisitionDateTime <= endDateUniversal) {
								meterDataList.Add(new MD {
									AcquisitionDateTime = value.AcquisitionDateTime,
									KeyMeter = keyMeter,
									Value = value.Value
								});
							}
						});
					}
					#endregion
					#region Get Data From EA (Memory)
					else {
						var includeStartDate = true;
						var includeEndDate = false;
						if (m.IsAcquisitionDateTimeEndInterval && !m.IsAccumulated) {
							includeStartDate = false; includeEndDate = true;
						}
						else if (m.IsAcquisitionDateTimeEndInterval && m.IsAccumulated) {
							includeStartDate = true; includeEndDate = true;
						}
						else if (!m.IsAcquisitionDateTimeEndInterval && m.IsAccumulated) {
							includeStartDate = true; includeEndDate = true;
						}

						//NOTE: This is if we want to make the change that accumulated meters will take an additional data point outside the given dates.
						//MeterDatas.TryGetValueAsList(keyMeter, out meterDataList, startDateUniversal, includeStartDate, endDateUniversal, includeEndDate,m);
						MeterDatas.TryGetValueAsList(keyMeter, out meterDataList, startDateUniversal, includeStartDate, endDateUniversal, includeEndDate);
					}
					#endregion
					if (meterDataList != null && meterDataList.Count > 0) {
						#region Old
						// The lock is at the meter level. Other meters can still be read and updated at the same time.
						//lock (meterDataDic) {
						//    var meterData = meterDataDic.Values.ToList();
						//    if (m.IsAcquisitionDateTimeEndInterval && !m.IsAccumulated) {
						//        m.MDData = FilterItems(meterData, md => md.AcquisitionDateTime > startDateUniversal && md.AcquisitionDateTime <= endDateUniversal);
						//    }
						//    else if (m.IsAcquisitionDateTimeEndInterval && m.IsAccumulated) {
						//        m.MDData = FilterItems(meterData, md => md.AcquisitionDateTime >= startDateUniversal && md.AcquisitionDateTime <= endDateUniversal);
						//    }
						//    else if (!m.IsAcquisitionDateTimeEndInterval && m.IsAccumulated) {
						//        m.MDData = FilterItems(meterData, md => md.AcquisitionDateTime >= startDateUniversal && md.AcquisitionDateTime <= endDateUniversal);
						//    }
						//    else {
						//        m.MDData = FilterItems(meterData, md => md.AcquisitionDateTime >= startDateUniversal && md.AcquisitionDateTime < endDateUniversal);
						//    }
						//}
						#endregion
						m.MDData = meterDataList;
					}
					else
						m.MDData = new List<MD>();
					swGetData.Stop();

					swTransformData.Start();
					//Calculate accumulated
					if (!rawData && m.IsAccumulated) {
						m.MDData = CalculateAccumulated(m.MDData, m.RolloverValue, m.IsAcquisitionDateTimeEndInterval);
					}
					//Apply Meter Factor and Offset
					if (!rawData && ((m.Factor != 1) || (m.Offset.HasValue && m.Offset.Value != 0))) {
						m.MDData = Clone(m.MDData);
						if (m.Factor != 1)
							ApplyFactor(m.Factor, m.MDData);
						if (m.Offset.HasValue && m.Offset.Value != 0)
							ApplyOffset(m.Offset.Value, m.MDData);
					}
					swTransformData.Stop();
				}
			});

			Debug.WriteLine(swGetData.ElapsedMilliseconds);
			Debug.WriteLine(swTransformData.ElapsedMilliseconds);

		}
		#endregion

		#region FixRollupForMeterIsAcquisitionDateTimeEndInterval

		/// <summary>
		/// Fixes the rollup for meter that have the IsAcquisitionDateTimeEndInterval set to true, we need to remove  1 second to fix the daily (and above)
		/// aggregations.
		/// </summary>
		/// <param name="meters">The meters.</param>
		/// <param name="timeinterval">The timeinterval.</param>
		private void FixRollupForMeterIsAcquisitionDateTimeEndInterval(List<Meter> meters, AxisTimeInterval timeinterval) {
			switch (timeinterval) {
				case AxisTimeInterval.Global:
				case AxisTimeInterval.Years:
				case AxisTimeInterval.Quarters:
				case AxisTimeInterval.Semesters:
				case AxisTimeInterval.Months:
				case AxisTimeInterval.Weeks:
				case AxisTimeInterval.Days:
				case AxisTimeInterval.Hours:
				case AxisTimeInterval.Minutes:
				case AxisTimeInterval.Seconds:
					foreach (var meter in meters) {
						if (meter.IsAcquisitionDateTimeEndInterval && !meter.IsMeterDataRollupFixed && meter.IsVirtual == false) {
							var newData = new List<MD>();
							if (meter.MDData != null) {
								newData = new List<MD>(meter.MDData.Count);
								foreach (var md in meter.MDData) {
									newData.Add(new MD() {
										KeyMeter = md.KeyMeter,
										Value = md.Value,
										AcquisitionDateTime = md.AcquisitionDateTime.AddSeconds(-1),
										Validity = md.Validity
									});
								};
							}
							meter.MDData = newData;
							meter.IsMeterDataRollupFixed = true;
						}
					};
					break;
			}
		}

	  
		#endregion

		#region HistoricalPsetCreateMeters
		/// <summary>
		/// Create "dynamic" meters to handle historical pset data.
		/// </summary>
		/// <param name="chart">The chart.</param>
		/// <param name="locations">The locations attached to this chart.</param>
		/// <param name="meters">The meters collection we are going to update.</param>
		/// <param name="startDate">The start date.</param>
		/// <param name="endDate">The end date.</param>
		/// <param name="timeInterval">The time interval.</param>
		/// <param name="locationFilter">The location filter.</param>
		private void HistoricalPsetCreateMeters(Chart chart, List<Location> locations, List<Meter> meters, DateTime startDate, DateTime endDate, AxisTimeInterval timeInterval, List<Filter> locationFilter) {
			var retVal = new List<Meter>();
			if (chart.HistoricalPsets == null || chart.HistoricalPsets.Count <= 0)
				return;
			var psets = new List<string>();
			foreach (var location in locations) {
				//We fetch the full hierarchy of the current location.
				List<Location> allLocationInHierarchy =
					tenantContainer.Entry.SpatialHierarchy.Values.Where(
						l =>l != null && ((!string.IsNullOrEmpty(l.KeyLocationPath) && l.KeyLocationPath.StartsWith(location.KeyLocationPath)) //get all children
											||
											(location.KeyLocationPath.Split(('/')).Select(p=>p.Trim()).Where(p=>!String.IsNullOrWhiteSpace(p)).Contains(l.KeyLocation))) //get all parents
											).ToList();

				allLocationInHierarchy = FilterLocations(allLocationInHierarchy, locationFilter);
				//For each pset we see if there is some data
				foreach (var pset in chart.HistoricalPsets) {
					foreach (var loc in allLocationInHierarchy) {
						//we make sure we only process a pset once.
						if (!psets.Contains(loc.KeyLocation + pset.KeyPropertySingleValue)) {
							psets.Add(loc.KeyLocation + pset.KeyPropertySingleValue);
							//we create a temporary meter
							var meter = new Meter() {
								Name = chart.Localisation == ChartLocalisation.ByMeter ? loc.Name : pset.AttributeName,
								KeyMeter = pset.KeyPropertySingleValue,
								KeyLocation = loc.KeyLocation,
								LocationLongPath = loc.LongPath,
								LocationLevel = loc.Level,
								LocationName = loc.Name,
								LocationShortPath = loc.ShortPath,
								LocationTypeName = loc.TypeName,
								GroupOperation = pset.GroupOperation,
								MDData = new List<MD>(),
								IsHistoricalPset = true,
								//Since this field is part of the
								ClassificationItemTitle = string.Format("{0}-{1}", pset.PsetName, pset.AttributeName),
								ClassificationItemLongPath = pset.AttributeLabel
							};
							//we check if there is data
							var psetsData = new List<string>();
							foreach (var data in tenantContainer.Entry.HistoricalData.Values) {
								if (data.KeyObject == loc.KeyLocation && data.KeyPropertySingleValue == pset.KeyPropertySingleValue) {
									if (psetsData.Contains(data.KeyPsetAttributeHistorical) == false) {
										meter.MDData.Add(new MD {
											AcquisitionDateTime = data.AttributeAcquisitionDateTime,
											Value = data.AttributeValue
										});
										//we make sure we add the data only once per "Meter".
										psetsData.Add(data.KeyPsetAttributeHistorical);
									}
								}
							}

							if (meter.MDData.Count > 0) {
								if (pset.FillMissingValues) {
									MeterHelper.FillMissingValues(meter, timeInterval, startDate, endDate, chart.GetTimeZone());
								}
								//After filling the missing values we can now filter by the Chart StartDate and EndDate
								var universalStartDate = startDate.ToUniversalTime();
								var universalEndDate = endDate.ToUniversalTime();
								meter.MDData = meter.MDData.Where(md => md.AcquisitionDateTime >= universalStartDate && md.AcquisitionDateTime < universalEndDate).ToList();
								retVal.Add(meter);
							}
						}
					}
				}
			}
			if (retVal.Count > 0) {
				FilterByCalendar(retVal, startDate, endDate, chart.CalendarEventCategory, chart.CalendarMode); //, chart.GetTimeZone()
			}
			meters.AddRange(retVal);
		}

		#endregion

		#region FilterLocations
		/// <summary>
		/// Filters the locations.
		/// </summary>
		/// <param name="locations">The locations.</param>
		/// <param name="filters">The filters.</param>
		/// <returns></returns>
		private List<Location> FilterLocations(List<Location> locations, List<Filter> filters) {

			if (filters == null || filters.Count == 0)
				return locations;

			var retVal = new List<Location>();

			foreach (var loc in locations) {
				var c = (from f in filters
						 where f.Key == loc.KeyLocation && f.Type != FilterType.Ascendant
						 select f).Count();
				if (c > 0) {
					retVal.Add(loc);
				}
			}

			return retVal;
		}
		#endregion

		#region AlarmInstancesCreateMeters
		/// <summary>
		/// Alarms the instances create meters.
		/// </summary>
		/// <param name="chart">The chart.</param>
		/// <param name="meters">The meters.</param>
		private void AlarmInstancesCreateMeters(Chart chart, List<Meter> meters) {
			var retVal = new Dictionary<string, Meter>();
			var alarmInstances = chart.AlarmInstances;
			if (alarmInstances == null || alarmInstances.Count <= 0)
				return;

			foreach (var alarmInstance in alarmInstances) {
				if (!string.IsNullOrEmpty(alarmInstance.KeyAlarmDefinitionLocation)) {
					var loc = tenantContainer.Entry.SpatialHierarchy[alarmInstance.KeyAlarmDefinitionLocation];
					var classification = tenantContainer.Entry.ClassificationItemHierarchy[alarmInstance.KeyClassificationItem];
					var keyMeter = loc.KeyLocation + "_" + alarmInstance.KeyClassificationItem;
					Meter meter;
					if (!retVal.TryGetValue(keyMeter, out meter)) {
						meter = new Meter {
							Name = classification.Title,

							KeyMeter = keyMeter,

							KeyLocation = loc.KeyLocation,
							LocationLongPath = loc.LongPath,
							LocationLevel = loc.Level,
							LocationName = loc.Name,
							LocationShortPath = loc.ShortPath,
							LocationTypeName = loc.TypeName,

							KeyClassificationItem = classification.KeyClassificationItem,
							ClassificationItemTitle = classification.Title,

							GroupOperation = MathematicOperator.SUM,
							MDData = new List<MD>()
						};
						retVal.Add(keyMeter, meter);
					}

					meter.MDData.Add(new MD {
						AcquisitionDateTime = alarmInstance.InstanceDateTime,
						Value = alarmInstance.Value
					});
				}
			};
			meters.AddRange(retVal.Values);
		}
		#endregion

		#region FixMeterDataCount
		/// <summary>
		/// Fixes the meter data count.
		/// </summary>
		/// <param name="meter">The meter.</param>
		private void FixMeterDataCount(Meter meter) {
			if (meter.MDData != null) {
				var count = 0;
				var keyMeter = MeterHelper.ConvertMeterKeyToInt(meter.KeyMeter);
				//we need to assign a KeyMeterData for Virtual Meter in order for the Values to be displayed in the MeterData grid.
				meter.MDData.ForEach(md => {
					md.KeyMeter = keyMeter;
					md.KeyMeterData = count++;
				});
			}
		}
		#endregion

		#region ApplyVirtualMeterCalendar
		private void ApplyVirtualMeterCalendar(List<Meter> meters) {
			foreach (Meter m in meters) {
				if (m.IsVirtual) {
					//we apply the calendar directly to the meter data
					if (m.CalendarEventCategory != null && m.MDData != null && m.MDData.Count > 0 && (m.CalendarMode != ChartCalendarMode.SplitByEvent || string.IsNullOrEmpty(m.CalendarEventCategoryTitle) == false)) {
						var startDate = m.MDData.Min(md => md.AcquisitionDateTime);
						var endDate = m.MDData.Max(md => md.AcquisitionDateTime);
						FilterByCalendar(new List<Meter> { m }, startDate, endDate, m.CalendarEventCategory, m.CalendarMode);

						if (m.CalendarMode == ChartCalendarMode.SplitByEvent) {
							if (m.DataByCalendarEvent != null && m.DataByCalendarEvent.ContainsKey(m.CalendarEventCategoryTitle))
								m.MDData = m.DataByCalendarEvent[m.CalendarEventCategoryTitle];
							else
								m.MDData = new List<MD>();
							m.DataByCalendarEvent = null;
						}
						FixMeterDataCount(m);
					}
				}
			}
		}
		#endregion

		#region ApplyMeterOperations
		/// <summary>
		/// Generate the Virtual Meters data
		/// </summary>
		/// <param name="meters">The meters.</param>
        /// <param name="errors">The errors.</param>
		private void ApplyMeterOperations(List<Meter> meters, List<string> errors ) {
			var virtualMeterDataList = new ConcurrentDictionary<int, List<MD>>();
			var metersWithLevel = new SortedDictionary<int, List<Meter>>();

			//We check the level of each virtual meter, and we order them in order to make that level 1 virtual meters are processed before level 2.
			foreach (Meter m in meters) {
				if (m.IsVirtual && m.MeterOperations != null && m.MeterOperations.Count > 0) {
					var level = GetMeterOperationLevel(m);
					List<Meter> list = null;
					if (!metersWithLevel.TryGetValue(level, out list)) {
						list = new List<Meter>();
						metersWithLevel.Add(level, list);
					}

					list.Add(m);
				}
			}
		    var concurrentErrorsList = new ConcurrentQueue<string>();
			//metersWithLevel = metersWithLevel.OrderBy(tuple => tuple.Item1).ToList();
			foreach (var kvp in metersWithLevel) {
				List<Meter> list = kvp.Value;
				Parallel.ForEach(list, m => {
					if (m.IsVirtual && m.MeterOperations != null && m.MeterOperations.Count > 0) {
						try {

							var isSimple = MeterOperationsIsSimple(m);
							//simple case where the virtual operations are just summing a list of meters. Speeds up the calculation
							if (isSimple) {
								ProcessMeterOperationSimple(meters, m, virtualMeterDataList);
							}
							else {
								// Assumption: Meter operations are ordered by the Order property by the database query.
								foreach (MeterOperation op in m.MeterOperations) {
									ProcessMeterOperation(meters, op, virtualMeterDataList, m.MeterOperationTimeScaleInterval, m.GetTimeZone(), concurrentErrorsList);
								}
							}
                            List<MD> finalData;
                            if (virtualMeterDataList.ContainsKey(Convert.ToInt32(m.KeyMeterOperationResult))) {
                                finalData = virtualMeterDataList[Convert.ToInt32(m.KeyMeterOperationResult)];
                            }
                            else {
                                throw new SystemException(String.Format(Langue.error_meteroperation_resultdoesnotexist, m.KeyMeterOperationResult));
                            }

							m.MDData = finalData;
							FixMeterDataCount(m);
						}
						catch (SystemException e) {
							using (TracingService.StartTracing("EnergyAggregator - " + ContextHelper.ApplicationName + " - ApplyMeterOperations", m.KeyMeter)) {
								TracingService.Write(e);
							}
							if (m.MDData == null)
								m.MDData = new List<MD>();
						}
					}
				});
			}
            if (errors != null && concurrentErrorsList.Count > 0) {
                errors.AddRange(concurrentErrorsList.ToList()); 
		}
		}

		/// <summary>
		/// Determines whether the specified meter has only simple meter operations, so we can speed up the calculations.
		/// </summary>
		/// <param name="m">The meter.</param>
		private bool MeterOperationsIsSimple(Meter m) {
			bool additionOnly = true;

			if (m.MeterOperations != null && !string.IsNullOrEmpty(m.KeyMeterOperationResult)) {

				if (m.MeterOperations.OrderBy(op => op.Order).Last().KeyMeterOperation != m.KeyMeterOperationResult)
					additionOnly = false;


				foreach (MeterOperation op in m.MeterOperations) {
					if (op.Operator != ArithmeticOperator.Add && op.Operator != ArithmeticOperator.None) {
						additionOnly = false;
						break;
					}
					if (op.Factor1 != 1) {
						additionOnly = false;
						break;
					}
					if (op.Offset1.HasValue && op.Offset1.Value != 0) {
						additionOnly = false;
						break;
					}
					if (op.Function1 != AlgebricFunction.None) {
						additionOnly = false;
						break;
					}
					if (op.Operator != ArithmeticOperator.None) {
						if (op.Function2 != AlgebricFunction.None) {
							additionOnly = false;
							break;
						}
						if (op.Factor2 != 1) {
							additionOnly = false;
							break;
						}
						if (op.Offset2.HasValue && op.Offset2.Value != 0) {
							additionOnly = false;
							break;
						}
					}
				}

			}
			//if (m.MeterOperationTimeScaleInterval != AxisTimeInterval.None) {
			//	additionOnly = false;
			//}
			return additionOnly;
		}

		/// <summary>
		/// Gets the meter operation level : 0 means not a Virtual Meter, 1 means a Virtual Meter of real meters, 2 means a Virtual meter of Virtual Meters.
		/// </summary>
		/// <param name="meter">The meter.</param>
		/// <returns></returns>
		private int GetMeterOperationLevel(Meter meter) {
			var retVal = 0;
			if (meter.IsVirtual && meter.MeterOperations != null && meter.MeterOperations.Count > 0) {
				foreach (var op in meter.MeterOperations) {
					Meter meter1;
					if (!string.IsNullOrEmpty(op.KeyMeter1) && tenantContainer.Entry.Meters.TryGetValue(MeterHelper.ConvertMeterKeyToInt(op.KeyMeter1), out meter1)) {
						retVal = Math.Max(retVal, (meter1.IsVirtual ? 2 : 1));
					}

					Meter meter2;
					if (!string.IsNullOrEmpty(op.KeyMeter2) && tenantContainer.Entry.Meters.TryGetValue(MeterHelper.ConvertMeterKeyToInt(op.KeyMeter2), out meter2)) {
						retVal = Math.Max(retVal, (meter2.IsVirtual ? 2 : 1));
					}
				}

			}
			return retVal;
		}
		/// <summary>
		/// Loads the meter used in an operation.
		/// </summary>
		/// <param name="meters">The meters.</param>
		/// <returns></returns>
		private List<Meter> LoadMetersUsedInOperation(List<Meter> meters) {
			// We do this twice because we can have VirtualMeters of Virtual Meters
			//TODO: Replace this with a complete resolution of all virtual meters references. for now we can handle only two levels.
			var retVal = LoadMetersUsedInOperationSub(meters);
			retVal = LoadMetersUsedInOperationSub(retVal);
			return retVal;
		}

		/// <summary>
		/// Loads the meter used in an operation.
		/// </summary>
		/// <param name="meters">The meters.</param>
		/// <returns></returns>
		private List<Meter> LoadMetersUsedInOperationSub(List<Meter> meters) {
			var retVal = new ConcurrentDictionary<string, Meter>(meters.Distinct(m => m.KeyMeter).ToDictionary(m => m.KeyMeter, m => m));
			var keyMetersUsedInOperation = new ConcurrentDictionary<string, string>();

			// we gather all the keys of the meter that are used in the calculation
			Parallel.ForEach(meters, m => {
				if (m.IsVirtual && m.MeterOperations != null && m.MeterOperations.Count > 0) {
					foreach (MeterOperation op in m.MeterOperations) {
						if (!string.IsNullOrEmpty(op.KeyMeter1)) {
							var key = op.KeyMeter1;
							keyMetersUsedInOperation.TryAdd(key, key);
						}
						if (!string.IsNullOrEmpty(op.KeyMeter2)) {
							var key = op.KeyMeter2;
							keyMetersUsedInOperation.TryAdd(key, key);
						}
					}
				}
			});

			//for each meter we check if its in the retVal collection
			foreach (var key in keyMetersUsedInOperation.Keys) {
				if (retVal.ContainsKey(key) == false) {
					//if not we add it.
					Meter meter = null;
					if (tenantContainer.Entry.Meters.TryGetValue(MeterHelper.ConvertMeterKeyToInt(key), out meter)) {
						//we have to clone it in order not to modify the static meter instance.
						retVal[meter.KeyMeter] = meter.Clone();
					}

				}
			}
			return retVal.Values.ToList();
		}

		/// <summary>
		/// Processes a  single meter operation.
		/// </summary>
		/// <param name="meters">The meters.</param>
		/// <param name="op">The Meter operation.</param>
		/// <param name="virtualMeterDataList">A dictionnary to hold the intermediate calculation data.</param>
		/// <param name="interval">The interval.</param>
		/// <param name="timezone">The timezone in which to compute the timeinterval aggregation.</param>
        /// <param name="errors">The errors.</param>
		private void ProcessMeterOperation(List<Meter> meters, MeterOperation op, ConcurrentDictionary<int, List<MD>> virtualMeterDataList, AxisTimeInterval interval, TimeZoneInfo timezone, ConcurrentQueue<string> errors) {
			List<MD> retVal, data1, data2 = null;
			data1 = ProcessMeterOperation_GetMeterData(meters, op.KeyMeter1, op.KeyMeterOperation1, op, interval, op.Function1, virtualMeterDataList, timezone, errors : errors);
			if (op.Operator == ArithmeticOperator.None) {
                if (data1 != null) {
				ApplyFactor(op.Factor1, data1);
				ApplyOffset(op.Offset1, data1);
                }
				retVal = data1;
			}
			else {
				data2 = ProcessMeterOperation_GetMeterData(meters, op.KeyMeter2, op.KeyMeterOperation2, op, interval, op.Function2, virtualMeterDataList, timezone, errors : errors);
				retVal = ApplyOperator(op.Operator, op.Factor1, op.Offset1, data1, op.Factor2, op.Offset2, data2);
			}
			virtualMeterDataList[Convert.ToInt32(op.KeyMeterOperation)] = retVal;
		}

		/// <summary>
		/// Processes meter operations in case of a simple meter(only Add or None).
		/// </summary>
		/// <param name="meters">The meters.</param>
		/// <param name="m">The m.</param>
		/// <param name="virtualMeterDataList">The virtual meter data list.</param>
		private void ProcessMeterOperationSimple(List<Meter> meters, Meter m, ConcurrentDictionary<int, List<MD>> virtualMeterDataList) {
			var timezone = m.GetTimeZone();
			var interval = m.MeterOperationTimeScaleInterval;
			List<MD> retVal = new List<MD>();

			if (m.MeterOperations.Count == 1 && !string.IsNullOrEmpty(m.MeterOperations[0].KeyMeter1) && m.MeterOperations[0].Operator == ArithmeticOperator.None) {
				var op = m.MeterOperations[0];
				retVal = ProcessMeterOperation_GetMeterData(meters, op.KeyMeter1, null, op, interval, AlgebricFunction.None, null, timezone);
				virtualMeterDataList[Convert.ToInt32(m.KeyMeterOperationResult)] = retVal;
			}
			else {

				Parallel.ForEach(m.MeterOperations, op => {
					if (!string.IsNullOrEmpty(op.KeyMeter1)) {
						var result = ProcessMeterOperation_GetMeterData(meters, op.KeyMeter1, null, op, interval, AlgebricFunction.None, null, timezone);
						if (result != null) {
							lock (retVal) {
								retVal.AddRange(result);
							}
						}
					}
					if (op.Operator != ArithmeticOperator.None && !string.IsNullOrEmpty(op.KeyMeter2)) {
						var result = ProcessMeterOperation_GetMeterData(meters, op.KeyMeter2, null, op, interval, AlgebricFunction.None, null, timezone);
						if (result != null) {
							lock (retVal) {
								retVal.AddRange(result);
							}
						}
					}
				});

				retVal = GroupListMeterDataByTimeIntervalToList(retVal, AxisTimeInterval.Seconds, MathematicOperator.SUM);
				virtualMeterDataList[Convert.ToInt32(m.KeyMeterOperationResult)] = retVal;
			}
		}

		/// <summary>
		/// Processes the meter operation.
		/// </summary>
		/// <param name="meters">The meters.</param>
		/// <param name="KeyMeter">The key meter.</param>
		/// <param name="KeyMeterOperation">The key meter operation.</param>
        /// <param name="MeterOperationContainer">The meter operation container.</param>
		/// <param name="interval">The interval.</param>
		/// <param name="function">The function.</param>
		/// <param name="virtualMeterDataList">The virtual meter data list.</param>
		/// <param name="timezone">The timezone.</param>
        /// <param name="errors">The errors.</param>
		/// <returns></returns>
		private List<MD> ProcessMeterOperation_GetMeterData(List<Meter> meters, string KeyMeter, string KeyMeterOperation, MeterOperation MeterOperationContainer, AxisTimeInterval interval, AlgebricFunction function, ConcurrentDictionary<int, List<MD>> virtualMeterDataList, TimeZoneInfo timezone, ConcurrentQueue<string> errors = null) {
			List<MD> retVal = null;
			try {
				if (string.IsNullOrEmpty(KeyMeter) && !string.IsNullOrEmpty(KeyMeterOperation) && KeyMeterOperation != "0") {
                    if (virtualMeterDataList.ContainsKey(Convert.ToInt32(KeyMeterOperation))) {
					retVal = virtualMeterDataList[Convert.ToInt32(KeyMeterOperation)];
                        // we need to clone to prevent modifying the in memory records.
					retVal = Clone(retVal);
				}

                    else {
                        if (errors != null) {
                            var meter = meters.FirstOrDefault(m => m.KeyMeter == MeterOperationContainer.KeyMeter);
                            errors.Enqueue(string.Format(Langue.err_meteroperationreference_couldnotberesolved, KeyMeterOperation, MeterOperationContainer.LocalId ,meter != null ? meter.LocalId : string.Empty));
                        }
                    }

                }
				else if (!string.IsNullOrEmpty(KeyMeter)) {
					var meter = meters.First(m => m.KeyMeter == KeyMeter);
					//DISABLED FOR NOW : we need to fix the rollup in case the chart interval is minute and the meteroperation interval is hours.
					FixRollupForMeterIsAcquisitionDateTimeEndInterval(new List<Meter> { meter }, AxisTimeInterval.Minutes); //interval : we dont use interval because when it s none then the meter isnt adjusted, but we need to in case of virtual meters.
					// timezone sent to Grouping method to get the grouping by the virtual meter timezone.
					retVal = GroupListMeterDataByTimeIntervalToList(meter.MDData, interval, meter.GroupOperation, timezone);
				}
				ApplyFunction(function, retVal);
			}
			catch (Exception e) {
				TracingService.Write(e);
			}
			return retVal;
		}


		/// <summary>
		/// Clones the specified MD list.
		/// </summary>
		/// <param name="data">The MD list.</param>
		/// <param name="startDate">The start date.</param>
		/// <param name="endDate">The end date.</param>
		/// <returns></returns>
		private List<MD> Clone(List<MD> data, DateTime? startDate = null, DateTime? endDate = null) {
			var retVal = new List<MD>();
			if (data != null) {
				retVal = data.AsParallel().
						   Where(md =>
							   (!startDate.HasValue || md.AcquisitionDateTime >= startDate.Value.ToUniversalTime())
							   &&
							   (!endDate.HasValue || md.AcquisitionDateTime < endDate.Value.ToUniversalTime())
						   ).
						   Select(md => new MD() {
							   AcquisitionDateTime = md.AcquisitionDateTime,
							   Value = md.Value,
							   Validity = md.Validity,
							   KeyMeter = md.KeyMeter,
							   KeyMeterData = md.KeyMeterData
						   }).ToList();
			}
			return retVal;
		}

		/// <summary>
		/// Applies an Algebric function (cos, sin ...) to a list of MD.
		/// </summary>
		/// <param name="func">The function to apply.</param>
		/// <param name="data">The data.</param>
		private void ApplyFunction(AlgebricFunction func, List<MD> data) {
			if (func != AlgebricFunction.None) {
				data.ForEach(md => md.Value = ApplyFunction(func, md.Value));
			}
		}

		/// <summary>
		/// Applies a multiplicative factor to a list of MD.
		/// </summary>
		/// <param name="factor1">The factor.</param>
		/// <param name="data">The data.</param>
		private void ApplyFactor(double factor1, List<MD> data) {
			if (factor1 != 1)
				data.ForEach(md => md.Value = ApplyFactor(factor1, md.Value));
		}

		/// <summary>
		/// Applies an offset to a list of MD.
		/// </summary>
		/// <param name="offset">The offset.</param>
		/// <param name="data">The data.</param>
		private void ApplyOffset(double? offset, List<MD> data) {
			if (offset.HasValue && Math.Abs(offset.Value) > 0 && data.Count > 0) {
				data.ForEach(md => md.Value = ApplyOffset(offset.Value, md.Value));
			}
		}

		/// <summary>
		/// Applies a mathematic calculation (a*x+c+b*y+d) to 2 list of MD.
		/// </summary>
		/// <param name="math">The mathematic operator (+,-..).</param>
		/// <param name="factor1">The factor 1.</param>
		/// <param name="offset1">The offset1.</param>
		/// <param name="data1">The data 1.</param>
		/// <param name="factor2">The factor 2.</param>
		/// <param name="offset2">The offset2.</param>
		/// <param name="data2">The data 2.</param>
		/// <returns></returns>
		private List<MD> ApplyOperator(ArithmeticOperator math, double factor1, double? offset1, List<MD> data1, double factor2, double? offset2, List<MD> data2) {
			if (math == ArithmeticOperator.None) return data1;

			var dataGroupByTime = new Dictionary<DateTime, Tuple<double?, double?, byte?, byte?>>();

			if (data1 != null) {
				foreach (var md in data1) {
					var value = new Tuple<double?, double?, byte?, byte?>(md.Value, null, md.Validity, null);
					dataGroupByTime.Add(md.AcquisitionDateTime, value);
				}
			}

			if (data2 != null) {
				foreach (var md in data2) {
					Tuple<double?, double?, byte?, byte?> value;
					dataGroupByTime.TryGetValue(md.AcquisitionDateTime, out value);
					if (value != null) {
						value = new Tuple<double?, double?, byte?, byte?>(value.Item1, md.Value, value.Item3, md.Validity);
					}
					else {
						value = new Tuple<double?, double?, byte?, byte?>(null, md.Value, null, md.Validity);
					}
					dataGroupByTime[md.AcquisitionDateTime] = value;
				}
			}

			var retVal = dataGroupByTime.AsParallel().Select(kv => new MD {
				AcquisitionDateTime = kv.Key,
				Value = ApplyOperator(math, factor1, offset1, kv.Value.Item1, factor2, offset2, kv.Value.Item2),
				Validity = ((kv.Value.Item3.HasValue && kv.Value.Item3.Value == (byte)MeterDataValidity.Invalid.ParseAsInt()) || (kv.Value.Item4.HasValue && kv.Value.Item4.Value == (byte)MeterDataValidity.Invalid.ParseAsInt())) ? (byte)MeterDataValidity.Invalid.ParseAsInt() : (byte)MeterDataValidity.Valid.ParseAsInt()
			}).ToList();
			return retVal;

		}

		/// <summary>
		/// Applies an Algebric function (cos, sin ...) to a double
		/// </summary>
		/// <param name="func">The func.</param>
		/// <param name="value">The value.</param>
		/// <returns></returns>
		private double ApplyFunction(AlgebricFunction func, double value) {
			var retVal = value;
			switch (func) {
				case AlgebricFunction.Abs:
					retVal = Math.Abs(retVal);
					break;
				case AlgebricFunction.Cos:
					retVal = Math.Cos(retVal);
					break;
				case AlgebricFunction.Inverse:
					retVal = 1 / retVal;
					break;
				case AlgebricFunction.Log:
					retVal = Math.Log(retVal);
					break;
				case AlgebricFunction.Sin:
					retVal = Math.Sin(retVal);
					break;
				case AlgebricFunction.Sqrt:
					retVal = Math.Sqrt(retVal);
					break;
				case AlgebricFunction.Tan:
					retVal = Math.Tan(retVal);
					break;
				case AlgebricFunction.Count:
					retVal = 1;
					break;
			}
			return retVal;
		}

		/// <summary>
		/// Applies a multiplicative factor to a single value.
		/// </summary>
		/// <param name="factor">The factor.</param>
		/// <param name="value">The data.</param>
		private double ApplyFactor(double factor, double value) {
			var retVal = factor * value;
			return retVal;
		}

		/// <summary>
		/// Applies a offset  to a single value.
		/// </summary>
		/// <param name="offset">The offset.</param>
		/// <param name="value">The data.</param>
		private double ApplyOffset(double offset, double value) {
			var retVal = value + offset;
			return retVal;
		}

		/// <summary>
		/// Applies a mathematic calculation (a*x+b*y) beetween 2 double.
		/// </summary>
		/// <param name="math">The mathematic operator (+,- ..).</param>
		/// <param name="factor1">The factor 1.</param>
		/// <param name="offset1">The offset 1.</param>
		/// <param name="value1">The value 1.</param>
		/// <param name="factor2">The factor 2.</param>
		/// <param name="offset2">The offset 2.</param>
		/// <param name="value2">The value 2.</param>
		/// <returns></returns>
		private double ApplyOperator(ArithmeticOperator math, double factor1, double? offset1, double? value1, double? factor2, double? offset2, double? value2) {
			double a = 0;
			if (value1.HasValue) {
				a = value1.Value * factor1;
				if (offset1.HasValue) {
					a += offset1.Value;
				}
			}
			double? b = null;
			if (value2.HasValue) {
				b = value2.Value;

				if (factor2.HasValue) {
					b *= factor2.Value;
				}
				if (offset2.HasValue) {
					b += offset2.Value;
				}
			}

			var retVal = a;
			if (b.HasValue) {
				switch (math) {
					case ArithmeticOperator.Add:
						retVal = a + b.Value;
						break;
					case ArithmeticOperator.Subtract:
						retVal = a - b.Value;
						break;
					case ArithmeticOperator.Divide:
						if (Math.Abs(b.Value) > 0)
							retVal = a / b.Value;
						else
							retVal = 0;
						break;
					case ArithmeticOperator.Multiply:
						retVal = a * b.Value;
						break;
					case ArithmeticOperator.Power:
						retVal = Math.Pow(a, b.Value);
						break;
				}
			}
			else {
				//if b doesnt have value and we multiply or divide , we output 0.
				if (math == ArithmeticOperator.Multiply || math == ArithmeticOperator.Divide) {
					retVal = 0;
				}
			}

			return retVal;
		}
		#endregion

		#region ApplyDegreeDays

		/// <summary>
		/// Applies the degree days.
		/// </summary>
		/// <param name="meters">The meters.</param>
		/// <param name="rawData">if set to <c>true</c> return [raw data] and not degree day.</param>
		private void ApplyDegreeDays(List<Meter> meters, bool rawData) {
			if (rawData) {
				return;
			}
			foreach (var m in meters) {
				ApplyDegreeDays(m);
			}
		}

		/// <summary>
		/// Applies the degree days.
		/// </summary>
		/// <param name="meter">The meter.</param>
		private void ApplyDegreeDays(Meter meter) {
			#region Validity check
			if (!meter.DegreeDayBase.HasValue)
				return;
			if (meter.DegreeDayType == DegreeDayType.None)
				return;
			if (meter.MDData == null || meter.MDData.Count <= 0)
				return;
			#endregion

			//meter.MDData = Clone(meter.MDData);
			var timeZone = meter.GetTimeZone();

			var groupingInterval = AxisTimeInterval.Days;
			if (meter.DegreeDayType == DegreeDayType.CoolingHourBased || meter.DegreeDayType == DegreeDayType.HeatingHourBased || meter.DegreeDayType == DegreeDayType.Costic) {
				groupingInterval = AxisTimeInterval.Hours;
			}

			ITimeIntervalAggregator timeIntervalAggregator = EnergyAggregatorHelper.GetTimeAggregator(groupingInterval);
			Dictionary<long, List<MD>> groupedData = timeIntervalAggregator.GroupDataByTime(meter.MDData, timeZone);

			var degreeDayData = new List<MD>();
			var keyMeter = MeterHelper.ConvertMeterKeyToInt(meter.KeyMeter);
			var count = 0;
			//AmirTodo - consider renaming BaseTemp or something, T is confusing with generics
			var TBase = meter.DegreeDayBase.Value;
			#region Costic
			if (meter.DegreeDayType == DegreeDayType.Costic) {
				var data = new List<MD>();
				var days = new List<DateTime>();
				#region Selecting the days
				foreach (var period in groupedData) {
					data.AddRange(period.Value);
					var day = EnergyAggregatorHelper.ConvertLongToDateTime(period.Key);
					day = new DateTime(day.Year, day.Month, day.Day);
					if (days.Contains(day) == false)
						days.Add(day);
				}
				#endregion
				#region Calculation
				foreach (var day in days) {
					//Tn = Minimum du jour J relevé entre J-1 à 18h et J à 18h (Google translate: Minimum Day observed between J-1 and J 18h to 18h)
					var startDateTn = new DateTime(day.Year, day.Month, day.Day, 18, 0, 0).AddDays(-1);
					var endDateTn = new DateTime(day.Year, day.Month, day.Day, 18, 0, 0);
					//AmirTodo - consider turning data into sorted list.
					var subsetTn = data.Where(md => md.AcquisitionDateTime >= startDateTn && md.AcquisitionDateTime < endDateTn).ToList();
					if (subsetTn.Count > 0) {
						var Tn = subsetTn.Min(md => md.Value);
						//Tx = Maximum du jour J relevé entre J à 6h et J+1 à 6h et pour un seuil S
						var startDateTx = new DateTime(day.Year, day.Month, day.Day, 6, 0, 0);
						var endDateTx = new DateTime(day.Year, day.Month, day.Day, 6, 0, 0).AddDays(+1);
						var subsetTx = data.Where(md => md.AcquisitionDateTime >= startDateTx && md.AcquisitionDateTime < endDateTx).ToList();
						if (subsetTx.Count > 0) {
							//AmirTodo - Why do we do the Where again?
							var Tx = subsetTx.Max(md => md.Value);
							var value = double.MinValue;
							//Si Tn > S DJ = 0
							//Si Tn < S < Tx DJ = ( S - Tn ) / ( 0,08 + 0,42 x ( S - Tn ) / ( Tx - Tn))
							//Si Tx < S DJ = S - ( Tn + Tx) / 2

							if (Tn > TBase)
								value = 0;
							else if (Tn < TBase && TBase < Tx)
								value = (TBase - Tn) / (0.08 + 0.42 * (TBase - Tn) / (Tx - Tn));
							else if (Tx < TBase)
								value = TBase - (Tn + Tx) / 2;

							if (value < 0)
								value = 0;

							degreeDayData.Add(new MD() {
								AcquisitionDateTime = day,
								Value = value,
								KeyMeter = keyMeter,
								KeyMeterData = count++//we have to assign a KeyMeterData to display in the Grid Meter Data.
							});
						}
					}
				}
				#endregion
			}
			#endregion
			else {
				foreach (var period in groupedData) {
					var TMin = period.Value.Min(md => md.Value);
					var TMax = period.Value.Max(md => md.Value);

					var value = double.MinValue;

					switch (meter.DegreeDayType) {
						#region Heating
						case DegreeDayType.Heating:
							value = TBase - (TMax + TMin) / 2d;
							break;
						#endregion
						#region Cooling
						case DegreeDayType.Cooling:
							value = (TMax + TMin) / 2d - TBase;
							break;
						#endregion
						#region CoolingHourBased
						case DegreeDayType.CoolingHourBased:
							value = (TBase - (TMax + TMin) / 2d) / 24d;
							break;
						#endregion
						#region HeatingHourBased
						case DegreeDayType.HeatingHourBased:
							value = ((TMax + TMin) / 2d - TBase) / 24d;
							break;
						#endregion
						#region CoolingEnhanced
						case DegreeDayType.CoolingEnhanced: {
								if (TMin > TBase)
									value = 0;
								else if ((TMax + TMin) / 2d > TBase)
									value = (TBase - TMin) / 4d;
								else if (TMax >= TBase)
									value = (TBase - TMin) / 2d - (TMax - TBase) / 4d;
								else if (TMax < TBase)
									value = TBase - (TMax + TMin) / 2d;
							}
							break;
						#endregion
						#region HeatingEnhanced
						case DegreeDayType.HeatingEnhanced: {
								if (TMax < TBase)
									value = 0;
								else if ((TMax + TMin) / 2d < TBase)
									value = (TMax - TBase) / 4d;
								else if (TMin <= TBase)
									value = (TMax - TBase) / 2d - (TBase - TMax) / 4d;
								else if (TMin > TBase)
									value = (TMax + TMin) / 2d - TBase;
							}
							break;
						#endregion
					}
					if (value < 0)
						value = 0;

					degreeDayData.Add(new MD() {
						AcquisitionDateTime = EnergyAggregatorHelper.ConvertLongToDateTime(period.Key),
						Value = value,
						KeyMeter = keyMeter,
						KeyMeterData = count++//we have to assign a KeyMeterData to display in the Grid Meter Data.
					});
				}
			}
			//We convert the meter data back to utc because degree days need to be calculated in the local timezone because they are based on the hour the data was taken.
			degreeDayData = ChangeMeterDataToUtcTimeZone(degreeDayData, timeZone);
			meter.MDData = degreeDayData;
		}
		#endregion

		#region ApplyPsetHistoricalFactor
		/// <summary>
		/// Applies the pset historical factor.
		/// </summary>
		/// <param name="meters">The meters.</param>
		public void ApplyPsetHistoricalFactor(List<Meter> meters) {
			var spatialHierarchy = tenantContainer.Entry.SpatialHierarchy;
			var historicalData = tenantContainer.Entry.HistoricalData;

			//AmirTodo - is the parallel really helpful, needs further investigation.
			Parallel.ForEach(meters, m => {
				ApplyPsetHistoricalFactor(m, spatialHierarchy, historicalData);
			});
		}

		/// <summary>
		/// Applies the pset historical factor.
		/// </summary>
		/// <param name="m">The meter.</param>
		/// <param name="spatialHierarchy">The spatial hierarchy.</param>
		/// <param name="historicalData">The historical data.</param>
		public void ApplyPsetHistoricalFactor(Meter m, ConcurrentDictionary<string, Location> spatialHierarchy, ConcurrentDictionary<string, PsetAttributeHistorical> historicalData) {
			if (string.IsNullOrEmpty(m.PsetName) == false && string.IsNullOrEmpty(m.AttributeName) == false && m.MDData != null && m.MDData.Count > 0) {
				#region GetSpatialHierarchyFromLocation
				List<string> branch = null;
				if (m.UsageName == "IfcMeter") {
					branch = new List<string> { m.KeyMeter };
				}
				else {
					branch = GetSpatialHierarchyFromLocation(m.KeyLocation, spatialHierarchy);
				}
				#endregion
				#region GetPsetData
				SortedList<DateTime, double> psetData = new SortedList<DateTime, double>();
				//for each found location we look to see if it contains the correct data.
				foreach (var locKey in branch) {
					bool locationHasData = false;
					foreach (var data in historicalData.Values) {
						if (data.KeyObject == locKey && data.KeyPropertySingleValue == m.KeyPropertySingleValue) {
							locationHasData = true;
							psetData.Add(data.AttributeAcquisitionDateTime, data.AttributeValue);
						}
					}
					if (locationHasData == true) {
						break;
					}
				}
				#endregion
				#region ApplyPsetRatioHistorical
				if (psetData.Count > 0) {
					int psetDataIndex = 0;
					m.MDData = Clone(m.MDData).OrderBy(md => md.AcquisitionDateTime).ToList();//check if the sort is necessary
					for (var i = 0; i < m.MDData.Count; i++) {
						var md = m.MDData[i];
						psetDataIndex = ApplyPsetHistoricalFactorToMD(md, psetData, psetDataIndex, m.PsetFactorOperator);
					}
				}
				#endregion
			}
		}

		/// <summary>
		/// Applies the pset historical factor to MD.
		/// </summary>
		/// <param name="md">The md.</param>
		/// <param name="psetData">The pset data.</param>
		/// <param name="currentIndex">Index of the current.</param>
		/// <param name="op">The op.</param>
		/// <returns></returns>
		private int ApplyPsetHistoricalFactorToMD(MD md, SortedList<DateTime, double> psetData, int currentIndex, ArithmeticOperator op) {

			bool foundInterval = false;
			double factor = 1;
			while (!foundInterval && currentIndex < psetData.Count) {
				factor = psetData.Values[currentIndex];
				var startDate = psetData.Keys[currentIndex];
				var endDate = DateTime.MaxValue;
				if (currentIndex < psetData.Count - 1) {
					endDate = psetData.Keys[currentIndex + 1];
				}

				if (md.AcquisitionDateTime < startDate && currentIndex == 0) {
					foundInterval = true;
					factor = 1;
				}
				else if (md.AcquisitionDateTime >= startDate && md.AcquisitionDateTime < endDate) {
					foundInterval = true;
				}
				else {
					currentIndex += 1;
				}
			}
			switch (op) {
				case ArithmeticOperator.Divide:
					md.Value /= factor;
					break;
				case ArithmeticOperator.Multiply:
				default:
					md.Value *= factor;
					break;
			}
			return currentIndex;
		}
		#endregion

		#region FilterByCalendar
		/// <summary>
		/// Filters the Meter data by a specific calendar.
		/// We go up the spatial hierarchy to find on which level the calendar has been defined.
		/// </summary>
		/// <param name="meters">The meters.</param>
		/// <param name="startDate">The start date.</param>
		/// <param name="endDate">The end date.</param>
		/// <param name="category">The calendar event category.</param>
		/// <param name="calendarMode">The calendar mode.</param>
		private void FilterByCalendar(List<Meter> meters, DateTime startDate, DateTime endDate, CalendarEventCategory category, ChartCalendarMode calendarMode) {
			if (category == null)
				return;
			var sortedOccurencesByLocation = new Dictionary<string, SortedList<DateTime, List<VizEventOccurence>>>();
			var swFindLocation = new Stopwatch();
			var swGenerateOccurences = new Stopwatch();
			var swApplyFilter = new Stopwatch();

			foreach (Meter m in meters) {
				if (m.MDData == null || m.MDData.Count <= 0)
					continue;
				#region We go up the spatial hierarchy until we find a corresponding calendar
				swFindLocation.Start();
				List<VizEventOccurence> occurrences = new List<VizEventOccurence>();
				var keyLocation = m.KeyMeter;
				//We first check if a calendar has been defined on the meter level.
				tenantContainer.Entry.CalendarEventOccurrences.TryGetValue(keyLocation, out occurrences);
				if (!CalendarContainsCategory(occurrences, category)) {
					var found = false;
					keyLocation = m.KeyLocation;

					while (!found && string.IsNullOrEmpty(keyLocation) == false) {//(occurrences == null || occurrences.Count == 0) && 
						tenantContainer.Entry.CalendarEventOccurrences.TryGetValue(keyLocation, out occurrences);
						if (CalendarContainsCategory(occurrences, category)) {
							found = true;
							break;
						}
						else {
							Location location;
							if (tenantContainer.Entry.SpatialHierarchy.TryGetValue(keyLocation, out location)) {
								if (string.IsNullOrEmpty(location.KeyParent) == false) {
									keyLocation = location.KeyParent;
								}
								else
									found = true;
							}
							else {
								found = true;
							}
						}
					}
				}
				swFindLocation.Stop();
				#endregion
				#region We filter the occurences based on their category and their dates compared to the chart start date and end date
				swGenerateOccurences.Start();
				SortedList<DateTime, List<VizEventOccurence>> sortedOccurences = null;
				if (!sortedOccurencesByLocation.TryGetValue(keyLocation, out sortedOccurences) && occurrences != null) {
					sortedOccurences = new SortedList<DateTime, List<VizEventOccurence>>();
					#region We check the startdate/enddate of each occurence.
					var occurencesFiltered = occurrences.Where(occ => occ.Category == category.LocalId && !(occ.EndDate < startDate.ToUniversalTime() || occ.StartDate > endDate.ToUniversalTime())).ToList();
					#endregion
					#region Sorting the occurences by StartDateTime
					//We sort the event by StartDate
					foreach (var occ in occurencesFiltered) {
						List<VizEventOccurence> list = null;
						if (sortedOccurences.TryGetValue(occ.StartDate, out list) == false) {
							list = new List<VizEventOccurence>();
							sortedOccurences.Add(occ.StartDate, list);
						}
						list.Add(occ);
					}
					#endregion
					sortedOccurencesByLocation.Add(keyLocation, sortedOccurences);
				}
				swGenerateOccurences.Stop();
				#endregion
				#region we split the data based on the fact it is inside or outside the specified event
				swApplyFilter.Start();
				if (sortedOccurences == null || sortedOccurences.Count <= 0) {
					if (calendarMode == ChartCalendarMode.Exclude) {
						continue;
					}
					else {
						//if we didnt find any event for the meter we remove its data, unless we are in exclude mode
						m.MDData = new List<MD>();
						continue;
					}
				}

				var subData = new ConcurrentDictionary<string, List<MD>>();
				Parallel.ForEach(m.MDData, md => {
					bool insert = false;
					string eventName = "";
					double? factor = null;

					var indexMax = sortedOccurences.FindFirstIndexGreaterThanOrEqualTo(md.AcquisitionDateTime);
					//in that case we need to take the whole collection
					if (indexMax < 0 || indexMax >= sortedOccurences.Count) {
						indexMax = sortedOccurences.Count;
					}
					//we check if the last key we found is higher so we can discard it.
					else if (sortedOccurences.Keys[indexMax] == md.AcquisitionDateTime) {
						indexMax += 1;
					}
					for (var i = 0; i < indexMax; i++) {
						var subList = sortedOccurences.Values[i];
						if (subList != null && subList.Count > 0) {
							for (var j = 0; j < subList.Count; j++) {
								var occFiltered = subList[j];
								if (md.AcquisitionDateTime >= occFiltered.StartDate && md.AcquisitionDateTime < occFiltered.EndDate) {
									insert = true;
									factor = occFiltered.Factor;
									if (calendarMode == ChartCalendarMode.SplitByEvent) {
										eventName = occFiltered.Summary;
									}
									break;
								}
							}
						}
					}
					if ((insert && (calendarMode == ChartCalendarMode.Include || calendarMode == ChartCalendarMode.SplitByEvent)) || (!insert && calendarMode == ChartCalendarMode.Exclude)) {
						var subDataList = subData.GetOrAdd(eventName, s => new List<MD>());

						if (factor.HasValue && factor.Value != 1) {
							//we create a copy of the meter data to apply the ratio
							lock (subDataList) {
								subDataList.Add(new MD() {
									AcquisitionDateTime = md.AcquisitionDateTime,
									Value = md.Value * factor.Value,
									Validity = md.Validity
								});
							}
						}
						else {
							lock (subDataList) {
								subDataList.Add(md);
							}
						}
					}
				});

				if (calendarMode == ChartCalendarMode.SplitByEvent) {
					m.MDData = null;
					m.DataByCalendarEvent = new Dictionary<string, List<MD>>(subData);
				}
				else {
					if (subData != null && subData.Values != null && subData.Values.Count > 0)
						m.MDData = subData.Values.ElementAt(0);
					else
						m.MDData = new List<MD>();
				}
				swApplyFilter.Stop();
				#endregion
			}
		}

		/// <summary>
		/// Calendars the contains category.
		/// </summary>
		/// <param name="occurrences">The occurrences.</param>
		/// <param name="category">The category.</param>
		/// <returns></returns>
		private bool CalendarContainsCategory(List<VizEventOccurence> occurrences, CalendarEventCategory category) {
			return occurrences != null &&  occurrences.Any(occ => occ.Category == category.LocalId);
				}
		#endregion

		#region GroupByLocalisationAndClassification
		/*
		/// <summary>
		/// Returns a Tuple of DataSeries based on a Meter localisation and list of MD.
		/// The Geographic grouping is done through Location entities.
		/// </summary>
		/// <param name="meters">The meters.</param>
		/// <param name="chart">The chart.</param>
		/// <param name="historicalAnalysis">The historical analysis.</param>
		/// <returns></returns>
		private List<DataSerie> GroupByLocalisationAndClassificationOld(List<Meter> meters, Chart chart, ChartHistoricalAnalysis historicalAnalysis) {
			var existingSeries = new DataSerieCollection(chart.Series);
			var retVal = new Dictionary<string, DataSerie>();
			int siteMaxLevel = tenantContainer.Entry.SiteMaxLevel;
			var ClassificationItemHierarchy = tenantContainer.Entry.ClassificationItemHierarchy;
			var SpatialHierarchy = tenantContainer.Entry.SpatialHierarchy;

			foreach (var m in meters) {
				string locationName = "";
				Location finalLocation;
				ClassificationItem finalClassification;
				List<DataSerie> series = GetSeriesLocalIdAndData(chart.KeyChart, m, chart.Localisation, chart.LocalisationSpatialTypeName, chart.LocalisationSiteLevel, siteMaxLevel, chart.ClassificationLevel, chart.CalendarMode, historicalAnalysis, chart.UseSpatialPath, chart.UseClassificationPath, out locationName, out finalLocation, out finalClassification, ClassificationItemHierarchy, SpatialHierarchy);
				series.ForEach(s => {
					if (!string.IsNullOrEmpty(s.LocalId)) {
						DataSerie serie = null;
						//we fetch the existing serie or we create a new one if it does
						if (!retVal.TryGetValue(s.LocalId, out serie)) {
							serie = CreateOrGetDataserie(chart.KeyChart, existingSeries, s.LocalId, m, finalClassification);
							serie.MDData = s.MDData;
							serie.LocationName = locationName;
							serie.KeyMeter = s.KeyMeter;
							if (finalClassification != null) {
								serie.ClassificationName = finalClassification.Title;
								serie.KeyClassificationDrillDown = finalClassification.KeyClassificationItem;
							}
							if (finalLocation != null)
								serie.KeyLocation = finalLocation.KeyLocation;
							retVal.Add(serie.LocalId, serie);
						}
						else {
							if (s.MDData != null && s.MDData.Count > 0) {
								serie.MDData.AddRange(s.MDData);
							}
						}
					}
				});
				m.MDData = null;
				m.DataByCalendarEvent = null;
			}
			return retVal.Values.ToList();
		}
		 */

		/// <summary>
		/// Returns a Tuple of DataSeries based on a Meter localisation and list of MD.
		/// The Geographic grouping is done through Location entities.
		/// </summary>
		/// <param name="meters">The meters.</param>
		/// <param name="chart">The chart.</param>
		/// <param name="historicalAnalysis">The historical analysis.</param>
		/// <returns></returns>
		private List<DataSerie> GroupByLocalisationAndClassification(List<Meter> meters, Chart chart, ChartHistoricalAnalysis historicalAnalysis) {
			var existingSeries = new DataSerieCollection(chart.Series);
			
			int siteMaxLevel = tenantContainer.Entry.SiteMaxLevel;

			// in case of bad localization site level coming from unrefreshed UI
			chart.LocalisationSiteLevel = Math.Min(siteMaxLevel, chart.LocalisationSiteLevel);

			var bucketNumber = m_ProcessorCount;
			var bucketSize = meters.Count / bucketNumber + 1;
			var groupLock = new object();
			var groupedSeriesDictionary = new Dictionary<string, DataSerie>();
            var groupedSeriesOperatorsDictionary = new Dictionary<string, HashSet<MathematicOperator>>();

			//These must me initialized before the parallel work since when other threads are created, they do not have the access to this data
			var classificationItemHierarchy = tenantContainer.Entry.ClassificationItemHierarchy;
			var spatialHierarchy = tenantContainer.Entry.SpatialHierarchy;

			Parallel.For(0, bucketNumber, () => new List<DataSerie>(),
				(currentIndex, loopState, localResults) => {
					for (var j = bucketSize * currentIndex; j < Math.Min(meters.Count, bucketSize * (currentIndex + 1)); j++) {
						Meter meter = meters[j];
						string locationName;
						Location finalLocation;
						ClassificationItem finalClassification;
						IEnumerable<DataSerie> series = GetSeriesLocalIdAndData(chart, meter, siteMaxLevel, historicalAnalysis, out locationName, out finalLocation, out finalClassification, classificationItemHierarchy, spatialHierarchy);
						foreach (var s in series) {
							if (string.IsNullOrEmpty(s.LocalId))
							{
								continue;
							}

							DataSerie serie = CreateOrGetDataserie(chart.KeyChart, existingSeries, s.LocalId, s.Name, meter, finalClassification);
									// Make a copy of the serie so we won't change the original.
									var keyOriginalDataSerie = serie.KeyDataSerie;

									serie = serie.Copy(s.LocalId);
									serie.KeyDataSerie = keyOriginalDataSerie;
									serie.MDData = s.MDData ?? new List<MD>();
									serie.LocationName = locationName;
									serie.KeyMeter = s.KeyMeter;
									if (finalClassification != null) {
										serie.ClassificationName = finalClassification.Title;
										serie.KeyClassificationDrillDown = finalClassification.KeyClassificationItem;
									}
							if (finalLocation != null) {
										serie.KeyLocation = finalLocation.KeyLocation;
							}
								
							localResults.Add(serie);
						}
						meter.MDData = null;
						meter.DataByCalendarEvent = null;
					}
					return localResults;

				}, localResults => {
					lock (groupLock) {
						foreach (DataSerie dataSerie in localResults) {
							DataSerie resultSerie;
							if (groupedSeriesDictionary.TryGetValue(dataSerie.LocalId, out resultSerie)) {
								resultSerie.MDData.AddRange(dataSerie.MDData);
                                groupedSeriesOperatorsDictionary[dataSerie.LocalId].Add(dataSerie.GroupOperation);
							}
							else {
								groupedSeriesDictionary.Add(dataSerie.LocalId, dataSerie);
                                groupedSeriesOperatorsDictionary.Add(dataSerie.LocalId, new HashSet<MathematicOperator> { dataSerie.GroupOperation });
							}
						}
					}
				});

            ValidateOneGroupOperationPerSeries(chart, groupedSeriesOperatorsDictionary, groupedSeriesDictionary);

		    List<DataSerie> groupedSeriesList = groupedSeriesDictionary.Values.ToList();
			return groupedSeriesList;
		}

        #endregion

        #region ValidateOneGroupOperationPerSeries

        /// <summary>
        /// Validates all the series in the chart have only one group operation.
        /// </summary>
        /// <param name="chart">The chart</param>
        /// <param name="groupedSeriesOperatorsDictionary">Dictionary with Serie localId and the group operators for the Serie </param>
        /// <param name="groupedSeriesDictionary">Dictionary with Serie localId and the Serie</param>
        private static void ValidateOneGroupOperationPerSeries(Chart chart, Dictionary<string, HashSet<MathematicOperator>> groupedSeriesOperatorsDictionary,
            Dictionary<string, DataSerie> groupedSeriesDictionary) {

            // If a serie has more than one group operation
            foreach (var serieOperations in groupedSeriesOperatorsDictionary.Where(x => x.Value.Count > 1)) {
                var operations = serieOperations.Value;
//                var operationsLocalizedNamesCommaSeparated = string.Join(", ", operations.Select(x => x.GetLocalizedText()));
                var serieName = groupedSeriesDictionary[serieOperations.Key].Name;
                var errorMessage = string.Format(Langue.error_incompatiblegroupoperations, 
					serieName, chart.Title /*operationsLocalizedNamesCommaSeparated */);

                chart.Errors.Add(errorMessage);
                TracingService.Write(TraceEntrySeverity.Error, errorMessage, "Chart_Process", chart.KeyChart);
            }
        }
        
        #endregion

		#region CreateOrGetDataserie
        
		/// <summary>
		/// Look for an existing serie with the same LocalId or generate a new one either based on the parameter of the the GroupName series or from scratch.
		/// </summary>
		/// <param name="KeyChart">The key chart.</param>
		/// <param name="existingSeries">The dictionnary of LocalId, DataSeries.</param>
		/// <param name="serieLocalId">The serie local id.</param>
		/// <param name="serieName">Serie name to be used if a serie is created.</param>
		/// <param name="meter">The meter.</param>
		/// <param name="classification">The classification.</param>
		/// <returns></returns>
		private DataSerie CreateOrGetDataserie(string KeyChart, DataSerieCollection existingSeries, string serieLocalId, string serieName, Meter meter, ClassificationItem classification) {
			DataSerie retVal;

			//Check for series customization
			if (!existingSeries.TryGetDataSerie(serieLocalId, out retVal)) {
				DataSerie groupSerie;

				//Check for meter classifications customizations
				if (classification != null && classification.LocalIdPath != null && existingSeries.TryGetDataSerieByClassificationLocalIdPath(classification.LocalIdPath, out groupSerie)) {
					retVal = groupSerie.Copy(serieLocalId);
					retVal.Name = serieName;
					// We have to set the KeyClassificationItem to null otherwise multiple series will be considered as Classification Series and filtered on the ChartReduce
					retVal.KeyClassificationItem = null;
					retVal.KeyChart = KeyChart;
					retVal.GroupOperation = meter != null ? meter.GroupOperation : groupSerie.GroupOperation;
				}
				else {

					retVal = new DataSerie {
						KeyChart = KeyChart,
						Name = serieName,
						LocalId = serieLocalId,
						GroupOperation = meter != null ? meter.GroupOperation : MathematicOperator.SUM,
						DisplayLegendEntry = DataSerieLegendEntry.SUM,
						Unit = meter != null ? meter.UnitOutput : "",
						Hidden = false
					};
					if (meter != null) {
						switch (meter.GroupOperation) {
							case MathematicOperator.AVG:
								retVal.DisplayLegendEntry = DataSerieLegendEntry.AVG;
								break;

							case MathematicOperator.MAX:
								retVal.DisplayLegendEntry = DataSerieLegendEntry.MAX;
								break;

							case MathematicOperator.MIN:
								retVal.DisplayLegendEntry = DataSerieLegendEntry.MIN;
								break;

							case MathematicOperator.SUM:
								retVal.DisplayLegendEntry = DataSerieLegendEntry.SUM;
								break;
						}
					}
				}
			}
			if (classification != null) {
				retVal.ClassificationItemTitle = classification.Title;
				retVal.ClassificationItemLongPath = classification.LongPath;
				retVal.ClassificationItemLocalIdPath = classification.LocalIdPath;
			}
			//we have to do this because be default dataseries are created with the sum grouping operation.
			if (meter != null)
				retVal.GroupOperation = meter.GroupOperation;
			return retVal;
		}
		#endregion

		#region ApplyDataSerieCalculation
		/// <summary>
		/// Applies the data serie calculation.
		/// </summary>
		/// <param name="series">The series.</param>
		public void ApplyDataSerieCalculation(List<DataSerie> series) {
			//series.ForEach(s => {
			foreach (var s in series) {
				#region Ratio
				if (s.Ratio.HasValue && Math.Abs(s.Ratio.Value) > 0 && s.Ratio.Value != 1) {
					s.Points.ForEach(p => {
						if (p.YValue.HasValue)
							p.YValue /= s.Ratio;
					});
					s.Ratio = 1;
				}
				#endregion
			}

		}
		#endregion

		#region GroupByTimeInterval

		/// <summary>
		/// Groups the Chart by time interval.
		/// </summary>
		/// <param name="seriesWithMeterData">The series with meter data.</param>
		/// <param name="interval">The interval.</param>
		/// <param name="hideMeterDataValidity"> </param>
		/// <param name="timeZone">The time zone.</param>
		/// <param name="startDayOfWeek">The start day of week.</param>
		private void GroupListDataSerieByTimeInterval(List<DataSerie> seriesWithMeterData, AxisTimeInterval interval, bool hideMeterDataValidity, TimeZoneInfo timeZone = null, int startDayOfWeek = 1) {
			foreach (var s in seriesWithMeterData) {
				GroupDataSerieByTimeInterval(s, interval, hideMeterDataValidity, timeZone, startDayOfWeek);
			}
		}

		/// <summary>
		/// Groups the single Serie by time interval.
		/// </summary>
		/// <param name="serie">The serie with meter data.</param>
		/// <param name="interval">The interval.</param>
		/// <param name="hideMeterDataValidity">if set to <c>true</c> [hide meter data validity].</param>
		/// <param name="timeZone">The time zone.</param>
		/// <param name="startDayOfWeek">The start day of week.</param>
		private void GroupDataSerieByTimeInterval(DataSerie serie, AxisTimeInterval interval, bool hideMeterDataValidity, TimeZoneInfo timeZone = null, int startDayOfWeek = 1) {
			List<MD> data = serie.MDData;
			if (data != null && data.Count > 0){

				ITimeIntervalAggregator dataIntervalAggregator = EnergyAggregatorHelper.GetTimeAggregator(interval);
				AggregationResult result =  dataIntervalAggregator.Aggregate(serie.MDData, serie.GroupOperation, timeZone, startDayOfWeek);

				if (!result.Buckets.Any()){
					return;
				}
				foreach (var bucket in result.Buckets) {

					var dp = (new DataPoint() {
						XDateTime = bucket.DateTime.GetValueOrDefault(),
						YValue = bucket.Value,
						Name = bucket.Label,
					});
					if (!hideMeterDataValidity) {
						//if the datapoint contains an invalid meterdata, we mark it as invalid.
						if (bucket.MeterDataValidity == MeterDataValidity.Invalid) {
							dp.Highlight = DataPointHighlight.Border;
							dp.HighlightColor = "ff0000";
						}
						else if (bucket.MeterDataValidity == MeterDataValidity.Estimated) {
							dp.Highlight = DataPointHighlight.Border;
							dp.HighlightColor = "0000ff";
						}
					}
					serie.Points.Add(dp);
				}
			}
			serie.MDData = null;
		}


		/// <summary>
		/// Group a MD list by timeInterval based on the Mathematic Operator.
		/// </summary>
		/// <param name="data">The data.</param>
		/// <param name="interval">The interval.</param>
		/// <param name="op">The MathematicOperator.</param>
		/// <param name="timeZone">The time zone.</param>
		/// <returns></returns>
		private List<MD> GroupListMeterDataByTimeIntervalToList(List<MD> data, AxisTimeInterval interval, MathematicOperator op, TimeZoneInfo timeZone = null) {
			IEnumerable<MD> retVal;
			if (interval == AxisTimeInterval.None) {
				retVal = data.Select(md => new MD {
					AcquisitionDateTime = md.AcquisitionDateTime,
					Value = md.Value,
					Validity = md.Validity,
					KeyMeterData = md.KeyMeterData
				});
			}
			else {
				ITimeIntervalAggregator dataIntervalAggregator = EnergyAggregatorHelper.GetTimeAggregator(interval);
				AggregationResult result = dataIntervalAggregator.Aggregate(data, op, timeZone);

			    var useTimeZone = timeZone != null && !timeZone.Equals(TimeZoneInfo.Utc);
				retVal = result.Buckets.Select(t => new MD {
					AcquisitionDateTime = useTimeZone ? t.DateTime.Value.ConvertTimeToUtc(timeZone) : t.DateTime.Value,
					Value = t.Value,
					Validity = (byte)t.MeterDataValidity.ParseAsInt()
				});
			}
			return retVal.ToList();
		}

		/// <summary>
		/// Changes the meter data to specific time zone.
		/// </summary>
		/// <param name="data">The data.</param>
		/// <param name="timeZone">The time zone.</param>
		/// <param name="useParallel">if set to <c>true</c> [use parallel].</param>
		/// <returns></returns>
		private List<MD> ChangeMeterDataToSpecificTimeZone(List<MD> data, TimeZoneInfo timeZone, bool useParallel = true) {
			var retVal = new List<MD>();
			if (data == null) 
				return retVal;
			if (timeZone.Equals(TimeZoneInfo.Utc)) 
				return data;

			if (useParallel) {
				retVal = data.AsParallel().Select(md => {
					var changedDate = md.AcquisitionDateTime.ConvertTimeFromUtc(timeZone);
					return new MD {
						KeyMeter = md.KeyMeter,
						KeyMeterData = md.KeyMeterData,
						AcquisitionDateTime = changedDate,
						Value = md.Value,
						Validity = md.Validity
					};
				}).ToList();
			}
			else {
				retVal.AddRange(from md in data
								let changedDate = md.AcquisitionDateTime.ConvertTimeFromUtc(timeZone)
								select new MD {
									KeyMeter = md.KeyMeter,
									KeyMeterData = md.KeyMeterData,
									AcquisitionDateTime = changedDate,
									Value = md.Value,
									Validity = md.Validity
								});
			}
			return retVal;
		}

		/// <summary>
		/// Changes the meter data to UTC time zone.
		/// </summary>
		/// <param name="data">The data.</param>
		/// <param name="timeZone">The time zone.</param>
		/// <param name="useParallel">if set to <c>true</c> [use parallel].</param>
		/// <returns></returns>
		private List<MD> ChangeMeterDataToUtcTimeZone(List<MD> data, TimeZoneInfo timeZone, bool useParallel = true) {
			var retVal = new List<MD>();
			if (data == null) return retVal;
			if (timeZone == TimeZoneInfo.Utc) return data;
			if (useParallel) {
				retVal = data.AsParallel().Select(md => {
					var changedDate = md.AcquisitionDateTime.ConvertTimeToUtc(timeZone);
					return new MD {
						KeyMeter = md.KeyMeter,
						KeyMeterData = md.KeyMeterData,
						AcquisitionDateTime = changedDate,
						Value = md.Value,
						Validity = md.Validity
					};
				}).ToList();
			}
			else {
				retVal.AddRange(from md in data
								let changedDate = md.AcquisitionDateTime.ConvertTimeToUtc(timeZone)
								select new MD {
									KeyMeter = md.KeyMeter,
									KeyMeterData = md.KeyMeterData,
									AcquisitionDateTime = changedDate,
									Value = md.Value,
									Validity = md.Validity
								});
			}
			return retVal;
		}

		#endregion

		#region GenerateCalculatedSeries
        /// <summary>
        /// Generates the calculated series.
        /// </summary>
        /// <param name="keyChart">The key chart.</param>
        /// <param name="useSpatialPath">if set to <c>true</c> use spatial path.</param>
        /// <param name="useClassificationPath">if set to <c>true</c> use classification path.</param>
        /// <param name="calculations">The different calculations.</param>
        /// <param name="series">The data.</param>
        /// <param name="existingSeries">The existing series saved to the database.</param>
        /// <param name="historicalAnalysis">The historical analysis.</param>
        /// <returns></returns>
		internal List<string> GenerateCalculatedSeries(string keyChart, bool useSpatialPath,bool useClassificationPath, List<CalculatedSerie> calculations, List<DataSerie> series, DataSerieCollection existingSeries, ChartHistoricalAnalysis historicalAnalysis) {
			var errors = new List<string>();
			// we need to processthe calculation order by order so we can apply the limits
			if (calculations != null && calculations.Count > 0) {
				
				List<string> allClassificationsUsedInCalculatedSeries = GetAllClassificationsUsedInCalculatedSeries(calculations);
				string validationResult = ValidateCalculatedSeriesPerLocation(allClassificationsUsedInCalculatedSeries, series);
				if (!String.IsNullOrEmpty(validationResult)) {
					errors.Add(validationResult);
					TracingService.Write(TraceEntrySeverity.Error, validationResult, "Chart_Process", keyChart);
				}
				

				foreach (var calc in calculations.Where(c => c.RunSuccessfully == false && (c.RunOnlyInMainTimeline == false || historicalAnalysis == null)).OrderBy(c => c.Order).ToList()) {
					var retVal = new List<DataSerie>();
					
					var updateCalculations = UpdateCalculatedSeriesFromClassificationItem(new List<CalculatedSerie> { calc }, series, useSpatialPath);


					
					List<Tuple<DataSerie, CalculatedSerie, Exception>> results =
						FormulaEvaluatorService.Evaluate(series, updateCalculations,
							new EAFormulaEvaluatorContext(keyChart, errors));


					if (results != null) {
						foreach (var tuple in results) {
							try {
								var r = tuple.Item1;
								var c = tuple.Item2;
								var e = tuple.Item3;	// NOTE: Exception is not used here and no longer delivered from the FormulaEvaluatorService because errors are handled by the Context passed to the service
								List<DataPoint> pointsWithValue = null;
								if (r != null) {
									pointsWithValue = r.PointsWithValue;
								}
								if (pointsWithValue != null && (pointsWithValue.Count > 1 || (pointsWithValue.Count == 1 && pointsWithValue[0].XDateTime.HasValue))) {
									c.RunSuccessfully = true;

									#region Historical Change

									if (historicalAnalysis != null) {
										r.Name += string.Format(" - {0}", historicalAnalysis.Name);
										r.LocalId += string.Format(" - {0}", historicalAnalysis.LocalId);
									}

									#endregion

									#region Classification Search

									ClassificationItem classification = null;
									if (!string.IsNullOrEmpty(c.KeyClassificationItem)) {
										classification = tenantContainer.Entry.ClassificationItemHierarchy[c.KeyClassificationItem];
									}

									#endregion

									#region Serie Create or Get

									var calculateSerie = CreateOrGetDataserie(keyChart, existingSeries, r.LocalId, r.LocalId, null, classification);
									if (calculateSerie.Name == calculateSerie.LocalId)
										calculateSerie.Name = c.Name;
									if (string.IsNullOrEmpty(calculateSerie.Unit))
										calculateSerie.Unit = c.Unit;
									calculateSerie.Points = r.Points;

									#endregion

									#region Classification Assign

									calculateSerie.ClassificationName = classification != null ? classification.Title : calc.Name;
									calculateSerie.ClassificationItemLocalIdPath = classification != null ? classification.LocalIdPath : calc.Name;

									#endregion

									#region Location Assign

									//we need to assign the location info when we are in a TimeInterval None mode for DataSerie generated from calculation on classification.
									calculateSerie.KeyLocation = r.KeyLocation;
									if (!string.IsNullOrEmpty(c.KeyLocation)) {
										var loc = tenantContainer.Entry.SpatialHierarchy[c.KeyLocation];
										calculateSerie.KeyLocation = loc.KeyLocation;
										calculateSerie.LocationName = useSpatialPath ? loc.LongPath : loc.Name;
									}

									#endregion

									#region Limit and Color Elements

									LimitElements(new List<DataSerie> { calculateSerie }, false);
									ColorElements(new List<DataSerie> { calculateSerie });

									#endregion

									retVal.Add(calculateSerie);
									series.Add(calculateSerie);
								}
								else {
			// moved into FormulaEvaluatorService						errors.Add(string.Format(Langue.msg_error_calculatedserie_invalidformula, c.Formula));
								}
							}
							catch (Exception e) {
								errors.Add(e.Message + " " + e.StackTrace);
							}
						}
					}
					ApplyDataSerieCalculation(retVal);
				}
			}
			return errors;
		}

		class EAFormulaEvaluatorContext : StringListFormulaEvaluatorContext {
			private string m_KeyChart;

			public EAFormulaEvaluatorContext(string keyChart, List<string> errors) : base(errors) {
				m_KeyChart = keyChart;
			}

			public override void ReportError(string message) {
				base.ReportError(message);
				TracingService.Write(TraceEntrySeverity.Warning, message, "Chart_Process", m_KeyChart);
			}
		}

		/// <summary>
		/// Validates the calculated series per location.
		/// </summary>
		/// <param name="allClassificationsUsedInCalculatedSeries">All classifications used in calculated series.</param>
		/// <param name="series">The series.</param>
		/// <returns></returns>
		private string ValidateCalculatedSeriesPerLocation(List<string> allClassificationsUsedInCalculatedSeries, List<DataSerie> series) {
			var dataSeriesByLocation = GroupSeriesByKeyLocalisation(series);
			foreach (var kvp in dataSeriesByLocation) {
				foreach (var classificationsUsedInCalculatedSeries in allClassificationsUsedInCalculatedSeries) {
					if (kvp.Value.Count(serie => serie.ClassificationItemLocalIdPath == classificationsUsedInCalculatedSeries) > 1)
						return string.Format(Langue.msg_error_calculatedserie_multiple_series_with_same_keylocation_and_same_classification, classificationsUsedInCalculatedSeries, tenantContainer.Entry.SpatialHierarchy[kvp.Key].LongPath);
				}
			}
			return string.Empty;
		}


		/// <summary>
		/// Gets all classifications used in calculated series.
		/// </summary>
		/// <param name="calculations">The calculations.</param>
		/// <returns></returns>
		private List<string> GetAllClassificationsUsedInCalculatedSeries(List<CalculatedSerie> calculations) {
			HashSet<string> classificationHashSet = new HashSet<string>();
			foreach (var calculatedSerie in calculations) {
				MatchCollection classificationItemsInFormula = Regex.Matches(calculatedSerie.Formula, @"#(.*?)#");
				string[] classificationItems = new string[classificationItemsInFormula.Count];
				for (int i = 0; i < classificationItems.Length; i++) {
					classificationHashSet.Add(classificationItemsInFormula[i].Groups[1].Value);
				}
			}
			return classificationHashSet.ToList();
		}


		/// <summary>
		/// Updates the calculated series from classification item.
		/// </summary>
		/// <param name="calculations">The calculations.</param>
		/// <param name="series">The existing series.</param>
		/// <param name="useSpatialPath">if set to <c>true</c> [use spatial path].</param>
		/// <returns></returns>
		private List<CalculatedSerie> UpdateCalculatedSeriesFromClassificationItem(List<CalculatedSerie> calculations, List<DataSerie> series, bool useSpatialPath) {
			List<CalculatedSerie> retVal = new List<CalculatedSerie>();
			//we store in here the localid of the calculations that contained classificationitem tag # and the generated calculatedseries
			//in order to update other calculations that could be based on the updated ones.
			Dictionary<string, List<CalculatedSerie>> updatedCalculations = new Dictionary<string, List<CalculatedSerie>>();

			#region Group Series By Localisation
			//We go through each existing serie, and we look for its assigned location to create a list of all the localisation.
			var seriesGroupByLocalisation = GroupSeriesByKeyLocalisation(series);

			#endregion
			//We go through each calculation
			foreach (var calculatedSerie in calculations) {
				if (calculatedSerie.Formula.Contains("%")) {
					calculatedSerie.Formula = Helper.LocalizeInText(calculatedSerie.Formula, "%");
				}
				if (calculatedSerie.Formula.Contains("#")) {
					#region ClassificationItem tags extraction
					//if the formula contains one or more classification tag
					MatchCollection classificationItemsInFormula = Regex.Matches(calculatedSerie.Formula, @"#(.*?)#");
					string[] classificationItems = new string[classificationItemsInFormula.Count];
					for (int i = 0; i < classificationItems.Length; i++) {
						classificationItems[i] = classificationItemsInFormula[i].Groups[1].Value;
					}
					#endregion
					updatedCalculations.Add(calculatedSerie.LocalId, new List<CalculatedSerie>());

					#region Tag replacement by actual series local id
					foreach (string KeyLocation in seriesGroupByLocalisation.Keys) {
						var list = seriesGroupByLocalisation[KeyLocation];
						var finalFormula = calculatedSerie.Formula;
						#region Tags replacement
						list.ForEach(s => {
							foreach (var localidPath in classificationItems) {
								if (s.ClassificationItemLocalIdPath == localidPath) {
									finalFormula = finalFormula.Replace(string.Format("#{0}#", localidPath), s.LocalId);
								}
							}
						});
						#endregion

						#region we remove the remaining tags and replace it by 0.
						//if the formula contains one or more classification tag
						classificationItemsInFormula = Regex.Matches(finalFormula, @"#(.*?)#");
						for (int i = 0; i < classificationItemsInFormula.Count; i++) {
							finalFormula = finalFormula.Replace(string.Format("#{0}#", classificationItemsInFormula[i].Groups[1].Value), "0");
						}
						#endregion

						#region new CalculatedSerie generation
						var newCalc = new CalculatedSerie() {
							Formula = finalFormula,
							HideUsedSeries = calculatedSerie.HideUsedSeries,
							LocalId = string.Format("{0}_{1}", calculatedSerie.LocalId, KeyLocation),
							Name = string.Format("{1} - {0}", calculatedSerie.Name, useSpatialPath ? tenantContainer.Entry.SpatialHierarchy[KeyLocation].LongPath : tenantContainer.Entry.SpatialHierarchy[KeyLocation].Name),
							KeyLocation = KeyLocation,
							Order = calculatedSerie.Order,
							Unit = calculatedSerie.Unit,
							ClassificationItemLocalIdPath = calculatedSerie.ClassificationItemLocalIdPath,
							ClassificationItemTitle = calculatedSerie.ClassificationItemTitle,
							ClassificationItemLongPath = calculatedSerie.ClassificationItemLongPath,
							KeyClassificationItem = calculatedSerie.KeyClassificationItem,
							KeyClassificationItemPath = calculatedSerie.KeyClassificationItemPath,
							KeyLocalizationCulture = calculatedSerie.KeyLocalizationCulture,
							KeyChart = calculatedSerie.KeyChart
						};
						updatedCalculations[calculatedSerie.LocalId].Add(newCalc);
						retVal.Add(newCalc);
						#endregion
					}
					#endregion
				}
				else {
					retVal.Add(calculatedSerie);
				}

			};
			return retVal;
		}

		/// <summary>
		/// Groups the series by key localisation.
		/// </summary>
		/// <param name="series">The series.</param>
		/// <returns></returns>
		private static Dictionary<string, List<DataSerie>> GroupSeriesByKeyLocalisation(List<DataSerie> series) {
			Dictionary<string, List<DataSerie>> seriesGroupByLocalisation = new Dictionary<string, List<DataSerie>>();
			foreach (var serie in series) {
				if (!string.IsNullOrEmpty(serie.KeyLocation)) {
					if (seriesGroupByLocalisation.ContainsKey(serie.KeyLocation)) {
						seriesGroupByLocalisation[serie.KeyLocation].Add(serie);
					}
					else {
						seriesGroupByLocalisation.Add(serie.KeyLocation, new List<DataSerie> { serie });
					}
				}
			}
			return seriesGroupByLocalisation;
		}

		#endregion

		#region GenerateStatisticalSeries
        /// <summary>
        /// Generates the statistical series.
        /// </summary>
        /// <param name="statisticalSeries">The statistical series.</param>
        /// <param name="series">The series.</param>
        /// <param name="existingSeries">The existing series.</param>
        /// <param name="afterCalculatedSeries">if set to <c>true</c> [after calculated series].</param>
        /// <param name="historicalAnalysis">The historical analysis.</param>
        /// <param name="keyChart">The key chart.</param>
        private void GenerateStatisticalSeries(List<StatisticalSerie> statisticalSeries, List<DataSerie> series, DataSerieCollection existingSeries, bool afterCalculatedSeries, ChartHistoricalAnalysis historicalAnalysis, string keyChart)
        {
			if (series != null && series.Count > 0 && statisticalSeries != null && statisticalSeries.Count > 0) {
				var retVal = new List<DataSerie>();
				statisticalSeries.ForEach(statistical => {
					if (statistical.RunAfterCalculatedSeries == afterCalculatedSeries) {
						var points = new Dictionary<object, List<DataPoint>>();
						#region We sort the points per date or name
						foreach (var s in series) {
							if (s.ClassificationItemLocalIdPath == statistical.ClassificationItemLocalIdPath) {
								if (s.Hidden == false || statistical.IncludeHiddenSeries) {
									s.PointsWithValue.ForEach(p => {
										List<DataPoint> col = null;
										var key = p.XDateTime.HasValue ? (object)p.XDateTime.Value : (object)p.Name;
										if (!points.TryGetValue(key, out col)) {
											col = new List<DataPoint>();
											points.Add(key, col);
										}
										col.Add(p);
									});
								}
							}
						}
						#endregion
						if (points.Count > 0) {
							#region Serie Create or Get
							var classification = tenantContainer.Entry.ClassificationItemHierarchy[statistical.KeyClassificationItem];
							var localId = String.Format("{0} - {1}", statistical.Operator.ToString(), classification.Title);// "Statistical " + classification.LocalIdPath + statistical.LocalId;
							var name = String.Format("{0} - {1}", statistical.Operator.GetLocalizedText(), classification.Title);
							var nameExtension = string.Empty;
							if (historicalAnalysis != null) {
								localId += string.Format(" - {0}", String.IsNullOrEmpty(historicalAnalysis.Name) ? historicalAnalysis.KeyChart : historicalAnalysis.Name);
								nameExtension = string.Format(" - {0}", String.IsNullOrEmpty(historicalAnalysis.Name) ? historicalAnalysis.KeyChart : historicalAnalysis.Name);
							}
						    var calculateSerie = CreateOrGetDataserie(keyChart, existingSeries, localId, name, null, null);
							calculateSerie.Name += nameExtension;
							retVal.Add(calculateSerie);
							#endregion
							#region Points calculation
							foreach (var list in points.Values) {
								if (list.Count > 0) {
									var np = list[0].Clone();

									switch (statistical.Operator) {
										case MathematicOperator.AVG:
											np.YValue = list.Average(p => p.YValue);
											break;
										case MathematicOperator.MIN:
											np.YValue = list.Min(p => p.YValue);
											break;
										case MathematicOperator.MAX:
											np.YValue = list.Max(p => p.YValue);
											break;
										case MathematicOperator.SUM:
											np.YValue = list.Sum(p => p.YValue);
											break;

									}

									calculateSerie.Points.Add(np);
								}
							}
							#endregion
						}
					}
				});
				ApplyDataSerieCalculation(retVal);
				series.AddRange(retVal);
			}
		}
		#endregion

		#region SortPoints
		/// <summary>
		/// Sort the list of points for each serie in the collection.
		/// </summary>
		/// <param name="series">The list of DataSeries.</param>
		private void SortPoints(List<DataSerie> series) {
			DataSerieHelper.SortPoints(series);
		}
		#endregion

		#region ApplyLocationName

		/// <summary>
		/// If the chart TimeInterval is set to None, we need to remove the DataPoints XDateTime 
		/// and we need to fill the Name with the Serie LocationName.
		/// </summary>
		/// <param name="chartTimeInterval">The chart time interval.</param>
		/// <param name="series">The series.</param>
		private void ApplyLocationName(AxisTimeInterval chartTimeInterval, List<DataSerie> series) {
			if (chartTimeInterval == AxisTimeInterval.None) {
				series.ForEach(s => {
					s.Points.ForEach(p => {
						p.XDateTime = null;
						p.Name = string.IsNullOrEmpty(s.LocationName) ? s.Name : s.LocationName;
					});
				});
			}
		}
		#endregion

		#region SpecificAnalysis
		/// <summary>
		/// We apply here the Specific Analysis defined in the Chart: Correlation, HeatMap etc...
		/// </summary>
		/// <param name="chart">The chart.</param>
		/// <param name="series">The series.</param>
		private void SpecificAnalysis(Chart chart, List<DataSerie> series) {
			switch (chart.SpecificAnalysis) {
				case ChartSpecificAnalysis.HeatMap:
					SpecificAnalysisHelper.HeatMap(chart, series);
					break;

				case ChartSpecificAnalysis.Correlation:
					SpecificAnalysisHelper.Correlation(chart, series);
					break;

				case ChartSpecificAnalysis.DifferenceHighlight:
					SpecificAnalysisHelper.DifferenceHighlight(chart, series);
					break;

				case ChartSpecificAnalysis.LoadDurationCurve:
					SpecificAnalysisHelper.LoadDurationCurve(chart, series);
					break;
			}
		}
		#endregion

		#region Meter functions

		/// <summary>
		/// Gets the serie local id.
		/// </summary>
		/// <param name="chart">The chart.</param>
		/// <param name="meter">The meter.</param>
		/// <param name="siteMaxLevel">The site TMax level.</param>
		/// <param name="historicalAnalysis">The historical analysis.</param>
		/// <param name="locationName">Name of the final location.</param>
		/// <param name="finalLocation">The final location.</param>
		/// <param name="finalClassificationItem">The final classification item.</param>
		/// <param name="classificationItemHierarchy">The classification item hierarchy.</param>
		/// <param name="spatialHierarchy">The spatial hierarchy.</param>
		/// <returns></returns>
		private IEnumerable<DataSerie> GetSeriesLocalIdAndData(Chart chart, Meter meter, int siteMaxLevel, ChartHistoricalAnalysis historicalAnalysis, out string locationName, out Location finalLocation, out ClassificationItem finalClassificationItem, ConcurrentDictionary<string, ClassificationItem> classificationItemHierarchy, ConcurrentDictionary<string, Location> spatialHierarchy) {
			var retVal = new List<DataSerie>();
			finalLocation = null;
			finalClassificationItem = null;

			#region Classification
			string classificationName = "";
			if (!string.IsNullOrEmpty(meter.KeyClassificationItem)) {

				ClassificationItem classificationItem = finalClassificationItem = classificationItemHierarchy[meter.KeyClassificationItem];
				bool found = true;
				while (found && classificationItem != null && classificationItem.Level > chart.ClassificationLevel) {
					found = classificationItemHierarchy.TryGetValue(classificationItem.KeyParent, out classificationItem);
					if (found)
						finalClassificationItem = classificationItem;
				}

				classificationName = chart.UseClassificationPath ? finalClassificationItem.LongPath : finalClassificationItem.Title;
			}
			else if (!string.IsNullOrEmpty(meter.ClassificationItemTitle)) {
				classificationName = meter.ClassificationItemTitle;
			}

			#endregion
			#region Location
			Location meterLocation = spatialHierarchy[meter.KeyLocation];

			locationName = "";
			//The location description that is used to compose the series name
			string locationDescriptionForSeriesName = string.Empty;

			if (chart.Localisation == ChartLocalisation.ByMeter) {
				finalLocation = meterLocation;
				if (chart.UseSpatialPath) {
					locationName = string.Format("{0} / {1}", finalLocation.LongPath, meter.Name);
					locationDescriptionForSeriesName = chart.HideFullSpatialPath ? meter.Name : locationName;
				}
				else {
					locationName = locationDescriptionForSeriesName = meter.Name;
				}
			}
			else if (chart.Localisation == ChartLocalisation.ByGroupName) {
				finalLocation = meterLocation;
				locationName = locationDescriptionForSeriesName = classificationName;
			}
			else if (chart.LocalisationSpatialTypeName.HasValue){
				// if the required aggregated location is lower than the meterLocationType then we do nothing;
				if (meterLocation.TypeName <= chart.LocalisationSpatialTypeName){
					while (meterLocation.TypeName < chart.LocalisationSpatialTypeName){
						meterLocation = spatialHierarchy[meterLocation.KeyParent];
					}
					// if this is a site go up by level
					if (chart.LocalisationSpatialTypeName.Value == HierarchySpatialTypeName.IfcSite){
						while (chart.LocalisationSiteLevel > (siteMaxLevel - meterLocation.Level)){
							meterLocation = spatialHierarchy[meterLocation.KeyParent];
						}
					}
				}
				finalLocation = meterLocation;
				string locationDescription = string.Empty;
				switch (finalLocation.IfcType){
					case "IfcBuildingStorey":
					{
						locationDescription = finalLocation.Name;
								Location parentLocation;
						if (spatialHierarchy.TryGetValue(finalLocation.KeyParent, out parentLocation)){
							locationDescription = string.Format("{0} / {1}", parentLocation.Name, locationDescription);
								}
							}
							break;
					case "IfcSpace":
					{
						locationDescription = finalLocation.Name;
								Location parentLocation;
						if (spatialHierarchy.TryGetValue(finalLocation.KeyParent, out parentLocation)){
							locationDescription = string.Format("{0} / {1}", parentLocation.Name, locationDescription);
							if (spatialHierarchy.TryGetValue(parentLocation.KeyParent, out parentLocation)){
								locationDescription = string.Format("{0} / {1}", parentLocation.Name, locationDescription);
									}
								}
							}
							break;
						default:
						locationDescription = finalLocation.Name;
							break;

				}

				if (chart.UseSpatialPath){
					locationName = finalLocation.LongPath;
					locationDescriptionForSeriesName = chart.HideFullSpatialPath ? locationDescription : locationName;
					}
				else{
					locationName = locationDescriptionForSeriesName = locationDescription;
				}


			}

			#endregion
			#region Series LocalId and Name

			string name;

			bool isByClassificationGrouping = chart.Localisation == ChartLocalisation.ByGroupName;

			//Construct the series LocalId
			string localId = isByClassificationGrouping
				? classificationName
				: string.Format("{0} - {1}", locationName, classificationName);

			
			//Construct the series name
			if (isByClassificationGrouping){
				name = meter.IsHistoricalPset ? meter.ClassificationItemLongPath : const_classification_name_placeholder;
			}
			else{
				name = meter.IsHistoricalPset
					? string.Format("{0} - {1}", locationDescriptionForSeriesName, meter.ClassificationItemLongPath)
					: (chart.DisplayClassifications ? string.Format("{0} - {1}", locationDescriptionForSeriesName, const_classification_name_placeholder) : locationDescriptionForSeriesName);
			}

			#endregion
			#region Historical Analysis
			if (historicalAnalysis != null) {
				string historicalSeriesSuffix = string.Format(" - {0}", String.IsNullOrEmpty(historicalAnalysis.Name) ? historicalAnalysis.KeyChart : historicalAnalysis.Name);
				localId += historicalSeriesSuffix;
				name += historicalSeriesSuffix;
			}

			#endregion
			#region SplitByEvent mode
			if (chart.CalendarMode == ChartCalendarMode.SplitByEvent && meter.DataByCalendarEvent != null && meter.DataByCalendarEvent.Count > 0) {
				retVal.AddRange(meter.DataByCalendarEvent.Keys.Select(eventType => new DataSerie {
					KeyChart = chart.KeyChart,
					LocalId = string.Format("{0} - {1}", localId, eventType),
					Name = string.Format("{0} - {1}", name, eventType),
					MDData = meter.DataByCalendarEvent[eventType]
				}));
			}
			else {
				var s = new DataSerie { LocalId = localId, Name = name, MDData = meter.MDData };
				retVal.Add(s);
			}
			#endregion



			#region KeyMeter
			if (chart.Localisation == ChartLocalisation.ByMeter) {
				foreach (DataSerie s in retVal) {
					s.KeyMeter = meter.KeyMeter;
				}
			}
			#endregion

			#region Series Name - apply classification customizations

			foreach (DataSerie serie in retVal){

				string seriesClassificationName = classificationName;
				//Get the customized classification name(if exists) only if no series customization exist on that series
				if (!chart.Series.HasSeriesCustomizationFor(serie.LocalId)) {
					seriesClassificationName = GetCustomizedClassificationNameIfNeeded(chart, finalClassificationItem, classificationName);
				}

				if (serie.Name.Contains(const_classification_name_placeholder)) {
					serie.Name = serie.Name.Replace(const_classification_name_placeholder, seriesClassificationName);
				}
			}

			#endregion

			return retVal;
		}

		private static string GetCustomizedClassificationNameIfNeeded(Chart chart, ClassificationItem finalClassificationItem, string seriesClassificationName) {
			DataSerie customizedClassificationSerie;
			string customizedClassificationName = seriesClassificationName; 

			if (finalClassificationItem != null &&
				finalClassificationItem.LocalIdPath != null && 
				chart.Series.TryGetDataSerieByClassificationLocalIdPath(finalClassificationItem.LocalIdPath,
				out customizedClassificationSerie)){
				// When grouped Serie classification is not the meter classification, we'll apply the serie name only if it has changed from the default name.
				customizedClassificationName = customizedClassificationSerie.ClassificationItemLocalIdPath !=
				                               finalClassificationItem.LocalIdPath &&
				                               customizedClassificationSerie.ClassificationItemLongPath ==
				                               customizedClassificationSerie.Name
					? seriesClassificationName
					: customizedClassificationSerie.Name;
			}

			return customizedClassificationName;
		}

		#endregion

		#region MD functions
		/// <summary>
		/// Calculates the MD of an accumulated Meter.
		/// </summary>
		/// <param name="data">The data.</param>
		/// <param name="rollOverValue">The roll over value.</param>
		/// <param name="IsAcquisitionDateTimeEndInterval">if set to <c>true</c> [is acquisition date time end interval].</param>
		/// <returns></returns>
		private List<MD> CalculateAccumulated(List<MD> data, double? rollOverValue, bool IsAcquisitionDateTimeEndInterval) {
			var retVal = new List<MD>();
			if (!(data == null || data.Count == 0)) {
				var previousValue = data[0].Value;
				var rollOverCount = 0;
				for (var i = 1; i < data.Count; i++) {
					var tmpValue = data[i].Value;
					if (rollOverValue.HasValue) {
						if (tmpValue + rollOverCount * rollOverValue.Value < previousValue) {
							rollOverCount++;
						}
						tmpValue += rollOverCount * rollOverValue.Value;
					}
					MD dataToUpdate;
					if (IsAcquisitionDateTimeEndInterval)
						dataToUpdate = data[i];
					else
						dataToUpdate = data[i - 1];
					retVal.Add(new MD() {
						KeyMeter = dataToUpdate.KeyMeter,
						KeyMeterData = dataToUpdate.KeyMeterData,
						AcquisitionDateTime = dataToUpdate.AcquisitionDateTime,
						Value = tmpValue - previousValue,
						Validity = dataToUpdate.Validity
					});
					previousValue = tmpValue;
				}
			}
			return retVal;
		}
		#endregion

		#region AlarmDefinition_Process
		/// <summary>
		/// Process a single AlarmDefinition to generate the AlarmInstances.
		/// </summary>
		/// <param name="context">The context.</param>
		/// <param name="alarm">The alarm.</param>
		/// <returns></returns>
		private IEnumerable<AlarmInstance> AlarmDefinition_ProcessSingle(EnergyAggregatorContext context, AlarmDefinition alarm) {
			#region StartDate and EndDate
			var startDate = tenantContainer.Entry.StartDate;
			if (alarm.LastProcessDateTime.HasValue) {
				//we offset by 30 days just to be safe
				switch (alarm.TimeInterval) {
					case AxisTimeInterval.None:
						startDate = alarm.LastProcessDateTime.Value.AddDays(-7);
						break;
					case AxisTimeInterval.Global:
						startDate = alarm.LastProcessDateTime.Value.AddDays(-30);
						break;
					default:
						startDate = alarm.LastProcessDateTime.Value.AddInterval(alarm.TimeInterval, -4);
						break;
				}
			}
			else if (alarm.StartDate.HasValue) {
				startDate = alarm.StartDate.Value;
			}

			var endDate = DateTime.UtcNow.AddDays(1);
			if (alarm.EndDate.HasValue) {
				endDate = alarm.EndDate.Value;
			}
			#endregion

			using (TracingService.StartTracing("Alarm Definition Processing", alarm.KeyAlarmDefinition)) {
				try {
					var retVal = new List<AlarmInstance>();
					#region Meters
					if (alarm.Meters != null && alarm.Meters.Count > 0) {
						var meters = GetMeterDataRealAndVirtual(alarm.Meters, startDate, endDate, false, true, alarm.TimeInterval);
						if (alarm.CalendarEventCategory != null) {
							FilterByCalendar(meters, startDate, endDate, alarm.CalendarEventCategory, alarm.CalendarMode);//, alarm.GetTimeZone()
						}
						foreach (var meter in meters) {
							var meterDataList = ChangeMeterDataToSpecificTimeZone(meter.MDData, alarm.GetTimeZone());
							List<MD> dataProcessed = GroupListMeterDataByTimeIntervalToList(meterDataList, alarm.TimeInterval, meter.GroupOperation);
							retVal.AddRange(AlarmDefinition_ProcessMeterData(alarm, meter, dataProcessed));
						}
					}
					#endregion
					#region Charts
					if (alarm.Charts != null && alarm.Charts.Count > 0) {
						foreach (var tuple in alarm.Charts) {

							List<Meter> meters;
							List<Location> locations;

							FetchMetersAndLocations(tuple.Item2 != null ? tuple.Item2.Select(m => MeterHelper.ConvertMeterKeyToInt(m.KeyMeter)).ToList() : new List<int>(),
								tuple.Item3 != null ? tuple.Item3.Select(l => l.KeyLocation).ToList() : new List<string>(), out meters, out locations);

							Chart resultingChart = Chart_Process(context, tuple.Item1.KeyChart, FilterHelper.GetFilter<Location>(), null);
							var serieCollection = resultingChart.Series;

							if (!resultingChart.IsEnergyAggregatorLoadingMeterData) {
								if (serieCollection != null && serieCollection.Count > 0) {
									var alarmsFromChartDataPoints = AlarmDefinition_ProcessDataSerieCollection(alarm, serieCollection);
									retVal.AddRange(alarmsFromChartDataPoints);
								}

								// Get alarms created by Algorithms during chart processing.
								if (resultingChart.ScriptAlarmInstances != null) {
									retVal.AddRange(resultingChart.ScriptAlarmInstances);
								}
							}
						}
					}
					#endregion
					return retVal;
				}
				catch (Exception exception) {
					TracingService.Write(exception);
					return new List<AlarmInstance>();
				}
			}
		}

		/// <summary>
		/// Process a single meter against an alarmDefinition to generate the AlarmInstances.
		/// </summary>
		/// <param name="alarm">The alarm.</param>
		/// <param name="meter">The meter.</param>
		/// <param name="meterData">The meter data.</param>
		/// <returns></returns>
		private List<AlarmInstance> AlarmDefinition_ProcessMeterData(AlarmDefinition alarm, Meter meter, List<MD> meterData) {
			var retVal = new List<AlarmInstance>();
			for (var i = 0; i < meterData.Count; i++) {
				double? value = null;
				string description = null;

				var md = meterData[i];
				if (alarm.UsePercentageDelta && i > 0) {
					var mdPrevious = meterData[i - 1];
					if (Math.Abs(mdPrevious.Value) > 0) {
						value = (md.Value - mdPrevious.Value) / mdPrevious.Value * 100d;
						description = value.Value.ToString("f") + "%";
					}
				}
				else if (!alarm.UsePercentageDelta) {
					value = md.Value;
				}
				if (value.HasValue) {
					if (AlarmDefinition_CheckSingleValue(alarm, value.Value)) {
						retVal.Add(GenerateAlarmInstanceFromMeterData(alarm, meter, md, description));
					}
				}
			}
			return retVal;
		}


		/// <summary>
		/// Process a single meter against an alarmDefinition to generate the AlarmInstances.
		/// </summary>
		/// <param name="alarm">The alarm.</param>
		/// <param name="series">The series.</param>
		/// <returns></returns>
		private List<AlarmInstance> AlarmDefinition_ProcessDataSerieCollection(AlarmDefinition alarm, DataSerieCollection series) {
			var retVal = new List<AlarmInstance>();
			series.ToList().ForEach(serie => {
				if (serie.Hidden == false) {
					List<DataPoint> pointsWithValue = serie.PointsWithValue;
					for (var i = 0; i < pointsWithValue.Count; i++) {
						double? value = null;
						string description = null;

						var dp = pointsWithValue[i];
						if (alarm.UsePercentageDelta && i > 0) {
							var dpPrevious = pointsWithValue[i - 1];
							if (Math.Abs(dpPrevious.YValue.Value) > 0) {
								value = (dp.YValue - dpPrevious.YValue) / dpPrevious.YValue * 100d;
								description = value.Value.ToString("f") + "%";
							}
						}
						else if (!alarm.UsePercentageDelta) {
							value = dp.YValue;
						}
						if (value.HasValue) {
							if (AlarmDefinition_CheckSingleValue(alarm, value.Value)) {
								retVal.Add(GenerateAlarmInstanceFromDataSerie(alarm, serie, dp, description));
							}
						}
					}
				}
			});
			return retVal;
		}


		/// <summary>
		/// Generates the alarm instances from meter data.
		/// </summary>
		/// <param name="alarm">The alarm.</param>
		/// <param name="meter">The meter.</param>
		/// <param name="md">The meter data.</param>
		/// <param name="description">The description.</param>
		/// <returns></returns>
		private AlarmInstance GenerateAlarmInstanceFromMeterData(AlarmDefinition alarm, Meter meter, MD md, string description = null) {
			// Convert timezone back to utc, after changing it to alarm timezone for processing.
			var utcAcquisitionDateTime = md.AcquisitionDateTime.ConvertTimeToUtc(alarm.GetTimeZone());

			var retVal = new AlarmInstance {
				InstanceDateTime = utcAcquisitionDateTime,
				KeyAlarmDefinition = alarm.KeyAlarmDefinition,
				AlarmDefinition = alarm,
				Status = AlarmInstanceStatus.Open,
				Value = md.Value,
				KeyMeter = meter.KeyMeter,
				KeyMeterData = md.KeyMeterData == 0 ? null : md.KeyMeterData.ToString(),
				Description = description
			};
			return retVal;
		}

		/// <summary>
		/// Generates the alarm instance from a dataserie's datapoint.
		/// </summary>
		/// <param name="alarm">The alarm.</param>
		/// <param name="serie">The serie.</param>
		/// <param name="point">The point.</param>
		/// <param name="description">The description.</param>
		/// <returns></returns>
		private AlarmInstance GenerateAlarmInstanceFromDataSerie(AlarmDefinition alarm, DataSerie serie, DataPoint point, string description = null) {
			var retVal = new AlarmInstance() {
				InstanceDateTime = point.XDateTime.HasValue ? point.XDateTime.Value : DateTime.UtcNow,
				KeyAlarmDefinition = alarm.KeyAlarmDefinition,
				AlarmDefinition = alarm,
				Status = AlarmInstanceStatus.Open,
				Value = point.YValue.Value,
				KeyDataSerie = serie.KeyDataSerie,
				DataSerieLocalId = serie.LocalId,
				KeyChart = serie.KeyChart,
				Description = description
			};
			return retVal;
		}

		/// <summary>
		/// Returns true if we need to generate an alarm.
		/// </summary>
		/// <param name="alarm">the alarm definition</param>
		/// <param name="value">the value</param>
		/// <returns></returns>
		private bool AlarmDefinition_CheckSingleValue(AlarmDefinition alarm, double value) {
			switch (alarm.Condition) {
				case FilterCondition.Equals:
					if (value == alarm.Threshold) {
						return true;
					}
					break;
				case FilterCondition.NotEquals:
					if (value != alarm.Threshold) {
						return true;
					}
					break;
				case FilterCondition.Between:
					if (value >= alarm.Threshold && value <= alarm.Threshold2) {
						return true;
					}
					break;
				case FilterCondition.Higher:
					if (value >= alarm.Threshold) {
						return true;
					}
					break;

				case FilterCondition.Lower:
					if (value <= alarm.Threshold) {
						return true;
					}
					break;

				case FilterCondition.Outside:
					if (value <= alarm.Threshold || value >= alarm.Threshold2) {
						return true;
					}
					break;
			}
			return false;
		}
		#endregion

		#region MeterValidationRule_Process
		/// <summary>
		/// Process a single MeterValidationRule to generate the AlarmInstances.
		/// </summary>
		/// <param name="rule">The rule.</param>
		/// <returns></returns>
		private List<AlarmInstance> MeterValidationRule_ProcessSingle(MeterValidationRule rule) {
			#region StartDate and EndDate
			var startDate = tenantContainer.Entry.StartDate;
			if (rule.LastProcessDateTime.HasValue) {
				//we offset by 30 days just to be safe
				startDate = rule.LastProcessDateTime.Value.AddDays(-30);
			}
			else if (rule.StartDate.HasValue) {
				startDate = rule.StartDate.Value;
			}

			var endDate = tenantContainer.Entry.EndDate;
			if (rule.EndDate.HasValue) {
				endDate = rule.EndDate.Value;
			}
			#endregion

			var retVal = new List<AlarmInstance>();
			#region Meters
			if (rule.Meters != null && rule.Meters.Count > 0) {
				var meters = GetMeterDataRealAndVirtual(rule.Meters, startDate, endDate, false, true);

				meters.ForEach(meter => {
					retVal.AddRange(MeterValidationRule_ProcessMeterData(rule, meter, meter.MDData));
				});
			}
			#endregion

			return retVal;
		}

		/// <summary>
		/// Process a single meter against an MeterValidationRule to generate the AlarmInstances.
		/// </summary>
		/// <param name="rule">The rule.</param>
		/// <param name="meter">The meter.</param>
		/// <param name="meterData">The meter data.</param>
		/// <returns></returns>
		private List<AlarmInstance> MeterValidationRule_ProcessMeterData(MeterValidationRule rule, Meter meter, List<MD> meterData) {
			var retVal = new List<AlarmInstance>();

			if (meterData == null || meterData.Count == 0 || meter.IsVirtual == true)
				return retVal;

			meterData = meterData.OrderBy(md => md.AcquisitionDateTime).ToList();
			var previousMD = meterData[0];
			var recurringValuesCount = 0;
			for (var i = 0; i < meterData.Count; i++) {
				var currentMD = meterData[i];

				#region ValueOutsideBoundary
				if ((rule.MinValue.HasValue && currentMD.Value < rule.MinValue.Value) || (rule.MaxValue.HasValue && currentMD.Value > rule.MaxValue.Value)) {

					var instance = GenerateAlarmInstanceFromMeterData(rule, meter, currentMD, MeterValidationRuleAlarmType.ValueOutsideBoundary);
					retVal.Add(instance);
				}
				#endregion

				#region IndexDecrease
				if (rule.IncreaseOnly == true && meter.IsAccumulated && currentMD.Value < 0) {//previousMD.Value) {
					var instance = GenerateAlarmInstanceFromMeterData(rule, meter, currentMD, MeterValidationRuleAlarmType.IndexDecrease);
					retVal.Add(instance);
				}
				#endregion

				#region RecurringValues
				if (currentMD.Value == previousMD.Value) {
					recurringValuesCount += 1;
				}
				else {
					recurringValuesCount = 0;
				}
				if (rule.MaxRecurringValues.HasValue && recurringValuesCount >= rule.MaxRecurringValues) {
					var instance = GenerateAlarmInstanceFromMeterData(rule, meter, currentMD, MeterValidationRuleAlarmType.RecurringValues, recurringValuesCount.ToString());
					retVal.Add(instance);
				}
				#endregion

				#region Spike
				if (rule.SpikeDetection.HasValue && Math.Abs(previousMD.Value) > 0) {
					if (rule.SpikeDetectionAbsThreshold.HasValue == false || (rule.SpikeDetectionAbsThreshold.HasValue && Math.Abs(previousMD.Value) > rule.SpikeDetectionAbsThreshold.Value)) {
						var delta = (currentMD.Value - previousMD.Value) / previousMD.Value * 100d;
						if (delta > rule.SpikeDetection.Value) {
							var description = delta.ToString("f") + "%";
							var instance = GenerateAlarmInstanceFromMeterData(rule, meter, currentMD, MeterValidationRuleAlarmType.Spike, description);
							retVal.Add(instance);
						}
					}
				}
				#endregion

				#region Missing
				if (rule.AcquisitionFrequency.HasValue && rule.AcquisitionInterval != AxisTimeInterval.None) {
					var missingDates = MeterValidationRule_GetMissingDateTime(previousMD.AcquisitionDateTime, currentMD.AcquisitionDateTime, rule.AcquisitionFrequency.Value, rule.AcquisitionInterval);
					if (missingDates.Count > 0) {
						var description = missingDates.Count.ToString();
						var instance = GenerateAlarmInstanceFromMeterData(rule, meter, previousMD.AcquisitionDateTime, currentMD.AcquisitionDateTime, MeterValidationRuleAlarmType.Missing, description);
						retVal.Add(instance);
					}
				}
				#endregion

				if (rule.Validity.HasValue && rule.Validity.Value != MeterDataValidity.Invalid) {
					if (currentMD.Validity == rule.Validity.Value.ParseAsInt()) {
						var instance = GenerateAlarmInstanceFromMeterData(rule, meter, currentMD, MeterValidationRuleAlarmType.Validity, rule.Validity.Value.GetLocalizedText());
						retVal.Add(instance);
					}
				}
				previousMD = currentMD;
			}

			#region Late
			if (rule.Delay.HasValue && rule.DelayInterval != AxisTimeInterval.None) {
				var lastMeterDataDateTime = previousMD.AcquisitionDateTime;
				if (lastMeterDataDateTime.AddInterval(rule.DelayInterval, rule.Delay.Value) < DateTime.UtcNow) {
					var instance = GenerateAlarmInstanceFromMeterData(rule, meter, previousMD.AcquisitionDateTime, DateTime.UtcNow, MeterValidationRuleAlarmType.Late);
					retVal.Add(instance);
				}
			}
			#endregion

			return retVal;
		}

		/// <summary>
		///Get missing datetimes.
		/// </summary>
		/// <param name="startDate">The start date.</param>
		/// <param name="endDate">The end date.</param>
		/// <param name="frequency">The frequency.</param>
		/// <param name="interval">The interval.</param>
		/// <returns></returns>
		private List<DateTime> MeterValidationRule_GetMissingDateTime(DateTime startDate, DateTime endDate, int frequency, AxisTimeInterval interval) {
			List<DateTime> retVal = new List<DateTime>();
			if (endDate <= startDate || interval == AxisTimeInterval.None)
				return retVal;

			startDate = startDate.AddInterval(interval, frequency);
			while (startDate < endDate) {
				retVal.Add(startDate);
				startDate = startDate.AddInterval(interval, frequency);
			}
			return retVal;
		}

		/// <summary>
		/// Generates the alarm instances from meter data.
		/// </summary>
		/// <param name="rule">The rule.</param>
		/// <param name="meter">The meter.</param>
		/// <param name="md">The meter data.</param>
		/// <param name="type">The type.</param>
		/// <param name="description">The description.</param>
		/// <returns></returns>
		private AlarmInstance GenerateAlarmInstanceFromMeterData(MeterValidationRule rule, Meter meter, MD md, MeterValidationRuleAlarmType type, string description = null) {
			var retVal = new AlarmInstance() {
				InstanceDateTime = md.AcquisitionDateTime,
				KeyMeterValidationRule = rule.KeyMeterValidationRule,
				KeyMeterData = md.KeyMeterData.ToString(),
				MeterValidationRule = rule,
				MeterValidationRuleAlarmType = type,
				Status = AlarmInstanceStatus.Open,
				Value = md.Value,
				KeyMeter = meter.KeyMeter,
				Description = description,
				Name = meter.Name
			};
			return retVal;
		}

		/// <summary>
		/// Generates the alarm instances for missing element with startdate, enddate.
		/// </summary>
		/// <param name="rule">The rule.</param>
		/// <param name="meter">The meter.</param>
		/// <param name="startDate">The start date.</param>
		/// <param name="endDate">The end date.</param>
		/// <param name="type">The type.</param>
		/// <param name="description">The description.</param>
		/// <returns></returns>
		private AlarmInstance GenerateAlarmInstanceFromMeterData(MeterValidationRule rule, Meter meter, DateTime startDate, DateTime endDate, MeterValidationRuleAlarmType type, string description = null) {
			var retVal = new AlarmInstance() {
				KeyMeterValidationRule = rule.KeyMeterValidationRule,
				MeterValidationRule = rule,
				InstanceDateTime = startDate,
				MeterValidationRuleAlarmType = type,
				MeterDataAcquisitionDateTimeStart = startDate,
				MeterDataAcquisitionDateTimeEnd = endDate,
				Status = AlarmInstanceStatus.Open,
				KeyMeter = meter.KeyMeter,
				Description = description
			};
			return retVal;
		}
		#endregion

		#region LimitAndColorElements
		/// <summary>
		/// Limits elements based on their serie parameters.
		/// </summary>
		/// <param name="series">The series.</param>
		/// <param name="rawData">if set to <c>true</c> we limit [raw data], otherwise we limit aggregated data.</param>
		private void LimitElements(List<DataSerie> series, bool rawData) {
			//series.ForEach(s => {
			foreach (var s in series) {
				#region Limit Elements
				if (s.LimitElementMinValue.HasValue || s.LimitElementMaxValue.HasValue) {
					//here we filter before timeinterval grouping
					if (rawData == true && s.LimitElementRawData == rawData) {
						if (s.LimitElementMinValue.HasValue && s.LimitElementMaxValue.HasValue) {
							s.MDData = s.MDData.Where(m => m.Value >= s.LimitElementMinValue.Value && m.Value <= s.LimitElementMaxValue.Value).ToList();
						}
						else if (s.LimitElementMinValue.HasValue) {
							s.MDData = s.MDData.Where(m => m.Value >= s.LimitElementMinValue.Value).ToList();
						}
						else if (s.LimitElementMaxValue.HasValue) {
							s.MDData = s.MDData.Where(m => m.Value <= s.LimitElementMaxValue.Value).ToList();
						}
						if (s.LimitElementCount && s.MDData.Count > 0) {
							var newData = new List<MD>();
							foreach (var m in s.MDData) {
								newData.Add(new MD() {
									AcquisitionDateTime = m.AcquisitionDateTime,
									KeyMeter = m.KeyMeter,
									KeyMeterData = m.KeyMeterData,
									Validity = m.Validity,
									Value = 1
								});
							};
							s.MDData = newData;
						}

					}
					//here we filter after timeinterval grouping
					if (rawData == false && s.LimitElementRawData == rawData) {
						if (s.LimitElementMinValue.HasValue && s.LimitElementMaxValue.HasValue) {
							s.Points = s.Points.Where(p => p.YValue.HasValue && p.YValue.Value >= s.LimitElementMinValue.Value && p.YValue.Value <= s.LimitElementMaxValue.Value).ToList();
						}
						else if (s.LimitElementMinValue.HasValue) {
							s.Points = s.Points.Where(p => p.YValue.HasValue && p.YValue.Value >= s.LimitElementMinValue.Value).ToList();
						}
						else if (s.LimitElementMaxValue.HasValue) {
							s.Points = s.Points.Where(p => p.YValue.HasValue && p.YValue.Value <= s.LimitElementMaxValue.Value).ToList();
						}
						if (s.LimitElementCount) {
							s.PointsWithValue.ForEach(p => p.YValue = 1);
						}
					}
				}
				#endregion
				#region Limit By Legend Entry
				if (rawData == false && (s.LimitByLegendEntryMaxValue.HasValue || s.LimitByLegendEntryMinValue.HasValue)) {
					var legendEntryValue = double.NaN;
					switch (s.DisplayLegendEntry) {
						case DataSerieLegendEntry.AVG:
							legendEntryValue = DataSerieHelper.Avg(s);
							break;
						case DataSerieLegendEntry.MIN:
							legendEntryValue = DataSerieHelper.Min(s);
							break;
						case DataSerieLegendEntry.MAX:
							legendEntryValue = DataSerieHelper.Max(s);
							break;
						case DataSerieLegendEntry.SUM:
							legendEntryValue = DataSerieHelper.Sum(s);
							break;

					}
					if (!double.IsNaN(legendEntryValue)) {
						if (s.LimitByLegendEntryMaxValue.HasValue && s.LimitByLegendEntryMinValue.HasValue && (legendEntryValue < s.LimitByLegendEntryMinValue || legendEntryValue > s.LimitByLegendEntryMaxValue)) {
							s.Hidden = true;
						}
						else if (s.LimitByLegendEntryMaxValue.HasValue && !s.LimitByLegendEntryMinValue.HasValue && (legendEntryValue > s.LimitByLegendEntryMaxValue)) {
							s.Hidden = true;
						}
						else if (!s.LimitByLegendEntryMaxValue.HasValue && s.LimitByLegendEntryMinValue.HasValue && (legendEntryValue < s.LimitByLegendEntryMinValue)) {
							s.Hidden = true;
						}
					}
				}
				#endregion
			};
		}

		/// <summary>
		/// Colors the elements.
		/// </summary>
		/// <param name="series">The series.</param>
		private void ColorElements(List<DataSerie> series) {
			foreach (var s in series) {
				if (s.ColorElements != null && s.ColorElements.Count > 0) {
					foreach (var colorElement in s.ColorElements) {
						if (!string.IsNullOrEmpty(colorElement.Color)) {
							List<DataPoint> points = null;
							if (colorElement.MinValue.HasValue && colorElement.MaxValue.HasValue) {
								points = s.Points.Where(p => p.YValue.HasValue && p.YValue.Value >= colorElement.MinValue.Value && p.YValue.Value < colorElement.MaxValue.Value).ToList();
							}
							else if (colorElement.MinValue.HasValue) {
								points = s.Points.Where(p => p.YValue.HasValue && p.YValue.Value >= colorElement.MinValue.Value).ToList();
							}
							else if (colorElement.MaxValue.HasValue) {
								points = s.Points.Where(p => p.YValue.HasValue && p.YValue.Value < colorElement.MaxValue.Value).ToList();
							}
							if (points != null && points.Count > 0) {
								foreach (var p in points) {
									p.HighlightColor = colorElement.Color;
									p.Highlight = colorElement.Highlight;
								}
							}
						}
					}
				}
			}
		}
		#endregion

		#region ApplyPsetRatio

		/// <summary>
		/// Applies the pset ratio.
		/// </summary>
		/// <param name="series">The series.</param>
		/// <param name="interval">The interval.</param>
		/// <param name="startDate">The start date.</param>
		/// <param name="endDate">The end date.</param>
		/// <param name="hideMeterDataValidity">if set to <c>true</c> [hide meter data validity].</param>
		/// <param name="timeZone">The time zone.</param>
		/// <returns></returns>
		public List<DataSerie> ApplyPsetRatios(List<DataSerie> series, AxisTimeInterval interval, DateTime startDate, DateTime endDate, bool hideMeterDataValidity, TimeZoneInfo timeZone = null) {
			//var retVal = new List<DataSerie>();
			//series.ForEach(serie => retVal.Add(ApplyPsetRatios(serie, interval, startDate, endDate, timeZone)));
			//return retVal;
			var spatialHierarchy = tenantContainer.Entry.SpatialHierarchy;
			var historicalData = tenantContainer.Entry.HistoricalData;
			var meters = tenantContainer.Entry.Meters;
			//AmirTodo: check if the parallel here helps or hurts performance.
			var retVal = series.AsParallel().Select(serie => ApplyPsetRatios(spatialHierarchy, historicalData, meters, serie, interval, startDate, endDate, hideMeterDataValidity, timeZone)).ToList();

			return retVal;
		}

		/// <summary>
		/// Applies the pset ratios.
		/// </summary>
		/// <param name="spatialHierarchy">The spatial hierarchy.</param>
		/// <param name="historicalData">The historical data.</param>
		/// <param name="meters">The meters.</param>
		/// <param name="serie">The serie.</param>
		/// <param name="interval">The interval.</param>
		/// <param name="startDate">The start date.</param>
		/// <param name="endDate">The end date.</param>
		/// <param name="hideMeterDataValidity"> </param>
		/// <param name="timeZone">The time zone.</param>
		/// <returns></returns>
		public DataSerie ApplyPsetRatios(ConcurrentDictionary<string, Location> spatialHierarchy, ConcurrentDictionary<string, PsetAttributeHistorical> historicalData, ConcurrentDictionary<int, Meter> meters, DataSerie serie, AxisTimeInterval interval, DateTime startDate, DateTime endDate, bool hideMeterDataValidity, TimeZoneInfo timeZone = null) {
			var ratios = serie.PsetRatios;
			if (ratios != null && ratios.Count > 0) {
				var psetWithLoc = ratios.ToDictionary(pset => pset, pset => ApplyPsetRatiosGetLocations(spatialHierarchy, meters, serie, pset));

				serie = psetWithLoc.Aggregate(serie, (current, kvp) => ApplyPsetRatioUnique(spatialHierarchy, historicalData, meters, current, kvp.Key, kvp.Value, interval, startDate, endDate, hideMeterDataValidity, timeZone));
			}
			return serie;
		}

		/// <summary>
		/// Get the locations when applying a pset ratio.
		/// </summary>
		/// <param name="spatialHierarchy">The spatial hierarchy.</param>
		/// <param name="meters">The meters.</param>
		/// <param name="serie">The serie.</param>
		/// <param name="pset">The pset.</param>
		/// <returns></returns>
		private List<string> ApplyPsetRatiosGetLocations(ConcurrentDictionary<string, Location> spatialHierarchy, ConcurrentDictionary<int, Meter> meters, DataSerie serie, DataSeriePsetRatio pset) {
			var psetLocFilter = new List<string>();
			if ((serie.MDData != null && serie.MDData.Count > 0) && string.IsNullOrEmpty(serie.KeyLocation) == false) {
				//we select the list of different meters that compose this dataserie
				var keys = serie.MDData.Select(md => md.KeyMeter).Distinct().ToList();
				//if the pset is at the meter level, nothing else to do
				if (pset.UsageName == "IfcMeter") {
					keys.ForEach(keyMeter => psetLocFilter.Add(MeterHelper.ConvertMeterKeyToString(keyMeter)));
				}
				//if the dataserie is in fact coming from historical data , then it doesnt have meters
				else if (keys.Count == 1 && keys[0] == 0) {
					//psetLocFilter.AddRange(GetSpatialHierarchyFromLocation(serie.KeyLocation, spatialHierarchy));
					//changed because of bug 2588
					psetLocFilter.Add(serie.KeyLocation);
				}
				//if not for each meter we go up the spatial hierarchy until we reach the dataserie level and we make sure to take only each location once.
				else {
					//var serieLocation = spatialHierarchy[serie.KeyLocation];
					psetLocFilter.AddRange(keys.Select(keyMeter => spatialHierarchy[meters[keyMeter].KeyLocation]).Select(loc => loc.KeyLocation));
				}
			}
			return psetLocFilter;
		}

		/// <summary>
		/// Applies the pset ratio unique.
		/// </summary>
		/// <param name="spatialHierarchy">The spatial hierarchy.</param>
		/// <param name="historicalData">The historical data.</param>
		/// <param name="meters">The meters.</param>
		/// <param name="serie">The serie.</param>
		/// <param name="pset">The pset.</param>
		/// <param name="psetLocFilter">The pset loc filter.</param>
		/// <param name="interval">The interval.</param>
		/// <param name="startDate">The start date.</param>
		/// <param name="endDate">The end date.</param>
		/// <param name="hideMeterDataValidity"> </param>
		/// <param name="timeZone">The time zone.</param>
		/// <returns></returns>
		public DataSerie ApplyPsetRatioUnique(ConcurrentDictionary<string, Location> spatialHierarchy, ConcurrentDictionary<string, PsetAttributeHistorical> historicalData, ConcurrentDictionary<int, Meter> meters, DataSerie serie, DataSeriePsetRatio pset, List<string> psetLocFilter, AxisTimeInterval interval, DateTime startDate, DateTime endDate, bool hideMeterDataValidity, TimeZoneInfo timeZone = null) {
			if (((serie.MDData != null && serie.MDData.Count > 0) || (serie.Points != null && serie.Points.Count > 0)) && string.IsNullOrEmpty(serie.KeyLocation) == false && psetLocFilter != null) {
				DataSerie seriePset = null;
				int seriePsetCountForAverage = 1;
				var foundLocations = new List<string>();
				foreach (var key in psetLocFilter) {
					List<string> branch = null;
					if (pset.UsageName == "IfcMeter") {
						branch = new List<string> { key };
					}
					else {
						branch = GetSpatialHierarchyFromLocation(key, spatialHierarchy);
					}
					//for each found location we look to see if it contains the correct data.
					foreach (var locKey in branch) {
						//we create a new dataserie to hold the pset values.
						var seriePsetNew = new DataSerie {
							Name = pset.PsetName + " - " + pset.AttributeName,
							GroupOperation = pset.GroupOperation,
							MDData = new List<MD>()
						};
						bool locationHasData = false;
						foreach (var data in historicalData.Values) {
							if (data.KeyObject == locKey && data.KeyPropertySingleValue == pset.KeyPropertySingleValue) {
								locationHasData = true;
								if (foundLocations.Contains(locKey) == false) {
									seriePsetNew.MDData.Add(new MD {
										AcquisitionDateTime = data.AttributeAcquisitionDateTime,
										Value = data.AttributeValue
									});
								}
							}
						}
						if (locationHasData) {
							//we could be in a situation where locationHasData is true but the seriePsetNew has no data because the location has already been processed.
							//so either we skip the level or we go out if the StopAtFirstLevelEncountered is set to true
							if (seriePsetNew.MDData.Count > 0) {
								//we keep a record of found locations so we dont process them twice.
								foundLocations.Add(locKey);
								// we group the serie and the pset serie by the same time interval in order to be able to do the calculations.
								GroupDataSerieByTimeInterval(seriePsetNew, interval, hideMeterDataValidity, timeZone);
								//Here we fill the gaps for the pset dataserie (if we re not using the SUM grouping operator)
								if (pset.FillMissingValues) {
									DataSerieHelper.FillMissingValues(seriePsetNew, interval, startDate.AddDays(-1).ConvertTimeFromUtc(timeZone), endDate.AddDays(1).ConvertTimeFromUtc(timeZone), true);
								}
								if (seriePset == null) {
									seriePset = seriePsetNew;
								}
								else {
									if (seriePset != seriePsetNew) {
										seriePsetCountForAverage += 1;
										switch (pset.GroupOperation) {
											case MathematicOperator.AVG:
												seriePset = DataSerieHelper.MathematicOperation(seriePset, seriePsetNew, MathematicOperator.SUM);
												break;

											default:
												seriePset = DataSerieHelper.MathematicOperation(seriePset, seriePsetNew, pset.GroupOperation);
												break;
										}
									}
								}

							}
							if (pset.StopAtFirstLevelEncountered) {
								//we go out of the branch when we have encountered the desired data.
								break;
							}
						}
					}
				}
				if (seriePset != null && seriePset.PointsWithValue.Count > 0) {
					if (pset.GroupOperation == MathematicOperator.AVG) {
						seriePset = DataSerieHelper.ArithmeticOperation(seriePset, seriePsetCountForAverage, ArithmeticOperator.Divide);
					}

					if (serie.MDData.Count > 0) {
						GroupDataSerieByTimeInterval(serie, interval, hideMeterDataValidity, timeZone);
					}

				    var originalSeriesKey = serie.KeyDataSerie;
                    //we compute the ratio calculation.
					serie = DataSerieHelper.ArithmeticOperation(serie, seriePset, pset.Operator);
				    if (!string.IsNullOrWhiteSpace(originalSeriesKey)) {
				        serie.KeyDataSerie = originalSeriesKey;
				    }
				}
			}
			return serie;
		}
		/// <summary>
		/// Gets the spatial hierarchy from location.
		/// </summary>
		/// <param name="KeyLocation">The key location.</param>
		/// <param name="spatialHierarchy">The spatial hierarchy.</param>
		/// <returns></returns>
		private List<string> GetSpatialHierarchyFromLocation(string KeyLocation, ConcurrentDictionary<string, Location> spatialHierarchy) {
			if (spatialHierarchy.ContainsKey(KeyLocation)) {
				var loc = spatialHierarchy[KeyLocation];
				return GetSpatialHierarchyFromLocation(loc, spatialHierarchy);
			}
			return new List<string>();
		}

		/// <summary>
		/// Gets the spatial hierarchy from location.
		/// </summary>
		/// <param name="loc">The loc.</param>
		/// <param name="spatialHierarchy">The spatial hierarchy.</param>
		/// <returns></returns>
		private List<string> GetSpatialHierarchyFromLocation(Location loc, ConcurrentDictionary<string, Location> spatialHierarchy) {
			var psetLocFilter = new List<Location>();
			while (loc != null) {
				psetLocFilter.Add(loc);
				if (string.IsNullOrEmpty(loc.KeyParent))
					loc = null;
				else
					loc = spatialHierarchy[loc.KeyParent];
			}
			return psetLocFilter.OrderByDescending(l => l.Level).Distinct().Select(l => l.KeyLocation).ToList();
		}


		///// <summary>
		///// Applies the pset ratio unique.
		///// </summary>
		///// <param name="spatialHierarchy">The spatial hierarchy.</param>
		///// <param name="historicalData">The historical data.</param>
		///// <param name="meters">The meters.</param>
		///// <param name="serie">The serie.</param>
		///// <param name="pset">The pset.</param>
		///// <param name="interval">The interval.</param>
		///// <param name="startDate">The start date.</param>
		///// <param name="endDate">The end date.</param>
		///// <param name="timeZone">The time zone.</param>
		///// <returns></returns>
		//public DataSerie ApplyPsetRatioUniqueOld(ConcurrentDictionary<string, Location> spatialHierarchy, ConcurrentDictionary<string, PsetAttributeHistorical> historicalData, ConcurrentDictionary<int, Meter> meters, DataSerie serie, DataSeriePsetRatio pset, AxisTimeInterval interval, DateTime startDate, DateTime endDate, TimeZoneInfo timeZone = null) {
		//    if (((serie.MDData != null && serie.MDData.Count > 0) || (serie.Points != null && serie.Points.Count > 0)) && string.IsNullOrEmpty(serie.KeyLocation) == false) {

		//        //we create a new dataserie to hold the pset values.
		//        var seriePset = new DataSerie() {
		//            Name = pset.PsetName + " - " + pset.AttributeName,
		//            GroupOperation = pset.GroupOperation,
		//            MDData = new List<MD>()
		//        };
		//        var psetLocFilter = new List<string>();
		//        if (pset.UsageName == "IfcMeter") {
		//            var keys = serie.MDData.Select(md => md.KeyMeter).Distinct().ToList();
		//            keys.ForEach(keyMeter => psetLocFilter.Add(MeterHelper.ConvertMeterKeyToString(keyMeter)));
		//        }
		//        else {
		//            //We fetch all the parent or child locations from the current location.
		//            var serieLocation = spatialHierarchy[serie.KeyLocation];
		//            var relatedLocations =
		//                (from l in spatialHierarchy.Values
		//                 where l != null && !string.IsNullOrEmpty(l.KeyLocationPath) && !string.IsNullOrEmpty(serieLocation.KeyLocationPath) && (serieLocation.KeyLocationPath.StartsWith(l.KeyLocationPath) || l.KeyLocationPath.StartsWith(serieLocation.KeyLocationPath))
		//                 select l).OrderByDescending(loc => loc.Level).ToList();
		//            relatedLocations.ForEach(loc => psetLocFilter.Add(loc.KeyLocation));
		//        }
		//        //we go up the locations until we find a location that contains the correct data.
		//        foreach (var locKey in psetLocFilter) {
		//            //bool found = false;
		//            foreach (var data in historicalData.Values) {
		//                if (data.KeyObject == locKey && data.KeyPropertySingleValue == pset.KeyPropertySingleValue) {
		//                    seriePset.MDData.Add(new MD {
		//                        AcquisitionDateTime = data.AttributeAcquisitionDateTime,
		//                        Value = data.AttributeValue
		//                    });
		//                    //found = true;
		//                }
		//            }
		//            //if (found)
		//            //	break;
		//        }

		//        if (serie.MDData.Count > 0) {
		//            GroupDataSerieByTimeInterval(serie, interval, timeZone);
		//        }
		//        // we group the serie and the pset serie by the same time interval in order to be able to do the calculations.
		//        GroupDataSerieByTimeInterval(seriePset, interval, timeZone);
		//        //Here we fill the gaps for the pset dataserie (if we re not using the SUM grouping operator)
		//        if (pset.FillMissingValues) {
		//            DataSerieHelper.FillMissingValues(seriePset, interval, startDate, endDate, true);
		//        }
		//        //we compute the ratio calculation.
		//        serie = DataSerieHelper.ArithmeticOperation(serie, seriePset, pset.Operator);
		//    }
		//    return serie;
		//}

		#endregion

		#region RunPython
		/// <summary>
		/// Runs the python script.
		/// </summary>
		/// <param name="chart">The chart.</param>
		/// <param name="series">The series.</param>
		/// <param name="scripts">The scripts.</param>
		private void RunPython(Chart chart, List<DataSerie> series, List<ChartAlgorithm> scripts) {
			if (scripts != null && scripts.Count > 0) {

				ScriptEngine engine = Python.CreateEngine();
				engine.Runtime.LoadAssembly(typeof(Chart).Assembly);

				ScriptScope m_scope = null;
				m_scope = engine.CreateScope();
				m_scope.SetVariable("chart", chart);
				m_scope.SetVariable("series", series);
				m_scope.SetVariable("helper", IronPython.Runtime.Types.DynamicHelpers.GetPythonTypeFromType(typeof(DataSerieHelper)));


				var sb = new StringBuilder();
				sb.AppendLine("import sys");
				sb.AppendLine("import clr");
				sb.AppendLine("clr.AddReference(\"System.Core\")");
				sb.AppendLine("import System");
				sb.AppendLine("clr.AddReference(\"System.Drawing\")");
				sb.AppendLine("import System.Drawing");

				sb.AppendLine("from  Vizelia.FOL.BusinessEntities import *");
				sb.AppendLine("from System import DateTime");
				sb.AppendLine("from System.Drawing import Color");

				var inputs = new Dictionary<string, string>();

				scripts.OrderBy(chartscript => chartscript.Order).ToList().ForEach(chartscript => {
					var script = chartscript.Algorithm;
					var code = script.Code;
					if (chartscript.AlgorithmInputValues != null && chartscript.AlgorithmInputValues.Count > 0) {
						chartscript.AlgorithmInputValues.ForEach(input => {
							inputs.Add(input.Name, input.Value);
						});
					}
					if (script != null && !string.IsNullOrEmpty(script.Code))
						sb.AppendLine(script.Code);
				});
				m_scope.SetVariable("inputs", inputs);

				try {
					var finalCode = sb.ToString();
					Microsoft.Scripting.Hosting.ScriptSource source = engine.CreateScriptSourceFromString(finalCode, SourceCodeKind.AutoDetect);
					source.Execute(m_scope);
				}
				catch (Exception e) {
					chart.Errors.Add(e.Message);
				}

				//Fix for dynamically created series.
				series.ForEach(s => {
					s.KeyChart = chart.KeyChart;
					s.LocalId = string.IsNullOrEmpty(s.LocalId) ? s.Name : s.LocalId;
				});

				scripts.ForEach(chartscript => {
					UpdateAlgorithmInputValues(chartscript.AlgorithmInputValues, inputs);
				});
			}
		}
		#endregion

		#region RunCSharp
		/// <summary>
		/// Runs the C sharp.
		/// </summary>
		/// <param name="context">The context.</param>
		/// <param name="chart">The chart.</param>
		/// <param name="series">The series.</param>
		/// <param name="scripts">The scripts.</param>
		private void RunCSharp(EnergyAggregatorContext context, ref Chart chart, ref List<DataSerie> series, List<ChartAlgorithm> scripts) {
			if (scripts == null) return;

			using (TracingService.StartTracing("Algorithms for chart", chart.KeyChart)) {
				//Stopwatch sw = Stopwatch.StartNew();

				foreach (var script in scripts) {
					var inputs = script.GetAlgorithmInputValuesDictionary();

					List<string> errors;
					Tuple<Chart, List<DataSerie>> retVal = ExecuteInstance(context, chart, series, inputs, script, out errors);
					chart = retVal.Item1;
					series = retVal.Item2;

					if (errors != null) {
						chart.Errors.AddRange(errors);
					}
				}

				//sw.Stop();
				//TracingService.Write("Took " + sw.ElapsedMilliseconds + " ms.");
			}
		}

		/// <summary>
		/// Executes the instance.
		/// </summary>
		/// <param name="context">The context.</param>
		/// <param name="chart">The chart.</param>
		/// <param name="series">The series.</param>
		/// <param name="inputs">The inputs.</param>
		/// <param name="algorithm">The algorithm.</param>
		/// <param name="errors">The errors.</param>
		/// <returns></returns>
		private Tuple<Chart, List<DataSerie>> ExecuteInstance(EnergyAggregatorContext context, Chart chart, List<DataSerie> series, Dictionary<string, string> inputs, ChartAlgorithm algorithm, out List<string> errors) {
			Tuple<Chart, List<DataSerie>> retVal = null;

			try {
				AlgorithmInstance<IAnalyticsExtensibility> instance = CreateAlgorithmInstance(algorithm, out errors);

				if (instance != null) {
					Thread thread = null;
					var locationFilters = ContextHelper.Get("LocationFilter");

					var task = Helper.StartNewThread(() => {
						try {
							thread = Thread.CurrentThread;

							if (locationFilters != null) {
								ContextHelper.Add("LocationFilter", locationFilters);
							}

							using (TracingService.StartTracing("Algorithm Execution", algorithm.AlgorithmName)) {
								Tuple<Chart, List<DataSerie>, Dictionary<string, string>> algorithmResult = instance.Algorithm.Execute(context, chart, series, inputs);
								retVal = new Tuple<Chart, List<DataSerie>>(algorithmResult.Item1, algorithmResult.Item2);

								if (algorithmResult.Item3 != null) {
									// Null returned = no change.
									inputs = algorithmResult.Item3;
								}
							}
						}
						catch (Exception ex) {
							string name = algorithm.Algorithm != null ? algorithm.Algorithm.Name : algorithm.AlgorithmName;
							chart.Errors.Add(string.Format("{0}: {1}", name, ex.Message));
							TracingService.Write(ex);
						}
					});

					task.Wait(VizeliaConfiguration.Instance.AlgorithmsConfiguration.TimeoutInSeconds * 1000);

					if (!task.IsCompleted) {
						if (thread != null) {
							thread.Abort();
							TracingService.Write(algorithm.AlgorithmName + Langue.msg_algorithm_task_aborted);
						}
					}
					else {
						algorithm.AlgorithmInputValues = UpdateAlgorithmInputValues(algorithm.AlgorithmInputValues, inputs, algorithm);
					}
				}
			}
			catch (Exception e) {
				errors = new List<string> { e.ToString() };
			}

			if (retVal == null) {
				// There was an error.
				retVal = new Tuple<Chart, List<DataSerie>>(chart, series);
			}

			return retVal;
		}

		private static AlgorithmInstance<IAnalyticsExtensibility> CreateAlgorithmInstance(ChartAlgorithm script, out List<string> errors) {
			// Create the algorithm and cache it in-memory.
			AlgorithmInstance<IAnalyticsExtensibility> instance = null;
			errors = new List<string>();

			if (script.Algorithm != null) {
				try {
					instance = tenantContainer.Entry.AlgorithmInstances.GetOrAdd(script.KeyAlgorithm, s => {
						List<string> localErrors;

						var localInstance = AlgorithmHelper.CreateInstance<IAnalyticsExtensibility>(script, out localErrors);

						if (localInstance == null || localErrors.Any()) {
							throw new VizeliaException(string.Join("@!@!@!", localErrors));
						}

						return localInstance;
					});
				}
				catch (Exception e) {
					if (e.Message.Contains("@!@!@!")) {
						errors = e.Message.Split(new[] { "@!@!@!" }, StringSplitOptions.RemoveEmptyEntries).ToList();
					}
					else {
						errors = new List<string> { e.Message };
					}
				}
			}
			else {
				instance = AlgorithmHelper.CreateInstance<IAnalyticsExtensibility>(script, out errors);
			}

			return instance;
		}


		/// <summary>
		/// Updates the algorithm input values.
		/// </summary>
		/// <param name="algorithmInputValues">The script input values.</param>
		/// <param name="inputs">The inputs.</param>
		/// <param name="algorithm">The algorithm.</param>
		/// <returns></returns>
		private List<AlgorithmInputValue> UpdateAlgorithmInputValues(List<AlgorithmInputValue> algorithmInputValues, Dictionary<string, string> inputs, ChartAlgorithm algorithm = null) {
			if (algorithmInputValues == null)
				algorithmInputValues = new List<AlgorithmInputValue>();

			foreach (var algorithmInputValue in algorithmInputValues) {
				var oldValue = algorithmInputValue.Value;
				var newValue = inputs[algorithmInputValue.Name];
				if (newValue != oldValue) {
					algorithmInputValue.Value = newValue;
					algorithmInputValue.IsDirty = true;
				}
				inputs.Remove(algorithmInputValue.Name);
			}

			foreach (var input in inputs) {
				var inputValue = new AlgorithmInputValue {
					IsDirty = true,
					KeyChart = algorithm.KeyChart,
					KeyAlgorithmInputDefinition = null,
					KeyAlgorithmInputValue = null,
					KeyAlgorithm = algorithm.KeyAlgorithm,
					Name = input.Key,
					Value = input.Value
				};

				algorithmInputValues.Add(inputValue);
			}

			return algorithmInputValues;
		}
		#endregion

		#region DebugMeters
		/// <summary>
		/// Debugs the meters.
		/// </summary>
		/// <param name="KeyChart">The key chart.</param>
		/// <param name="meters">The meters.</param>
		private void DebugMeters(string KeyChart, List<Meter> meters) {
			foreach (var m in meters) {
				if (m.MDData != null) {
					Debug.WriteLine(string.Format("Chart {0} - Meter {1} - Count {2} - StartDate {3} - EndDate {4}", KeyChart, m.KeyMeter, m.MDData.Count, m.MDData.OrderBy(md => md.AcquisitionDateTime).First().AcquisitionDateTime.ToString(), m.MDData.OrderBy(md => md.AcquisitionDateTime).Last().AcquisitionDateTime.ToString()));
				}
			}

		}
		#endregion

		#region OffsetHistorical
		/// <summary>
		/// Offset Historical analysis dataseries that have the IncludeInMainTimeline set to true and include them in the main collection.
		/// </summary>
        /// <param name="chartHistoricalAnalysis">The chart historical analysis.</param>
		/// <param name="series">The series.</param>
		private void OffsetHistorical(Dictionary<ChartHistoricalAnalysis, DataSerieCollection> chartHistoricalAnalysis, List<DataSerie> series) {
			var historicals = new Dictionary<ChartHistoricalAnalysis, DataSerieCollection>();
		    if (chartHistoricalAnalysis == null) return;

			foreach (var kvp in chartHistoricalAnalysis) {
				var historicalAnalysis = kvp.Key;
				var s = kvp.Value;
				if (historicalAnalysis.IncludeInMainTimeline) {
					DataSerieHelper.OffsetByTimeinterval(s, historicalAnalysis.Frequency, historicalAnalysis.Interval);
					series.AddRange(s.ToList());
				}
				else {
					historicals.Add(kvp.Key, kvp.Value);
				}
			}
            chartHistoricalAnalysis.Clear();
		    foreach (var dataSerieCollection in historicals) {
		        chartHistoricalAnalysis.Add(dataSerieCollection.Key,dataSerieCollection.Value);
			}
		}
		#endregion
		#endregion
	}
}

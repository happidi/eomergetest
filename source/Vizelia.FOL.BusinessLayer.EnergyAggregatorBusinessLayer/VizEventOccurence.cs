using System;

namespace Vizelia.FOL.BusinessLayer {
	/// <summary>
	/// Simple class to hold in UTC the calendar event occurences data
	/// </summary>
	[Serializable]
	public class VizEventOccurence {
		/// <summary>
		/// StartDate
		/// </summary>
		public DateTime StartDate { get; set; }
		/// <summary>
		/// EndDate
		/// </summary>
		public DateTime EndDate { get; set; }
		/// <summary>
		/// Factor
		/// </summary>
		public double? Factor { get; set; }
		/// <summary>
		/// Summary
		/// </summary>
		public string Summary { get; set; }
		/// <summary>
		/// Category
		/// </summary>
		public string Category { get; set; }
	}
}
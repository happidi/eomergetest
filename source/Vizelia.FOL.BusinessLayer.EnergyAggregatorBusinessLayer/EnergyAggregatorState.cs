﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using DDay.iCal;
using Vizelia.FOL.BusinessEntities;
using System.Threading;
using Vizelia.FOL.Providers;
namespace Vizelia.FOL.BusinessLayer {

	/// <summary>
	/// The energy aggregator memory state.
	/// </summary>
	public class EnergyAggregatorState {
		/// <summary>
		/// private static collection of all MeterDatas grouped by KeyMeter;
		/// </summary>
		//private ConcurrentDictionary<int, SortedList<DateTime, MD>> m_MeterDatas = new ConcurrentDictionary<int, SortedList<DateTime, MD>>();
		//private FileBasedDict<Dictionary<DateTime, MD>> m_MeterDatas = new FileBasedDict<Dictionary<DateTime, MD>>(EnergyAggregatorBusinessLayer.Directory);
		private IMeterDataStorage m_MeterDatas = MeterDataService.Providers["InMemory"].GetStorage();
		/// <summary>
		/// private static collection of all Meter grouped by KeyMeter;
		/// </summary>
		private ConcurrentDictionary<int, Meter> m_Meters = new ConcurrentDictionary<int, Meter>();

		/// <summary>
		/// private static collection of all Location entities;
		/// </summary>
		private ConcurrentDictionary<string, Location> m_SpatialHierarchy = new ConcurrentDictionary<string, Location>();

		/// <summary>
		/// private static collection of all ClassificationItem entities;
		/// </summary>
		private ConcurrentDictionary<string, ClassificationItem> m_ClassificationItemHierarchy = new ConcurrentDictionary<string, ClassificationItem>();

		/// <summary>
		/// A private collection of all calendar events occurences between the start date and the end date of this aggregator
		/// </summary>
		private ConcurrentDictionary<string, List<VizEventOccurence>> m_CalendarEventOccurrences = new ConcurrentDictionary<string, List<VizEventOccurence>>();

		/// <summary>
		/// A private collection of all historical data (PsetAttributeHistorical).
		/// </summary>
		private ConcurrentDictionary<string, PsetAttributeHistorical> m_HistoricalData = new ConcurrentDictionary<string, PsetAttributeHistorical>();

		/// <summary>
		/// A private collection of all algorithm instances
		/// </summary>
		private ConcurrentDictionary<string, AlgorithmInstance<IAnalyticsExtensibility>> m_AlgorithmInstances = new ConcurrentDictionary<string, AlgorithmInstance<IAnalyticsExtensibility>>();

		/// <summary>
		/// A private counter for the number of Crud notifications in progress.
		/// </summary>
		private int m_CrudNotificationInProgress = 0;
		/// <summary>
		/// Initializes a new instance of the <see cref="EnergyAggregatorState"/> class.
		/// </summary>
		public EnergyAggregatorState() {
			SiteMaxLevel = 0;
			LoadingMeterData = false;
		}

		/// <summary>
		/// if <T>true</T>, the service is loading meterData.
		/// </summary>
		public bool LoadingMeterData { get; set; }

		/// <summary>
		/// The end date of this instance of energy aggregator.
		/// </summary>
		public DateTime EndDate { get; set; }

		/// <summary>
		/// The start date of this instance of energy aggregator.
		/// </summary>
		public DateTime StartDate { get; set; }

		/// <summary>
		/// Max level of sites in the hierarchy.
		/// </summary>
		public int SiteMaxLevel { get; set; }

		/// <summary>
		/// Gets or sets the algorithm instances.
		/// </summary>
		/// <value>
		/// The algorithm instances.
		/// </value>
		public ConcurrentDictionary<string, AlgorithmInstance<IAnalyticsExtensibility>> AlgorithmInstances {
			get { return m_AlgorithmInstances; }
			set { m_AlgorithmInstances = value; }
		}

		/// <summary>
		/// A private collection of all historical data (PsetAttributeHistorical).
		/// </summary>
		public ConcurrentDictionary<string, PsetAttributeHistorical> HistoricalData {
			get { return m_HistoricalData; }
			set { m_HistoricalData = value; }
		}
		
		/// <summary>
		/// A private collection of all calendar events occurences between the start date and the end date of this aggregator
		/// </summary>
		public ConcurrentDictionary<string, List<VizEventOccurence>> CalendarEventOccurrences {
			get { return m_CalendarEventOccurrences; }
			set { m_CalendarEventOccurrences = value; }
		}

		/// <summary>
		/// private static collection of all ClassificationItem entities;
		/// </summary>
		public ConcurrentDictionary<string, ClassificationItem> ClassificationItemHierarchy {
			get { return m_ClassificationItemHierarchy; }
			set { m_ClassificationItemHierarchy = value; }
		}

		/// <summary>
		/// private static collection of all Location entities;
		/// </summary>
		public ConcurrentDictionary<string, Location> SpatialHierarchy {
			get { return m_SpatialHierarchy; }
			set { m_SpatialHierarchy = value; }
		}

		/// <summary>
		/// private static collection of all Meter grouped by KeyMeter;
		/// </summary>
		public ConcurrentDictionary<int, Meter> Meters {
			get { return m_Meters; }
			set { m_Meters = value; }
		}

		/// <summary>
		/// private static collection of all MeterDatas grouped by KeyMeter;
		/// </summary>
		public IMeterDataStorage MeterDatas {
			get { return m_MeterDatas; }
			set { m_MeterDatas = value; }
		}

		/// <summary>
		/// Gets or sets the crud notification inprogress.
		/// </summary>
		/// <value>
		/// The crud notification inprogress.
		/// </value>
		public int CrudNotificationInProgress {
			get { return m_CrudNotificationInProgress; }
			set { m_CrudNotificationInProgress = value; }
		}

		/// <summary>
		/// Increment the notification in progress counter .
		/// </summary>
		/// <returns></returns>
		public int CrudNotificationInProgressIncrement() {
			Interlocked.Increment(ref m_CrudNotificationInProgress);
			return m_CrudNotificationInProgress;
		}

		/// <summary>
		/// Decrement the notification in progress counter .
		/// </summary>
		/// <returns></returns>
		public int CrudNotificationInProgressDecrement() {
			Interlocked.Decrement(ref m_CrudNotificationInProgress);
			return m_CrudNotificationInProgress;
		}

		/// <summary>
		/// Gets or sets a value indicating whether this instance is initialized.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if this instance is initialized; otherwise, <c>false</c>.
		/// </value>
		public bool IsInitialized { get; set; }
	}
}


﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using Vizelia.FOL.BusinessEntities;
using Extreme.Statistics;
using Vizelia.FOL.Common.Localization;

namespace Vizelia.FOL.Common {
	/// <summary>
	/// Specific analysis helper for HeatMap, Correlation etc...
	/// </summary>
	public static class SpecificAnalysisHelper {

		#region HeatMap
		/// <summary>
		/// Apply the HeatMap specific Analysis : 
		/// instead of displaying the values of each series, we display a Color beetwen red and green representing the value.
		/// </summary>
		/// <param name="chart">The chart.</param>
		/// <param name="series">The series.</param>
		public static void HeatMap(Chart chart, List<DataSerie> series) {
			var min = double.MaxValue;
			var max = double.MinValue;
			series.ForEach(s => s.PointsWithValue.ForEach(p => {
				if (p.YValue < min)
					min = p.YValue.Value;
				if (p.YValue > max)
					max = p.YValue.Value;

			}));

			var minColor = Color.Green;
			var maxColor = Color.Red;
			if (!string.IsNullOrEmpty(chart.HeatMapMinColor))
				minColor = ColorTranslator.FromHtml("#" + chart.HeatMapMinColor);
			if (!string.IsNullOrEmpty(chart.HeatMapMaxColor))
				maxColor = ColorTranslator.FromHtml("#" + chart.HeatMapMaxColor);

			series.ForEach(s => s.PointsWithValue.ForEach(p => {
				p.Color = GetHeatMapColor(minColor, maxColor, min, max, p.YValue.Value);
				p.OverridenYValue = 1;
			}));
		}

		/// <summary>
		/// Gets the color of the heat map.
		/// </summary>
		/// <param name="minColor">Color of the TMin.</param>
		/// <param name="maxColor">Color of the TMax.</param>
		/// <param name="min">The min.</param>
		/// <param name="max">The max.</param>
		/// <param name="val">The val.</param>
		/// <returns></returns>
		private static Color GetHeatMapColor(Color minColor, Color maxColor, double min, double max, double val) {
			double steps = (max - min);
			val = max - val;
			double stepR = (maxColor.R - minColor.R) / (steps - 1);
			double stepG = (maxColor.G - minColor.G) / (steps - 1);
			double stepB = (maxColor.B - minColor.B) / (steps - 1);

			var retVal = Color.FromArgb((byte)(maxColor.R - stepR * val), (byte)(maxColor.G - stepG * val), (byte)(maxColor.B - stepB * val));
			return retVal;

		}
		#endregion
		#region Correlation

		/// <summary>
		/// Apply the Correlation specific Analysis : 
		/// </summary>
		/// <param name="chart">The chart.</param>
		/// <param name="series">The series.</param>
		public static void Correlation(Chart chart, List<DataSerie> series) {
			foreach (var serie in series) {
				if (serie.LocalId.StartsWith(ChartHelper.GetCorrelationCloudDataSerieLocalId()) == false && serie.LocalId.StartsWith(ChartHelper.GetCorrelationLineDataSerieLocalId()) == false)
					serie.Hidden = true;
			}
			CorrelationSub(chart, series, chart.CorrelationXSerieLocalId, chart.CorrelationYSerieLocalId, "");
			CorrelationSub(chart, series, chart.CorrelationXSerieLocalId2, chart.CorrelationYSerieLocalId2, "2");
		}
		/// <summary>
		/// Apply the Correlation specific Analysis :
		/// </summary>
		/// <param name="chart">The chart.</param>
		/// <param name="series">The series.</param>
		/// <param name="xSerieLocalId">The x serie local id.</param>
		/// <param name="ySerieLocalId">The y serie local id.</param>
		/// <param name="index">The index.</param>
		public static void CorrelationSub(Chart chart, List<DataSerie> series, string xSerieLocalId, string ySerieLocalId, string index) {
			if (!string.IsNullOrEmpty(xSerieLocalId) && !string.IsNullOrEmpty(ySerieLocalId)) {

				DataSerie xSerie = null;
				DataSerie ySerie = null;

				foreach (var serie in series) {
					if (serie.LocalId == xSerieLocalId)
						xSerie = serie;
					if (serie.LocalId == ySerieLocalId)
						ySerie = serie;
				}
				if (xSerie != null && ySerie != null) {
					var dictionary = DataSerieHelper.GroupByDateTimeExact(xSerie, ySerie);
					var xValues = new List<double>();
					var yValues = new List<double>();
					var cloudSerie = new DataSerie() {
						LocalId = ChartHelper.GetCorrelationCloudDataSerieLocalId() + index,
						Name = string.Format("{0} {1} {2}", xSerie.Name, Langue.msg_chart_versus, ySerie.Name),
						Hidden = false,
						KeyChart = chart.KeyChart,
						Points = new List<DataPoint>(),
						Type = DataSerieType.Marker,
						HideLegendEntry = true
					};

					var lineSerie = new DataSerie() {
						LocalId = ChartHelper.GetCorrelationLineDataSerieLocalId() + index,
						Name = string.Format("{0} {1} {2} {3}", xSerie.Name, Langue.msg_chart_versus, ySerie.Name, Langue.msg_chart_correlation_line),
						Hidden = false,
						KeyChart = chart.KeyChart,
						Points = new List<DataPoint>(),
						Type = DataSerieType.Line
					};
					foreach (var keyvalue in dictionary) {
						var t = keyvalue.Value;
						var p = new DataPoint {
							Name = keyvalue.Key.ToString(chart.TimeIntervalFormat),
							XValue = t.Item1,
							YValue = t.Item2,
							Tooltip = keyvalue.Key.ToString(chart.TimeIntervalFormat)
						};

						var remove = false;
						if (chart.CorrelationRemovedPoints != null && chart.CorrelationRemovedPoints.Count > 0) {
							chart.CorrelationRemovedPoints.ForEach(dp => {
								if (dp.LocalId == cloudSerie.LocalId) {
									if (Math.Round(dp.YValue, chart.DisplayDecimalPrecision) == Math.Round(p.YValue.Value, chart.DisplayDecimalPrecision) && Math.Round(dp.XValue, chart.DisplayDecimalPrecision) == Math.Round(p.XValue.Value, chart.DisplayDecimalPrecision)) {
										remove = true;
									}
								}
							});
						}
						if (remove) {
							p.Color = Color.Red;
						}
						else {
							xValues.Add(t.Item1);
							yValues.Add(t.Item2);
						}
						cloudSerie.Points.Add(p);
					}

					if (xValues.Count > 0 && yValues.Count > 0) {
						#region Using Extreme.Mathematics
						try {
							var model = new SimpleRegressionModel(yValues.ToArray(), xValues.ToArray());
							model.Compute();
							lineSerie.Name += String.Format(" {0} : {1}", Langue.msg_chart_correlation_rsquared, model.RSquared.ToString("N" + Math.Max(2, chart.DisplayDecimalPrecision)));
							var line = model.GetRegressionLine();
							var linePoints = from point in cloudSerie.PointsWithValue
											 where xValues.Contains(point.XValue.Value)
											 select new DataPoint {
												 Name = point.Name,
												 XValue = point.XValue,
												 YValue = line.ValueAt(point.XValue.Value)
											 };

							lineSerie.Points.AddRange(linePoints);
						}
						#endregion
						#region Custom Implementation
						catch (System.ComponentModel.LicenseException) {
							var lineSimple = SimpleLinearRegression(xValues.ToList(), yValues.ToList());
							lineSerie.Name += String.Format(" {0} : {1}", Langue.msg_chart_correlation_rsquared, lineSimple["Corelation"].ToString("N" + Math.Max(2, chart.DisplayDecimalPrecision)));

							var linePoints = from point in cloudSerie.PointsWithValue
											 where xValues.Contains(point.XValue.Value)
											 select new DataPoint {
												 Name = point.Name,
												 XValue = point.XValue,
												 YValue = lineSimple["Beta"] * point.XValue + lineSimple["Alpha"]
											 };


							lineSerie.Points.AddRange(linePoints);
						}
						#endregion
						series.Add(cloudSerie);
						series.Add(lineSerie);
					}

				}
			}
		}
		#endregion
		#region DifferenceHighlight
		/// <summary>
		/// Highlight the differences beetween the 2 selected DataSerie, and display elements when the % of difference is higher than the defined threshold.
		/// </summary>
		/// <param name="chart">The chart.</param>
		/// <param name="series">The series.</param>
		public static void DifferenceHighlight(Chart chart, List<DataSerie> series) {
			if (string.IsNullOrEmpty(chart.DifferenceHighlightSerie1LocalId) || string.IsNullOrEmpty(chart.DifferenceHighlightSerie2LocalId))
				return;
			DataSerie serie1 = null;
			DataSerie serie2 = null;

			foreach (var serie in series) {
				if (serie.LocalId == chart.DifferenceHighlightSerie1LocalId) { serie1 = serie; }
				else if (serie.LocalId == chart.DifferenceHighlightSerie2LocalId) { serie2 = serie; }
				else { serie.Hidden = true; }
			}

			if (serie1 != null && serie2 != null) {
				serie1.Type = DataSerieType.Line;
				serie2.Type = DataSerieType.Line;
				var differenceSerie = new DataSerie() {
					LocalId = ChartHelper.GetDifferenceHighlightLocalId(),
					Name = Langue.msg_chart_differencehighlight_line,
					Hidden = false,
					KeyChart = chart.KeyChart,
					Points = new List<DataPoint>(),
					Type = DataSerieType.AreaLine,
					ColorR = Color.Yellow.R,
					ColorG = Color.Yellow.G,
					ColorB = Color.Yellow.B,
					HideLegendEntry = true
				};

				var dictionary = DataSerieHelper.GroupByDateTimeExact(serie1, serie2);

				foreach (var kvp in dictionary) {
					if (Math.Abs(kvp.Value.Item1) > 0) {
						var diff = (kvp.Value.Item1 - kvp.Value.Item2) * 100d / kvp.Value.Item1;
						var p = new DataPoint() {
							XDateTime = kvp.Key,
							YValueStart = Math.Min(kvp.Value.Item1, kvp.Value.Item2),
							YValue = Math.Max(kvp.Value.Item1, kvp.Value.Item2),
							Tooltip = diff.ToString(chart.NumericFormat) + " %",
							Color = Math.Abs(diff) > chart.DifferenceHighlightSerieThreshold ? (Color?)null : Color.Transparent
						};
						differenceSerie.Points.Add(p);
					}
				}
				series.Add(differenceSerie);
			}
		}
		#endregion
		#region SimpleLinearRegression
		/// <summary>
		/// Simple Linear Regression.
		/// </summary>
		/// <param name="X">The X.</param>
		/// <param name="Y">The Y.</param>
		/// <returns></returns>
		private static Dictionary<string, double> SimpleLinearRegression(List<double> X, List<double> Y) {
			//Variable declarations            
			int num = 0; //use for List count
			double sumX = 0; //summation of x[i]
			double sumY = 0; //summation of y[i]
			double sum2X = 0; // summation of x[i]*x[i]
			double sum2Y = 0; // summation  of y[i]*y[i]
			double sumXY = 0; // summation of x[i] * y[i]  
			double denX = 0;
			double denY = 0;
			double top = 0;
			double corelation = 0; // holds Corelation
			double slope = 0; // holds slope(beta)
			double y_intercept = 0; //holds y-intercept (alpha)

			//Standard error variables
			double sum_res = 0.0;
			double yhat = 0;
			double res = 0;
			double standardError = 0; //
			int n = 0;
			//End standard variable declaration
			Dictionary<string, double> result
				= new Dictionary<string, double>(); //Stores the final result
			//End variable declaration

			#region Computation begins

			num = X.Count;  //Since the X and Y list are of same length, so 
			// we can take the count of any one list 
			sumX = X.Sum();  //Get Sum of X list
			sumY = Y.Sum(); //Get Sum of Y list           
			X.ForEach(i => { sum2X += i * i; }); //Get sum of x[i]*x[i]           
			Y.ForEach(i => { sum2Y += i * i; }); //Get sum of y[i]*y[i]            
			sumXY = Enumerable.Range(0, num).Select(i => X[i] * Y[i]).Sum();//Get Summation of x[i] * y[i]

			//Find denx, deny,top
			denX = num * sum2X - sumX * sumX;
			denY = num * sum2Y - sumY * sumY;
			top = num * sumXY - sumX * sumY;

			//Find corelation, slope and y-intercept
			corelation = top / Math.Sqrt(denX * denY);
			slope = top / denX;
			y_intercept = (sumY - sumX * slope) / num;


			//Implementation of Standard Error
			sum_res = Enumerable.Range(0, num).Aggregate(0.0, (sum, i) => {
				yhat = y_intercept + (slope * X[i]);
				res = yhat - Y[i];
				n++;
				return sum + res * res;
			});

			if (n > 2) {
				standardError = sum_res / (1.0 * n - 2.0);
				standardError = Math.Pow(standardError, 0.5);
			}
			else
				standardError = 0;

			#endregion

			//Add the computed value to the resultant dictionary
			result.Add("Beta", slope);
			result.Add("Alpha", y_intercept);
			result.Add("Corelation", corelation);
			result.Add("StandardError", standardError);
			return result;
		}
		#endregion
		#region LoadDurationCurve
		/// <summary>
		/// Create the load duration curves.
		/// </summary>
		/// <param name="chart">The chart.</param>
		/// <param name="series">The series.</param>
		public static void LoadDurationCurve(Chart chart, List<DataSerie> series) {
			var curves = new List<DataSerie>();
			foreach (var s in series) {
				if (s.Hidden != true) {
					int count = 0;
					var points = s.PointsWithValue.OrderByDescending(p => p.YValue.Value).ToList();
					var curve = s.Copy(s.LocalId, false);
                    curve.KeyDataSerie = s.KeyDataSerie;
					int subtrahend = points.Count > 1 ? 1 : 0;
					//curve.DisplayLegendEntry = s.DisplayLegendEntry;
					foreach (var p in points) {
						var np = new DataPoint() {
							XValue = (((double)count++) / (double)(points.Count - subtrahend)) * 100.0,
							YValue = p.YValue,
							Name = count.ToString()
						};
						curve.Points.Add(np);
					}
					curves.Add(curve);
				}
			}
			series.Clear();
			series.AddRange(curves);
		}
		#endregion
	}
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using System.Web.Security;
using Vizelia.FOL.DataLayer;

namespace Vizelia.FOL.Common.Localization {
	/// <summary>
	/// Data layer access to langue messages stored in database.
	/// </summary>
	class DatabaseMessageBrokerDB {
		readonly DataAccess m_DB;

		/// <summary>
		/// Public ctor.
		/// </summary>
		public DatabaseMessageBrokerDB() {
			m_DB = new DataAccess();
		}

		/// <summary>
		/// Gets the messages for a specific langue.
		/// </summary>
		/// <param name="cultureName">The langue.</param>
		/// <returns>A dictionary of the messages.</returns>
		public Dictionary<KeyValuePair<string, string>, string> GetMessage(string cultureName) {
			DataSet ds = m_DB.ExecuteDataSet("Localization_MessageGetAll", Membership.ApplicationName, cultureName);
			var query = from r in ds.Tables[0].AsEnumerable()
						select new { Id = r.Field<int>("Id").ToString(CultureInfo.InvariantCulture), Key = r.Field<string>("Key"), Value = r.Field<string>("Value") };
			return query.ToDictionary(p => new KeyValuePair<string, string>(p.Key, p.Id), p => p.Value);
		}

		/// <summary>
		/// Gets the messages for a specific langue.
		/// </summary>
		/// <param name="cultureName">The langue.</param>
		/// <returns>A dictionary of the messages.</returns>
		public Dictionary<string, string> GetMessage2(string cultureName) {
			DataSet ds = m_DB.ExecuteDataSet("Localization_MessageGetAll", Membership.ApplicationName, cultureName);
			var query = from r in ds.Tables[0].AsEnumerable()
						select new { Id = r.Field<int>("Id").ToString(CultureInfo.InvariantCulture), Key = r.Field<string>("Key"), Value = r.Field<string>("Value") };
			return query.ToDictionary(p => p.Key, p => p.Value);
		}



		/// <summary>
		/// Saves a message in database.
		/// Returns the id of the message in database.
		/// </summary>
		/// <param name="Key">The key of the message.</param>
		/// <param name="Value">The value of the message.</param>
		/// <param name="cultureName">The culture name.</param>
		public int SaveMessage(string Key, string Value, string cultureName) {
			DbCommand command = m_DB.GetStoredProcCommand("Localization_MessageCreateItem");
			m_DB.AddInParameterString(command, "Application", Membership.ApplicationName);
			m_DB.AddInParameterString(command, "Key", Key);
			m_DB.AddInParameterString(command, "Value", Value);
			m_DB.AddInParameterString(command, "Culture", cultureName);
			m_DB.AddOutParameterInt(command, "Return");
			m_DB.AddReturnParameter(command);
			m_DB.ExecuteNonQuery(command);

			int num = m_DB.GetReturnParameter(command);
			if (num != 0)
				throw new ArgumentException(Langue.error_msg_uniquefield, "Key");
			int result = Convert.ToInt32(m_DB.GetParameterValue(command, "Return"));
			return result;
		}

		/// <summary>
		/// Deletes a message in database
		/// </summary>
		/// <param name="Id">The Key of the message.</param>
		public void DeleteMessage(string Id) {
			m_DB.ExecuteNonQuery("Localization_MessageDeleteItem", Membership.ApplicationName, Id);
		}

		/// <summary>
		/// Updates a message in database.
		/// </summary>
		/// <param name="Key">The Key of the message.</param>
		/// <param name="Value">The value of the message.</param>
		/// <param name="cultureName">The culture name.</param>
		public void UpdateMessage(string Key, string Value, string cultureName) {
			m_DB.ExecuteNonQuery("Localization_MessageUpdateItem", Membership.ApplicationName, Key, Value, cultureName);
		}

		/// <summary>
		/// Gets the supported cultures.
		/// </summary>
		/// <returns>A list of the supported cultures.</returns>
		public List<string> GetSupportedCultures() {
			DataSet ds = m_DB.ExecuteDataSet("Localization_CultureGetAll");
			var query = from r in ds.Tables[0].AsEnumerable()
						select r.Field<string>("CultureCode");
			return query.ToList();
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Resources;
using System.Web.Security;

namespace Vizelia.FOL.Common.Localization {

	/// <summary>
	/// A custom ResourceManager implementation that works with a Database.
	/// </summary>
	class DatabaseResourceManager : ResourceManager {

		private Hashtable m_ResourceSets;
		
		/// <summary>
		/// Public ctor.
		/// </summary>
		public DatabaseResourceManager() {
			m_ResourceSets = new Hashtable();
		}

		/// <summary>
		/// Clears all the resources.
		/// </summary>
		public void ClearResource() {
			m_ResourceSets.Clear();
		}

		/// <summary>
		/// Provides the implementation for finding a System.Resources.ResourceSet.
		/// </summary>
		/// <param name="culture"> The System.Globalization.CultureInfo to look for.</param>
		/// <param name="createIfNotExists">If true and if the System.Resources.ResourceSet has not been loaded yet, load it.</param>
		/// <param name="tryParents">If the System.Resources.ResourceSet cannot be loaded, try parent System.Globalization.CultureInfo objects to see if they exist.</param>
		/// <returns></returns>
		protected override ResourceSet InternalGetResourceSet(CultureInfo culture, bool createIfNotExists, bool tryParents) {
			DatabaseResourceSet resourceSet;
			string languageName = culture.TwoLetterISOLanguageName;
			var languageCodeAndTenant = new KeyValuePair<string, string>(languageName, Membership.ApplicationName);
			if (m_ResourceSets.Contains(languageCodeAndTenant)) {
				resourceSet = m_ResourceSets[languageCodeAndTenant] as DatabaseResourceSet;
			}
			else {
				// lock to prevent synchronisation exception.
				// An additional check would be to verify the existence in ResourceSets again just before adding.
				lock (this) {
					resourceSet = new DatabaseResourceSet(culture);
					if (!m_ResourceSets.Contains(languageCodeAndTenant))
						m_ResourceSets.Add(languageCodeAndTenant, resourceSet);
				}
			}
			return resourceSet;
		}

		/// <summary>
		/// Adds a new resource. 
		/// Returns the id of the new insterted resource.
		/// </summary>
		/// <param name="Key">The Key.</param>
		/// <param name="Value">The Value.</param>
		/// <param name="cultureName">The culture name.</param>
		public int AddResource(string Key, string Value, string cultureName) {
			var messages = new DatabaseMessageBrokerDB();
			var retVal = messages.SaveMessage(Key, Value, cultureName);
			return retVal;
		}

		/// <summary>
		/// Adds a new resource.
		/// </summary>
		/// <param name="Key">The Key.</param>
		/// <param name="Value">The Value.</param>
		public int AddResource(string Key, string Value) {
			var retVal = AddResource(Key, Value, CultureInfo.CurrentUICulture.TwoLetterISOLanguageName);
			return retVal;
		}

		
	}

}


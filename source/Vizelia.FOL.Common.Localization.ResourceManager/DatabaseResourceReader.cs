﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Resources;
using System.Collections;
using System.Data.SqlClient;
using System.Globalization;

namespace Vizelia.FOL.Common.Localization {

	/// <summary>
	/// A custom implementation of a IResourceReader.
	/// </summary>
	class DatabaseResourceReader : IResourceReader {
		private string language;

		public DatabaseResourceReader(CultureInfo culture) {
			this.language = culture.TwoLetterISOLanguageName;
			if (language == "")
				language = "neutral";
		}


		public void Close() {
			throw new NotImplementedException();
		}

		public IDictionaryEnumerator GetEnumerator() {
			Hashtable dict = new Hashtable();
			DatabaseMessageBrokerDB messages = new DatabaseMessageBrokerDB();
			return messages.GetMessage2(this.language).GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator() {
			return this.GetEnumerator();
		}

		public void Dispose() {
			throw new NotImplementedException();
		}
	}
}

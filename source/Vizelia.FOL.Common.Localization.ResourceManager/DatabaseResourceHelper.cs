﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace Vizelia.FOL.Common.Localization {


	/// <summary>
	/// Database resource Helper.
	/// </summary>
	public static class DatabaseResourceHelper {
		static DatabaseResourceManager m_DatabaseResourceManager;

		/// <summary>
		/// Static ctor. 
		/// Note that a static ctor should never makes use of private, public nor protected.
		/// </summary>
		static DatabaseResourceHelper() {
			m_DatabaseResourceManager = new DatabaseResourceManager();
		}

		/// <summary>
		/// Returns the value of the specified System.String resource.
		/// </summary>
		/// <param name="name">The name of the resource to get.</param>
		/// <returns> The value of the resource localized for the caller's current culture settings.</returns>
		public static string GetString(string name) {
			var result = m_DatabaseResourceManager.GetString(name);
			return result;
		}

		/// <summary>
		/// Returns the value of the specified System.String resource.
		/// </summary>
		/// <param name="name">The name of the resource to get.</param>
		/// <param name="culture">The culture.</param>
		/// <returns></returns>
		public static string GetString(string name, string culture) {
			var result = m_DatabaseResourceManager.GetString(name, new CultureInfo(culture));
			return result;
		}

		/// <summary>
		/// Gets the messages for a specific langue.
		/// </summary>
		/// <param name="cultureName">The langue.</param>
		/// <returns>A dictionary of the messages.</returns>
		public static Dictionary<KeyValuePair<string, string>, string> GetMessages(string cultureName) {
			var messages = new DatabaseMessageBrokerDB();
			return messages.GetMessage(cultureName.ToLower());
		}

		/// <summary>
		/// Clears the resource cache.
		/// </summary>
		public static void ClearResource() {
			m_DatabaseResourceManager.ClearResource();
		}

		/// <summary>
		/// Adds a resource to database.
		/// </summary>
		/// <param name="Key">The Key</param>
		/// <param name="Value">The Value</param>
		/// <param name="cultureName">The culture name.</param>
		public static int AddResource(string Key, string Value, string cultureName) {
			var retVal = m_DatabaseResourceManager.AddResource(Key, Value, cultureName);
			m_DatabaseResourceManager.ClearResource();
			return retVal;
		}


		/// <summary>
		/// Adds a resource to database.
		/// </summary>
		/// <param name="Key">The key.</param>
		/// <param name="Value">The value.</param>
		public static void AddResource(string Key, string Value) {
			m_DatabaseResourceManager.AddResource(Key, Value);
			m_DatabaseResourceManager.ClearResource();
		}

		/// <summary>
		/// Deletes a resource from database.
		/// </summary>
		/// <param name="Id">The key of the message.</param>
		public static void DeleteResource(string Id) {
			var messages = new DatabaseMessageBrokerDB();
			messages.DeleteMessage(Id);
			m_DatabaseResourceManager.ClearResource();
		}

		/// <summary>
		/// Updates a resource in database.
		/// </summary>
		/// <param name="Key">The key.</param>
		/// <param name="Value">The value.</param>
		/// <param name="cultureName">The culture name.</param>
		public static void UpdateResource(string Key, string Value, string cultureName) {
			var messages = new DatabaseMessageBrokerDB();
			messages.UpdateMessage(Key, Value, cultureName.ToLower());
			m_DatabaseResourceManager.ClearResource();
		}

		/// <summary>
		/// Gets the supported cultures.
		/// </summary>
		/// <returns></returns>
		public static List<string> GetSupportedCultures() {
			var messages = new DatabaseMessageBrokerDB();
			return messages.GetSupportedCultures();

		}
	}

}

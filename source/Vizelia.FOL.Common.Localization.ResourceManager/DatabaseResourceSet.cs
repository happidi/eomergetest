﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Resources;
using System.Globalization;

namespace Vizelia.FOL.Common.Localization {
	/// <summary>
	/// A custom implementation of ResourceSet.
	/// </summary>
	class DatabaseResourceSet : ResourceSet {
		internal DatabaseResourceSet(CultureInfo culture) : base(new DatabaseResourceReader(culture)) {
		}
		
		/// <summary>
		/// Gets the defautl reader.
		/// </summary>
		/// <returns></returns>
		public override Type GetDefaultReader() {
			return typeof(DatabaseResourceReader);
		}

	}
}

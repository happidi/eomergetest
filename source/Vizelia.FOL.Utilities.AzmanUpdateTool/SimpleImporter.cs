﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml;
using NetSqlAzMan;
using NetSqlAzMan.Cache;
using NetSqlAzMan.Interfaces;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Security;

namespace Vizelia.FOL.Utilities.AzmanUpdateTool {
	/// <summary>
	/// Imports the specified XML node.
	/// </summary>
	public class SimpleImporter {

		private string m_ConnectionString;
		private string m_schedulerUsername;

		/// <summary>
		/// Initializes a new instance of the <see cref="SimpleImporter"/> class.
		/// </summary>
		/// <param name="schedulerUsername">The scheduler username.</param>
		/// <param name="dbPrefix">The db prefix.</param>
		/// <param name="dbServer">The db server.</param>
		/// <param name="dbUser">The db user.</param>
		/// <param name="dbPwd">The db PWD.</param>
		public SimpleImporter(string schedulerUsername, string dbPrefix, string dbServer, string dbUser, string dbPwd) {
			m_schedulerUsername = schedulerUsername;
			m_ConnectionString = String.Format("Data Source={0};Initial Catalog={1}_azman;User ID={2};Password={3}", dbServer, dbPrefix, dbUser, dbPwd);
		}

		/// <summary>
		/// Imports the specified XML node.
		/// </summary>
		/// <param name="xmlNode">The XML node.</param>
		/// <param name="tenantName">Name of the tenant.</param>
		public void Import(XmlNode xmlNode, string tenantName) {
			var nodeApplication = xmlNode.SelectSingleNode("//Store/Applications/Application") as XmlElement;
			var azManStorages = new SqlAzManStorage(m_ConnectionString);


			// make sure store always exist.
			IAzManStore store = azManStorages.Stores["Vizelia"];

			if (nodeApplication != null)
				nodeApplication.SetAttribute("Name", tenantName);

			const SqlAzManMergeOptions azManMergeOptions = SqlAzManMergeOptions.CreatesNewItems | SqlAzManMergeOptions.OverwritesExistingItems;

			azManStorages.ImportChildren(xmlNode, false, false, false, azManMergeOptions);

			AssociateRolesToApplicationGroups(store.GetApplication(tenantName));
			AssociateSystemUsersToApplicationGroups(store.GetApplication(tenantName));
		}

		/// <summary>
		/// Diffs the import.
		/// </summary>
		/// <param name="xmlNode">The XML node.</param>
		/// <param name="tenantName">Name of the tenant.</param>
		public void DiffImport(XmlNode xmlNode, string tenantName) {

			var storageCache = new StorageCache(m_ConnectionString);
			storageCache.BuildStorageCache("Vizelia", tenantName);
		
			var stream = new MemoryStream();
			var streamWriter = new StreamWriter(stream);
		
			streamWriter.Write(
				"<?xml version=\"1.0\" encoding=\"utf-8\"?><NetSqlAzMan><Store Name=\"Vizelia\" Description=\"\"><Attributes /><Permissions><Managers /><Users /><Readers /></Permissions><StoreGroups /><Applications><Application Name=\"FacilityOnLine\" Description=\"\"><Attributes /><Permissions><Managers /><Users /><Readers /></Permissions>");


			var applicationGroupsNode = xmlNode.SelectSingleNode("//Store/Applications/Application/ApplicationGroups") as XmlElement;
			streamWriter.Write(applicationGroupsNode.OuterXml);

			streamWriter.Write("<Items>");
			var itemsNode = xmlNode.SelectSingleNode("//Store/Applications/Application/Items") as XmlElement;

			Dictionary<string, IAzManItem> azManItems = storageCache.Storage.GetStores().First().GetApplication(tenantName).Items;
			foreach (XmlNode item in itemsNode.ChildNodes) {
				if (item.Name == "Item") {

					//Add item type check
					if (!(azManItems.ContainsKey(item.Attributes["Name"].Value)) || azManItems[item.Attributes["Name"].Value].Description != item.Attributes["Description"].Value) {
						streamWriter.Write(item.OuterXml);
					}
					else {
						var serverItem = azManItems[item.Attributes["Name"].Value];
						var memberNames = serverItem.Members.Select(m => m.Key);
						foreach (XmlNode memberNode in item.ChildNodes) {
							if (memberNode.Name == "Members") {
								if (memberNames.Count() != memberNode.ChildNodes.Count) {
									streamWriter.Write(item.OuterXml);
								}
								else {
									foreach (XmlNode member in memberNode.ChildNodes) {
										if (!memberNames.Contains(member.Attributes["Name"].Value)) {
											streamWriter.Write(item.OuterXml);
											break;
										}
									}
								}
							}
							if (memberNode.Name == "Attributes") {
								if ((serverItem.Attributes.Count != memberNode.ChildNodes.Count) || memberNode.ChildNodes.Count > 0) {
									streamWriter.Write(item.OuterXml);
								}
							}
						}
					}
				}
			}


			streamWriter.Write("</Items></Application></Applications></Store></NetSqlAzMan>");
			streamWriter.Flush();

			var xmlDoc = new XmlDocument();
			stream.Seek(0, SeekOrigin.Begin);
			xmlDoc.Load(stream);
			Import(xmlDoc.DocumentElement, tenantName);
			streamWriter.Close();
		}

		/// <summary>
		/// Associates the system users to application groups.
		/// </summary>
		/// <param name="application">The application.</param>
		private void AssociateSystemUsersToApplicationGroups(IAzManApplication application) {
			AssociateUserToGroup("Admin", "Admins", application);
			AssociateUserToGroup(m_schedulerUsername, "Admins", application);
		}

		/// <summary>
		/// Associates the user to an application group.
		/// </summary>
		/// <param name="userName">Name of the user.</param>
		/// <param name="applicationGroupName">Name of the application group.</param>
		/// <param name="application">The application.</param>
		private void AssociateUserToGroup(string userName, string applicationGroupName, IAzManApplication application) {

			var adminsGroup = ApplicationGroup_GetAll(application).FirstOrDefault(appGroup => appGroup.Name == applicationGroupName);

			if (adminsGroup != null) {
				if (!adminsGroup.Members.Values.Select(member => member.SID).Contains(application.GetDBUser(userName).CustomSid)) {
					adminsGroup.CreateApplicationGroupMember(application.GetDBUser(userName).CustomSid, WhereDefined.Database, true);
				}
			}

		}

		/// <summary>
		/// Associates the roles to application groups.
		/// </summary>
		/// <param name="application">The application.</param>
		private void AssociateRolesToApplicationGroups(IAzManApplication application) {
			var allApplicationGroups = ApplicationGroup_GetAll(application).Select(appGroup => new ApplicationGroup(appGroup));
			List<AuthorizationItem> allRoles = AzManRole_GetAll(application);

			foreach (var applicationGroup in allApplicationGroups) {
				var role = allRoles.FirstOrDefault(rl => rl.Name == applicationGroup.Name);
				if (role != null) {
					CrudAuthorizations(applicationGroup, new CrudStore<AuthorizationItem> {
						create = new List<AuthorizationItem> { role }
					}, application);
				}

			}
		}
		/// <summary>
		/// Gets all the application groups.
		/// </summary>
		/// <returns></returns>
		public List<IAzManApplicationGroup> ApplicationGroup_GetAll(IAzManApplication application) {
			return application.GetApplicationGroups().OrderBy(p => p.Name).ToList();
		}

		/// <summary>
		/// Gets all the azman roles
		/// </summary>
		/// <returns>A list of all the azman items that are roles.</returns>
		private List<AuthorizationItem> AzManRole_GetAll(IAzManApplication application) {
			return application.GetItems(ItemType.Role).Select(role => new AuthorizationItem(role)).ToList();
		}

		/// <summary>
		/// Modify the list of authorizations for an application group.
		/// </summary>
		/// <param name="applicationGroup">The application group.</param>
		/// <param name="roles">The roles.</param>
		/// <param name="application">The application.</param>
		private void CrudAuthorizations(ApplicationGroup applicationGroup, CrudStore<AuthorizationItem> roles, IAzManApplication application) {
			if (roles == null)
				return;
			IAzManSid ownerSid;
			WhereDefined ownerLocation;
			GetOwnerInfo(out ownerSid, out ownerLocation, application);

			if (roles.create != null) {
				foreach (AuthorizationItem role in roles.create) {
					IAzManItem item = application.GetItemById(role.Id);
					if (item != null && (!item.Authorizations.Select(auth => auth.SID.StringValue).Contains(applicationGroup.Id)))
						item.CreateAuthorization(ownerSid, ownerLocation, new SqlAzManSID(applicationGroup.Id), WhereDefined.Application, AuthorizationType.Allow, null, null);
				}
			}

		}

		/// <summary>
		/// Gets the owner info.
		/// </summary>
		/// <param name="ownerSid">The owner sid.</param>
		/// <param name="ownerLocation">The owner location.</param>
		/// <param name="application">The application.</param>
		private void GetOwnerInfo(out IAzManSid ownerSid, out WhereDefined ownerLocation, IAzManApplication application) {
			ownerSid = application.GetDBUser("Admin").CustomSid;
			ownerLocation = WhereDefined.Database;
		}
	}
}

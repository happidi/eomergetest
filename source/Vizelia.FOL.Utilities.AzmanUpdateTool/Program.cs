﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml;
using Vizelia.FOL.DataLayer;
using Vizelia.FOL.DataLayer.Interfaces;
using Vizelia.FOL.Utilities.CommandLine;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.Utilities.AzmanUpdateTool {
	/// <summary>
	/// Update the azman config provided a new azman.xml
	/// </summary>
	class Program {

		private static string AzmanFile { get; set; }
		private static string TenantName { get; set; }
		private static string SchedulerUsername { get; set; }
		private static string DBPrefix { get; set; }
		private static string DBPwd { get; set; }
		private static string DBServer { get; set; }
		private static string DBUser { get; set; }
		private static string UseFullAzmanFile { get; set; }

		static Assembly CurrentDomain_AssemblyResolve(object sender, ResolveEventArgs args) {
			string dllName = args.Name.Contains(',') ? args.Name.Substring(0, args.Name.IndexOf(',')) : args.Name.Replace(".dll", "");
			dllName = dllName.Replace(".", "_");
			if (dllName.EndsWith("_resources")) return null;
			System.Resources.ResourceManager rm = new System.Resources.ResourceManager(typeof(Program).Namespace + ".Resources", Assembly.GetExecutingAssembly());
			byte[] bytes = (byte[])rm.GetObject(dllName);
			return Assembly.Load(bytes);

		}

		static void Main(string[] args) {
			AppDomain.CurrentDomain.AssemblyResolve += CurrentDomain_AssemblyResolve;
			Console.ForegroundColor = ConsoleColor.Gray;
			if ((args.Length == 0) || (args[0].Contains("?"))) {
				ShowUsage();
				return;
			}

			Console.ForegroundColor = ConsoleColor.Red;

			Arguments CommandLine = new Arguments(args);
			bool flgArgumentValid = true;

			if (CommandLine["AzmanFile"] == null) {
				Console.WriteLine("Argument AzmanFile not supplied");
				flgArgumentValid = false;
			}


			if (!flgArgumentValid) {
				UsageHelper.Abort("Operation aborted.");
			}


			Console.ForegroundColor = ConsoleColor.Gray;
			Console.WriteLine("Upgrade is starting...");
			Console.WriteLine("");
			Console.ForegroundColor = ConsoleColor.DarkGray;
			ConfigureProperties(
				CommandLine["AzmanFile"],
				CommandLine["TenantName"],
				CommandLine["SchedulerUsername"],
				CommandLine["DBPrefix"],
				CommandLine["DBServer"],
				CommandLine["DBUser"],
				CommandLine["DBPwd"],
				CommandLine["UseFullAzmanFile"]);

			Console.WriteLine("");
			ImportAzman();
			Console.ForegroundColor = ConsoleColor.Green;
			Console.WriteLine("");
			Console.WriteLine("Upgrade completed.");

			Console.ForegroundColor = ConsoleColor.Gray;
		}

		/// <summary>
		/// Imports the azman.
		/// </summary>
		private static void ImportAzman() {
			var xmlDoc = new XmlDocument();
			xmlDoc.Load(AzmanFile);
			List<string> tenants = TenantName == "*" ? GetTenantList() : TenantName.Split(';').ToList();

			SimpleImporter importer = new SimpleImporter(SchedulerUsername, DBPrefix, DBServer, DBUser, DBPwd);
			foreach (var tenant in tenants) {
				try {
					Console.WriteLine("Upgrading " + tenant + "...");
					if (UseFullAzmanFile.ToLower() != "true")
						importer.DiffImport(xmlDoc.DocumentElement, tenant);
					else 
						importer.Import(xmlDoc.DocumentElement, tenant);
					
				}
				catch (Exception e) {
					Console.WriteLine("Updating tenant failed, tenant name:" + tenant);
					Console.WriteLine("Error:" + e.Message);
				}

			}
		}

		/// <summary>
		/// Gets the tenant list.
		/// </summary>
		/// <returns></returns>
		private static List<string> GetTenantList() {
			var retVal = new List<string>();
			IDataAccess db = new DataAccess(DBServer, DBPrefix + "_code", DBUser, DBPwd);
			DbCommand command = db.GetStoredProcCommand("Tenancy_TenantGetAll");
			db.AddInParameterString(command, "Application", "");
			using (var reader = db.ExecuteReader(command)) {
				while (reader.Read()) {
					retVal.Add(reader.GetValue<string>("KeyTenant"));
				}
			}
			return retVal;
		}

		/// <summary>
		/// Configures the properties.
		/// </summary>
		/// <param name="azmanFile">The azman file.</param>
		/// <param name="tenantName">Name of the tenant.</param>
		/// <param name="schedulerUsername">The scheduler username.</param>
		/// <param name="dbPrefix">The db prefix.</param>
		/// <param name="dbServer">The db server.</param>
		/// <param name="dbUser">The db user.</param>
		/// <param name="dbPwd">The db PWD.</param>
		private static void ConfigureProperties(
			string azmanFile,
			string tenantName,
			string schedulerUsername,
			string dbPrefix,
			string dbServer,
			string dbUser,
			string dbPwd,
			string useFullAzmanFile) {
			AzmanFile = azmanFile;
			TenantName = tenantName ?? "*";
			SchedulerUsername = schedulerUsername ?? "Scheduler";
			DBPrefix = dbPrefix ?? "vizelia";
			DBServer = dbServer ?? "(local)";
			DBUser = dbUser ?? "sa";
			DBPwd = dbPwd ?? "vizmaster01";
			UseFullAzmanFile = (useFullAzmanFile ?? "false");
			CheckProperties();

			Console.WriteLine("...configure properties");
		}

		

		/// <summary>
		/// Checks the properties.
		/// </summary>
		private static void CheckProperties() {
			Console.ForegroundColor = ConsoleColor.Red;
			CheckFile(AzmanFile);
			Console.ForegroundColor = ConsoleColor.Gray;
		}

		/// <summary>
		/// Checks the file. Exit if not found.
		/// </summary>
		/// <param name="path">The path.</param>
		private static void CheckFile(string path) {
			if (!File.Exists(path)) {
				Console.WriteLine("Could not find file : {0}", path);
				UsageHelper.Abort("Operation aborted.");
			}
		}

		/// <summary>
		/// Shows the usage.
		/// </summary>
		private static void ShowUsage() {
			Console.Clear();
			Console.WriteLine("Command-line tool for upgrading Azman in Energy Operation {0}.", Assembly.GetExecutingAssembly().GetName().Version);
			Console.WriteLine("Copyright (c) Vizelia - Schneider Electric. All rights reserved.");
			Console.WriteLine("");

			var usage = UsageHelper.GetUsageString(new List<ArgumentHelpStrings> {
															new ArgumentHelpStrings {
			                                             	                        	Syntax ="/AzmanFile:<string>",
																						Help = "The azman file to import.",
																						Type = ArgumentType.Mandatory
			                                             	                        },
			                                             	new ArgumentHelpStrings {
			                                             	                        	Syntax ="/TenantName:<string>",
																						Help = "The tenant name. Default to * to indicate all tenants, otherwise semicolon separated list.",
																						Type = ArgumentType.Optional
			                                             	                        },
															new ArgumentHelpStrings {
																					    Syntax = "/SchedulerUsername:<Username>",
																					    Help = "Scheduler username. Default to Scheduler",
																						Type = ArgumentType.Optional
															                        },
															new ArgumentHelpStrings {
			                                             	                        	Syntax ="/DBPrefix:<string>",
																						Help = "The prefix used for databases. Default to vizelia.",
																						Type = ArgumentType.Optional
			                                             	                        }
																					,
															new ArgumentHelpStrings {
			                                             	                        	Syntax ="/DBServer:<string>",
																						Help = "The database server. Default to (local).",
																						Type = ArgumentType.Optional
			                                             	                        },
															new ArgumentHelpStrings {
			                                             	                        	Syntax ="/DBUser:<string>",
																						Help = "A valid user for the database. Default to sa.",
																						Type = ArgumentType.Optional
			                                             	                        }
																					,
															new ArgumentHelpStrings {
			                                             	                        	Syntax ="/DBPwd:<string>",
																						Help = "A valid password for the database user.",
																						Type = ArgumentType.Optional
			                                             	                        }
															
			                                             });

			Console.Write(usage);

		}
	}
}

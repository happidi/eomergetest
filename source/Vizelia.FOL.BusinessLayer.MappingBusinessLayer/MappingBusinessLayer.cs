﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web.Security;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.BusinessLayer.Broker;
using Vizelia.FOL.BusinessLayer.Interfaces;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Configuration;
using Vizelia.FOL.Common.Localization;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.BusinessLayer {
	/// <summary>
	/// Business Layer implementation for Mapping.
	/// </summary>
	public class MappingBusinessLayer : IMappingBusinessLayer {
		private const int const_max_nested_archives_depth = 2;
		private const string const_new_filename_on_server_format = "{0}__importing_{1}.{2}";
		private const string const_new_filename_on_server_regex = @"^(?<filename>.+)__importing_(?<guid>[^.]+)\.(?<extension>.*)$";
		private const string const_cannot_delete_working_directory_message = "Cannot delete working directory for scheduled mapping, if the problem persists, you may run out of space.";
		private const string const_working_directory_subdirectory_name = "SEMapping";
		private const string const_cannot_rename_file_for_access_message = "Cannot rename file for access, from '{0}' to '{1}'. This may be caused if the file is still being uploaded. If the error repeats several times for this file, this may indicate a problem.";
		private const string const_external_crud_notification_service_not_ready = "ExternalCrudNotificationService is not ready for notifications. Aborting this mapping task in order to allow it to ready itself. If this persists, contact your system administrator.";
		private const string const_mail_service_desk = "MAIL_ServiceDesk";
		private const string const_mail_init_tenant = "MAIL_InitTenant";
		private const string const_mail_reset_password = "MAIL_ResetPassword";
		private const string const_mail_chart_scheduler = "MAIL_ChartScheduler";
		private const string const_mail_mapping_report = "MAIL_MappingReport";
		private const string const_mail_long_running_operation_result = "MAIL_LongRunningOperationResult";
		private const string const_mail_meter_validation_rule = "MAIL_MeterValidationRule";
		private const string const_mail_forgot_password = "MAIL_ForgotPassword";
		private const string const_mail_alarm = "MAIL_Alarm";
		private const string const_date_time_format = "yyyy'-'MM'-'dd'-'HH'-'mm'-'ss";

		/// <summary>
		/// Initializes the current tenant with data needed to begin working with it.
		/// </summary>
		/// <param name="operationId">The operation id.</param>
		/// <param name="adminPassword">The admin password.</param>
		public void InitializeTenant(Guid operationId, string adminPassword) {
			LongRunningOperationService.Invoke(operationId, () => DoInitDatabaseLongRunning(adminPassword));
		}

		/// <summary>
		/// Does the init database long running.
		/// </summary>
		/// <returns></returns>
		private static LongRunningOperationResult DoInitDatabaseLongRunning(string adminPassword) {
			using (TracingService.StartTracing("InitializeTenant", string.Empty)) {

				TracingService.Write("Perparing entities for initial mapping...");

				// The order of adding to this list matters.
				var entitiesToCreate = new List<IMappingRecordConverter>();

				//SvcFinder.BuildAzManOperations();

				entitiesToCreate.AddRange(BuildClassificationDefinitions());

				var organization = new DefaultOrganizationMapping {
					Id = "Vizelia",
					Name = "Vizelia",
					Description = "Vizelia"
				};
				entitiesToCreate.Add(organization);

				var site = new DefaultSiteMapping {
					Description = "Root site",
					LocalId = "site_root",
					Name = "Root site"
				};
				entitiesToCreate.Add(site);

				var rootClassificationItem = new DefaultClassificationItemMapping {
					LocalId = "Root",
					Title = "Classification root",
					ColorR = 255,
					ColorG = 255,
					ColorB = 255
				};
				entitiesToCreate.Add(rootClassificationItem);

				var workClassificationItem = new DefaultClassificationItemMapping {
					LocalId = "WORKTYPE",
					Title = "WORKTYPE",
					ParentLocalId = rootClassificationItem.LocalId,
					ColorR = 144,
					ColorG = 193,
					ColorB = 208
				};
				entitiesToCreate.Add(workClassificationItem);


				var meterClassificationItem = new DefaultClassificationItemMapping {
					LocalId = "METER",
					Title = "METER",
					ParentLocalId = rootClassificationItem.LocalId,
					ColorR = 144,
					ColorG = 193,
					ColorB = 208
				};
				entitiesToCreate.Add(meterClassificationItem);

				var equipmentClassificationItem = new DefaultClassificationItemMapping {
					LocalId = "EQUIPMENT",
					Title = "EQUIPMENT",
					ParentLocalId = rootClassificationItem.LocalId,
					ColorR = 144,
					ColorG = 193,
					ColorB = 208
				};
				entitiesToCreate.Add(equipmentClassificationItem);


				var alarmClassificationItem = new DefaultClassificationItemMapping {
					LocalId = "ALARM",
					Title = "ALARM",
					ParentLocalId = rootClassificationItem.LocalId,
					ColorR = 144,
					ColorG = 193,
					ColorB = 208
				};
				entitiesToCreate.Add(alarmClassificationItem);

				var eventlogClassificationItem = new DefaultClassificationItemMapping {
					LocalId = "EVENTLOG",
					Title = "EVENTLOG",
					ParentLocalId = rootClassificationItem.LocalId,
					ColorR = 144,
					ColorG = 193,
					ColorB = 208
				};
				entitiesToCreate.Add(eventlogClassificationItem);

				var kpiClassificationItem = new DefaultClassificationItemMapping {
					LocalId = "KPI",
					Title = "KPI",
					ParentLocalId = rootClassificationItem.LocalId,
					ColorR = 144,
					ColorG = 193,
					ColorB = 208
				};
				entitiesToCreate.Add(kpiClassificationItem);

				var vizeliaWorkClassificationItem = new DefaultClassificationItemMapping {
					LocalId = "vizelia_worktype_clsitem",
					Title = "Vizelia Work Type",
					ParentLocalId = workClassificationItem.LocalId
				};
				entitiesToCreate.Add(vizeliaWorkClassificationItem);

				var organizationClassificationItem = new DefaultClassificationItemMapping {
					LocalId = "ORGANIZATION",
					Title = "ORGANIZATION",
					ParentLocalId = rootClassificationItem.LocalId,
					ColorR = 134,
					ColorG = 63,
					ColorB = 109
				};
				entitiesToCreate.Add(organizationClassificationItem);

				var vizeliaOrganizationClassificationItem = new DefaultClassificationItemMapping {
					LocalId = "vizelia_organization_clsitem",
					Title = "Vizelia Organization",
					ParentLocalId = organizationClassificationItem.LocalId
				};
				entitiesToCreate.Add(vizeliaOrganizationClassificationItem);

				var mailClassificationItem = new DefaultClassificationItemMapping {
					LocalId = "MAIL",
					Title = "MAIL",
					ParentLocalId = rootClassificationItem.LocalId,
					ColorR = 144,
					ColorG = 193,
					ColorB = 208
				};
				entitiesToCreate.Add(mailClassificationItem);

				entitiesToCreate.Add(new DefaultClassificationItemMapping {
					LocalId = const_mail_init_tenant,
					Title = "Init Tenant",
					ParentLocalId = mailClassificationItem.LocalId
				});
				entitiesToCreate.Add(new DefaultClassificationItemMapping {
					LocalId = const_mail_service_desk,
					Title = "Service Desk",
					ParentLocalId = mailClassificationItem.LocalId
				});
				entitiesToCreate.Add(new DefaultClassificationItemMapping {
					LocalId = const_mail_reset_password,
					Title = "Reset Password",
					ParentLocalId = mailClassificationItem.LocalId
				});
				entitiesToCreate.Add(new DefaultClassificationItemMapping {
					LocalId = const_mail_chart_scheduler,
					Title = "Chart Scheduler",
					ParentLocalId = mailClassificationItem.LocalId
				});
				entitiesToCreate.Add(new DefaultClassificationItemMapping {
					LocalId = const_mail_mapping_report,
					Title = "Mapping Report",
					ParentLocalId = mailClassificationItem.LocalId
				});
				entitiesToCreate.Add(new DefaultClassificationItemMapping {
					LocalId = const_mail_long_running_operation_result,
					Title = "Long Running Operation Result",
					ParentLocalId = mailClassificationItem.LocalId
				});
				;
				entitiesToCreate.Add(new DefaultClassificationItemMapping {
					LocalId = const_mail_alarm,
					Title = "Alarm",
					ParentLocalId = mailClassificationItem.LocalId
				});
				entitiesToCreate.Add(new DefaultClassificationItemMapping {
					LocalId = const_mail_meter_validation_rule,
					Title = "Meter Validation Rule",
					ParentLocalId = mailClassificationItem.LocalId
				});

				entitiesToCreate.Add(new DefaultClassificationItemMapping {
					LocalId = const_mail_forgot_password,
					Title = "Forgot Password",
					ParentLocalId = mailClassificationItem.LocalId
				});

				const string mailBodyPostfix = "<br><br>@Model.ApplicationTitle @Model.ApplicationVersionNumber - @Model.Url <br>Tenant : @Model.ApplicationName  <br>Server  : @Model.MachineName  <br><br>This email is sent from an automated machine. Please do not reply to this email address. E-mails sent to this address are not read or replied.";

				const string initTenantMailBody = "Hello, <br>The new Tenant ''@Model.Tenant.Name'' is ready for use. <br>Username: Admin <br>Password: @Model.Password";
				entitiesToCreate.Add(new DefaultMailMapping {
					KeyClassificationItem = const_mail_init_tenant,
					Name = "Init Tenant",
					From = "@Model.ApplicationTitle (@Model.MachineName)",
					To = "@Model.Tenant.AdminEmail",
					Subject = "Your new tenant is ready for use",
					Body = initTenantMailBody + mailBodyPostfix,
					IsActive = true
				});

				entitiesToCreate.Add(new DefaultMailMapping {
					KeyClassificationItem = const_mail_reset_password,
					Name = "Reset Password",
					From = "@Model.ApplicationTitle (@Model.MachineName)",
					To = "@Model.Occupant.FullName <@Model.Email>",
					Subject = "Reset Password",
					Body = "Dear @Model.UserName, your password has been reseted to : @Model.NewPassword" + mailBodyPostfix,
					IsActive = true
				});

				entitiesToCreate.Add(new DefaultMailMapping {
					KeyClassificationItem = const_mail_chart_scheduler,
					Name = "Chart Scheduler",
					From = "@Model.ApplicationTitle (@Model.MachineName)",
					To = "@Model.ChartScheduler.Emails",
					Subject = "@(@Model.ChartScheduler.Subject ?? (@Langue.msg_chartscheduler_result_email_subject + \"-\" + @Model.ChartScheduler.Title))",
					Body = "@Model.ChartScheduler.Body" + mailBodyPostfix,
					IsActive = true
				});

				const string mappingReportMailBody = "Integration report of xml data <br>------------------------------------- <br>Please, find attached the report that was generated on: @Model.DateTime UTC";
				entitiesToCreate.Add(new DefaultMailMapping {
					KeyClassificationItem = const_mail_mapping_report,
					Name = "Mapping Report",
					From = "@Model.ApplicationTitle (@Model.MachineName)",
					To = "@Model.MappingSummary.Emails",
					Subject = "Mapping Report",
					Body = mappingReportMailBody + mailBodyPostfix,
					IsActive = true
				});

				entitiesToCreate.Add(new DefaultMailMapping {
					KeyClassificationItem = const_mail_long_running_operation_result,
					Name = "Long Running Operation Result",
					From = "@Model.ApplicationTitle (@Model.MachineName)",
					To = "@Model.UserEmailAddress",
					Subject = "@Langue.msg_long_running_operation_result_email_subject",
					Body = mailBodyPostfix,
					IsActive = true
				});

				entitiesToCreate.Add(new DefaultMailMapping {
					KeyClassificationItem = const_mail_alarm,
					Name = "Alarm",
					From = "@Model.ApplicationTitle (@Model.MachineName)",
					To = "@Model.Alarm.Emails",
					Subject = "@Langue.msg_alarm : @Model.Alarm.Title (@Model.InstanceCount @Langue.msg_alarminstances)",
					Body = "@Model.Description" + mailBodyPostfix,
					IsActive = true
				});

				entitiesToCreate.Add(new DefaultMailMapping {
					KeyClassificationItem = const_mail_meter_validation_rule,
					Name = "Meter Validation Rule",
					From = "@Model.ApplicationTitle (@Model.MachineName)",
					To = "@Model.MeterValidationRule.Emails",
					Subject = "@Langue.msg_metervalidationrule : @Model.MeterValidationRule.Title (@Model.InstanceCount @Langue.msg_alarminstances)",
					Body = "@Model.Description" + mailBodyPostfix,
					IsActive = true
				});

				entitiesToCreate.Add(new DefaultMailMapping {
					KeyClassificationItem = const_mail_forgot_password,
					Name = "Forgot Password",
					From = "@Model.ApplicationTitle (@Model.MachineName)",
					To = "@Model.Occupant.FullName <@Model.Email>",
					Subject = "Forgot Password",
					Body = "Dear @Model.UserName, your password has been reseted to : @Model.NewPassword <br>Please use it within the next 24 hours." + mailBodyPostfix,
					IsActive = true
				});

				var occupant = new DefaultOccupantMapping {
					Id = "viz_admin",
					FirstName = "Admin",
					LastName = "Vizelia",
					OrganizationId = organization.Id,
					ClassificationItemWorktypeLocalId = vizeliaWorkClassificationItem.LocalId,
					ClassificationItemOrganizationLocalId = vizeliaOrganizationClassificationItem.LocalId,
					LocationLocalId = site.LocalId,
					LocationTypeName = HierarchySpatialTypeName.IfcSite
				};
				entitiesToCreate.Add(occupant);

				TracingService.Write("Starting the mapping process...");
				IMappingSummary summary = MappingService.MapEntities(entitiesToCreate);
				string entitiesMappingLog = summary.MappingLog.ToString();
				TracingService.Write("Mapping process complete");

				var usersToCreate = new List<IMappingRecordConverter>();
				TracingService.Write("Preparing Admin Membership User...");
				// create the Admin user (Admin login)
				string password = Membership.GeneratePassword(Membership.MinRequiredPasswordLength,
															  Membership.MinRequiredNonAlphanumericCharacters);
				password = adminPassword;

				var broker = Helper.CreateInstance<TenantBroker>();
				var tenant = broker.GetItem(ContextHelper.ApplicationName);

				var adminUser = new DefaultFOLMembershipUserMapping {
					OccupantId = occupant.Id,
					UserName = "Admin",
					FirstName = "Admin",
					LastName = "Vizelia",
					IsApproved = true,
					Email = tenant.AdminEmail,
					Comment = "Admin account",
					Password = password,
					NeverExpires = true
				};

				if (Membership.GetUser(adminUser.UserName) != null)
					Membership.DeleteUser(adminUser.UserName, true);

				usersToCreate.Add(adminUser);

				TracingService.Write("Preparing Scheduler Membership User...");
				// create the scheduler service login (Scheduler will use these to authenticate 
				var schedulerUser = new DefaultFOLMembershipUserMapping {
					OccupantId = occupant.Id,
					UserName = VizeliaConfiguration.Instance.Authentication.SchedulerUsername,
					FirstName = VizeliaConfiguration.Instance.Authentication.SchedulerUsername,
					LastName = "Vizelia",
					IsApproved = true,
					Email = tenant.AdminEmail,
					Comment = "Scheduler account",
					Password = VizeliaConfiguration.Instance.Authentication.SchedulerPassword,
					NeverExpires = true
				};

				if (Membership.GetUser(schedulerUser.UserName) != null)
					Membership.DeleteUser(schedulerUser.UserName, true);

				usersToCreate.Add(schedulerUser);

				TracingService.Write("Starting the mapping process for users...");
				summary = MappingService.MapEntities(usersToCreate);
				TracingService.Write("Mapping process for users complete");

				// Make sure the Scheduler user password does not have to be changed (like windows service account) on first login
				// this tricks the membership provider to think we have already changed
				//MembershipUser schedulerMembershipUser = Membership.GetUser(schedulerUser.UserName, false);
				//if (schedulerMembershipUser != null)
				//    schedulerMembershipUser.ChangePassword(schedulerUser.Password, schedulerUser.Password);

				summary.MappingLog.AppendLine("password : " + password);
				//Roles.AddUserToRoles(user.UserName, new[] { "core_usermanagement_read", "core_usermanagement_write" })
				string log = entitiesMappingLog + Environment.NewLine + summary.MappingLog;
				MemoryStream stream = log.GetStream();
				var result = new StreamResult(stream, "InitDataBase_Log", "txt", MimeType.Text);

				TracingService.Write("Done");

				var currentOperationId = LongRunningOperationService.GetCurrentOperationId();
				CacheService.Add(string.Format("admin_pwd_{0}", currentOperationId), password, 60);

				return new LongRunningOperationResult(LongRunningOperationStatus.ResultAvailable, result);
			}
		}

		/// <summary>
		/// Builds the classification definitions.
		/// </summary>
		/// <returns></returns>
		private static IEnumerable<IMappingRecordConverter> BuildClassificationDefinitions() {
			/*
			INSERT INTO config_ClassificationDefinition
			(App, Category, LocalId , IconCls, IconRootOnly)
			VALUES
			('FacilityOnLine', 'actionrequest' ,	'CTIREQUEST',		'viz-icon-small-actionrequest',		0) ,
			('FacilityOnLine', 'work' ,				'CTIWORK',			'viz-icon-small-work',				0) ,
			('FacilityOnLine', 'task' ,				'CTITASK',			'viz-icon-small-task',				0) ,
			('FacilityOnLine', 'space' ,			'SPACE',			NULL				,				0) ,
			('FacilityOnLine', 'equipment' ,		'EQUIPMENT',		'viz-icon-small-equipment'	,		0) ,
			('FacilityOnLine', 'organization' ,		'ORGANIZATION',		NULL	,							0) ,
			('FacilityOnLine', 'worktype' ,			'WORKTYPE',			'viz-icon-small-worktype',			0) ,
			('FacilityOnLine', 'meter',				'METER',			'viz-icon-small-meter'	,			1),
			('FacilityOnLine', 'alarm',				'ALARM',			'viz-icon-small-alarm',				1)
			*/
			return new List<IMappingRecordConverter> {
			                                 	new DefaultClassificationDefinition {
			                                 	                             	Category = "actionrequest",
			                                 	                             	LocalId = "CTIREQUEST",
			                                 	                             	IconCls = "viz-icon-small-actionrequest",
			                                 	                             	IconRootOnly = false
			                                 	                             },
			                                 	new DefaultClassificationDefinition {
			                                 	                             	Category = "work",
			                                 	                             	LocalId = "CTIWORK",
			                                 	                             	IconCls = "viz-icon-small-work",
			                                 	                             	IconRootOnly = false
			                                 	                             },
			                                 	new DefaultClassificationDefinition {
			                                 	                             	Category = "task",
			                                 	                             	LocalId = "CTITASK",
			                                 	                             	IconCls = "viz-icon-small-task",
			                                 	                             	IconRootOnly = false
			                                 	                             },
			                                 	new DefaultClassificationDefinition {
			                                 	                             	Category = "space",
			                                 	                             	LocalId = "SPACE",
			                                 	                             	IconRootOnly = false
			                                 	                             },
			                                 	new DefaultClassificationDefinition {
			                                 	                             	Category = "equipment",
			                                 	                             	LocalId = "EQUIPMENT",
			                                 	                             	IconCls = "viz-icon-small-equipment",
			                                 	                             	IconRootOnly = false
			                                 	                             },
			                                 	new DefaultClassificationDefinition {
			                                 	                             	Category = "organization",
			                                 	                             	LocalId = "ORGANIZATION",
			                                 	                             	IconRootOnly = false
			                                 	                             },
			                                 	new DefaultClassificationDefinition {
			                                 	                             	Category = "worktype",
			                                 	                             	LocalId = "WORKTYPE",
			                                 	                             	IconCls = "viz-icon-small-worktype",
			                                 	                             	IconRootOnly = false
			                                 	                             },
			                                 	new DefaultClassificationDefinition {
			                                 	                             	Category = "meter",
			                                 	                             	LocalId = "METER",
			                                 	                             	IconCls = "viz-icon-small-meter",
			                                 	                             	IconRootOnly = true
			                                 	                             },
			                                 	new DefaultClassificationDefinition {
			                                 	                             	Category = "alarm",
			                                 	                             	LocalId = "ALARM",
			                                 	                             	IconCls = "viz-icon-small-alarm",
			                                 	                             	IconRootOnly = true
			                                 	                             },
			                                 	new DefaultClassificationDefinition {
			                                 	                             	Category = "eventlog",
			                                 	                             	LocalId = "EVENTLOG",
			                                 	                             	IconCls = "viz-icon-small-eventlog",
			                                 	                             	IconRootOnly = true
			                                 	                             },
			                                 	new DefaultClassificationDefinition {
			                                 	                             	Category = "mail",
			                                 	                             	LocalId = "MAIL",
			                                 	                             	IconCls = "viz-icon-small-mail",
			                                 	                             	IconRootOnly = false
			                                 	                             },
												new DefaultClassificationDefinition {
			                                 	                             	Category = "kpi",
			                                 	                             	LocalId = "KPI",
			                                 	                             	IconCls = "viz-icon-small-chart-kpi",
			                                 	                             	IconRootOnly = true
			                                 	                             }
			                                 };
		}


		/// <summary>
		/// Starts the mapping process. 
		/// This mapping is invoked manually. For scheduled mapping, see the PerformScheduledMapping method.
		/// </summary>
		/// <param name="operationId">The operation unique id.</param>
		/// <param name="fileName">The name of the mapping file.</param>
		/// <param name="mappingFile">The stream mapping file.</param>
		public void BeginMapping(Guid operationId, string fileName, Stream mappingFile) {
			LongRunningOperationService.Invoke(
				operationId,
				() => {
					LongRunningOperationResult result = MappingService.Map(fileName, mappingFile);

					return result;
				});
		}

		/// <summary>
		/// Gets all mappings.
		/// </summary>
		/// <returns></returns>
		public string GetAllMappings() {
			string allMappingObjects = MappingService.GetAllMappingObjects();
			return allMappingObjects;
		}

		/// <summary>
		/// Gets the allowed mapping file extensions.
		/// </summary>
		/// <returns></returns>
		public IEnumerable<string> GetAllowedMappingFileExtensions() {
			IEnumerable<string> allowedExtensions = MappingService.GetAllowedMappingFileExtensions();

			return allowedExtensions;
		}


		/// <summary>
		/// Tests the connection to the file source.
		/// This is a long running operation that returns a business object.
		/// </summary>
		/// <param name="keyMappingTask">The key of the mapping task.</param>
		/// <param name="operationId">The operation id.</param>
		public void BeginTestConnection(string keyMappingTask, Guid operationId) {
			LongRunningOperationService.Invoke(
				operationId,
				() => {
					MappingTask mappingTask = GetMappingTask(keyMappingTask);
					using (IRemoteFileAccessClient client = RemoteFileAccessService.GetClient(mappingTask)) {
						RemoteFileAccessConnectionTestResult testResult = client.TestConnection();
						return new LongRunningOperationResult(LongRunningOperationStatus.ResultAvailable, testResult);
					}
				});
		}


		/// <summary>
		/// Performs the scheduled mapping.
		/// </summary>
		/// <param name="keyMappingTask">The key mapping task.</param>
		public void PerformScheduledMapping(string keyMappingTask) {
			using (TracingService.StartTracing("Scheduled Mapping", keyMappingTask)) {
				TracingService.Trace("PerformScheduledMapping", () => {
					TracingService.Write("Starting scheduled mapping for mapping task with key " + keyMappingTask);
					TracingService.Write("Getting mapping task from database");
					MappingTask task = GetMappingTask(keyMappingTask);

					if (task == null) {
						TracingService.Write(TraceEntrySeverity.Warning, "Mapping task does not exist. Possibly deleted and still referenced by this scheduled job.");
					}
					else {
						// Put a lock on the task so it won't get executed by other threads / machines.
						ScheduledTaskHelper.RunLocked<MappingTask>(keyMappingTask, () => PerformScheduledMappingTask(task));
					}

					TracingService.Write("Scheduled mapping done!");
				});
			}

		}

		/// <summary>
		/// Gets the mapping task.
		/// </summary>
		/// <param name="keyMappingTask">The key mapping task.</param>
		/// <returns></returns>
		private static MappingTask GetMappingTask(string keyMappingTask) {
			IBroker<MappingTask> broker = BrokerFactory.CreateBroker<MappingTask>();
			MappingTask task = broker.GetItem(keyMappingTask);

			return task;
		}


		/// <summary>
		/// Performs the scheduled mapping task.
		/// </summary>
		/// <param name="mappingTask">The file source.</param>
		private void PerformScheduledMappingTask(MappingTask mappingTask) {

			MappingContext mappingContext = MappingService.StartMappingProcess();
			IMappingSummary summary = mappingContext.MappingSummary;

			mappingContext.MappingConfiguration = new MappingConfiguration() {
				AutoCreateMeterOptions = mappingTask.AutoCreateMeterOptions,
				AutoCreatedMetersKeyLocation = mappingTask.AutoCreatedMetersKeyLocation
			};

			// Get the occupant related to the mapping task, if such was specified.
			if (!string.IsNullOrWhiteSpace(mappingTask.Emails)) {
				summary.Emails = mappingTask.Emails;
			}
			else {
				summary.ShouldSendReport = false;
			}

			DirectoryInfo workingDirectory = null;

			try {
				if (!ExternalCrudNotificationService.IsReadyForNotifications()) {
					TracingService.Write(TraceEntrySeverity.Warning, const_external_crud_notification_service_not_ready);
				}
				else {
					workingDirectory = GetWorkingDirectory();
					string remouteOutputBasePath = VizeliaConfiguration.Instance.Mapping.ProcessedFilesRelativePath.Replace('/', '\\').TrimEnd('\\');
					string remoteOutputPathForCurrent = remouteOutputBasePath + "\\" + DateTime.Now.ToString(const_date_time_format);
					TracingService.Write("Obtaining remote file access client for address {0} and protocol {1}", mappingTask.Address, mappingTask.FileSourceType);

					using (IRemoteFileAccessClient client = RemoteFileAccessService.GetClient(mappingTask)) {
						bool anyFilesProcessed;

						try {
							anyFilesProcessed = PerformMappingOnRemoteFiles(client, workingDirectory, mappingContext, remoteOutputPathForCurrent);
						}
						catch (Exception e) {
							// We finish the mapping process so the log can be written to the remote path.
							if (summary.TotalErrorCount == 0) {
								// The exception wasn't logged internally. We'll log it.
								summary.LogError(e, Langue.msg_mapping_error);
							}
							MappingService.FinishMappingProcess(summary);
							WriteMappingLogToRemoteFile(client, remouteOutputBasePath, remoteOutputPathForCurrent, summary);
							throw;
						}

						if (anyFilesProcessed || summary.TotalErrorCount > 0) {
							// We finish the mapping process so the log can be written to the remote path.
							MappingService.FinishMappingProcess(summary);
							WriteMappingLogToRemoteFile(client, remouteOutputBasePath, remoteOutputPathForCurrent, summary);
						}
						else {
							TracingService.Write("Mapping finished on empty input. Not sending summary.");
						}
					}
				}
			}
			catch (Exception exception) {
				TracingService.Write(exception);

				// We finish the mapping process if it wasn't finished before.
				if (summary.IsMappingInProgress) {
					MappingService.FinishMappingProcess(summary, exception);
				}
				// Keep throwing it up so it will appear in the task step history log.
				throw;
			}
			finally {
				if (workingDirectory != null) {
					DeleteWorkingDirectory(workingDirectory);
				}
			}
		}

		/// <summary>
		/// Writes the mapping log to a remote file.
		/// </summary>
		/// <param name="client">The client.</param>
		/// <param name="remouteOutputBasePath"> </param>
		/// <param name="remoteOutputPath">The remote output path.</param>
		/// <param name="summary">The summary.</param>
		private static void WriteMappingLogToRemoteFile(IRemoteFileAccessClient client, string remouteOutputBasePath, string remoteOutputPath, IMappingSummary summary) {
			try {
				client.CreateDirectory(remoteOutputPath);
				using (Stream stream = summary.MappingLog.ToString().GetStream()) {
					string filename = remoteOutputPath + "\\MappingLog.txt";
					client.UploadFile(filename, stream);
				}

				client.DeleteOldDirectories(remouteOutputBasePath, DateTime.Now.AddDays(-1 * VizeliaConfiguration.Instance.Mapping.DaysToKeepProcessedFiles));
			}
			catch (Exception exception) {
				TracingService.Write(TraceEntrySeverity.Error, "Cannot write mapping log to remote file: " + exception);
			}
		}

		/// <summary>
		/// Deletes the working directory.
		/// </summary>
		/// <param name="workingDirectory">The working directory.</param>
		private static void DeleteWorkingDirectory(DirectoryInfo workingDirectory) {
			try {
				TracingService.Write("Deleting working directory");

				DirectoryInfo parent = workingDirectory.Parent;
				Directory.Delete(workingDirectory.FullName, true);

				// Delete the directory for the date, only if it's empty.
				if (parent != null && parent.Parent != null && parent.Parent.Name.Equals(const_working_directory_subdirectory_name)) {
					try {
						// Only delete it if it's empty. It's important to try-catch here and not check and delete, since 
						// if another task is running now it might create content in the directory between our check and delete 
						// operations. In this scenario, the delete operation will throw anyway.
						parent.Delete();
					}
					catch { }
				}
			}
			catch (Exception exception) {
				TracingService.Write("Error deleting working directory: {0}", exception);
				throw new VizeliaException(const_cannot_delete_working_directory_message, exception);
			}
		}

		/// <summary>
		/// Performs the mapping on remote files.
		/// </summary>
		/// <param name="client">The client.</param>
		/// <param name="workingDirectory">The working directory.</param>
		/// <param name="mappingContext">The mapping context.</param>
		/// <param name="remoteOutputPath">The remote output path.</param>
		/// <returns>
		/// A boolean value indicating whether any files were processed.
		/// </returns>
		private bool PerformMappingOnRemoteFiles(IRemoteFileAccessClient client, DirectoryInfo workingDirectory, MappingContext mappingContext, string remoteOutputPath) {
			TracingService.Write("Connecting...");
			client.Connect();
			TracingService.Write("Connected, checking file list on file source.");

			// Loop while remote file source still has files, we take the oldest (or with first filename) in each iteration.
			// Here may be an error if the file is in use or access denied or local disk writing problem.
			string filename;
			bool anyFilesProcessed = false;

			int queueLength;
			while (null != (filename = GetNextFileToProcess(client, out queueLength))) {
				TracingService.Write("Going to process file \"{0}\" (Queue length: {1})", filename, queueLength);
				bool filesProcessedThisIteration = ProcessRemoteFile(workingDirectory, mappingContext, filename, client, remoteOutputPath);
				anyFilesProcessed = anyFilesProcessed || filesProcessedThisIteration;
			}

			TracingService.Write("No more files to process in file source.");

			return anyFilesProcessed;
		}

		private bool ProcessRemoteFile(DirectoryInfo workingDirectory, MappingContext mappingContext, string filename, IRemoteFileAccessClient client, string remoteOutputPath) {
			bool processedAnyFiles = false;
			string newFilenameOnServer = RenameFileForAccess(client, filename);

			try {
				TracingService.Write("Downloading renamed file \"{0}\" to local path \"{1}\" with original filename", newFilenameOnServer, workingDirectory);
				DownloadFile(client, workingDirectory, newFilenameOnServer, filename);

				// Here may be an error if the file is invalid (corrupt archive or mapping error).
				ProcessFile(workingDirectory, mappingContext);
				processedAnyFiles = true;
				MoveProcessedRemoteFile(client, newFilenameOnServer, filename, remoteOutputPath);
			}
			catch (Exception exception) {
				TracingService.Write(TraceEntrySeverity.Error, "An error has occured: " + exception);
				mappingContext.MappingSummary.LogError(exception, string.Empty, true);

				if (!VizeliaConfiguration.Instance.Mapping.ShouldSkipFileOnError) {
					try {
						// Rename the file back if possible.
						client.RenameFile(newFilenameOnServer, filename);
						TracingService.Write("The file has been renamed back to its original filename.");
					}
					catch (Exception renameException) {
						string message = string.Format("The file could not have been renamed back to its original filename: {0}", renameException);
						TracingService.Write(TraceEntrySeverity.Error, message);
						mappingContext.MappingSummary.LogError(renameException, message, true);
					}

					throw;
				}

				// Else:
				TracingService.Write("Skipping file on error, as configured.");
				MoveProcessedRemoteFile(client, newFilenameOnServer, filename, remoteOutputPath);
			}

			return processedAnyFiles;
		}

		/// <summary>
		/// Moves the processed remote file to the remote output folder.
		/// </summary>
		/// <param name="client">The client.</param>
		/// <param name="newFilenameOnServer">The new filename on server.</param>
		/// <param name="originalFilename">The original filename.</param>
		/// <param name="remoteOutputPath">The remote output path.</param>
		private void MoveProcessedRemoteFile(IRemoteFileAccessClient client, string newFilenameOnServer, string originalFilename, string remoteOutputPath) {
			client.CreateDirectory(remoteOutputPath);
			string destFilename = remoteOutputPath + "\\" + originalFilename;
			client.RenameFile(newFilenameOnServer, destFilename);
		}

		/// <summary>
		/// Gets the next file to process.
		/// </summary>
		/// <param name="client">The client.</param>
		/// <param name="queueLength">Length of the queue on the remote file source.</param>
		/// <returns>
		/// The next file name if such exists, otherwise null.
		/// </returns>
		private string GetNextFileToProcess(IRemoteFileAccessClient client, out int queueLength) {
			string nextFilename;
			IEnumerable<string> filesByDate = client.GetFileNamesSortedByDate();

			switch (VizeliaConfiguration.Instance.Mapping.FetchingOrder) {
				case MappingFetchingOrder.ByTime:
					// Input enumerable is already sorted by datetime, oldest first.
			        var files = filesByDate.ToList();
					nextFilename = files.FirstOrDefault(CanMapFile);
					queueLength = files.Count;
					break;
				case MappingFetchingOrder.ByFilename:
					// Reorder by file name
                    var filesByName = filesByDate.OrderBy(n => n).ToList();
					nextFilename = filesByName.FirstOrDefault(CanMapFile);
					queueLength = filesByName.Count;
					break;
				default:
					throw new ArgumentOutOfRangeException("MappingFetchingOrder");
			}

			return nextFilename;
		}

		/// <summary>
		/// Determines whether the mapping business layer can map the specified filename.
		/// </summary>
		/// <param name="filename">The filename.</param>
		/// <returns>
		///   <c>true</c> if the mapping business layer can map the specified filename; otherwise, <c>false</c>.
		/// </returns>
		private static bool CanMapFile(string filename) {
			bool canMapFile = CompressionHelper.IsArchive(filename) || MappingService.CanMapFile(filename);

			return canMapFile;
		}

		/// <summary>
		/// Renames the file on the server to indicate that it is not in use.
		/// </summary>
		/// <param name="client">The client.</param>
		/// <param name="filename">The filename.</param>
		/// <returns>The new filename on the server.</returns>
		private static string RenameFileForAccess(IRemoteFileAccessClient client, string filename) {
			TracingService.Write("Going to rename file \"{0}\"", filename);

			// Rename it to make sure it's not in use. 
			// This action has some tolerance to the file being in use for a reasonable amount of time, defined in the config file.
			Match match = Regex.Match(filename, const_new_filename_on_server_regex);

			string filenamePart;
			string extensionPart;

			if (match.Success && match.Groups["guid"].Success && match.Groups["filename"].Success && match.Groups["extension"].Success) {
				TracingService.Write("The file was already renamed by us, we will rename it again.");
				// This means we already tried to download it once and renamed it then.
				// Lets rename it again in order to make sure it's not in use.
				filenamePart = match.Groups["filename"].Value;
				extensionPart = match.Groups["extension"].Value;
			}
			else {
				int seperator = filename.LastIndexOf('.');
				filenamePart = filename.Substring(0, seperator);
				extensionPart = filename.Substring(seperator + 1);
			}

			string newFilenameOnServer = string.Format(const_new_filename_on_server_format, filenamePart, Guid.NewGuid(), extensionPart);

			try {
				client.RenameFile(filename, newFilenameOnServer);
			}
			catch (Exception ftpException) {
				string message = string.Format(const_cannot_rename_file_for_access_message, filename, newFilenameOnServer);
				throw new VizeliaException(message, ftpException);
			}

			TracingService.Write("File renamed successfully to: {0}", newFilenameOnServer);

			return newFilenameOnServer;
		}


		/// <summary>
		/// Downloads the file.
		/// </summary>
		/// <param name="client">The client.</param>
		/// <param name="workingDirectory">The working directory.</param>
		/// <param name="serverFilename">The server filename.</param>
		/// <param name="filename">The filename.</param>
		private static void DownloadFile(IRemoteFileAccessClient client, DirectoryInfo workingDirectory, string serverFilename, string filename) {
			// Download the file locally as its original filename.
			string localDestinationFilename = Path.Combine(workingDirectory.FullName, filename);
			client.DownloadFile(serverFilename, localDestinationFilename);
		}

		/// <summary>
		/// Processes the file.
		/// </summary>
		/// <param name="workingDirectory">The working directory.</param>
		/// <param name="mappingContext">The mappingContext.</param>
		private void ProcessFile(DirectoryInfo workingDirectory, MappingContext mappingContext) {
			TracingService.Write("Examining local directory for archives that need to be decompressed.");
			bool inArchive = CompressionHelper.ExtractAllArchives(workingDirectory.FullName, false, const_max_nested_archives_depth);

			try {
				MapFilesInWorkingDirectory(workingDirectory, mappingContext, inArchive);
			}
			finally {
				TracingService.Write("Deleting all content of working directory.");
				workingDirectory.DeleteAllContent();
			}
		}


		/// <summary>
		/// Maps the files in the working directory.
		/// Each file is deleted after its mapping is complete.
		/// When the method is done, the working directory is empty except for empty subdirectories that may be created.
		/// </summary>
		/// <param name="workingDirectory">The working directory.</param>
		/// <param name="mappingContext">The mapping Context.</param>
		/// <param name="inArchive">if set to <c>true</c>, we are processing files from an archive.</param>
		private void MapFilesInWorkingDirectory(DirectoryInfo workingDirectory, MappingContext mappingContext, bool inArchive) {
			TracingService.Write("Preparing to perform mapping on local downloaded and extracted files, if any exist.");

			// This will get all the files in subdirectories too.
			FileInfo[] allFiles = workingDirectory.GetFiles("*.*", SearchOption.AllDirectories);
			FileInfo[] sortedFiles = allFiles.OrderBy(f => f.CreationTime).ToArray();

			for (int i = 0; i < sortedFiles.Length; i++) {
				MapFile(mappingContext, sortedFiles[i], inArchive, i, sortedFiles.Length);
			}
		}

		/// <summary>
		/// Maps the file.
		/// </summary>
		/// <param name="mappingContext">The mapping context.</param>
		/// <param name="currentFile">The current file.</param>
		/// <param name="inArchive">if set to <c>true</c>, we are processing files from an archive.</param>
		/// <param name="fileIndex"> </param>
		/// <param name="filesCount"> </param>
		private static void MapFile(MappingContext mappingContext, FileInfo currentFile, bool inArchive, int fileIndex, int filesCount) {
			if (currentFile.Length == 0) {
				TracingService.Write(string.Format("Skipping empty file '{0}'.", currentFile.Name));
				return;
			}

			if (inArchive && !MappingService.CanMapFile(currentFile.Name)) {
				// We skip unmappable files only when they are extracted from archive files locally.
				TracingService.Write(TraceEntrySeverity.Warning, string.Format("Skipping unmappable file '{0}' since it originates in extracted archive.", currentFile.Name));
			}
			else {
				string progress = inArchive ? string.Format(" ({0} out of {1} in archive)", (fileIndex + 1), filesCount) : string.Empty;
				TracingService.Write("Opening file for mapping{0}: \"{1}\"", progress, currentFile.FullName);

				using (FileStream fileStream = File.Open(currentFile.FullName, FileMode.Open, FileAccess.Read, FileShare.None)) {
					TracingService.Write("Calling the MappingService to map the file");
					MappingService.Map(currentFile.Name, fileStream, mappingContext);
					fileStream.Close();
				}

				TracingService.Write("File mapped and closed.");
			}

		}


		/// <summary>
		/// Gets the working directory.
		/// </summary>
		private static DirectoryInfo GetWorkingDirectory() {
			TracingService.Write("Creating working directory");
			string subdirectoryName = string.Format("{0}\\{1}", DateTime.Today.ToString("yyyy'-'MM'-'dd"), Guid.NewGuid().ToString());
			string workingDirectoryName = Path.Combine(GetBaseMappingWorkingDirectory(), subdirectoryName);


			var workingDirectory = new DirectoryInfo(workingDirectoryName);
			workingDirectory.Create();

			TracingService.Write("Working directory created: {0}", workingDirectory.FullName);

			return workingDirectory;
		}

		/// <summary>
		/// Gets the base mapping working directory.
		/// </summary>
		/// <returns></returns>
		private static string GetBaseMappingWorkingDirectory() {
			return Path.Combine(VizeliaConfiguration.Instance.Mapping.WorkDirectoryPath,
			                    const_working_directory_subdirectory_name);
		}

		#region MappingTask

		/// <summary>
		/// Gets a json store for the business entity MappingTask.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<MappingTask> MappingTask_GetStore(PagingParameter paging, PagingLocation location) {
			var broker = Helper.CreateInstance<MappingTaskBroker>();
			return broker.GetStore(paging, location);
		}

		/// <summary>
		/// Saves a crud store for the business entity MappingTask.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<MappingTask> MappingTask_SaveStore(CrudStore<MappingTask> store) {
			var broker = Helper.CreateInstance<MappingTaskBroker>();
			return broker.SaveStore(store);
		}

		/// <summary>
		/// Gets a list for the business entity MappingTask. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		public List<MappingTask> MappingTask_GetAll(PagingParameter paging, PagingLocation location) {
			int total;
			var broker = Helper.CreateInstance<MappingTaskBroker>();
			return broker.GetAll(paging, location, out total);
		}

		/// <summary>
		/// Gets a specific item for the business entity MappingTask.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public MappingTask MappingTask_GetItem(string Key) {
			var broker = Helper.CreateInstance<MappingTaskBroker>();
			return broker.GetItem(Key);
		}

		/// <summary>
		/// Loads a specific item for the business entity MappingTask.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse MappingTask_FormLoad(string Key) {
			var broker = Helper.CreateInstance<MappingTaskBroker>();
			return broker.FormLoad(Key);
		}


		/// <summary>
		/// Creates a new business entity MappingTask and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse MappingTask_FormCreate(MappingTask item) {
			var broker = Helper.CreateInstance<MappingTaskBroker>();
			return broker.FormCreate(item);
		}

		/// <summary>
		/// Updates an existing business entity MappingTask and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse MappingTask_FormUpdate(MappingTask item) {
			var broker = Helper.CreateInstance<MappingTaskBroker>();
			return broker.FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse MappingTask_FormUpdateBatch(string[] keys, MappingTask item) {
			var broker = Helper.CreateInstance<MappingTaskBroker>();
			return broker.FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Deletes an existing business entity MappingTask.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool MappingTask_Delete(MappingTask item) {
			var broker = Helper.CreateInstance<MappingTaskBroker>();
			return broker.Delete(item);
		}

		/// <summary>
		/// Gets a json store for the business entity MeterDataExportTask.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<MeterDataExportTask> MeterDataExportTask_GetStore(PagingParameter paging, PagingLocation location) {
			MeterDataExportTaskBroker broker = Helper.CreateInstance<MeterDataExportTaskBroker>();
			return broker.GetStore(paging, location);
		}

		/// <summary>
		/// Saves a crud store for the business entity MeterDataExportTask.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<MeterDataExportTask> MeterDataExportTask_SaveStore(CrudStore<MeterDataExportTask> store) {
			MeterDataExportTaskBroker broker = Helper.CreateInstance<MeterDataExportTaskBroker>();
			return broker.SaveStore(store);
		}

		/// <summary>
		/// Gets a list for the business entity MeterDataExportTask. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		public List<MeterDataExportTask> MeterDataExportTask_GetAll(PagingParameter paging, PagingLocation location) {
			int total;
			MeterDataExportTaskBroker broker = Helper.CreateInstance<MeterDataExportTaskBroker>();
			return broker.GetAll(paging, location, out total);
		}

		/// <summary>
		/// Gets a specific item for the business entity MeterDataExportTask.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public MeterDataExportTask MeterDataExportTask_GetItem(string Key) {
			MeterDataExportTaskBroker broker = Helper.CreateInstance<MeterDataExportTaskBroker>();
			return broker.GetItem(Key);
		}

		/// <summary>
		/// Loads a specific item for the business entity MeterDataExportTask.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse MeterDataExportTask_FormLoad(string Key) {
			MeterDataExportTaskBroker broker = Helper.CreateInstance<MeterDataExportTaskBroker>();
			return broker.FormLoad(Key);
		}


		/// <summary>
		/// Creates a new business entity MeterDataExportTask and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <param name="meters"> </param>
		/// <returns></returns>
		public FormResponse MeterDataExportTask_FormCreate(MeterDataExportTask item, CrudStore<Meter> meters) {
			FormResponse response;
			var broker = Helper.CreateInstance<MeterDataExportTaskBroker>();
			var meterBroker = Helper.CreateInstance<MeterBroker>();

			using (var t = Helper.CreateTransactionScope()) {
				response = broker.FormCreate(item);

				if (response.success) {
					try {
						var exportTask = response.data as MeterDataExportTask;
						if (exportTask != null) {
							meterBroker.SaveMeterDataExportTaskMeters(exportTask, meters);
						}
						t.Complete();
					}
					catch (Exception ex) {
						response.success = false;
						response.msg = ex.Message;
					}
				}
			}

			return response;
		}

		/// <summary>
		/// Updates an existing business entity MeterDataExportTask and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <param name="meters"> </param>
		/// <returns></returns>
		public FormResponse MeterDataExportTask_FormUpdate(MeterDataExportTask item, CrudStore<Meter> meters) {
			FormResponse response;
			var broker = Helper.CreateInstance<MeterDataExportTaskBroker>();
			var meterBroker = Helper.CreateInstance<MeterBroker>();

			using (var t = Helper.CreateTransactionScope()) {
				response = broker.FormUpdate(item);

				if (response.success) {
					try {
						var exportTask = response.data as MeterDataExportTask;
						if (exportTask != null) {
							meterBroker.SaveMeterDataExportTaskMeters(exportTask, meters);
						}
						t.Complete();
					}
					catch (Exception ex) {
						response.success = false;
						response.msg = ex.Message;
					}
				}
			}

			return response;
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse MeterDataExportTask_FormUpdateBatch(string[] keys, MeterDataExportTask item) {
			MeterDataExportTaskBroker broker = Helper.CreateInstance<MeterDataExportTaskBroker>();
			return broker.FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Deletes an existing business entity MeterDataExportTask.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool MeterDataExportTask_Delete(MeterDataExportTask item) {
			MeterDataExportTaskBroker broker = Helper.CreateInstance<MeterDataExportTaskBroker>();
			return broker.Delete(item);
		}

		/// <summary>
		/// Gets a list of the given business entity, ready for export.
		/// </summary>
		/// <param name="entityType">Type of the entity, expressed as the class name.</param>
		/// <param name="paging">The paging parameter.</param>
		/// <returns></returns>
		public List<BaseBusinessEntity> GetEntitiesForExport(string entityType, PagingParameter paging) {
			// Get the business entity from the Common assembly.
			string fullEntityType = string.Format("Vizelia.FOL.BusinessEntities.{0}", entityType);
			Type type = typeof(BaseBusinessEntity).Assembly.GetType(fullEntityType);

			if (type == null) {
				throw new ArgumentException(string.Format("Could not find type {0}", entityType));
			}

			var brokerObject = BrokerFactory.CreateBroker(type);

			var mappingBroker = brokerObject as IMappingBroker;
			if (mappingBroker == null) {
				throw new ArgumentException("No mapping broker found for given entity.");
			}

			int total;
			List<BaseBusinessEntity> entities = mappingBroker.GetAllForExport(paging, PagingLocation.Database, out total);

			return entities;
		}

		#endregion

		/// <summary>
		/// Maps the excel to XML.
		/// </summary>
		/// <param name="memoryStream">The memory stream.</param>
		/// <param name="operationId">The operation id.</param>
		/// <returns></returns>
		public Guid MapExcelToXml(Stream memoryStream, Guid? operationId = null) {
			Stream localStream = new MemoryStream();
			memoryStream.CopyTo(localStream);
			if (operationId.HasValue) {
				LongRunningOperationService.Invoke(operationId.Value, () => DoMapExcelToXml(localStream));
			}
			else {
				operationId = Guid.NewGuid();
				ThreadPool.QueueUserWorkItem(callBack => {
					try {
						LongRunningOperationService.Invoke(operationId.Value, () => DoMapExcelToXml(localStream));
					}
					catch {
						// Do nothing so background thread won't be killed.
					}
				});
			}

			return operationId.Value;
		}

		/// <summary>
		/// Does the map excel to XML.
		/// </summary>
		/// <returns></returns>
		private static LongRunningOperationResult DoMapExcelToXml(Stream memoryStream) {
			LongRunningOperationResult retVal;
			using (TracingService.StartTracing("MapExcelToXml", string.Empty)) {
				TracingService.Write("Reading entities for mapping...");
				// Getting the entities from the excel file.
				var entitiesList = MappingConversionService.GetEntities(memoryStream);

				TracingService.Write("Mapping entities to xml stream...");

				// Export the entites to xml file.
				var entitiesXml = MappingService.Providers["XmlSerialization"].Export(entitiesList);

				var result = new StreamResult(entitiesXml.GetStream(), "BusinessEntities", "xml", MimeType.Xml);

				TracingService.Write("Done");

				retVal = new LongRunningOperationResult(LongRunningOperationStatus.ResultAvailable, result);
			}
			return retVal;
		}

		/// <summary>
		/// Exports the meter data to CSV.
		/// </summary>
		public void ExportMeterData(string keyMeterDataExportTask) {
			using (TracingService.StartTracing("ExportMeterData", keyMeterDataExportTask)) {
				TracingService.Trace("ExportMeterData", () => ExportMeterDataInternal(keyMeterDataExportTask));
			}
		}

		/// <summary>
		/// Cleans the mapping temp folder.
		/// </summary>
		public void CleanMappingTempFolder() {
			string mappingTempDirectoryPath = GetBaseMappingWorkingDirectory();
			if (Directory.Exists(mappingTempDirectoryPath)) {
				try {
					var mappingTempDirectory = new DirectoryInfo(mappingTempDirectoryPath);
					foreach (FileInfo file in mappingTempDirectory.GetFiles()) {
						file.Delete();
					}
					foreach (DirectoryInfo subDirectory in mappingTempDirectory.GetDirectories()) {
						subDirectory.Delete(true);
					}
				}
				catch (Exception ex) {
					TracingService.Write(TraceEntrySeverity.Error, "there was an error while deleting mapping temp directory : {0}", ex);
				}
			}

		}

		/// <summary>
		/// Exports the meter data (internal).
		/// </summary>
		/// <param name="keyMeterDataExportTask">The key meter data export task.</param>
		private void ExportMeterDataInternal(string keyMeterDataExportTask) {
			ExternalCrudNotificationService.ThrowOnNotReady();

			MeterDataExportTask exportTask = MeterDataExportTask_GetItem(keyMeterDataExportTask);
			MappingTask mappingTask = MappingTask_GetItem(exportTask.KeyMappingTask);

			if (mappingTask == null) {
				throw new VizeliaException("Could not find mapping task to access file source.");
			}

			// Export the meter data
			string timestamp = DateTime.UtcNow.ToString(const_date_time_format);
			string filename = string.IsNullOrWhiteSpace(exportTask.Filename) ? string.Empty : exportTask.Filename + " ";
			var files = ExportMetersToStreams(exportTask, filename, timestamp);

			if (!files.Any()) {
				TracingService.Write("No meter data to export for the selected time range of the selected meters. Ending export.");
				return;
			}

			// Compress output
			if (exportTask.Compress) {
				TracingService.Write("Compressing exported meter data...");
				string archiveFileName = string.Format("{0}{1}.zip", filename, timestamp);
				Stream compressedFile = CompressionHelper.Compress(files, archiveFileName);
				files.Clear();
				files.Add(new Tuple<string, Stream>(archiveFileName, compressedFile));
			}

			// Upload output
			TracingService.Write(string.Format("Going to upload {0} file(s) to address {1}...", files.Count, mappingTask.Address));

			using (IRemoteFileAccessClient client = RemoteFileAccessService.GetClient(mappingTask)) {
				for (int i = 0; i < files.Count; i++) {
					TracingService.Write(string.Format("Uploading file {0} out of {1}: \"{2}\"...", (i + 1), files.Count, files[i].Item1));
					client.UploadFile(files[i].Item1, files[i].Item2);
				}
			}

		}

		/// <summary>
		/// Exports the meters to streams.
		/// </summary>
		/// <param name="exportTask">The export task.</param>
		/// <param name="filename">The filename.</param>
		/// <param name="timestamp">The timestamp.</param>
		/// <returns></returns>
		private List<Tuple<string, Stream>> ExportMetersToStreams(MeterDataExportTask exportTask, string filename, string timestamp) {
			// Get the meters associated with the task, to export their meter data.
			var energyBusinessLayer = Helper.Resolve<IEnergyBusinessLayer>();
			List<string> localIds = GetMetersLocalIdsToExport(exportTask, energyBusinessLayer);

			if (!localIds.Any()) {
				// Nothing to export.
				return new List<Tuple<string, Stream>>();
			}

			TracingService.Write(string.Format("Exporting meter data for {0} meters for last {1} {2}...", localIds.Count, exportTask.TimeRange, exportTask.TimeRangeInterval.ToString()));
			List<Tuple<string, Stream>> streams = GetMetersStreams(exportTask, filename, timestamp, energyBusinessLayer, localIds);

			return streams;
		}

		/// <summary>
		/// Gets the meters streams.
		/// </summary>
		/// <param name="exportTask">The export task.</param>
		/// <param name="filename">The filename.</param>
		/// <param name="timestamp">The timestamp.</param>
		/// <param name="energyBusinessLayer">The energy business layer.</param>
		/// <param name="localIds">The local ids.</param>
		/// <returns></returns>
		private static List<Tuple<string, Stream>> GetMetersStreams(MeterDataExportTask exportTask, string filename, string timestamp, IEnergyBusinessLayer energyBusinessLayer, IEnumerable<string> localIds) {
			// Determine time range.
			DateTime startDate = Helper.DateTimeNow.AddInterval(exportTask.TimeRangeInterval, -1 * exportTask.TimeRange);
			DateTime endDate = Helper.DateTimeNow;

			var files = new List<Tuple<string, Stream>>();

			if (exportTask.SingleOutputFile) {
				// Export all the meters as a single bulk operation.
				string csv = energyBusinessLayer.MeterData_GetCSV(localIds, startDate, endDate);

				if (!string.IsNullOrWhiteSpace(csv)) {
					files.Add(new Tuple<string, Stream>(string.Format("{0}{1}.csv", filename, timestamp), csv.GetStream()));
				}
			}
			else {
				// Export each meter as a different file.
				foreach (string localId in localIds) {
					string csv = energyBusinessLayer.MeterData_GetCSV(new List<string> { localId }, startDate, endDate);

					if (!string.IsNullOrWhiteSpace(csv)) {
						files.Add(new Tuple<string, Stream>(string.Format("{0}{1} {2}.csv", filename, localId, timestamp), csv.GetStream()));
					}
				}
			}

			return files;
		}

		/// <summary>
		/// Gets the meters local ids to export.
		/// </summary>
		/// <param name="exportTask">The export task.</param>
		/// <param name="energyBusinessLayer">The energy business layer.</param>
		/// <returns></returns>
		private static List<string> GetMetersLocalIdsToExport(MeterDataExportTask exportTask, IEnergyBusinessLayer energyBusinessLayer) {
			List<string> localIds;

			if (exportTask.ExportAllMeters) {
				List<Meter> allMeters = energyBusinessLayer.Meter_GetAll(new PagingParameter(), PagingLocation.Database);
				localIds = allMeters.Select(m => m.LocalId).ToList();
			}
			else {
				JsonStore<Meter> metersToExport = energyBusinessLayer.Meter_GetStoreByKeyMeterDataExportTask(new PagingParameter(), exportTask.KeyMeterDataExportTask);
				localIds = metersToExport.records.Select(m => m.LocalId).ToList();
			}

			return localIds;
		}
	}
}

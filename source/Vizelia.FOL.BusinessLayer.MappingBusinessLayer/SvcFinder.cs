﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.ServiceModel;
using System.Xml.Linq;
using Vizelia.FOL.Infrastructure;
using Vizelia.FOL.Utilities.Builder;

namespace Vizelia.FOL.BusinessLayer {
	/// <summary>
	/// A helper for finding all functions of svc services.
	/// </summary>
	[Obsolete]
	public static class SvcFinder {

		private static AzManGenerator _mAzmanGenerator;

		/// <summary>
		/// Gets or sets the path.
		/// </summary>
		public static string BinPath { get; set; }

		/// <summary>
		/// Initializes a new instance of the <see cref="SvcFinder"/> class.
		/// </summary>
		static SvcFinder() {
			BinPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "bin");
			_mAzmanGenerator = new WCFProxyGenerator().CreateAzmanGenerator(AppDomain.CurrentDomain.BaseDirectory);
			AppDomain.CurrentDomain.AssemblyResolve += CurrentDomain_AssemblyResolve;
		}


		/// <summary>
		/// Handler for resolving assembly reference.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="args"></param>
		/// <returns></returns>
		private static Assembly CurrentDomain_AssemblyResolve(object sender, ResolveEventArgs args) {
			string[] parts = args.Name.Split(',');
			string file = BinPath + "\\" + parts[0].Trim() + ".dll";
			if (!File.Exists(file)) return null;
			Assembly asm = Assembly.LoadFrom(file);
			return asm;
		}


		/// <summary>
		/// Builds the az man operations.
		/// </summary>
		public static void BuildAzManOperations() {
			DirectoryInfo d = new DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory);
			DirectoryInfo binDirectory = new DirectoryInfo(BinPath);
			FileInfo[] svcFiles = d.GetFiles("*.svc", SearchOption.TopDirectoryOnly);
		
			foreach (FileInfo svcFile in svcFiles) {
				string serviceText = svcFile.OpenText().ReadToEnd();
				string serviceType = WCFHelper.GetSvcTypeFromContent(serviceText);
				Type t = Assembly.LoadFile(binDirectory.FullName + "\\" + serviceType + ".dll").GetType(serviceType);
				InspectService(t, svcFile.Name);

			}
		}

		
		/// <summary>
		/// Inspects the service.
		/// </summary>
		/// <param name="t">The t.</param>
		/// <param name="svcFileName">Name of the SVC file.</param>
		private static void InspectService(Type t, string svcFileName) {
			foreach (Type iType in t.GetInterfaces()) {
				object[] serviceAttributes = iType.GetCustomAttributes(typeof(ServiceContractAttribute), true);
				if (serviceAttributes.Length > 0) {
					string serviceNamespace = WCFHelper.GetClientTypeNamespace(((ServiceContractAttribute)serviceAttributes[0]).Namespace);
					foreach (MethodInfo mi in iType.GetMethods(BindingFlags.Instance | BindingFlags.Public)) {
						object[] operationattributes = mi.GetCustomAttributes(typeof(OperationContractAttribute), true);
						if (operationattributes.Length > 0) {
							try {
								InspectMethod(serviceNamespace, mi, t);
							}
							catch { }
						}
					}
				}
			}
		}


		/// <summary>
		/// Inspects the method.
		/// </summary>
		/// <param name="serviceNamespace">The service namespace.</param>
		/// <param name="mi">The mi.</param>
		/// <param name="t">The t.</param>
		private static void InspectMethod(string serviceNamespace, MethodInfo mi, Type t) {
			var methodXmlDoc = GetXmlDoc(mi.DeclaringType.Assembly.GetName().Name, mi.DeclaringType.FullName + "." + mi.Name);
			_mAzmanGenerator.AddOperation(t.FullName + "." + mi.Name, methodXmlDoc.Trim());
		}

		/// <summary>
		/// Gets the xml summary comment.
		/// </summary>
		/// <param name="fullType">the full type of the assembly</param>
		/// <param name="objectName">The object name to retreive to comment from.</param>
		/// <returns>The comment as a string</returns>
		private static string GetXmlDoc(string fullType, string objectName) {
			try {
				DirectoryInfo binDirectory = new DirectoryInfo(BinPath);
				XDocument loaded = XDocument.Load(binDirectory.FullName + "\\" + fullType + ".XML");
				var q = from c in loaded.Descendants("member")
						where c.Attribute("name").Value.Contains(objectName + "(") || c.Attribute("name").Value.EndsWith(objectName)
						select (string)c.Element("summary");
				var first = q.First();
				return first.Replace("\n", "");
			}
			catch {
				return "";
			}
		}

	}
}

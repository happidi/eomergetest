﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Web;
using System.Web.Security;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessLayer.Interfaces;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Configuration;
using Vizelia.FOL.Providers;
using Vizelia.FOL.WCFService.Contracts;

namespace Vizelia.FOL.WCFService {
	/// <summary>
	/// Implementation for Public Services.
	/// </summary>
	[ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple, InstanceContextMode = InstanceContextMode.PerCall, UseSynchronizationContext = false)]
	public class PublicWCF : BaseWCF, IPublicWCF {

		/// <summary>
		/// Gets the store of authentication providers declared in vizelia section of web.config.
		/// The returned store is used to build a domain combobox in login form.
		/// </summary>
		/// <returns></returns>
		public JsonStore<ListElement> AuthenticationProvider_GetStore() {
			var module = Helper.Resolve<IAuthenticationBusinessLayer>();
			var retVal = module.AuthenticationProvider_GetStore();
			return retVal;
		}

		/// <summary>
		/// Returns the stream of the image (with map) from a chart stored in the cache
		/// (used by printscreen.aspx because cuttycapt doesnt send cookies).
		/// </summary>
		/// <param name="applicationName">Name of the application.</param>
		/// <param name="guid">The guid of the image.</param>
		/// <returns></returns>
		public Stream Chart_GetStreamImageFromCache(string applicationName, string guid) {
			Stream retval = null;
			TenantHelper.RunInDifferentTenantContext(applicationName, () => {
				var energyBusinessLayer = Helper.Resolve<IEnergyBusinessLayer>();
				retval = energyBusinessLayer.Chart_GetStreamImageFromCache(guid);
				if (WebOperationContext.Current != null)
					WebOperationContext.Current.OutgoingResponse.ContentType = MimeType.Png;
			});
			return retval;
		}

		/// <summary>
		/// Returns the stream of the image (without map) from a chart.
		/// (used by printscreen.aspx because cuttycapt doesnt send cookies).
		/// </summary>
		/// <param name="applicationName">Name of the application.</param>
		/// <param name="Key">The key of the Chart.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <returns></returns>
		public Stream Chart_GetStreamImage(string applicationName, string Key, int width, int height) {
			Stream retval = null;
			TenantHelper.RunInDifferentTenantContext(applicationName, () => {
				var energyBusinessLayer = Helper.Resolve<IEnergyBusinessLayer>();
				var result = energyBusinessLayer.Chart_GetStreamImage(Key, width, height);
				if (WebOperationContext.Current != null)
					WebOperationContext.Current.OutgoingResponse.ContentType = result.MimeType;
				retval = result.ContentStream;
			}, "Admin");
			return retval;
		}

		/// <summary>
		/// Returns the stream of the image  from a dynamicdisplayimage.
		/// (used by printscreen.aspx because cuttycapt doesnt send cookies).
		/// </summary>
		/// <param name="applicationName">Name of the application.</param>
		/// <param name="Key">The key of the dynamicdisplayimage.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <returns></returns>
		public Stream DynamicDisplayImage_GetStreamImage(string applicationName, string Key, int width, int height) {
			Stream retval = null;
			TenantHelper.RunInDifferentTenantContext(applicationName, () => {
				var energyBusinessLayer = Helper.Resolve<IEnergyBusinessLayer>();
				var result = energyBusinessLayer.DynamicDisplayImage_GetStreamImage(Key, width, height);
				if (WebOperationContext.Current != null)
					WebOperationContext.Current.OutgoingResponse.ContentType = result.MimeType;
				retval = result.ContentStream;
			});
			return retval;
		}

		/// <summary>
		/// Returns the stream of the image from a playlist page (corresponding to the latest generation of the playlist).
		/// </summary>
		/// <param name="Key">The key of the playlist page.</param>
		/// <param name="applicationName">Name of the application.</param>
		/// <returns></returns>
		public Stream PlaylistPage_GetStreamImage(string Key, string applicationName) {
			Stream retval = null;
			TenantHelper.RunInDifferentTenantContext(applicationName, () => {
				var energyBusinessLayer = Helper.Resolve<IEnergyBusinessLayer>();
				var result = energyBusinessLayer.PlaylistPage_GetStreamImage(Key);
				if (WebOperationContext.Current != null)
					WebOperationContext.Current.OutgoingResponse.ContentType = result.MimeType;
				retval = result.ContentStream;
			});
			return retval;
		}

		/// <summary>
		/// Initializes the current tenant with data needed to begin working with it.
		/// </summary>
		/// <param name="operationId">The operation id.</param>
		/// <param name="adminPassword">The admin password.</param>
		/// <param name="tenantName">Name of the tenant.</param>
		public void InitializeTenant(Guid operationId, string adminPassword, string tenantName) {
			var applicationName = string.IsNullOrEmpty(tenantName) ? ContextHelper.ApplicationName : tenantName;
			TenantHelper.RunInDifferentTenantContext(applicationName, () => InitializeTenantInternal(operationId, adminPassword));
		}

		/// <summary>
		/// Initializes the tenant internal.
		/// </summary>
		/// <param name="operationId">The operation id.</param>
		/// <param name="adminPassword">The admin password.</param>
		private void InitializeTenantInternal(Guid operationId, string adminPassword) {
			using (
				TracingService.StartTracing("InitializeTenant", operationId.ToString())) {
				try {
					// This method may only run once: When the tenant is not initialized.
					TracingService.Write("Checking whether tenant is initialized or not...");
					var tenancyBusinessLayer = Helper.Resolve<ITenancyBusinessLayer>();

					Tenant tenant = tenancyBusinessLayer.Tenant_GetItem(ContextHelper.ApplicationName);

					if (tenant == null) {
						TracingService.Write("Tenant record not found.");
						// There are no tenants in the DB. Lets create this one automatically.
						tenant = tenancyBusinessLayer.Tenant_CreateAdminTenant();
					}
					if (tenant.IsInitialized) {
						TracingService.Write(TraceEntrySeverity.Error, "Tenant is already initialized, use ICoreWCF.ResetTenant instead.");
						return;
					}

					// This method is run via an unauthenticated service call, lets start a session that will
					// end when the request ends, so we can use DocumentHelper and other components that need the session.
					SessionService.StartSession(string.Empty);

					// Perform the initialization logic.
					TracingService.Write("Tenant is uninitialized, running ResetTenant method.");
					tenancyBusinessLayer.ResetTenant(operationId, adminPassword);
					string resetTenantCacheKey = string.Format("admin_pwd_{0}", operationId);
					string password = (string)CacheService.GetData(resetTenantCacheKey);
					CacheService.Remove(resetTenantCacheKey);

					TracingService.Write("ResetTenant complete.");
					// Now the tenant is initialized.

					TracingService.Write("Sending initial login details to tenant admin email...");
					SendCompletionEmail(password, tenant);

					tenant.IsInitialized = true;
					tenancyBusinessLayer.Tenant_FormUpdate(tenant, Guid.NewGuid());
					TracingService.Write("InitializeTenant complete. Tenant marked as initialized.");
				}
				catch (Exception exception) {
					TracingService.Write(exception);
					throw;
				}
			}
		}

		/// <summary>
		/// Sends the completion email.
		/// </summary>
		/// <param name="password">The password.</param>
		/// <param name="tenant">The tenant.</param>
		private static void SendCompletionEmail(string password, Tenant tenant) {
			var coreBusinessLayer = Helper.Resolve<ICoreBusinessLayer>();
			coreBusinessLayer.Tenant_SendCompletionEmail(password, tenant);
		}


		/// <summary>
		/// Determines whether the current tenant is initialized.
		/// </summary>
		/// <returns>
		///   <c>true</c> if the current tenant is initialized; otherwise, <c>false</c>.
		/// </returns>
		public bool IsTenantInitialized() {
			var tenancyBusinessLayer = Helper.Resolve<ITenancyBusinessLayer>();
			return tenancyBusinessLayer.IsTenantInitialized();
		}



		/// <summary>
		/// Checks user credentials and creates an authentication ticket (cookie) if the credentials are valid.
		/// Returns a detail message with the reason for not login the user.
		/// </summary>
		/// <param name="userName">The user name to be validated.</param>
		/// <param name="password">The password for the specified user.</param>
		/// <param name="createPersistentCookie">A value that indicates whether the authentication ticket remains valid across sessions.</param>
		/// <param name="provider">The provider used to check credentials</param>
		/// <returns>
		/// true if user credentials are valid; otherwise, false.
		/// </returns>
		public FormResponse Login_Response(string userName, string password, bool createPersistentCookie, string provider) {
			var module = Helper.Resolve<IAuthenticationBusinessLayer>();
			var retVal = module.LoginResponse(userName, password, createPersistentCookie, provider, null);
			return retVal;
		}

		/// <summary>
		/// Resets the password for a membership user that forgot the password.
		/// </summary>
		/// <param name="username">The username.</param>
		/// <param name="email">The email.</param>
		/// <returns></returns>
		public FormResponse UserForgotPassword(string username, string email) {
			var module = Helper.Resolve<IAuthenticationBusinessLayer>();
			var retVal = module.User_ForgotPassword(username, email);
			return retVal;
		}

		/// <summary>
		/// Starts an sso login process
		/// </summary>
		/// <param name="providerName">The key.</param>
		/// <returns></returns>
		public void SSOLogin(string providerName) {
			var module = Helper.Resolve<IAuthenticationBusinessLayer>();
			module.SSOLogin(providerName);
		}

		/// <summary>
		/// Clears the authentication ticket (cookie) in the browser.
		/// </summary>
		public void Logout() {
			var module = Helper.Resolve<IAuthenticationBusinessLayer>();
			module.Logout();
		}

		/// <summary>
		/// Resumes the pending workflows.
		/// </summary>
		public void ResumePendingWorkflows() {
			var module = Helper.Resolve<IServiceDeskBusinessLayer>();
			module.Workflow_ResumePending();
		}

		/// <summary>
		/// Changes the password for a membership user through a form with old password and new password.
		/// </summary>
		/// <param name="applicationName">Name of the application.</param>
		/// <param name="username">The user name.</param>
		/// <param name="oldPassword">The old password.</param>
		/// <param name="newPassword">The new password.</param>
		/// <returns></returns>
		public FormResponse User_ChangePasswordForm(string applicationName, string username, string oldPassword, string newPassword) {
			FormResponse retVal = null;
			TenantHelper.RunInDifferentTenantContext(applicationName, () => {
				var module = Helper.Resolve<IAuthenticationBusinessLayer>();
				retVal = module.User_ChangePasswordForm(username, oldPassword, newPassword);
			});
			return retVal;
		}

		/// <summary>
		/// Return a stream from a VirtualFile Path.
		/// </summary>
		/// <param name="applicationName">Name of the application.</param>
		/// <param name="value">The value.</param>
		/// <returns></returns>
		public Stream VirtualFile_GetStreamImage(string applicationName, string value) {
			Stream retval = null;
			TenantHelper.RunInDifferentTenantContext(applicationName, () => {
				var m_EnergyBusinessLayer = Helper.Resolve<IEnergyBusinessLayer>();
				StreamResult result = m_EnergyBusinessLayer.VirtualFile_GetStreamImage(value);
				if (WebOperationContext.Current != null)
					WebOperationContext.Current.OutgoingResponse.ContentType = result.MimeType;
				retval = result.ContentStream;
			}, "Admin");
			return retval;
		}


		/// <summary>
		/// Determines whether the current user is user authenticated.
		/// </summary>
		/// <returns>
		///   <c>true</c> if the current user is authenticated otherwise, <c>false</c>.
		/// </returns>
		public UserAuthenticationStatus IsUserAuthenticated() {
			try {

				string tenantName = ContextHelper.ApplicationName;

				var tenancyBusinessLayer = Helper.Resolve<ITenancyBusinessLayer>();
				List<Tenant> allTenants = tenancyBusinessLayer.Tenant_GetAll(new PagingParameter() { }, PagingLocation.Database);
				var tenants = allTenants.Where(tenant => tenant.Name == tenantName);
				string referer = HttpContext.Current.Request.Headers["Referer"].ToLower().TrimEnd('/');
				Uri refererUri = new Uri(referer);

				if ((HttpContext.Current.User.Identity.IsAuthenticated) && (tenants.Any(t => t.Url.ToLower().TrimEnd('/') == refererUri.Authority))) {
					HttpCookie authCookie = HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName];
					if (authCookie != null) {
						string encTicket = authCookie.Value;
						if (!String.IsNullOrEmpty(encTicket)) {
							// decrypt the ticket if possible. 
							FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(encTicket);

							if (!ticket.Expired) {
								var cookieData = new CookieData(ticket.UserData);
								if (SessionService.GetActiveSessions().Contains(cookieData.SessionId)) {
									return new UserAuthenticationStatus() { PageId = cookieData.PageId, IsAuthenticated = true };
								}
							}
						}

					}
					return new UserAuthenticationStatus() { PageId = null, IsAuthenticated = false }; ;
				}
				else {
					return new UserAuthenticationStatus() { PageId = null, IsAuthenticated = false }; ;
				}
			}
			catch {
				return new UserAuthenticationStatus() { PageId = null, IsAuthenticated = false }; ;
			}
		}


		/// <summary>
		/// Determines whether the client and the server have the same time.
		/// </summary>
		/// <param name="clientTime"></param>
		/// <returns>
		///   <c>true</c> if the client and the server have the same time. otherwise, <c>false</c>.
		/// </returns>
		public bool IsTimeSynchronized(DateTime clientTime) {
			var coreBusinessLayer = Helper.Resolve<ICoreBusinessLayer>();
			return coreBusinessLayer.IsTimeSynchronized(clientTime);
		}

		/// <summary>
		/// Gets the application settings.
		/// </summary>
		/// <returns></returns>
		public ApplicationSettings GetApplicationSettings() {
			var coreBusinessLayer = Helper.Resolve<ICoreBusinessLayer>();
			return coreBusinessLayer.GetApplicationSettings();
		}
	}
}

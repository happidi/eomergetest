﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Text;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Permissions;
using System.Threading;
using System.Web.Security;
using DDay.iCal;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessLayer.Broker;
using Vizelia.FOL.BusinessLayer.Interfaces;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Configuration;
using Vizelia.FOL.Common.Localization;
using Vizelia.FOL.DataLayer;
using Vizelia.FOL.DataLayer.Interfaces;
using Vizelia.FOL.Infrastructure;
using Vizelia.FOL.Providers;
using Vizelia.FOL.WF.Common;
using TaskScheduler = Vizelia.FOL.BusinessEntities.TaskScheduler;

namespace Vizelia.FOL.BusinessLayer {

	/// <summary>
	/// Business Layer implementation for Core.
	/// </summary>
	public class CoreBusinessLayer : ICoreBusinessLayer {

		private readonly ICoreDataAccess m_CoreDataAccess;
		private const string const_login_footer_resource_name = "msg_login_footer";

		/// <summary>
		/// Public ctor.
		/// Used in build to avoid coupling with data layer as we don't have configuration.
		/// </summary>
		/// <param name="includeDataLayer">False to by pass data layer creation , true otherwise.</param>
		public CoreBusinessLayer(bool includeDataLayer) {
			if (includeDataLayer)
				m_CoreDataAccess = Helper.CreateInstance<CoreDataAccess, ICoreDataAccess>();
		}

		/// <summary>
		/// Public ctor.
		/// </summary>
		public CoreBusinessLayer() {
			m_CoreDataAccess = Helper.CreateInstance<CoreDataAccess, ICoreDataAccess>();
		}

		/// <summary>
		/// Returns a list of AlamrDefinition for a specific location.
		/// </summary>
		/// <param name="KeyLocation">The key of the location.</param>
		/// <param name="maxCount">The max count.</param>
		/// <returns>
		/// A list of meter entities.
		/// </returns>
		private List<AlarmDefinition> AlarmDefinition_GetListByLocation(string KeyLocation, int maxCount = 0) {
			var broker = Helper.CreateInstance<AlarmDefinitionBroker>();
			var retVal = broker.GetListByKeyLocation(KeyLocation);
			if (maxCount > 0 && retVal.Count > maxCount)
				retVal = retVal.Take(maxCount).ToList();
			return retVal;
		}

		/// <summary>
		/// Gets the tree of assemblies.
		/// </summary>
		/// <returns></returns>
		public List<TreeNode> Assembly_GetTree() {
			List<TreeNode> retVal = new List<TreeNode>();
			Dictionary<string, TreeNode> map = new Dictionary<string, TreeNode>();

			var assembliesVizelia = Directory.GetFiles(@"d:\tfs\Vizelia\dev\BIN", "Vizelia*.dll");

			foreach (var assemblyFile in assembliesVizelia) {
				var assembly = Assembly.ReflectionOnlyLoadFrom(assemblyFile);

				var references = assembly.GetReferencedAssemblies().Where(p => p.FullName.Contains("Vizelia"));
				TreeNode nodeParent;
				if (map.ContainsKey(assembly.FullName)) {
					nodeParent = map[assembly.FullName];
					nodeParent.text = assembly.GetName().Name + " " + assembly.ImageRuntimeVersion;
					nodeParent.qtip = (Convert.ToInt32(nodeParent.qtip) + 1).ToString(CultureInfo.InvariantCulture);
				}
				else {
					nodeParent = new TreeNode {
						Key = assembly.FullName,
						qtip = "0",
						text = assembly.GetName().Name + " " + assembly.ImageRuntimeVersion,
						leaf = true
					};
					map.Add(assembly.FullName, nodeParent);
				}

				foreach (var subAssembly in references) {
					TreeNode node;
					if (map.ContainsKey(subAssembly.FullName))
						node = map[subAssembly.FullName];
					else {
						node = new TreeNode {
							Key = subAssembly.FullName,
							text = subAssembly.FullName,
							qtip = "1",
							leaf = true
						};
						map.Add(subAssembly.FullName, node);
					}

					nodeParent.leaf = false;
					node.qtip = (Convert.ToInt32(node.qtip) + 1).ToString(CultureInfo.InvariantCulture);
					nodeParent.AddChild(node);

				}
				retVal = map.Select(p => p.Value).Where(p => p.qtip == "0").OrderBy(p => p.text).ToList();

			}
			return retVal;
		}

		/// <summary>
		/// Begins the importing.
		/// </summary>
		/// <param name="operationId">The operation id.</param>
		/// <param name="documentName">Name of the document.</param>
		/// <param name="documentStream">The document stream.</param>
		/// <param name="importOptions">The import options.</param>
		public void BeginImporting(Guid operationId, string documentName, MemoryStream documentStream, AzmanImportOptions importOptions) {
			LongRunningOperationService.Invoke(
				operationId,
				() => {
					LongRunningOperationResult result = Import(documentStream, importOptions);
					return result;
				});
		}

		/// <summary>
		/// Deletes an existing business entity Building.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool Building_Delete(Building item) {
			BuildingBroker broker = Helper.CreateInstance<BuildingBroker>();
			return broker.Delete(item);
		}

		/// <summary>
		/// Creates a new business entity Building and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse Building_FormCreate(Building item) {
			BuildingBroker broker = Helper.CreateInstance<BuildingBroker>();
			FormResponse response = broker.FormCreate(item);
			return response;
		}

		/// <summary>
		/// Loads a specific item for the business entity Building.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse Building_FormLoad(string Key) {
			BuildingBroker broker = Helper.CreateInstance<BuildingBroker>();
			return broker.FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity Building and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse Building_FormUpdate(Building item) {
			BuildingBroker broker = Helper.CreateInstance<BuildingBroker>();
			FormResponse response = broker.FormUpdate(item);
			return response;
		}

		/// <summary>
		/// Returns children of a specific building.
		/// </summary>
		/// <param name="KeyBuilding">The key of the parent building.</param>
		/// <returns>A list of builidng entities.</returns>
		private List<Building> Building_GetChildren(string KeyBuilding) {
			BuildingBroker broker = Helper.CreateInstance<BuildingBroker>();
			return broker.GetChildren(KeyBuilding);
		}

		/// <summary>
		/// Gets a specific item for the business entity Building.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		private Building Building_GetItem(string Key, params string[] fields) {
			BuildingBroker broker = Helper.CreateInstance<BuildingBroker>();
			return broker.GetItem(Key, fields);
		}

		/// <summary>
		/// Gets a json store for the business entity Building.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<Building> Building_GetStore(PagingParameter paging, PagingLocation location) {
			BuildingBroker broker = Helper.CreateInstance<BuildingBroker>();
			return broker.GetStore(paging, location);
		}

		/// <summary>
		/// Saves a crud store for the business entity Building.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<Building> Building_SaveStore(CrudStore<Building> store) {
			BuildingBroker broker = Helper.CreateInstance<BuildingBroker>();
			return broker.SaveStore(store);
		}

		/// <summary>
		/// Deletes an existing business entity BuildingStorey.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool BuildingStorey_Delete(BuildingStorey item) {
			BuildingStoreyBroker broker = Helper.CreateInstance<BuildingStoreyBroker>();
			return broker.Delete(item);
		}

		/// <summary>
		/// Creates a new business entity BuildingStorey and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse BuildingStorey_FormCreate(BuildingStorey item) {
			BuildingStoreyBroker broker = Helper.CreateInstance<BuildingStoreyBroker>();
			FormResponse response = broker.FormCreate(item);
			return response;
		}

		/// <summary>
		/// Loads a specific item for the business entity BuildingStorey.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse BuildingStorey_FormLoad(string Key) {
			BuildingStoreyBroker broker = Helper.CreateInstance<BuildingStoreyBroker>();
			return broker.FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity BuildingStorey and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse BuildingStorey_FormUpdate(BuildingStorey item) {
			BuildingStoreyBroker broker = Helper.CreateInstance<BuildingStoreyBroker>();
			return broker.FormUpdate(item);
		}

		/// <summary>
		/// Returns children of a specific building.
		/// </summary>
		/// <param name="KeyBuilding">The key of the parent building.</param>
		/// <returns>A list of building entities.</returns>
		private List<BuildingStorey> BuildingStorey_GetChildren(string KeyBuilding) {
			BuildingStoreyBroker broker = Helper.CreateInstance<BuildingStoreyBroker>();
			return broker.GetChildren(KeyBuilding);
		}

		/// <summary>
		/// Gets a json store for the business entity BuildingStorey.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<BuildingStorey> BuildingStorey_GetStore(PagingParameter paging, PagingLocation location) {
			BuildingStoreyBroker broker = Helper.CreateInstance<BuildingStoreyBroker>();
			return broker.GetStore(paging, location);
		}

		/// <summary>
		/// Saves a crud store for the business entity BuildingStorey.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<BuildingStorey> BuildingStorey_SaveStore(CrudStore<BuildingStorey> store) {
			BuildingStoreyBroker broker = Helper.CreateInstance<BuildingStoreyBroker>();
			return broker.SaveStore(store);
		}

		/// <summary>
		/// Gets the store of calendar day names.
		/// </summary>
		/// <returns></returns>
		public JsonStore<ListElement> CalendarDayName_GetStore() {
			List<ListElement> before = new List<ListElement>();
			List<ListElement> after = new List<ListElement>();

			var values = Enum.GetValues(typeof(DayOfWeek));

			int firstDayOfWeekIndex = (int)CultureInfo.CurrentCulture.DateTimeFormat.FirstDayOfWeek;

			foreach (var value in values) {
				string dayName = CultureInfo.CurrentCulture.DateTimeFormat.DayNames[(int)value];

				ListElement el = new ListElement {
					Id = ((int)value).ToString(CultureInfo.InvariantCulture),
					Value = ((int)value).ToString(CultureInfo.InvariantCulture),
					MsgCode = dayName
				};

				if ((int)value < firstDayOfWeekIndex)
					after.Add(el);
				else
					before.Add(el);
			}
			before.AddRange(after);
			return before.ToJsonStore();
		}

		/// <summary>
		/// Copy existing CalendarEvent.
		/// </summary>
		/// <param name="keys">The list of key and keyLocation of item to copy.</param>
		/// <returns></returns>
		public FormResponse CalendarEvent_Copy(string[][] keys) {
			var response = new FormResponse { success = true };
			var broker = Helper.CreateInstance<CalendarEventBroker>();
			try {
				foreach (string[] key in keys) {
					var Key = key[0];
					var KeyLocation = key[1];

					var form = CalendarEvent_FormLoad(Key, KeyLocation);
					if (form != null && form.data != null) {
						var calEvent = form.data as CalendarEvent;
						if (calEvent == null)
							return response;
						if (calEvent.IsAllDay)
							calEvent.EndDate = ((DateTime)calEvent.EndDate).AddDays(-1);
						else {
							calEvent.StartDate = TimeZoneInfo.ConvertTimeFromUtc((DateTime)calEvent.StartDate, calEvent.TimeZone);
							calEvent.EndDate = TimeZoneInfo.ConvertTimeFromUtc((DateTime)calEvent.EndDate, calEvent.TimeZone);
						}
						broker.Copy(calEvent, out response);
					}
				}
			}
			catch (Exception ex) {
				response.success = false;
				response.msg = ex.Message;
			}
			return response;
		}

		/// <summary>
		/// Deletes an existing business entity CalendarEvent.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <param name="KeyLocation">The location key.</param>
		/// <returns></returns>
		public bool CalendarEvent_Delete(CalendarEvent item, string KeyLocation) {
			CalendarEventBroker broker = Helper.CreateInstance<CalendarEventBroker>();
			return broker.Delete(item, KeyLocation);
		}

		/// <summary>
		/// Create a business entity CalendarEvent.
		/// </summary>
		/// <param name="item">The entity CalendarEvent.</param>
		/// <param name="KeyLocation">The location Key.</param>
		/// <returns></returns>
		public FormResponse CalendarEvent_FormCreate(CalendarEvent item, string KeyLocation) {
			CalendarEventBroker broker = Helper.CreateInstance<CalendarEventBroker>();
			return broker.FormCreate(item, KeyLocation);
		}

		/// <summary>
		/// Loads a specific item for the business entity CalendarEvent.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="KeyLocation">The location Key</param>
		/// <returns></returns>
		public FormResponse CalendarEvent_FormLoad(string Key, string KeyLocation) {
			CalendarEventBroker broker = Helper.CreateInstance<CalendarEventBroker>();
			return broker.FormLoad(Key, KeyLocation);
		}

		/// <summary>
		/// Create a business entity CalendarEvent.
		/// </summary>
		/// <param name="item">The entity CalendarEvent.</param>
		/// <param name="KeyLocation">The location Key</param>
		/// <returns></returns>
		public FormResponse CalendarEvent_FormUpdate(CalendarEvent item, string KeyLocation) {
			CalendarEventBroker broker = Helper.CreateInstance<CalendarEventBroker>();
			return broker.FormUpdate(item, KeyLocation);
		}

		/// <summary>
		/// Gets a dictionnary of all the calendar data as an iCal string in a dictionnary group by key object.
		/// </summary>
		/// <returns></returns>
		public Dictionary<string, Tuple<string, string>> CalendarEvent_GetAll() {
			var retVal = m_CoreDataAccess.Calendar_GetAll();
			return retVal;
		}

		/// <summary>
		/// Gets the CalendarEvent store for a location.
		/// </summary>
		/// <param name="KeyLocation">The key of the location.</param>
		/// <param name="category">The category.</param>
		/// <param name="paging">The paging parameter.</param>
		/// <returns>A json store of the result.</returns>
		public JsonStore<CalendarEvent> CalendarEvent_GetStoreFromLocation(string KeyLocation, string category, PagingParameter paging) {
			CalendarEventCategoryBroker brokerCategory = Helper.CreateInstance<CalendarEventCategoryBroker>();
			List<CalendarEventCategory> categories = brokerCategory.GetAll();

			string itemZoneId = m_CoreDataAccess.Calendar_GetTimeZoneId(KeyLocation);
			string iCalData = m_CoreDataAccess.Calendar_GetFromObject(KeyLocation);
			if (iCalData == null)
				return new JsonStore<CalendarEvent>();

			iCalendar iCal = CalendarHelper.Deserialize(iCalData);
			CalendarHelper.AddTimeZone(itemZoneId, iCal);
			var retVal = FilterListEventByCategory(iCal.Events.ToBusinessEntity(KeyLocation), category);

			foreach (CalendarEvent calEvent in retVal) {
				//calEvent.TimeZoneId = itemZoneId;		
				calEvent.CategoryEntity = (from c in categories where c.LocalId == calEvent.Category select c).FirstOrDefault();
			}

			return retVal.ToJsonStoreWithFilter(paging);
		}

		/// <summary>
		/// Gets the CalendarEvent store of occurrences for a location.
		/// </summary>
		/// <param name="KeyLocation">The key location.</param>
		/// <param name="paging">The paging.</param>
		/// <param name="start">The start.</param>
		/// <param name="end">The end.</param>
		/// <param name="category">The category.</param>
		/// <returns></returns>
		public JsonStore<CalendarEvent> CalendarEvent_GetStoreOccurence(string KeyLocation, PagingParameter paging, DateTime start, DateTime end, string category) {
			CalendarEventCategoryBroker brokerCategory = Helper.CreateInstance<CalendarEventCategoryBroker>();
			List<CalendarEventCategory> categories = brokerCategory.GetAll();

			string iCalData = m_CoreDataAccess.Calendar_GetFromObject(KeyLocation);
			var timeZoneId = m_CoreDataAccess.Calendar_GetTimeZoneId(KeyLocation);
			timeZoneId = !String.IsNullOrWhiteSpace(timeZoneId) ? timeZoneId : "UTC";
			if (iCalData == null || (!string.IsNullOrEmpty(category) && iCalData.Contains(category) == false))
				return new JsonStore<CalendarEvent>();

			iCalendar iCal = CalendarHelper.Deserialize(iCalData);
			CalendarHelper.AddTimeZone(timeZoneId, iCal);
			// Expand date start and end to catch maximum occurrences
			DateTime startDate = new DateTime(start.Year, start.Month, start.Day).AddDays(-1);
			DateTime endDate = new DateTime(end.Year, end.Month, end.Day).AddDays(1);

			IList<Occurrence> occurrences = iCal.GetOccurrences<Event>(startDate, endDate);
			var queryEvents = new List<CalendarEvent>();
			// Iterate through each occurrence
			foreach (Occurrence o in occurrences) {
				// Cast the component to an event
				Event evt = o.Source as Event;
				if (evt != null) {
					// Make sure the event is active (hasn't been cancelled)
					if (evt.IsActive() && (string.IsNullOrEmpty(category) || evt.Category() == category)) {
						var calEvent = evt.ToBusinessEntity((Period)o.Period);
						//Update the ColorId of the event base an it s category
						//calEvent.TimeZoneId = timeZoneId;

						if (!string.IsNullOrEmpty(evt.Category())) {
							calEvent.ColorId = (from c in categories where c.LocalId == evt.Category() select c.ColorId).FirstOrDefault();
						}
						queryEvents.Add(calEvent);
					}
				}
			}
			return queryEvents.ToJsonStoreWithFilter(paging);
		}




		/// <summary>
		/// Saves a crud store for the business entity CalendarEvent.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <param name="KeyLocation">The loation Key.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<CalendarEvent> CalendarEvent_SaveStore(CrudStore<CalendarEvent> store, string KeyLocation) {
			CalendarEventBroker broker = Helper.CreateInstance<CalendarEventBroker>();
			return broker.SaveStore(store, KeyLocation);
		}

		/// <summary>
		/// Deletes an existing business entity CalendarEventCategory.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool CalendarEventCategory_Delete(CalendarEventCategory item) {
			CalendarEventCategoryBroker broker = Helper.CreateInstance<CalendarEventCategoryBroker>();
			return broker.Delete(item);
		}

		/// <summary>
		/// Creates a new business entity CalendarEventCategory and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse CalendarEventCategory_FormCreate(CalendarEventCategory item) {
			CalendarEventCategoryBroker broker = Helper.CreateInstance<CalendarEventCategoryBroker>();
			return broker.FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity CalendarEventCategory.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse CalendarEventCategory_FormLoad(string Key) {
			CalendarEventCategoryBroker broker = Helper.CreateInstance<CalendarEventCategoryBroker>();
			return broker.FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity CalendarEventCategory and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse CalendarEventCategory_FormUpdate(CalendarEventCategory item) {
			CalendarEventCategoryBroker broker = Helper.CreateInstance<CalendarEventCategoryBroker>();
			return broker.FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse CalendarEventCategory_FormUpdateBatch(string[] keys, CalendarEventCategory item) {
			CalendarEventCategoryBroker broker = Helper.CreateInstance<CalendarEventCategoryBroker>();
			return broker.FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity CalendarEventCategory.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public List<CalendarEventCategory> CalendarEventCategory_GetAll(PagingParameter paging, PagingLocation location, params string[] fields) {
			int total;
			CalendarEventCategoryBroker broker = Helper.CreateInstance<CalendarEventCategoryBroker>();
			return broker.GetAll(paging, location, out total, fields: fields);
		}

		/// <summary>
		/// Gets a specific item for the business entity CalendarEventCategory.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public CalendarEventCategory CalendarEventCategory_GetItem(string Key, params string[] fields) {
			CalendarEventCategoryBroker broker = Helper.CreateInstance<CalendarEventCategoryBroker>();
			return broker.GetItem(Key, fields);
		}

		/// <summary>
		/// Gets a json store for the business entity CalendarEventCategory.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<CalendarEventCategory> CalendarEventCategory_GetStore(PagingParameter paging, PagingLocation location) {
			CalendarEventCategoryBroker broker = Helper.CreateInstance<CalendarEventCategoryBroker>();
			return broker.GetStore(paging, location);
		}

		/// <summary>
		/// Saves a crud store for the business entity CalendarEventCategory.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<CalendarEventCategory> CalendarEventCategory_SaveStore(CrudStore<CalendarEventCategory> store) {
			CalendarEventCategoryBroker broker = Helper.CreateInstance<CalendarEventCategoryBroker>();
			return broker.SaveStore(store);
		}

		/// <summary>
		/// Gets the store of calendar frequency occurrences.
		/// </summary>
		/// <returns></returns>
		public JsonStore<ListElement> CalendarFrequencyOccurrence_GetStore() {
			var values = Enum.GetValues(typeof(CalendarFrequencyOccurrence));
			List<ListElement> result = (from object value in values
										select new ListElement {
											Id = ((int)value).ToString(CultureInfo.InvariantCulture),
											Value = ((int)value).ToString(CultureInfo.InvariantCulture),
											MsgCode = Langue.ResourceManager.GetString("msg_calendar_frequencyoccurrence_" + value.ToString().ToLower())
										}).ToList();
			var retVal = result.ToJsonStore();
			return retVal;
		}

		/// <summary>
		/// Gets the store of calendar month names.
		/// </summary>
		/// <returns></returns>
		public JsonStore<ListElement> CalendarMonthName_GetStore() {
			List<ListElement> result = new List<ListElement>();
			for (int i = 0; i <= 11; i++) {
				ListElement el = new ListElement {
					Id = (i + 1).ToString(CultureInfo.InvariantCulture),
					Value = (i + 1).ToString(CultureInfo.InvariantCulture),
					MsgCode = CultureInfo.CurrentCulture.DateTimeFormat.MonthNames[i]
				};
				result.Add(el);
			}
			return result.ToJsonStore();
		}

		/// <summary>
		/// Iterates through the classes and gets all the matching methods
		/// </summary>
		/// <param name="typeName">Name of the type.</param>
		/// <returns></returns>
		private List<ClassDefinition> ClassDefinition_GetListByTypeName(string typeName) {

			var foundClasses = ClassFinder.GetClassFromPathThatInheritFromType(typeName);

			return foundClasses.Select(foundClass => new ClassDefinition {
				IconCls = foundClass.GetAttributeValue<IconClsAttribute>("PropertyNameId"),
				MsgCode = foundClass.GetAttributeValue<StepInformationAttribute>("MsgCode") ?? foundClass.FullName,
				ClassName = foundClass.FullName,
				AssemblyQualifiedName = foundClass.AssemblyQualifiedName,
				MsgCodeDescription = foundClass.GetAttributeValue<StepInformationAttribute>("MsgCodeDescription") ?? foundClass.AssemblyQualifiedName,
			}).ToList();
		}

		/// <summary>
		/// Gets all the classes that inherit from the type given.
		/// </summary>
		/// <param name="typeName">Name of the type.</param>
		/// <returns></returns>
		public JsonStore<ClassDefinition> ClassDefinition_GetStoreByTypeName(string typeName) {
			var retVal = ClassDefinition_GetListByTypeName(typeName);
			return retVal.ToJsonStore();
		}

		/// <summary>
		/// Deletes an existing business entity ClassificationDefinition.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool ClassificationDefinition_Delete(ClassificationDefinition item) {
			ClassificationDefinitionBroker broker = Helper.CreateInstance<ClassificationDefinitionBroker>();
			return broker.Delete(item);
		}

		/// <summary>
		/// Creates a new business entity ClassificationDefinition and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse ClassificationDefinition_FormCreate(ClassificationDefinition item) {
			ClassificationDefinitionBroker broker = Helper.CreateInstance<ClassificationDefinitionBroker>();
			return broker.FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity ClassificationDefinition.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse ClassificationDefinition_FormLoad(string Key) {
			ClassificationDefinitionBroker broker = Helper.CreateInstance<ClassificationDefinitionBroker>();
			return broker.FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity ClassificationDefinition and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse ClassificationDefinition_FormUpdate(ClassificationDefinition item) {
			ClassificationDefinitionBroker broker = Helper.CreateInstance<ClassificationDefinitionBroker>();
			return broker.FormUpdate(item);
		}

		/// <summary>
		/// Gets a list for the business entity ClassificationDefinition. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public List<ClassificationDefinition> ClassificationDefinition_GetAll(PagingParameter paging, PagingLocation location, params string[] fields) {
			int total;
			ClassificationDefinitionBroker broker = Helper.CreateInstance<ClassificationDefinitionBroker>();
			return broker.GetAll(paging, location, out total, fields: fields); ;
		}

		/// <summary>
		/// Gets a specific item for the business entity ClassificationDefinition.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public ClassificationDefinition ClassificationDefinition_GetItem(string Key, params string[] fields) {
			ClassificationDefinitionBroker broker = Helper.CreateInstance<ClassificationDefinitionBroker>();
			return broker.GetItem(Key, fields);
		}

		/// <summary>
		/// Gets a json store for the business entity ClassificationDefinition.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<ClassificationDefinition> ClassificationDefinition_GetStore(PagingParameter paging,
																					 PagingLocation location) {
			ClassificationDefinitionBroker broker = Helper.CreateInstance<ClassificationDefinitionBroker>();
			return broker.GetStore(paging, location);
		}

		/// <summary>
		/// Saves a crud store for the business entity ClassificationDefinition.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<ClassificationDefinition> ClassificationDefinition_SaveStore(
			CrudStore<ClassificationDefinition> store) {
			ClassificationDefinitionBroker broker = Helper.CreateInstance<ClassificationDefinitionBroker>();
			return broker.SaveStore(store);
		}

		/// <summary>
		/// Deletes an existing business entity ClassificationItem.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool ClassificationItem_Delete(ClassificationItem item) {
			ClassificationItemBroker broker = Helper.CreateInstance<ClassificationItemBroker>();
			return broker.Delete(item);
		}

		/// <summary>
		/// Creates a new business entity ClassificationItem and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse ClassificationItem_FormCreate(ClassificationItem item) {
			ClassificationItemBroker broker = Helper.CreateInstance<ClassificationItemBroker>();
			return broker.FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity ClassificationItem.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse ClassificationItem_FormLoad(string Key) {
			ClassificationItemBroker broker = Helper.CreateInstance<ClassificationItemBroker>();
			return broker.FormLoad(Key);
		}

		/// <summary>
		/// Classifications the item_ get all.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="location">The location.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public List<ClassificationItem> ClassificationItem_GetAll(PagingParameter paging, PagingLocation location, params string[] fields) {
			int total;
			ClassificationItemBroker broker = Helper.CreateInstance<ClassificationItemBroker>();
			return broker.GetAll(paging, location, out total, isHierarchyEnabled: false, fields: fields);
		}

		/// <summary>
		/// Gets a ClassificationItem.
		/// </summary>
		/// <param name="Key">The key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public ClassificationItem ClassificationItem_GetItem(string Key, params string[] fields) {
			var broker = Helper.CreateInstance<ClassificationItemBroker>();
			return broker.GetItem(Key, fields);
		}

		/// <summary>
		/// Gets a json store for the business entity ClassificationItem.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<ClassificationItem> ClassificationItem_GetStore(PagingParameter paging, PagingLocation location) {
			var broker = Helper.CreateInstance<ClassificationItemBroker>();
			var flgRelation = false;
			if (!String.IsNullOrEmpty(paging.query)) {
				flgRelation = paging.query.IndexOf("&", StringComparison.Ordinal) >= 0;
				paging.query = paging.query.Replace("&", "");
			}
			var retVal = broker.GetStore(paging, location);
			if (flgRelation) {
				var keys = retVal.records.Select(p => p.KeyClassificationItem).Distinct().ToList();
				paging.AddInnerJoinFilter("KeyChildren", keys, "innerjoin");
				retVal = broker.GetListRelationship(paging);
				retVal.records.ForEach(p => p.KeyClassificationItem = p.KeyClassificationItem + p.KeyParent);
				return retVal;
			}
			return retVal;
		}

		/// <summary>
		/// Gets a json store for the business entity ClassificationItem.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location.</param>
		/// <returns>
		/// A json store.
		/// </returns>
		public JsonStore<ClassificationItem> ClassificationItem_GetStoreByKeyword(PagingParameter paging, PagingLocation location) {
			ClassificationItemBroker broker = Helper.CreateInstance<ClassificationItemBroker>();
			return broker.GetAllClassificationItem(paging);
		}


		/// <summary>
		/// Updates an existing business entity ClassificationItem and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse ClassificationItem_FormUpdate(ClassificationItem item) {
			ClassificationItemBroker broker = Helper.CreateInstance<ClassificationItemBroker>();
			return broker.FormUpdate(item);
		}

		/// <summary>
		/// Gets a dictionnary for the business entity ClassificationItem by key word.
		/// </summary>
		/// <param name="KeyClassificationItemPath">The key classification item path.</param>
		/// <returns></returns>
		public Dictionary<string, ClassificationItem> ClassificationItem_GetDictionary(string KeyClassificationItemPath) {
			ClassificationItemBroker broker = Helper.CreateInstance<ClassificationItemBroker>();
			var retVal = broker.GetDictionary(KeyClassificationItemPath);
			return retVal;
		}

		/// <summary>
		/// Gets a json store of ClassificationItem based on related object Key.
		/// </summary>
		/// <param name="KeyObject">The key object.</param>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>
		/// A json store.
		/// </returns>
		public JsonStore<ClassificationItem> ClassificationItem_GetStoreByKeyObject(string KeyObject, PagingParameter paging, PagingLocation location) {
			ClassificationItemBroker brokerClassificationItem = Helper.CreateInstance<ClassificationItemBroker>();
			var reVal = brokerClassificationItem.GetStoreByKeyObject(paging, KeyObject);
			return reVal;
		}

		/// <summary>
		/// Generates the json structure of the ClassificationItem treeview.
		/// </summary>
		/// <param name="Key">The Key of the parent node.</param>
		/// <param name="entity">The parent entity.</param>
		/// <param name="excludePset">True to exclude pset nodes.</param>
		/// <param name="excludeRelationship">True to exclude non direct children nodes.</param>
		/// <param name="flgFilter">if set to <c>true</c> Flag filter.</param>
		/// <returns>
		/// A list of treenodes in json.
		/// </returns>
		public List<TreeNode> ClassificationItem_GetTree(string Key, ClassificationItem entity, bool excludePset, bool excludeRelationship, bool flgFilter) {
			ClassificationItemBroker broker = Helper.CreateInstance<ClassificationItemBroker>();
			List<TreeNode> result = new List<TreeNode>();
			if (Key.Trim() == "")
				Key = null;
			else if (!string.IsNullOrWhiteSpace(Key) && (Key.IndexOf("#", StringComparison.Ordinal) < 0)) {
				if (entity == null)
					entity = new ClassificationItem { Code = "", Level = 0 };
				return ClassificationItem_GetTree(Key, entity.Code, entity.Level, flgFilter);
			}

			// adding the priority
			if (excludeRelationship != true) {
				Priority priorityNode;

				if (entity != null && entity.Relation != null) {
					priorityNode = Priority_GetItemByClassification(entity.KeyClassificationItem, entity.Relation.KeyParent);
				}
				else {
					priorityNode = Priority_GetItemByClassification(Key, null);
				}

				if (priorityNode != null)
					result.Add(priorityNode.GetTree());

			}

			// adding the workflows attached
			if (excludeRelationship != true) {
				WorkflowAssemblyDefinition workflowNode;
				if (entity != null && entity.Relation != null) {
					workflowNode = broker.GetWorkflowAssemblyDefinitionByClassification(entity.KeyClassificationItem,
																						entity.Relation.KeyParent);
				}
				else {
					workflowNode = broker.GetWorkflowAssemblyDefinitionByClassification(Key, null);
				}


				if (workflowNode != null) {
					var node = workflowNode.GetTree();
					var eventList = WorkflowHelper.GetStateMachineEventsTransitions(workflowNode.AssemblyQualifiedName);
					node.leaf = false;
					node.children = (from e in eventList
									 select e.GetTree()).OrderBy(p => p.text).ToList();
					result.Add(node);
				}
			}

			// adding the psets
			if (!excludePset) {
				ObjectClassificationBroker brokerObjectClassification = Helper.CreateInstance<ObjectClassificationBroker>();
				TreeNode psetNode = brokerObjectClassification.GetTreePset(Key);
				if (psetNode != null)
					result.Add(psetNode);
			}

			List<ClassificationItem> children;
			List<ClassificationItem> distinctChildren;
			// adding children and possible relationships 
			if (!excludeRelationship && entity != null && entity.Relation != null) {
				children = broker.GetChildren(Key, entity.Relation.RelationPath);
				distinctChildren =
					children.Distinct(new BaseBusinessEntityComparer<ClassificationItem>()).ToList();
			}
			//else if (!excludeRelationship && entity != null && entity.Relation == null)
			//	result.AddRange(broker.GetChildren(Key, "").Distinct(new BaseBusinessEntityComparer<ClassificationItem>()).ToList().GetTree());
			else {
				children = broker.GetChildren(Key);
				distinctChildren = children.Distinct(new BaseBusinessEntityComparer<ClassificationItem>()).ToList();
			}

			List<TreeNode> treeNodes = distinctChildren.GetTree();
			result.AddRange(treeNodes);



			List<Filter> filters = FilterHelper.GetFilter<ClassificationItem>();
			// If we would like to return an empty result if no filter spatial was associated with the user, we should not check Count>0
			if (flgFilter && filters != null && filters.Count > 0) {
				var query = (from n in distinctChildren
							 join p in filters.Select(filter => filter.Key)
								 on
								 n.KeyClassificationItem equals p
							 where n.Relation == null
							 select n).
								Union(
								from n in distinctChildren
								where n.Relation != null
								select n
								);
				distinctChildren = query.ToList();
				if (distinctChildren.Count > 0)
					return distinctChildren.GetTree();
			}

			return result;
		}


		/// <summary>
		/// Generates the json structure of the ClassificationItem treeview by key word and tree level.
		/// </summary>
		/// <param name="query">The query.</param>
		/// <param name="familly">The familly code of notion.</param>
		/// <param name="level">The level.</param>
		/// <param name="flgFilter">if set to <c>true</c> [FLG filter].</param>
		/// <returns>
		/// a list of treenodes in json.
		/// </returns>
		public List<TreeNode> ClassificationItem_GetTree(string query, string familly, int level, bool flgFilter) {
			ClassificationItemBroker broker = Helper.CreateInstance<ClassificationItemBroker>();
			var filtersCode = new List<GridFilter> { new GridFilter { field = "Code", data = new GridData { type = "string", comparison = "end", value = familly } } };
			var jsonClassif = broker.GetStore(new PagingParameter { start = 0, limit = 0, query = query, filter = "Title LocalId Code", filters = filtersCode }, PagingLocation.Database, false);

			List<Filter> classificationDD = m_CoreDataAccess.GetClassificationItemHierarchy((from n in jsonClassif.records where n.Code.IndexOf(familly, StringComparison.Ordinal) == 0 select n.KeyClassificationItem).Distinct().ToArray(), HierarchyDirection.ASC);
			var classificationItemHierarchy = broker.GetList(classificationDD.Select(s => s.Key).ToArray()).Where(p => p.KeyParent != null).ToList();

			// If we would like to return an empty result if no filter classification items was associated with the user, we should not check Count > 0
			List<TreeNode> nodes = classificationItemHierarchy.GetTree();
			List<Filter> filters = FilterHelper.GetFilter<ClassificationItem>();
			if (flgFilter && filters != null && filters.Count > 0) {
				var filteredItemHierarchy = (from n in classificationItemHierarchy
											 join p in filters on n.KeyClassificationItem equals p.Key
											 select n).ToList();
				if (filteredItemHierarchy.Count > 0) {
					classificationItemHierarchy = filteredItemHierarchy;
					var nodeQuery = from n in filteredItemHierarchy.GetTree()
									join p in filters on n.Key equals p.Key
									select n.SetFilterType(p.Type);
					nodes = nodeQuery.ToList();
				}
				else {
					nodes = classificationItemHierarchy.GetTree();
				}
			}


			var keyQuery = classificationItemHierarchy.Where(p => p.KeyParent != null).Select(p => p.KeyClassificationItem);
			var keyParentQuery = classificationItemHierarchy.Where(p => p.KeyParent != null).Select(p => p.KeyParent).Distinct();
			var extractQuery = keyParentQuery.Except(keyQuery);
			var finalQuery = from p in classificationItemHierarchy
							 where extractQuery.Contains(p.KeyParent)
							 select p;

			if (finalQuery.Any()) {
				var roots = finalQuery.ToList().GetTree();
				var nodeResults = new List<TreeNode>();
				PopulateTree<ClassificationItem>(roots, nodes, level, nodeResults);
				return nodeResults;
			}
			return nodes;
		}



		/// <summary>
		/// Generates the json structure of the ClassificationItem treeview from a related object.
		/// </summary>
		/// <param name="Key">The Key of the parent node.</param>
		/// <param name="entity">The parent entity.</param>
		/// <param name="KeyRelatedObject">The key object.</param>
		/// <returns>
		/// A list of treenodes in json.
		/// </returns>
		public List<TreeNode> ClassificationItem_GetTreeByRelatedObject(string Key, ClassificationItem entity, string KeyRelatedObject) {

			var nodes = ClassificationItem_GetTree(Key, entity, true, true, false);
			var rapidnodes = new List<TreeNode>();
			if (!string.IsNullOrEmpty(Key)) {
				if (!string.IsNullOrWhiteSpace(Key) && (Key.IndexOf("#", StringComparison.Ordinal) < 0)) {
					rapidnodes = ClassificationItem_GetTree(Key, entity.Code, entity.Level, false);
				}

				var paging = new PagingParameter();
				var filterByLocation = ClassificationItem_GetStoreByKeyObject(KeyRelatedObject, paging, PagingLocation.Database).records.Select(p => p.KeyClassificationItem);
				List<Filter> classificationItemHierarchy = m_CoreDataAccess.GetClassificationItemHierarchy(filterByLocation.ToArray());
				if ((rapidnodes.Count > 0) && (classificationItemHierarchy.Count > 0)) {
					var query = from n in rapidnodes
								let classificationItem = n.entity as ClassificationItem
								join p in classificationItemHierarchy.Select(c => c.Key)
								on
								n.Key equals p
								where classificationItem != null && classificationItem.Relation == null
								select n;
					return query.ToList();
				}
				if ((rapidnodes.Count > 0) && (classificationItemHierarchy.Count <= 0)) {
					return rapidnodes;
				}
				if (classificationItemHierarchy.Count > 0) {
					var query = (from n in nodes
								 let classificationItem = n.entity as ClassificationItem
								 join p in classificationItemHierarchy.Select(c => c.Key)
									on
									n.Key equals p
								 where classificationItem != null && classificationItem.Relation == null
								 select n).
						Union(
							from n in nodes
							let item = n.entity as ClassificationItem
							where item != null && item.Relation != null
							select n
						);

					var filteredNodes = query.ToList();
					if (filteredNodes.Count > 0)
						return filteredNodes;
				}
			}
			return nodes;
		}


		/// <summary>
		/// Gets a json store for the business entity Link.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<Link> Link_GetStore(PagingParameter paging, PagingLocation location) {
			LinkBroker broker = Helper.CreateInstance<LinkBroker>();
			return broker.GetStore(paging, location);
		}

		/// <summary>
		/// Gets a json store for the business entity Link that are roots.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<Link> Link_GetStoreRootOnly(PagingParameter paging, PagingLocation location) {
			LinkBroker broker = Helper.CreateInstance<LinkBroker>();
			return broker.GetStoreRootOnly(paging, location);
		}

		/// <summary>
		/// Saves a crud store for the business entity Link.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<Link> Link_SaveStore(CrudStore<Link> store) {
			LinkBroker broker = Helper.CreateInstance<LinkBroker>();
			return broker.SaveStore(store);
		}

		/// <summary>
		/// Gets a list for the business entity Link. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public List<Link> Link_GetAll(PagingParameter paging, PagingLocation location, params string[] fields) {
			int total;
			LinkBroker broker = Helper.CreateInstance<LinkBroker>();
			return broker.GetAll(paging, location, out total, fields: fields); ;
		}

		/// <summary>
		/// Gets a specific item for the business entity Link.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public Link Link_GetItem(string Key, params string[] fields) {
			LinkBroker broker = Helper.CreateInstance<LinkBroker>();
			return broker.GetItem(Key, fields);
		}

		/// <summary>
		/// Loads a specific item for the business entity Link.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse Link_FormLoad(string Key) {
			LinkBroker broker = Helper.CreateInstance<LinkBroker>();
			return broker.FormLoad(Key);
		}


		/// <summary>
		/// Creates a new business entity Link and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse Link_FormCreate(Link item) {
			LinkBroker broker = Helper.CreateInstance<LinkBroker>();
			return broker.FormCreate(item);
		}

		/// <summary>
		/// Updates an existing business entity Link and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse Link_FormUpdate(Link item) {
			LinkBroker broker = Helper.CreateInstance<LinkBroker>();
			return broker.FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse Link_FormUpdateBatch(string[] keys, Link item) {
			LinkBroker broker = Helper.CreateInstance<LinkBroker>();
			return broker.FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Deletes an existing business entity Link.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool Link_Delete(Link item) {
			LinkBroker broker = Helper.CreateInstance<LinkBroker>();
			return broker.Delete(item);
		}

		/// <summary>
		/// Gets a json store of ClassificationItem based on ClassificationDefinition.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<ClassificationItem> ClassificationItemDefinition_GetStore(PagingParameter paging, PagingLocation location) {
			ClassificationItemBroker broker = Helper.CreateInstance<ClassificationItemBroker>();
			return broker.GetAllClassificationItemDefinition().ToJsonStore();
		}

		/// <summary>
		/// Creates an association between a ClassificationItem and a Priority.
		/// </summary>
		/// <param name="KeyClassificationChildren">The Key of the ClassificationItem.</param>
		/// <param name="KeyClassificationParent">The Key of the parent ClassificationItem in case the priority is added to a relationship.</param>
		/// <param name="KeyPriority">The Key of the priority.</param>
		/// <returns></returns>
		public bool ClassificationItemPriority_Add(string KeyClassificationChildren, string KeyClassificationParent, string KeyPriority) {
			PriorityBroker brokerPriority = Helper.CreateInstance<PriorityBroker>();
			return brokerPriority.AddPriorityClassification(KeyClassificationChildren, KeyClassificationParent, KeyPriority);
		}

		/// <summary>
		/// Deletes an association between a ClassificationItem and a Priority.
		/// </summary>
		/// <param name="KeyClassificationChildren">The Key of the ClassificationItem.</param>
		/// <param name="KeyClassificationParent">The Key of the parent ClassificationItem in case the priority is added to a relationship.</param>
		/// <returns></returns>
		public bool ClassificationItemPriority_Delete(string KeyClassificationChildren, string KeyClassificationParent) {
			PriorityBroker brokerPriority = Helper.CreateInstance<PriorityBroker>();
			return brokerPriority.DeletePriorityClassification(KeyClassificationChildren, KeyClassificationParent);
		}

		/// <summary>
		/// Creates an Association beetween a list of Pset's Attributes and a ClassificationItem.
		/// </summary>
		/// <param name="Key">the Key of the ClassificationItem.</param>
		/// <param name="psetDefinitions">The list pset definitions.</param>
		/// <param name="psetAttributeDefinitions">The list of pset attribute definitions.</param>
		/// <remarks>Any attributes of a the pset definitions provided will be included, only the specified attributes definition of the last argument will be included.</remarks>
		/// <returns></returns>
		public bool ClassificationItemPset_Add(string Key, List<PsetDefinition> psetDefinitions, List<PsetAttributeDefinition> psetAttributeDefinitions) {
			PsetAttributeDefinitionBroker brokerPsetAttributeDefinition = Helper.CreateInstance<PsetAttributeDefinitionBroker>();
			List<PsetAttributeDefinition> list = new List<PsetAttributeDefinition>();
			if (psetDefinitions != null && psetDefinitions.Count > 0) {
				list.AddRange(
					brokerPsetAttributeDefinition.GetAttributesFromPsetDefinitionList(
						psetDefinitions.Select(p => p.KeyPropertySet).ToArray()));
			}
			if (psetAttributeDefinitions != null && psetAttributeDefinitions.Count > 0) {
				list.AddRange(psetAttributeDefinitions);
			}
			List<string> keys = list.Select(p => p.KeyPropertySingleValue).Distinct().ToList();
			ObjectClassificationBroker brokerObjectClassification = Helper.CreateInstance<ObjectClassificationBroker>();
			return keys.Select(key => new ObjectClassification {
				KeyClassificationItem = Key,
				KeyObject = key,
				TypeName = "IfcPropertySingleValue"
			}).Aggregate(true, (current, assignation) => current && brokerObjectClassification.FormCreate(assignation).success);
		}

		/// <summary>
		/// Cleans all the pset values for the classification item
		/// </summary>
		/// <param name="key">The key.</param>
		/// <returns></returns>
		public void ClassificationItemPset_CleanAllPsetValues(string key) {

			string keyclassificationItemXml = Helper.BuildXml(new[] { key });

			ClassificationItemBroker classificationItemBroker = Helper.CreateInstance<ClassificationItemBroker>();
			classificationItemBroker.ClassificationItem_CleanAllPsetValue(keyclassificationItemXml);

		}

		/// <summary>
		/// Deletes an Association beetween a list of Pset's Attributes and a ClassificationItem.
		/// </summary>
		/// <param name="Key">The key.</param>
		/// <param name="psetDefinitions">The pset definitions.</param>
		/// <param name="psetAttributeDefinitions">The pset attribute definitions.</param>
		/// <param name="shouldClean">if set to <c>true</c> [should clean].</param>
		/// <returns></returns>
		public bool ClassificationItemPset_Delete(string Key, List<PsetDefinition> psetDefinitions, List<PsetAttributeDefinition> psetAttributeDefinitions, bool shouldClean) {


			PsetAttributeDefinitionBroker brokerPsetAttributeDefinition = Helper.CreateInstance<PsetAttributeDefinitionBroker>();
			List<PsetAttributeDefinition> listWithData = new List<PsetAttributeDefinition>();
			if (psetDefinitions != null && psetDefinitions.Count > 0) {
				listWithData.AddRange(
					brokerPsetAttributeDefinition.GetAttributesFromPsetDefinitionList(
						psetDefinitions.Select(p => p.KeyPropertySet).ToArray()));
			}
			if (psetAttributeDefinitions != null && psetAttributeDefinitions.Count > 0) {
				listWithData.AddRange(psetAttributeDefinitions);
			}
			List<string> keys = listWithData.Select(p => p.KeyPropertySingleValue).Distinct().ToList();
			ObjectClassificationBroker brokerObjectClassification = Helper.CreateInstance<ObjectClassificationBroker>();

			bool result = keys.Select(key => new ObjectClassification {
				KeyClassificationItem = Key,
				KeyObject = key,
				TypeName = "IfcPropertySingleValue"
			}).Select(brokerObjectClassification.GetItemByValues).Where(assignation => assignation != null).Aggregate(true, (current, assignation) => current && brokerObjectClassification.Delete(assignation));

			if (shouldClean) {
				CleanPset(Key, psetDefinitions, psetAttributeDefinitions, listWithData);
			}
			return result;
		}



		/// <summary>
		/// Creates an association between a ClassificationItem and a WorkflowAssemblyDefinition.
		/// </summary>
		/// <param name="KeyClassificationChildren">The Key of the ClassificationItem.</param>
		/// <param name="KeyClassificationParent">The Key of the parent ClassificationItem in case the priority is added to a relationship.</param>
		/// <param name="WorkflowAssemblyDefinitionName">Name of the workflow assembly definition.</param>
		/// <param name="WorkflowAssemblyDefinitionAssemblyQualifiedName">Name of the workflow assembly definition assembly qualified.</param>
		/// <returns></returns>
		public bool ClassificationItemWorkflowAssemblyDefinition_Add(string KeyClassificationChildren,
																	 string KeyClassificationParent,
																	 string WorkflowAssemblyDefinitionName,
																	 string WorkflowAssemblyDefinitionAssemblyQualifiedName) {
			ClassificationItemBroker brokerClassificationItem = Helper.CreateInstance<ClassificationItemBroker>();
			return brokerClassificationItem.AddWorkflowAssemblyDefinitionClassification(KeyClassificationChildren,
																						KeyClassificationParent,
																						WorkflowAssemblyDefinitionName,
																						WorkflowAssemblyDefinitionAssemblyQualifiedName);
		}

		/// <summary>
		/// Deletes an association between a ClassificationItem and a WorkflowAssemblyDefinition.
		/// </summary>
		/// <param name="KeyClassificationChildren">The Key of the ClassificationItem.</param>
		/// <param name="KeyClassificationParent">The Key of the parent ClassificationItem in case the priority is added to a relationship.</param>
		/// <returns></returns>
		public bool ClassificationItemWorkflowAssemblyDefinition_Delete(string KeyClassificationChildren,
																		string KeyClassificationParent) {
			ClassificationItemBroker brokerClassificationItem = Helper.CreateInstance<ClassificationItemBroker>();
			return brokerClassificationItem.DeleteWorkflowAssemblyDefinitionClassification(KeyClassificationChildren,
																						   KeyClassificationParent);
		}

		/// <summary>
		/// Cleans the pset.
		/// </summary>
		/// <param name="KeyClassificationItem">The classification item key.</param>
		/// <param name="psetDefinitions">The pset definitions.</param>
		/// <param name="psetAttributeDefinitions">The pset attribute definitions.</param>
		/// <param name="listWithData">The pset attribute definitions as retrieved from the DB.</param>
		private static void CleanPset(string KeyClassificationItem, List<PsetDefinition> psetDefinitions, List<PsetAttributeDefinition> psetAttributeDefinitions, IEnumerable<PsetAttributeDefinition> listWithData) {
			string keyPset;
			string psetAttributeListKeysXml;

			string keyclassificationItemXml = Helper.BuildXml(new[] { KeyClassificationItem });

			if (psetDefinitions.Any()) {
				keyPset = psetDefinitions.First().KeyPropertySet;

				string[] attributeNames = listWithData.Select(attrib => attrib.AttributeName).ToArray();
				psetAttributeListKeysXml = Helper.BuildXml(attributeNames);
			}
			else {
				keyPset = psetAttributeDefinitions.First().KeyPropertySet;
				psetAttributeListKeysXml = Helper.BuildXml(psetAttributeDefinitions.Select(def => def.AttributeName).ToArray());
			}

			ClassificationItemBroker classificationItemBroker = Helper.CreateInstance<ClassificationItemBroker>();
			classificationItemBroker.ClassificationItem_CleanPsetValue(keyclassificationItemXml, keyPset, psetAttributeListKeysXml);
		}

		/// <summary>
		/// Gets all Color names.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <returns></returns>
		public JsonStore<ListElement> Color_GetStore(PagingParameter paging) {
			var colors = typeof(Color).GetProperties();
			var result = colors.Where(x => x.PropertyType == typeof(Color)).Select(color => new ListElement {
				Id = color.Name,
				MsgCode = color.Name,
				Value = color.Name
			}).ToList();
			//result.Add(new ListElement { Id = "Custom", MsgCode = "Custom", Value = "Custom" });
			// define a default sort order when no parameter was passed.
			if ((paging != null) && String.IsNullOrEmpty(paging.sort)) {
				paging.sort = "Value";
				paging.dir = "ASC";
			}
			var retVal = result.ToJsonStoreWithFilter(paging);
			return retVal;
		}

		/// <summary>
		/// Converts the business entities.
		/// </summary>
		/// <param name="filename">The filename.</param>
		/// <returns></returns>
		public List<string> ConvertBusinessEntities(string filename) {
			var entitiesList = MappingConversionService.GetEntities(filename);
			return (from Dictionary<string, object> entitiesDic in entitiesList select MappingService.Export(entitiesDic.Select(x => x.Value).ToList())).ToList();
		}

		/// <summary>
		/// Gets the store of desktop background.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <returns></returns>
		public JsonStore<ListElement> DesktopBackground_GetStore(PagingParameter paging) {
			var retVal = TenantHelper.GetCurrentTenantDesktopImageList();
			return retVal.ToJsonStoreWithFilter(paging);
		}

		/// <summary>
		/// Audit the get store by entity key - for the template view.
		/// </summary>
		/// <param name="entityName">Name of the entity type.</param>
		/// <param name="paging">The paging.</param>
		/// <param name="entityKey"></param>
		/// <returns></returns>
		public JsonStore<DetailedAudit> DetailedAudit_GetStoreByKey(string entityName, PagingParameter paging, string entityKey) {
			paging = AddAuditPagingFilters(entityName, paging, entityKey);
			var broker = Helper.CreateInstance<DetailedAuditBroker>();
			broker.EntityName = entityName;
			return broker.GetStore(paging, PagingLocation.Database);
		}

		/// <summary>
		/// Deletes an existing business entity Document.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool Document_Delete(Document item) {
			DocumentBroker broker = Helper.CreateInstance<DocumentBroker>();
			return broker.Delete(item);
		}

		/// <summary>
		/// Creates a new business entity Document and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse Document_FormCreate(Document item) {
			DocumentBroker broker = Helper.CreateInstance<DocumentBroker>();
			return broker.FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity Document.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse Document_FormLoad(string Key) {
			DocumentBroker broker = Helper.CreateInstance<DocumentBroker>();
			return broker.FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity Document and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse Document_FormUpdate(Document item) {
			DocumentBroker broker = Helper.CreateInstance<DocumentBroker>();
			return broker.FormUpdate(item);
		}

		/// <summary>
		/// Gets a stream of a document that is an image.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <returns></returns>
		public StreamResult Document_GetImageStream(string key) {
			var doc = Document_GetItem(key);
			var streamResult = new StreamResult(doc.GetStream(), doc.DocumentName, doc.DocumentType);
			return streamResult;
		}

		/// <summary>
		/// Gets a specific item for the business entity Document.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public Document Document_GetItem(string Key, params string[] fields) {
			DocumentBroker broker = Helper.CreateInstance<DocumentBroker>();
			return broker.GetItem(Key, fields);
		}

		/// <summary>
		/// Gets the metadata item for a Document.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public DocumentMetadata DocumentMetadata_GetItem(string Key, params string[] fields) {
			var broker = Helper.CreateInstance<DocumentMetadataBroker>();
			var retVal = broker.GetItem(Key, fields);
			return retVal;
		}

		/// <summary>
		/// Gets the updatable batch properties from an entity type.
		/// </summary>
		/// <param name="entityType">The type of the entity.</param>
		/// <returns></returns>
		public List<string> Entity_GetUpdatableBatchProperties(string entityType) {
			Type type = Type.GetType(String.Format("Vizelia.FOL.BusinessEntities.{0}, Vizelia.FOL.Common", entityType));
			return Helper.GetUpdatableBatchProperties(type);
		}

		/// <summary>
		/// Prepares the documents of the entity
		/// </summary>
		/// <param name="fieldsAndDocuments">The fields and corresponding documents.</param>
		/// <returns></returns>
		public FormResponse EntityDocument_FormPrepare(Dictionary<string, Document> fieldsAndDocuments) {
			try {

				var fieldsAndGuids = new Dictionary<string, string>();
				foreach (var fieldsAndDocument in fieldsAndDocuments) {
					var field = fieldsAndDocument.Key;
					var document = fieldsAndDocument.Value;
					var documentGuid = DocumentHelper.PrepareDocument(document);
					fieldsAndGuids.Add(field, documentGuid);
				}
				List<SimpleListElement> listElements = fieldsAndGuids.Select(fieldAndGuid =>
																			 new SimpleListElement {
																				 Id = fieldAndGuid.Key,
																				 Value = fieldAndGuid.Value
																			 })
					.ToList();
				//This is here because otherwise the response is opened in a save as dialog.
				var retVal = new FormResponse {
					data = new DocumentResponse { FieldsAndGuids = listElements },
					success = true
				};
				return retVal;
			}
			catch (Exception e) {
				return new FormResponse { success = false, msg = e.Message };
			}
		}

		/// <summary>
		/// Gets all elements from an enum. 
		/// </summary>
		/// <param name="enumTypeName">The type name of the enum.</param>
		/// <returns></returns>
		public JsonStore<ListElementGeneric<int>> Enum_GetStore(string enumTypeName) {
			List<ListElementGeneric<int>> result = new List<ListElementGeneric<int>>();
			Type enumType = Type.GetType(enumTypeName);
			if (enumType != null) {
				result = EnumExtension.GetListElementNumeric(enumType);
			}
			return result.ToJsonStore();

		}


		/// <summary>
		/// Get the count of all entities for a tenant.
		/// </summary>
		/// <returns></returns>
		public JsonStore<ListElementGeneric<long>> EntityCount_GetStore() {
			var baseBusinessEntitiyType = typeof(BaseBusinessEntity);
			var result = m_CoreDataAccess.EntityCount_GetAll();
			var query = from p1 in result
						join p2 in baseBusinessEntitiyType.Assembly.GetTypes()
						on p1.Id equals p2.Name
						select new ListElementGeneric<long> {
							Id = p1.Id,
							MsgCode = Helper.GetAttributeValue<LocalizedTextAttribute>(p2),
							IconCls = Helper.GetAttributeValue<IconClsAttribute>(p2),
							Value = p1.Value
						};

			return query.OrderBy(p => p.Label).ToList().ToJsonStore();
		}


		/// <summary>
		/// Gets all the enums from the application.
		/// </summary>
		/// <returns></returns>
		public Dictionary<string, JsonStore<ListElementGeneric<int>>> Enum_GetStores() {
			Type[] types = typeof(BaseBusinessEntity).Assembly.GetTypes();
			IEnumerable<Type> enums = from type in types
									  where type.IsEnum
									  select type;
			Dictionary<string, JsonStore<ListElementGeneric<int>>> retVal =
					enums.ToDictionary(e => e.FullName + "," + e.Module.Name.Substring(0, e.Module.Name.Length - 4), e => Enum_GetStore(e.AssemblyQualifiedName));
			return retVal;
		}


		/// <summary>
		/// Filters a list of calendar events to extract them based on their category.
		/// </summary>
		/// <param name="list">The list to filter.</param>
		/// <param name="category">The category.</param>
		/// <returns></returns>
		private List<CalendarEvent> FilterListEventByCategory(IEnumerable<CalendarEvent> list, string category) {
			var query = from p in list
						where p.Category == category
						select p;

			return query.ToList();
		}

		/// <summary>
		/// Gets all the installed Font Family.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <returns></returns>
		public JsonStore<ListElement> FontFamily_GetStore(PagingParameter paging) {
			using (var fonts = new InstalledFontCollection()) {
				List<ListElement> result = fonts.Families.Select(font => new ListElement {
					Id = font.Name,
					MsgCode = font.Name,
					Value = font.Name
				}).ToList();

				// define a default sort order when no parameter was passed.
				if ((paging != null) && String.IsNullOrEmpty(paging.sort)) {
					paging.sort = "Value";
					paging.dir = "ASC";
				}
				return result.ToJsonStoreWithFilter(paging);
				//return result.ToJsonStore();
			}
		}

		/// <summary>
		/// Deletes an existing business entity Furniture.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool Furniture_Delete(Furniture item) {
			FurnitureBroker broker = Helper.CreateInstance<FurnitureBroker>();
			return broker.Delete(item);
		}

		/// <summary>
		/// Creates a new business entity Furniture and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse Furniture_FormCreate(Furniture item) {
			FurnitureBroker broker = Helper.CreateInstance<FurnitureBroker>();
			return broker.FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity Furniture.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse Furniture_FormLoad(string Key) {
			FurnitureBroker broker = Helper.CreateInstance<FurnitureBroker>();
			return broker.FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity Furniture and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse Furniture_FormUpdate(Furniture item) {
			FurnitureBroker broker = Helper.CreateInstance<FurnitureBroker>();
			return broker.FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse Furniture_FormUpdateBatch(string[] keys, Furniture item) {
			FurnitureBroker broker = Helper.CreateInstance<FurnitureBroker>();
			return broker.FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity Furniture. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public List<Furniture> Furniture_GetAll(PagingParameter paging, PagingLocation location, params string[] fields) {
			int total;
			FurnitureBroker broker = Helper.CreateInstance<FurnitureBroker>();
			return broker.GetAll(paging, location, out total, fields: fields); ;
		}

		/// <summary>
		/// Gets a specific item for the business entity Furniture.
		/// </summary>
		/// <param name="KeySpace">The key space.</param>
		/// <returns></returns>
		public List<Furniture> Furniture_GetChildren(string KeySpace) {
			FurnitureBroker broker = Helper.CreateInstance<FurnitureBroker>();
			return broker.GetChildren(KeySpace);
		}

		/// <summary>
		/// Gets a specific item for the business entity Furniture.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public Furniture Furniture_GetItem(string Key, params string[] fields) {
			FurnitureBroker broker = Helper.CreateInstance<FurnitureBroker>();
			return broker.GetItem(Key, fields);
		}

		/// <summary>
		/// Gets a json store for the business entity Furniture.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<Furniture> Furniture_GetStore(PagingParameter paging, PagingLocation location) {
			FurnitureBroker broker = Helper.CreateInstance<FurnitureBroker>();
			return broker.GetStore(paging, location);
		}

		/// <summary>
		/// Saves a crud store for the business entity Furniture.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<Furniture> Furniture_SaveStore(CrudStore<Furniture> store) {
			FurnitureBroker broker = Helper.CreateInstance<FurnitureBroker>();
			return broker.SaveStore(store);
		}

		/// <summary>
		/// Gets the entity properties tree.
		/// </summary>
		/// <param name="type">The type.</param>
		/// <param name="depth">The depth.</param>
		/// <returns></returns>
		public List<TreeNode> GetEntityTree(Type type, int depth = -1) {
			// Recursion stop condition
			if (depth == 0)
				return null;

			var retVal = new List<TreeNode>();
			foreach (var propertyInfo in type.GetProperties().OrderBy(x => x.Name)) {
				if (Attribute.GetCustomAttribute(propertyInfo, typeof(ViewableByDataModelAttribute)) != null) {
					if (propertyInfo.PropertyType.IsAssignableFrom(typeof(List<Pset>))) {
						retVal.AddRange(GetPsetPropertyTreeNode(type, propertyInfo));
					}
					else if (propertyInfo.PropertyType.IsGenericType && propertyInfo.PropertyType.GetGenericTypeDefinition() == typeof(List<>)) {
						Type itemType = propertyInfo.PropertyType.GetGenericArguments()[0];
						var children = GetEntityTree(itemType, depth - 1);
						var iconCls = GetVizIconCls(itemType);

						retVal.Add(new TreeNode {
							Key = propertyInfo.Name + "[]",
							text = propertyInfo.Name + "[]",
							children = children,
							leaf = children == null,
							iconCls = iconCls
						});
					}
					else {
						var iconCls = GetVizIconCls(propertyInfo.PropertyType);

						// Recursion - gets the entity children nodes
						var children = GetEntityTree(propertyInfo.PropertyType, depth - 1);
						retVal.Add(new TreeNode {
							Key = propertyInfo.Name,
							text = propertyInfo.Name,
							children = children,
							leaf = children == null,
							iconCls = iconCls
						});
					}
				}
			}
			foreach (var methodInfo in type.GetMethods().OrderBy(x => x.Name)) {
				if (Attribute.GetCustomAttribute(methodInfo, typeof(ViewableByDataModelAttribute)) != null) {
					var parameters = methodInfo.GetParameters();
					retVal.Add(new TreeNode {
						Key = methodInfo.Name,
						text = (parameters.Length > 0) ? methodInfo.Name + "(" + String.Join(",", parameters.Select(x => x.Name)) + ")" : methodInfo.Name + "()",
						leaf = true,
						iconCls = "viz-icon-small-function"
					});
				}
			}
			return retVal.Count > 0 ? retVal : null;
		}

		/// <summary>
		/// Gets the general type tree.
		/// </summary>
		/// <param name="typeNames">The type names.</param>
		/// <param name="classificationItemLocalIdPath">The classification item local id path.</param>
		/// <returns></returns>
		public List<TreeNode> GetGeneralTypeTree(List<string> typeNames, string classificationItemLocalIdPath) {
			if (!string.IsNullOrEmpty(classificationItemLocalIdPath)) {
				var classificationMapping = VizeliaConfiguration.Instance.ClassificationToDataModelTypeMapping[classificationItemLocalIdPath];
				if (classificationMapping != null) {
					typeNames.AddRange(classificationMapping.ModelDataTypeNames.Split(';'));
				}
			}
			var retVal = new List<TreeNode>();
			foreach (string typeName in typeNames) {
				TreeNode node = null;
				var type = typeof(BaseBusinessEntity).Assembly.GetType(typeName);
				if (type != null) {
					node = new TreeNode {
						Key = "Model",
						text = type.Name,
						children = GetEntityTree(type, 3),
						iconCls = GetVizIconCls(type)
					};
				}
				else {
					type = Type.GetType(typeName);
					if (type != null)
						node = new TreeNode {
							Key = type.Name,
							text = type.Name,
							children = GetTypeTree(type),
							iconCls = "viz-icon-small-module"
						};
				}
				if (node != null)
					retVal.Add(node);
			}
			return retVal;
		}

		/// <summary>
		/// Returns the progress of operationId.
		/// This function is called by a timer on a client, usually when downloading a file, and the long processing function is responsible for updating Session[operationId] with the status of the operation.
		/// </summary>
		/// <param name="operationId">The id of the iframe generated on client side for calling the long process function</param>
		/// <returns>The progress of the operation</returns>
		public LongRunningOperationState GetLongRunningOperationProgress(Guid operationId) {
			var state = LongRunningOperationService.GetOperationState(operationId);
			return state;
		}

		/// <summary>
		/// Gets the long running operation result entity.
		/// </summary>
		/// <param name="operationId">The operation id.</param>
		/// <returns>A file result for the given operation ID</returns>
		public BaseBusinessEntity GetLongRunningOperationResultEntity(Guid operationId) {
			var state = LongRunningOperationService.GetOperationState(operationId);
			var entity = state.Result as BaseBusinessEntity;

			if (entity == null) {
				throw new ArgumentException("Result of given operation is not a BaseBusinessEntity");
			}

			LongRunningOperationService.Complete(operationId);

			return entity;
		}

		/// <summary>
		/// Gets the long running operation result file.
		/// </summary>
		/// <param name="operationId">The operation id.</param>
		public StreamResult GetLongRunningOperationResultFile(Guid operationId) {
			var progress = LongRunningOperationService.GetOperationState(operationId);
			var file = progress.Result as StreamResult;

			if (file == null) {
				throw new ArgumentException("Result of given operation is not a StreamResult");
			}

			LongRunningOperationService.Complete(operationId);
			return file;
		}

		/// <summary>
		/// Gets the pset property tree node.
		/// </summary>
		/// <param name="type">The type.</param>
		/// <param name="propertyInfo">The property info.</param>
		/// <returns></returns>
		private IEnumerable<TreeNode> GetPsetPropertyTreeNode(Type type, PropertyInfo propertyInfo) {
			var retVal = new List<TreeNode>();
			var ifcTypeName = Helper.GetAttributeValue<IfcTypeNameAttribute>(type);
			var psetList = PsetDefinition_GetListByUsageName(ifcTypeName ?? type.Name, null);
			foreach (PsetDefinition psetDefinition in psetList) {
				var newNode = new TreeNode {
					text = psetDefinition.PsetName,
					children = new List<TreeNode>(),
					leaf = false,
					iconCls = GetVizIconCls(propertyInfo.PropertyType)
				};
				foreach (PsetAttributeDefinition psetAttributeDefinition in psetDefinition.Attributes) {
					newNode.children.Add(new TreeNode {
						text = psetAttributeDefinition.AttributeName,
						children = null,
						leaf = true,
						iconCls = GetVizIconCls(typeof(String))
					});
				}
				if (newNode.children.Count == 0) {
					newNode.children = null;
					newNode.leaf = true;
				}
				retVal.Add(newNode);
			}
			return retVal;
		}

		/// <summary>
		/// Gets the TypeName of a record.
		/// </summary>
		/// <param name="Key">The Key of the record.</param>
		/// <returns>The TypeName of the record.</returns>
		public string GetTypeNameFromRecord(string Key) {
			return m_CoreDataAccess.GetTypeNameFromRecord(Key);
		}

		/// <summary>
		/// Gets the type tree.
		/// </summary>
		/// <param name="type">The type.</param>
		/// <returns></returns>
		private List<TreeNode> GetTypeTree(Type type) {
			return type.GetMethods().OrderBy(x => x.Name).Select(methodInfo => new TreeNode {
				text = methodInfo.Name + "(" + string.Join(",", methodInfo.GetParameters().ToArray<object>()) + ")",
				children = null,
				leaf = true,
				iconCls = "viz-icon-small-function",
				qtip = methodInfo.ToString()
			}).ToList();
		}

		/// <summary>
		/// Gets the viz icon class.
		/// </summary>
		/// <param name="type">The type.</param>
		/// <returns></returns>
		private string GetVizIconCls(Type type) {
			var retVal = "viz-icon-small-";
			var genericParams = type.GetGenericArguments();
			if (type.IsGenericType && genericParams[0] != null && !String.IsNullOrEmpty(genericParams[0].FullName))
				retVal += genericParams[0].FullName;
			else
				retVal += type.FullName;

			retVal = retVal.ToLower().Replace('.', '-') + " " + Helper.GetAttributeValue<IconClsAttribute>(type);

			return retVal;
		}

		/// <summary>
		/// Gets the WorkflowAssemblyDefinition assigned to a any upper level of ClassificationItem Hierarchy.
		/// </summary>
		/// <param name="KeyClassificationChildren">The key classification children.</param>
		/// <param name="KeyClassificationParent">The key classification parent.</param>
		/// <returns></returns>
		public WorkflowAssemblyDefinition GetWorlflowAssemblyDefinitionByClassificationAscendant(
			string KeyClassificationChildren, string KeyClassificationParent) {
			ClassificationItemBroker brokerClassificationItem = Helper.CreateInstance<ClassificationItemBroker>();
			return brokerClassificationItem.GetWorkflowAssemblyDefinitionByClassificationAscendant(KeyClassificationChildren,
																								   KeyClassificationParent);
		}

		/// <summary>
		/// Audit the get store by entity key - for the grid view.
		/// </summary>
		/// <param name="entityName">Name of the entity type - Fully qualified name.</param>
		/// <param name="paging">The paging.</param>
		/// <returns></returns>
		public JsonStore<Audit> Audit_GetStore(string entityName, PagingParameter paging) {
			paging = AddAuditPagingFilters(entityName, paging);
			var broker = Helper.CreateInstance<AuditBroker>();
			return broker.GetAuditStore(paging, PagingLocation.Database, entityName);
		}

		private static PagingParameter AddAuditPagingFilters(string tableName, PagingParameter paging, string entityKey = null) {
			var retVal = paging ?? new PagingParameter();
			retVal.filters = retVal.filters ?? new List<GridFilter>();
			foreach (var gridFilter in retVal.filters.Where(gridFilter => gridFilter.field == "Operation")) {
				try {
					gridFilter.data.value = gridFilter.data.value.ParseAsEnum<AuditOperation>().ParseAsInt().ToString(CultureInfo.InvariantCulture);
				}
				catch { ;}
			}
			retVal.filters.Add(new GridFilter { data = new GridData { type = "string", value = tableName, comparison = "eq" }, field = "TableName" });
			if (!string.IsNullOrWhiteSpace(entityKey)) {
				retVal.filters.Add(new GridFilter { data = new GridData { type = "string", value = entityKey, comparison = "eq" }, field = "PrimaryKeyValue" });
			}
			retVal.filters.Add(new GridFilter { data = new GridData { type = "string", value = ContextHelper.ApplicationName, comparison = "eq" }, field = "TenantName" });

			retVal.dir = string.IsNullOrWhiteSpace(retVal.dir) ? "DESC" : retVal.dir;
			retVal.sort = string.IsNullOrWhiteSpace(retVal.sort) ? "AuditId" : retVal.sort;

			return retVal;
		}

		//private static string GetTableNameByEntityName(string entityName) {
		//    var retval = entityName;
		//    if (entityName.Equals("MeterData")) {
		//        retval = string.Format("{0}_{1}", retval, ContextHelper.ApplicationName);
		//    }
		//    return retval;
		//}

		/// <summary>
		/// Imports the azman from the specified document stream.
		/// </summary>
		/// <param name="documentStream">The document stream.</param>
		/// <param name="importOptions">The import options.</param>
		/// <returns></returns>
		private LongRunningOperationResult Import(MemoryStream documentStream, AzmanImportOptions importOptions) {
			((IFOLRoleProvider)Roles.Provider).Import(documentStream, importOptions);
			return new LongRunningOperationResult(LongRunningOperationStatus.Completed);
		}

		/// <summary>
		/// Initilization of the application.
		/// </summary>
		public static void Init() {
			ClassificationItemBroker.ClearClassificationItemDefinitionCache();
		}

		/// <summary>
		/// Deletes an existing business entity Job.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool Job_Delete(Job item) {
			JobBroker broker = Helper.CreateInstance<JobBroker>();
			return broker.Delete(item);
		}

		/// <summary>
		/// Creates a new business entity Job and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse Job_FormCreate(Job item) {
			JobBroker broker = Helper.CreateInstance<JobBroker>();
			return broker.FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity Job.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse Job_FormLoad(string Key) {
			JobBroker broker = Helper.CreateInstance<JobBroker>();
			return broker.FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity Job and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse Job_FormUpdate(Job item) {
			JobBroker broker = Helper.CreateInstance<JobBroker>();
			return broker.FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse Job_FormUpdateBatch(string[] keys, Job item) {
			JobBroker broker = Helper.CreateInstance<JobBroker>();
			return broker.FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity Job. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public List<Job> Job_GetAll(PagingParameter paging, PagingLocation location, params string[] fields) {
			int total;
			JobBroker broker = Helper.CreateInstance<JobBroker>();
			return broker.GetAll(paging, location, out total, fields: fields); ;
		}

		/// <summary>
		/// Gets a specific item for the business entity Job.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public Job Job_GetItem(string Key, params string[] fields) {
			JobBroker broker = Helper.CreateInstance<JobBroker>();
			return broker.GetItem(Key, fields);
		}

		/// <summary>
		/// Executes the selected job
		/// </summary>
		/// <param name="Key">The key.</param>
		/// <returns></returns>
		public Job Job_ExecuteItem(string Key) {
			JobBroker broker = Helper.CreateInstance<JobBroker>();
			return broker.ExecuteItem(Key);
		}

		/// <summary>
		/// Pauses the selected job
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public Job Job_PauseItem(string Key) {
			JobBroker broker = Helper.CreateInstance<JobBroker>();
			return broker.PauseItem(Key);
		}

		/// <summary>
		/// Resumes the selected job
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public Job Job_ResumeItem(string Key) {
			JobBroker broker = Helper.CreateInstance<JobBroker>();
			return broker.ResumeItem(Key);
		}

		/// <summary>
		/// Gets a json store for the business entity Job.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<Job> Job_GetStore(PagingParameter paging, PagingLocation location) {
			var jobBroker = Helper.CreateInstance<JobBroker>();
			var traceBroker = Helper.CreateInstance<TraceEntryBroker>();
			var jobs = jobBroker.GetStore(paging, location);

			Job_PopulateWithAggregatedInfo(jobs.records, traceBroker);

			return jobs;
		}

		/// <summary>
		/// Job_s the populate aggregated info.
		/// </summary>
		/// <param name="jobs">The jobs.</param>
		/// <param name="traceBroker">The trace broker.</param>
		private static void Job_PopulateWithAggregatedInfo(IEnumerable<Job> jobs, TraceEntryBroker traceBroker) {
			Dictionary<string, List<TraceEntry>> jobsSummary = traceBroker.GetLastJobRunByKeys(jobs.Select(job => job.KeyJob + "." + job.Name));

			foreach (var job in jobs) {

				if (!jobsSummary.ContainsKey(job.KeyJob + "." + job.Name)) {
					continue;
				}

				var jobSummary = jobsSummary[job.KeyJob + "." + job.Name];

				job.LastRunErrorMessage = string.Empty;
				var maxError = TraceEntrySeverity.Information;
				var maxErrorMessage = string.Empty;

				IEnumerable<TraceEntry> nonVerboseTraceErrors = jobSummary.Where(entry => entry.Severity != TraceEntrySeverity.Verbose);
				foreach (var traceEntry in nonVerboseTraceErrors) {
					if (traceEntry.Severity >= maxError) {
						maxError = traceEntry.Severity;
						maxErrorMessage = traceEntry.Message;
					}
				}

				job.LastRunSeverity = maxError;
				job.LastRunErrorMessage = maxErrorMessage;

				if (jobSummary.Any()) {
					var jobStart = job.LastRunTime = jobSummary.Min(x => x.Timestamp);
					var jobEnd = jobSummary.Max(x => x.Timestamp);
					var duration = jobEnd - jobStart;
					job.LastRunDuration = new TimeSpan(duration.Value.Ticks).ToString();
				}
			}
		}

		/// <summary>
		/// Saves a crud store for the business entity Job.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<Job> Job_SaveStore(CrudStore<Job> store) {
			JobBroker broker = Helper.CreateInstance<JobBroker>();
			return broker.SaveStore(store);
		}

		/// <summary>
		/// Gets the child entities by parent job step.
		/// </summary>
		/// <typeparam name="TChildEntity">The type of the child entity.</typeparam>
		/// <param name="keyJob">The key job.</param>
		/// <param name="keyStep">The key step.</param>
		/// <param name="paging">The paging.</param>
		/// <returns></returns>
		public JsonStore<TChildEntity> GetChildEntitiesByParentJobStep<TChildEntity>(string keyJob, string keyStep, PagingParameter paging) where TChildEntity : BaseBusinessEntity {
			// This is in case a new step is created, then the keys are still null.
			if (string.IsNullOrEmpty(keyJob) || string.IsNullOrEmpty(keyStep)) {
				return new JsonStore<TChildEntity>();
			}

			// Get the list of child entity keys by the job and the step specified by the parameters.
			Job job = Job_GetItem(keyJob);
			Step step = job.Steps.FirstOrDefault(s => s.KeyStep.Equals(keyStep));

			if (step != null && (step.ModelDataKeys == null || !step.ModelDataKeys.Any())) {
				// No children found, return an empty JsonStore instead of querying the DB with an empty join.
				return new JsonStore<TChildEntity>();
			}

			if (step != null) {
				string keysList = string.Join(",", step.ModelDataKeys.ToArray());

				// Modify the PagingParameter to filter by child entities only.
				if (paging.filters == null) {
					paging.filters = new List<GridFilter>();
				}

				string entityKeyPropertyName = "Key" + typeof(TChildEntity).Name;
				GridFilter keyFilter = paging.filters.FirstOrDefault(f => f.field.Equals(entityKeyPropertyName) && f.data.type.Equals("innerjoin"));

				if (keyFilter == null) {
					paging.filters.Add(new GridFilter { field = entityKeyPropertyName, data = new GridData { type = "innerjoin", value = keysList } });
				}
				else {
					keyFilter.data.value = keysList;
				}
			}

			// Query the broker of the entity by using the modified innerjoin parameter values.
			var broker = BrokerFactory.CreateBroker<TChildEntity>();
			JsonStore<TChildEntity> children = broker.GetStore(paging, PagingLocation.Database, false);

			return children;
		}


		/// <summary>
		/// Gets a json store of a business entity ListElement.
		/// </summary>
		/// <param name="listName">The name of the list.</param>
		/// <returns></returns>
		public JsonStore<ListElement> ListElement_GetStore(string listName) {
			ListElementBroker broker = Helper.CreateInstance<ListElementBroker>(listName);
			return broker.GetStore(new PagingParameter { dir = "ASC", sort = "Value" }, PagingLocation.Memory);
		}

		/// <summary>
		/// Gets a json store of exposed Cultures.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <returns></returns>
		public JsonStore<LocalizationCulture> LocalizationCulture_GetStore(PagingParameter paging) {
			CultureInfo[] aCultures = CultureInfo.GetCultures(CultureTypes.AllCultures);
			var query = aCultures.Select(g => new { Name = g.DisplayName + " - " + g.NativeName, Code = g.Name });
			List<string> supportedCultures = DatabaseResourceHelper.GetSupportedCultures();

			var filter = from p in query
						 join o in supportedCultures on p.Code.ToLower() equals o.ToLower()
						 select new LocalizationCulture { Key = p.Code, Value = p.Name };
			// define a default sort order when no parameter was passed.
			if ((paging != null) && String.IsNullOrEmpty(paging.sort)) {
				paging.sort = "Value";
				paging.dir = "ASC";
			}
			var retVal = filter.ToList();

			return retVal.ToJsonStoreWithFilter(paging);

		}

		/// <summary>
		/// Creates a new LocalizationResource.
		/// </summary>
		/// <param name="item">The localization resource item.</param>
		/// <returns>The FormResponse object.</returns>
		public FormResponse LocalizationResource_FormCreate(LocalizationResource item) {
			var broker = Helper.CreateInstance<LocalizationResourceBroker>();
			var formResponse = broker.FormCreate(item);
			CacheService.Remove(CacheKey.const_cache_localization);
			return formResponse;
		}

		/// <summary>
		/// Updates a CreateLocalizationResource.
		/// </summary>
		/// <param name="item">The localization resource item.</param>
		/// <returns>The FormResponse object.</returns>
		public FormResponse LocalizationResource_FormUpdate(LocalizationResource item) {
			var broker = Helper.CreateInstance<LocalizationResourceBroker>();
			var formResponse = broker.FormUpdate(item);
			CacheService.Remove(CacheKey.const_cache_localization);
			return formResponse;
		}

		/// <summary>
		/// Gets a specific item for the business entity LocalizationResource.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public LocalizationResource LocalizationResource_GetItem(string Key, params string[] fields) {
			LocalizationResourceBroker broker = Helper.CreateInstance<LocalizationResourceBroker>();
			return broker.GetItem(Key, fields);
		}

		/// <summary>
		/// Gets all the localization resource.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public List<LocalizationResource> LocalizationResource_GetAll(PagingParameter paging, PagingLocation location, params string[] fields) {
			var broker = Helper.CreateInstance<LocalizationResourceBroker>();
			int total;
			var retVal = broker.GetAll(paging, location, out total, fields: fields);
			return retVal;
		}

		/// <summary>
		/// Gets all the localization message for a specified language.
		/// </summary>
		/// <param name="language">2 letters Language source. e.g. "en , da" for English or Danish.</param>
		/// <param name="paging">The paging parameter.</param>
		/// <returns></returns>
		public JsonStore<LocalizationResource> LocalizationResource_GetStore(string language, PagingParameter paging) {
			var broker = Helper.CreateInstance<LocalizationResourceBroker>();
			var retVal = broker.GetStore(paging, language);
			retVal.metaData.idProperty = "Key"; // override the id of the record to speed up filtering on client side.
			return retVal;
		}

		/// <summary>
		/// Saves a LocalizationResource store.
		/// </summary>
		/// <param name="language">The base language.</param>
		/// <param name="store">The crud store.</param>
		/// <returns></returns>
		public JsonStore<LocalizationResource> LocalizationResource_SaveStore(string language, CrudStore<LocalizationResource> store) {
			var broker = Helper.CreateInstance<LocalizationResourceBroker>();
			CacheService.Remove(CacheKey.const_cache_localization);
			return broker.SaveStore(store, language);
		}

		/// <summary>
		/// Translate Text using Google Translate.
		/// </summary>
		/// <param name="input">The string to translate.</param>
		/// <param name="languageFrom">2 letters Language source. e.g. "en , da" language pair means to translate from English to Danish</param>
		/// <param name="languageTo">2 letters language destination. e.g. "en , da" language pair means to translate from English to Danish</param>
		/// <returns>Translated to String</returns>
		public string LocalizationResource_TranslateText(string input, string languageFrom, string languageTo) {
			return TranslationHelper.TranslateText(input, languageFrom, languageTo);
		}

		/// <summary>
		/// Gets a dictionnary for the business entity Location, by KeyLocation. 
		/// </summary>
		/// <returns></returns>
		public Dictionary<string, Location> Location_GetDictionary() {
			LocationBroker broker = Helper.CreateInstance<LocationBroker>();
			var retVal = broker.GetDictionary();
			return retVal;
		}

		/// <summary>
		/// Gets a specific item for the business entity Location.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public Location Location_GetItem(string Key, params string[] fields) {
			LocationBroker broker = Helper.CreateInstance<LocationBroker>();
			return broker.GetItem(Key, fields);
		}

		/// <summary>
		/// Gets a list of item for the business entity Location.
		/// </summary>
		/// <param name="Keys"></param>
		/// <returns></returns>
		public List<Location> Location_GetList(string[] Keys) {
			LocationBroker broker = Helper.CreateInstance<LocationBroker>();
			return broker.GetList(Keys);
		}

		/// <summary>
		/// Get the SpatialHierarchy as a list of Location business entity.
		/// </summary>
		/// <param name="Keys">The keys.</param>
		/// <param name="hierarchyDirection">The hierarchy direction.</param>
		/// <returns></returns>
		public List<Location> Location_GetSpatialHierarchy(string[] Keys, HierarchyDirection hierarchyDirection = HierarchyDirection.ASC) {
			List<Filter> spatial = m_CoreDataAccess.GetSpatialHierarchy(Keys, string.Empty, hierarchyDirection);
			var retVal = Location_GetList(spatial.Select(s => s.Key).ToArray());
			return retVal;
		}

		/// <summary>
		/// Get all Location entities.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="location">The location.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public List<Location> Location_GetAll(PagingParameter paging, PagingLocation location, params string[] fields) {
			LocationBroker broker = Helper.CreateInstance<LocationBroker>();
			int total;
			return broker.GetAll(paging, location, out total, fields: fields);
		}

		/// <summary>
		/// Gets a json store for the business entity Location.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<Location> Location_GetStore(PagingParameter paging, PagingLocation location) {
			LocationBroker broker = Helper.CreateInstance<LocationBroker>();
			return broker.GetStore(paging, location);
		}

		/// <summary>
		/// Creates Location entity.
		/// </summary>
		/// <param name="location">The location.</param>
		/// <returns></returns>
		public FormResponse Location_FormCreate(Location location) {
			LocationBroker broker = Helper.CreateInstance<LocationBroker>();
			return broker.FormCreate(location);
		}

		/// <summary>
		/// Updates Location entity.
		/// </summary>
		/// <param name="location">The location.</param>
		/// <returns></returns>
		public FormResponse Location_FormUpdate(Location location) {
			LocationBroker broker = Helper.CreateInstance<LocationBroker>();
			return broker.FormUpdate(location);
		}

		/// <summary>
		/// Deletes Location entity.
		/// </summary>
		/// <param name="location">The location.</param>
		/// <returns></returns>
		public bool Location_Delete(Location location) {
			LocationBroker broker = Helper.CreateInstance<LocationBroker>();
			return broker.Delete(location.KeyLocation);
		}
		/// <summary>
		/// Gets the store of logging for a specific method.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="methodName">Name of the method.</param>
		/// <returns></returns>
		public JsonStore<Logging> Logging_GetStoreByMethodName(PagingParameter paging, string methodName) {
			LoggingBroker broker = Helper.CreateInstance<LoggingBroker>();
			JsonStore<Logging> loggingGetStoreByMethodName = broker.GetStoreByMethodName(paging, methodName);
			foreach (var record in loggingGetStoreByMethodName.records) {
				var traceEntry = TraceEntry_GetStoreByRequestId(new PagingParameter { start = 0, limit = 0 }, record.RequestId);
				foreach (var entry in traceEntry.records) {
					if (record.Severity == TraceEntrySeverity.Information) {
						if ((entry.Severity == TraceEntrySeverity.Error) || (entry.Severity == TraceEntrySeverity.Warning)) {
							record.Severity = entry.Severity;
						}
					}
					if (record.Severity == TraceEntrySeverity.Warning)
						if (entry.Severity == TraceEntrySeverity.Error) {
							record.Severity = entry.Severity;
							break;
						}

				}
			}

			return loggingGetStoreByMethodName;
		}

		/// <summary>
		/// Saves a crud store for the business entity Logging.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<Logging> Logging_SaveStore(CrudStore<Logging> store) {
			LoggingBroker broker = Helper.CreateInstance<LoggingBroker>();
			return broker.SaveStore(store);
		}

		/// <summary>
		/// Returns a list of meters for a specific location.
		/// </summary>
		/// <param name="KeyLocation">The key of the location.</param>
		/// <param name="maxCount">The max count.</param>
		/// <returns>
		/// A list of meter entities.
		/// </returns>
		private List<Meter> Meter_GetListByLocation(string KeyLocation, int maxCount = 0) {
			var broker = Helper.CreateInstance<MeterBroker>();
			var retVal = broker.GetListByKeyLocation(KeyLocation);
			if (maxCount > 0 && retVal.Count > maxCount)
				retVal = retVal.Take(maxCount).ToList();
			return retVal;
		}

		/// <summary>
		/// Methods the name of the definition_ get list by type.
		/// </summary>
		/// <typeparam name="T">The attribute used to return method information.</typeparam>
		/// <param name="typeName">Name of the type.</param>
		/// <param name="mustHaveAttribute">if set to <c>true</c> [must have attribute].</param>
		/// <returns></returns>
		private List<MethodDefinition> MethodDefinition_GetListByTypeName<T>(string typeName, bool mustHaveAttribute) where T : InformationAttribute {
			List<Type> foundClasses = ClassFinder.GetClassFromPathThatInheritFromType(typeName);
			List<MethodDefinition> retVal = new List<MethodDefinition>();
			foreach (var foundClass in foundClasses) {
				MethodInfo[] methods = foundClass.GetMethods();
				foreach (MethodInfo methodInfo in methods) {

					var item = new MethodDefinition {
						IconCls = methodInfo.GetAttributeValue<IconClsAttribute>("PropertyNameId"),
						MsgCode = methodInfo.GetAttributeValue<T>("MsgCode") ?? foundClass.FullName,
						ClassName = foundClass.FullName,
						AssemblyQualifiedName = foundClass.AssemblyQualifiedName,
						MethodName = methodInfo.Name,
						MsgCodeDescription = methodInfo.GetAttributeValue<T>("MsgCodeDescription") ?? foundClass.AssemblyQualifiedName,
						GridXType = methodInfo.GetAttributeValue<T>("GridXType"),
						StoreXType = methodInfo.GetAttributeValue<T>("StoreXType"),
						KeyEntity = methodInfo.GetAttributeValue<T>("KeyEntity")
					};
					if (!mustHaveAttribute)
						retVal.Add(item);
					if (mustHaveAttribute) {
						if (methodInfo.HasAttribute<T>())
							retVal.Add(item);
					}
				}
			}
			return retVal;
		}

		/// <summary>
		/// Gets all the methods on classes that inherit from the type given that have the display attribute.
		/// </summary>
		/// <param name="typeName">Name of the type.</param>
		/// <returns></returns>
		public JsonStore<MethodDefinition> MethodDefinition_GetStoreByTypeName(string typeName) {
			var retVal = MethodDefinition_GetListByTypeName<StepInformationAttribute>(typeName, true);
			return retVal.ToJsonStore();
		}

		/// <summary>
		/// Saves a crud store for the business entity ObjectClassification.
		/// </summary>
		/// <param name="KeyObject">The key object.</param>
		/// <param name="store">The crud store.</param>
		/// <returns>
		/// A json store containing the value after the save, or a message for each error.
		/// </returns>
		public JsonStore<ObjectClassification> ObjectClassification_SaveStoreByKeyObject(string KeyObject, CrudStore<ClassificationItem> store) {
			ObjectClassificationBroker broker = Helper.CreateInstance<ObjectClassificationBroker>();
			CrudStore<ObjectClassification> crudStore = new CrudStore<ObjectClassification> { create = new List<ObjectClassification>(), destroy = new List<ObjectClassification>() };
			if (store.create != null)
				foreach (var classificationItem in store.create) {
					crudStore.create.Add(new ObjectClassification { KeyClassificationItem = classificationItem.KeyClassificationItem, KeyObject = KeyObject });
				}
			if (store.destroy != null)
				foreach (var classificationItem in store.destroy) {
					crudStore.destroy.Add(broker.GetItemByValues(new ObjectClassification { KeyClassificationItem = classificationItem.KeyClassificationItem, KeyObject = KeyObject }));
				}
			return broker.SaveStore(crudStore);
		}

		/// <summary>
		/// Deletes an existing business entity Occupant.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool Occupant_Delete(Occupant item) {
			OccupantBroker broker = Helper.CreateInstance<OccupantBroker>();
			return broker.Delete(item);
		}

		/// <summary>
		/// Creates a new business entity Occupant and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse Occupant_FormCreate(Occupant item) {
			OccupantBroker broker = Helper.CreateInstance<OccupantBroker>();
			FormResponse response = broker.FormCreate(item);
			return response;
		}


		/// <summary>
		/// Creates a default occupant from user details.
		/// </summary>
		/// <param name="lastName">The last name.</param>
		/// <param name="firstName">The first name.</param>
		/// <param name="userName">The user name.</param>
		/// <param name="constOccupantLocalIdPrefix">The const occupant local id prefix.</param>
		/// <param name="updateExisting">Update Existing.</param>
		/// <returns></returns>
		public Occupant Occupant_CreateFromUser(string lastName, string firstName, string userName, string constOccupantLocalIdPrefix, bool updateExisting = false) {

			// Create or update the Occupant
			var occupantBroker = new OccupantBroker();

			string localId = constOccupantLocalIdPrefix + userName;

			Occupant existingOccupant = occupantBroker.GetItemByLocalId(localId);
			Occupant occupant;

			if (existingOccupant != null) {
				// Update occupant to new information
				if (updateExisting) {
					existingOccupant.FirstName = firstName;
					existingOccupant.LastName = lastName;
					List<Tuple<string, string, string>> changedFields = null;
					occupantBroker.Update(existingOccupant, ref changedFields);
				}
				return existingOccupant;
			}
			// Create the Occupant.
			string organizationKey = GetRootOrganizationKey();


			occupant = occupantBroker.Create(new Occupant {
				FirstName = firstName,
				LastName = lastName,
				Id = localId,
				KeyOrganization = organizationKey
			});

			return occupant;
		}

		/// <summary>
		/// Determines whether the client and the server have the same time.
		/// </summary>
		/// <param name="clientTime"></param>
		/// <returns>
		///   <c>true</c> if the client and the server have the same time. otherwise, <c>false</c>.
		/// </returns>
		public bool IsTimeSynchronized(DateTime clientTime) {
			var now = DateTime.Now;
			var timeDiff = now - clientTime;
			if (Math.Abs(timeDiff.TotalMinutes) > 5) {
				TracingService.Write(TraceEntrySeverity.Warning, "Time not synchronized between client and server. Client time is " + clientTime + " server time is " + now);
				return false;
			}
			return true;
		}

		/// <summary>
		/// Gets the key of the root organization.
		/// </summary>
		/// <returns></returns>
		private string GetRootOrganizationKey() {
			List<TreeNode> organizationTree = Organization_GetTree(" ");

			if (organizationTree.Count == 0) {
				throw new VizeliaException("No organizations found, cannot persist user.");
			}

			return organizationTree[0].Key;
		}

		/// <summary>
		/// Loads a specific item for the business entity Occupant.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse Occupant_FormLoad(string Key) {
			OccupantBroker broker = Helper.CreateInstance<OccupantBroker>();
			return broker.FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity Occupant and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse Occupant_FormUpdate(Occupant item) {
			OccupantBroker broker = Helper.CreateInstance<OccupantBroker>();
			FormResponse response = broker.FormUpdate(item);
			//PsetBroker psetBroker = Helper.CreateInstance<PsetBroker>();
			//if (item.PropertySetList != null && item.PropertySetList.Count > 0) {

			//	psetBroker.Save(response, psets);
			//	psetBroker.CleanObsoleteValues(item.KeyOccupant, item.IfcType, item.KeyClassificationItemWorktype);
			//}
			return response;
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse Occupant_FormUpdateBatch(string[] keys, Occupant item) {
			OccupantBroker broker = Helper.CreateInstance<OccupantBroker>();
			return broker.FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity Occupant.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public List<Occupant> Occupant_GetAll(PagingParameter paging, PagingLocation location, params string[] fields) {
			int total;
			OccupantBroker broker = Helper.CreateInstance<OccupantBroker>();
			return broker.GetAll(paging, location, out total, fields: fields);
		}

		/// <summary>
		/// Gets a specific item for the business entity Occupant.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public Occupant Occupant_GetItem(string Key, params string[] fields) {
			OccupantBroker broker = Helper.CreateInstance<OccupantBroker>();
			return broker.GetItem(Key, fields);
		}

		/// <summary>
		/// Gets the occupant pset.
		/// </summary>
		/// <param name="PsetName">Name of the pset.</param>
		/// <param name="KeyOccupant">The key occupant.</param>
		/// <returns></returns>
		public Dictionary<string, string> Occupant_GetPset(string PsetName, string KeyOccupant) {
			PsetBroker broker = Helper.CreateInstance<PsetBroker>();
			return broker.GetItem(PsetName, KeyOccupant).Attributes.ToDictionary(p => p.Key, p => p.Value);
		}

		/// <summary>
		/// Gets the Occupant store.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="location">The location.</param>
		/// <returns>A json store of the result.</returns>
		public JsonStore<Occupant> Occupant_GetStore(PagingParameter paging, PagingLocation location) {
			OccupantBroker broker = Helper.CreateInstance<OccupantBroker>();
			return broker.GetStore(paging, location);
		}

		/// <summary>
		/// Saves a crud store for the business entity Occupant.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<Occupant> Occupant_SaveStore(CrudStore<Occupant> store) {
			OccupantBroker broker = Helper.CreateInstance<OccupantBroker>();
			return broker.SaveStore(store);
		}



		/// <summary>
		/// Deletes an existing business entity Organization.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool Organization_Delete(Organization item) {
			OrganizationBroker broker = Helper.CreateInstance<OrganizationBroker>();
			return broker.Delete(item);
		}

		/// <summary>
		/// Creates a new business entity Organization and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse Organization_FormCreate(Organization item) {
			OrganizationBroker broker = Helper.CreateInstance<OrganizationBroker>();
			return broker.FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity Organization.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse Organization_FormLoad(string Key) {
			OrganizationBroker broker = Helper.CreateInstance<OrganizationBroker>();
			return broker.FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity Organization and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse Organization_FormUpdate(Organization item) {
			OrganizationBroker broker = Helper.CreateInstance<OrganizationBroker>();
			return broker.FormUpdate(item);
		}

		/// <summary>
		/// Gets a list for the business entity Organization. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public List<Organization> Organization_GetAll(PagingParameter paging, PagingLocation location, params string[] fields) {
			int total;
			OrganizationBroker broker = Helper.CreateInstance<OrganizationBroker>();
			return broker.GetAll(paging, location, out total, isHierarchyEnabled: false, fields: fields); ;
		}

		/// <summary>
		/// Gets a specific item for the business entity Organization.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public Organization Organization_GetItem(string Key, params string[] fields) {
			OrganizationBroker broker = Helper.CreateInstance<OrganizationBroker>();
			return broker.GetItem(Key, fields);
		}

		/// <summary>
		/// Gets a json store for the business entity Organization.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<Organization> Organization_GetStore(PagingParameter paging, PagingLocation location) {
			OrganizationBroker broker = Helper.CreateInstance<OrganizationBroker>();
			return broker.GetStore(paging, location);
		}

		/// <summary>
		/// Generates the json structure of the Organization treeview.
		/// </summary>
		/// <param name="Key">The Key of the parent node.</param>
		/// <returns>a list of treenodes in json.</returns>
		public List<TreeNode> Organization_GetTree(string Key) {
			OrganizationBroker broker = Helper.CreateInstance<OrganizationBroker>();
			if (Key.Trim() == "")
				Key = null;
			List<TreeNode> result = broker.GetChildren(Key).GetTree();
			if (!string.IsNullOrWhiteSpace(Key) && (Key.IndexOf("#", StringComparison.Ordinal) < 0)) {
				return Organization_GetTree(Key, "", 1);
			}
			return result;
		}

		/// <summary>
		/// Generates the json structure of the Organization treeview.
		/// </summary>
		/// <param name="query">The query.</param>
		/// <param name="familly">The familly code of notion.</param>
		/// <param name="level">The level.</param>
		/// <returns>
		/// a list of treenodes in json.
		/// </returns>
		public List<TreeNode> Organization_GetTree(string query, string familly, int level) {
			OrganizationBroker broker = Helper.CreateInstance<OrganizationBroker>();
			//var filtersCode = new List<GridFilter>() { new GridFilter { field = "Code", data = new GridData { type = "string", value = familly } } };
			var jsonClassif = broker.GetStore(new PagingParameter { start = 0, limit = 0, query = query, filter = "Id Name" }, PagingLocation.Database, false);

			List<Filter> classificationDD = m_CoreDataAccess.GetOrganizationHierarchy((from n in jsonClassif.records select n.KeyOrganization).Distinct().ToArray(), HierarchyDirection.ASC);
			var orgHierarchy = broker.GetList(classificationDD.Select(s => s.Key).ToArray()).Where(p => p.KeyParent != null).ToList();

			List<TreeNode> nodes = orgHierarchy.GetTree();

			var keyQuery = orgHierarchy.Where(p => p.KeyParent != null).Select(p => p.KeyOrganization);
			var keyParentQuery = orgHierarchy.Where(p => p.KeyParent != null).Select(p => p.KeyParent).Distinct();
			var extractQuery = keyParentQuery.Except(keyQuery);
			var finalQuery = from p in orgHierarchy
							 where extractQuery.Contains(p.KeyParent)
							 select p;

			if (finalQuery.Any()) {
				var roots = finalQuery.ToList().GetTree();
				var nodeResults = new List<TreeNode>();
				PopulateTree<Organization>(roots, nodes, 2, nodeResults);
				return nodeResults;
			}
			return nodes;
		}


		/// <summary>
		/// Saves a crud store for the business entity Organization.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<Organization> Organization_SaveStore(CrudStore<Organization> store) {
			OrganizationBroker broker = Helper.CreateInstance<OrganizationBroker>();
			return broker.SaveStore(store);
		}

		/// <summary>
		/// Gets the priority assigned to a ClassificationItem.
		/// </summary>
		/// <param name="KeyClassificationChildren">The key classification children.</param>
		/// <param name="KeyClassificationParent">The key classification parent.</param>
		/// <returns></returns>
		public Priority Priority_GetItemByClassification(string KeyClassificationChildren, string KeyClassificationParent) {
			PriorityBroker brokerPriority = Helper.CreateInstance<PriorityBroker>();
			return brokerPriority.GetItemByClassification(KeyClassificationChildren, KeyClassificationParent);
		}

		/// <summary>
		/// Gets the priority assigned to a any upper level of ClassificationItem Hierarchy.
		/// </summary>
		/// <param name="KeyClassificationChildren">The key classification children.</param>
		/// <param name="KeyClassificationParent">The key classification parent.</param>
		/// <returns></returns>
		public Priority Priority_GetItemByClassificationAscendant(string KeyClassificationChildren,
																  string KeyClassificationParent) {
			PriorityBroker brokerPriority = Helper.CreateInstance<PriorityBroker>();
			return brokerPriority.GetItemByClassificationAscendant(KeyClassificationChildren, KeyClassificationParent);
		}

		/// <summary>
		/// Cleans the obsolete values of pset for an object.
		/// </summary>
		/// <param name="KeyObject">The key object.</param>
		/// <param name="UsageName">Name of the usage.</param>
		/// <param name="KeyClassificationItem">The key classification item.</param>
		public void Pset_CleanObsoleteValues(string KeyObject, string UsageName, string KeyClassificationItem) {
			PsetBroker broker = Helper.CreateInstance<PsetBroker>();
			broker.CleanObsoleteValues(KeyObject, UsageName, KeyClassificationItem);
		}

		/// <summary>
		/// Gets a list for the business entity Pset. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public List<Pset> Pset_GetAll(PagingParameter paging, PagingLocation location, params string[] fields) {
			int total;
			var broker = Helper.CreateInstance<PsetBroker>();
			return broker.GetAll(paging, location, out total, fields: fields);
		}

		/// <summary>
		/// Gets a specific item for the business entity Pset.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public Pset Pset_GetItem(string Key, params string[] fields) {
			var broker = Helper.CreateInstance<PsetBroker>();
			return broker.GetItem(Key, fields);
		}

		/// <summary>
		/// Gets the pset for an object.
		/// </summary>
		/// <param name="KeyObjectArray">The key object array.</param>
		/// <returns></returns>
		public List<Pset> Pset_GetList(string[] KeyObjectArray) {
			PsetBroker broker = Helper.CreateInstance<PsetBroker>();
			return broker.GetList(KeyObjectArray);
		}

		/// <summary>
		/// Generates the json structure of the Pset treeview.
		/// </summary>
		/// <param name="Key">The Key of the parent node.</param>
		/// <param name="usageName">The usageName (IfcBuilding, IfcActionRequest etc...) for filtering the results. If null no filter should occur.</param>
		/// <returns>a list of treenodes in json.</returns>
		public List<TreeNode> Pset_GetTree(string Key, string usageName) {
			List<TreeNode> retVal;
			Key = Key.Trim();
			if (String.IsNullOrEmpty(Key)) {
				PsetDefinitionBroker broker = Helper.CreateInstance<PsetDefinitionBroker>();
				List<PsetDefinition> result = broker.GetAll();
				var query = from p in result
							select p;
				// if usageName exists we filter the result.
				if (!String.IsNullOrEmpty(usageName)) {
					query = from p in query
							where p.UsageName == usageName
							select p;
				}
				retVal = query.ToList().GetTree();
			}
			else {
				PsetAttributeDefinitionBroker broker = Helper.CreateInstance<PsetAttributeDefinitionBroker>();
				retVal = broker.GetChildren(Key).GetTree();
			}
			if (retVal != null)
				retVal = retVal.OrderBy(n => n.text).ToList();
			return retVal;


		}

		/// <summary>
		/// Deletes a PsetAttributeDefinition.
		/// </summary>
		/// <param name="item">The pset attribute  definition item to delete</param>
		/// <returns>
		/// True if operation was successfull, false otherwise.
		/// </returns>
		public bool PsetAttributeDefinition_Delete(PsetAttributeDefinition item) {
			PsetAttributeDefinitionBroker broker = Helper.CreateInstance<PsetAttributeDefinitionBroker>();
			return broker.Delete(item);
		}

		/// <summary>
		/// Creates a new PsetAttributeDefinition.
		/// </summary>
		/// <param name="item">The pset attribute  definition item.</param>
		/// <returns>The FormResponse object.</returns>
		public FormResponse PsetAttributeDefinition_FormCreate(PsetAttributeDefinition item) {
			PsetAttributeDefinitionBroker broker = Helper.CreateInstance<PsetAttributeDefinitionBroker>();
			var retval = broker.FormCreate(item);
			return retval;
		}

		/// <summary>
		/// Updates a PsetAttributeDefinition.
		/// </summary>
		/// <param name="item">The pset attribute definition item.</param>
		/// <returns>The FormResponse object.</returns>
		public FormResponse PsetAttributeDefinition_FormUpdate(PsetAttributeDefinition item) {
			PsetAttributeDefinitionBroker broker = Helper.CreateInstance<PsetAttributeDefinitionBroker>();
			FormResponse response = broker.FormUpdate(item);
			return response;
		}

		/// <summary>
		/// Get All PsetAttributeDefinition(s).
		/// </summary>
		/// <param name="paging"></param>
		/// <param name="location"></param>
		/// <param name="fields"></param>
		/// <returns></returns>
		public List<PsetAttributeDefinition> PsetAttributeDefinition_GetAll(PagingParameter paging, PagingLocation location, params string[] fields) {
			int total;
			var broker = Helper.CreateInstance<PsetAttributeDefinitionBroker>();
			var response = broker.GetAll(paging, location, out total, fields: fields);
			return response;
		}

		/// <summary>
		/// Get PseAttributeDefinition Item by key.
		/// </summary>
		/// <param name="key"></param>
		/// <param name="fields"></param>
		/// <returns></returns>
		public PsetAttributeDefinition PsetAttributeDefinition_GetItem(string key, params string[] fields) {
			var broker = Helper.CreateInstance<PsetAttributeDefinitionBroker>();
			var item = broker.GetItem(key, fields);
			return item;
		}

		/// <summary>
		/// Deletes an existing business entity PsetAttributeHistorical.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool PsetAttributeHistorical_Delete(PsetAttributeHistorical item) {
			PsetAttributeHistoricalBroker broker = Helper.CreateInstance<PsetAttributeHistoricalBroker>();
			return broker.Delete(item);
		}

		/// <summary>
		/// Creates a new business entity PsetAttributeHistorical and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse PsetAttributeHistorical_FormCreate(PsetAttributeHistorical item) {
			PsetAttributeHistoricalBroker broker = Helper.CreateInstance<PsetAttributeHistoricalBroker>();
			return broker.FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity PsetAttributeHistorical.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse PsetAttributeHistorical_FormLoad(string Key) {
			PsetAttributeHistoricalBroker broker = Helper.CreateInstance<PsetAttributeHistoricalBroker>();
			return broker.FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity PsetAttributeHistorical and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse PsetAttributeHistorical_FormUpdate(PsetAttributeHistorical item) {
			PsetAttributeHistoricalBroker broker = Helper.CreateInstance<PsetAttributeHistoricalBroker>();
			return broker.FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse PsetAttributeHistorical_FormUpdateBatch(string[] keys, PsetAttributeHistorical item) {
			PsetAttributeHistoricalBroker broker = Helper.CreateInstance<PsetAttributeHistoricalBroker>();
			return broker.FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity PsetAttributeHistorical. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public List<PsetAttributeHistorical> PsetAttributeHistorical_GetAll(PagingParameter paging, PagingLocation location, params string[] fields) {
			int total;
			PsetAttributeHistoricalBroker broker = Helper.CreateInstance<PsetAttributeHistoricalBroker>();
			return broker.GetAll(paging, location, out total, fields: fields); ;
		}

		/// <summary>
		/// Gets a dictionary of all business entities PsetAttributeHistorical. 
		/// </summary>
		/// <returns></returns>
		public Dictionary<string, PsetAttributeHistorical> PsetAttributeHistorical_GetDictionary() {
			PsetAttributeHistoricalBroker broker = Helper.CreateInstance<PsetAttributeHistoricalBroker>();
			var retVal = broker.GetDictionary();
			return retVal;
		}

		/// <summary>
		/// Gets a specific item for the business entity PsetAttributeHistorical.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public PsetAttributeHistorical PsetAttributeHistorical_GetItem(string Key, params string[] fields) {
			PsetAttributeHistoricalBroker broker = Helper.CreateInstance<PsetAttributeHistoricalBroker>();
			return broker.GetItem(Key, fields);
		}

		/// <summary>
		/// Gets a json store for the business entity PsetAttributeHistorical.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<PsetAttributeHistorical> PsetAttributeHistorical_GetStore(PagingParameter paging, PagingLocation location) {
			PsetAttributeHistoricalBroker broker = Helper.CreateInstance<PsetAttributeHistoricalBroker>();
			return broker.GetStore(paging, location);
		}

		/// <summary>
		/// Gets a store of PsetAttributeHistorical for a specific Object.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyObject">The key object.</param>
		/// <param name="KeyPropertySingleValue">The key property single value.</param>
		/// <returns>
		/// A JsonStore.
		/// </returns>
		public JsonStore<PsetAttributeHistorical> PsetAttributeHistorical_GetStoreByKeyObject(PagingParameter paging,
																							  string KeyObject,
																							  string KeyPropertySingleValue) {
			PsetAttributeHistoricalBroker broker = Helper.CreateInstance<PsetAttributeHistoricalBroker>();
			return broker.GetStoreByKeyObject(paging, KeyObject, KeyPropertySingleValue);
		}

		/// <summary>
		/// Saves a crud store for the business entity PsetAttributeHistorical.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<PsetAttributeHistorical> PsetAttributeHistorical_SaveStore(CrudStore<PsetAttributeHistorical> store) {
			PsetAttributeHistoricalBroker broker = Helper.CreateInstance<PsetAttributeHistoricalBroker>();
			return broker.SaveStore(store);
		}

		/// <summary>
		/// Deletes a PsetDefinition
		/// </summary>
		/// <param name="item">Tthe item to delete.</param>
		/// <returns>True if operation was successfull, false otherwise.</returns>
		public bool PsetDefinition_Delete(PsetDefinition item) {
			PsetDefinitionBroker broker = Helper.CreateInstance<PsetDefinitionBroker>();
			return broker.Delete(item);
		}

		/// <summary>
		/// Creates a new PsetDefinition.
		/// </summary>
		/// <param name="item">The pset definition item.</param>
		/// <returns>The FormResponse object.</returns>
		public FormResponse PsetDefinition_FormCreate(PsetDefinition item) {
			PsetDefinitionBroker broker = Helper.CreateInstance<PsetDefinitionBroker>();
			var retval = broker.FormCreate(item);
			return retval;
		}

		/// <summary>
		/// Updates a PsetDefinition.
		/// </summary>
		/// <param name="item">The pset definition item.</param>
		/// <returns>The FormResponse object.</returns>
		public FormResponse PsetDefinition_FormUpdate(PsetDefinition item) {
			PsetDefinitionBroker broker = Helper.CreateInstance<PsetDefinitionBroker>();
			FormResponse response = broker.FormUpdate(item);
			return response;
		}

		/// <summary>
		/// Get All the PsetDefinition(s).
		/// </summary>
		/// <param name="paging"></param>
		/// <param name="location"></param>
		/// <param name="fields"></param>
		/// <returns></returns>
		public List<PsetDefinition> PsetDefinition_GetAll(PagingParameter paging, PagingLocation location, params string[] fields) {
			int total;
			var broker = Helper.CreateInstance<PsetDefinitionBroker>();
			var response = broker.GetAll(paging, location, out total, fields: fields);
			return response;
		}

		/// <summary>
		/// Get PsetDefinition Item by key.
		/// </summary>
		/// <param name="key"></param>
		/// <param name="fields"></param>
		/// <returns></returns>
		public PsetDefinition PsetDefinition_GetItem(string key, params string[] fields) {
			var broker = Helper.CreateInstance<PsetDefinitionBroker>();
			var item = broker.GetItem(key, fields);
			return item;
		}

		/// <summary>
		/// Gets the pset definition list corresponding to a specific usage name.
		/// </summary>
		/// <param name="UsageName">Name of the usage.</param>
		/// <param name="KeyClassificationItem">The KeyClassificationItem (optional).</param>
		/// <returns></returns>
		public List<PsetDefinition> PsetDefinition_GetListByUsageName(string UsageName, string KeyClassificationItem) {
			PsetDefinitionBroker broker = Helper.CreateInstance<PsetDefinitionBroker>();
			List<PsetDefinition> results = String.IsNullOrEmpty(KeyClassificationItem) ? broker.GetListByUsageName(UsageName) : broker.GetListHerited(UsageName, KeyClassificationItem);
			return results;
		}


		/// <summary>
		/// Gets the pset definition store corresponding to a specific usage name.
		/// </summary>
		/// <param name="UsageName">Name of the usage.</param>
		/// <param name="KeyClassificationItem">The KeyClassificationItem (optional).</param>
		/// <returns></returns>
		public JsonStore<PsetDefinition> PsetDefinition_GetStoreByUsageName(string UsageName, string KeyClassificationItem) {
			var retVal = PsetDefinition_GetListByUsageName(UsageName, KeyClassificationItem);
			return retVal.ToJsonStore();
		}

		/// <summary>
		/// Gets the pset list specified.
		/// </summary>
		/// <param name="listName">The name of the psetlist to retreive.</param>
		/// <param name="paging">The paging parameter.</param>
		/// <returns></returns>
		public JsonStore<ListElement> PsetListElement_GetStore(string listName, PagingParameter paging) {
			ListElementBroker broker = Helper.CreateInstance<ListElementBroker>(listName);
			JsonStore<ListElement> result = broker.GetPsetStore(paging, PagingLocation.Memory);
			return result;
		}

		/// <summary>
		/// Saves a Pset List.
		/// </summary>
		/// <param name="listName">The name of the list to save.</param>
		/// <param name="store">The crud store of the list.</param>
		/// <returns></returns>
		public JsonStore<ListElement> PsetListElement_SaveStore(string listName, CrudStore<ListElement> store) {
			ListElementBroker broker = Helper.CreateInstance<ListElementBroker>(listName);
			return broker.SaveStore(store);
		}

		/// <summary>
		/// Deletes an existing business entity Site.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool Site_Delete(Site item) {
			SiteBroker broker = Helper.CreateInstance<SiteBroker>();
			return broker.Delete(item);
		}

		/// <summary>
		/// Creates a new business entity Site and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse Site_FormCreate(Site item) {
			SiteBroker broker = Helper.CreateInstance<SiteBroker>();
			FormResponse response = broker.FormCreate(item);
			return response;
		}

		/// <summary>
		/// Loads a specific item for the business entity Site.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse Site_FormLoad(string Key) {
			SiteBroker broker = Helper.CreateInstance<SiteBroker>();
			return broker.FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity Site and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse Site_FormUpdate(Site item) {
			SiteBroker broker = Helper.CreateInstance<SiteBroker>();
			return broker.FormUpdate(item);
		}

		/// <summary>
		/// Returns children for a specific site.
		/// </summary>
		/// <param name="KeySite">The key of the parent site.</param>
		/// <returns>A list of site entities.</returns>
		private List<Site> Site_GetChildren(string KeySite) {
			SiteBroker broker = Helper.CreateInstance<SiteBroker>();
			return broker.GetChildren(KeySite);
		}

		/// <summary>
		/// Returns a specific site.
		/// </summary>
		/// <param name="KeySite">The key of site.</param>
		/// <returns>A site entity.</returns>
		private Site Site_GetItem(string KeySite) {
			SiteBroker broker = Helper.CreateInstance<SiteBroker>();
			return broker.GetItem(KeySite);

		}

		/// <summary>
		/// Returns a list a specific sites.
		/// </summary>
		/// <param name="Keys">The keys of the sites.</param>
		/// <returns>A list of site entities.</returns>
		private List<Site> Site_GetList(string[] Keys) {
			SiteBroker broker = Helper.CreateInstance<SiteBroker>();
			return broker.GetList(Keys);
		}

		/// <summary>
		/// Return the max level of sites in the spatial hierarchy.
		/// </summary>
		/// <returns></returns>
		public int Site_GetMaxLevel() {
			SiteBroker broker = Helper.CreateInstance<SiteBroker>();
			var retVal = broker.GetMaxLevel();
			return retVal;
		}

		/// <summary>
		/// Gets a json store for the business entity Site.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		[PrincipalPermission(SecurityAction.Demand, Role = "Vizelia.FOL.WCFService.CoreWCF.PsetListElement_SaveStore")]
		public JsonStore<Site> Site_GetStore(PagingParameter paging, PagingLocation location) {
			SiteBroker broker = Helper.CreateInstance<SiteBroker>();
			return broker.GetStore(paging, location);
		}

		/// <summary>
		/// Saves a crud store for the business entity Site.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<Site> Site_SaveStore(CrudStore<Site> store) {
			SiteBroker broker = Helper.CreateInstance<SiteBroker>();
			return broker.SaveStore(store);
		}

		/// <summary>
		/// Deletes an existing business entity Space.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool Space_Delete(Space item) {
			SpaceBroker broker = Helper.CreateInstance<SpaceBroker>();
			return broker.Delete(item);
		}

		/// <summary>
		/// Creates a new business entity Space and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse Space_FormCreate(Space item) {
			SpaceBroker broker = Helper.CreateInstance<SpaceBroker>();
			return broker.FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity Space.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse Space_FormLoad(string Key) {
			SpaceBroker broker = Helper.CreateInstance<SpaceBroker>();
			return broker.FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity Space and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse Space_FormUpdate(Space item) {
			SpaceBroker broker = Helper.CreateInstance<SpaceBroker>();
			return broker.FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse Space_FormUpdateBatch(string[] keys, Space item) {
			SpaceBroker broker = Helper.CreateInstance<SpaceBroker>();
			return broker.FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity Space. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public List<Space> Space_GetAll(PagingParameter paging, PagingLocation location, params string[] fields) {
			int total;
			SpaceBroker broker = Helper.CreateInstance<SpaceBroker>();
			return broker.GetAll(paging, location, out total, fields: fields);
		}

		/// <summary>
		/// Returns children of a specific building storey.
		/// </summary>
		/// <param name="KeyBuildingStorey">The key of the parent building storey.</param>
		/// <returns>A list of space entities.</returns>
		private List<Space> Space_GetChildren(string KeyBuildingStorey) {
			SpaceBroker broker = Helper.CreateInstance<SpaceBroker>();
			return broker.GetChildren(KeyBuildingStorey);
		}

		/// <summary>
		/// Gets a specific item for the business entity Space.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public Space Space_GetItem(string Key, params string[] fields) {
			SpaceBroker broker = Helper.CreateInstance<SpaceBroker>();
			return broker.GetItem(Key, fields);
		}

		/// <summary>
		/// Gets a json store for the business entity Space.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<Space> Space_GetStore(PagingParameter paging, PagingLocation location) {
			SpaceBroker broker = Helper.CreateInstance<SpaceBroker>();
			return broker.GetStore(paging, location);
		}

		/// <summary>
		/// Saves a crud store for the business entity Space.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<Space> Space_SaveStore(CrudStore<Space> store) {
			SpaceBroker broker = Helper.CreateInstance<SpaceBroker>();
			return broker.SaveStore(store);
		}

		/// <summary>
		/// Generates the json structure of the Spatial treeview.
		/// </summary>
		/// <param name="Key">The Key of the parent node.</param>
		/// <param name="flgFilter">True to filter the result, false otherwise.</param>
		/// <param name="ExcludeMetersAndAlarms">if set to <c>true</c> [filter meters and alarms out of the response tree].</param>
		/// <returns>
		/// a list of treenodes in json.
		/// </returns>
		public List<TreeNode> Spatial_GetTree(string Key, bool flgFilter, bool ExcludeMetersAndAlarms) {
			List<TreeNode> nodes = new List<TreeNode>();
			if (!string.IsNullOrWhiteSpace(Key) && (Key.IndexOf("#", StringComparison.Ordinal) < 0)) {
				nodes = Spatial_GetTree(Key);
				return nodes;
			}

			string typeName = GetTypeNameFromRecord(Key);
			switch (typeName) {
				// when an null or undefined Key is passed we assume we need to get the root nodes.
				case "":
					nodes.AddRange(Site_GetChildren(null).GetTree());
					break;
				case "IfcSite":
					nodes.AddRange(Site_GetChildren(Key).GetTree());
					nodes.AddRange(Building_GetChildren(Key).GetTree());
					break;
				case "IfcBuilding":
					nodes.AddRange(Building_GetChildren(Key).GetTree());
					nodes.AddRange(BuildingStorey_GetChildren(Key).GetTree());
					break;
				case "IfcBuildingStorey":
					nodes.AddRange(Space_GetChildren(Key).GetTree());
					break;
				case "IfcSpace":
					break;
				default:
					nodes.AddRange(Site_GetChildren(Key).GetTree());
					break;
			}

			nodes.AddRange(Furniture_GetChildren(Key).GetTree());

			List<Filter> filters = FilterHelper.GetFilter<Location>();
			// If we would like to return an empty result if no filter spatial was associated with the user, we should not check Count > 0
			if (flgFilter && filters != null && filters.Count > 0) {

				var query = from n in nodes
							join p in filters on n.id equals p.Key
							select n.SetFilterType(p.Type);
				nodes = query.Distinct().ToList();

				// filter on the requested node.
				Filter currentFilter = (from f in filters where f.Key == Key select f).FirstOrDefault();
				if (currentFilter != null && currentFilter.Type != FilterType.Ascendant && ExcludeMetersAndAlarms == false) {
					nodes.AddRange(Meter_GetListByLocation(Key, 500).GetTree());
					nodes.AddRange(AlarmDefinition_GetListByLocation(Key, 500).GetTree());
				}
			}
			else if (ExcludeMetersAndAlarms == false) {
				nodes.AddRange(Meter_GetListByLocation(Key, 500).GetTree());
				nodes.AddRange(AlarmDefinition_GetListByLocation(Key, 500).GetTree());
			}

			return nodes;
		}

		/// <summary>
		/// Generates the json structure of the Spatial treeview.
		/// </summary>
		/// <param name="query">The query.</param>
		/// <returns>
		/// a list of treenodes in json.
		/// </returns>
		public List<TreeNode> Spatial_GetTree(string query) {
			List<TreeNode> nodes;
			List<Filter> filters = FilterHelper.GetFilter<Location>();


			LocationBroker broker = Helper.CreateInstance<LocationBroker>();
			var jsonLocation = broker.GetAllPagingByKeyword(new PagingParameter { start = 0, limit = 0 }, query);
			//var jsonLocation = broker.GetStore(new PagingParameter { start = 0, limit = 0, query = query, filter = "Name Description LocalId" }, PagingLocation.Database, false);

			MeterBroker meterBroker = Helper.CreateInstance<MeterBroker>();
			var meters = meterBroker.GetStore(new PagingParameter { start = 0, limit = 0, query = query, filter = "Name Description LocalId" }, PagingLocation.Database, false).records;

			AlarmDefinitionBroker alarmBroker = Helper.CreateInstance<AlarmDefinitionBroker>();
			var alarms = alarmBroker.GetStore(new PagingParameter { start = 0, limit = 0, query = query, filter = "Title Description LocalId" }, PagingLocation.Database, false).records;


			var locations = Location_GetSpatialHierarchy((from n in jsonLocation.records select n.KeyLocation).Union(from m in meters select m.KeyLocation).Union(from m in alarms select m.KeyLocation).Distinct().ToArray());

			// If we would like to return an empty result if no filter spatial was associated with the user, we should not check Count > 0
			if (filters != null && filters.Count > 0) {
				var nodeQuery = from n in locations.GetTree()
								join p in filters on n.id equals p.Key
								select n.SetFilterType(p.Type);
				nodes = nodeQuery.ToList();
			}
			else {
				nodes = locations.GetTree();
			}

			var fullnodes = (from n in nodes select n).Union(from m in meters.GetTree() select m).Union(from a in alarms.GetTree() select a);

			var finalQuery = locations.Where(p => p.KeyParent == null);
			if (!finalQuery.Any()) {
				var keyQuery = locations.Where(p => p.KeyParent != null).Select(p => p.KeyLocation);
				var keyParentQuery = locations.Where(p => p.KeyParent != null).Select(p => p.KeyParent).Distinct();
				var extractQuery = keyParentQuery.Except(keyQuery);
				finalQuery = from p in locations
							 where extractQuery.Contains(p.KeyParent)
							 select p;
			}
			if (finalQuery.Any()) {
				var roots = finalQuery.ToList().GetTree();
				PopulateTree<Location>(roots, fullnodes.ToList());
				return roots;
			}

			return nodes;
		}

		/// <summary>
		/// Populates the tree.
		/// </summary>
		/// <param name="curNodes">The cur nodes.</param>
		/// <param name="allNodes">The locations.</param>
		/// <returns></returns>
		private List<TreeNode> PopulateTree<T>(List<TreeNode> curNodes, List<TreeNode> allNodes) {
			foreach (var curNode in curNodes) {
				var query = from n in allNodes
							where GetLocationKeyParent(n) == curNode.Key
							select n;
				foreach (var node in query.ToList()) {
					PopulateTree<T>(new List<TreeNode> { node }, allNodes);
					curNode.AddChild(node);
				}
			}
			return curNodes;
		}

		/// <summary>
		/// Gets the location key parent for the entity.
		/// </summary>
		/// <param name="node">The tree node.</param>
		/// <returns></returns>
		private string GetLocationKeyParent(TreeNode node) {
			if (node == null)
				return string.Empty;
			if (node.entity == null)
				return string.Empty;
			if ((node.entity as Meter) != null)
				return (node.entity as Meter).KeyLocation;
			else if ((node.entity as AlarmDefinition) != null)
				return (node.entity as AlarmDefinition).KeyLocation;
			else if ((node.entity as IHierarchy) != null)
				return (node.entity as IHierarchy).KeyParent;
			else
				return string.Empty;
		}
		/// <summary>
		/// Populates the tree by tree level.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="curNodes">The cur nodes.</param>
		/// <param name="allNodes">The locations.</param>
		/// <param name="level">The level.</param>
		/// <param name="resultNodes">The result nodes.</param>
		/// <returns></returns>
		private List<TreeNode> PopulateTree<T>(List<TreeNode> curNodes, List<TreeNode> allNodes, int level, List<TreeNode> resultNodes) {
			var innerLevel = level - 1;
			foreach (var curNode in curNodes) {
				var query = from n in allNodes
							where GetLocationKeyParent(n) == curNode.Key
							select n;
				foreach (var node in query.ToList()) {
					PopulateTree<T>(new List<TreeNode> { node }, allNodes, innerLevel, resultNodes);
					curNode.AddChild(node);
				}
			}
			if (innerLevel == -1) {
				resultNodes.AddRange(curNodes);
			}
			return curNodes;
		}

		/// <summary>
		/// Gets a json store of locations found by keyword.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="keyword">The keyword.</param>
		/// <returns>
		/// A json store.
		/// </returns>
		public JsonStore<Location> Spatial_GetStoreByKeyword(PagingParameter paging, string keyword) {
			LocationBroker broker = Helper.CreateInstance<LocationBroker>();
			var result = broker.GetAllPagingByKeyword(paging, paging.query);
			var retVal = new List<Location>();
			List<Filter> filters = FilterHelper.GetFilter<Location>();
			// If we would like to return an empty result if no filter spatial was associated with the user, we should not check Count > 0
			if (filters != null && filters.Count > 0) {
				var query = from n in result.records
							join p in filters on n.KeyLocation equals p.Key
							select n;
				retVal.AddRange(query.ToList());
				query = from n in result.records
						join p in filters on n.KeyParent equals p.Key
						select n;
				retVal.AddRange(query.ToList());
				result.records = retVal;
			}

			return result;
		}


		/// <summary>
		/// Creates a new business entity TaskScheduler and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse TaskScheduler_FormCreate(TaskScheduler item) {
			FormResponse response = new FormResponse { success = false };

			ChartSchedulerBroker broker = Helper.CreateInstance<ChartSchedulerBroker>();
			throw new NotImplementedException();
		}

		/// <summary>
		/// Gets the store of time zones.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <returns></returns>
		public JsonStore<ListElement> TimeZone_GetStore(PagingParameter paging) {
			var timeZones = TimeZoneInfo.GetSystemTimeZones();
			var result = (from timeZone in timeZones
						  select new ListElement {
							  Id = timeZone.Id,
							  Value = timeZone.BaseUtcOffset.TotalMinutes.ToString(CultureInfo.InvariantCulture),
							  IconCls = timeZone.BaseUtcOffset.ToString(),
							  MsgCode = TimeZoneHelper.GetLocalizedDisplayName(timeZone)
						  });
			if (!String.IsNullOrEmpty(paging.query))
				result = result.Where(p => p.Label.ToLower().Contains(paging.query.ToLower()));
			return result.ToList().RankToJson(paging.sort, paging.dir, paging.start, paging.limit);
		}

		/// <summary>
		/// Deletes an existing business entity TraceEntry.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool TraceEntry_Delete(TraceEntry item) {
			TraceEntryBroker broker = Helper.CreateInstance<TraceEntryBroker>();
			return broker.Delete(item);
		}

		/// <summary>
		/// Gets a json store for the business entity TraceEntry.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<TraceEntry> TraceEntry_GetStore(PagingParameter paging, PagingLocation location) {
			TraceEntryBroker broker = Helper.CreateInstance<TraceEntryBroker>();
			return broker.GetStore(paging, location);
		}

		/// <summary>
		/// Gets a json store for the business entity TraceSummary.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="location">The location.</param>
		/// <returns></returns>
		public JsonStore<TraceSummary> TraceSummary_GetStore(PagingParameter paging, PagingLocation location) {
			var broker = Helper.CreateInstance<TraceSummaryBroker>();
			var retVal = broker.GetAll();
			return retVal.ToJsonStore(retVal.Count, paging);
		}

		/// <summary>
		/// Gets a store of TraceEntries for a specific request id.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="requestId">The request id.</param>
		/// <returns></returns>
		public JsonStore<TraceEntry> TraceEntry_GetStoreByRequestId(PagingParameter paging, string requestId) {
			TraceEntryBroker broker = Helper.CreateInstance<TraceEntryBroker>();
			return broker.GetStoreByRequestId(paging, requestId);
		}

		/// <summary>
		/// Get a store of TraceEntries for a specific job run instance request id.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="jobRunInstanceRequestId">The job run instance request id.</param>
		/// <returns></returns>
		public JsonStore<TraceEntry> TraceEntry_GetStoreByJobRunInstanceRequestId(PagingParameter paging, string jobRunInstanceRequestId) {
			TraceEntryBroker broker = Helper.CreateInstance<TraceEntryBroker>();
			return broker.GetStoreByJobRunInstanceRequestId(paging, jobRunInstanceRequestId);
		}

		/// <summary>
		/// Get a store of TraceEntries for a specific job run instance request id.
		/// </summary>
		/// <param name="jobRunInstanceRequestId">The job run instance request id.</param>
		/// <returns></returns>
		public JsonStore<TraceEntry> TraceEntry_GetStoreByJobRunInstanceRequestId(string jobRunInstanceRequestId) {
			TraceEntryBroker broker = Helper.CreateInstance<TraceEntryBroker>();
			return broker.GetStoreByJobRunInstanceRequestId(jobRunInstanceRequestId);
		}

		/// <summary>
		/// Jobs the instance_ get store by job id.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="keyJob">The key job.</param>
		/// <returns></returns>
		public JsonStore<JobInstance> JobInstance_GetStoreByJobId(PagingParameter paging, string keyJob) {
			TraceEntryBroker broker = Helper.CreateInstance<TraceEntryBroker>();
			JsonStore<JobInstance> result = broker.GetStoreByJobId(paging, keyJob);

			foreach (var jobInstance in result.records) {
				jobInstance.Duration = (jobInstance.EndDate - jobInstance.StartDate).ToString();
				jobInstance.FirstErrorMessage = string.Empty;
			}
			return result;

		}



		/// <summary>
		/// Gets the TraceEntry for an exception that occured during the request specified by the request id.
		/// </summary>
		/// <param name="requestId">The request id.</param>
		/// <returns></returns>
		public TraceEntry TraceEntry_GetErrorEntryByRequestId(string requestId) {
			TraceEntryBroker broker = Helper.CreateInstance<TraceEntryBroker>();
			return broker.GetErrorEntryByRequestId(requestId);
		}

		/// <summary>
		/// Saves a crud store for the business entity TraceEntry.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<TraceEntry> TraceEntry_SaveStore(CrudStore<TraceEntry> store) {
			TraceEntryBroker broker = Helper.CreateInstance<TraceEntryBroker>();
			return broker.SaveStore(store);
		}

		/// <summary>
		/// Updates the long running operation.
		/// </summary>
		/// <param name="operationId">The operation id.</param>
		/// <param name="clientRequest">The client request.</param>
		public void UpdateLongRunningOperation(Guid operationId, LongRunningOperationClientRequest clientRequest) {
			LongRunningOperationService.UpdateOperation(operationId, clientRequest);
		}

		/// <summary>
		/// Creates UserProfilePreference entity.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <returns></returns>
		public FormResponse UserProfilePreference_FormCreate(UserProfilePreference item) {
			var broker = Helper.CreateInstance<UserProfilePreferenceBroker>();
			return broker.FormCreate(item);
		}

		/// <summary>
		/// Updates UserProfilePreference entity.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <returns></returns>
		public FormResponse UserProfilePreference_FormUpdate(UserProfilePreference item) {
			var broker = Helper.CreateInstance<UserProfilePreferenceBroker>();
			return broker.FormUpdate(item);
		}

		/// <summary>
		/// Deletes UserProfilePreference entity.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <returns></returns>
		public bool UserProfilePreference_Delete(UserProfilePreference item) {
			var broker = Helper.CreateInstance<UserProfilePreferenceBroker>();
			return broker.Delete(item);
		}

		/// <summary>
		/// Get list of UserProfilePreference for the current user.
		/// </summary>
		/// <param name="userName">Name of the user.</param>
		/// <returns></returns>
		public List<UserProfilePreference> UserProfilePreference_GetListByUser(string userName = null) {
			var broker = Helper.CreateInstance<UserProfilePreferenceBroker>();
			var retVal = broker.GetListByKeyUser(userName ?? Helper.GetCurrentUserName());
			return retVal;
		}

		/// <summary>
		/// Deletes all UserProfilePreference data for the current user.
		/// </summary>
		public bool UserProfilePreference_DeleteAllData() {
			var brokerUserProfilePreferences = Helper.CreateInstance<UserProfilePreferenceBroker>();
			var brokerFOLMembershipUser = Helper.CreateInstance<FOLMembershipUserBroker>();
			var currentUser = Helper.GetCurrentFOLMembershipUser();
			var userName = currentUser.UserName;
			brokerFOLMembershipUser.Save(currentUser);
			var retVal = brokerUserProfilePreferences.DeleteAllDataByKeyUser(userName);
			return retVal;
		}

		/// <summary>
		/// Gets a specific item for the business entity UserProfilePreference.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public UserProfilePreference UserProfilePreference_GetItem(string Key, params string[] fields) {
			var broker = Helper.CreateInstance<UserProfilePreferenceBroker>();
			return broker.GetItem(Key, fields);
		}

		/// <summary>
		/// Get list of UserProfilePreference for the current user.
		/// Used by OData API with dynamic method invocation therfore using GetAll naming convention and parameters.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="location">The location.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public List<UserProfilePreference> UserProfilePreference_GetAll(PagingParameter paging, PagingLocation location, params string[] fields) {
			return UserProfilePreference_GetListByUser();
		}

		/// <summary>
		/// Sets the preference key and value for the user
		/// </summary>
		/// <param name="preferenceKey">The preference key.</param>
		/// <param name="preferenceValue">The preference value.</param>
		/// <param name="userName">Name of the user, Set to String.Empty to aplly globally.</param>
		public void UserProfilePreference_Update(string preferenceKey, string preferenceValue, string userName = null) {
			userName = userName ?? Helper.GetCurrentUserName();
			var broker = Helper.CreateInstance<UserProfilePreferenceBroker>();
			broker.FormUpdate(new UserProfilePreference { KeyUser = userName, PreferenceKey = preferenceKey, PreferenceValue = preferenceValue });
		}

		/// <summary>
		/// Sets the preference key and value for the tenant
		/// </summary>
		/// <param name="preferenceKey">The preference key.</param>
		/// <param name="preferenceValue">The preference value.</param>
		public void UserProfilePreference_UpdateGlobal(string preferenceKey, string preferenceValue) {
			UserProfilePreference_Update(preferenceKey, preferenceValue, String.Empty);
		}

		/// <summary>
		/// Gets a specific item for the business entity UserPreferences.
		/// </summary>
		/// <param name="key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse UserPreferences_FormLoad(string key) {
			var broker = Helper.CreateInstance<UserPreferencesBroker>();
			return broker.FormLoad(key);
		}

		/// <summary>
		/// Creates UserPreferences entity.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <returns></returns>
		public FormResponse UserPreferences_FormCreate(UserPreferences item) {
			var broker = Helper.CreateInstance<UserPreferencesBroker>();
			return broker.FormCreate(item);
		}

		/// <summary>
		/// Updates UserPreferences entity.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <returns></returns>
		public FormResponse UserPreferences_FormUpdate(UserPreferences item) {
			var broker = Helper.CreateInstance<UserPreferencesBroker>();
			return broker.FormUpdate(item);
		}

		/// <summary>
		/// Deletes UserPreferences entity.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <returns></returns>
		public bool UserPreferences_Delete(UserPreferences item) {
			var broker = Helper.CreateInstance<UserPreferencesBroker>();
			return broker.Delete(item);
		}

		/// <summary>
		/// Gets the differents versions of the frameworks used encapsulated in a business entity.
		/// </summary>
		/// <returns></returns>
		public Versions Versions_GetItem() {
			return new Versions {
				Extjs = Convert.ToString(VizeliaConfiguration.Instance.Script.Extjs),
				Vizjs = Convert.ToString(VizeliaConfiguration.Instance.Script.Vizjs)
			};
		}

		/// <summary>
		/// Purges the server logs.
		/// </summary>
		public void PurgeLogs() {
			int daysToKeepLogs = VizeliaConfiguration.Instance.DaysToKeepLogs;
			string message = string.Format("Purging log and trace items older than {0} days", daysToKeepLogs);
			TracingService.Trace("PurgeLogs", string.Empty, message, () => PurgeLogs(daysToKeepLogs));
		}

		private void PurgeLogs(int daysToKeepLogs) {
			var loggingBroker = Helper.CreateInstance<LoggingBroker>();
			loggingBroker.Purge(daysToKeepLogs);

			var traceEntryBroker =
				Helper.CreateInstance<TraceEntryBroker>();
			traceEntryBroker.Purge(daysToKeepLogs);
		}

		/// <summary>
		/// Tenant_s the send completion email.
		/// </summary>
		/// <param name="password">The password.</param>
		/// <param name="tenant">The tenant.</param>
		public void Tenant_SendCompletionEmail(string password, Tenant tenant) {
			var mailBroker = Helper.CreateInstance<MailBroker>();
			List<Mail> mails = mailBroker.GetListByClassificationItemLocalId(MailBroker.const_inittenant);
			var mailMessages = MailService.ConvertMailsToMailMessages(new ModelDataInitTenant { Tenant = tenant, Password = password }, mails);
			foreach (var mailMessage in mailMessages) {
				MailService.Send(mailMessage);
			}
		}

		/// <summary>
		/// Deletes an existing business entity AuditEntity.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool AuditEntity_Delete(AuditEntity item) {
			var broker = Helper.CreateInstance<AuditEntityBroker>();
			return broker.Delete(item);
		}

		/// <summary>
		/// Creates a new business entity AuditEntity and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse AuditEntity_FormCreate(AuditEntity item) {
			var broker = Helper.CreateInstance<AuditEntityBroker>();
			return broker.FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity AuditEntity.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse AuditEntity_FormLoad(string Key) {
			var broker = Helper.CreateInstance<AuditEntityBroker>();
			return broker.FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity AuditEntity and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse AuditEntity_FormUpdate(AuditEntity item) {
			var broker = Helper.CreateInstance<AuditEntityBroker>();
			return broker.FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse AuditEntity_FormUpdateBatch(string[] keys, AuditEntity item) {
			var broker = Helper.CreateInstance<AuditEntityBroker>();
			return broker.FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity AuditEntity. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public List<AuditEntity> AuditEntity_GetAll(PagingParameter paging, PagingLocation location, params string[] fields) {
			int total;
			var broker = Helper.CreateInstance<AuditEntityBroker>();
			return broker.GetAll(paging, location, out total, fields: fields);
		}

		/// <summary>
		/// Gets a specific item for the business entity AuditEntity.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public AuditEntity AuditEntity_GetItem(string Key, params string[] fields) {
			var broker = Helper.CreateInstance<AuditEntityBroker>();
			return broker.GetItem(Key, fields);
		}

		/// <summary>
		/// Get store of AuditEntity.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="location">The location.</param>
		/// <returns></returns>
		public JsonStore<AuditEntity> AuditEntity_GetStore(PagingParameter paging, PagingLocation location) {
			var auditEntityBroker = Helper.CreateInstance<AuditEntityBroker>();
			var retval = auditEntityBroker.GetStore(paging, location);
			return retval;
		}

		/// <summary>
		/// Saves a crud store for the business entity AuditEntity.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<AuditEntity> AuditEntity_SaveStore(CrudStore<AuditEntity> store) {
			var auditEntityBroker = Helper.CreateInstance<AuditEntityBroker>();
			var retval = auditEntityBroker.SaveStore(store);
			return retval;
		}

		/// <summary>
		/// Get all AuditEntity aggregated.
		/// </summary>
		/// <returns></returns>
		public JsonStore<AuditEntity> AuditEntity_GetAllAggregated() {
			var auditEntityBroker = Helper.CreateInstance<AuditEntityBroker>();
			var retval = auditEntityBroker.GetAllAggregated();
			return retval;
		}

		/// <summary>
		/// Get all Auditable entities.
		/// </summary>
		/// <returns></returns>
		public JsonStore<ListElement> AuditableEntity_GetStore() {
			var baseBusinessEntitiyType = typeof(BaseBusinessEntity);
			var retVal = (from type in baseBusinessEntitiyType.Assembly.GetTypes()
						  where type.GetCustomAttributes(typeof(AuditableEntityAttribute), true).Length > 0 && type.Name != "AuthorizationItem"
						  select new ListElement {
							  Value = type.Name,
							  IconCls = Helper.GetAttributeValue<IconClsAttribute>(type),
							  MsgCode = Langue.ResourceManager.GetString(Helper.GetAttributeValue<LocalizedTextAttribute>(type))
						  }).ToList();
			var authorizationItemList = new List<ListElement> { 
				new ListElement { Value = "AuthorizationItem.Operation", IconCls = AuthorizationItem.IconClsOperation, MsgCode = Langue.ResourceManager.GetString(AuthorizationItem.MsgCodeOperation)},
				new ListElement { Value = "AuthorizationItem.Task", IconCls = AuthorizationItem.IconClsTask, MsgCode = Langue.ResourceManager.GetString(AuthorizationItem.MsgCodeTask)},
				new ListElement { Value = "AuthorizationItem.Filter", IconCls = AuthorizationItem.IconClsFilter, MsgCode = Langue.ResourceManager.GetString(AuthorizationItem.MsgCodeFilter)},
				new ListElement { Value = "AuthorizationItem.Role", IconCls = AuthorizationItem.IconClsRole, MsgCode = Langue.ResourceManager.GetString(AuthorizationItem.MsgCodeRole)}};
			retVal.AddRange(authorizationItemList);
			return retVal.OrderBy(x => x.Label).ToList().ToJsonStore();
		}

		/// <summary>
		/// Deletes an existing business entity Logo.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool Logo_Delete(Logo item) {
			var broker = Helper.CreateInstance<LogoBroker>();
			return broker.Delete(item);
		}

		/// <summary>
		/// Creates a new business entity Logo and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse Logo_FormCreate(Logo item) {
			var broker = Helper.CreateInstance<LogoBroker>();
			return broker.FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity Logo.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse Logo_FormLoad(string Key) {
			var broker = Helper.CreateInstance<LogoBroker>();
			return broker.FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity Logo and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse Logo_FormUpdate(Logo item) {
			var broker = Helper.CreateInstance<LogoBroker>();
			return broker.FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse Logo_FormUpdateBatch(string[] keys, Logo item) {
			var broker = Helper.CreateInstance<LogoBroker>();
			return broker.FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity Logo. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public List<Logo> Logo_GetAll(PagingParameter paging, PagingLocation location, params string[] fields) {
			int total;
			var broker = Helper.CreateInstance<LogoBroker>();
			return broker.GetAll(paging, location, out total, fields: fields); ;
		}

		/// <summary>
		/// Gets a specific item for the business entity Logo.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public Logo Logo_GetItem(string Key, params string[] fields) {
			var broker = Helper.CreateInstance<LogoBroker>();
			return broker.GetItem(Key, fields);
		}

		/// <summary>
		/// Gets a json store for the business entity Logo.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<Logo> Logo_GetStore(PagingParameter paging, PagingLocation location) {
			var broker = Helper.CreateInstance<LogoBroker>();
			return broker.GetStore(paging, location);
		}

		/// <summary>
		/// Returns the stream of the image  from a Logo.
		/// </summary>
		/// <param name="Key">The key of the logo.</param>
		/// <returns></returns>
		public StreamResult Logo_GetStreamImage(string Key) {
			var broker = Helper.CreateInstance<LogoBroker>();
			Logo image = broker.GetItem(Key, "*");
			StreamResult retVal;

			// If logo exists
			if (image != null && image.Image != null) {
				retVal = new StreamResult(image.Image.GetStream(), image.Image.DocumentType);
			}
			else {
				var memStream = new MemoryStream();
				var stream = Helper.GetResourceStream(("Vizelia.FOL.Web.resources.images.default.embedded.s.gif"));
				stream.CopyTo(memStream);
				retVal = new StreamResult(memStream, "image/gif");
			}

			return retVal;
		}

		/// <summary>
		/// Saves a crud store for the business entity Logo.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<Logo> Logo_SaveStore(CrudStore<Logo> store) {
			LogoBroker broker = Helper.CreateInstance<LogoBroker>();
			return broker.SaveStore(store);
		}

		/// <summary>
		/// Gets the application settings.
		/// </summary>
		/// <returns></returns>
		public ApplicationSettings GetApplicationSettings() {
			var applicationSettings = new ApplicationSettings {
				HelpUrl = VizeliaConfiguration.Instance.Deployment.HelpURL.ToLower().Replace("[culture]", Thread.CurrentThread.CurrentUICulture.ToString()),
				DisablePermissions = VizeliaConfiguration.Instance.Authentication.DisablePermissions,
				AllowRememberMe = VizeliaConfiguration.Instance.Authentication.AllowRememberMe,
				SessionAspTimeOutInMinutes = SessionService.Timeout.TotalMinutes,
				AllowForgotPassword = VizeliaConfiguration.Instance.Authentication.AllowForgotPassword,
				RawDataUsedInMeterGridByDefault = VizeliaConfiguration.Instance.Deployment.RawDataUsedInMeterGridByDefault,
				DefaultMeterDataValidity = (int)VizeliaConfiguration.Instance.Deployment.DefaultMeterDataValidity,
				EnableGoogleAnalytics = VizeliaConfiguration.Instance.Analytics.EnableGoogleAnalytics,
				GoogleAnalyticsAccountId = VizeliaConfiguration.Instance.Analytics.GoogleAnalyticsAccountId,
			};

			return applicationSettings;
		}

		/// <summary>
		/// localize text before login.
		/// </summary>
		/// <param name="msgCode">The message code.</param>
		/// <param name="returnNullIfNotFound">if set to <c>true</c> [return null if not found].</param>
		/// <returns></returns>
		public string PreLoginLocalizeText(string msgCode, bool returnNullIfNotFound = false) {
			return PreLoginLocalizeText(new List<string> { msgCode }, returnNullIfNotFound)[msgCode];
		}

		/// <summary>
		/// localize text before login.
		/// </summary>
		/// <param name="msgCodes">The message codes.</param>
		/// <param name="returnNullIfNotFound">if set to <c>true</c> [return null if not found].</param>
		/// <returns></returns>
		public Dictionary<string, string> PreLoginLocalizeText(List<string> msgCodes, bool returnNullIfNotFound = false) {
			var tenancyBusinessLayer = Helper.Resolve<ITenancyBusinessLayer>();
			List<Tenant> tenantsForHostHeader = tenancyBusinessLayer.GetTenantsForCurrentRequestHostHeader();
			bool tenantHasItsOwnUrl = tenantsForHostHeader.Count == 1;
			Dictionary<string, string> localizedTexts = null;

			if (tenantHasItsOwnUrl) {
				// The tenant might have its own login footer, with its own ICP number. Get the text in that tenant context.
				TenantHelper.RunInDifferentTenantContext(tenantsForHostHeader[0].Name, () => localizedTexts = LocalizeTexts(msgCodes, returnNullIfNotFound));
			}
			else {
				// Get the general login footer as defined in the default tenant or resources.
				localizedTexts = LocalizeTexts(msgCodes, returnNullIfNotFound);
			}

			return localizedTexts;
		}

		private static Dictionary<string, string> LocalizeTexts(IEnumerable<string> msgCodes, bool returnNullIfNotFound) {
			return msgCodes.ToDictionary(msgCode => msgCode, msgCode => Helper.LocalizeText(msgCode, returnNullIfNotFound));
		}

		/// <summary>
		/// Deletes an existing business entity CustomData.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool CustomData_Delete(CustomData item) {
			var broker = Helper.CreateInstance<CustomDataBroker>();
			return item.KeyUser == Helper.GetCurrentFOLMembershipUser().KeyUser && broker.Delete(item);
		}

		/// <summary>
		/// Deletes all custom data by container name and key user.
		/// </summary>
		/// <param name="containerName">Name of the container.</param>
		/// <param name="keyUser">The key user.</param>
		public bool CustomData_DeleteAll(string containerName, string keyUser = null) {
			if (string.IsNullOrWhiteSpace(containerName)) {
				throw new ArgumentNullException("containerName");
			}

			if (string.IsNullOrEmpty(keyUser)) {
				keyUser = Helper.GetCurrentFOLMembershipUser().KeyUser;
			}
			var broker = Helper.CreateInstance<CustomDataBroker>();
			return broker.DeleteAll(containerName, keyUser);
		}
		/// <summary>
		/// Creates a new business entity CustomData and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse CustomData_FormCreate(CustomData item) {
			item.KeyUser = Helper.GetCurrentFOLMembershipUser().KeyUser;
			var broker = Helper.CreateInstance<CustomDataBroker>();
			return broker.FormCreate(item);
		}

		/// <summary>
		/// Updates an existing business entity CustomData and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse CustomData_FormUpdate(CustomData item) {
			item.KeyUser = Helper.GetCurrentFOLMembershipUser().KeyUser;
			var broker = Helper.CreateInstance<CustomDataBroker>();
			return broker.FormUpdate(item);
		}

		/// <summary>
		/// Gets a list for the business entity CustomData. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public List<CustomData> CustomData_GetAll(PagingParameter paging, PagingLocation location, params string[] fields) {
			int total;
			var broker = Helper.CreateInstance<CustomDataBroker>();
			if (paging.filters == null) {
				paging.filters = new List<GridFilter>();
			}
			var keyUser = Helper.GetCurrentFOLMembershipUser().KeyUser;
			const string const_keyuser = "KeyUser";
			paging.filters.RemoveAll(x => x.field == const_keyuser);
			paging.filters.Add(new GridFilter { field = const_keyuser, data = new GridData { comparison = "eq", type = "string", value = keyUser } });
			return broker.GetAll(paging, location, out total, fields: fields);
		}

		/// <summary>
		/// Gets a specific item for the business entity CustomData.
		/// </summary>
		/// <param name="key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public CustomData CustomData_GetItem(string key, params string[] fields) {
			var broker = Helper.CreateInstance<CustomDataBroker>();
			var item = broker.GetItem(key, fields);
			if (item != null)
				return item.KeyUser == Helper.GetCurrentFOLMembershipUser().KeyUser ? item : null;
			return null;
		}

		/// <summary>
		/// Get the LatestHistoricalPset values by location key
		/// </summary>
		/// <param name="keyLocation">The location key</param>
		/// <returns></returns>
		public List<LatestHistoricalPset> GetLatestHistoricalPsetValuesByLocationKey(string keyLocation) {
			var broker = Helper.CreateInstance<PsetAttributeHistoricalBroker>();
			var result = broker.GetLatestHistoricalPsetValuesByLocationKey(keyLocation);

			return result;
		}

		/// <summary>
		/// Get the LatestHistoricalPset values of locations by Pset
		/// </summary>
		/// <param name="keyPropertySet">The pset key</param>
		/// <returns></returns>
		public List<LatestHistoricalPset> GetLatestHistoricalPsetValuesOfLocationsByPset(string keyPropertySet) {
			var broker = Helper.CreateInstance<PsetAttributeHistoricalBroker>();
			var result = broker.GetLatestHistoricalPsetValuesOfLocationsByPset(keyPropertySet);
			return result;
		}

		/// <summary>
		/// Get All PsetValue(s).
		/// </summary>
		/// <param name="paging"></param>
		/// <param name="location"></param>
		/// <param name="fields"></param>
		/// <returns></returns>
		public List<PsetValue> PsetValue_GetAll(PagingParameter paging, PagingLocation location, params string[] fields) {
			var broker = Helper.CreateInstance<PsetValueBroker>();
			int total;
			var result = broker.GetAll(paging, location, out total, fields: fields);
			return result;
		}

		/// <summary>
		/// Get PsetValue by key.
		/// </summary>
		/// <param name="key"></param>
		/// <param name="fields"></param>
		/// <returns></returns>
		public PsetValue PsetValue_GetItem(string key, params string[] fields) {
			var broker = Helper.CreateInstance<PsetValueBroker>();
			var result = broker.GetItem(key);
			return result;
		}

		/// <summary>
		/// Updates an existing business entity PsetValue and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse PsetValue_FormUpdate(PsetValue item) {
			var broker = Helper.CreateInstance<PsetValueBroker>();
			FormResponse response = broker.FormUpdate(item);
			return response;
		}

		/// <summary>
		/// Creates PsetValue item.
		/// </summary>
		/// <param name="item"></param>
		/// <returns></returns>
		public FormResponse PsetValue_FormCreate(PsetValue item) {
			var broker = Helper.CreateInstance<PsetValueBroker>();
			FormResponse response = broker.FormCreate(item);
			return response;
		}

		/// <summary>
		/// Deletes the PsetValue item.
		/// </summary>
		/// <param name="item"></param>
		/// <returns></returns>
		public bool PsetValue_Delete(PsetValue item) {
			var broker = Helper.CreateInstance<PsetValueBroker>();
			bool ret = broker.Delete(item);
			return ret;
		} 
	}
}
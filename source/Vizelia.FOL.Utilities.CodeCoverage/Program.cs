﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using Vizelia.FOL.Utilities.CommandLine;

namespace Vizelia.FOL.Utilities.CodeCoverage {
	class Program {


		private static string BinDirectory { get; set; }
		private static string TargetDirectory { get; set; }


		const string toolsPath = @"%ProgramFiles(x86)%\Microsoft Visual Studio 10.0\Team Tools\Performance Tools\x64";
		const string coverageFile = @"results.coverage";
		const string instrumentBatch = @"instrument.bat";
		const string startBatch = @"start_coverage.bat";
		const string stopBatch = @"stop_coverage.bat";


		static void Main(string[] args) {

			Console.ForegroundColor = ConsoleColor.Gray;
			if ((args.Length == 0) || (args[0].Contains("?"))) {
				ShowUsage();
				return;
			}

			Console.ForegroundColor = ConsoleColor.Red;

			Arguments CommandLine = new Arguments(args);
			bool flgArgumentValid = true;
			if (CommandLine["BinDirectory"] == null) {
				Console.WriteLine("Argument BinDirectory not supplied");
				flgArgumentValid = false;
			}

			if (CommandLine["TargetDirectory"] == null) {
				Console.WriteLine("Argument TargetDirectory not supplied");
				flgArgumentValid = false;
			}

			if (!flgArgumentValid) {
				UsageHelper.Abort("Execution aborted.");
			}

			Console.ForegroundColor = ConsoleColor.Gray;
			Console.WriteLine("Building scripts...");
			Console.WriteLine("");
			Console.ForegroundColor = ConsoleColor.DarkGray;

			ConfigureProperties(
				CommandLine["BinDirectory"],
				CommandLine["TargetDirectory"]);

			BuildScripts();

			Console.ForegroundColor = ConsoleColor.Green;
			Console.WriteLine("");
			Console.WriteLine("You can now run the batch files ({0}, {1}, and {2}." , instrumentBatch, startBatch, stopBatch);

			Console.ForegroundColor = ConsoleColor.Gray;

		}

		/// <summary>
		/// Configures the properties.
		/// </summary>
		/// <param name="bin">The bin.</param>
		/// <param name="targetDirectory">The target directory.</param>
		private static void ConfigureProperties(
			string bin,
			string targetDirectory) {
			BinDirectory = bin; 
			TargetDirectory = targetDirectory; 
			CheckProperties();
		}


		/// <summary>
		/// Checks the directory. Exit if not found.
		/// </summary>
		/// <param name="path">The path.</param>
		private static void CheckDirectory(string path) {
			if (!Directory.Exists(path)) {
				Console.WriteLine("Could not find directory : {0}", path);
				UsageHelper.Abort("Execution aborted.");
			}
		}

		/// <summary>
		/// Checks the properties.
		/// </summary>
		private static void CheckProperties() {
			Console.ForegroundColor = ConsoleColor.Red;

			CheckDirectory(TargetDirectory);
			CheckDirectory(BinDirectory);

			Console.ForegroundColor = ConsoleColor.Gray;
		}


		/// <summary>
		/// Builds the scripts.
		/// </summary>
		private static void BuildScripts() {

			var bins = Directory.GetFiles(BinDirectory).Where(p => p.StartsWith(Path.Combine(BinDirectory, "Vizelia.FOL.WCF"))).Where(p => p.EndsWith(".dll"));
			using (var bat = File.CreateText(Path.Combine(TargetDirectory, instrumentBatch))) {
				foreach (var bin in bins) {
					bat.WriteLine("\"" + toolsPath + @"\vsinstr""" + @" /coverage ""{0}""", bin);
				}
			}


			using (var bat = File.CreateText(Path.Combine(TargetDirectory, startBatch))) {
				bat.WriteLine("iisreset /start");
				bat.WriteLine("\"" + toolsPath + @"\vsperfcmd"" /start:coverage /output:""{0}"" /cs /user:Everyone", Path.Combine(TargetDirectory, coverageFile));

			}
			using (var bat = File.CreateText(Path.Combine(TargetDirectory, stopBatch))) {
				bat.WriteLine("iisreset /stop");
				bat.WriteLine("\"" + toolsPath + @"\vsperfcmd"" /shutdown");
				bat.WriteLine("iisreset /start");
			}
		}

		/// <summary>
		/// Shows the usage.
		/// </summary>
		private static void ShowUsage() {
			Console.Clear();
			Console.WriteLine("Command-line tool for instrumenting bin and calculating code covergage");
			Console.WriteLine("Copyright (c) Vizelia - Schneider Electric. All rights reserved.");
			Console.WriteLine("");

			var usage = UsageHelper.GetUsageString(new List<ArgumentHelpStrings> {
			                                             	new ArgumentHelpStrings {
			                                             	                        	Syntax ="/BinDirectory:<string>",
																						Help = "The directory containing the binaries to instrument.",
																						Type = ArgumentType.Mandatory
			                                             	                        },
															new ArgumentHelpStrings {
			                                             	                        	Syntax ="/TargetDirectory:<string>",
																						Help = String.Format("The directory that will contain the 3 batch script to run ({0}, {1}, {2})." , instrumentBatch, startBatch, stopBatch),
																						Type = ArgumentType.Mandatory
			                                             	                        }
														

			                                             });

			Console.Write(usage);

		}
	}
}

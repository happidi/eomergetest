﻿using System;
using System.Collections.Generic;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessLayer.Broker;
using Vizelia.FOL.Common;
using Vizelia.FOL.WF.Common;

namespace Vizelia.FOL.WF.Extensions {
	/// <summary>
	/// A custom implementation of local service for Task.
	/// </summary>
	public class TaskLocalService : BaseLocalService<Task> {
		private TaskBroker _broker;

		/// <summary>
		/// Initializes a new instance of the <see cref="TaskLocalService"/> class.
		/// </summary>
		public TaskLocalService() {
			_broker = Helper.CreateInstance<TaskBroker>();
		}

		/// <summary>
		/// Resolves the workflow definition.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <returns></returns>
		public override WorkflowDefinition ResolveWorkflowDefinition(Task entity) {
			WorkflowDefinition wfDef = null;
			// Finding the assigned workflow assembly definition in ascendant for the couple of classification items.
			ClassificationItemBroker brokerClassificationItem = Helper.CreateInstance<ClassificationItemBroker>();
			var wflAssembyDef = brokerClassificationItem.GetWorkflowAssemblyDefinitionByClassificationAscendant(null, entity.KeyClassificationItem);
			if (wflAssembyDef != null) {
				Type wfType = Type.GetType(wflAssembyDef.AssemblyQualifiedName);
				wfDef = new WorkflowDefinition {
					LocalServiceType = typeof(IBaseLocalService<Task>),
					WorkflowType = wfType,
					WorkflowExtensions = new List<Type>(),
					AssemblyAndNamespaceDictionary = new Dictionary<string, string> {
			                               	                                                                	{"Microsoft.VisualBasic", "Microsoft.VisualBasic"},
			                               	                                                                	{"Vizelia.FOL.WF.Security", "Vizelia.FOL.WF.Security"},
			                               	                                                                	{"Vizelia.FOL.Common", "Vizelia.FOL.BusinessEntities"}
			                               	                                                                }
				};
			}
			return wfDef;
		}

		/// <summary>
		/// Gets the item by instance id.
		/// </summary>
		/// <param name="instanceId">The instance id.</param>
		/// <returns></returns>
		public override Task GetItemByInstanceId(Guid instanceId) {
			return _broker.GetItemByInstanceId(instanceId);
		}

		/// <summary>
		/// Saves the item.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <returns></returns>
		public override Task SaveItem(Task entity) {
			return _broker.Save(entity);
		}
	}
}
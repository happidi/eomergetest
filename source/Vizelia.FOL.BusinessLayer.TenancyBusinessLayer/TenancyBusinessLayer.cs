﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Security;
using System.Xml;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessLayer.Broker;
using Vizelia.FOL.BusinessLayer.Interfaces;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Configuration;
using Vizelia.FOL.Providers;
using Vizelia.FOL.ServiceAgents;
using Vizelia.FOL.WCFService.Contracts;

namespace Vizelia.FOL.BusinessLayer {
	/// <summary>
	/// Business layer implementation for Tenancy.
	/// </summary>
	public class TenancyBusinessLayer : ITenancyBusinessLayer {
		/// <summary>
		/// Creates the admin tenant.
		/// </summary>
		public Tenant Tenant_CreateAdminTenant() {
			var tenant = new Tenant {
				AdminEmail = VizeliaConfiguration.Instance.Deployment.AdminTenantEmail,
				KeyTenant = VizeliaConfiguration.Instance.Deployment.ApplicationName,
				Url = Helper.GetFrontendUrl(),
				IsInitialized = false
			};

			var broker = Helper.CreateInstance<TenantBroker>();
			FormResponse formResponse = broker.FormCreate(tenant);

			if (!formResponse.success) {
				throw new VizeliaException("Tenant creation failed: " + formResponse.msg);
			}

			return tenant;
		}
		
		/// <summary>
		/// Deletes an existing business entity Tenant.
		/// </summary>
		/// <param name="keyTenant">The key tenant.</param>
		/// <param name="operationId">The operation id.</param>
		public void Tenant_Delete(string keyTenant, Guid operationId) {
			LongRunningOperationService.Invoke(operationId, () => {
				// Delete all user ssid from cache
				var brokerUsers = Helper.CreateInstance<FOLMembershipUserBroker>();
				TenantHelper.RunInDifferentTenantContext(keyTenant, () => {
					var users = brokerUsers.GetAll(false);
					var roleProvider = (IFOLRoleProvider)Roles.Provider;
					foreach (var folMembershipUser in users) {
						roleProvider.ClearCache(folMembershipUser.UserName);
					}
				});
				
				TenantBroker broker = Helper.CreateInstance<TenantBroker>();
				broker.Delete(keyTenant);

				return new LongRunningOperationResult(LongRunningOperationStatus.Completed);
			});
		}

		/// <summary>
		/// Creates a new business entity Tenant and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <param name="operationId">The operation id.</param>
		public void Tenant_FormCreate(Tenant item, Guid operationId) {
			LongRunningOperationService.Invoke(operationId, () => {
				// Create tenant
				LongRunningOperationService.ReportProgress();
				var broker = Helper.CreateInstance<TenantBroker>();
				FormResponse formResponse = broker.FormCreate(item);

			    if (formResponse.success) {
			        // Initialize tenant
			        InitializeNewTenant(item);
			    }
				
			    return new LongRunningOperationResult(LongRunningOperationStatus.ResultAvailable, formResponse);
			});
		}

		/// <summary>
		/// Loads a specific item for the business entity Tenant.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse Tenant_FormLoad(string Key) {
			TenantBroker broker = Helper.CreateInstance<TenantBroker>();
			return broker.FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity Tenant and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <param name="operationId">The operation id.</param>
		/// <returns></returns>
		public void Tenant_FormUpdate(Tenant item, Guid operationId) {
			LongRunningOperationService.Invoke(operationId, () => {
				var broker = Helper.CreateInstance<TenantBroker>();
				FormResponse formResponse = broker.FormUpdate(item);

				return new LongRunningOperationResult(LongRunningOperationStatus.ResultAvailable, formResponse);
			});
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse Tenant_FormUpdateBatch(string[] keys, Tenant item) {
			TenantBroker broker = Helper.CreateInstance<TenantBroker>();
			return broker.FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity Tenant. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		public List<Tenant> Tenant_GetAll(PagingParameter paging, PagingLocation location) {
			int total;
			TenantBroker broker = Helper.CreateInstance<TenantBroker>();
			return broker.GetAll(paging, location, out total);
		}

		/// <summary>
		/// Gets a specific item for the business entity Tenant.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public Tenant Tenant_GetItem(string Key) {
			TenantBroker broker = Helper.CreateInstance<TenantBroker>();
			return broker.GetItem(Key);
		}

		/// <summary>
		/// Gets a json store for the business entity Tenant.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<Tenant> Tenant_GetStore(PagingParameter paging, PagingLocation location) {
			TenantBroker broker = Helper.CreateInstance<TenantBroker>();
			return broker.GetStore(paging, location);
		}



		/// <summary>
		/// Saves a crud store for the business entity Tenant.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<Tenant> Tenant_SaveStore(CrudStore<Tenant> store) {
			TenantBroker broker = Helper.CreateInstance<TenantBroker>();
			return broker.SaveStore(store);
		}

		/// <summary>
		/// Resets the current tenant.
		/// </summary>
		/// <param name="operationId">The operation id.</param>
		/// <param name="adminPassword">The admin password.</param>
		public void ResetTenant(Guid operationId, string adminPassword) {
			LongRunningOperationService.Invoke(operationId, () => {
				using (TracingService.StartTracing("ResetTenant", operationId.ToString())) {

					ResetMapping(operationId, adminPassword);
					ResetAzmanConfiguration();
					ResetEnergy();

					TracingService.Write("ResetTenant complete");

					return new LongRunningOperationResult(LongRunningOperationStatus.Completed);
				}
			});
		}

		/// <summary>
		/// Imports the azman configuration.
		/// </summary>
		private void ResetAzmanConfiguration() {
			TracingService.Write("AzMan Configuration starting...");

			Stream stream = File.OpenRead(AppDomain.CurrentDomain.BaseDirectory + "/azman/azman.xml");
			var settings = new XmlReaderSettings { IgnoreComments = true, IgnoreWhitespace = true, IgnoreProcessingInstructions = true, DtdProcessing = DtdProcessing.Prohibit };
			XmlReader reader = XmlReader.Create(stream, settings);

			XmlDocument xmlDoc = new XmlDocument();

			xmlDoc.Load(reader);

			Tenant tenant = Tenant_GetItem(ContextHelper.ApplicationName);
			AzmanImportOptions importOptions = ((tenant == null) || (!tenant.IsInitialized)) ? AzmanImportOptions.FullImport : AzmanImportOptions.CreateAndOverwrite;
			((IFOLRoleProvider)Roles.Provider).Import(xmlDoc.DocumentElement, importOptions, "Admin");

			reader.Close();
			stream.Close();
			TracingService.Write("AzMan Configuration completed");
	
		}


		private static void ResetEnergy() {
			TracingService.Write("Initializing energy module");
			LongRunningOperationService.ReportProgress(50, "Initializing energy module");
			LongRunningOperationService.PrepareForChildOperation("Energy", 50, 100);
			var energyModule = Helper.Resolve<IEnergyBusinessLayer>();
			energyModule.InitializeTenant();
		}

		/// <summary>
		/// Resets the mapping.
		/// </summary>
		/// <param name="operationId">The operation id.</param>
		/// <param name="adminPassword">The admin password.</param>
		private static void ResetMapping(Guid operationId, string adminPassword) {
			TracingService.Write("Creating basic entities");
			LongRunningOperationService.ReportProgress(5, "Creating basic entities");
			LongRunningOperationService.PrepareForChildOperation("Mapping", 5, 50);
			var mappingModule = Helper.Resolve<IMappingBusinessLayer>();

			Guid mappingOperationId = Guid.NewGuid();

			mappingModule.InitializeTenant(mappingOperationId, adminPassword);

			string mappingPasswordCacheKey = string.Format("admin_pwd_{0}", mappingOperationId);
			string password = (string)CacheService.GetData(mappingPasswordCacheKey);
			CacheService.Remove(mappingPasswordCacheKey);

			string resetTenantCacheKey = string.Format("admin_pwd_{0}", operationId);
			CacheService.Add(resetTenantCacheKey, password);
		}

		/// <summary>
		/// Calls the method on the new tenant to initialize itself.
		/// Returns the operation ID of that long-running operation.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <returns></returns>
		private static void InitializeNewTenant(Tenant item) {
			var energyBusinessLayer = Helper.Resolve<IEnergyBusinessLayer>();
			energyBusinessLayer.InitializeEnergyAggregatorServiceAgent(new List<string> { ContextHelper.ApplicationName });

			if (TenantProvisioningService.Mode == MultitenantHostingMode.Isolated) {
				// We call the IPublicWCF.InitializeTenant method on the given tenant.
				// It is possible that the tenant web server isn't up yet, so we wait a bit, and retry if an exception is thrown.
				Thread.Sleep(3000);
				// **Hi there!** Are you debugging this method? It will call a newly created worker process 
				// on the newly created tenant, so attach your debugger to any unattached w3wp.exe.
				Helper.RetryIfFailed(5, 2000, () =>
					CrossTenantServiceCall.CallUnauthenticated(item.Url, (IPublicWCF p) => p.InitializeTenant(Guid.NewGuid(), item.AdminPassword, null)));
			}
			else {
				var publicWCF = Helper.Resolve<IPublicWCF>();
				publicWCF.InitializeTenant(Guid.NewGuid(), item.AdminPassword, item.Name);
			}
		}

		/// <summary>
		/// Gets the tenants for current request host header.
		/// </summary>
		/// <returns></returns>
		public List<Tenant> GetTenantsForCurrentRequestHostHeader() {
			var tenancyBusinessLayer = Helper.Resolve<ITenancyBusinessLayer>();
			List<Tenant> allTenants = tenancyBusinessLayer.Tenant_GetAll(new PagingParameter(), PagingLocation.Database);

			string requestHostHeader = HttpContext.Current.Request.Url.Host.ToLower();
			List<Tenant> tenantsForHostHeader =
				allTenants.Where(t => t.Url != null && requestHostHeader.Equals(t.Url.ToLower().TrimEnd('/'))).ToList();
			return tenantsForHostHeader;
		}

		/// <summary>
		/// Checks if tenant is initialized
		/// </summary>
		/// <returns></returns>
		public bool IsTenantInitialized() {
			var tenancyBusinessLayer = Helper.Resolve<ITenancyBusinessLayer>();
			Tenant tenant = tenancyBusinessLayer.Tenant_GetItem(ContextHelper.ApplicationName);
			return tenant != null && tenant.IsInitialized;
		}
	}

}
﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.Threading;
using Microsoft.WindowsAzure.Diagnostics;
using Microsoft.WindowsAzure.ServiceRuntime;
using Vizelia.FOL.WCFService;

namespace Vizelia.FOL.EnergyAggregatorService {
	/// <summary>
	/// The worker role.
	/// </summary>
	public class WorkerRole : RoleEntryPoint {

		/// <summary>
		/// The service host for EnergyAggregator WCF.
		/// </summary>
		private ServiceHost host;

		/// <summary>
		/// Called by Windows Azure after the role instance has been initialized. This method serves as the
		/// main thread of execution for your role.
		/// </summary>
		public override void Run() {
			StartWCFService();
			while (true) {
				Thread.Sleep(10000);
			}
		}

		/// <summary>
		/// Called by Windows Azure to initialize the role instance.
		/// </summary>
		/// <returns>
		/// True if initialization succeeds, False if it fails. The default implementation returns True.
		/// </returns>
		public override bool OnStart() {
			ServicePointManager.DefaultConnectionLimit = 12;
			
			DiagnosticMonitorConfiguration dmc = DiagnosticMonitor.GetDefaultInitialConfiguration();
			dmc.WindowsEventLog.DataSources.Add("Application!*");
			dmc.WindowsEventLog.ScheduledTransferPeriod = TimeSpan.FromMinutes(1);
			DiagnosticMonitor.Start("Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString", dmc);
	
			RoleEnvironment.Changing += RoleEnvironmentChanging;
			return base.OnStart();
		}

		/// <summary>
		/// Called by Windows Azure when the role instance is to be stopped.
		/// </summary>
		public override void OnStop() {
			StopWCFService();
			base.OnStop();
		}

		/// <summary>
		/// Starts the WCF service.
		/// </summary>
		private void StartWCFService() {
			try {
			IPEndPoint ip = RoleEnvironment.CurrentRoleInstance.InstanceEndpoints["EnergyAggregatorEndpoint"].IPEndpoint;
			Uri baseAddress = new Uri(String.Format("net.tcp://{0}", ip));
				host = new ServiceHost(typeof(EnergyAggregatorWCF), baseAddress);
				host.Description.Endpoints.ToList().ForEach(ep => Trace.WriteLine(ep.Address.Uri.AbsoluteUri, "Information"));
				host.Open();
			}
			catch (Exception ex) {
				Trace.WriteLine(ex.Message, "Error");
				EventLog.WriteEntry("EnergyAggregator Error", ex.Message);
			}
			Trace.WriteLine("EnergyAggregator WCF service running v1.3");
		}

		/// <summary>
		/// Stops the WCF service.
		/// </summary>
		private void StopWCFService() {
			if (host != null) {
				try {
					host.Close();
				}
				catch (Exception ex) {
					Trace.WriteLine(ex.Message, "Error");
					host.Abort();
					throw;
				}
			}
		}

		/// <summary>
		/// Roles the environment changing.
		/// </summary>
		/// <param name="sender">The sender.</param>
		/// <param name="e">The <see cref="Microsoft.WindowsAzure.ServiceRuntime.RoleEnvironmentChangingEventArgs"/> instance containing the event data.</param>
		private void RoleEnvironmentChanging(object sender, RoleEnvironmentChangingEventArgs e) {
			if (e.Changes.Any(change => change is RoleEnvironmentConfigurationSettingChange)) {
				e.Cancel = true;
			}
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.DataLayer.BrokerDB;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.BusinessLayer.Broker {


	/// <summary>
	/// The broker for business entity ClassificationItem.
	/// </summary>
	public class ClassificationItemBroker : MappingEnabledBroker<ClassificationItem, ClassificationItemBrokerDB> {

		/// <summary>
		/// Creates an association between a ClassificationItem and a WorkflowAssemblyDefinition.
		/// </summary>
		/// <param name="keyClassificationChildren">The Key of the ClassificationItem.</param>
		/// <param name="keyClassificationParent">The Key of the parent ClassificationItem in case the priority is added to a relationship.</param>
		/// <param name="workflowAssemblyDefinitionName">Name of the workflow assembly definition.</param>
		/// <param name="workflowAssemblyDefinitionAssemblyQualifiedName">Name of the workflow assembly definition assembly qualified.</param>
		/// <returns></returns>
		public bool AddWorkflowAssemblyDefinitionClassification(string keyClassificationChildren, string keyClassificationParent, string workflowAssemblyDefinitionName, string workflowAssemblyDefinitionAssemblyQualifiedName) {
			return ((ClassificationItemBrokerDB)m_brokerDB).AddWorkflowAssemblyDefinitionClassification(keyClassificationChildren, keyClassificationParent, workflowAssemblyDefinitionName, workflowAssemblyDefinitionAssemblyQualifiedName);
		}

		/// <summary>
		/// Creates the specified item.
		/// The overrides is necessary to update ClassificationHelper.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <param name="formResponse">The form response.</param>
		/// <returns></returns>
		protected override ClassificationItem Create(ClassificationItem item, out FormResponse formResponse) {
			ClassificationItem result = base.Create(item, out formResponse);
			ClearClassificationItemDefinitionCache();
			return result;
		}

		/// <summary>
		/// Deletes the specified item.
		/// The overrides is necessary to update ClassificationHelper.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <param name="persistedItem">The persisted item.</param>
		/// <returns></returns>
		public override bool Delete(ClassificationItem item, ClassificationItem persistedItem = null) {
			bool result = base.Delete(item, persistedItem);
			ClearClassificationItemDefinitionCache();
			return result;
		}

		/// <summary>
		/// Deletes an association between a ClassificationItem and a WorkflowAssemblyDefinition.
		/// </summary>
		/// <param name="keyClassificationChildren">The Key of the ClassificationItem.</param>
		/// <param name="keyClassificationParent">The Key of the parent ClassificationItem in case the priority is added to a relationship.</param>
		/// <returns></returns>
		public bool DeleteWorkflowAssemblyDefinitionClassification(string keyClassificationChildren, string keyClassificationParent) {
			return ((ClassificationItemBrokerDB)m_brokerDB).DeleteWorkflowAssemblyDefinitionClassification(keyClassificationChildren, keyClassificationParent);
		}

		/// <summary>
		/// Gets the children.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <param name="path">The path.</param>
		/// <returns></returns>
		public List<ClassificationItem> GetChildren(string key, string path) {
			return ((ClassificationItemBrokerDB)m_brokerDB).GetChildren(key, path);
		}

		/// <summary>
		/// Gets a dictionnary for all the ClassificationItem, by KeyClassificationItem. 
		/// </summary>
		/// <returns></returns>
		public Dictionary<string, ClassificationItem> GetDictionary(string keyClassificationItemPath) {
			var list = ((ClassificationItemBrokerDB)m_brokerDB).GetAll(keyClassificationItemPath);
			var retVal = new Dictionary<string, ClassificationItem>();
			list.ForEach(l => retVal.Add(l.KeyClassificationItem, l));
			return retVal;
		}

		/// <summary>
		/// Gets a json store of ClassificationItem for a specific mail
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="keyChart">The Key of the chart.</param>
		/// <returns>A JsonStore.</returns>
		public JsonStore<ClassificationItem> GetFilterEventLogClassificationStoreByKeyChart(PagingParameter paging, string keyChart) {
			int total;
			return ((ClassificationItemBrokerDB)m_brokerDB).GetFilterEventLogClassificationAllPagingByKeyChart(paging, keyChart, out total).ToJsonStoreWithFilter(paging);
		}

		/// <summary>
		/// Gets a store of filter meter classification for a specific AlarmDefinition.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="keyAlarmDefinition">The Key of the AlarmDefinition.</param>
		/// <returns>A JsonStore.</returns>
		public JsonStore<ClassificationItem> GetFilterMeterClassificationStoreByKeyAlarmDefinition(PagingParameter paging, string keyAlarmDefinition) {
			int total;
			return ((ClassificationItemBrokerDB)m_brokerDB).GetFilterMeterClassificationAllPagingByKeyAlarmDefinition(paging, keyAlarmDefinition, out total).ToJsonStore(total, paging);
		}

		/// <summary>
		/// Gets a store of filter meter classification for a specific MeterValidationRule.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="keyMeterValidationRule">The Key of the MeterValidationRule.</param>
		/// <returns>A JsonStore.</returns>
		public JsonStore<ClassificationItem> GetFilterMeterClassificationStoreByKeyMeterValidationRule(PagingParameter paging, string keyMeterValidationRule) {
			int total;
			return ((ClassificationItemBrokerDB)m_brokerDB).GetFilterMeterClassificationAllPagingByKeyMeterValidationRule(paging, keyMeterValidationRule, out total).ToJsonStore(total, paging);
		}

		/// <summary>
		/// Gets a store of filter meter classification for a specific chart.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="keyChart">The Key of the chart.</param>
		/// <returns>A JsonStore.</returns>
		public JsonStore<ClassificationItem> GetFilterMeterClassificationStoreByKeyChart(PagingParameter paging, string keyChart) {
			int total;
			return ((ClassificationItemBrokerDB)m_brokerDB).GetFilterMeterClassificationAllPagingByKeyChart(paging, keyChart, out total).ToJsonStore(total, paging);
		}

		/// <summary>
		/// Gets a store of filter alarm definition classification for a specific AlarmTable.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="keyAlarmTable">The Key of the AlarmTable.</param>
		/// <returns>A JsonStore.</returns>
		public JsonStore<ClassificationItem> GetFilterAlarmDefinitionClassificationStoreByKeyAlarmTable(PagingParameter paging, string keyAlarmTable) {
			int total;
			return ((ClassificationItemBrokerDB)m_brokerDB).GetFilterAlarmDefinitionClassificationStoreByKeyAlarmTable(paging, keyAlarmTable, out total).ToJsonStore(total, paging);
		}

		/// <summary>
		/// Gets a store of filter alarm definition classification for a specific Chart.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="keyChart">The Key of the Chart.</param>
		/// <returns>A JsonStore.</returns>
		public JsonStore<ClassificationItem> GetFilterAlarmDefinitionClassificationStoreByKeyChart(PagingParameter paging, string keyChart) {
			int total;
			return GetFilterAlarmDefinitionClassificationListByKeyChart(paging, keyChart, out total).ToJsonStore(total, paging);
		}

		/// <summary>
		/// Gets the filter alarm definition classification list by key chart.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="keyChart">The key chart.</param>
		/// <param name="total">The total.</param>
		/// <returns>List of ClassificationItem</returns>
		public List<ClassificationItem> GetFilterAlarmDefinitionClassificationListByKeyChart(PagingParameter paging, string keyChart, out int total) {
			return ((ClassificationItemBrokerDB)m_brokerDB).GetFilterAlarmDefinitionClassificationStoreByKeyChart(paging, keyChart, out total);
		}

		/// <summary>
		/// Gets a json store of ClassificationItem for a specific mail.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="keyMail">The Key of the mail.</param>
		public JsonStore<ClassificationItem> GetStoreByKeyMail(PagingParameter paging, string keyMail) {
			int total;
			return ((ClassificationItemBrokerDB)m_brokerDB).GetAllPagingByKeyMail(paging, keyMail, out total).ToJsonStore(total, paging);
		}


		/// <summary>
		/// Gets a json store of ClassificationItem for a specific object  except objects having their own specific function (mail etc...).
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="keyObject">The key object.</param>
		public JsonStore<ClassificationItem> GetStoreByKeyObject(PagingParameter paging, string keyObject) {
			int total;
			return ((ClassificationItemBrokerDB)m_brokerDB).GetAllPagingByKeyObject(paging, keyObject, out total).ToJsonStore(total, paging);
		}


		/// <summary>
		/// Gets the WorkflowAssemblyDefinition object assigned to a ClassificationItem.
		/// </summary>
		/// <param name="keyClassificationChildren">The key classification children.</param>
		/// <param name="keyClassificationParent">The key classification parent.</param>
		/// <returns></returns>
		public WorkflowAssemblyDefinition GetWorkflowAssemblyDefinitionByClassification(string keyClassificationChildren, string keyClassificationParent) {
			return ((ClassificationItemBrokerDB)m_brokerDB).GetWorkflowAssemblyDefinitionByClassification(keyClassificationChildren, keyClassificationParent);
		}

		/// <summary>
		/// Gets the WorkflowAssemblyDefinition object assigned to a ClassificationItem.
		/// </summary>
		/// <param name="keyClassificationChildren">The key classification children.</param>
		/// <param name="keyClassificationParent">The key classification parent.</param>
		/// <returns></returns>
		public WorkflowAssemblyDefinition GetWorkflowAssemblyDefinitionByClassificationAscendant(string keyClassificationChildren, string keyClassificationParent) {
			return ((ClassificationItemBrokerDB)m_brokerDB).GetWorkflowAssemblyDefinitionByClassificationAscendant(keyClassificationChildren, keyClassificationParent);
		}

		/// <summary>
		/// Resolves the references during mapping.
		/// </summary>
		/// <param name="mappingRecord">The mapping record.</param>
		/// <param name="persistedEntity">The persisted entity.</param>
		protected override void ResolveReferencesForImport(MappingRecord<ClassificationItem> mappingRecord, ClassificationItem persistedEntity) {
			ResolveParentReferenceForImport<ClassificationItem>(e => e.KeyParent, mappingRecord.Entity, persistedEntity);
			ResolveParentReferenceForImport<FOLMembershipUser>(e => e.Creator, mappingRecord.Entity, persistedEntity);
		}

		/// <summary>
		/// Resolves the references for export.
		/// </summary>
		/// <param name="entity">The entity.</param>
		protected override void ResolveReferencesForExport(ClassificationItem entity) {
			ResolveParentReferenceForExport<ClassificationItem>(e => e.KeyParent, entity);
			ResolveParentReferenceForExport<FOLMembershipUser>(e => e.Creator, entity);
		}

		/// <summary>
		/// Saves the chart filter eventlog classification.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="filterEventLogClassification">The filter eventlog classification.</param>
		/// <returns></returns>
		public List<ClassificationItem> SaveChartFilterEventLogClassification(Chart entity, CrudStore<ClassificationItem> filterEventLogClassification) {
			return ((ClassificationItemBrokerDB)m_brokerDB).SaveChartFilterEventLogClassification(entity, filterEventLogClassification);
		}


		/// <summary>
		/// Saves the chart Filter AlarmDefinition Classification.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="filterAlarmDefinitionClassification">The filter alarm definition classification.</param>
		/// <returns></returns>
		public List<ClassificationItem> SaveChartFilterAlarmDefinitionClassification(Chart entity, CrudStore<ClassificationItem> filterAlarmDefinitionClassification) {
			return ((ClassificationItemBrokerDB)m_brokerDB).SaveChartFilterAlarmDefinitionClassification(entity, filterAlarmDefinitionClassification);
		}

		/// <summary>
		/// Saves the chart filter meter classification.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="filterMeterClassification">The filter meter classification.</param>
		/// <returns></returns>
		public List<ClassificationItem> SaveChartFilterMeterClassification(Chart entity, CrudStore<ClassificationItem> filterMeterClassification) {
			return ((ClassificationItemBrokerDB)m_brokerDB).SaveChartFilterMeterClassification(entity, filterMeterClassification);
		}

		/// <summary>
		/// Saves the AlarmDefinition filter meter classification.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="filterMeterClassification">The filter meter classification.</param>
		/// <returns></returns>
		public List<ClassificationItem> SaveAlarmDefinitionFilterMeterClassification(AlarmDefinition entity, CrudStore<ClassificationItem> filterMeterClassification) {
			return ((ClassificationItemBrokerDB)m_brokerDB).SaveAlarmDefinitionFilterMeterClassification(entity, filterMeterClassification);
		}


		/// <summary>
		/// Saves the MeterValidationRule filter meter classification.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="filterMeterClassification">The filter meter classification.</param>
		/// <returns></returns>
		public List<ClassificationItem> SaveMeterValidationRuleFilterMeterClassification(MeterValidationRule entity, CrudStore<ClassificationItem> filterMeterClassification) {
			return ((ClassificationItemBrokerDB)m_brokerDB).SaveMeterValidationRuleFilterMeterClassification(entity, filterMeterClassification);
		}

		/// <summary>
		/// Saves the AlarmTable filter AlarmDefinition classification.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="filterAlarmDefinitionClassification">The filter AlarmDefinition classification.</param>
		/// <returns></returns>
		public List<ClassificationItem> SaveAlarmTableFilterAlarmDefinitionClassification(AlarmTable entity, CrudStore<ClassificationItem> filterAlarmDefinitionClassification) {
			return ((ClassificationItemBrokerDB)m_brokerDB).SaveAlarmTableFilterAlarmDefinitionClassification(entity, filterAlarmDefinitionClassification);
		}

		/// <summary>
		/// Saves the mail priorities.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="classifications">The classifications.</param>
		/// <returns></returns>
		public List<ClassificationItem> SaveMailClassifications(Mail entity, CrudStore<ClassificationItem> classifications) {
			return ((ClassificationItemBrokerDB)m_brokerDB).SaveMailClassifications(entity, classifications);
		}

		/// <summary>
		/// Gets children for an item.
		/// </summary>
		/// <param name="key">The Key of the parent item.</param>
		/// <param name="keyChart">The key chart.</param>
		/// <param name="keyAlarmDefinition">The key alarm definition.</param>
		/// <param name="keyMeterValidationRule">The key meter validation rule.</param>
		/// <param name="existing">if set to <c>true</c>
		/// the method will only return MeterClassifcation for meter that are attached to the chart,
		/// else it will return all available MeterClassification in the spatial filter of the chart.</param>
		/// <param name="filterSpatial">The current filter spatial of the Chart.</param>
		/// <returns></returns>
		public List<ClassificationItem> MeterClassification_GetChildren(string key, string keyChart, string keyAlarmDefinition, string keyMeterValidationRule, bool existing, CrudStore<Location> filterSpatial) {
			var retVal = ((ClassificationItemBrokerDB)m_brokerDB).MeterClassification_GetChildren(key, keyChart, keyAlarmDefinition, keyMeterValidationRule, existing, filterSpatial);
			return retVal;
		}

		/// <summary>
		/// Get the max level of Meter Classification in the ClassificationItem Hierarchy.
		/// </summary>
		/// <returns></returns>
		public int MeterClassification_GetMaxLevel() {
			return ((ClassificationItemBrokerDB)m_brokerDB).MeterClassification_GetMaxLevel();
		}

		/// <summary>
		/// Cleans the pset values for the given classification item and psets
		/// </summary>
		/// <param name="keyclassificationItemXml">The keyclassification item XML.</param>
		/// <param name="keyPset">The key pset.</param>
		/// <param name="psetAttributeListKeysXml">The pset attribute list keys XML.</param>
		public void ClassificationItem_CleanPsetValue(string keyclassificationItemXml, string keyPset, string psetAttributeListKeysXml) {
			((ClassificationItemBrokerDB)m_brokerDB).ClassificationItem_CleanPsetValue(keyclassificationItemXml, keyPset, psetAttributeListKeysXml);
		}


		/// <summary>
		/// Cleans all the pset values for the classification item
		/// </summary>
		/// <param name="keyclassificationItemXml">The key.</param>
		/// <returns></returns>
		public void ClassificationItem_CleanAllPsetValue(string keyclassificationItemXml) {
			((ClassificationItemBrokerDB)m_brokerDB).ClassificationItem_CleanAllPsetValue(keyclassificationItemXml);

		}




		/// <summary>
		/// Creates an association between a ClassificationItem and a WorkflowAssemblyDefinition.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <param name="formResponse">The form response.</param>
		/// <returns></returns>
		protected override ClassificationItem Update(ClassificationItem item, out FormResponse formResponse) {
			ClassificationItem result = base.Update(item, out formResponse);
			ClearClassificationItemDefinitionCache();
			return result;
		}

		/// <summary>
		/// An override for the Mapping_Create method that ensures the classifications are rebuilt after they are modified.
		/// </summary>
		/// <param name="mappingRecord">The mapping record.</param>
		/// <param name="localId">The local id.</param>
		/// <param name="formResponse">The form response.</param>
		protected override ClassificationItem Mapping_Create(MappingRecord<ClassificationItem> mappingRecord, string localId, out FormResponse formResponse) {
			ClassificationItem result = base.Mapping_Create(mappingRecord, localId, out formResponse);
			ClearClassificationItemDefinitionCache();
			return result;
		}

		/// <summary>
		/// An override for the Mapping_Update method that ensures the classifications are rebuilt after they are modified.
		/// </summary>
		/// <param name="mappingRecord">The mapping record.</param>
		/// <param name="localId">The local id.</param>
		/// <param name="formResponse">The form response.</param>
		/// <returns></returns>
		protected override ClassificationItem Mapping_Update(MappingRecord<ClassificationItem> mappingRecord, string localId, out FormResponse formResponse) {
			ClassificationItem result = base.Mapping_Update(mappingRecord, localId, out formResponse);
			ClearClassificationItemDefinitionCache();
			return result;
		}

		/// <summary>
		/// An override for the Mapping_Delete method that ensures the classifications are rebuilt after they are modified.
		/// </summary>
		/// <param name="mappingRecord">The mapping record.</param>
		/// <param name="localId">The local id.</param>
		/// <param name="formResponse">The form response.</param>
		protected override void Mapping_Delete(MappingRecord<ClassificationItem> mappingRecord, string localId, out FormResponse formResponse) {
			base.Mapping_Delete(mappingRecord, localId, out formResponse);
			ClearClassificationItemDefinitionCache();
		}

		/// <summary>
		/// Gets a classification item with hierachy fields.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <returns></returns>
		public ClassificationItem GetItemFromHierarchy(string key) {
			return ((ClassificationItemBrokerDB)m_brokerDB).GetItemFromHierarchy(key);
		}

		/// <summary>
		/// Gets all the classification items which are root nodes issued from ClassificationDefinition.
		/// </summary>
		/// <returns></returns>
		public List<ClassificationItem> GetAllClassificationItemDefinition() {
			return ((ClassificationItemBrokerDB)m_brokerDB).GetAllClassificationItemDefinition();
		}


		/// <summary>
		/// Gets a dictionnary for the business entity ClassificationItem by key word.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <returns>
		/// The list of items.
		/// </returns>
		public JsonStore<ClassificationItem> GetAllClassificationItem(PagingParameter paging) {
			int total;
			return ((ClassificationItemBrokerDB)m_brokerDB).GetAll(paging, out total).ToJsonStore();
		}

		/// <summary>
		/// Gets all the classificaion items from their relationships.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <returns>
		/// The list of items.
		/// </returns>
		public JsonStore<ClassificationItem> GetListRelationship(PagingParameter paging) {
			int total;
			return ((ClassificationItemBrokerDB)m_brokerDB).GetListRelationship(paging, out total).ToJsonStore(total);
		}

		/// <summary>
		/// Gets the category root.
		/// </summary>
		/// <param name="category">The category. Use ClassificationCategory struct here.</param>
		/// <returns></returns>
		public ClassificationItem GetCategoryRoot(string category) {
			var classifications = ((ClassificationItemBrokerDB)m_brokerDB).GetAllClassificationItemDefinition();
			var rootClassification = classifications.FirstOrDefault(p => IsClassificationItemOfCategory(p, category));
			return rootClassification;
		}

		/// <summary>
		/// Determines whether the classification item is of specified category.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <param name="category">The category. Use ClassificationCategory struct here.</param>
		public bool IsClassificationItemOfCategory(ClassificationItem item, string category) {
			var retVal = String.Equals(item.Category, category, StringComparison.CurrentCultureIgnoreCase);
			return retVal;
		}

		/// <summary>
		/// Clears the classification items definition in cache.
		/// </summary>
		public static void ClearClassificationItemDefinitionCache() {
			CacheService.Remove(CacheKey.const_cache_classifications);
		}

		/// <summary>
		/// Populates the list.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <param name="loadMode">The load mode.</param>
		/// <param name="fields">The fields.</param>
		public override void PopulateList(ClassificationItem item, EntityLoadMode loadMode, params string[] fields) {
			foreach (var field in fields) {
				if (field.ContainsAny("Children")) {
					int total;
					var pagingParameter = new PagingParameter {
						filters = new List<GridFilter>
						{
							new GridFilter
							{
								field = "KeyParent",
								data = new GridData
								{
									comparison = "eq",
									type = "string",
									value = item.KeyClassificationItem
								}
							}
						}
					};
					var classificationItems = GetAll(pagingParameter, PagingLocation.Database, out total);
					ResolveForExport(classificationItems, loadMode);
					item.Children = classificationItems;
				}
			}
		}
	}
}

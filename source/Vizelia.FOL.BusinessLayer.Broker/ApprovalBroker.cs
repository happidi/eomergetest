﻿using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.DataLayer.BrokerDB;

namespace Vizelia.FOL.BusinessLayer.Broker {

	/// <summary>
	/// The broker for business entity Approval
	/// </summary>
	public class ApprovalBroker : BaseBroker<Approval, ApprovalBrokerDB> {
		/// <summary>
		/// Gets a list of Approvals by its ActionRequest key.
		/// </summary>
		/// <param name="keyActionRequest">The key of the ActionRequest.</param>
		/// <returns></returns>
		public virtual JsonStore<Approval> GetListByKeyActionRequest(string keyActionRequest) {
			var retVal = ((ApprovalBrokerDB)m_brokerDB).GetListByKeyActionRequest(keyActionRequest).ToJsonStore();
			return retVal;
		}
	}

}

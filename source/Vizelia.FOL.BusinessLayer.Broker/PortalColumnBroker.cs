﻿using System.Collections.Generic;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.DataLayer.BrokerDB;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for business entity PortalColumn.
	/// </summary>
	public class PortalColumnBroker : MappingEnabledBroker<PortalColumn, PortalColumnBrokerDB> {

		/// <summary>
		/// Get the list of saved PortalColumn from a Portal tab.
		/// </summary>
		/// <param name="KeyPortalTab">The key of the Portal tab.</param>
		public List<PortalColumn> GetListByKeyPortalTab(string KeyPortalTab) {
			return ((PortalColumnBrokerDB)m_brokerDB).GetListByKeyPortalTab(KeyPortalTab);
		}

		/// <summary>
		/// Gets a store of PortalColumn for a specific Portal tab.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyPortalTab">The Key of the Portal tab.</param>
		/// <returns>A JsonStore.</returns>
		public JsonStore<PortalColumn> GetStoreByKeyPortalTab(PagingParameter paging, string KeyPortalTab) {
			int total;
			return ((PortalColumnBrokerDB)m_brokerDB).GetAllPagingByKeyPortalTab(paging, KeyPortalTab, out total).ToJsonStore(total, paging);
		}


		/// <summary>
		/// Saves the PortalTab PortalColumn.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="portalcolumn">The PortalColumn store.</param>
		/// <returns></returns>
		public List<PortalColumn> SavePortalTabPortalColumn(PortalTab entity, CrudStore<PortalColumn> portalcolumn) {
			return ((PortalColumnBrokerDB)m_brokerDB).SavePortalTabPortalColumn(entity, portalcolumn);
		}


		/// <summary>
		/// Populate the chidren items specified as fields. "*" indicates that all fields should be populated.
		/// </summary>
		/// <param name="item">The item</param>
		/// <param name="loadMode">The load mode.</param>
		/// <param name="fields">The list of fields. "*" indicates all fields.</param>
		public override void PopulateList(PortalColumn item, EntityLoadMode loadMode, params string[] fields) {
			foreach (string field in fields) {
				if (field.ContainsAny("*", "Portlets")) {
					PortletBroker brokerPortlets = Helper.CreateInstance<PortletBroker>();
					var portlets = brokerPortlets.GetListByKeyPortalColumn(item.KeyPortalColumn);
					if (portlets != null && portlets.Count > 0) {
						brokerPortlets.PopulateList(portlets, loadMode, fields);
						brokerPortlets.ResolveForExport(portlets, loadMode);
						item.Portlets = portlets;
					}
					break;
				}
			}
		}

		/// <summary>
		/// Copy an existing PortalColumn.
		/// </summary>
		/// <param name="item">the item to be copied.</param>
		/// <param name="formResponse">The response of the operation.</param>
		/// <param name="Creator">The creator.</param>
		/// <param name="incrementTitle">if set to <c>true</c> [increment title].</param>
		/// <returns></returns>
		public override PortalColumn Copy(PortalColumn item, out FormResponse formResponse, string Creator = null, bool incrementTitle = true) {
			var newItem = base.Copy(item, out formResponse, Creator);
			newItem.Portlets = new List<Portlet>();
			if (formResponse.success) {
				PortletBroker brokerPortlets = Helper.CreateInstance<PortletBroker>();
				foreach (var portlet in item.Portlets) {
					FormResponse response;
					portlet.KeyPortalColumn = newItem.KeyPortalColumn;
					newItem.Portlets.Add(brokerPortlets.Copy(portlet, out response, Creator, incrementTitle));
				}
			}
			return newItem;
		}

		/// <summary>
		/// Resolves the references for export.
		/// </summary>
		/// <param name="entity">The entity.</param>
		protected override void ResolveReferencesForExport(PortalColumn entity) {
			ResolveParentReferenceForExport<PortalTab>(e => e.KeyPortalTab, entity);
		}

		/// <summary>
		/// Resolves the references during mapping.
		/// </summary>
		/// <param name="mappingRecord">The mapping record.</param>
		/// <param name="persistedEntity">The persisted entity.</param>
		protected override void ResolveReferencesForImport(MappingRecord<PortalColumn> mappingRecord, PortalColumn persistedEntity) {
			ResolveParentReferenceForImport<PortalTab>(e => e.KeyPortalTab, mappingRecord.Entity, persistedEntity);
		}
	}

}
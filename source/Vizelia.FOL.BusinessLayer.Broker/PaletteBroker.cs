﻿using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.DataLayer.BrokerDB;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for business entity Palette.
	/// </summary>
	public class PaletteBroker : MappingEnabledBroker<Palette, PaletteBrokerDB> {
		/// <summary>
		/// Populate the chidren items specified as fields. "*" indicates that all fields should be populated.
		/// </summary>
		/// <param name="item">The item</param>
		/// <param name="loadMode">The load mode.</param>
		/// <param name="fields">The list of fields. "*" indicates all fields.</param>
		public override void PopulateList(Palette item, EntityLoadMode loadMode, params string[] fields) {
			foreach (string field in fields) {
				if (field.ContainsAny("*", "Colors")) {
					PaletteColorBroker brokerColor = Helper.CreateInstance<PaletteColorBroker>();
					var colors = brokerColor.GetListByKeyPalette(item.KeyPalette);
					if (colors != null && colors.Count > 0) {
						brokerColor.ResolveForExport(colors, loadMode);
						item.Colors = colors;
					}
					break;
				}
			}
		}

		/// <summary>
		/// Resolves the references for export.
		/// </summary>
		/// <param name="entity">The entity.</param>
		protected override void ResolveReferencesForExport(Palette entity) {
			// Do nothing.
		}

		/// <summary>
		/// Resolves the references during mapping.
		/// </summary>
		/// <param name="mappingRecord">The mapping record.</param>
		/// <param name="persistedEntity">The persisted entity.</param>
		protected override void ResolveReferencesForImport(MappingRecord<Palette> mappingRecord, Palette persistedEntity) {
			// Do nothing.
		}

		/// <summary>
		/// Copies the specified item.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <param name="formResponse">The form response.</param>
		/// <param name="Creator">The creator.</param>
		/// <param name="incrementTitle">if set to <c>true</c> [increment title].</param>
		/// <returns></returns>
		public override Palette Copy(Palette item, out FormResponse formResponse, string Creator = null, bool incrementTitle = true) {
			
			var brokerColor = Helper.CreateInstance<PaletteColorBroker>();
			var colors = brokerColor.GetListByKeyPalette(item.KeyPalette);
			
			var newItem = base.Copy(item, out formResponse, Creator);
			if (formResponse.success) {
				if (colors != null && colors.Count > 0) {
					foreach (var color in colors) {
						FormResponse response;
						color.KeyPalette = newItem.KeyPalette;
						brokerColor.Copy(color, out response, Creator, incrementTitle);
					}
				}
			}
			return newItem;
		}

	}

}
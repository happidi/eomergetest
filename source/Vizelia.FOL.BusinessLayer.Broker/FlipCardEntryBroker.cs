﻿using System.Collections.Generic;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.DataLayer.BrokerDB;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for business entity FlipCardEntry.
	/// </summary>
	public class FlipCardEntryBroker : MappingEnabledBroker<FlipCardEntry, FlipCardEntryBrokerDB> {

		/// <summary>
		/// Get the list of saved FlipCardEntry from a FlipCard
		/// </summary>
		/// <param name="KeyFlipCard">The key of the FlipCard.</param>
		public List<FlipCardEntry> GetListByKeyFlipCard(string KeyFlipCard) {
			return ((FlipCardEntryBrokerDB)m_brokerDB).GetListByKeyFlipCard(KeyFlipCard);
		}

		/// <summary>
		/// Gets a store of FlipCardEntry for a specific FlipCard.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyFlipCard">The Key of the FlipCard.</param>
		/// <returns>A JsonStore.</returns>
		public JsonStore<FlipCardEntry> GetStoreByKeyFlipCard(PagingParameter paging, string KeyFlipCard) {
			int total;
			return ((FlipCardEntryBrokerDB)m_brokerDB).GetAllPagingByKeyFlipCard(paging, KeyFlipCard, out total).ToJsonStore(total, paging);
		}

		/// <summary>
		/// Saves the FlipCard FlipCardEntrys.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="FlipCardEntry">The FlipCardEntry store.</param>
		/// <returns></returns>
		public List<FlipCardEntry> SaveFlipCardFlipCardEntry(FlipCard entity, CrudStore<FlipCardEntry> FlipCardEntry) {
			return ((FlipCardEntryBrokerDB)m_brokerDB).SaveFlipCardFlipCardEntry(entity, FlipCardEntry);
		}

		/// <summary>
		/// Populate the chidren items specified as fields. "*" indicates that all fields should be populated.
		/// </summary>
		/// <param name="item">The item</param>
		/// <param name="loadMode">The load mode.</param>
		/// <param name="fields">The list of fields. "*" indicates all fields.</param>
		public override void PopulateList(FlipCardEntry item, EntityLoadMode loadMode, params string[] fields) {
			foreach (string field in fields) {
				if (field.ContainsAny("*", "Entity")) {
					if (item != null && !string.IsNullOrEmpty(item.KeyEntity)) {
						IBusinessEntityBroker broker = GetBusinessEntityBroker(item.TypeEntity);
						BaseBusinessEntity entity = broker.GetItem(item.KeyEntity);
						// should there be broker.populatelist("*") here?
						var mappingBroker = broker as IMappingBroker;

						if (mappingBroker != null) {
							mappingBroker.ResolveForExport(entity, loadMode);
						}

						item.Entity = entity;

					}
					break;
				}
			}
		}

		/// <summary>
		/// Resolves the references for import.
		/// </summary>
		/// <param name="mappingRecord">The mapping record.</param>
		/// <param name="persistedEntity">The persisted entity.</param>
		protected override void ResolveReferencesForImport(MappingRecord<FlipCardEntry> mappingRecord, FlipCardEntry persistedEntity) {
			ResolveParentReferenceForImport<FlipCard>(e => e.KeyFlipCard, mappingRecord.Entity, persistedEntity);
			ResolvePortletReferenceForImport(mappingRecord, persistedEntity);
		}

		/// <summary>
		/// Resolves the references for export.
		/// </summary>
		/// <param name="entity">The entity.</param>
		protected override void ResolveReferencesForExport(FlipCardEntry entity) {
			ResolveParentReferenceForExport<FlipCard>(e => e.KeyFlipCard, entity);
			ReolvePortletReferenceForExport(entity);
		}

		/// <summary>
		/// Reolves the portlet reference for export.
		/// </summary>
		/// <param name="entity">The entity.</param>
		private void ReolvePortletReferenceForExport(FlipCardEntry entity) {
			if (!string.IsNullOrWhiteSpace(entity.KeyEntity)) {
				switch (entity.TypeEntity) {
					case "Vizelia.FOL.BusinessEntities.AlarmTable":
						ResolveParentReferenceForExport<AlarmTable>(e => e.KeyEntity, entity);
						break;
					case "Vizelia.FOL.BusinessEntities.Chart":
						ResolveParentReferenceForExport<Chart>(e => e.KeyEntity, entity);
						break;
					case "Vizelia.FOL.BusinessEntities.DrawingCanvas":
						ResolveParentReferenceForExport<DrawingCanvas>(e => e.KeyEntity, entity);
						break;
					case "Vizelia.FOL.BusinessEntities.FlipCard":
						ResolveParentReferenceForExport<FlipCard>(e => e.KeyEntity, entity);
						break;
					case "Vizelia.FOL.BusinessEntities.Map":
						ResolveParentReferenceForExport<Map>(e => e.KeyEntity, entity);
						break;
					case "Vizelia.FOL.BusinessEntities.WeatherLocation":
						ResolveParentReferenceForExport<WeatherLocation>(e => e.KeyEntity, entity);
						break;
					case "Vizelia.FOL.BusinessEntities.WebFrame":
						ResolveParentReferenceForExport<WebFrame>(e => e.KeyEntity, entity);
						break;
				}
			}
		}

		/// <summary>
		/// Resolves the portlet reference for import.
		/// </summary>
		/// <param name="mappingRecord">The mapping record.</param>
		/// <param name="persistedEntity">The persisted entity.</param>
		private void ResolvePortletReferenceForImport(MappingRecord<FlipCardEntry> mappingRecord, FlipCardEntry persistedEntity) {
			if (!string.IsNullOrWhiteSpace(mappingRecord.Entity.KeyEntity)) {
				switch (mappingRecord.Entity.TypeEntity) {
					case "Vizelia.FOL.BusinessEntities.AlarmTable":
						ResolveParentReferenceForImport<AlarmTable>(e => e.KeyEntity, mappingRecord.Entity, persistedEntity);
						break;
					case "Vizelia.FOL.BusinessEntities.Chart":
						ResolveParentReferenceForImport<Chart>(e => e.KeyEntity, mappingRecord.Entity, persistedEntity);
						break;
					case "Vizelia.FOL.BusinessEntities.DrawingCanvas":
						ResolveParentReferenceForImport<DrawingCanvas>(e => e.KeyEntity, mappingRecord.Entity, persistedEntity);
						break;
					case "Vizelia.FOL.BusinessEntities.FlipCard":
						ResolveParentReferenceForImport<FlipCard>(e => e.KeyEntity, mappingRecord.Entity, persistedEntity);
						break;
					case "Vizelia.FOL.BusinessEntities.Map":
						ResolveParentReferenceForImport<Map>(e => e.KeyEntity, mappingRecord.Entity, persistedEntity);
						break;
					case "Vizelia.FOL.BusinessEntities.WeatherLocation":
						ResolveParentReferenceForImport<WeatherLocation>(e => e.KeyEntity, mappingRecord.Entity, persistedEntity);
						break;
					case "Vizelia.FOL.BusinessEntities.WebFrame":
						ResolveParentReferenceForImport<WebFrame>(e => e.KeyEntity, mappingRecord.Entity, persistedEntity);
						break;
				}
			}
		}
	}

}
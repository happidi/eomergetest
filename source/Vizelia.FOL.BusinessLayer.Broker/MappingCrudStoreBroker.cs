﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for a mapping crud store.
	/// This broker gets a MappingCrudStore that holds entities that only have local ids populated.
	/// It resolves all the entities and then calls the children entities broker to perform the association with the parent entity.
	/// </summary>
	/// <typeparam name="TParent">The type of the parent.</typeparam>
	/// <typeparam name="TChildren">The type of the children.</typeparam>
	public class MappingCrudStoreBroker<TParent, TChildren> : MarshalByRefObject, IMappingBroker
		where TParent : BaseBusinessEntity, new()
		where TChildren : BaseBusinessEntity {
		/// <summary>
		/// Executes the mapping for the specified entity.
		/// </summary>
		/// <param name="mappingRecord">The mapping record.</param>
		/// <param name="action">The action.</param>
		/// <param name="formResponse">The form response.</param>
		private void Mapping(IMappingRecord mappingRecord, string action, out FormResponse formResponse) {
			var crudStore = (MappingCrudStore<TParent, TChildren>)mappingRecord.Entity;

			// Resolve the children
			ResolveList(crudStore.create);
			ResolveList(crudStore.update);
			ResolveList(crudStore.destroy);

			// Resolve the parent
			var parent = ResolveParent(crudStore);

			// Use the children entities broker to associate them to the parent
			AssociateChildrenWithParent(parent, crudStore);

			formResponse = new FormResponse {success = true};
		}

		/// <summary>
		/// Gets all items fully populated and resolved for export.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="total">The total of results.</param>
		/// <returns>
		/// A list of items
		/// </returns>
		public List<BaseBusinessEntity> GetAllForExport(PagingParameter paging, PagingLocation location, out int total) {
			throw new NotSupportedException("This object is not supposed to be retreived for export.");
		}

		/// <summary>
		/// Resolves the list for export.
		/// </summary>
		/// <param name="list">The list.</param>
		/// <param name="loadMode">The load mode.</param>
		/// <returns></returns>
		public void ResolveForExport(List<BaseBusinessEntity> list, EntityLoadMode loadMode) {
			throw new NotImplementedException();
		}

		/// <summary>
		/// Resolves the entity for export.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="loadMode">The load mode.</param>
		/// <returns></returns>
		public void ResolveForExport(BaseBusinessEntity entity, EntityLoadMode loadMode) {
			throw new NotImplementedException();
		}

		/// <summary>
		/// Gets the maximum size of the batch.
		/// </summary>
		/// <value>
		/// The maximum size of the batch.
		/// </value>
		public int MaximumBatchSize {
			get { return 1; }
		}

		/// <summary>
		/// Gets a value indicating whether this broker writes failures to the trace or not.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if this broker writes failures to the trace; otherwise, <c>false</c>.
		/// </value>
		public bool WritesFailuresToTrace {
			get { return false; }
		}

		/// <summary>
		/// Executes the mapping for the specified entities.
		/// </summary>
		/// <param name="mappingRecords">The mapping records.</param>
		/// <param name="action">The action.</param>
		/// <param name="mappingContext">The mapping context</param>
		/// <param name="formResponses">The form responses.</param>
		public void Mapping(IEnumerable<IMappingRecord> mappingRecords, string action,
							MappingContext mappingContext,
							out IEnumerable<Tuple<string, FormResponse>> formResponses){
			var singleFormResponses = new List<Tuple<string, FormResponse>>();

			foreach (var mappingRecord in mappingRecords) {
				FormResponse formResponse;
				Mapping(mappingRecord, action, out formResponse);
				singleFormResponses.Add(new Tuple<string, FormResponse>(mappingRecord.LocalId, formResponse));
			}

			formResponses = singleFormResponses;
		}

		/// <summary>
		/// Gets the entity fully populated, resolved and ready for export.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <returns></returns>
		public BaseBusinessEntity GetEntityForExport(string key) {
			throw new NotSupportedException("This object is not supposed to be retreived for export.");
		}

		/// <summary>
		/// Prepares to export the IMappableEntity.
		/// </summary>
		/// <param name="entity">The entity.</param>
		public void PrepareToExport(IMappableEntity entity) {
			throw new NotSupportedException("This object is not supposed to be retreived for export.");
		}

		/// <summary>
		/// Associates the children with parent by calling the children entities broker.
		/// </summary>
		/// <param name="parent">The parent.</param>
		/// <param name="crudStore">The crud store.</param>
		private void AssociateChildrenWithParent(TParent parent, MappingCrudStore<TParent, TChildren> crudStore) {
			IBroker<TChildren> broker = BrokerFactory.CreateBroker<TChildren>();

			Type brokerConcreteType = broker.GetType();
			MethodInfo associationMethod = brokerConcreteType.GetMethods(BindingFlags.Public | BindingFlags.Instance).Single(
				info => IsAssociationMethod(info, crudStore.PropertyName));

			associationMethod.Invoke(broker, new object[] {parent, crudStore});
		}

		/// <summary>
		/// Determines whether the given method is the one that associates between children entities and parent entity.
		/// </summary>
		/// <param name="method">The method.</param>
		/// <param name="propertyName">Name of the property.</param>
		/// <returns>
		///   <c>true</c> if the given method is the one that associates between children entities and parent entity; otherwise, <c>false</c>.
		/// </returns>
		private bool IsAssociationMethod(MethodInfo method, string propertyName) {
			ParameterInfo[] parameters = method.GetParameters();
			
			// If a property name was given, we'll use it to search the Save methods.
			bool isMatchingMethod = 
					method.Name.StartsWith("Save") &&
					(string.IsNullOrWhiteSpace(propertyName) || method.Name.Contains(propertyName)) &&
					parameters.Length == 2 &&
					parameters[0].ParameterType == typeof(TParent) &&
					parameters[1].ParameterType == typeof(CrudStore<TChildren>);

			return isMatchingMethod;
		}

		/// <summary>
		/// Resolves the parent entity.
		/// </summary>
		/// <param name="crudStore">The crud store.</param>
		private TParent ResolveParent(MappingCrudStore<TParent, TChildren> crudStore) {
			IBroker<TParent> broker = BrokerFactory.CreateBroker<TParent>();

			var parentEntity = crudStore.GetParentEntity();
			var localId = parentEntity.GetKey();
			var resolvedParent = broker.GetItemByLocalId(localId);

			if(resolvedParent == null) {
				throw new VizeliaException(string.Format("Could not resolve MappingCrudStore parent entity of type '{0}' with local id '{1}'", typeof(TParent).Name, localId));
			}

			return resolvedParent;
		}

		/// <summary>
		/// Resolves a list of entities.
		/// The entity list contains entities that only have local ids assigned.
		/// This method loads resolved entities by the given entities local ids and replaces them.
		/// </summary>
		/// <param name="children">The children.</param>
		private void ResolveList(List<TChildren> children) {
			IBroker<TChildren> broker = BrokerFactory.CreateBroker<TChildren>();

			// The local id is stored in the Key property of the entity.
			IEnumerable<string> localIds = children.Select(c => c.GetKey());
			List<TChildren> resolvedChildren = localIds.Select(localId => broker.GetItemByLocalId(localId, true)).ToList();

			children.Clear();
			children.AddRange(resolvedChildren);
		}
	}
}
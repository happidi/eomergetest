﻿using System.Collections.Generic;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using Microsoft.Practices.EnterpriseLibrary.Validation.PolicyInjection;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Localization;
using Vizelia.FOL.DataLayer.BrokerDB;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for business entity Link.
	/// </summary>
	public class LinkBroker : MappingEnabledBroker<Link, LinkBrokerDB> {
		/// <summary>
		/// Resolves the references during mapping.
		/// </summary>
		/// <param name="mappingRecord">The mapping record.</param>
		/// <param name="persistedEntity">The persisted entity.</param>
		protected override void ResolveReferencesForImport(MappingRecord<Link> mappingRecord, Link persistedEntity) {
			ResolveParentReferenceForImport<Link>(e => e.KeyParent, mappingRecord.Entity, persistedEntity);
			ResolveParentReferenceForImport<DynamicDisplayImage>(e => e.KeyImage, mappingRecord.Entity, persistedEntity);
		}

		/// <summary>
		/// Resolves the references for export.
		/// </summary>
		/// <param name="entity">The entity.</param>
		protected override void ResolveReferencesForExport(Link entity) {
			ResolveParentReferenceForExport<Link>(e => e.KeyParent, entity);
			ResolveParentReferenceForExport<DynamicDisplayImage>(e => e.KeyImage, entity);
		}

		/// <summary>
		/// Validates the specified item.
		/// </summary>
		/// <param name="item">The item.</param>
		protected override void Validate(Link item) {
			base.Validate(item);
			if (item.KeyLink != null) {
				int total;
				var gridData = new GridData {
					comparison = "eq",
					type = "string",
					value = item.KeyLink
				};
				m_brokerDB.GetAll(new PagingParameter {
					start = 0,
					limit = 1,
					filter = "Name",
					filters = new List<GridFilter> { new GridFilter { data = gridData, field = "KeyParent" } }
				}, PagingLocation.Database, out total);
				if (total > 0) {
					var validationResults = new ValidationResults();
					validationResults.AddResult(new ValidationResult(Langue.error_msg_link_grandchild, item, "KeyParent", "", null));
					throw new ArgumentValidationException(validationResults, "item");
				}
			}

			if (item.LinkValue == null) {
				throw new MappingException("Link value can not be empty");
			}

			if (item.LocalId == null) {
				throw new MappingException("Id can not be empty");
			}
		}

		/// <summary>
		/// Gets the store root only.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="location">The location.</param>
		/// <returns></returns>
		public JsonStore<Link> GetStoreRootOnly(PagingParameter paging, PagingLocation location) {
			int total;
			return ((LinkBrokerDB)m_brokerDB).GetStoreRootOnly(paging, location, out total).ToJsonStore(total, paging);
		}
	}
}
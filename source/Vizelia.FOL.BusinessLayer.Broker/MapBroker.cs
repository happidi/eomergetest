﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.DataLayer.BrokerDB;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Localization;
using Vizelia.FOL.BusinessEntities.Mapping;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for business entity Map.
	/// </summary>
	public class MapBroker : MappingEnabledBroker<Map, MapBrokerDB> {
		/// <summary>
		/// Resolves the references for export.
		/// </summary>
		/// <param name="entity">The entity.</param>
		protected override void ResolveReferencesForExport(Map entity) {
			ResolveParentReferenceForExport<ClassificationItem>(e => e.KeyClassificationItemAlarmDefinition, entity);
			ResolveLocationReferenceForExport(entity);
		}

		/// <summary>
		/// Resolves the references during mapping.
		/// </summary>
		/// <param name="mappingRecord">The mapping record.</param>
		/// <param name="persistedEntity">The persisted entity.</param>
		protected override void ResolveReferencesForImport(MappingRecord<Map> mappingRecord, Map persistedEntity) {
			ResolveParentReferenceForImport<ClassificationItem>(e => e.KeyClassificationItemAlarmDefinition, mappingRecord.Entity, persistedEntity);
			ResolveLocationReferenceForImport(mappingRecord, persistedEntity);
		}
	}
}
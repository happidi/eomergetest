﻿using System.Collections.Generic;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.DataLayer.BrokerDB;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for business entity Report Subscription.
	/// </summary>
	public class ReportSubscriptionBroker : BaseBroker<ReportSubscription, ReportSubscriptionBrokerDB> {
		/// <summary>
		/// Gets all by report path.
		/// </summary>
		/// <param name="reportPath">The report path.</param>
		/// <param name="paging">The paging.</param>
		/// <param name="totalCount">The total count.</param>
		/// <returns></returns>
		public List<ReportSubscription> GetAllByReportPath(string reportPath, PagingParameter paging, out int totalCount) {
			return ((ReportSubscriptionBrokerDB)m_brokerDB).GetAllByReportPath(reportPath, paging, out totalCount);
		}

		/// <summary>
		/// Deletes an item from database.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <param name="persistedItem">The persisted item.</param>
		/// <returns>
		/// True if the delete operation succeeded, false otherwise.
		/// </returns>
		public override bool Delete(ReportSubscription item, ReportSubscription persistedItem = null) {
			var retVal = m_brokerDB.Delete(item);
			return retVal;
		}
	}
}
﻿using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.DataLayer.BrokerDB;
using Vizelia.FOL.Common;
using System.Collections.Generic;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for business entity FlipCard.
	/// </summary>
	public class FlipCardBroker : MappingEnabledBroker<FlipCard, FlipCardBrokerDB> {
		/// <summary>
		/// Populate the chidren items specified as fields. "*" indicates that all fields should be populated.
		/// </summary>
		/// <param name="item">The item</param>
		/// <param name="loadMode">The load mode.</param>
		/// <param name="fields">The list of fields. "*" indicates all fields.</param>
		public override void PopulateList(FlipCard item, EntityLoadMode loadMode, params string[] fields) {
			foreach (string field in fields) {
				if (field.ContainsAny("*", "Cards")) {
					var brokerEntry = Helper.CreateInstance<FlipCardEntryBroker>();
					var pages = brokerEntry.GetListByKeyFlipCard(item.KeyFlipCard);
					brokerEntry.PopulateList(pages, "*");
					//brokerEntry.ResolveForExport(pages, loadMode);
					item.Cards = pages;
				}
			}
		}

		/// <summary>
		/// Copies the specified item.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <param name="formResponse">The form response.</param>
		/// <param name="Creator">The creator.</param>
		/// <param name="incrementTitle">if set to <c>true</c> [increment title].</param>
		/// <returns></returns>
		public override FlipCard Copy(FlipCard item, out FormResponse formResponse, string Creator = null, bool incrementTitle = true) {
			var brokerEntry = Helper.CreateInstance<FlipCardEntryBroker>();
			var cards = brokerEntry.GetListByKeyFlipCard (item.KeyFlipCard);

			var newItem = base.Copy(item, out formResponse, Creator);
			newItem.Cards = new List<FlipCardEntry>();
			if (formResponse.success) {
				if (cards != null && cards.Count > 0) {
					foreach (var card in cards) {
						FormResponse response;
						card.KeyFlipCard = newItem.KeyFlipCard;
						var newcard=brokerEntry.Copy(card, out response, Creator, incrementTitle);
						newcard = brokerEntry.GetItem(newcard.KeyFlipCardEntry, "*");
						newItem.Cards.Add(newcard);
					}
				}
				formResponse.data = newItem;
			}
			return newItem;
		}

		/// <summary>
		/// Resolves the references for import.
		/// </summary>
		/// <param name="mappingRecord">The mapping record.</param>
		/// <param name="persistedEntity">The persisted entity.</param>
		protected override void ResolveReferencesForImport(MappingRecord<FlipCard> mappingRecord, FlipCard persistedEntity) {
			//Nothing to do here.
		}

		/// <summary>
		/// Resolves the references for export.
		/// </summary>
		/// <param name="entity">The entity.</param>
		protected override void ResolveReferencesForExport(FlipCard entity) {
			//Nothing to do here.
		}
	}

}
﻿using System.Collections.Generic;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.DataLayer.BrokerDB;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for business entity EnergyCertificateCategory.
	/// </summary>
	public class EnergyCertificateCategoryBroker : MappingEnabledBroker<EnergyCertificateCategory, EnergyCertificateCategoryBrokerDB> {

		/// <summary>
		/// Get the list of saved EnergyCertificateCategory from a EnergyCertificate
		/// </summary>
		/// <param name="KeyEnergyCertificate">The key of the EnergyCertificate.</param>
		public List<EnergyCertificateCategory> GetListByKeyEnergyCertificate(string KeyEnergyCertificate) {
			return ((EnergyCertificateCategoryBrokerDB)m_brokerDB).GetListByKeyEnergyCertificate(KeyEnergyCertificate);
		}

		/// <summary>
		/// Gets a store of EnergyCertificateCategory for a specific EnergyCertificate.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyEnergyCertificate">The Key of the EnergyCertificate.</param>
		/// <returns>A JsonStore.</returns>
		public JsonStore<EnergyCertificateCategory> GetStoreByKeyEnergyCertificate(PagingParameter paging, string KeyEnergyCertificate) {
			int total;
			return ((EnergyCertificateCategoryBrokerDB)m_brokerDB).GetAllPagingByKeyEnergyCertificate(paging, KeyEnergyCertificate, out total).ToJsonStore(total, paging);
		}


		/// <summary>
		/// Saves the EnergyCertificate EnergyCertificateCategorys.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="EnergyCertificateCategory">The EnergyCertificateCategory store.</param>
		/// <returns></returns>
		public List<EnergyCertificateCategory> SaveEnergyCertificateEnergyCertificateCategory(EnergyCertificate entity, CrudStore<EnergyCertificateCategory> EnergyCertificateCategory) {
			return ((EnergyCertificateCategoryBrokerDB)m_brokerDB).SaveEnergyCertificateEnergyCertificateCategory(entity, EnergyCertificateCategory);
		}

		/// <summary>
		/// Resolves the references during mapping.
		/// </summary>
		/// <param name="mappingRecord">The mapping record.</param>
		/// <param name="persistedEntity">The persisted entity.</param>
		protected override void ResolveReferencesForImport(MappingRecord<EnergyCertificateCategory> mappingRecord, EnergyCertificateCategory persistedEntity) {
			ResolveParentReferenceForImport<EnergyCertificate>(e => e.KeyEnergyCertificate, mappingRecord.Entity, persistedEntity);
		}

		/// <summary>
		/// Resolves the references for export.
		/// </summary>
		/// <param name="entity">The entity.</param>
		protected override void ResolveReferencesForExport(EnergyCertificateCategory entity) {
			ResolveParentReferenceForExport<EnergyCertificate>(e => e.KeyEnergyCertificate, entity);
		}
	}
}


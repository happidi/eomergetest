﻿using System;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.DataLayer.BrokerDB;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using System.Collections.Generic;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for business entity WorkflowInstanceDefinition.
	/// </summary>
	public class WorkflowInstanceDefinitionBroker : MappingEnabledBroker<WorkflowInstanceDefinition, WorkflowInstanceDefinitionBrokerDB> {

		/// <summary>
		/// Gets a item by its instanceId.
		/// </summary>
		/// <param name="instanceId">The instanceId of the item.</param>
		/// <returns></returns>
		public virtual WorkflowInstanceDefinition GetItemByInstanceId([NotNullValidator] Guid instanceId) {
			return ((WorkflowInstanceDefinitionBrokerDB)m_brokerDB).GetItemByInstanceId(instanceId);
		}


		/// <summary>
		/// Detaches the instance from workflow.
		/// </summary>
		/// <param name="instanceId">The instance id.</param>
		public void WorkflowDetachInstance([NotNullValidator] Guid instanceId) {
			((WorkflowInstanceDefinitionBrokerDB)m_brokerDB).WorkflowDetachInstance(instanceId);
		}

		/// <summary>
		/// Resolves the references for export.
		/// </summary>
		/// <param name="entity">The entity.</param>
		protected override void ResolveReferencesForExport(WorkflowInstanceDefinition entity) {
			// Do nothing.
		}

		/// <summary>
		/// Resolves the references during mapping.
		/// </summary>
		/// <param name="mappingRecord">The mapping record.</param>
		/// <param name="persistedEntity">The persisted entity.</param>
		protected override void ResolveReferencesForImport(MappingRecord<WorkflowInstanceDefinition> mappingRecord, WorkflowInstanceDefinition persistedEntity) {
			// Do nothing.
		}

		/// <summary>
		/// Gets the list workflows pending.
		/// </summary>
		/// <returns></returns>
		public List<Guid> GetListPendingWorkflows() {
			return ((WorkflowInstanceDefinitionBrokerDB)m_brokerDB).GetListPendingWorkflows();
		}

	}

}
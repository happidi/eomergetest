﻿using System;
using System.Collections.Generic;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using Vizelia.FOL.DataLayer.BrokerDB;

namespace Vizelia.FOL.BusinessLayer.Broker {

	/// <summary>
	/// The broker for list element entity. 
	/// </summary>
	public class ListElementBroker : MappingEnabledBroker<ListElement, ListElementBrokerDB> {
		/// <summary>
		/// Public ctor.
		/// The list name must be specified.
		/// </summary>
		/// <param name="listName">The list name.</param>
		public ListElementBroker(string listName)
			: this() {
			((ListElementBrokerDB)m_brokerDB).ListName = listName;
		}

		/// <summary>
		/// Public ctor for mapping procces.
		/// </summary>
		public ListElementBroker() {
			m_brokerDB = Helper.CreateInstance<ListElementBrokerDB>();
		}

		/// <summary>
		/// Gets all the element from a pset list.
		/// </summary>
		/// <returns></returns>
		public List<ListElement> GetPsetAll() {
			return ((ListElementBrokerDB)m_brokerDB).GetPsetAll();
		}

		/// <summary>
		/// Gets all the  element from a pset list and returns a JsonStore.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="location">The location of parameter.</param>
		/// <returns>
		/// A JsonStore.
		/// </returns>
		public JsonStore<ListElement> GetPsetStore(PagingParameter paging, PagingLocation location) {
			return GetPsetAll().ToJsonStoreWithFilter(paging);
		}

		/// <summary>
		/// Mapping_s the create.
		/// </summary>
		/// <param name="mappingRecord">The mapping record.</param>
		/// <param name="localId">The local id.</param>
		/// <param name="formResponse">The form response.</param>
		/// <returns></returns>
		protected override ListElement Mapping_Create(MappingRecord<ListElement> mappingRecord, string localId, out FormResponse formResponse) {
			var listName = ExtractListNameFromLocalId(localId);
			((ListElementBrokerDB)m_brokerDB).ListName = listName;
			return base.Mapping_Create(mappingRecord, localId, out formResponse);
		}

		/// <summary>
		/// Mapping_s the update.
		/// </summary>
		/// <param name="mappingRecord">The mapping record.</param>
		/// <param name="localId">The local id.</param>
		/// <param name="formResponse">The form response.</param>
		/// <returns></returns>
		protected override ListElement Mapping_Update(MappingRecord<ListElement> mappingRecord, string localId, out FormResponse formResponse) {
			var listName = ExtractListNameFromLocalId(localId);
			((ListElementBrokerDB)m_brokerDB).ListName = listName;
			return base.Mapping_Update(mappingRecord, localId, out formResponse);
		}

		/// <summary>
		/// Mapping_s the delete.
		/// </summary>
		/// <param name="mappingRecord">The mapping record.</param>
		/// <param name="localId">The local id.</param>
		/// <param name="formResponse">The form response.</param>
		protected override void Mapping_Delete(MappingRecord<ListElement> mappingRecord, string localId, out FormResponse formResponse) {
			var listName = ExtractListNameFromLocalId(localId);
			((ListElementBrokerDB)m_brokerDB).ListName = listName;
			base.Mapping_Delete(mappingRecord, localId, out formResponse);
		}

		private static string ExtractListNameFromLocalId(string localId) {
			var parameters = localId.Split(new[] { "ListName=", "MsgCode=" }, StringSplitOptions.RemoveEmptyEntries);
			var listName = parameters[0];
			return listName;
		}

		/// <summary>
		/// Resolves the references for import.
		/// </summary>
		/// <param name="mappingRecord">The mapping record.</param>
		/// <param name="persistedEntity">The persisted entity.</param>
		protected override void ResolveReferencesForImport(MappingRecord<ListElement> mappingRecord, ListElement persistedEntity) {
			// Do nothing.
		}

		/// <summary>
		/// Resolves the references for export.
		/// </summary>
		/// <param name="entity">The entity.</param>
		protected override void ResolveReferencesForExport(ListElement entity) {
			// Do nothing.
		}
	}
}

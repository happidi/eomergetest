﻿using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using Vizelia.FOL.DataLayer.BrokerDB;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for business entity DynamicDisplayImage.
	/// </summary>
	public class DynamicDisplayImageBroker : MappingEnabledBroker<DynamicDisplayImage, DynamicDisplayImageBrokerDB> {
		/// <summary>
		/// Resolves the references for export.
		/// </summary>
		/// <param name="entity">The entity.</param>
		protected override void ResolveReferencesForExport(DynamicDisplayImage entity) {
		}

		/// <summary>
		/// Resolves the references during mapping.
		/// </summary>
		/// <param name="mappingRecord">The mapping record.</param>
		/// <param name="persistedEntity">The persisted entity.</param>
		protected override void ResolveReferencesForImport(MappingRecord<DynamicDisplayImage> mappingRecord, DynamicDisplayImage persistedEntity) {
		}

		/// <summary>
		/// Populate the chidren items specified as fields. "*" indicates that all fields should be populated.
		/// </summary>
		/// <param name="item">The item</param>
		/// <param name="loadMode">The load mode.</param>
		/// <param name="fields">The list of fields. "*" indicates all fields.</param>
		public override void PopulateList(DynamicDisplayImage item, EntityLoadMode loadMode, params string[] fields) {
			foreach (string field in fields) {
				if (field.ContainsAny("*", "Image")) {
					if (item != null && !string.IsNullOrEmpty(item.KeyImage)) {
						DocumentBroker brokerDocument = Helper.CreateInstance<DocumentBroker>();
						item.Image = brokerDocument.GetItem(item.KeyImage.Trim());
					}
				}
			}
		}

		/// <summary>
		/// Updates the documents.
		/// </summary>
		/// <param name="item">The item.</param>
		protected override void UpdateDocuments(DynamicDisplayImage item) {
			if (item.KeyImage.StartsWith("document_")) {
				CleanDocuments(item);
				SaveDocuments(item);
			}
			else {
				item.KeyImage = m_brokerDB.GetItem(item.KeyDynamicDisplayImage).KeyImage;
			}
		}

		/// <summary>
		/// Gets a store of DynamicDisplayImage Image for a specific type.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="type">The type.</param>
		/// <returns>
		/// A JsonStore.
		/// </returns>
		public JsonStore<DynamicDisplayImage> GetStoreByType(PagingParameter paging, DynamicDisplayImageType  type) {
			int total;
			return ((DynamicDisplayImageBrokerDB)m_brokerDB).GetAllPagingByType(paging, type, out total).ToJsonStore(total, paging);
		}

	}
}

﻿using System;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common.Configuration;
using Vizelia.FOL.Common.Localization;
using Vizelia.FOL.DataLayer.BrokerDB;
using Vizelia.FOL.Common;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for business entity Algorithm.
	/// </summary>
	public class AlgorithmBroker : BaseBroker<Algorithm, AlgorithmBrokerDB> {

		/// <summary>
		/// Creates the specified item.
		/// This override ensures the item is validated.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <param name="formResponse">The form response.</param>
		/// <returns></returns>
		protected override Algorithm Create(Algorithm item, out FormResponse formResponse) {
			Algorithm retVal;

			if (ValidateAlgorithm(item, out formResponse)) {
				retVal = base.Create(item, out formResponse);
			}
			else {
				retVal = null;
			}

			return retVal;
		}

		/// <summary>
		/// Updates the specified item.
		/// This override ensures the item is validated.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <param name="formResponse">The form response.</param>
		/// <returns></returns>
		protected override Algorithm Update(Algorithm item, out FormResponse formResponse) {
			Algorithm retVal;

			if (ValidateAlgorithm(item, out formResponse)) {
				retVal = base.Update(item, out formResponse);
			}
			else {
				retVal = null;
			}

			return retVal;
		}

		/// <summary>
		/// Validates the specified algorithm.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <param name="formResponse"> </param>
		private static bool ValidateAlgorithm(Algorithm item, out FormResponse formResponse) {
			bool isValid;
			string errorMessage;
			formResponse = new FormResponse {success = true};

			switch (item.Source) {
				case AlgorithmSource.BuiltIn:
					formResponse = new FormResponse {success = false, msg = "Cannot create a built-in algorithm."};
					isValid = false;
					break;

				case AlgorithmSource.Editable:
					if(!VizeliaConfiguration.Instance.AlgorithmsConfiguration.AllowEditableSource) {
						isValid = false;
						errorMessage = "Editable algorithms are not allowed. Edit configuration to allow.";
					}
					else {
						item.RenewUniqueId();
						isValid = AlgorithmHelper.ValidateAlgorithm<IAnalyticsExtensibility>(item, out errorMessage);
					}

					if (!isValid) {
						formResponse = new FormResponse { success = false, msg = errorMessage };
					}
					break;

				case AlgorithmSource.ExternalLibrary:
					// Validate we have an attachment.
					if(string.IsNullOrEmpty(item.KeyBinary)) {
						formResponse = new FormResponse { success = false, msg = Langue.error_algorithm_missing_binary }; 
						isValid = false;
					}
					else if (item.KeyBinary.StartsWith("document_")) {
						// Validation of the attachment only needed if a binary has been introduced in this call, and not previously persisted.
						item.RenewUniqueId();

						isValid = AlgorithmHelper.ValidateAlgorithm<IAnalyticsExtensibility>(item, out errorMessage);

						if (!isValid) {
							formResponse = new FormResponse { success = false, msg = errorMessage };
						}
					}
					else {
						// This is a previously validated binary.
						item.RenewUniqueId(); // force its reloading in the EA. it is possible that the Source property has changed from Code to DLL.
						isValid = true;
					}
					break;

				default:
					throw new ArgumentOutOfRangeException("Source");
			}

			return isValid;
		}

		/// <summary>
		/// Updates the documents.
		/// </summary>
		/// <param name="item">The item.</param>
		protected override void UpdateDocuments(Algorithm item) {
			if (item.KeyBinary != null && item.KeyBinary.StartsWith("document_")) {
				CleanDocuments(item);
				SaveDocuments(item);
			}
			else {
				Algorithm persistedAlgorithm = m_brokerDB.GetItem(item.KeyAlgorithm);
				item.KeyBinary = persistedAlgorithm.KeyBinary;
			}
		}

		/// <summary>
		/// Populates the list.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <param name="loadMode">The load mode.</param>
		/// <param name="fields">The fields.</param>
		public override void PopulateList(Algorithm item, EntityLoadMode loadMode, params string[] fields) {
			base.PopulateList(item, loadMode, fields);

			foreach (string field in fields) {
				if (field.ContainsAny("*", "Binary") && item.Source == AlgorithmSource.ExternalLibrary) {

					try {
						bool isCached = AlgorithmHelper.CacheAlgorithmBinaries(item);

						if (!isCached) {
							if (!string.IsNullOrWhiteSpace(item.KeyBinary)) {
								var broker = Helper.CreateInstance<DocumentBroker>();
								item.Binary = broker.GetItem(item.KeyBinary);
							}

							// Try again, this time with the binary file to cache.
							AlgorithmHelper.CacheAlgorithmBinaries(item);
						}
					}
					catch (Exception e) {
						TracingService.Write(e);
					}
				}
			}
		}
	}

}
﻿using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.DataLayer.BrokerDB;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for business entity DynamicDisplay.
	/// </summary>
	public class DynamicDisplayBroker : MappingEnabledBroker<DynamicDisplay, DynamicDisplayBrokerDB> {
		/// <summary>
		/// Populate the chidren items specified as fields. "*" indicates that all fields should be populated.
		/// </summary>
		/// <param name="item">The item</param>
		/// <param name="loadMode">The load mode.</param>
		/// <param name="fields">The list of fields. "*" indicates all fields.</param>
		public override void PopulateList(DynamicDisplay item, EntityLoadMode loadMode, params string[] fields) {
			foreach (string field in fields) {
				if (field.ContainsAny("*", "CustomColorTemplate")) {
					if (string.IsNullOrEmpty(item.KeyCustomColorTemplate) == false) {
						var brokerColor = Helper.CreateInstance<DynamicDisplayColorTemplateBroker>();
						DynamicDisplayColorTemplate template = brokerColor.GetItem(item.KeyCustomColorTemplate);
						if (template != null) {
							brokerColor.ResolveForExport(template, loadMode);
							item.CustomColorTemplate = template;
						}
					}
				}

				var brokerImage = Helper.CreateInstance<DynamicDisplayImageBroker>();

				if (field.ContainsAny("*", "HeaderPictureImage")) {
					if (string.IsNullOrEmpty(item.HeaderPicture) == false) {
						DynamicDisplayImage image = brokerImage.GetItem(item.HeaderPicture);
						if (image != null) {
							brokerImage.PopulateList(image, loadMode, fields);
							brokerImage.ResolveForExport(image, loadMode);
							item.HeaderPictureImage = image;
						}
					}
				}

				if (field.ContainsAny("*", "MainPictureImage")) {
					if (string.IsNullOrEmpty(item.MainPicture) == false) {
						DynamicDisplayImage image = brokerImage.GetItem(item.MainPicture);
						if (image != null) {
							brokerImage.PopulateList(image, loadMode, fields);
							brokerImage.ResolveForExport(image, loadMode);
							item.MainPictureImage = image;
						}
					}
				}

				if (field.ContainsAny("*", "FooterPictureImage")) {
					if (string.IsNullOrEmpty(item.FooterPicture) == false) {
						DynamicDisplayImage image = brokerImage.GetItem(item.FooterPicture);
						if (image != null) {
							brokerImage.PopulateList(image, loadMode, fields);
							brokerImage.ResolveForExport(image, loadMode);
							item.FooterPictureImage = image;
						}
					}
				}
			}
		}


		/// <summary>
		/// Resolves the references for export.
		/// </summary>
		/// <param name="entity">The entity.</param>
		protected override void ResolveReferencesForExport(DynamicDisplay entity) {
			ResolveParentReferenceForExport<DynamicDisplayColorTemplate>(e => e.KeyCustomColorTemplate, entity);
			ResolveParentReferenceForExport<FOLMembershipUser>(e => e.Creator, entity);
			ResolveParentReferenceForExport<DynamicDisplayImage>(e => e.HeaderPicture, entity);
			ResolveParentReferenceForExport<DynamicDisplayImage>(e => e.MainPicture, entity);
			ResolveParentReferenceForExport<DynamicDisplayImage>(e => e.FooterPicture, entity);
		}

		/// <summary>
		/// Resolves the references during mapping.
		/// </summary>
		/// <param name="mappingRecord">The mapping record.</param>
		/// <param name="persistedEntity">The persisted entity.</param>
		protected override void ResolveReferencesForImport(MappingRecord<DynamicDisplay> mappingRecord, DynamicDisplay persistedEntity) {
			ResolveParentReferenceForImport<DynamicDisplayColorTemplate>(e => e.KeyCustomColorTemplate, mappingRecord.Entity, persistedEntity);
			ResolveParentReferenceForImport<FOLMembershipUser>(e => e.Creator, mappingRecord.Entity, persistedEntity);
			ResolveParentReferenceForImport<DynamicDisplayImage>(e => e.HeaderPicture, mappingRecord.Entity, persistedEntity);
			ResolveParentReferenceForImport<DynamicDisplayImage>(e => e.MainPicture, mappingRecord.Entity, persistedEntity);
			ResolveParentReferenceForImport<DynamicDisplayImage>(e => e.FooterPicture, mappingRecord.Entity, persistedEntity);
		}
	}
}

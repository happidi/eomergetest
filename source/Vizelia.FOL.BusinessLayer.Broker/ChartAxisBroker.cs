﻿using System.Collections.Generic;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.DataLayer.BrokerDB;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for business entity ChartAxis.
	/// </summary>
	public class ChartAxisBroker : MappingEnabledBroker<ChartAxis, ChartAxisBrokerDB> {

		/// <summary>
		/// Get the list of saved ChartAxis from a Chart
		/// </summary>
		/// <param name="KeyChart">The key of the chart.</param>
		public List<ChartAxis> GetListByKeyChart(string KeyChart) {
			return ((ChartAxisBrokerDB)m_brokerDB).GetListByKeyChart(KeyChart);
		}

		/// <summary>
		/// Gets a store of ChartAxis for a specific chart.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyChart">The Key of the chart.</param>
		/// <returns>A JsonStore.</returns>
		public JsonStore<ChartAxis> GetStoreByKeyChart(PagingParameter paging, string KeyChart) {
			int total;
			return ((ChartAxisBrokerDB)m_brokerDB).GetAllPagingByKeyChart(paging, KeyChart, out total).ToJsonStore(total, paging);
		}


		/// <summary>
		/// Saves the chart ChartAxis.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="chartaxis">The ChartAxis store.</param>
		/// <returns></returns>
		public List<ChartAxis> SaveChartChartAxis(Chart entity, CrudStore<ChartAxis> chartaxis) {
			return ((ChartAxisBrokerDB)m_brokerDB).SaveChartChartAxis(entity, chartaxis);
		}


		/// <summary>
		/// Populate the chidren items specified as fields. "*" indicates that all fields should be populated.
		/// </summary>
		/// <param name="item">The item</param>
		/// <param name="loadMode">The load mode.</param>
		/// <param name="fields">The list of fields. "*" indicates all fields.</param>
		public override void PopulateList(ChartAxis item, EntityLoadMode loadMode, params string[] fields) {
			foreach (string field in fields) {
				if (field.ContainsAny("*", "Markers")) {
					var brokerMarker = Helper.CreateInstance<ChartMarkerBroker>();
					var markers = brokerMarker.GetListByKeyChartAxis(item.KeyChartAxis);
					if (markers != null && markers.Count > 0) {
						brokerMarker.ResolveForExport(markers, loadMode);
						item.Markers = markers;
					}
				}
			}
		}

		/// <summary>
		/// Resolves the references for export.
		/// </summary>
		/// <param name="entity">The entity.</param>
		protected override void ResolveReferencesForExport(ChartAxis entity) {
			ResolveParentReferenceForExport<Chart>(e => e.KeyChart, entity);
		}

		/// <summary>
		/// Resolves the references during mapping.
		/// </summary>
		/// <param name="mappingRecord">The mapping record.</param>
		/// <param name="persistedEntity">The persisted entity.</param>
		protected override void ResolveReferencesForImport(MappingRecord<ChartAxis> mappingRecord, ChartAxis persistedEntity) {
			ResolveParentReferenceForImport<Chart>(e => e.KeyChart, mappingRecord.Entity, persistedEntity);
		}


		/// <summary>
		/// Copies the specified item.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <param name="formResponse">The form response.</param>
		/// <param name="Creator">The creator.</param>
		/// <param name="incrementTitle">if set to <c>true</c> [increment title].</param>
		/// <returns></returns>
		public override ChartAxis Copy(ChartAxis item, out FormResponse formResponse, string Creator = null, bool incrementTitle = true) {
			var brokerMarker = Helper.CreateInstance<ChartMarkerBroker>();
			var markers = brokerMarker.GetListByKeyChartAxis(item.KeyChartAxis);

			var newItem = base.Copy(item, out formResponse, Creator);

			if (markers != null && markers.Count > 0) {
				markers.ForEach(marker => {
					marker.LocalId = null;
					marker.KeyChartMarker = null;
					marker.KeyChartAxis = newItem.KeyChartAxis;
					brokerMarker.Save(marker);
				});
			}
			return newItem;
		}
	}

}
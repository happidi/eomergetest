﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.DataLayer.BrokerDB;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Localization;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for business entity Job.
	/// </summary>
	public class JobBroker : BaseBroker<Job, JobBrokerDB> {
		/// <summary>
		/// Executes the item.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <returns></returns>
		public Job ExecuteItem(string key) {
			var job = SchedulerService.GetJob(key);
			SchedulerService.ExecuteJob(job);
			return job;
		}


		/// <summary>
		/// Pauses the selected job
		/// </summary>
		/// <param name="key">The key.</param>
		/// <returns></returns>
		public Job PauseItem(string key) {
			var job = SchedulerService.GetJob(key);
			SchedulerService.PauseItem(job);
			return job;
		}


		/// <summary>
		/// Resumes the selected job
		/// </summary>
		/// <param name="key">The key.</param>
		/// <returns></returns>
		public Job ResumeItem(string key) {
			var job = SchedulerService.GetJob(key);
			SchedulerService.ResumeItem(job);
			return job;

		}
	}
}

﻿using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.DataLayer.BrokerDB;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for business entity DocumentMetadata.
	/// </summary>
	public class DocumentMetadataBroker : BaseBroker<DocumentMetadata, DocumentMetadataBrokerDB> {
		// Nothing to see here, move along.
	}

}
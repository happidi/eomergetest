﻿using System;
using System.Collections.Generic;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Localization;
using Vizelia.FOL.DataLayer.BrokerDB;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for AuditEntity entity.
	/// </summary>
	public class AuditEntityBroker : BaseBroker<AuditEntity, AuditEntityBrokerDB> {

		/// <summary>
		/// Initializes a new instance of the <see cref="AuditEntityBroker"/> class.
		/// </summary>
		public AuditEntityBroker() {
			m_database_errors = new DatabaseErrors();
			m_database_errors.Add(100, Langue.error_msg_uniquefield, "EntityName");
		}

		/// <summary>
		/// cache key for AuditEntityList.
		/// </summary>
		protected const string const_cache_auditlist = "AuditEntityList";

		/// <summary>
		/// Gets the store aggregated.
		/// </summary>
		/// <returns></returns>
		public JsonStore<AuditEntity> GetAllAggregated() {
			var retVal = GetListAggregated().ToJsonStore();
			return retVal;
		}

		/// <summary>
		/// Gets the list aggregated.
		/// </summary>
		/// <returns></returns>
		public List<AuditEntity> GetListAggregated() {
			return CacheService.Get(AuditEntityListCacheKey, InternalGetAllAggregated, 20);
		}

		private static string AuditEntityListCacheKey {
			get { return ContextHelper.ApplicationName + const_cache_auditlist; }
		}

		private static List<AuditEntity> InternalGetAllAggregated() {
			var brokerDB = Helper.CreateInstance<AuditEntityBrokerDB>();
			var auditList = brokerDB.GetAuditEntitiesAggregated();
			return auditList;
		}

		/// <summary>
		/// Creates the specified item.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <returns></returns>
		public override AuditEntity Create(AuditEntity item) {
			var retVal = base.Create(item);
			CacheService.Add(AuditEntityListCacheKey, InternalGetAllAggregated());
			return retVal;
		}

		/// <summary>
		/// Updates the specified item.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <param name="changedFields">The changed fields.</param>
		/// <returns></returns>
		public override AuditEntity Update(AuditEntity item, ref List<Tuple<string, string, string>> changedFields) {
			var retVal = base.Update(item, ref changedFields);
			CacheService.Add(AuditEntityListCacheKey, InternalGetAllAggregated());
			return retVal;
		}

		/// <summary>
		/// Deletes the specified item.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <param name="persistedItem">The persisted item.</param>
		/// <returns></returns>
		public override bool Delete(AuditEntity item, AuditEntity persistedItem = null) {
			var retVal = base.Delete(item, persistedItem);
			CacheService.Add(AuditEntityListCacheKey, InternalGetAllAggregated());
			return retVal;
		}
	}
}

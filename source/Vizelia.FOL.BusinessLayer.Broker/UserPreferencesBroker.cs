﻿using System;
using System.Linq;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.DataLayer.BrokerDB;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker For User Preferences
	/// </summary>
	public class UserPreferencesBroker : MappingEnabledBroker<UserPreferences, UserPreferencesBrokerDB> {
		/// <summary>
		/// Resolves references for import
		/// </summary>
		/// <param name="mappingRecord"></param>
		/// <param name="persistedEntity"></param>
		protected override void ResolveReferencesForImport(MappingRecord<UserPreferences> mappingRecord, UserPreferences persistedEntity) {
			// no implementation
		}

		/// <summary>
		/// Resolves references for export
		/// </summary>
		/// <param name="entity"></param>
		protected override void ResolveReferencesForExport(UserPreferences entity) {
			// no implementation 
		}

	    public override UserPreferences Create(UserPreferences item) {
            ValidateTimeZonId(item);
	        return base.Create(item);
	    }

	    protected override UserPreferences Update(UserPreferences item, out FormResponse formResponse) {
	        ValidateTimeZonId(item);
            return base.Update(item, out formResponse);
	    }

        private void ValidateTimeZonId(UserPreferences userPreferences)
	    {
            // Makre sure the timezone exists
            if (userPreferences != null &&
                userPreferences.TimeZoneId != null &&
                !TimeZoneInfo.GetSystemTimeZones().Select(x => x.Id).Contains(userPreferences.TimeZoneId)) {
                throw new ArgumentException(string.Format("Invalid timezone: {0}", userPreferences.TimeZoneId));
            }
	    }
	}
}
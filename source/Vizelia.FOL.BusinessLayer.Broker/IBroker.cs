using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// An interface representing a broker for an entity.
	/// </summary>
	/// <typeparam name="TEntity">The type of the entity.</typeparam>
	public interface IBroker<TEntity> where TEntity : BaseBusinessEntity {
		/// <summary>
		/// Gets a item.
		/// </summary>
		/// <param name="Key">The Key of the item.</param>
		/// <returns>The item.</returns>
		TEntity GetItem(string Key);

		/// <summary>
		/// Gets a item.
		/// </summary>
		/// <param name="Key">The Key of the item.</param>
		/// <param name="fields">the list of field to fetch. "*" to fetch all children fields.</param>
		/// <returns>The item.</returns>
		TEntity GetItem(string Key, params string[] fields);

		/// <summary>
		/// Gets a item by its localId.
		/// </summary>
		/// <param name="localId">The LocalId of the item.</param>
		/// <returns></returns>
		TEntity GetItemByLocalId(string localId);

		/// <summary>
		/// Gets the item by local id.
		/// </summary>
		/// <param name="localId">The local id.</param>
		/// <param name="throwWhenItemNotFound">if set to <c>true</c> [throw when item not found].</param>
		/// <returns></returns>
		TEntity GetItemByLocalId(string localId, bool throwWhenItemNotFound);

		/// <summary>
		/// Gets all the items and returns a JsonStore.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="location">The location of parameter.</param>
		/// <param name="useSecurableFilter">if set to <c>true</c> [use securable filter].</param>
		/// <param name="fields">The fields.</param>
		/// <returns>
		/// A JsonStore.
		/// </returns>
		JsonStore<TEntity> GetStore(PagingParameter paging, PagingLocation location, bool useSecurableFilter = true, params string[] fields );
	}
}

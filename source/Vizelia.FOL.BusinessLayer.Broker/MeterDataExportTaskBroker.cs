﻿using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.DataLayer.BrokerDB;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for business entity MeterDataExportTask.
	/// </summary>
	public class MeterDataExportTaskBroker : BaseBroker<MeterDataExportTask, MeterDataExportTaskBrokerDB> {
		
	}
}
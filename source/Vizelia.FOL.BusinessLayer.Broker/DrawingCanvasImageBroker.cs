﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.DataLayer.BrokerDB;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Localization;
using Vizelia.FOL.BusinessEntities.Mapping;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for business entity DrawingCanvasImage.
	/// </summary>
	public class DrawingCanvasImageBroker : MappingEnabledBroker<DrawingCanvasImage, DrawingCanvasImageBrokerDB> {

		/// <summary>
		/// Get the list of saved DrawingCanvas Image from a KeyDrawingCanvas.
		/// </summary>
		/// <param name="KeyDrawingCanvas">The key of the  DrawingCanvas.</param>
		public List<DrawingCanvasImage> GetListByKeyDrawingCanvas(string KeyDrawingCanvas) {
			return ((DrawingCanvasImageBrokerDB)m_brokerDB).GetListByKeyDrawingCanvas(KeyDrawingCanvas);
		}

		/// <summary>
		/// Gets a store of DrawingCanvas Image for a specific DrawingCanvas.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyDrawingCanvas">The Key of the DrawingCanvas.</param>
		/// <returns>A JsonStore.</returns>
		public JsonStore<DrawingCanvasImage> GetStoreByKeyDrawingCanvas(PagingParameter paging, string KeyDrawingCanvas) {
			int total;
			return ((DrawingCanvasImageBrokerDB)m_brokerDB).GetAllPagingByKeyDrawingCanvas(paging, KeyDrawingCanvas, out total).ToJsonStore(total, paging);
		}


		/// <summary>
		/// Saves the DrawingCanvas Images.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="images">The DrawingCanvasImage store.</param>
		/// <returns></returns>
		public List<DrawingCanvasImage> SaveDrawingCanvasDrawingCanvasImage(DrawingCanvas entity, CrudStore<DrawingCanvasImage> images) {
			return ((DrawingCanvasImageBrokerDB)m_brokerDB).SaveDrawingCanvasDrawingCanvasImage(entity, images);
		}

		/// <summary>
		/// Resolves the references during mapping.
		/// </summary>
		/// <param name="mappingRecord">The mapping record.</param>
		/// <param name="persistedEntity">The persisted entity.</param>
		protected override void ResolveReferencesForImport(MappingRecord<DrawingCanvasImage> mappingRecord, DrawingCanvasImage persistedEntity) {
			ResolveParentReferenceForImport<DrawingCanvas>(e => e.KeyDrawingCanvas, mappingRecord.Entity, persistedEntity);
			ResolveParentReferenceForImport<DynamicDisplayImage>(e => e.KeyImage, mappingRecord.Entity, persistedEntity);
		}

		/// <summary>
		/// Resolves the references for export.
		/// </summary>
		/// <param name="entity">The entity.</param>
		protected override void ResolveReferencesForExport(DrawingCanvasImage entity) {
			ResolveParentReferenceForExport<DrawingCanvas>(e => e.KeyDrawingCanvas, entity);
			ResolveParentReferenceForExport<DynamicDisplayImage>(e => e.KeyImage, entity);
		}
	}

}
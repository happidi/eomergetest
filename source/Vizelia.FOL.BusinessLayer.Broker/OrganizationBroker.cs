﻿using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.DataLayer.BrokerDB;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for business entity Organization.
	/// </summary>
	public class OrganizationBroker : MappingEnabledBroker<Organization, OrganizationBrokerDB> {
		/// <summary>
		/// Resolves the references for export.
		/// </summary>
		/// <param name="entity">The entity.</param>
		protected override void ResolveReferencesForExport(Organization entity) {
			ResolveParentReferenceForExport<Organization>(e => e.KeyParent, entity);
		}

		/// <summary>
		/// Resolves the references during mapping.
		/// </summary>
		/// <param name="mappingRecord">The mapping record.</param>
		/// <param name="persistedEntity">The persisted entity.</param>
		protected override void ResolveReferencesForImport(MappingRecord<Organization> mappingRecord, Organization persistedEntity) {
			ResolveParentReferenceForImport<Organization>(e => e.KeyParent, mappingRecord.Entity, persistedEntity);
		}

	}

}
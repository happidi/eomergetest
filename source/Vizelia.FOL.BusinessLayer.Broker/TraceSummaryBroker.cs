﻿using System.Collections.Generic;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.DataLayer.BrokerDB;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for business entity TraceSummary.
	/// </summary>
	public class TraceSummaryBroker : BaseBroker<TraceSummary, TraceSummaryBrokerDB> {
		/// <summary>
		/// Gets all the TraceSummaries
		/// </summary>
		/// <returns></returns>
		public List<TraceSummary> GetAll() {
			var retVal = ((TraceSummaryBrokerDB)m_brokerDB).GetAll();
			return retVal;
		}
	}

}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.BusinessLayer.Interfaces;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Localization;
using Vizelia.FOL.DataLayer.BrokerDB;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for business entity PsetDefinition.
	/// </summary>
	public class PsetDefinitionBroker : MappingEnabledBroker<PsetDefinition, PsetDefinitionBrokerDB> {

		/// <summary>
		/// Public ctor.
		/// </summary>
		public PsetDefinitionBroker()
			: base() {
			m_database_errors.Add(101, Langue.error_msg_uniquefield, "PsetName");
		}



		/// <summary>
		/// Gets a list of pset definition by usage name.
		/// </summary>
		/// <param name="UsageName">The usage name.</param>
		/// <returns></returns>
		public List<PsetDefinition> GetListByUsageName(string UsageName) {
			int total;

			if (UsageName == Helper.GetAttributeValue<IfcTypeNameAttribute>(typeof(ActionRequest)) ||
				UsageName == Helper.GetAttributeValue<IfcTypeNameAttribute>(typeof(Task)) ||
				//UsageName == Helper.GetAttributeValue<IfcTypeNameAttribute>(typeof(Furniture)) ||
				//UsageName == Helper.GetAttributeValue<IfcTypeNameAttribute>(typeof(Meter)) ||
				UsageName == Helper.GetAttributeValue<IfcTypeNameAttribute>(typeof(Occupant))) {
				return new List<PsetDefinition>();
			}

			var filters = new List<GridFilter>{ 
				new GridFilter {
					field = "UsageName",
					data = new GridData { comparison = "eq" , type = "string" , value = UsageName}
				}
			};
			List<PsetDefinition> results = this.GetAll(new PagingParameter { start = 0, limit = 0, filters = filters }, PagingLocation.Database, out total);
			return results.PopulateChildren();
		}

		/// <summary>
		/// Gets the pset definition list corresponding to a specific usage name.
		/// </summary>
		/// <param name="UsageName">Name of the usage.</param>
		/// <param name="KeyClassificationItem">The KeyClassificationItem (optional).</param>
		/// <returns></returns>
		public List<PsetDefinition> GetListByUsageName(string UsageName, string KeyClassificationItem) {
			List<PsetDefinition> results;
			if (String.IsNullOrEmpty(KeyClassificationItem))
				results = this.GetListByUsageName(UsageName);
			else
				results = this.GetListHerited(UsageName, KeyClassificationItem);
			return results;
		}

		/// <summary>
		/// Gets the list of pset herited from a ClassificationItem.
		/// </summary>
		/// <param name="UsageName">Name of the usage.</param>
		/// <param name="KeyClassificationItem">The key classification item.</param>
		/// <returns></returns>
		public List<PsetDefinition> GetListHerited(string UsageName, string KeyClassificationItem) {
			return ((PsetDefinitionBrokerDB)m_brokerDB).GetListHerited(UsageName, KeyClassificationItem);
		}

		/// <summary>
		/// Resolves the references.
		/// </summary>
		/// <param name="mappingRecord">The mapping record.</param>
		/// <param name="persistedEntity">The persisted entity.</param>
		protected override void ResolveReferencesForImport(MappingRecord<PsetDefinition> mappingRecord, PsetDefinition persistedEntity) {
			// Do nothing.
		}

		/// <summary>
		/// Resolves the references for export.
		/// </summary>
		/// <param name="entity">The entity.</param>
		protected override void ResolveReferencesForExport(PsetDefinition entity) {
			// Do nothing.
		}

		/// <summary>
		/// Populates the list.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <param name="loadMode">The load mode.</param>
		/// <param name="fields">The fields.</param>
		public override void PopulateList(PsetDefinition item, EntityLoadMode loadMode, params string[] fields) {
			foreach (var field in fields) {
				if (field.ContainsAny("Attributes")) {
					var broker = Helper.CreateInstance<PsetAttributeDefinitionBrokerDB>();
					var attributes = broker.GetAttributesFromPsetDefinitionList(new []{item.KeyPropertySet});
					if (attributes.Any()) {
						item.Attributes = attributes;
					}
				}
			}
		}
	}
}
﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Vizelia.FOL.DataLayer.BrokerDB;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for business entity Document.
	/// </summary>
	public class DocumentBroker : BaseBroker<Document, DocumentBrokerDB> {

		/// <summary>
		/// Gets the document from the session and saves it to the document store
		/// </summary>
		/// <param name="documentId">The document id.</param>
		/// <returns></returns>
		private string SaveDocument(string documentId) {
			var document = DocumentHelper.RetrieveDocument(documentId);

			var savedDocument = m_brokerDB.Update(document);
			var keyDocument = savedDocument.KeyDocument;
			return keyDocument;
		}



		/// <summary>
		/// Deletes the documents associated with the entity
		/// </summary>
		/// <param name="itemWithDocuments">The item with documents.</param>
		public void DeleteDocuments(ISupportDocuments itemWithDocuments) {
			var documentProperties = DocumentHelper.GetDocumentProperties(itemWithDocuments);
			var documentIds = DocumentHelper.GetDocumentIds(itemWithDocuments, documentProperties);

			foreach (var documentId in documentIds) {
				m_brokerDB.Delete(documentId);
			}
		}


		/// <summary>
		/// Cleans the documents that appear in the old item but not in the new one.
		/// </summary>
		/// <param name="oldItem">The old item.</param>
		/// <param name="item">The item.</param>
		public void CleanDocuments(ISupportDocuments oldItem, ISupportDocuments item) {
			var documentIdsToDelete = DocumentHelper.GetDocumentIdsToDelete(oldItem, item);
			foreach (var documentIdToDelete in documentIdsToDelete) {
				m_brokerDB.Delete(documentIdToDelete);
			}
		}


		/// <summary>
		/// Cleans the pset documents that are in the item but are not in the old item.
		/// </summary>
		/// <param name="pset">The pset.</param>
		/// <param name="psetDefinition">The pset definition.</param>
		/// <param name="oldItem">The old item.</param>
		public void CleanPsetDocuments(Pset pset, Pset oldItem, PsetDefinition psetDefinition)
		{
			var documentAttributes = GetDocumentAttributes(oldItem, psetDefinition);

			foreach (var oldDocumentAttribute in documentAttributes) {
				var attribute = oldDocumentAttribute;
				IList<PsetAttribute> attributes  = new List<PsetAttribute>();
				foreach (var attrib in pset.Attributes)
				{
					if (attrib.Key == attribute.Key && attrib.Value != attribute.Value && !string.IsNullOrEmpty(attribute.Value))
					{
						attributes.Add(attrib);
					}
				}

				if (attributes.Count() > 0) {
					m_brokerDB.Delete(oldDocumentAttribute.Value);
				}
			}
		}

		/// <summary>
		/// Gets the document attributes.
		/// </summary>
		/// <param name="oldItem">The old item.</param>
		/// <param name="psetDefinition">The pset definition.</param>
		/// <returns></returns>
		private static List<PsetAttribute> GetDocumentAttributes(Pset oldItem, PsetDefinition psetDefinition)
		{
			var documentAttributes = new List<PsetAttribute>();
			foreach (PsetAttribute psetAttribute in oldItem.Attributes)
			{
				if (psetDefinition.Attributes.Exists(attributeDefinition =>
				                                     attributeDefinition.AttributeDataType == DocumentHelper.PsetDocumentDataType &&
				                                     attributeDefinition.AttributeName == psetAttribute.Key))
				{
					documentAttributes.Add(psetAttribute);
				}
			}
			return documentAttributes;
		}

		/// <summary>
		/// Saves the document psets.
		/// </summary>
		/// <param name="item">The item.</param>
		public void SaveDocumentPsets(Pset item) {
			foreach (var psetAttribute in item.Attributes) {
				if (psetAttribute.Value != null && DocumentHelper.IsValidDocumentIdentifier(psetAttribute.Value)) {
					var keyDocument = SaveDocument(psetAttribute.Value);
					psetAttribute.Value = keyDocument;
				}
			}
		}


		/// <summary>
		/// Saves the documents associated with the entity.
		/// </summary>
		/// <param name="item">The item.</param>
		public void SaveDocuments(ISupportDocuments item) {
			var documentProperties = DocumentHelper.GetDocumentProperties(item);

			foreach (PropertyInfo documentProperty in documentProperties) {
				var documentPropertyValue = documentProperty.GetValue(item, null);
				if (documentPropertyValue is string) {
					var documentGuid = documentPropertyValue as string;
					var keyDocument = SaveDocument(documentGuid);
					documentProperty.SetValue(item, keyDocument, null);
				}
			}
		}

	}
}
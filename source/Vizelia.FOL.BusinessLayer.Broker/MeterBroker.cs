﻿using System.Collections.Generic;
using System.Linq;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using Vizelia.FOL.DataLayer.BrokerDB;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for business entity Meter.
	/// </summary>
	public class MeterBroker : MappingEnabledBroker<Meter, MeterBrokerDB> {

		/// <summary>
		/// Initializes a new instance of the <see cref="MeterBroker"/> class.
		/// </summary>
		public MeterBroker() {
			m_database_errors.Add(50000, "error_msg_invalid_dataseriepsetratio_pset", "PsetFactor");
		}

		#region Location and Classification
		/// <summary>
		/// Gets a list of Meters for a specific location.
		/// </summary>
		/// <param name="KeyLocation">The Key location.</param>
		/// <returns></returns>
		public virtual List<Meter> GetListByKeyLocation(string KeyLocation) {
			return ((MeterBrokerDB)m_brokerDB).GetListByKeyLocation(KeyLocation);
		}


		/// <summary>
		/// Gets the list of Meters by locations and classificatons.
		/// </summary>
		/// <param name="Locations">The locations.</param>
		/// <param name="ClassificationItems">The classification items.</param>
		/// <returns></returns>
		public List<Meter> GetListByLocationsAndClassificatons(string[] Locations, string[] ClassificationItems) {
			var list = ((MeterBrokerDB)m_brokerDB).GetListByLocationsAndClassificatons(Locations, ClassificationItems);
			return list;
		}

		#endregion

		#region Chart
		/// <summary>
		/// Gets a list of Meters for a specific chart.
		/// </summary>
		/// <param name="KeyChart">The Key of the Chart.</param>
		/// <returns></returns>
		public virtual List<Meter> GetListByKeyChart(string KeyChart) {
			int total;
			PagingParameter paging = new PagingParameter() { limit = 0 };
			return ((MeterBrokerDB)m_brokerDB).GetAllPagingByKeyChart(paging, KeyChart, out total);
		}


		/// <summary>
		/// Gets the meter local ids by a list of local ids.
		/// </summary>
		/// <param name="localIds">The local ids.</param>
		/// <returns>The local ids.</returns>
		public HashSet<string> GetMeterLocalIdsByLocalIds(IEnumerable<string> localIds) {
			return ((MeterBrokerDB)m_brokerDB).GetMeterLocalIdsByLocalIds(localIds);
		}

		/// <summary>
		/// Gets a list of Meters for a specific chart and populate the chidren items specified as fields. "*" indicates that all fields should be populated.
		/// </summary>
		/// <param name="KeyChart">The Key of the Chart.</param>
		///<param name="fields">The list of fields. "*" indicates all fields.</param>
		/// <returns></returns>
		public virtual List<Meter> GetListByKeyChart(string KeyChart, params string[] fields) {
			var list = GetListByKeyChart(KeyChart);
			PopulateList(list, fields);
			return list;
		}

		/// <summary>
		/// Gets a store of meters for a specific chart.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyChart">The Key of the chart.</param>
		/// <returns>A JsonStore.</returns>
		public JsonStore<Meter> GetStoreByKeyChart(PagingParameter paging, string KeyChart) {
			int total;
			return ((MeterBrokerDB)m_brokerDB).GetAllPagingByKeyChart(paging, KeyChart, out total).ToJsonStore(total, paging);
		}

		/// <summary>
		/// Saves the chart meters.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="meters">The meters.</param>
		/// <returns></returns>
		public List<Meter> SaveChartMeters(Chart entity, CrudStore<Meter> meters) {
			return ((MeterBrokerDB)m_brokerDB).SaveChartMeters(entity, meters);
		}

		/// <summary>
		/// Returns the complete list of Meter for a specific Chart based on Meter Classification Filter, Spatial Filter, Meters instances.
		/// </summary>
		/// <param name="keyChart">The Key of the chart.</param>
		/// <param name="keyMeterAndLocationOnly">if set to <c>true</c> [key meter and location only].</param>
		/// <param name="filterSpatialKeys">The filter spatial keys.</param>
		/// <param name="metersClassificationsKeys">The meters classifications keys.</param>
		/// <param name="metersKeys">The meters keys.</param>
		/// <param name="fields">The list of fields. "*" indicates all fields.</param>
		/// <returns></returns>
		public List<Meter> GetCompleteListByKeyChart(string keyChart, bool keyMeterAndLocationOnly, IEnumerable<string> filterSpatialKeys = null,
				IEnumerable<string> metersClassificationsKeys = null, IEnumerable<string> metersKeys = null, params string[] fields) {

			var retVal = ((MeterBrokerDB)m_brokerDB).GetCompleteListByKeyChart(keyChart, keyMeterAndLocationOnly, filterSpatialKeys, metersClassificationsKeys, metersKeys);
			if (fields.Length > 0)
				PopulateList(retVal, fields);
			return retVal;
		}
		#endregion

		#region MeterDataExportTask

		/// <summary>
		/// Gets a store of meters for a specific meter data export task.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyMeterDataExportTask">The key meter data export task.</param>
		/// <returns>
		/// A JsonStore.
		/// </returns>
		public JsonStore<Meter> GetStoreByKeyMeterDataExportTask(PagingParameter paging, string KeyMeterDataExportTask) {
			int total;
			return ((MeterBrokerDB)m_brokerDB).GetAllPagingByKeyMeterDataExportTask(paging, KeyMeterDataExportTask, out total).ToJsonStore(total, paging);
		}

		/// <summary>
		/// Saves the MeterDataExportTask meters.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="meters">The meters.</param>
		/// <returns></returns>
		public List<Meter> SaveMeterDataExportTaskMeters(MeterDataExportTask entity, CrudStore<Meter> meters) {
			return ((MeterBrokerDB)m_brokerDB).SaveMeterDataExportTaskMeters(entity, meters);
		}

		#endregion

		#region AlarmDefinition
		/// <summary>
		/// Gets a store of meters for a specific AlarmDefinition.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyAlarmDefinition">The key alarm definition.</param>
		/// <returns>
		/// A JsonStore.
		/// </returns>
		public JsonStore<Meter> GetStoreByKeyAlarmDefinition(PagingParameter paging, string KeyAlarmDefinition) {
			int total;
			return ((MeterBrokerDB)m_brokerDB).GetAllPagingByKeyAlarmDefinition(paging, KeyAlarmDefinition, out total).ToJsonStore(total, paging);
		}

		/// <summary>
		/// Saves the AlarmDefinition meters.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="meters">The meters.</param>
		/// <returns></returns>
		public List<Meter> SaveAlarmDefinitionMeters(AlarmDefinition entity, CrudStore<Meter> meters) {
			return ((MeterBrokerDB)m_brokerDB).SaveAlarmDefinitionMeters(entity, meters);
		}


		/// <summary>
		/// Returns the complete list of Meter for a specific AlarmDefinition based on Meter Classification Filter, Spatial Filter, Meters instances.
		/// </summary>
		/// <param name="KeyAlarmDefinition">The Key of the AlarmDefinition.</param>
		/// <returns></returns>
		public List<Meter> GetCompleteListByKeyAlarmDefinition(string KeyAlarmDefinition) {
			var retVal = ((MeterBrokerDB)m_brokerDB).GetCompleteListByKeyAlarmDefinition(KeyAlarmDefinition);
			return retVal;
		}
		#endregion

		#region MeterValidationRule
		/// <summary>
		/// Gets a store of meters for a specific MeterValidationRule.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyMeterValidationRule">The key MeterValidationRule.</param>
		/// <returns>
		/// A JsonStore.
		/// </returns>
		public JsonStore<Meter> GetStoreByKeyMeterValidationRule(PagingParameter paging, string KeyMeterValidationRule) {
			int total;
			return ((MeterBrokerDB)m_brokerDB).GetAllPagingByKeyMeterValidationRule(paging, KeyMeterValidationRule, out total).ToJsonStore(total, paging);
		}


		/// <summary>
		/// Saves the MeterValidationRule meters.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="meters">The meters.</param>
		/// <returns></returns>
		public List<Meter> SaveMeterValidationRuleMeters(MeterValidationRule entity, CrudStore<Meter> meters) {
			return ((MeterBrokerDB)m_brokerDB).SaveMeterValidationRuleMeters(entity, meters);
		}

		/// <summary>
		/// Returns the complete list of Meter for a specific MeterValidationRule based on Meter Classification Filter, Spatial Filter, Meters instances.
		/// </summary>
		/// <param name="KeyMeterValidationRule">The Key of the MeterValidationRule.</param>
		/// <param name="fields">The list of fields. "*" indicates all fields.</param>
		/// <returns></returns>
		public List<Meter> GetCompleteListByKeyMeterValidationRule(string KeyMeterValidationRule, params string[] fields) {
			var retVal = ((MeterBrokerDB)m_brokerDB).GetCompleteListByKeyMeterValidationRule(KeyMeterValidationRule);
			if (fields.Length > 0)
				PopulateList(retVal, fields);
			return retVal;
		}

		#endregion

		#region EndPoint
		/// <summary>
		/// Retrieves all Meters that have an Endpoint attached.
		/// </summary>
		/// <returns></returns>
		public List<Meter> GetAllWithEndpoint() {
			var retVal = ((MeterBrokerDB)m_brokerDB).GetAllWithEndpoint();
			return retVal;
		}
		#endregion

		/// <summary>
		/// Populate the chidren items specified as fields. "*" indicates that all fields should be populated.
		/// </summary>
		/// <param name="item">The item</param>
		/// <param name="loadMode">The load mode.</param>
		/// <param name="fields">The list of fields. "*" indicates all fields.</param>
		public override void PopulateList(Meter item, EntityLoadMode loadMode, params string[] fields) {
			foreach (var field in fields) {
				if (field.ContainsAny("*", "MeterOperations")) {
					if (item.IsVirtual) {
						var brokerMeterOperation = Helper.CreateInstance<MeterOperationBroker>();
						var operations = brokerMeterOperation.GetListByKeyMeter(item.KeyMeter);
						brokerMeterOperation.ResolveForExport(operations, loadMode);
						item.MeterOperations = operations;
					}
				}
				if (field.ContainsAny("*", "CalendarEventCategory") && !string.IsNullOrEmpty(item.KeyCalendarEventCategory)) {
					var brokerCalendarEventCategory = Helper.CreateInstance<CalendarEventCategoryBroker>();
					item.CalendarEventCategory = brokerCalendarEventCategory.GetItem(item.KeyCalendarEventCategory);
				}
				if (!item.IsVirtual) {
					if (field.Equals("LastData")) {
						var brokerMeterData = Helper.CreateInstance<MeterDataBroker>();
						var meterData = brokerMeterData.GetLastByKeyMeter(item.KeyMeter);
						brokerMeterData.ResolveForExport(meterData, loadMode);
						item.LastData = meterData;
					}
					if (field.Equals("Data")) {
						var brokerMeterData = Helper.CreateInstance<MeterDataBroker>();
						int total;
						var pagingParameter = new PagingParameter {
							filters = new List<GridFilter>
							{
								new GridFilter
								{
									field = "KeyMeter", data=new GridData
									{
										comparison = "eq", type = "string", value = item.KeyMeter
									}
								}
							}
						};
						var meterData = brokerMeterData.GetAll(pagingParameter, PagingLocation.Database, out total);
						brokerMeterData.ResolveForExport(meterData, loadMode);
						item.Data = meterData;
					}
				}
			}
		}

		/// <summary>
		/// Resolves the references during mapping.
		/// </summary>
		/// <param name="mappingRecord">The mapping record.</param>
		/// <param name="persistedEntity">The persisted entity.</param>
		protected override void ResolveReferencesForImport(MappingRecord<Meter> mappingRecord, Meter persistedEntity) {
			ResolveLocationReferenceForImport(mappingRecord, persistedEntity);
			ResolveParentReferenceForImport<ClassificationItem>(e => e.KeyClassificationItem, mappingRecord.Entity, persistedEntity);
			ResolveParentReferenceForImport<MeterOperation>(e => e.KeyMeterOperationResult, mappingRecord.Entity, persistedEntity);
			ResolveParentReferenceForImport<CalendarEventCategory>(e => e.KeyCalendarEventCategory, mappingRecord.Entity, persistedEntity);
			if (!string.IsNullOrWhiteSpace(mappingRecord.Entity.KeyEndpoint)) {
				switch (mappingRecord.Entity.EndpointType) {
					case "Vizelia.FOL.BusinessEntities.RESTDataAcquisitionEndpoint":
						ResolveParentReferenceForImport<RESTDataAcquisitionEndpoint>(e => e.KeyEndpoint, mappingRecord.Entity, persistedEntity);
						break;
					case "Vizelia.FOL.BusinessEntities.StruxureWareDataAcquisitionEndpoint":
						ResolveParentReferenceForImport<StruxureWareDataAcquisitionEndpoint>(e => e.KeyEndpoint, mappingRecord.Entity, persistedEntity);
						break;
					case "Vizelia.FOL.BusinessEntities.EWSDataAcquisitionEndpoint":
						ResolveParentReferenceForImport<EWSDataAcquisitionEndpoint>(e => e.KeyEndpoint, mappingRecord.Entity, persistedEntity);
						break;
				}
			}
		}

		/// <summary>
		/// Prepares to export entity.
		/// </summary>
		/// <param name="entity">The entity.</param>
		protected override void ResolveReferencesForExport(Meter entity) {
			ResolveLocationReferenceForExport(entity);
			ResolveParentReferenceForExport<ClassificationItem>(e => e.KeyClassificationItem, entity);
			ResolveParentReferenceForExport<MeterOperation>(e => e.KeyMeterOperationResult, entity);
			ResolveParentReferenceForExport<CalendarEventCategory>(e => e.KeyCalendarEventCategory, entity);
			if (!string.IsNullOrWhiteSpace(entity.KeyEndpoint)) {
				switch (entity.EndpointType) {
					case "Vizelia.FOL.BusinessEntities.RESTDataAcquisitionEndpoint":
						ResolveParentReferenceForExport<RESTDataAcquisitionEndpoint>(e => e.KeyEndpoint, entity);
						break;
					case "Vizelia.FOL.BusinessEntities.StruxureWareDataAcquisitionEndpoint":
						ResolveParentReferenceForExport<StruxureWareDataAcquisitionEndpoint>(e => e.KeyEndpoint, entity);
						break;
				}
			}
		}

		/// <summary>
		/// Gets a dictionnary for all the ClassificationItem, by KeyClassificationItem. 
		/// </summary>
		/// <returns></returns>
		public Dictionary<int, Meter> GetDictionary() {
			int total;
			var list = m_brokerDB.GetAll(new PagingParameter(), PagingLocation.Database, out total);
			PopulateList(list, "MeterOperations", "CalendarEventCategory");

			return list.ToDictionary(m => MeterHelper.ConvertMeterKeyToInt(m.KeyMeter));
		}

	}

}
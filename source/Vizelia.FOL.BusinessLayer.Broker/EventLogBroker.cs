﻿using System.Collections.Generic;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.DataLayer.BrokerDB;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for business entity EventLog.
	/// </summary>
	public class EventLogBroker : MappingEnabledBroker<EventLog, EventLogBrokerDB> {

		/// <summary>
		/// Get the list of EventLog from a Chart
		/// </summary>
		/// <param name="KeyChart">The key of the chart.</param>
		public List<EventLog> GetListByKeyChart(string KeyChart) {
			return ((EventLogBrokerDB)m_brokerDB).GetListByKeyChart(KeyChart);
		}

		/// <summary>
		/// Resolves the references for export.
		/// </summary>
		/// <param name="entity">The entity.</param>
		protected override void ResolveReferencesForExport(EventLog entity) {
			ResolveLocationReferenceForExport(entity);
			ResolveParentReferenceForExport<ClassificationItem>(e => e.KeyClassificationItem, entity);
		}

		/// <summary>
		/// Resolves the references during mapping.
		/// </summary>
		/// <param name="mappingRecord">The mapping record.</param>
		/// <param name="persistedEntity">The persisted entity.</param>
		protected override void ResolveReferencesForImport(MappingRecord<EventLog> mappingRecord, EventLog persistedEntity) {
			ResolveLocationReferenceForImport(mappingRecord, persistedEntity);
			ResolveParentReferenceForImport<ClassificationItem>(e => e.KeyClassificationItem, mappingRecord.Entity, persistedEntity);
		}

	}

}
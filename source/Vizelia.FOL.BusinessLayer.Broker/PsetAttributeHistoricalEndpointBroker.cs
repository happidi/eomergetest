﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.DataLayer.BrokerDB;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Localization;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for business entity PsetAttributeHistoricalEndpoint.
	/// </summary>
	public class PsetAttributeHistoricalEndpointBroker : BaseBroker<PsetAttributeHistoricalEndpoint, PsetAttributeHistoricalEndpointBrokerDB> {

		/// <summary>
		///  Get the  PsetAttributeHistoricalEndpoint for a specific Object.
		/// </summary>
		/// <param name="KeyObject">The key object.</param>
		/// <param name="KeyPropertySingleValue">The key property single value.</param>
		/// <returns></returns>
		public PsetAttributeHistoricalEndpoint GetItemByKeyObject(string KeyObject, string KeyPropertySingleValue) {
			return ((PsetAttributeHistoricalEndpointBrokerDB)m_brokerDB).GetItemByKeyObject(KeyObject, KeyPropertySingleValue);
		}


	}
}
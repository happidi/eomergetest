﻿using System.Collections.Generic;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.DataLayer.BrokerDB;
using Vizelia.FOL.Common;
using System;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for business entity AlarmInstance.
	/// </summary>
	public class AlarmInstanceBroker : MappingEnabledBroker<AlarmInstance, AlarmInstanceBrokerDB> {

		/// <summary>
		/// Get the list of saved AlarmInstance for a specific AlarmDefinition .
		/// </summary>
		/// <param name="KeyAlarmDefinition">The key alarm definition.</param>
		/// <returns></returns>
		public List<AlarmInstance> GetListByKeyAlarmDefinition(string KeyAlarmDefinition) {
			return ((AlarmInstanceBrokerDB)m_brokerDB).GetListByKeyAlarmDefinition(KeyAlarmDefinition);
		}

		/// <summary>
		/// Get the list of saved AlarmInstance for a specific Chart (that are coming from a AlarmDefinition based on this Chart).
		/// </summary>
		/// <param name="KeyChart">The key chart.</param>
		/// <returns></returns>
		public List<AlarmInstance> GetListByKeyChart(string KeyChart) {
			return ((AlarmInstanceBrokerDB)m_brokerDB).GetListByKeyChart(KeyChart);
		}

		/// <summary>
		/// Get the list of saved AlarmInstance for a specific Chart (that are coming from a cross of the Chart AlarmDefinition Classification and Spatial Filter).
		/// </summary>
		/// <param name="KeyChart">The key chart.</param>
		/// <param name="startDate">The start date.</param>
		/// <param name="endDate">The end date.</param>
		/// <param name="timeInterval">The time interval.</param>
		/// <param name="timeZone">The time zone.</param>
		/// <returns></returns>
		public List<AlarmInstanceAsDataSource> GetListAsDataSourceByKeyChart(string KeyChart, DateTime startDate, DateTime endDate, AxisTimeInterval timeInterval, TimeZoneInfo timeZone) {
			return ((AlarmInstanceBrokerDB)m_brokerDB).GetListAsDataSourceByKeyChart(KeyChart, startDate, endDate, timeInterval, timeZone);
		}

		/// <summary>
		/// Gets all the AlarmInstance for a specific AlarmDefinition.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyAlarmDefinition">The Key of the AlarmDefinition.</param>
		public JsonStore<AlarmInstance> GetStoreByKeyAlarmDefinition(PagingParameter paging, string KeyAlarmDefinition) {
			int total;
			return ((AlarmInstanceBrokerDB)m_brokerDB).GetAllPagingByKeyAlarmDefinition(paging, KeyAlarmDefinition, out total).ToJsonStore(total, paging);
		}

		/// <summary>
		/// Gets all the AlarmInstance for a specific AlarmTable.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyAlarmTable">The Key of the AlarmTable.</param>
		public JsonStore<AlarmInstance> GetStoreByKeyAlarmTable(PagingParameter paging, string KeyAlarmTable) {
			int total;
			return ((AlarmInstanceBrokerDB)m_brokerDB).GetAllPagingByKeyAlarmTable(paging, KeyAlarmTable, out total).ToJsonStore(total, paging);
		}


		/// <summary>
		/// Gets all the AlarmInstance for a specific MeterValidationRule.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyMeterValidationRule">The key meter validation rule.</param>
		/// <returns></returns>
		public JsonStore<AlarmInstance> GetStoreByKeyMeterValidationRule(PagingParameter paging, string KeyMeterValidationRule) {
			int total;
			return ((AlarmInstanceBrokerDB)m_brokerDB).GetAllPagingByKeyMeterValidationRule(paging, KeyMeterValidationRule, out total).ToJsonStore(total, paging);
		}

		/// <summary>
		/// Gets all the AlarmInstance for a specific MeterData.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyMeterData">The key meter data.</param>
		/// <returns></returns>
		public JsonStore<AlarmInstance> GetStoreByKeyMeterData(PagingParameter paging, string KeyMeterData) {
			int total;
			return ((AlarmInstanceBrokerDB)m_brokerDB).GetAllPagingByKeyMeterData(paging, KeyMeterData, out total).ToJsonStore(total, paging);
		}

		/// <summary>
		/// Gets all the AlarmInstance for a specific Meter.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyMeter">The key meter.</param>
		/// <returns></returns>
		public JsonStore<AlarmInstance> GetStoreByKeyMeter(PagingParameter paging, string KeyMeter) {
			int total;
			return ((AlarmInstanceBrokerDB)m_brokerDB).GetAllPagingByKeyMeter(paging, KeyMeter, out total).ToJsonStore(total, paging);
		}

		/// <summary>
		/// Get the list of opened AlarmInstance for a specific Location .
		/// </summary>
		/// <param name="KeyLocation">The key of the location.</param>
		/// <param name="KeyClassificationItem">The key classification item.</param>
		/// <returns></returns>
		public List<AlarmInstance> GetOpenListByKeyLocation(string KeyLocation, string KeyClassificationItem) {
			return ((AlarmInstanceBrokerDB)m_brokerDB).GetOpenListByKeyLocation(KeyLocation, KeyClassificationItem);
		}


		/// <summary>
		/// Get the latest AlarmInstances with an InstanceDatetime newer than Now - seconds.
		/// </summary>
		/// <param name="seconds">The seconds.</param>
		/// <param name="paging">The paging.</param>
		/// <returns></returns>
		public List<AlarmInstance> GetLatest(int seconds, PagingParameter paging) {
			int total;
			return ((AlarmInstanceBrokerDB)m_brokerDB).GetLatest(seconds, paging, out  total);
		}

		/// <summary>
		/// Populate the chidren items specified as fields. "*" indicates that all fields should be populated.
		/// </summary>
		/// <param name="item">The item</param>
		/// <param name="loadMode">The load mode.</param>
		/// <param name="fields">The list of fields. "*" indicates all fields.</param>
		public override void PopulateList(AlarmInstance item, EntityLoadMode loadMode, params string[] fields) {
			foreach (string field in fields) {
				if (field.ContainsAny("*", "AlarmDefinition") && string.IsNullOrEmpty(item.KeyAlarmDefinition) == false) {
					var brokerAlarmDefinition = Helper.CreateInstance<AlarmDefinitionBroker>();
					AlarmDefinition alarmDefinition = brokerAlarmDefinition.GetItem(item.KeyAlarmDefinition);
					if (alarmDefinition != null) {
						brokerAlarmDefinition.ResolveForExport(alarmDefinition, loadMode);
						item.AlarmDefinition = alarmDefinition;
					}
					break;
				}

				if (field.ContainsAny("*", "MeterValidationRule") && string.IsNullOrEmpty(item.KeyMeterValidationRule) == false) {
					var brokerMeterValidationRule = Helper.CreateInstance<MeterValidationRuleBroker>();
					MeterValidationRule rule = brokerMeterValidationRule.GetItem(item.KeyMeterValidationRule);
					if (rule != null) {
						//brokerMeterValidationRule.ResolveForExport(rule, loadMode);
						item.MeterValidationRule = rule;
					}
					break;
				}
			}
		}

		/// <summary>
		/// Resolves the references for import.
		/// </summary>
		/// <param name="mappingRecord">The mapping record.</param>
		/// <param name="persistedEntity">The persisted entity.</param>
		protected override void ResolveReferencesForImport(MappingRecord<AlarmInstance> mappingRecord, AlarmInstance persistedEntity) {
			ResolveParentReferenceForImport<AlarmDefinition>(e => e.KeyAlarmDefinition, mappingRecord.Entity, persistedEntity);
			ResolveParentReferenceForImport<Meter>(e => e.KeyMeter, mappingRecord.Entity, persistedEntity);
		}

		/// <summary>
		/// Resolves the references for export.
		/// </summary>
		/// <param name="entity">The entity.</param>
		protected override void ResolveReferencesForExport(AlarmInstance entity) {
			ResolveParentReferenceForExport<AlarmDefinition>(e => e.KeyAlarmDefinition, entity);
			ResolveParentReferenceForExport<Meter>(e => e.KeyMeter, entity);
		}
		
		/// <summary>
		/// Deletes all existing alarm instances for a specific AlarmDefinition or MeterValidationRule.
		/// </summary>
		/// <param name="KeyAlarmDefinition">The key alarm definition.</param>
		/// <param name="KeyMeterValidationRule">The key meter validation rule.</param>
		/// <returns></returns>
		public bool DeleteAll(string KeyAlarmDefinition, string KeyMeterValidationRule) {
			return ((AlarmInstanceBrokerDB)m_brokerDB).DeleteAll(KeyAlarmDefinition, KeyMeterValidationRule);
		}

	    /// <summary>
	    /// Deletes the instances by key source.
	    /// </summary>
		/// <param name="NumberOfRowsToDelete">The number of rows to delete.</param>
	    /// <param name="SourceKeysList">The source list.</param>
	    /// <param name="InstanceSource">The instance source.</param>
	    /// <exception cref="System.NotImplementedException"></exception>
	    public void DeleteLimitedNumberRowsByKeySource(int NumberOfRowsToDelete, IEnumerable<string> SourceKeysList, AlarmInstanceSource InstanceSource) {
            int totalRowsToDelete = ((AlarmInstanceBrokerDB)m_brokerDB).GetCountByKeySource(SourceKeysList, InstanceSource);
            if (totalRowsToDelete > 0) {
            foreach (var key in SourceKeysList) {
                var rowsDeleted = 0;
	            var totalRowsDeleted = 0;
                do {
                    rowsDeleted = ((AlarmInstanceBrokerDB)m_brokerDB).DeleteLimitedNumberRowsByKeySource(NumberOfRowsToDelete, key, InstanceSource);
                    totalRowsDeleted += rowsDeleted;
                    LongRunningOperationService.ReportProgress(totalRowsDeleted * 100 / totalRowsToDelete);
                } while (rowsDeleted == NumberOfRowsToDelete);
	        }
        }
	}
	}

}

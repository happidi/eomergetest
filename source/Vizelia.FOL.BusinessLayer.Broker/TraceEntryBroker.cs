﻿using System.Collections.Generic;
using System.Linq;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.DataLayer.BrokerDB;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for business entity TraceEntry.
	/// </summary>
	public class TraceEntryBroker : BaseBroker<TraceEntry, TraceEntryBrokerDB> {

		/// <summary>
		/// Gets a store of TraceEntries for a specific request id.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="requestId">The request id.</param>
		public JsonStore<TraceEntry> GetStoreByRequestId(PagingParameter paging, string requestId) {
			int total;
			if (paging.filters == null)
				paging.filters = new List<GridFilter>();

			paging.filters.Add(new GridFilter {
				data = new GridData {
					comparison = "eq",
					type = "string",
					value = requestId
				},
				field = "RequestId"
			});

			return m_brokerDB.GetAll(paging, PagingLocation.Database, out total).ToJsonStore(total, paging);
		}

		/// <summary>
		/// Gets the TraceEntry for an exception that occured during the request specified by the request id.
		/// </summary>
		/// <param name="requestId">The request id.</param>
		/// <returns></returns>
		public TraceEntry GetErrorEntryByRequestId(string requestId) {
			var pagingParameter = new PagingParameter(){filters = new List<GridFilter>()};
			pagingParameter.filters.Add(new GridFilter {
				data = new GridData {
					comparison = "eq",
					type = "string",
					value = "ErrorHandler"
				},
				field = "TraceCategory"
			});
			var results = GetStoreByRequestId(pagingParameter, requestId);
			if (results.records.Count == 0)
				return null;
			return results.records[0];
		}

		/// <summary>
		/// Purges the specified days to keep logs.
		/// </summary>
		/// <param name="daysToKeepLogs">The days to keep logs.</param>
		public void Purge(int daysToKeepLogs) {
			((TraceEntryBrokerDB)m_brokerDB).Purge(daysToKeepLogs);
		}


		/// <summary>
		/// Gets the store by job run instance request id.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="jobRunInstanceRequestId">The job run instance request id.</param>
		/// <returns></returns>
		public JsonStore<TraceEntry> GetStoreByJobRunInstanceRequestId(PagingParameter paging, string jobRunInstanceRequestId) {
			JsonStore<TraceEntry> storeByJobRunInstanceRequestId = GetStoreByJobRunInstanceRequestId(jobRunInstanceRequestId);
			var result = storeByJobRunInstanceRequestId.records;
			return result.ToJsonStoreWithFilter(paging);
		}

		/// <summary>
		/// Gets the store by job run instance request id.
		/// </summary>
		/// <param name="jobRunInstanceRequestId">The job run instance request id.</param>
		/// <returns></returns>
		public JsonStore<TraceEntry> GetStoreByJobRunInstanceRequestId(string jobRunInstanceRequestId) {
			return ((TraceEntryBrokerDB)m_brokerDB).GetStoreByJobRunInstanceRequestId(jobRunInstanceRequestId);
		}

		/// <summary>
		/// Gets the store of last job run by key job.
		/// </summary>
		/// <param name="jobKeys">The key job.</param>
		/// <returns></returns>
		public Dictionary<string, List<TraceEntry>> GetLastJobRunByKeys(IEnumerable<string> jobKeys)
		{
			return ((TraceEntryBrokerDB)m_brokerDB).GetLastJobRunByKeys(jobKeys);
		}

		/// <summary>
		/// Gets the store by job identifier.
		/// </summary>
		/// <param name="paging"></param>
		/// <param name="keyJob">The key job.</param>
		/// <returns></returns>
		public JsonStore<JobInstance> GetStoreByJobId(PagingParameter paging, string keyJob)
		{
			return ((TraceEntryBrokerDB)m_brokerDB).GetStoreByKeyJob(paging, keyJob);
		}
	}
}
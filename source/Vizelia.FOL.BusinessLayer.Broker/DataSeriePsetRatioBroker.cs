﻿using System.Collections.Generic;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.DataLayer.BrokerDB;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for business entity DataSeriePsetRatio.
	/// </summary>
	public class DataSeriePsetRatioBroker : MappingEnabledBroker<DataSeriePsetRatio, DataSeriePsetRatioBrokerDB> {

		/// <summary>
		/// Initializes a new instance of the <see cref="DataSeriePsetRatioBroker"/> class.
		/// </summary>
		public DataSeriePsetRatioBroker() {
			m_database_errors.Add(50000, "error_msg_invalid_dataseriepsetratio_pset", "Pset");
		}

		/// <summary>
		/// Get the list of saved DataSeriePsetRatio from a DataSerie
		/// </summary>
		/// <param name="KeyDataSerie">The key of the DataSerie.</param>
		public List<DataSeriePsetRatio> GetListByKeyDataSerie(string KeyDataSerie) {
			return ((DataSeriePsetRatioBrokerDB)m_brokerDB).GetListByKeyDataSerie(KeyDataSerie);
		}

		/// <summary>
		/// Gets a store of DataSeriePsetRatio for a specific DataSerie.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyDataSerie">The Key of the DataSerie.</param>
		/// <returns>A JsonStore.</returns>
		public JsonStore<DataSeriePsetRatio> GetStoreByKeyDataSerie(PagingParameter paging, string KeyDataSerie) {
			int total;
			return ((DataSeriePsetRatioBrokerDB)m_brokerDB).GetAllPagingByKeyDataSerie(paging, KeyDataSerie, out total).ToJsonStore(total, paging);
		}


		/// <summary>
		/// Saves the DataSerie DataSeriePsetRatio.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="ratios">The DataSeriePsetRatio store.</param>
		/// <returns></returns>
		public List<DataSeriePsetRatio> SaveDataSerieDataSeriePsetRatio(DataSerie entity, CrudStore<DataSeriePsetRatio> ratios) {
			return ((DataSeriePsetRatioBrokerDB)m_brokerDB).SaveDataSerieDataSeriePsetRatio(entity, ratios);
		}


		/// <summary>
		/// Resolves the references for import.
		/// </summary>
		/// <param name="mappingRecord">The mapping record.</param>
		/// <param name="persistedEntity">The persisted entity.</param>
		protected override void ResolveReferencesForImport(MappingRecord<DataSeriePsetRatio> mappingRecord, DataSeriePsetRatio persistedEntity) {
			ResolveParentReferenceForImport<DataSerie>(e => e.KeyDataSerie, mappingRecord.Entity, persistedEntity);
			ResolveParentReferenceForImport<Chart>(e => e.KeyChart, mappingRecord.Entity, persistedEntity);
			ResolveParentReferenceForImport<PsetAttributeDefinition>(e => e.PsetAttributeLocalId, mappingRecord.Entity, persistedEntity);
		}

		/// <summary>
		/// Resolves the references for export.
		/// </summary>
		/// <param name="entity">The entity.</param>
		protected override void ResolveReferencesForExport(DataSeriePsetRatio entity) {
			ResolveParentReferenceForExport<DataSerie>(e => e.KeyDataSerie, entity);
			ResolveParentReferenceForExport<Chart>(e => e.KeyChart, entity);
		}
	}

}
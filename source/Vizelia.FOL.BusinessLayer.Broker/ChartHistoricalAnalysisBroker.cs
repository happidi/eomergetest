﻿using System.Collections.Generic;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.DataLayer.BrokerDB;
using Vizelia.FOL.Common.Localization;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for business entity ChartHistoricalAnalysis.
	/// </summary>
	public class ChartHistoricalAnalysisBroker : MappingEnabledBroker<ChartHistoricalAnalysis, ChartHistoricalAnalysisBrokerDB> {


		/// <summary>
		/// Initializes a new instance of the <see cref="ChartHistoricalAnalysisBroker"/> class.
		/// </summary>
		public ChartHistoricalAnalysisBroker() {
				m_database_errors.Add(1, Langue.error_msg_charthistoricalanalysis_alreadyexist, "Frequency");
				m_database_errors.Add(2, Langue.error_msg_uniquefield, "Name");
		}
		/// <summary>
		/// Get the list of saved ChartHistoricalAnalysis from a Chart
		/// </summary>
		/// <param name="KeyChart">The key of the chart.</param>
		public List<ChartHistoricalAnalysis> GetListByKeyChart(string KeyChart) {
			return ((ChartHistoricalAnalysisBrokerDB)m_brokerDB).GetListByKeyChart(KeyChart);
		}

		/// <summary>
		/// Gets a store of ChartHistoricalAnalysis for a specific chart.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyChart">The Key of the chart.</param>
		/// <returns>A JsonStore.</returns>
		public JsonStore<ChartHistoricalAnalysis> GetStoreByKeyChart(PagingParameter paging, string KeyChart) {
			int total;
			return ((ChartHistoricalAnalysisBrokerDB)m_brokerDB).GetAllPagingByKeyChart(paging, KeyChart, out total).ToJsonStore(total, paging);
		}


		/// <summary>
		/// Saves the chart ChartHistoricalAnalysis.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="charthistoricalanalysis">The ChartHistoricalAnalysis store.</param>
		/// <returns></returns>
		public List<ChartHistoricalAnalysis> SaveChartChartHistoricalAnalysis(Chart entity, CrudStore<ChartHistoricalAnalysis> charthistoricalanalysis) {
			return ((ChartHistoricalAnalysisBrokerDB)m_brokerDB).SaveChartChartHistoricalAnalysis(entity, charthistoricalanalysis);
		}

		/// <summary>
		/// Resolves the references for export.
		/// </summary>
		/// <param name="entity">The entity.</param>
		protected override void ResolveReferencesForExport(ChartHistoricalAnalysis entity) {
			ResolveParentReferenceForExport<Chart>(e => e.KeyChart, entity);
		}

		/// <summary>
		/// Resolves the references during mapping.
		/// </summary>
		/// <param name="mappingRecord">The mapping record.</param>
		/// <param name="persistedEntity">The persisted entity.</param>
		protected override void ResolveReferencesForImport(MappingRecord<ChartHistoricalAnalysis> mappingRecord, ChartHistoricalAnalysis persistedEntity) {
			ResolveParentReferenceForImport<Chart>(e => e.KeyChart, mappingRecord.Entity, persistedEntity);
		}

		/// <summary>
		/// Copies the specified item.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <param name="formResponse">The form response.</param>
		/// <param name="Creator">The creator.</param>
		/// <param name="incrementTitle">if set to <c>true</c> [increment title].</param>
		/// <returns></returns>
		public override ChartHistoricalAnalysis Copy(ChartHistoricalAnalysis item, out FormResponse formResponse, string Creator = null, bool incrementTitle = true) {
			var localId = item.LocalId;
			var newItem = base.Copy(item, out formResponse, Creator);

			//we need to fix the LocalId of the CartHistoricalAnalysis because it s set to null in the base.Copy and so a GUID is generated.
			newItem.LocalId = localId;
			this.Save(newItem);
			return newItem;
		}
	}

}
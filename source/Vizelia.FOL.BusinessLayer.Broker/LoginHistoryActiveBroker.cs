﻿using Vizelia.FOL.DataLayer.BrokerDB;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for business entity Login History active.
	/// </summary>
	public class LoginHistoryActiveBroker : LoginHistoryBroker {

		/// <summary>
		/// Public ctor.
		/// </summary>
		public LoginHistoryActiveBroker() {
			m_brokerDB = Helper.CreateInstance<LoginHistoryActiveBrokerDB>();
			//m_brokerDB = new LoginHistoryActiveBrokerDB();
		}

		

	}

}
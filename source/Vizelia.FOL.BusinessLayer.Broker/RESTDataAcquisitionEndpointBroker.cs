﻿using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.DataLayer.BrokerDB;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for business entity RESTDataAcquisitionEndpoint.
	/// </summary>
	public class RESTDataAcquisitionEndpointBroker : DataAcquisitionEndpointBroker<RESTDataAcquisitionEndpoint, RESTDataAcquisitionEndpointBrokerDB> {
		/// <summary>
		/// Resolves the references for import.
		/// </summary>
		/// <param name="mappingRecord">The mapping record.</param>
		/// <param name="persistedEntity">The persisted entity.</param>
		protected override void ResolveReferencesForImport(MappingRecord<RESTDataAcquisitionEndpoint> mappingRecord, RESTDataAcquisitionEndpoint persistedEntity) {
			ResolveParentReferenceForImport<RESTDataAcquisitionContainer>(x => x.KeyDataAcquisitionContainer, mappingRecord.Entity, persistedEntity);
		}

		/// <summary>
		/// Resolves the references for export.
		/// </summary>
		/// <param name="entity">The entity.</param>
		protected override void ResolveReferencesForExport(RESTDataAcquisitionEndpoint entity) {
			ResolveParentReferenceForExport<RESTDataAcquisitionContainer>(x => x.KeyDataAcquisitionContainer, entity);
		}
	}
}
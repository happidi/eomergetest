﻿using System.Collections.Generic;
using System.Linq;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.DataLayer.BrokerDB;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// 
	/// </summary>
	public class DetailedAuditBroker : BaseBroker<DetailedAudit, DetailedAuditBrokerDB> {
		/// <summary>
		/// Gets or sets the name of the entity.
		/// </summary>
		/// <value>
		/// The name of the entity.
		/// </value>
		public string EntityName { get; set; }

		/// <summary>
		/// Gets all.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="location">The location.</param>
		/// <param name="total">The total.</param>
		/// <param name="useSecurableFilter">if set to <c>true</c> [use securable filter].</param>
		/// <param name="isHierarchyEnabled">if set to <c>true</c> [is hierarchy enabled].</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public override List<DetailedAudit> GetAll(PagingParameter paging, PagingLocation location, out int total, bool useSecurableFilter = true, bool isHierarchyEnabled = true, params string[] fields) {
			DrillDownFilters(paging);
			var entityName = EntityName.Split('.')[0];
			var type = typeof(BaseBusinessEntity);
			((DetailedAuditBrokerDB) m_brokerDB).LoadPropertiesTree(type.Assembly.GetType(type.FullName.Replace(type.Name, entityName)));
			var detailedAudits = m_brokerDB.GetAll(paging, location, out total, useSecurableFilter);
			var audits = detailedAudits.Where(x => x.ChangedFields != null && x.ChangedFields.Count > 0).ToList();
			total = audits.Count;
			return audits;
		}
	}
}

﻿using System.Collections.Generic;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.DataLayer.BrokerDB;
using Vizelia.FOL.Common;
using Vizelia.FOL.BusinessEntities.Mapping;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for business entity DrawingCanvas.
	/// </summary>
	public class DrawingCanvasBroker : MappingEnabledBroker<DrawingCanvas, DrawingCanvasBrokerDB> {
		/// <summary>
		/// Populate the chidren items specified as fields. "*" indicates that all fields should be populated.
		/// </summary>
		/// <param name="item">The item</param>
		/// <param name="loadMode">The load mode.</param>
		/// <param name="fields">The list of fields. "*" indicates all fields.</param>
		public override void PopulateList(DrawingCanvas item, EntityLoadMode loadMode, params string[] fields) {
			foreach (string field in fields) {
				if (field.ContainsAny("*", "Items")) {
					item.Items = new List<DrawingCanvasItem>();

					var brokerImage = Helper.CreateInstance<DrawingCanvasImageBroker>();
					item.Items.AddRange(brokerImage.GetListByKeyDrawingCanvas(item.KeyDrawingCanvas));

					var brokerChart = Helper.CreateInstance<DrawingCanvasChartBroker>();
					item.Items.AddRange(brokerChart.GetListByKeyDrawingCanvas(item.KeyDrawingCanvas));
				}
			}
		}

		/// <summary>
		/// Copies the specified item.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <param name="formResponse">The form response.</param>
		/// <param name="Creator">The creator.</param>
		/// <param name="incrementTitle">if set to <c>true</c> [increment title].</param>
		/// <returns></returns>
		public override DrawingCanvas Copy(DrawingCanvas item, out FormResponse formResponse, string Creator = null, bool incrementTitle = true) {
			var newItem= base.Copy(item, out formResponse, Creator, incrementTitle);
			var  brokerChart = Helper.CreateInstance<DrawingCanvasChartBroker>();
			var  brokerImage= Helper.CreateInstance<DrawingCanvasImageBroker>();
			foreach (var child in item.Items) {
				FormResponse response;
				if(child.GetType()==typeof(DrawingCanvasImage)) {
					((DrawingCanvasImage)child).KeyDrawingCanvas=newItem.KeyDrawingCanvas;
					brokerImage.Copy((DrawingCanvasImage)child, out response, Creator);
				}
				else if(child.GetType()==typeof(DrawingCanvasChart)) {
					((DrawingCanvasChart)child).KeyDrawingCanvas = newItem.KeyDrawingCanvas;
					brokerChart.Copy((DrawingCanvasChart)child, out response, Creator);
				}
			}
			return newItem;
		}

		/// <summary>
		/// Resolves the references for export.
		/// </summary>
		/// <param name="entity">The entity.</param>
		protected override void ResolveReferencesForExport(DrawingCanvas entity) {
			// Do nothing.
		}

		/// <summary>
		/// Resolves the references during mapping.
		/// </summary>
		/// <param name="mappingRecord">The mapping record.</param>
		/// <param name="persistedEntity">The persisted entity.</param>
		protected override void ResolveReferencesForImport(MappingRecord<DrawingCanvas> mappingRecord, DrawingCanvas persistedEntity) {
			// Do nothing.
		}
	}
}
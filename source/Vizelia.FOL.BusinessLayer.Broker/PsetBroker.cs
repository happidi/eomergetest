﻿using System.Collections.Generic;
using System.Linq;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Vizelia.FOL.DataLayer.BrokerDB;

namespace Vizelia.FOL.BusinessLayer.Broker {

	/// <summary>
	/// The broker for pset.
	/// </summary>
	public class PsetBroker : BaseBroker<Pset, PsetBrokerDB> {


		/// <summary>
		/// Gets an pset given its name and the KeyObject it applies to.
		/// </summary>
		/// <param name="psetName">The name of the pset.</param>
		/// <param name="keyObject">The Key of the entity the pset applies to.</param>
		/// <returns>The pset.</returns>
		public Pset GetItem(string psetName, string keyObject) {
			Pset item = ((PsetBrokerDB)m_brokerDB).GetItem(psetName, keyObject);
			return item;
		}

		/// <summary>
		/// Gets a list of psets.
		/// </summary>
		/// <param name="psetName">The name of the pset.</param>
		/// <param name="keyObjects">An array of keys object for which we want to retreive the pset.</param>
		/// <returns>The list of items corresponding to the provided keys.</returns>
		public List<Pset> GetList(string psetName, string[] keyObjects) {
			List<Pset> psets = ((PsetBrokerDB)m_brokerDB).GetList(psetName, keyObjects);
			return psets;
		}

		/// <summary>
		/// Gets the name of the list by pset name and attribute.
		/// </summary>
		/// <param name="psetName">Name of the pset.</param>
		/// <param name="attributeName">Name of the attribute.</param>
		/// <param name="keyObjects">The key objects.</param>
		/// <returns></returns>
		public List<Pset> GetListByPsetNameAndAttributeName(string psetName, string attributeName, string[] keyObjects) {
			List<Pset> psets = ((PsetBrokerDB)m_brokerDB).GetListByPsetNameAndAttributeName(psetName, attributeName, keyObjects);
			return psets;
		}

		/// <summary>
		/// Creates the specified items.
		/// </summary>
		/// <param name="items">The items.</param>
		/// <param name="formResponse">The form response.</param>
		/// <returns></returns>
		public List<Pset> Create(List<Pset> items, out FormResponse formResponse) {
			formResponse = new FormResponse { success = true };
			// each pset is saved separatly
			foreach (Pset item in items) {
				FormResponse response;
				Create(item, out response);
				formResponse.success = formResponse.success && response.success;
				formResponse.msg += response.msg + "<br/>";
				if (response.errors != null) {
					foreach (var error in response.errors) {
						formResponse.AddError(error.id, error.msg);
					}
				}
				// formResponse.data = response.data;
			}

			if (!formResponse.success) {
				return items;
			}

			string[] keys = items.Select(item => item.KeyObject).Distinct().ToArray();
			List<Pset> psets = GetList(keys);

			return psets;
		}

		/// <summary>
		/// Creates the specified item.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <param name="formResponse">The form response.</param>
		/// <returns></returns>
		protected override Pset Create(Pset item, out FormResponse formResponse) {
			// Psets that have no attributes are not saved to the database, so no use of 
			// doing the database call.
			if (item == null || item.Attributes == null || !item.Attributes.Any()) {
				formResponse = new FormResponse { success = true };
				return item;
			}

			return base.Create(item, out formResponse);
		}

		/// <summary>
		/// Cleans the obsolete values of pset for an object.
		/// </summary>
		/// <param name="keyObject">The Key of the object to clean.</param>
		/// <param name="keyPropertySingleValues">The correct list of PsetAttributes that the object should expose.</param>
		private void CleanObsoleteValues(string keyObject, string[] keyPropertySingleValues) {
			((PsetBrokerDB)m_brokerDB).CleanObsoleteValues(keyObject, keyPropertySingleValues);
		}



		/// <summary>
		/// Saves a list of pset and returns the resulting list on the IPset interface PropertySetList of the response.data entity. 
		/// </summary>
		/// <param name="response">The response of the entity saved operation.</param>
		/// <param name="psets">The list of psets.</param>
		/// <returns>A formresponse corresponding to the save of the list of psets. Usually this result is not used.</returns>
		public void Save(FormResponse response, List<Pset> psets) {
			if (!response.success) return;

			FormResponse responsePset = null;
			IPset entityPset = response.data as IPset;
			if (entityPset != null && psets != null) {
				entityPset.PropertySetList = SaveForm((BaseBusinessEntity)response.data, psets, out responsePset);
			}

			if (responsePset != null && !responsePset.success) {
				throw new VizeliaException(responsePset.msg);
			}
		}

		/// <summary>
		/// Generic function to save a list of psets.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <param name="psets">The psets.</param>
		/// <param name="response">The response.</param>
		/// <returns></returns>
		private List<Pset> SaveForm(BaseBusinessEntity item, List<Pset> psets, out FormResponse response) {
			if (item != null && psets != null) {
				string propertyId = Helper.GetPropertyNameId(item.GetType());
				string propertyIdValue = item.GetPropertyValueString(propertyId, false, false);

				foreach (var pset in psets) {
					pset.KeyObject = propertyIdValue;
				}
				List<Pset> result = Create(psets, out response);
				return result;
			}
			response = null;
			return null;
		}

		/// <summary>
		/// Cleans the obsolete values of pset for an object.
		/// </summary>
		/// <param name="keyObject">The key object.</param>
		/// <param name="usageName">Name of the usage.</param>
		/// <param name="keyClassificationItem">The key classification item.</param>
		public void CleanObsoleteValues(string keyObject, string usageName, string keyClassificationItem) {
			PsetDefinitionBroker psetDefinitionBroker = Helper.CreateInstance<PsetDefinitionBroker>();
			var results = psetDefinitionBroker.GetListByUsageName(usageName, keyClassificationItem);
			CleanObsoleteValues(keyObject, (results.SelectMany(pset => pset.Attributes,
				(pset, attribute) => attribute.KeyPropertySingleValue)).ToArray());
		}
	}
}
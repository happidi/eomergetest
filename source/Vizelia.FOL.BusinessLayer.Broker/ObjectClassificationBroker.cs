﻿using System;
using System.Collections.Generic;
using System.Linq;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.DataLayer.BrokerDB;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Localization;


namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for business entity Building.
	/// </summary>
	public class ObjectClassificationBroker : MappingEnabledBroker<ObjectClassification, ObjectClassificationBrokerDB> {

		/// <summary>
		/// Gets an ObjectClassification from its values.
		/// </summary>
		/// <param name="item">The item containing the values.</param>
		/// <returns>A ObjectClassification if values were found in database, else null.</returns>
		public ObjectClassification GetItemByValues(ObjectClassification item) {
			return ((ObjectClassificationBrokerDB)m_brokerDB).GetItemByValues(item);
		}

		/// <summary>
		/// Gets a list of ObjectClassification from the KeyObject and optionally the Location of the Classification.
		/// This is usefull for deleting all assignments to an object without having to get the list before deleting.
		/// </summary>
		/// <param name="item">The item containing the values.</param>
		/// <returns>A ObjectClassification if values were found in database, else null.</returns>
		public List<ObjectClassification> GetListByKeyObject(ObjectClassification item) {
			return ((ObjectClassificationBrokerDB)m_brokerDB).GetListByKeyObject(item);
		}

		/// <summary>
		/// Gets the list of assigned object by key classification item.
		/// </summary>
		/// <param name="KeyClassificationItem">The key classification item.</param>
		/// <returns></returns>
		public List<ObjectClassification> GetListByKeyClassificationItem(string KeyClassificationItem) {
			return ((ObjectClassificationBrokerDB)m_brokerDB).GetListByKeyClassificationItem(KeyClassificationItem);
		}

		/// <summary>
		/// Gets the tree pset from a ClassificationItem node.
		/// </summary>
		/// <returns></returns>
		public TreeNode GetTreePset(string KeyClassificationItem) {
			PsetAttributeDefinitionBroker brokerPsetAttributeDefinition = Helper.CreateInstance<PsetAttributeDefinitionBroker>();
			List<ObjectClassification> listObjectClassification = this.GetListByKeyClassificationItem(KeyClassificationItem);
			List<PsetAttributeDefinition> listPsetAttributeDefintion = brokerPsetAttributeDefinition.GetList(listObjectClassification.Select(p => p.KeyObject).ToArray());
			PsetDefinitionBroker brokerPsetDefinition = Helper.CreateInstance<PsetDefinitionBroker>();
			List<TreeNode> nodesPset = listPsetAttributeDefintion.Select(p => p.KeyPropertySet).Distinct().Select(p => brokerPsetDefinition.GetItem(p)).ToList().GetTree();

			foreach (TreeNode node in nodesPset) {
				node.children = (from p in listPsetAttributeDefintion
								 where p.KeyPropertySet == ((PsetDefinition)node.entity).KeyPropertySet
								 select p.GetTree()).ToList();
			}
			if (nodesPset.Count > 0) {
				PsetDefinition pset = new PsetDefinition {
					PsetName = Langue.msg_pset
				};
				TreeNode psetNode = pset.GetTree();
				psetNode.children = nodesPset;
				return psetNode;
			}
			else
				return null;
		}

		/// <summary>
		/// Executes Mapping for a delete action.
		/// </summary>
		/// <param name="mappingRecord">The mapping record.</param>
		/// <param name="localId">The localid.</param>
		/// <param name="formResponse">The form response.</param>
		protected override void Mapping_Delete(MappingRecord<ObjectClassification> mappingRecord, string localId, out FormResponse formResponse) {
			formResponse = new FormResponse();
			var objectsToDelete = new List<ObjectClassification>();

			// case a specific KeyClassificationItem was provided
			if (mappingRecord.IsPropertyAssigned("KeyClassificationItem")) {
				PrepareEntityForImport(mappingRecord, null, localId, out formResponse);
				ObjectClassification self = GetItemByValues(mappingRecord.Entity);
				if (self != null)
					objectsToDelete.Add(self);

			}
			else {
				try {
					ResolveReferenceObject(mappingRecord.Entity, null);
					objectsToDelete = GetListByKeyObject(mappingRecord.Entity);
				}
				catch (ArgumentException exception) {
					formResponse.AddError(localId, exception.Message);
					formResponse.success = false;
					formResponse.exception = exception;
					return;
				}
			}

			if (objectsToDelete.Count == 0) {
				formResponse.success = false;
				formResponse.msg = "Entity not found";
				return;
			}

			foreach (ObjectClassification self in objectsToDelete) {
				bool deleted = false;
				try {
					deleted = Delete(self);
				}
				catch (Exception ex) {
					formResponse.msg = ex.Message;
				}
				formResponse.success = deleted;
				if (!deleted)
					return;

			}
		}

		/// <summary>
		/// Resolves the reference from KeyObject and TypeName.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="entityPersisted">The entity persisted.</param>
		private void ResolveReferenceObject(ObjectClassification entity, ObjectClassification entityPersisted) {
			switch (entity.TypeName) {
				case "IfcSite":
					ResolveParentReferenceForImport<Site>("KeyObject", entity, entityPersisted);
					break;
				case "IfcSpace":
					ResolveParentReferenceForImport<Space>("KeyObject", entity, entityPersisted);
					break;
				case "IfcOccupant":
					ResolveParentReferenceForImport<Occupant>("KeyObject", entity, entityPersisted);
					break;
				default:
					throw new ArgumentException("Unexpected TypeName");
			}
		}

		/// <summary>
		/// Resolves the references for export.
		/// </summary>
		/// <param name="entity">The entity.</param>
		protected override void ResolveReferencesForExport(ObjectClassification entity) {
			throw new NotImplementedException();
		}

		/// <summary>
		/// Resolves the references during mapping.
		/// </summary>
		/// <param name="mappingRecord">The mapping record.</param>
		/// <param name="persistedEntity">The persisted entity.</param>
		protected override void ResolveReferencesForImport(MappingRecord<ObjectClassification> mappingRecord, ObjectClassification persistedEntity) {
			ResolveParentReferenceForImport<ClassificationItem>("KeyClassificationItem", mappingRecord.Entity, persistedEntity);
			ResolveReferenceObject(mappingRecord.Entity, persistedEntity);
		}

	}

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.DataLayer.BrokerDB;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Localization;
using Vizelia.FOL.BusinessEntities.Mapping;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for business entity ChartDrillDown.
	/// </summary>
	public class ChartDrillDownBroker : BaseBroker<ChartDrillDown, ChartDrillDownBrokerDB> {


		/// <summary>
		/// Get the ChartDrillDown entity of a Chart
		/// </summary>
		/// <param name="KeyChart">The key of the chart.</param>
		public ChartDrillDown GetItemByKeyChart(string KeyChart) {
			return ((ChartDrillDownBrokerDB)m_brokerDB).GetItemByKeyChart(KeyChart);
		}

	}

}
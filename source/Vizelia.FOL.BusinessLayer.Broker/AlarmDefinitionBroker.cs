﻿using System.Collections.Generic;
using System.Linq;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using Vizelia.FOL.DataLayer.BrokerDB;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for business entity AlarmDefinition.
	/// </summary>
	public class AlarmDefinitionBroker : MappingEnabledBroker<AlarmDefinition, AlarmDefinitionBrokerDB> {


		/// <summary>
		/// Gets a list of AlarmDefinition for a specific location.
		/// </summary>
		/// <param name="KeyLocation">The Key location.</param>
		/// <returns></returns>
		public List<AlarmDefinition> GetListByKeyLocation(string KeyLocation) {
			return ((AlarmDefinitionBrokerDB)m_brokerDB).GetListByKeyLocation(KeyLocation);
		}

		/// <summary>
		/// Gets all  Enabled AlarmDefinition.
		/// </summary>
		/// <returns></returns>
		public List<AlarmDefinition> GetAllEnabled() {
			var paging = new PagingParameter {
				start = 0,
				limit = 0,
				filters = new List<GridFilter>
				{
					new GridFilter
					{
						field="Enabled",
						data=new GridData
						{
							type="boolean",
							value="true"
						}
					}
				}
			};
			int total;
			var retVal = m_brokerDB.GetAll(paging, PagingLocation.Database, out total);
			return retVal;
		}

		/// <summary>
		/// Get the list of saved AlarmDefinition for a specific AlarmTable .
		/// </summary>
		/// <param name="KeyAlarmTable">The key of the AlarmTable.</param>
		public List<AlarmDefinition> GetListByKeyAlarmTable(string KeyAlarmTable) {
			return ((AlarmDefinitionBrokerDB)m_brokerDB).GetListByKeyAlarmTable(KeyAlarmTable);
		}

		/// <summary>
		/// Get the list of saved AlarmDefinition for a specific Chart .
		/// </summary>
		/// <param name="KeyChart">The key chart.</param>
		/// <returns></returns>
		public List<AlarmDefinition> GetListByKeyChart(string KeyChart) {
			return ((AlarmDefinitionBrokerDB)m_brokerDB).GetListByKeyChart(KeyChart);
		}

		/// <summary>
		/// Gets all the AlarmDefinition for a specific AlarmTable.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyAlarmTable">The key alarm table.</param>
		/// <returns></returns>
		public JsonStore<AlarmDefinition> GetStoreByKeyAlarmTable(PagingParameter paging, string KeyAlarmTable) {
			int total;
			return ((AlarmDefinitionBrokerDB)m_brokerDB).GetAllPagingByKeyAlarmTable(paging, KeyAlarmTable, out total).ToJsonStore(total, paging);
		}

		/// <summary>
		/// Saves the AlarmTable alarmdefinitions.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="alarmdefinitions">The alarmdefinitions.</param>
		/// <returns></returns>
		public List<AlarmDefinition> SaveAlarmTableAlarmDefinitions(AlarmTable entity, CrudStore<AlarmDefinition> alarmdefinitions) {
			return ((AlarmDefinitionBrokerDB)m_brokerDB).SaveAlarmTableAlarmDefinitions(entity, alarmdefinitions);
		}

		/// <summary>
		/// Returns the complete list of AlarmDefinition for a specific Alarmtale based on AlarmDefinition Classification Filter, Spatial Filter, AlarmDefinition instances.
		/// </summary>
		/// <param name="KeyAlarmTable">The Key of the AlarmTable.</param>
		/// <param name="fields">The list of fields. "*" indicates all fields.</param>
		/// <returns></returns>
		public List<AlarmDefinition> GetCompleteListByKeyAlarmTable(string KeyAlarmTable, params string[] fields) {
			List<AlarmDefinition> definitions = ((AlarmDefinitionBrokerDB)m_brokerDB).GetCompleteListByKeyAlarmTable(KeyAlarmTable);
			if (definitions != null && definitions.Count > 0) {
				PopulateList(definitions, fields);
			}
			return definitions;
		}

		/// <summary>
		/// Populate the chidren items specified as fields. "*" indicates that all fields should be populated.
		/// </summary>
		/// <param name="item">The item</param>
		/// <param name="loadMode">The load mode.</param>
		/// <param name="fields">The list of fields. "*" indicates all fields.</param>
		public override void PopulateList(AlarmDefinition item, EntityLoadMode loadMode, params string[] fields) {
			foreach (string field in fields) {
				if (field.ContainsAny("*", "Meters")) {
					var brokerMeter = Helper.CreateInstance<MeterBroker>();
					var meters = brokerMeter.GetCompleteListByKeyAlarmDefinition(item.KeyAlarmDefinition);
					if (meters != null && meters.Count > 0) {
						brokerMeter.PopulateList(meters, loadMode, "*");
						brokerMeter.ResolveForExport(meters, loadMode);
						item.Meters = meters;
					}
				}

				if (field.ContainsAny("*", "CalendarEventCategory") && string.IsNullOrEmpty(item.KeyCalendarEventCategory) == false) {
					var brokerCalendarEventCategory = Helper.CreateInstance<CalendarEventCategoryBroker>();
					CalendarEventCategory eventCategory = brokerCalendarEventCategory.GetItem(item.KeyCalendarEventCategory);
					if (eventCategory != null) {
						brokerCalendarEventCategory.ResolveForExport(eventCategory, loadMode);
						item.CalendarEventCategory = eventCategory;
					}
				}

				if (field.ContainsAny("*", "ConnectedCharts")) {
					var brokerChart = Helper.CreateInstance<ChartBroker>();
					var charts = brokerChart.GetListByKeyAlarmDefinition(item.KeyAlarmDefinition);
					if (charts != null && charts.Any()) {
						brokerChart.ResolveForExport(charts, loadMode);
						item.ConnectedCharts = charts;
					}
				}
			}
		}

		/// <summary>
		/// Resolves the references for export.
		/// </summary>
		/// <param name="entity">The entity.</param>
		protected override void ResolveReferencesForExport(AlarmDefinition entity) {
			ResolveParentReferenceForExport<ClassificationItem>(e => e.KeyClassificationItem, entity);
			ResolveLocationReferenceForExport(entity);
		}

		/// <summary>
		/// Resolves the references during mapping.
		/// </summary>
		/// <param name="mappingRecord">The mapping record.</param>
		/// <param name="persistedEntity">The persisted entity.</param>
		protected override void ResolveReferencesForImport(MappingRecord<AlarmDefinition> mappingRecord, AlarmDefinition persistedEntity) {
			ResolveParentReferenceForImport<ClassificationItem>(e => e.KeyClassificationItem, mappingRecord.Entity, persistedEntity);
			ResolveLocationReferenceForImport(mappingRecord, persistedEntity);
		}
	}
}
﻿using System;
using System.Collections.Generic;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.DataLayer.BrokerDB;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for business entity ChartPsetAttributeHistorical.
	/// </summary>
	public class ChartPsetAttributeHistoricalBroker : MappingEnabledBroker<ChartPsetAttributeHistorical, ChartPsetAttributeHistoricalBrokerDB> {

		/// <summary>
		/// Get the list of saved ChartPsetAttributeHistorical from a Chart
		/// </summary>
		/// <param name="KeyChart">The key of the chart.</param>
		public List<ChartPsetAttributeHistorical> GetListByKeyChart(string KeyChart) {
			return ((ChartPsetAttributeHistoricalBrokerDB)m_brokerDB).GetListByKeyChart(KeyChart);
		}

		/// <summary>
		/// Gets a store of ChartPsetAttributeHistorical for a specific chart.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyChart">The Key of the chart.</param>
		/// <returns>A JsonStore.</returns>
		public JsonStore<ChartPsetAttributeHistorical> GetStoreByKeyChart(PagingParameter paging, string KeyChart) {
			int total;
			return ((ChartPsetAttributeHistoricalBrokerDB)m_brokerDB).GetAllPagingByKeyChart(paging, KeyChart, out total).ToJsonStore(total, paging);
		}


		/// <summary>
		/// Saves the chart ChartPsetAttributeHistorical.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="historicals">The ChartPsetAttributeHistorical store.</param>
		/// <returns></returns>
		public List<ChartPsetAttributeHistorical> SaveChartChartPsetAttributeHistorical(Chart entity, CrudStore<ChartPsetAttributeHistorical> historicals) {
			return ((ChartPsetAttributeHistoricalBrokerDB)m_brokerDB).SaveChartChartPsetAttributeHistorical(entity, historicals);
		}


		/// <summary>
		/// Resolves the references during mapping.
		/// </summary>
		/// <param name="mappingRecord">The mapping record.</param>
		/// <param name="persistedEntity">The persisted entity.</param>
		protected override void ResolveReferencesForImport(MappingRecord<ChartPsetAttributeHistorical> mappingRecord, ChartPsetAttributeHistorical persistedEntity) {
			ResolveParentReferenceForImport<Chart>(e => e.KeyChart, mappingRecord.Entity, persistedEntity);
			ResolveParentReferenceForImport<PsetAttributeDefinition>(e => e.KeyPropertySingleValue, mappingRecord.Entity, persistedEntity);

		}

		/// <summary>
		/// Resolves the references for export.
		/// </summary>
		/// <param name="entity">The entity.</param>
		protected override void ResolveReferencesForExport(ChartPsetAttributeHistorical entity) {
			ResolveParentReferenceForExport<Chart>(e => e.KeyChart, entity);
			ResolveParentReferenceForExport<PsetAttributeDefinition>(e => e.KeyPropertySingleValue, entity);
		}
	}

}
﻿using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common.Localization;
using Vizelia.FOL.DataLayer.BrokerDB;

namespace Vizelia.FOL.BusinessLayer.Broker {
	
	/// <summary>
	/// The broker for business entity PsetValue.
	/// </summary>
	public class PsetValueBroker : BaseBroker<PsetValue, PsetValueBrokerDB> {
		
		/// <summary>
		/// PsetValueBroker constructor.
		/// </summary>
		public PsetValueBroker() {
			m_database_errors.Add(2627, Langue.error_msg_uniquefield, "KeyObject, PsetName, AttributeName");
		}
	}
}

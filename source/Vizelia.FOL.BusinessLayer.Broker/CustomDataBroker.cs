﻿using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.DataLayer.BrokerDB;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for business entity CustomData
	/// </summary>
	public class CustomDataBroker : BaseBroker<CustomData, CustomDataBrokerDB> {
		/// <summary>
		/// Initializes a new instance of the <see cref="CustomDataBroker"/> class.
		/// </summary>
		public CustomDataBroker()
			: base() {
			if (m_database_errors.ContainsKey(100))
				m_database_errors[100].field = "ContainerName + KeyUser + DataKey";
		}

		/// <summary>
		/// Deletes all data by user name.
		/// </summary>
		/// <param name="containerName">Name of the container.</param>
		/// <param name="keyUser">The key user.</param>
		/// <returns></returns>
		public bool DeleteAll(string containerName, string keyUser) {
			var retVal = ((CustomDataBrokerDB)m_brokerDB).DeleteAll(containerName, keyUser);
			return retVal;
		}
	}
}

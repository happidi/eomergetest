﻿using System.Collections.Generic;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.DataLayer.BrokerDB;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for business entity ChartMarker.
	/// </summary>
	public class ChartMarkerBroker : MappingEnabledBroker<ChartMarker, ChartMarkerBrokerDB> {

		/// <summary>
		/// Get the list of saved ChartMarker from a Chart
		/// </summary>
		/// <param name="KeyChart">The key of the chart.</param>
		public List<ChartMarker> GetListByKeyChart(string KeyChart) {
			return ((ChartMarkerBrokerDB)m_brokerDB).GetListByKeyChart(KeyChart);
		}

		/// <summary>
		/// Get the list of saved ChartMarker from a Chart Axis
		/// </summary>
		/// <param name="KeyChartAxis">The key of the chart axis.</param>
		public List<ChartMarker> GetListByKeyChartAxis(string KeyChartAxis) {
			return ((ChartMarkerBrokerDB)m_brokerDB).GetListByKeyChartAxis(KeyChartAxis);
		}

		/// <summary>
		/// Gets a store of ChartMarker for a specific chart.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyChart">The Key of the chart.</param>
		/// <returns>A JsonStore.</returns>
		public JsonStore<ChartMarker> GetStoreByKeyChart(PagingParameter paging, string KeyChart) {
			int total;
			return ((ChartMarkerBrokerDB)m_brokerDB).GetAllPagingByKeyChart(paging, KeyChart, out total).ToJsonStore(total, paging);
		}


		/// <summary>
		/// Saves the chart ChartMarkers.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="ChartMarkers">The ChartMarkers.</param>
		/// <returns></returns>
		public List<ChartMarker> SaveChartChartMarkers(Chart entity, CrudStore<ChartMarker> ChartMarkers) {
			return ((ChartMarkerBrokerDB)m_brokerDB).SaveChartChartMarkers(entity, ChartMarkers);
		}

		/// <summary>
		/// Resolves the references for export.
		/// </summary>
		/// <param name="entity">The entity.</param>
		protected override void ResolveReferencesForExport(ChartMarker entity) {
			ResolveParentReferenceForExport<ChartAxis>(e => e.KeyChartAxis, entity);
		}

		/// <summary>
		/// Resolves the references during mapping.
		/// </summary>
		/// <param name="mappingRecord">The mapping record.</param>
		/// <param name="persistedEntity">The persisted entity.</param>
		protected override void ResolveReferencesForImport(MappingRecord<ChartMarker> mappingRecord, ChartMarker persistedEntity) {
			ResolveParentReferenceForImport<ChartAxis>(e => e.KeyChartAxis, mappingRecord.Entity, persistedEntity);
		}

	}

}
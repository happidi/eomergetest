﻿using System.Collections.Generic;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.DataLayer.BrokerDB;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for business entity ChartAlgorithm.
	/// </summary>
	public class ChartAlgorithmBroker : BaseBroker<ChartAlgorithm, ChartAlgorithmBrokerDB> {

		/// <summary>
		/// Get the list of saved  ChartAlgorithm from a Chart
		/// </summary>
		/// <param name="KeyChart">The key of the chart.</param>
		public List<ChartAlgorithm> GetListByKeyChart(string KeyChart) {
			return ((ChartAlgorithmBrokerDB)m_brokerDB).GetListByKeyChart(KeyChart);
		}

		/// <summary>
		/// Gets a store of  ChartAlgorithm for a specific chart.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyChart">The Key of the chart.</param>
		/// <returns>A JsonStore.</returns>
		public JsonStore<ChartAlgorithm> GetStoreByKeyChart(PagingParameter paging, string KeyChart) {
			int total;
			return ((ChartAlgorithmBrokerDB)m_brokerDB).GetAllPagingByKeyChart(paging, KeyChart, out total).ToJsonStore(total, paging);
		}

		/// <summary>
		/// Saves the chart ChartAlgorithms.
		/// </summary>
		/// <returns></returns>
		public List<ChartAlgorithm> SaveChartChartAlgorithm(Chart entity, CrudStore<ChartAlgorithm> algorithms) {
			return ((ChartAlgorithmBrokerDB)m_brokerDB).SaveChartChartAlgorithm(entity, algorithms);
		}

		/// <summary>
		/// Populate the chidren items specified as fields. "*" indicates that all fields should be populated.
		/// </summary>
		/// <param name="item">The item</param>
		/// <param name="loadMode">The load mode.</param>
		/// <param name="fields">The list of fields. "*" indicates all fields.</param>
		public override void PopulateList(ChartAlgorithm item, EntityLoadMode loadMode, params string[] fields) {
			foreach (string field in fields) {
				if (field.ContainsAny("*", "Algorithm")) {
					var brokerAlgorithm = Helper.CreateInstance<AlgorithmBroker>();
					item.Algorithm = brokerAlgorithm.GetItem(item.KeyAlgorithm, "*");
				}
				if (field.ContainsAny("*", "AlgorithmInputValues")) {
					var brokerScriptInputValue = Helper.CreateInstance<AlgorithmInputValueBroker>();
					item.AlgorithmInputValues = brokerScriptInputValue.GetListByKeyChart(item.KeyChart, item.KeyAlgorithm);
				}
			}
		}

		/// <summary>
		/// Copy an existing Chart.
		/// </summary>
		/// <param name="item">the item to be copied.</param>
		/// <param name="formResponse">The response of the operation.</param>
		/// <param name="Creator">The creator.</param>
		/// <param name="incrementTitle">if set to <c>true</c> [increment title].</param>
		/// <returns></returns>
		public override ChartAlgorithm Copy(ChartAlgorithm item, out FormResponse formResponse, string Creator = null, bool incrementTitle = true) {

			var brokerScriptInputValue = Helper.CreateInstance<AlgorithmInputValueBroker>();
			var values = brokerScriptInputValue.GetListByKeyChart(item.KeyChart, item.KeyAlgorithm);

			var newItem = base.Copy(item, out formResponse, Creator);
			if (formResponse.success) {
				if (values != null && values.Count > 0) {
					foreach (var value in values) {
						FormResponse response;
						value.KeyAlgorithm = newItem.KeyAlgorithm;
						brokerScriptInputValue.Copy(value, out response, Creator, incrementTitle);
					}
				}
			}
			return newItem;
		}
	}
}

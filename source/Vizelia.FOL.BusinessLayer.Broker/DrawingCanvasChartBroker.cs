﻿using System.Collections.Generic;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.DataLayer.BrokerDB;
using Vizelia.FOL.Common;
using Vizelia.FOL.BusinessEntities.Mapping;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for business entity DrawingCanvasChart.
	/// </summary>
	public class DrawingCanvasChartBroker : MappingEnabledBroker<DrawingCanvasChart, DrawingCanvasChartBrokerDB> {
		/// <summary>
		/// Populate the chidren items specified as fields. "*" indicates that all fields should be populated.
		/// </summary>
		/// <param name="item">The item</param>
		/// <param name="loadMode">The load mode.</param>
		/// <param name="fields">The list of fields. "*" indicates all fields.</param>
		public override void PopulateList(DrawingCanvasChart item, EntityLoadMode loadMode, params string[] fields) {
			foreach (string field in fields) {
				if (field.ContainsAny("*", "Chart")) {
					var brokerChart = Helper.CreateInstance<ChartBroker>();
					item.Chart = brokerChart.GetItem(item.KeyChart, fields);
				}
			}
		}

		/// <summary>
		/// Get the list of saved DrawingCanvas Chart from a KeyDrawingCanvas.
		/// </summary>
		/// <param name="KeyDrawingCanvas">The key of the  DrawingCanvas.</param>
		public List<DrawingCanvasChart> GetListByKeyDrawingCanvas(string KeyDrawingCanvas) {
			return ((DrawingCanvasChartBrokerDB)m_brokerDB).GetListByKeyDrawingCanvas(KeyDrawingCanvas);
		}

		/// <summary>
		/// Gets a store of DrawingCanvas Chart for a specific DrawingCanvas.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyDrawingCanvas">The Key of the DrawingCanvas.</param>
		/// <returns>A JsonStore.</returns>
		public JsonStore<DrawingCanvasChart> GetStoreByKeyDrawingCanvas(PagingParameter paging, string KeyDrawingCanvas) {
			int total;
			return ((DrawingCanvasChartBrokerDB)m_brokerDB).GetAllPagingByKeyDrawingCanvas(paging, KeyDrawingCanvas, out total).ToJsonStore(total, paging);
		}

		/// <summary>
		/// Saves the DrawingCanvas Charts.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="charts">The DrawingCanvasChart store.</param>
		/// <returns></returns>
		public List<DrawingCanvasChart> SaveDrawingCanvasDrawingCanvasChart(DrawingCanvas entity, CrudStore<DrawingCanvasChart> charts) {
			return ((DrawingCanvasChartBrokerDB)m_brokerDB).SaveDrawingCanvasDrawingCanvasChart(entity, charts);
		}

		/// <summary>
		/// Resolves the references during mapping.
		/// </summary>
		/// <param name="mappingRecord">The mapping record.</param>
		/// <param name="persistedEntity">The persisted entity.</param>
		protected override void ResolveReferencesForImport(MappingRecord<DrawingCanvasChart> mappingRecord, DrawingCanvasChart persistedEntity) {
			ResolveParentReferenceForImport<DrawingCanvas>(e => e.KeyDrawingCanvas, mappingRecord.Entity, persistedEntity);
			ResolveParentReferenceForImport<Chart>(e => e.KeyChart, mappingRecord.Entity, persistedEntity);
		}

		/// <summary>
		/// Resolves the references for export.
		/// </summary>
		/// <param name="entity">The entity.</param>
		protected override void ResolveReferencesForExport(DrawingCanvasChart entity) {
			ResolveParentReferenceForExport<DrawingCanvas>(e => e.KeyDrawingCanvas, entity);
			ResolveParentReferenceForExport<Chart>(e => e.KeyChart, entity);
		}
	}
}
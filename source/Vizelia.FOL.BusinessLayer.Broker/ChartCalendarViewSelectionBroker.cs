﻿using System.Collections.Generic;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.DataLayer.BrokerDB;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for business entity ChartCalendarViewSelection.
	/// </summary>
	public class ChartCalendarViewSelectionBroker : BaseBroker<ChartCalendarViewSelection, ChartCalendarViewSelectionBrokerDB> {

		/// <summary>
		/// Get the list of saved ChartCalendarViewSelection from a Chart
		/// </summary>
		/// <param name="KeyChart">The key of the chart.</param>
		public List<ChartCalendarViewSelection> GetListByKeyChart(string KeyChart) {
			return ((ChartCalendarViewSelectionBrokerDB)m_brokerDB).GetListByKeyChart(KeyChart);
		}

		/// <summary>
		/// Gets a store of ChartCalendarViewSelection for a specific chart.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyChart">The Key of the chart.</param>
		/// <returns>A JsonStore.</returns>
		public JsonStore<ChartCalendarViewSelection> GetStoreByKeyChart(PagingParameter paging, string KeyChart) {
			int total;
			return ((ChartCalendarViewSelectionBrokerDB)m_brokerDB).GetAllPagingByKeyChart(paging, KeyChart, out total).ToJsonStore(total, paging);
		}





	}

}
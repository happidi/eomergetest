﻿using System;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// A helper for Broker classes.
	/// </summary>
	public static class BrokerFactory {
		private const string const_broker_type_cache_key = "broker_type";

		/// <summary>
		/// Creates the broker for the given entity.
		/// </summary>
		/// <param name="entityType">Type of the entity.</param>
		/// <param name="useCached">if set to <c>true</c> [use cached].</param>
		/// <returns></returns>
		public static object CreateBroker(Type entityType, bool useCached = true) {
			object broker;
			if (useCached == false) {
				broker = CreateBrokerInternal(entityType);
			}
			else {
				broker = CacheService.MemoryCacheGetData(EncodeCacheKey(entityType));
				if (broker == null) {
					broker = CreateBrokerInternal(entityType);

					CacheService.MemoryCacheAdd(EncodeCacheKey(entityType), broker);
				}
			}
			return broker;
		}

		private static object CreateBrokerInternal(Type entityType) {
			Type brokerType;

			// Special handling for a MappingCrudStore since it is used with generic parameters. (This generic resolve can be done with Unity.)
			if (entityType.IsGenericType &&
				entityType.GetGenericTypeDefinition() == typeof(MappingCrudStore<,>)) {
				brokerType = typeof(MappingCrudStoreBroker<,>).MakeGenericType(entityType.GetGenericArguments());
			}
			else {
				var baseType = typeof(IBusinessEntityBroker);
				brokerType = baseType.Assembly.GetType(baseType.FullName.Replace(baseType.Name, entityType.Name + "Broker"), true);
			}

			//TODO: Policy injection doesn't work with this for some odd reason. Make this be wrapped by it.
			object broker = Activator.CreateInstance(brokerType);
			return broker;
		}

		private static string EncodeCacheKey(Type entityType) {
			return const_broker_type_cache_key + entityType.FullName;
		}

		/// <summary>
		/// Creates the broker for the given entity.
		/// </summary>
		public static IBroker<TEntity> CreateBroker<TEntity>() where TEntity : BaseBusinessEntity {
			object brokerObject = CreateBroker(typeof(TEntity));
			var broker = brokerObject as IBroker<TEntity>;

			if (broker == null) {
				throw new ArgumentException("Given TEntity has no corresponding IBroker<TEntity>.");
			}

			return broker;
		}
	}
}
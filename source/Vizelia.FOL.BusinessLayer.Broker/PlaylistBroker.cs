﻿using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.DataLayer.BrokerDB;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for business entity Playlist.
	/// </summary>
	public class PlaylistBroker : MappingEnabledBroker<Playlist, PlaylistBrokerDB> {
		/// <summary>
		/// Populate the chidren items specified as fields. "*" indicates that all fields should be populated.
		/// </summary>
		/// <param name="item">The item</param>
		/// <param name="loadMode">The load mode.</param>
		/// <param name="fields">The list of fields. "*" indicates all fields.</param>
		public override void PopulateList(Playlist item, EntityLoadMode loadMode, params string[] fields) {
			foreach (string field in fields) {
				if (field.ContainsAny("*", "Pages")) {
					var brokerPage = Helper.CreateInstance<PlaylistPageBroker>();
					var pages = brokerPage.GetListByKeyPlaylist(item.KeyPlaylist);
					brokerPage.ResolveForExport(pages, loadMode);
					item.Pages = pages;
				}
				if (field.ContainsAny("*", "VirtualFile")) {
					var brokerVirtualFile = Helper.CreateInstance<VirtualFileBroker>();
					item.VirtualFile = brokerVirtualFile.GetItemByKeyEntity(item.KeyPlaylist, item.GetType());
				}
			}
		}

		/// <summary>
		/// Resolves the references for export.
		/// </summary>
		/// <param name="entity">The entity.</param>
		protected override void ResolveReferencesForExport(Playlist entity) {
			ResolveParentReferenceForExport<FOLMembershipUser>(e => e.Creator, entity);
		}

		/// <summary>
		/// Resolves the references during mapping.
		/// </summary>
		/// <param name="mappingRecord">The mapping record.</param>
		/// <param name="persistedEntity">The persisted entity.</param>
		protected override void ResolveReferencesForImport(MappingRecord<Playlist> mappingRecord, Playlist persistedEntity) {
			// We do not resolve the localization culture property as it is supposed to be a two letter key such as "en" or "he" or "fr".
			ResolveParentReferenceForImport<FOLMembershipUser>(e => e.Creator, mappingRecord.Entity, persistedEntity);
		}

	}

}
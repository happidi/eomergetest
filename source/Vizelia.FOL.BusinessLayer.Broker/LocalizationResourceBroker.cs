﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Localization;
using Vizelia.FOL.DataLayer.BrokerDB;

using Vizelia.FOL.Infrastructure;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for business entity LocalizationResource.
	/// </summary>
	public class LocalizationResourceBroker : MappingEnabledBroker<LocalizationResource, LocalizationResourceBrokerDB> {
		/// <summary>
		/// Resolves the references for import.
		/// </summary>
		/// <param name="mappingRecord">The mapping record.</param>
		/// <param name="persistedEntity">The persisted entity.</param>
		protected override void ResolveReferencesForImport(MappingRecord<LocalizationResource> mappingRecord, LocalizationResource persistedEntity) {
			// Do nothing.
		}

		/// <summary>
		/// Resolves the references for export.
		/// </summary>
		/// <param name="entity">The entity.</param>
		protected override void ResolveReferencesForExport(LocalizationResource entity) {
			// Do nothing.
		}

		/// <summary>
		/// Gets all.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="location">The location.</param>
		/// <param name="total">The total.</param>
		/// <param name="useSecurableFilter">if set to <c>true</c> [use securable filter].</param>
		/// <param name="isHierarchyEnabled">if set to <c>true</c> [is hierarchy enabled].</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public override List<LocalizationResource> GetAll(PagingParameter paging, PagingLocation location, out int total, bool useSecurableFilter = true, bool isHierarchyEnabled = true, params string[] fields) {
			var allResources = base.GetAll(paging, location, out total, useSecurableFilter, isHierarchyEnabled, fields);
			var vizeliaResourceManager = Langue.ResourceManager as VizeliaResourceManager;
			if (vizeliaResourceManager != null) {
				var resxDefined = vizeliaResourceManager.GetResourceSet(CultureInfo.CurrentCulture, true, true);

				allResources.AddRange(from DictionaryEntry entry in resxDefined
									  select new LocalizationResource {
										  Id = "0",
										  Culture = CultureInfo.CurrentCulture.Name,
										  Key = entry.Key.ToString(),
										  Value = entry.Value.ToString(),
										  IsUserDefined = false
									  });
			}
			return allResources;
		}
		
		/// <summary>
		/// Gets the store.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="language">The language.</param>
		/// <returns></returns>	
		public JsonStore<LocalizationResource> GetStore(PagingParameter paging, string language) {
			if (String.IsNullOrEmpty(language))
				language = Helper.GetCurrentApplicableCulture();
			Dictionary<KeyValuePair<string, string>, string> messages = DatabaseResourceHelper.GetMessages(language);
			var query = from p in messages
						select new LocalizationResource { Key = p.Key.Key, Id = p.Key.Value, Value = p.Value, Culture = language };

			if (!String.IsNullOrEmpty(paging.query))
				query = query.Where(p => (p.Key != null && p.Key.ToLower().Contains(paging.query.ToLower())) || (p.Value != null && p.Value.ToLower().Contains(paging.query.ToLower())));
			return query.AsQueryable().Filter(paging).ToList().RankToJson(paging.sort, paging.dir, paging.start, paging.limit);
		}

		/// <summary>
		/// Saves the store.
		/// </summary>
		/// <param name="store">The store.</param>
		/// <param name="language">The language.</param>
		/// <returns></returns>
		public JsonStore<LocalizationResource> SaveStore(CrudStore<LocalizationResource> store, string language) {
			List<string> cultures = DatabaseResourceHelper.GetSupportedCultures();
			cultures.Remove(language);

			if (store.destroy != null) {
				foreach (var resource in store.destroy) {
					DatabaseResourceHelper.DeleteResource(resource.Id);
					ExternalCrudNotificationService.SendNotification(resource, CrudAction.Destroy);

					foreach (var culture in cultures) {
						var localizationResource = new LocalizationResource { Key = resource.Key, Culture = culture };
						ExternalCrudNotificationService.SendNotification(localizationResource, CrudAction.Destroy);
					}
				}
			}
			if (store.create != null) {
				// we first save the resource for the default language
				foreach (var resource in store.create) {
					DatabaseResourceHelper.AddResource(resource.Key, resource.Value, language);
				}
				// we then translate for each culture and save again
				foreach (string languageTo in cultures) {
					IEnumerable<LocalizationResource> translatedResources = TranslateResource(store.create, language,
																											 languageTo);
					foreach (LocalizationResource resource in translatedResources) {
						DatabaseResourceHelper.AddResource(resource.Key, resource.Value, languageTo);
					}
				}
			}

			if (store.update != null) {
				foreach (var resource in store.update) {
					DatabaseResourceHelper.UpdateResource(resource.Key, resource.Value, language);
				}
			}
			return this.GetStore(new PagingParameter { start = 0, limit = 100 }, language);
		}

		/// <summary>
		/// Translates resources.
		/// </summary>
		/// <param name="resources">The resources.</param>
		/// <param name="languageFrom">The language from.</param>
		/// <param name="languageTo">The language to.</param>
		/// <returns></returns>
		private IEnumerable<LocalizationResource> TranslateResource(List<LocalizationResource> resources, string languageFrom, string languageTo) {
			var query = from p in resources
						select p.Value;
			const string separator = "<x>";
			string xml = String.Join(separator, query.ToArray());

			//string xml = Helper.SerializeXml(query.ToList());
			try {
				xml = TranslationHelper.TranslateText(xml, languageFrom, languageTo);
			}
			catch { }

			List<string> translations = xml.Split(new string[] { separator }, StringSplitOptions.None).ToList();
			//List<string> translations = Helper.DeserializeXml<List<string>>(xml);

			int i = 0;
			var result = new List<LocalizationResource>();

			foreach (var resource in resources) {
				result.Add(new LocalizationResource { Id = resource.Id, Key = resource.Key, Value = translations[i].Trim() });
				i++;
			}
			return result;
		}
	}
}
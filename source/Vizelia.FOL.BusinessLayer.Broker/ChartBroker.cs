﻿using System.Collections.Generic;
using System.Linq;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using Vizelia.FOL.DataLayer.BrokerDB;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for business entity Chart.
	/// </summary>
	public class ChartBroker : MappingEnabledBroker<Chart, ChartBrokerDB> {
		/// <summary>
		/// Populate the chidren items specified as fields. "*" indicates that all fields should be populated.
		/// </summary>
		/// <param name="item">The item</param>
		/// <param name="loadMode">The load mode.</param>
		/// <param name="fields">The list of fields. "*" indicates all fields.</param>
		public override void PopulateList(Chart item, EntityLoadMode loadMode, params string[] fields) {
			foreach (string field in fields) {
				if (field.ContainsAny("*", "Series", "DataSeries")) {
					var brokerDataSerie = Helper.CreateInstance<DataSerieBroker>();
					item.Series = new DataSerieCollection();
					var list = brokerDataSerie.GetListByKeyChart(item.KeyChart);
					if (list != null && list.Count > 0) {
						brokerDataSerie.PopulateList(list, loadMode, fields);
						brokerDataSerie.ResolveForExport(list, loadMode);
						item.Series.AddRange(list);
						item.DataSeries = list;
					}
				}
				if (field.ContainsAny("*", "CalculatedSeries")) {
					var brokerCalculatedSerie = Helper.CreateInstance<CalculatedSerieBroker>();
					var list = brokerCalculatedSerie.GetListByKeyChart(item.KeyChart);
					if (list != null && list.Count > 0) {
						brokerCalculatedSerie.ResolveForExport(list, loadMode);
						item.CalculatedSeries = list;
					}
				}
				if (field.ContainsAny("*", "StatisticalSeries")) {
					var brokerStatisticalSerie = Helper.CreateInstance<StatisticalSerieBroker>();
					var list = brokerStatisticalSerie.GetListByKeyChart(item.KeyChart);
					if (list != null && list.Count > 0) {
						brokerStatisticalSerie.ResolveForExport(list, loadMode);
						item.StatisticalSeries = list;
					}
				}
				if (field.ContainsAny("*", "Axis", "ChartAxis")) {
					item.Axis = new Dictionary<string, ChartAxis>();
					var brokerAxis = Helper.CreateInstance<ChartAxisBroker>();
					var axisList = brokerAxis.GetListByKeyChart(item.KeyChart);
					if (axisList != null && axisList.Count > 0) {
						brokerAxis.PopulateList(axisList, loadMode, "*");
						brokerAxis.ResolveForExport(axisList, loadMode);
						foreach (var axis in axisList) {
							item.Axis.Add(axis.KeyChartAxis, axis);
						}
						item.ChartAxis = axisList;
					}
				}
				if (field.ContainsAny("*", "HistoricalAnalysisDefinitions")) {
					var brokerHistoricalAnalysis = Helper.CreateInstance<ChartHistoricalAnalysisBroker>();
					var historicalAnalysisList = brokerHistoricalAnalysis.GetListByKeyChart(item.KeyChart);
					brokerHistoricalAnalysis.PopulateList(historicalAnalysisList, loadMode, "*");
					brokerHistoricalAnalysis.ResolveForExport(historicalAnalysisList, loadMode);
					item.HistoricalAnalysisDefinitions = historicalAnalysisList;
				}

				// The data sources of the Chart
				if (field.ContainsAny("*", "Meters")) {
					var meterBroker = Helper.CreateInstance<MeterBroker>();
					var list = meterBroker.GetListByKeyChart(item.KeyChart);
					meterBroker.ResolveForExport(list, loadMode);
					item.Meters = list;
				}

				if (field.ContainsAny("*", "ResultingMeters")) {
					var meterBroker = Helper.CreateInstance<MeterBroker>();
					var list = meterBroker.GetCompleteListByKeyChart(item.KeyChart, false);
					meterBroker.ResolveForExport(list, loadMode);
					item.ResultingMeters = list;
				}

				if (field.ContainsAny("*", "FilterSpatial")) {
					var locationBroker = Helper.CreateInstance<LocationBroker>();
					List<Location> locations = locationBroker.GetFilterSpatialStoreByKeyChart(new PagingParameter(), item.KeyChart).records;
					locationBroker.PopulateList(locations, loadMode, "*");
					locationBroker.ResolveForExport(locations, loadMode);
					item.FilterSpatial = locations;
				}
				if (field.ContainsAny("*", "FilterSpatialPset")) {
					var filterSpatialPsetBroker = Helper.CreateInstance<FilterSpatialPsetBroker>();
					List<FilterSpatialPset> psets = filterSpatialPsetBroker.GetStoreByKeyChart(new PagingParameter(), item.KeyChart).records;
					filterSpatialPsetBroker.PopulateList(psets, loadMode, "*");
					filterSpatialPsetBroker.ResolveForExport(psets, loadMode);
					item.FilterSpatialPset = psets;
				}
				if (field.ContainsAny("*", "MetersClassifications")) {
					var classificationItemBroker = Helper.CreateInstance<ClassificationItemBroker>();
					List<ClassificationItem> metersClassifications = classificationItemBroker.GetFilterMeterClassificationStoreByKeyChart(new PagingParameter(), item.KeyChart).records;
					classificationItemBroker.PopulateList(metersClassifications, loadMode, "*");
					classificationItemBroker.ResolveForExport(metersClassifications, loadMode);
					item.MetersClassifications = metersClassifications;
				}
				if (field.ContainsAny("*", "EventLogClassifications")) {
					var classificationItemBroker = Helper.CreateInstance<ClassificationItemBroker>();
					List<ClassificationItem> eventLogClassifications = classificationItemBroker.GetFilterEventLogClassificationStoreByKeyChart(new PagingParameter(), item.KeyChart).records;
					classificationItemBroker.PopulateList(eventLogClassifications, loadMode, "*");
					classificationItemBroker.ResolveForExport(eventLogClassifications, loadMode);
					item.EventLogClassifications = eventLogClassifications;
				}
				if (field.ContainsAny("*", "HistoricalPsets")) {
					var brokerHistoricalPsets = Helper.CreateInstance<ChartPsetAttributeHistoricalBroker>();
					List<ChartPsetAttributeHistorical> historicals = brokerHistoricalPsets.GetListByKeyChart(item.KeyChart);
					brokerHistoricalPsets.PopulateList(historicals, loadMode, "*");
					brokerHistoricalPsets.ResolveForExport(historicals, loadMode);
					item.HistoricalPsets = historicals;
				}
				// End data sources.

				if (field.ContainsAny("*", "EventLogs")) {
					var brokerEventLog = Helper.CreateInstance<EventLogBroker>();
					var list = brokerEventLog.GetListByKeyChart(item.KeyChart);
					if (list != null && list.Count > 0) {
						brokerEventLog.ResolveForExport(list, loadMode);
						item.EventLogs = list;
					}
				}

				if (field.ContainsAny("*", "AlarmInstances")) {
					var brokerAlarmInstance = Helper.CreateInstance<AlarmInstanceBroker>();
					var list = brokerAlarmInstance.GetListAsDataSourceByKeyChart(item.KeyChart, item.StartDate, item.EndDate, item.TimeInterval, item.GetTimeZone());
					item.AlarmInstances = list;
				}

				if (field.ContainsAny("*", "DynamicDisplay") && !string.IsNullOrEmpty(item.KeyDynamicDisplay)) {
					var brokerDynamicDisplay = Helper.CreateInstance<DynamicDisplayBroker>();
					DynamicDisplay dynamicDisplay = brokerDynamicDisplay.GetItem(item.KeyDynamicDisplay);
					if (dynamicDisplay != null) {
						brokerDynamicDisplay.PopulateList(dynamicDisplay, loadMode, fields);
						brokerDynamicDisplay.ResolveForExport(dynamicDisplay, loadMode);
						item.DynamicDisplay = dynamicDisplay;
					}
				}
				if (field.ContainsAny("*", "VirtualFile")) {
					var brokerVirtualFile = Helper.CreateInstance<VirtualFileBroker>();
					VirtualFile virtualFile = brokerVirtualFile.GetItemByKeyEntity(item.KeyChart, item.GetType());
					if (virtualFile != null) {
						brokerVirtualFile.ResolveForExport(virtualFile, loadMode);
						item.VirtualFile = virtualFile;
					}
				}

				if (field.ContainsAny("*", "Palette") && !string.IsNullOrEmpty(item.KeyPalette)) {
					var brokerPalette = Helper.CreateInstance<PaletteBroker>();
					Palette palette = brokerPalette.GetItem(item.KeyPalette, "*");
					if (palette != null) {
						brokerPalette.ResolveForExport(palette, loadMode);
						item.Palette = palette;
					}
				}
				if (item.Palette == null) {
					item.Palette = PaletteHelper.GeneratePalette();
				}

				if (field.ContainsAny("*", "CalendarEventCategory") && !string.IsNullOrEmpty(item.KeyCalendarEventCategory)) {
					var brokerCalendarEventCategory = Helper.CreateInstance<CalendarEventCategoryBroker>();
					CalendarEventCategory eventCategory = brokerCalendarEventCategory.GetItem(item.KeyCalendarEventCategory);
					if (eventCategory != null) {
						brokerCalendarEventCategory.PopulateList(eventCategory, loadMode, fields);
						brokerCalendarEventCategory.ResolveForExport(eventCategory, loadMode);
						item.CalendarEventCategory = eventCategory;
					}
				}
				if (field.ContainsAny("*", "CorrelationRemovedPoints")) {
					var brokerCorrelationRemovedPoint = Helper.CreateInstance<ChartCorrelationRemovedDataPointBroker>();
					item.CorrelationRemovedPoints = brokerCorrelationRemovedPoint.GetListByKeyChart(item.KeyChart);
				}
				if (field.ContainsAny("*", "EnergyCertificate") && !string.IsNullOrEmpty(item.KeyEnergyCertificate)) {
					var brokerEnergyCertificate = Helper.CreateInstance<EnergyCertificateBroker>();
					var energyCertificate = brokerEnergyCertificate.GetItem(item.KeyEnergyCertificate);
					if (energyCertificate != null) {
						brokerEnergyCertificate.PopulateList(energyCertificate, loadMode, field);
						item.EnergyCertificate = energyCertificate;
					}
				}
				var brokerImage = Helper.CreateInstance<DynamicDisplayImageBroker>();
				if (field.ContainsAny("*", "ChartAreaPictureImage")) {
					if (string.IsNullOrEmpty(item.ChartAreaPicture) == false) {
						DynamicDisplayImage image = brokerImage.GetItem(item.ChartAreaPicture);
						if (image != null) {
							brokerImage.PopulateList(image, loadMode, fields);
							brokerImage.ResolveForExport(image, loadMode);
							item.ChartAreaPictureImage = image;
						}
					}
				}

				if (field.ContainsAny("*", "AlarmDefinitions")) {
					var brokerAlarmDefinition = Helper.CreateInstance<AlarmDefinitionBroker>();
					var list = brokerAlarmDefinition.GetListByKeyChart(item.KeyChart);
					if (list != null && list.Count > 0) {
						brokerAlarmDefinition.ResolveForExport(list, loadMode);
						item.AlarmDefinitions = list;
					}
				}

				if (field.ContainsAny("*", "AlarmDefinitionsClassifications")) {
					int total;
					var brokerClassificationItem = Helper.CreateInstance<ClassificationItemBroker>();
					var list = brokerClassificationItem.GetFilterAlarmDefinitionClassificationListByKeyChart(new PagingParameter(),
						item.KeyChart, out total);
					if (list != null && list.Count > 0) {
						brokerClassificationItem.ResolveForExport(list, loadMode);
						item.AlarmDefinitionsClassifications = list;
					}
				}

				if (field.ContainsAny("*", "DynamicDisplayHeaderPictureImage")) {
					if (string.IsNullOrEmpty(item.DynamicDisplayHeaderPicture) == false) {
						DynamicDisplayImage image = brokerImage.GetItem(item.DynamicDisplayHeaderPicture);
						if (image != null) {
							brokerImage.PopulateList(image, loadMode, fields);
							brokerImage.ResolveForExport(image, loadMode);
							item.DynamicDisplayHeaderPictureImage = image;
						}
					}
				}

				if (field.ContainsAny("*", "DynamicDisplayMainPictureImage")) {
					if (string.IsNullOrEmpty(item.DynamicDisplayMainPicture) == false) {
						DynamicDisplayImage image = brokerImage.GetItem(item.DynamicDisplayMainPicture);
						if (image != null) {
							brokerImage.PopulateList(image, loadMode, fields);
							brokerImage.ResolveForExport(image, loadMode);
							item.DynamicDisplayMainPictureImage = image;
						}
					}
				}

				if (field.ContainsAny("*", "FooterPictureImage")) {
					if (string.IsNullOrEmpty(item.DynamicDisplayFooterPicture) == false) {
						DynamicDisplayImage image = brokerImage.GetItem(item.DynamicDisplayFooterPicture);
						if (image != null) {
							brokerImage.PopulateList(image, loadMode, fields);
							brokerImage.ResolveForExport(image, loadMode);
							item.DynamicDisplayFooterPictureImage = image;
						}
					}
				}

				if (field.ContainsAny("*", "DrillDown")) {
					var brokerChartDrillDown = Helper.CreateInstance<ChartDrillDownBroker>();
					item.DrillDown = brokerChartDrillDown.GetItemByKeyChart(item.KeyChart);
				}

				if (field.ContainsAny("*", "CalendarViewSelections")) {
					var brokerChartCalendarViewSelection = Helper.CreateInstance<ChartCalendarViewSelectionBroker>();
					item.CalendarViewSelections = brokerChartCalendarViewSelection.GetListByKeyChart(item.KeyChart);
				}

				if (field.ContainsAny("*", "Algorithms")) {
					var brokerChartAlgorithm = Helper.CreateInstance<ChartAlgorithmBroker>();
					var algorithms = brokerChartAlgorithm.GetListByKeyChart(item.KeyChart);
					brokerChartAlgorithm.PopulateList(algorithms, loadMode, "*");
					item.Algorithms = algorithms;
				}
			}
		}


		/// <summary>
		/// Copy an existing Chart.
		/// </summary>
		/// <param name="item">the item to be copied.</param>
		/// <param name="formResponse">The response of the operation.</param>
		/// <param name="Creator">The creator.</param>
		/// <param name="incrementTitle">if set to <c>true</c> [increment title].</param>
		/// <returns></returns>
		public override Chart Copy(Chart item, out FormResponse formResponse, string Creator = null, bool incrementTitle = true) {

			//we need to get the current Spatial filter before erasing the Key.
			var keyChart = item.KeyChart;
			var brokerLocation = Helper.CreateInstance<LocationBroker>();
			var locations = brokerLocation.GetFilterSpatialStoreByKeyChart(new PagingParameter { start = 0, limit = 0 }, keyChart).records;

			var filterLocation = FilterHelper.GetFilter<Location>();
			if (filterLocation.Count > 0) {
				locations = (from p in locations
							 join q in filterLocation
							 on p.KeyLocation equals q.Key
							 where (q.Type == FilterType.Descendant || q.Type == FilterType.Direct)
							 select p).ToList();
			}
			var brokerFilterPset = Helper.CreateInstance<FilterSpatialPsetBroker>();
			var filterpsets = brokerFilterPset.GetListByKeyChart(keyChart);

			var brokerClassification = Helper.CreateInstance<ClassificationItemBroker>();
			var classifications = brokerClassification.GetFilterMeterClassificationStoreByKeyChart(new PagingParameter { start = 0, limit = 0 }, keyChart).records;
			var classificationsEventLog = brokerClassification.GetFilterEventLogClassificationStoreByKeyChart(new PagingParameter { start = 0, limit = 0 }, keyChart).records;
			var classificationAlarmDefinition = brokerClassification.GetFilterAlarmDefinitionClassificationStoreByKeyChart(new PagingParameter { start = 0, limit = 0 }, keyChart).records;

			var brokerHistoricalAnalysis = Helper.CreateInstance<ChartHistoricalAnalysisBroker>();
			var historicalanalysis = brokerHistoricalAnalysis.GetListByKeyChart(keyChart);

			var chartAlgorithmBroker = Helper.CreateInstance<ChartAlgorithmBroker>();
			var algorithms = chartAlgorithmBroker.GetListByKeyChart(keyChart);

			var brokerCustomCell = Helper.CreateInstance<ChartCustomGridCellBroker>();
			var cells = brokerCustomCell.GetListByKeyChart(keyChart);

			var brokerMeter = Helper.CreateInstance<MeterBroker>();
			var meters = brokerMeter.GetListByKeyChart(keyChart);

			var newItem = base.Copy(item, out formResponse, Creator, incrementTitle);

			var brokerDataSerie = Helper.CreateInstance<DataSerieBroker>();
			var brokerAxis = Helper.CreateInstance<ChartAxisBroker>();
			var brokerCalculatedSerie = Helper.CreateInstance<CalculatedSerieBroker>();
			var brokerStatisticalSerie = Helper.CreateInstance<StatisticalSerieBroker>();
			var brokerPsetAttributeHistorical = Helper.CreateInstance<ChartPsetAttributeHistoricalBroker>();

			if (locations != null && locations.Count > 0) {
				var crudLocations = new CrudStore<Location> { create = locations };
				brokerLocation.SaveChartFilterSpatial(newItem, crudLocations);
			}
			if (classifications != null && classifications.Count > 0) {
				var crudClassifications = new CrudStore<ClassificationItem> { create = classifications };
				brokerClassification.SaveChartFilterMeterClassification(newItem, crudClassifications);
			}
			if (classificationsEventLog != null && classificationsEventLog.Count > 0) {
				var crudClassificationsEventLog = new CrudStore<ClassificationItem> { create = classificationsEventLog };
				brokerClassification.SaveChartFilterEventLogClassification(newItem, crudClassificationsEventLog);
			}
			if (meters != null && meters.Count > 0) {
				var crudMeter = new CrudStore<Meter> { create = meters };
				brokerMeter.SaveChartMeters(newItem, crudMeter);
			}
			if (classificationAlarmDefinition != null && classificationAlarmDefinition.Count > 0) {
				var crudAlarmDef = new CrudStore<ClassificationItem> { create = classificationAlarmDefinition };
				brokerClassification.SaveChartFilterAlarmDefinitionClassification(newItem, crudAlarmDef);
			}

			if (filterpsets != null && filterpsets.Count > 0) {
				filterpsets.ForEach(filterpset => {
					filterpset.LocalId = null;
					filterpset.KeyFilterSpatialPset = null;
					filterpset.KeyChart = newItem.KeyChart;
					brokerFilterPset.Save(filterpset);
				});
			}
			if (historicalanalysis != null && historicalanalysis.Count > 0) {
				historicalanalysis.ForEach(historical => {
					historical.KeyChart = newItem.KeyChart;
					FormResponse response;
					brokerHistoricalAnalysis.Copy(historical, out response, Creator);
				});
			}
			if (algorithms != null && algorithms.Count > 0) {
				var brokerScriptInputValue = Helper.CreateInstance<AlgorithmInputValueBroker>();

				foreach (ChartAlgorithm chartAlgorithm in algorithms) {
					List<AlgorithmInputValue> inputValues = brokerScriptInputValue.GetListByKeyChart(keyChart, chartAlgorithm.KeyAlgorithm);
					chartAlgorithm.KeyChart = newItem.KeyChart;
					chartAlgorithm.KeyChartAlgorithm = null;
					//we cant use the copy because the KeyChart has already been changed.
					ChartAlgorithm newChartAlgorithm = chartAlgorithmBroker.Save(chartAlgorithm);

					if (inputValues != null) {
						foreach (var inputValue in inputValues) {
							inputValue.KeyAlgorithm = newChartAlgorithm.KeyAlgorithm;
							inputValue.KeyChart = newItem.KeyChart;
							inputValue.KeyAlgorithmInputValue = null;
							brokerScriptInputValue.Save(inputValue);
						}
					}
				}
			}

			if (cells != null && cells.Count > 0) {
				cells.ForEach(cell => {
					cell.KeyChartCustomGridCell = null;
					cell.KeyChart = newItem.KeyChart;
					brokerCustomCell.Save(cell);
				});
			}

			if (item.Axis != null && item.Axis.Count > 0) {
				item.Axis.Values.ToList().ForEach(axis => {
					var oldKey = axis.KeyChartAxis;
					axis.KeyChart = newItem.KeyChart;
					FormResponse response;
					var newAxis = brokerAxis.Copy(axis, out response, Creator);
					item.Series.ToList().ForEach(serie => {
						if (serie.KeyYAxis == oldKey) {
							serie.KeyYAxis = newAxis.KeyChartAxis;
						}
					});

				});
			}

			if (item.Series != null && item.Series.Count > 0) {
				item.Series.ToList().ForEach(serie => {
					serie.KeyChart = newItem.KeyChart;
					FormResponse response;
					brokerDataSerie.Copy(serie, out response, Creator);
				});
			}
			if (item.HistoricalPsets != null && item.HistoricalPsets.Count > 0) {
				item.HistoricalPsets.ForEach(historicalpset => {
					historicalpset.LocalId = null;
					historicalpset.KeyChart = newItem.KeyChart;
					historicalpset.KeyChartPsetAttributeHistorical = null;
					brokerPsetAttributeHistorical.Save(historicalpset);
				});
			}
			if (item.CalculatedSeries != null && item.CalculatedSeries.Count > 0) {
				item.CalculatedSeries.ForEach(serie => {
					//serie.LocalId = null;
					serie.KeyChart = newItem.KeyChart;
					FormResponse response;
					brokerCalculatedSerie.Copy(serie, out response, Creator);
				});
			}
			if (item.StatisticalSeries != null && item.StatisticalSeries.Count > 0) {
				item.StatisticalSeries.ForEach(serie => {
					//serie.LocalId = null;
					serie.KeyChart = newItem.KeyChart;
					FormResponse response;
					brokerStatisticalSerie.Copy(serie, out response, Creator);
				});
			}


			return newItem;
		}

		/// <summary>
		/// Clears the data source of an existing Chart (Meter, Spatial Filter, Meter Classification).
		/// </summary>
		/// <param name="item">The item.</param>
		/// <param name="deleteMeter">if set to <c>true</c> [delete meter].</param>
		/// <param name="deleterSpatialFilter">if set to <c>true</c> [deleter spatial filter].</param>
		/// <param name="deleteMeterClassification">if set to <c>true</c> [delete meter classification].</param>
		/// <returns></returns>
		public Chart ClearDataSource(Chart item, bool deleteMeter, bool deleterSpatialFilter, bool deleteMeterClassification) {
			var brokerMeter = Helper.CreateInstance<MeterBroker>();
			var brokerLocation = Helper.CreateInstance<LocationBroker>();
			var brokerClassification = Helper.CreateInstance<ClassificationItemBroker>();

			var locations = brokerLocation.GetFilterSpatialStoreByKeyChart(new PagingParameter { start = 0, limit = 0 }, item.KeyChart).records;
			var classifications = brokerClassification.GetFilterMeterClassificationStoreByKeyChart(new PagingParameter { start = 0, limit = 0 }, item.KeyChart).records;
			var meters = brokerMeter.GetListByKeyChart(item.KeyChart);


			if (deleterSpatialFilter) {
				if (locations != null && locations.Count > 0) {
					var crudLocations = new CrudStore<Location> { destroy = locations };
					brokerLocation.SaveChartFilterSpatial(item, crudLocations);
				}
			}

			if (deleteMeter) {
				if (meters != null && meters.Count > 0) {
					var crudMeter = new CrudStore<Meter> { destroy = meters };
					brokerMeter.SaveChartMeters(item, crudMeter);
				}
			}

			if (deleteMeterClassification) {
				if (classifications != null && classifications.Count > 0) {
					var crudClassifications = new CrudStore<ClassificationItem> { destroy = classifications };
					brokerClassification.SaveChartFilterMeterClassification(item, crudClassifications);
				}
			}

			//item.Meters = new List<Meter>();
			return item;
		}

		/// <summary>
		/// Saves the chartscheduler charts.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="charts">The charts.</param>
		/// <returns></returns>
		public List<Chart> SaveChartChartScheduler(ChartScheduler entity, CrudStore<Chart> charts) {
			return ((ChartBrokerDB)m_brokerDB).SaveChartChartScheduler(entity, charts);
		}


		/// <summary>
		/// Gets a list of Charts for a specific ChartScheduler.
		/// </summary>
		/// <param name="KeyChartScheduler">The Key ChartScheduler.</param>
		/// <returns></returns>
		public virtual List<Chart> GetListByKeyChartScheduler(string KeyChartScheduler) {
			return ((ChartBrokerDB)m_brokerDB).GetListByKeyChartScheduler(KeyChartScheduler);
		}


		/// <summary>
		/// Gets a store of charts for a specific chartscheduler.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyChartScheduler">The Key of the chartscheduler.</param>
		/// <returns>A JsonStore.</returns>
		public JsonStore<Chart> GetStoreByKeyChartScheduler(PagingParameter paging, string KeyChartScheduler) {
			int total;
			return ((ChartBrokerDB)m_brokerDB).GetAllPagingByKeyChartScheduler(paging, KeyChartScheduler, out total).ToJsonStore(total, paging);
		}

		/// <summary>
		/// Saves the Alarm definition charts.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="charts">The charts.</param>
		/// <returns></returns>
		public List<Chart> SaveChartAlarmDefinition(AlarmDefinition entity, CrudStore<Chart> charts) {
			return ((ChartBrokerDB)m_brokerDB).SaveChartAlarmDefinition(entity, charts);
		}

		/// <summary>
		/// Gets a list of Charts for a specific AlarmDefinition.
		/// </summary>
		/// <param name="KeyAlarmDefinition">The key alarm definition.</param>
		/// <returns></returns>
		public virtual List<Chart> GetListByKeyAlarmDefinition(string KeyAlarmDefinition) {
			return ((ChartBrokerDB)m_brokerDB).GetListByKeyAlarmDefinition(KeyAlarmDefinition);
		}


		/// <summary>
		/// Gets a store of charts for a specific AlarmDefinition.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyAlarmDefinition">The Key of the AlarmDefinition.</param>
		/// <returns>A JsonStore.</returns>
		public JsonStore<Chart> GetStoreByKeyAlarmDefinition(PagingParameter paging, string KeyAlarmDefinition) {
			int total;
			return ((ChartBrokerDB)m_brokerDB).GetAllPagingByKeyAlarmDefinition(paging, KeyAlarmDefinition, out total).ToJsonStore(total, paging);
		}

		/// <summary>
		/// Resolves the references during mapping.
		/// </summary>
		/// <param name="mappingRecord">The mapping record.</param>
		/// <param name="persistedEntity">The persisted entity.</param>
		protected override void ResolveReferencesForImport(MappingRecord<Chart> mappingRecord, Chart persistedEntity) {
			ResolveParentReferenceForImport<CalendarEventCategory>(e => e.KeyCalendarEventCategory, mappingRecord.Entity, persistedEntity);
			ResolveParentReferenceForImport<DynamicDisplay>(e => e.KeyDynamicDisplay, mappingRecord.Entity, persistedEntity);
			ResolveParentReferenceForImport<Palette>(e => e.KeyPalette, mappingRecord.Entity, persistedEntity);
			ResolveParentReferenceForImport<FOLMembershipUser>(e => e.Creator, mappingRecord.Entity, persistedEntity);
			ResolveParentReferenceForImport<ClassificationItem>(e => e.KeyClassificationItemKPI, mappingRecord.Entity, persistedEntity);
		}

		/// <summary>
		/// Prepares to export entity.
		/// </summary>
		/// <param name="entity">The entity.</param>
		protected override void ResolveReferencesForExport(Chart entity) {
			ResolveParentReferenceForExport<CalendarEventCategory>(e => e.KeyCalendarEventCategory, entity);
			ResolveParentReferenceForExport<DynamicDisplay>(e => e.KeyDynamicDisplay, entity);
			ResolveParentReferenceForExport<Palette>(e => e.KeyPalette, entity);
			ResolveParentReferenceForExport<FOLMembershipUser>(e => e.Creator, entity);
			ResolveParentReferenceForExport<ClassificationItem>(e => e.KeyClassificationItemKPI, entity);
		}

		/// <summary>
		/// Gets all the chart's keys that are using the specified dynamic display.
		/// </summary>
		/// <param name="KeyDynamicDisplay">The key dynamic display.</param>
		/// <returns></returns>
		public List<string> GetKeysByKeyDynamicDisplay(string KeyDynamicDisplay) {
			return ((ChartBrokerDB)m_brokerDB).GetKeysByKeyDynamicDisplay(KeyDynamicDisplay);
		}


		/// <summary>
		/// Gets the store by key classification item KPI.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="KeyClassificationItem">The key classification item.</param>
		/// <param name="roles">The roles.</param>
		/// <returns></returns>
		public JsonStore<Chart> GetStoreByKeyClassificationItemKPI(PagingParameter paging, string KeyClassificationItem, List<AuthorizationItem> roles) {
			int total;
			return ((ChartBrokerDB)m_brokerDB).GetAllPagingByKeyClassificationItemKPI(paging, KeyClassificationItem, roles, out total).ToJsonStore(total, paging);
		}

		/// <summary>
		/// Returns whether the Audit the should return changed fields even if not should be audited.
		/// </summary>
		/// <returns></returns>
		protected override bool ShouldReturnAuditChangedFields() {
			return true;
		}
	}
}

﻿using System;
using System.Collections.Generic;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The interface for mapping.
	/// It is used as a polymorphic interface during the mapping of an entity. It would be otherwise cumbersome to get the correct broker Mapping function.
	/// </summary>
	public interface IMappingBroker {
		/// <summary>
		/// Executes the mapping for the specified entities.
		/// </summary>
		/// <param name="mappingRecords">The mapping records.</param>
		/// <param name="action">The action.</param>
		/// <param name="formResponses">The form responses.</param>
		/// <param name="mappingContext">The mapping context</param>
		void Mapping(IEnumerable<IMappingRecord> mappingRecords,
					 string action,
					 MappingContext mappingContext,
				     out IEnumerable<Tuple<string,FormResponse>> formResponses);

		/// <summary>
		/// Gets all items fully populated and resolved for export.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="total">The total of results.</param>
		/// <returns>A list of items</returns>
		List<BaseBusinessEntity> GetAllForExport(PagingParameter paging, PagingLocation location, out int total);

		/// <summary>
		/// Resolves the list for export.
		/// </summary>
		/// <param name="list">The list.</param>
		/// <param name="loadMode">The load mode.</param>
		/// <returns></returns>
		void ResolveForExport(List<BaseBusinessEntity> list, EntityLoadMode loadMode);

		/// <summary>
		/// Resolves the entity for export.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="loadMode">The load mode.</param>
		/// <returns></returns>
		void ResolveForExport(BaseBusinessEntity entity, EntityLoadMode loadMode);

		/// <summary>
		/// Gets the maximum size of the batch.
		/// </summary>
		/// <value>
		/// The maximum size of the batch.
		/// </value>
		int MaximumBatchSize { get; }

		/// <summary>
		/// Gets a value indicating whether this broker writes failures to the trace or not.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if this broker writes failures to the trace; otherwise, <c>false</c>.
		/// </value>
		bool WritesFailuresToTrace { get; }
	}

}

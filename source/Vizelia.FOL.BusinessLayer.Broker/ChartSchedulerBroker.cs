﻿using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.DataLayer.BrokerDB;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for business entity ChartScheduler.
	/// </summary>
	public class ChartSchedulerBroker : MappingEnabledBroker<ChartScheduler, ChartSchedulerBrokerDB> {
		/// <summary>
		/// Populate the chidren items specified as fields. "*" indicates that all fields should be populated.
		/// </summary>
		/// <param name="item">The item</param>
		/// <param name="loadMode">The load mode.</param>
		/// <param name="fields">The list of fields. "*" indicates all fields.</param>
		public override void PopulateList(ChartScheduler item, EntityLoadMode loadMode, params string[] fields) {
			foreach (string field in fields) {
				if (field.ContainsAny("*", "PortalWindows")) {
					PortalWindowBroker brokerPortalWindow = Helper.CreateInstance<PortalWindowBroker>();
					var portalWindows = brokerPortalWindow.GetListByKeyChartScheduler(item.KeyChartScheduler);
					if (portalWindows != null && portalWindows.Count > 0) {
						brokerPortalWindow.ResolveForExport(portalWindows, loadMode);
						item.PortalWindows = portalWindows;
					}
				}
				if (field.ContainsAny("*", "Charts")) {
					ChartBroker brokerChart = Helper.CreateInstance<ChartBroker>();
					var charts = brokerChart.GetListByKeyChartScheduler(item.KeyChartScheduler);
					if (charts != null && charts.Count > 0) {
						brokerChart.ResolveForExport(charts, loadMode);
						item.Charts = charts;
					}
				}

			}
		}


		/// <summary>
		/// Resolves the references for export.
		/// </summary>
		/// <param name="entity">The entity.</param>
		protected override void ResolveReferencesForExport(ChartScheduler entity) {
			// Do nothing.
		}

		/// <summary>
		/// Resolves the references during mapping.
		/// </summary>
		/// <param name="mappingRecord">The mapping record.</param>
		/// <param name="persistedEntity">The persisted entity.</param>
		protected override void ResolveReferencesForImport(MappingRecord<ChartScheduler> mappingRecord, ChartScheduler persistedEntity) {
			// Do nothing.
		}
	}
}

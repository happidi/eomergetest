﻿using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using Vizelia.FOL.DataLayer.BrokerDB;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for business entity AlarmTable.
	/// </summary>
	public class AlarmTableBroker : MappingEnabledBroker<AlarmTable, AlarmTableBrokerDB> {
		/// <summary>
		/// Copy an existing Chart.
		/// </summary>
		/// <param name="item">the item to be copied.</param>
		/// <param name="formResponse">The response of the operation.</param>
		/// <param name="Creator">The creator.</param>
		/// <param name="incrementTitle">if set to <c>true</c> [increment title].</param>
		/// <returns></returns>
		public override AlarmTable Copy(AlarmTable item, out FormResponse formResponse, string Creator = null, bool incrementTitle = true) {
			//we need to get the current Spatial filter before erasing the Key.
			var brokerLocation = Helper.CreateInstance<LocationBroker>();
			var locations = brokerLocation.GetFilterSpatialStoreByKeyAlarmTable(new PagingParameter { start = 0, limit = 0 }, item.KeyAlarmTable).records;

			var brokerClassification = Helper.CreateInstance<ClassificationItemBroker>();
			var classifications = brokerClassification.GetFilterAlarmDefinitionClassificationStoreByKeyAlarmTable(new PagingParameter { start = 0, limit = 0 }, item.KeyAlarmTable).records;

			//if (incrementTitle)
			//	item.Title = ChartHelper.Increment(item.Title);
			var newItem = base.Copy(item, out formResponse, Creator, incrementTitle);


			var brokerAlarmDefinition = Helper.CreateInstance<AlarmDefinitionBroker>();
			if (locations != null && locations.Count > 0) {
				var crudLocations = new CrudStore<Location> { create = locations };
				brokerLocation.SaveAlarmTableFilterSpatial(newItem, crudLocations);
			}
			if (classifications != null && classifications.Count > 0) {
				var crudClassifications = new CrudStore<ClassificationItem> { create = classifications };
				brokerClassification.SaveAlarmTableFilterAlarmDefinitionClassification(newItem, crudClassifications);
			}

			if (item.AlarmDefinitions != null && item.AlarmDefinitions.Count > 0) {
				var crudAlarmDefinitions = new CrudStore<AlarmDefinition> { create = item.AlarmDefinitions };
				brokerAlarmDefinition.SaveAlarmTableAlarmDefinitions(newItem, crudAlarmDefinitions);
			}
			return newItem;
		}

		/// <summary>
		/// Resolves the references for import.
		/// </summary>
		/// <param name="mappingRecord">The mapping record.</param>
		/// <param name="persistedEntity">The persisted entity.</param>
		protected override void ResolveReferencesForImport(MappingRecord<AlarmTable> mappingRecord, AlarmTable persistedEntity) {
			//Nothing to do here.
		}

		/// <summary>
		/// Resolves the references for export.
		/// </summary>
		/// <param name="entity">The entity.</param>
		protected override void ResolveReferencesForExport(AlarmTable entity) {
			//Nothing to do here.
		}
	}

}
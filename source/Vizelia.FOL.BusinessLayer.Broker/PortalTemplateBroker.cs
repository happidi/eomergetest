﻿using System.Collections.Generic;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.DataLayer.BrokerDB;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for business entity PortalTemplate.
	/// </summary>
	public class PortalTemplateBroker : MappingEnabledBroker<PortalTemplate, PortalTemplateBrokerDB> {
		/// <summary>
		/// Populate the chidren items specified as fields. "*" indicates that all fields should be populated.
		/// </summary>
		/// <param name="item">The item</param>
		/// <param name="loadMode"> </param>
		/// <param name="fields">The list of fields. "*" indicates all fields.</param>
		public override void PopulateList(PortalTemplate item, EntityLoadMode loadMode, params string[] fields) {
			foreach (string field in fields) {
				if (field.ContainsAny("*", "PortalWindows")) {
					PortalWindowBroker brokerPortalWindow = Helper.CreateInstance<PortalWindowBroker>();
					var windows = brokerPortalWindow.GetListByKeyPortalTemplate(item.KeyPortalTemplate);
					brokerPortalWindow.ResolveForExport(windows, loadMode);
					item.PortalWindows = windows;
				}
				if (field.ContainsAny("*", "Users")) {
					FOLMembershipUserBroker brokerUser = Helper.CreateInstance<FOLMembershipUserBroker>();
					item.Users = brokerUser.GetListByKeyPortalTemplate(item.KeyPortalTemplate);
				}
			}
		}

        /// <summary>
        /// Gets a list of new user keys by key portal template.
        /// </summary>
        /// <param name="keyPortalTemplate">The keyPortalTemplate.</param>
        /// <returns></returns>
        public List<string> FOLMembershipUser_GetNewUserKeysByKeyPortalTemplate(int keyPortalTemplate) {
            return ((PortalTemplateBrokerDB)m_brokerDB).FOLMembershipUser_GetNewUserKeysByKeyPortalTemplate(keyPortalTemplate);
	    } 

		/// <summary>
		/// Resolves the references for export.
		/// </summary>
		/// <param name="entity">The entity.</param>
		protected override void ResolveReferencesForExport(PortalTemplate entity) {
			// Do nothing.
		}

		/// <summary>
		/// Resolves the references during mapping.
		/// </summary>
		/// <param name="mappingRecord">The mapping record.</param>
		/// <param name="persistedEntity">The persisted entity.</param>
		protected override void ResolveReferencesForImport(MappingRecord<PortalTemplate> mappingRecord, PortalTemplate persistedEntity) {
			// Do nothing.
		}

	}
}


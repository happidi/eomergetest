﻿using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.DataLayer.BrokerDB;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for business entity Tenant.
	/// </summary>
	public class TenantBroker : BaseBroker<Tenant, TenantBrokerDB> {
		
		
	}

}
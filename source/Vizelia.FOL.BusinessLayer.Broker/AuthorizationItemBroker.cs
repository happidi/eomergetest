﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.Configuration;
using System.Web.Hosting;
using System.Web.Security;
using NetSqlAzMan;
using NetSqlAzMan.Interfaces;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Localization;
using Vizelia.FOL.DataLayer.BrokerDB;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for AuthorizationItem.
	/// </summary>
	public class AuthorizationItemBroker : MappingEnabledBroker<AuthorizationItem, AuthorizationItemBrokerDB> {
		private const string const_ui_indicator = "@@@@";
		private readonly IFOLRoleProvider m_ProviderRole;

		/// <summary>
		/// Initializes a new instance of the <see cref="AuthorizationItemBroker"/> class.
		/// </summary>
		public AuthorizationItemBroker() {
			m_ProviderRole = (IFOLRoleProvider)Roles.Provider;
			m_brokerDB = Helper.CreateInstance<AuthorizationItemBrokerDB>();
			m_database_errors.Add(101, Langue.error_msg_uniquefield, "Name");
		}

		/// <summary>
		/// Resolves the references for import.
		/// </summary>
		/// <param name="mappingRecord">The mapping record.</param>
		/// <param name="persistedEntity">The persisted entity.</param>
		protected override void ResolveReferencesForImport(MappingRecord<AuthorizationItem> mappingRecord, AuthorizationItem persistedEntity) {
			mappingRecord.Entity.FilterSpatial = ResolveFiltersForImport<Location>(mappingRecord.Entity.FilterSpatial);
			mappingRecord.Entity.FilterChart = ResolveFiltersForImport<Chart>(mappingRecord.Entity.FilterChart);
			mappingRecord.Entity.FilterClassificationItem = ResolveFiltersForImport<ClassificationItem>(mappingRecord.Entity.FilterClassificationItem);
			mappingRecord.Entity.FilterPortalWindow = ResolveFiltersForImport<PortalWindow>(mappingRecord.Entity.FilterPortalWindow);
			mappingRecord.Entity.FilterAlarmDefinition = ResolveFiltersForImport<AlarmDefinition>(mappingRecord.Entity.FilterAlarmDefinition);
			mappingRecord.Entity.FilterDynamicDisplay = ResolveFiltersForImport<DynamicDisplay>(mappingRecord.Entity.FilterDynamicDisplay);
			mappingRecord.Entity.FilterPlaylist = ResolveFiltersForImport<Playlist>(mappingRecord.Entity.FilterPlaylist);
			mappingRecord.Entity.FilterChartScheduler = ResolveFiltersForImport<ChartScheduler>(mappingRecord.Entity.FilterChartScheduler);
			mappingRecord.Entity.FilterPortalTemplate = ResolveFiltersForImport<PortalTemplate>(mappingRecord.Entity.FilterPortalTemplate);
			mappingRecord.Entity.FilterReport = ResolveFiltersForImport<Report>(mappingRecord.Entity.FilterReport);
			ResolveParentReferenceForImport<AuthorizationItem>(e => e.ParentId, mappingRecord.Entity, persistedEntity);
		}

		/// <summary>
		/// Resolves the references for export.
		/// </summary>
		/// <param name="entity">The entity.</param>
		protected override void ResolveReferencesForExport(AuthorizationItem entity) {
			entity.FilterSpatial = ResolveFiltersForExport(entity.FilterSpatial);
			entity.FilterChart = ResolveFiltersForExport(entity.FilterChart);
			entity.FilterClassificationItem = ResolveFiltersForExport(entity.FilterClassificationItem);
			entity.FilterPortalWindow = ResolveFiltersForExport(entity.FilterPortalWindow);
			entity.FilterAlarmDefinition = ResolveFiltersForExport(entity.FilterAlarmDefinition);
			entity.FilterDynamicDisplay = ResolveFiltersForExport(entity.FilterDynamicDisplay);
			entity.FilterPlaylist = ResolveFiltersForExport(entity.FilterPlaylist);
			entity.FilterChartScheduler = ResolveFiltersForExport(entity.FilterChartScheduler);
			entity.FilterPortalTemplate = ResolveFiltersForExport(entity.FilterPortalTemplate);
			entity.FilterReport = ResolveFiltersForExport(entity.FilterReport);
			ResolveParentReferenceForExport<AuthorizationItem>(e => e.ParentId, entity);
		}

		/// <summary>
		/// Creates the specified item.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <returns></returns>
		public override AuthorizationItem Create(AuthorizationItem item) {
			Validate(item);
			AuthorizationItem retVal;
			switch (item.ItemType) {
				case "Role":
					retVal = m_ProviderRole.AzManRole_Create(item.ParentId, item.Name, item.Description, item.FilterSpatial, item.FilterReport, item.FilterChart, item.FilterAlarmDefinition, item.FilterDynamicDisplay, item.FilterPlaylist, item.FilterChartScheduler, item.FilterPortalTemplate, item.FilterPortalWindow, item.FilterClassificationItem, item.FilterLink);
					break;
				// Project
				case "Filter":
					retVal = m_ProviderRole.AzManFilter_Create(item.ParentId, item.Name, item.Description, item.FilterSpatial, item.FilterReport, item.FilterChart, item.FilterAlarmDefinition, item.FilterDynamicDisplay, item.FilterPlaylist, item.FilterChartScheduler, item.FilterPortalTemplate, item.FilterPortalWindow, item.FilterClassificationItem, item.FilterLink);
					break;
				case "Task":
					retVal = m_ProviderRole.AzManTask_Create(item.Name, item.Description);
					break;
				case "Operation":
					retVal = m_ProviderRole.AzManOperation_Create(item.Name, item.Description);
					break;
				default:
					throw new NotSupportedException();
			}
			if (retVal != null) {
				AuditAction(CrudAction.Create, null, retVal, item.ItemType);
			}
			return retVal;
		}

		/// <summary>
		/// Copies an azman role.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <returns>True if the copy worked, false otherwise.</returns>
		public bool AzManRole_Copy(AuthorizationItem item) {
			return m_ProviderRole.AzManRole_Copy(item);
		}

		/// <summary>
		/// Gets the item by local id.
		/// </summary>
		/// <param name="localId">The local id.</param>
		/// <returns></returns>
		public override AuthorizationItem GetItemByLocalId(string localId) {
			return m_ProviderRole.AuthorizationItem_GetByName(localId);
		}

		/// <summary>
		/// Updates the specified item.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <param name="changedFields">The changed fields.</param>
		/// <returns></returns>
		public override AuthorizationItem Update(AuthorizationItem item, ref List<Tuple<string, string, string>> changedFields) {
			AuthorizationItem persistedItem = null;
			var key = item.Id;
			if (key != null && ShouldAudit(item.ItemType)) {
				persistedItem = GetItem(key);
			}

			Validate(item);
			switch (item.ItemType) {
				case "Role":
					item.Item = m_ProviderRole.AzManFilter_Update(key, item.Name, item.Description, null, null, null, null, null, null, null, null, null, null, null);
					break;
				case "Filter":
					item.Item = m_ProviderRole.AzManFilter_Update(key, item.Name, item.Description, item.FilterSpatial, item.FilterReport, item.FilterChart, item.FilterAlarmDefinition, item.FilterDynamicDisplay, item.FilterPlaylist, item.FilterChartScheduler, item.FilterPortalTemplate, item.FilterPortalWindow, item.FilterClassificationItem, item.FilterLink);
					break;
				case "Task":
					item.Item = m_ProviderRole.AzManTask_Update(key, item.Name, item.Description);
					break;
				case "Operation":
					item.Item = m_ProviderRole.AzManFilter_Update(key, item.Name, item.Description, null, null, null, null, null, null, null, null, null, null, null);
					break;
				default:
					throw new NotSupportedException();
			}
			if (key != null && item.Item != null) {
				changedFields = AuditAction(CrudAction.Update, persistedItem, item, item.ItemType);
			}
			return item;
		}

		/// <summary>
		/// Deletes the specified item.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <returns></returns>
		private bool Delete(AuthorizationItem item) {
			return Delete(item, null);
		}

		/// <summary>
		/// Deletes the specified item.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <param name="persistedItem">The persisted item.</param>
		/// <returns></returns>
		public override bool Delete(AuthorizationItem item, AuthorizationItem persistedItem = null) {
			if (ShouldAudit(item.ItemType)) {
				if (persistedItem == null && item.Id != null) {
					persistedItem = GetItem(item.Id);
				}
				if (persistedItem == null && item.Name != null) {
					persistedItem = GetItemByLocalId(item.Name);
				}
			}

			var success = m_ProviderRole.AzManItem_Delete(item);

			if (success) {
				AuditAction(CrudAction.Destroy, persistedItem, null, item.ItemType);
			}
			return success;
		}

		/// <summary>
		/// Deletes the role.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <param name="persistedItem">The persisted item.</param>
		/// <returns></returns>
		public bool DeleteRole(AuthorizationItem item, AuthorizationItem persistedItem = null) {
			if (ShouldAudit(item.ItemType)) {
				persistedItem = persistedItem ?? GetItemByLocalId(item.Name);
			}
			var success = ((RoleProvider)m_ProviderRole).DeleteRole(item.Name, false);

			if (success) {
				AuditAction(CrudAction.Destroy, persistedItem, null, item.ItemType);
			}
			return success;
		}

		/// <summary>
		/// Get list of AzManRole by application group id.
		/// </summary>
		/// <param name="applicationGroupId">The application group id.</param>
		/// <returns></returns>
		public List<AuthorizationItem> AzManRole_GetListByApplicationGroupId(string applicationGroupId) {
			return m_ProviderRole.AzManRole_GetListByApplicationGroupId(applicationGroupId);
		}

		/// <summary>
		/// Saves the authorization item application group.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="roles">The roles.</param>
		public void SaveApplicationGroupAuthorizationItem(ApplicationGroup entity, CrudStore<AuthorizationItem> roles) {
			m_ProviderRole.CreateGroupAuthorizations(entity, roles);
		}

		/// <summary>
		/// Saves the authorization item chart.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="roles">The roles.</param>
		public void SaveChartAuthorizationItem(Chart entity, CrudStore<AuthorizationItem> roles) {
			((AuthorizationItemBrokerDB)m_brokerDB).SaveChartFilterAzManRole(entity, roles);
		}

		/// <summary>
		/// Gets all.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="location">The location.</param>
		/// <param name="total">The total.</param>
		/// <param name="useSecurableFilter">if set to <c>true</c> [use securable filter].</param>
		/// <param name="isHierarchyEnabled">if set to <c>true</c> [is hierarchy enabled].</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		/// <exception cref="System.NotImplementedException"></exception>
		public override List<AuthorizationItem> GetAll(PagingParameter paging, PagingLocation location, out int total, bool useSecurableFilter = true, bool isHierarchyEnabled = true, params string[] fields) {
			throw new NotImplementedException();
		}

		/// <summary>
		/// Gets the children.
		/// </summary>
		/// <param name="Key">The key.</param>
		/// <returns></returns>
		public override List<AuthorizationItem> GetChildren(string Key) {
			return m_ProviderRole.AzManItem_GetChildren(Key);
		}

		/// <summary>
		/// Gets the item.
		/// </summary>
		/// <param name="Key">The key.</param>
		/// <returns></returns>
		public override AuthorizationItem GetItem(string Key) {
			return m_ProviderRole.AzManItem_GetItem(Key);
		}

		/// <summary>
		/// Gets the tree.
		/// </summary>
		/// <param name="Key">The key.</param>
		/// <returns></returns>
		public List<TreeNode> AzManFilter_GetTree(string Key) {
			var retVal = m_ProviderRole.AzManFilter_GetTree(Key);
			return retVal;
		}

		/// <summary>
		/// Gets the tree.
		/// </summary>
		/// <param name="Key">The key.</param>
		/// <returns></returns>
		public List<TreeNode> AzManRole_GetTree(string Key) {
			var retVal = m_ProviderRole.AzManRole_GetTree(Key);
			return retVal;
		}

		/// <summary>
		/// Filter_s the get list.
		/// </summary>
		/// <param name="id">The id.</param>
		/// <param name="searchByName">if set to <c>true</c> [search by name].</param>
		/// <param name="typeName">Name of the type.</param>
		/// <returns></returns>
		public List<SecurableEntity> Filter_GetList(string id, bool searchByName, string typeName) {
			return m_ProviderRole.AzManFilter_Filter_GetList(id, searchByName, typeName);
		}


		/// <summary>
		/// Adds the operations.
		/// </summary>
		/// <param name="operationNames">Names of the operations.</param>
		public void AddOperations(IEnumerable<string> operationNames) {
			if (operationNames == null || !operationNames.Any())
				return;

			string applicationVirtualPath = HostingEnvironment.ApplicationVirtualPath;
			Configuration config = WebConfigurationManager.OpenWebConfiguration(applicationVirtualPath);
			RoleManagerSection configSection = (RoleManagerSection)config.GetSection("system.web/roleManager");
			ProviderSettings roleProvider = configSection.Providers[0];
			string dns = roleProvider.Parameters["connectionStringName"];
			var applicationName = ContextHelper.ApplicationName;
			string storeName = roleProvider.Parameters["storeName"];
			string connectionString = config.ConnectionStrings.ConnectionStrings[dns].ConnectionString;


			var sqlAzmanStorage = new SqlAzManStorage(connectionString);

			if (!sqlAzmanStorage.Stores.ContainsKey(storeName))
				return;

			var application = sqlAzmanStorage.Stores[storeName].Applications[applicationName];

			foreach (var operationName in operationNames) {
				application.CreateItem(operationName, String.Empty, ItemType.Operation);
			}
		}

		/// <summary>
		/// Saves the store.
		/// </summary>
		/// <param name="store">The store.</param>
		/// <returns></returns>
		public override JsonStore<AuthorizationItem> SaveStore(CrudStore<AuthorizationItem> store) {
			var result = new JsonStore<AuthorizationItem>();
			using (var t = Helper.CreateTransactionScope()) {
				SaveCrudStore(CrudAction.Destroy, store.destroy, result, Delete);
				SaveCrudStore(CrudAction.Create, store.create, result, Create);
				SaveCrudStore(CrudAction.Update, store.update, result, Update);
				if (!result.success) {
					result.records = null;
					return result;
				}
				t.Complete();
			}

			return result;
		}

		private delegate K CrudDelegate<T, K>(T item);
		private delegate K CrudWithFieldDelegate<T, K>(T item, ref List<Tuple<string, string, string>> changedFields);

		/// <summary>
		/// Saves a list of item. The responseStore argument should be instanciated outside of the function.
		/// </summary>
		/// <param name="action">The action (create, update, destroy).</param>
		/// <param name="items">The list of items to save.</param>
		/// <param name="responseStore">The response.</param>
		/// <param name="crud">The crud function.</param>
		private void SaveCrudStore<T, K>(string action, List<T> items, JsonStore<T> responseStore, CrudDelegate<T, K> crud) where T : BaseBusinessEntity {
			if (items == null)
				return;
			if (responseStore == null)
				responseStore = new JsonStore<T>();
			foreach (T item in items) {
				try {
					crud(item);
				}
				catch (Exception ex) {
					responseStore.success = false;
					responseStore.message = ex.Message;
					return;
				}

			}

		}

		/// <summary>
		/// Saves a list of item. The responseStore argument should be instanciated outside of the function.
		/// </summary>
		/// <param name="action">The action (create, update, destroy).</param>
		/// <param name="items">The list of items to save.</param>
		/// <param name="responseStore">The response.</param>
		/// <param name="crud">The crud function.</param>
		private void SaveCrudStore<T, K>(string action, List<T> items, JsonStore<T> responseStore, CrudWithFieldDelegate<T, K> crud) where T : BaseBusinessEntity {
			List<Tuple<string, string, string>> changedFields = null;

			if (items == null)
				return;
			if (responseStore == null)
				responseStore = new JsonStore<T>();
			foreach (T item in items) {
				try {
					crud(item, ref changedFields);
				}
				catch (Exception ex) {
					responseStore.success = false;
					responseStore.message = ex.Message;
					return;
				}
			}
		}

		/// <summary>
		/// Get store of tasks.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="onlyUITasks">if set to <c>true</c> [only UI tasks].</param>
		/// <returns></returns>
		public JsonStore<AuthorizationItem> Task_GetStore(PagingParameter paging, bool onlyUITasks) {
			var tasks = m_ProviderRole.AzManTask_GetAll();
			if (onlyUITasks) {
				tasks = tasks.Where(a => a.Name.Contains(const_ui_indicator)).ToList();
			}
			return tasks.ToJsonStoreWithFilter(paging);
		}

		/// <summary>
		/// Get store of user authorizations.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="username">The username.</param>
		/// <returns></returns>
		public JsonStore<AuthorizationItem> UserAuthorization_GetStore(PagingParameter paging, string username) {
			// we only returns here the level above the operation
			var retVal = m_ProviderRole.AuthorizationItem_GetAll(username).Where(p => p.ItemType != "Operation").ToList();
			return retVal.ToJsonStoreWithFilter(paging);
		}

		/// <summary>
		/// Get list of oprations.
		/// </summary>
		/// <returns></returns>
		public List<AuthorizationItem> Operation_GetList() {
			var retVal = m_ProviderRole.AzManOperation_GetAll();

			return retVal;
		}

		/// <summary>
		/// Invalidates the storage cache.
		/// </summary>
		public void InvalidateStorageCache() {
			m_ProviderRole.InvalidateStorageCache();
		}

		/// <summary>
		/// Saves the authorization item authorization item.
		/// </summary>
		/// <param name="authorizationItem">The authorization item.</param>
		/// <param name="authorizationsItems">The operations.</param>
		public bool SaveAuthorizationItemAuthorizationItem(AuthorizationItem authorizationItem, CrudStore<AuthorizationItem> authorizationsItems) {
			return m_ProviderRole.AddRemoveAuthorizationItems(authorizationItem, authorizationsItems);
		}

		private static List<SecurableEntity> ResolveFiltersForImport<TEntity>(IEnumerable<SecurableEntity> filters) where TEntity : BaseBusinessEntity {
			var retVal = new List<SecurableEntity>();
			if (filters != null)
				foreach (var filter in filters) {
					if (typeof(TEntity) == typeof(Location)) {
						var locationType = filter.SecurableName.ParseAsEnum<HierarchySpatialTypeName>();
						dynamic broker;
						switch (locationType) {
							case HierarchySpatialTypeName.IfcSite:
								broker = BrokerFactory.CreateBroker<Site>();
								break;
							case HierarchySpatialTypeName.IfcBuilding:
								broker = BrokerFactory.CreateBroker<Building>();
								break;
							case HierarchySpatialTypeName.IfcBuildingStorey:
								broker = BrokerFactory.CreateBroker<BuildingStorey>();
								break;
							case HierarchySpatialTypeName.IfcSpace:
								broker = BrokerFactory.CreateBroker<Space>();
								break;
							case HierarchySpatialTypeName.IfcFurniture:
								broker = BrokerFactory.CreateBroker<Furniture>();
								break;
							case HierarchySpatialTypeName.None:
								throw new ArgumentException("Value of LocationTypeName cannot be set to None when KeyLocation is assigned a value.");
							default:
								throw new ArgumentException(string.Format("Unexpected HierarchySpatialTypeName value."));
						}

						var item = broker.GetItemByLocalId(filter.LocalId) as ISecurableObject;
						if (item == null) {
							throw new ArgumentException(string.Format("Failed to resolve Location filter of {0} with LocalId {1}, entity does not exists", filter.SecurableName, filter.LocalId));
						}
						var securableObject = item.GetSecurableObject();
						retVal.Add(securableObject);
					}
					else {
						var broker = BrokerFactory.CreateBroker<TEntity>();
						var item = broker.GetItemByLocalId(filter.LocalId) as ISecurableObject;
						if (item == null) {
							throw new ArgumentException(string.Format("Failed to resolve {0} filter with LocalId {1}, entity does not exists", filter.SecurableName, filter.LocalId));
						}
						var securableObject = item.GetSecurableObject();
						retVal.Add(securableObject);
					}
				}
			return retVal;
		}

		private static List<SecurableEntity> ResolveFiltersForExport(IEnumerable<SecurableEntity> filters) {
			List<SecurableEntity> retVal = null;
			if (filters != null)
				retVal = filters.Select(filter => new SecurableEntity {
					LocalId = filter.SecurableName,
					SecurableName = filter.SecurableTypeName.Split('.').Last()
				}).ToList();

			return retVal;
		}

		/// <summary>
		/// Gets the azman tree starting from the given item.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <returns>
		/// A list of tree nodes
		/// </returns>
		public List<TreeNode> AzManItem_GetAncestorTree(string key) {
			return m_ProviderRole.AzManItem_GetAncestorTree(key);
		}
	}
}
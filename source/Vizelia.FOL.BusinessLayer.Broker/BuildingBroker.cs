﻿using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.DataLayer.BrokerDB;
using System.Collections.Generic;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for business entity Building.
	/// </summary>
	public class BuildingBroker : MappingEnabledBroker<Building, BuildingBrokerDB> {
		/// <summary>
		/// Resolves the references during mapping.
		/// </summary>
		/// <param name="mappingRecord">The entity.</param>
		/// <param name="persistedEntity">The entity persisted.</param>
		protected override void ResolveReferencesForImport(MappingRecord<Building> mappingRecord, Building persistedEntity) {
			ResolveParentReferenceForImport<Site>(e => e.KeySite, mappingRecord.Entity, persistedEntity);
			ResolveParentReferenceForImport<FOLMembershipUser>(e => e.Creator, mappingRecord.Entity, persistedEntity);
		}

		/// <summary>
		/// Resolves the references for export.
		/// </summary>
		/// <param name="entity">The entity.</param>
		protected override void ResolveReferencesForExport(Building entity) {
			ResolveParentReferenceForExport<Site>(e => e.KeySite, entity);
			ResolveParentReferenceForExport<FOLMembershipUser>(e => e.Creator, entity);
		}

		/// <summary>
		/// Get the list of Building from a Map
		/// </summary>
		/// <param name="KeyMap">The key of the Map.</param>
		public List<Building> GetListByKeyMap(string KeyMap) {
			return ((BuildingBrokerDB)m_brokerDB).GetListByKeyMap(KeyMap);
		}
	}

}

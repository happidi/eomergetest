using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using Vizelia.FOL.DataLayer.BrokerDB;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// Base business layer class for business entity broker that supports mappable entities.
	/// </summary>
	/// <typeparam name="TEntity">The type of the entity.</typeparam>
	/// <typeparam name="TEntityBrokerDB">The type of the entity broker DB.</typeparam>
	public abstract class MappingEnabledBroker<TEntity, TEntityBrokerDB> : BaseBroker<TEntity, TEntityBrokerDB>, IMappingBroker
		where TEntity : BaseBusinessEntity, IMappableEntity, new()
		where TEntityBrokerDB : BaseBrokerDBGeneric<TEntity>, new() {

		/// <summary>
		/// Gets all items fully populated and resolved for export.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="total">The total of results.</param>
		/// <returns>A list of items</returns>
		List<BaseBusinessEntity> IMappingBroker.GetAllForExport(PagingParameter paging, PagingLocation location, out int total) {
			List<TEntity> entities = GetAllForExport(paging, location, out total);
			List<BaseBusinessEntity> baseBusinessEntities = entities.OfType<BaseBusinessEntity>().ToList();

			return baseBusinessEntities;
		}

		/// <summary>
		/// Changes the item properties.
		/// Generates a LocalId for mappable entities.
		/// </summary>
		/// <param name="item">The item.</param>
		protected override void ChangeItemProperties(TEntity item) {
			base.ChangeItemProperties(item);
			if (String.IsNullOrEmpty(item.LocalId))
				item.LocalId = Guid.NewGuid().ToString();
		}


		/// <summary>
		/// Copies the specified item.
		/// In the case of a mappable entity we need to nullify the Localid so that the copied item gets a new generated Localid.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <param name="formResponse">The form response.</param>
		/// <param name="Creator">The creator.</param>
		/// <param name="incrementTitle">if set to <c>true</c> [increment title].</param>
		/// <returns></returns>
		public override TEntity Copy(TEntity item, out FormResponse formResponse, string Creator = null, bool incrementTitle = true) {
			item.LocalId = null;
			return base.Copy(item, out formResponse, Creator, incrementTitle);
		}

		/// <summary>
		/// Resolves the list for export.
		/// </summary>
		/// <param name="list">The list.</param>
		/// <param name="loadMode">The load mode.</param>
		/// <returns></returns>
		public void ResolveForExport(List<BaseBusinessEntity> list, EntityLoadMode loadMode) {
			List<TEntity> entities = list.Cast<TEntity>().ToList();
			ResolveForExport(entities, loadMode);
		}

		/// <summary>
		/// Resolves the entity for export.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="loadMode">The load mode.</param>
		/// <returns></returns>
		public void ResolveForExport(BaseBusinessEntity entity, EntityLoadMode loadMode) {
			ResolveForExport((TEntity)entity, loadMode);
		}

		/// <summary>
		/// Gets the maximum size of the batch.
		/// </summary>
		/// <value>
		/// The maximum size of the batch.
		/// </value>
		public virtual int MaximumBatchSize {
			get { return 1; }
		}

		/// <summary>
		/// Gets all items fully populated and resolved for export.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="total">The total of results.</param>
		/// <returns>A list of items</returns>
		public List<TEntity> GetAllForExport(PagingParameter paging, PagingLocation location, out int total) {
			List<TEntity> entities = GetAll(paging, location, out total);

			PopulateList(entities, EntityLoadMode.ForExport, "*");
			ResolveForExport(entities, EntityLoadMode.ForExport);

			return entities;
		}

		/// <summary>
		/// Resolves the reference for parent during mapping.
		/// The function will perform a search of the parent value and generate an error when not found.
		/// If parent value is ommitted it will try to get it from the persisted entity.
		/// </summary>
		/// <typeparam name="TParentEntity">The type of the parent entity</typeparam>
		/// <param name="childEntityParentKeyPropertyNameIndicatingExpression">The expression indicating the property of the child entity that holds the parent entity key.</param>
		/// <param name="childEntity">The child entity.</param>
		/// <param name="persistedChildEntity">The persisted child entity.</param>
		protected void ResolveParentReferenceForImport<TParentEntity>(Expression<Func<TEntity, string>> childEntityParentKeyPropertyNameIndicatingExpression, BaseBusinessEntity childEntity, BaseBusinessEntity persistedChildEntity) where TParentEntity : BaseBusinessEntity, IMappableEntity, new() {
			var member = Helper.GetMemberFromExpression(childEntityParentKeyPropertyNameIndicatingExpression);
			ResolveParentReferenceForImport<TParentEntity>(member.Name, childEntity, persistedChildEntity);
		}

		/// <summary>
		/// Resolves the reference for parent during export.
		/// The function will perform a search of the parent value and generate an error when not found.
		/// If parent value is ommitted it will try to get it from the persisted entity.
		/// </summary>
		protected void ResolveParentReferenceForExport<TParentEntity>(Expression<Func<TEntity, string>> childEntityParentKeyPropertyNameIndicatingExpression, BaseBusinessEntity entity) where TParentEntity : BaseBusinessEntity, IMappableEntity, new() {
			var member = Helper.GetMemberFromExpression(childEntityParentKeyPropertyNameIndicatingExpression);
			ResolveParentReferenceForExport<TParentEntity>(member.Name, entity);
		}

		/// <summary>
		/// Resolves the reference for parent during mapping.
		/// The function will perform a search of the parent value and generate an error when not found.
		/// If parent value is ommitted it will try to get it from the persisted entity.
		/// </summary>
		/// <typeparam name="TParentEntity">The type of the parent entity</typeparam>
		/// <param name="childEntityParentKeyProperty">The name of the property of the child entity that contains the parent entity key.</param>
		/// <param name="childEntity">The child entity.</param>
		/// <param name="persistedChildEntity">The persisted child entity.</param>
		protected void ResolveParentReferenceForImport<TParentEntity>(string childEntityParentKeyProperty, BaseBusinessEntity childEntity, BaseBusinessEntity persistedChildEntity) where TParentEntity : BaseBusinessEntity, new() {
			string parentLocalId = childEntity.GetPropertyValueString(childEntityParentKeyProperty, false, false);
			string resolvedKey = GetParentKey<TParentEntity>(childEntityParentKeyProperty, parentLocalId, persistedChildEntity);
			childEntity.SetPropertyValue(childEntityParentKeyProperty, resolvedKey);
		}

		/// <summary>
		/// Resolves the reference for parent during export.
		/// The function will perform a search of the parent value and generate an error when not found.
		/// If parent value is ommitted it will try to get it from the persisted entity.
		/// </summary>
		/// <typeparam name="TParentEntity">The type of the parent entity</typeparam>
		/// <param name="childEntityParentKeyProperty">The name of the property of the child entity that contains the parent entity key.</param>
		/// <param name="entity">The entity.</param>
		protected void ResolveParentReferenceForExport<TParentEntity>(string childEntityParentKeyProperty, BaseBusinessEntity entity) where TParentEntity : BaseBusinessEntity, IMappableEntity, new() {
			string parentKey = entity.GetPropertyValueString(childEntityParentKeyProperty, false, false);
			string resolvedLocalId = GetParentLocalId<TParentEntity>(childEntityParentKeyProperty, parentKey, entity);
			entity.SetPropertyValue(childEntityParentKeyProperty, resolvedLocalId);
		}


		/// <summary>
		/// Gets the parent key for the given local ID.
		/// </summary>
		/// <typeparam name="TParentEntity">The type of the parent entity.</typeparam>
		/// <param name="childEntityParentKeyPropertyNameIndicatingExpression">An expression indicating the property of the child entity that contains the parent key.</param>
		/// <param name="parentLocalId">The parent local id.</param>
		/// <param name="persistedChildEntity">The persisted child entity.</param>
		/// <returns>
		/// The key for the given local ID.
		/// </returns>
		protected static string GetParentKey<TParentEntity>(Expression<Func<TEntity, object>> childEntityParentKeyPropertyNameIndicatingExpression, string parentLocalId, BaseBusinessEntity persistedChildEntity) where TParentEntity : BaseBusinessEntity, new() {
			var member = Helper.GetMemberFromExpression(childEntityParentKeyPropertyNameIndicatingExpression);
			string parentKey = GetParentKey<TParentEntity>(member.Name, parentLocalId, persistedChildEntity);
			return parentKey;
		}

		/// <summary>
		/// Gets the parent key for the given local ID.
		/// </summary>
		/// <typeparam name="TParentEntity">The type of the parent entity.</typeparam>
		/// <param name="childEntityParentKeyProperty">The property on the child entity that contains the the parent key.</param>
		/// <param name="parentLocalId">The parent local id.</param>
		/// <param name="persistedChildEntity">The persisted child entity.</param>
		/// <returns>The key for the given local ID.</returns>
		protected static string GetParentKey<TParentEntity>(string childEntityParentKeyProperty, string parentLocalId, BaseBusinessEntity persistedChildEntity) where TParentEntity : BaseBusinessEntity, new() {
			string resolvedKey = null;

			if (parentLocalId != null) {
				// If we got an empty string, it means that the user explicitly wants to override the parent to null. 
				// This is different than ommitting the value.
				if (parentLocalId != String.Empty) {

					// Try retreiving the parent key by the local id and assign it back to the child entity.
					var parentEntity = GetParentEntityByLocalId<TParentEntity>(parentLocalId, childEntityParentKeyProperty);

					// Get the parent's key property value and assign it to the child entity's parent key property.
					resolvedKey = parentEntity.GetKey();
				}
				// Else: The local id is an empty string, so the key will be nullified.
			}
			else {
				// Check if this is an update of an existing entity and a new value for the parent local id wasn't specified.
				// This means the parent local id field value should not change and we can take the persisted entity value.
				if (persistedChildEntity != null) {
					object propertyValue = persistedChildEntity.GetPropertyValue(childEntityParentKeyProperty);
					resolvedKey = (propertyValue != null) ? propertyValue.ToString() : null;
				}
				// Else: This is a creation of a new entity and the parent is null.
			}

			return resolvedKey;
		}

		/// <summary>
		/// Gets the parent local ID for the given key.
		/// </summary>
		/// <typeparam name="TParentEntity">The type of the parent entity.</typeparam>
		/// <param name="childEntityParentKeyProperty">The property on the child entity that contains the the parent key.</param>
		/// <param name="parentKey">The parent key.</param>
		/// <param name="entity">The entity.</param>
		/// <returns></returns>
		protected string GetParentLocalId<TParentEntity>(string childEntityParentKeyProperty, string parentKey, BaseBusinessEntity entity) where TParentEntity : BaseBusinessEntity, IMappableEntity, new() {
			if (string.IsNullOrEmpty(parentKey)) {
				return null;
			}

			// Try retreiving the parent key by the local id and assign it back to the child entity.
			IBroker<TParentEntity> broker = BrokerFactory.CreateBroker<TParentEntity>();
			var parentEntity = broker.GetItem(parentKey);

			if (parentEntity == null) {
				throw new VizeliaException(string.Format("Could not find entity of type '{0}' with key '{1}', in order to resolve field {2} of entity of type '{3}' with key '{4}' for export.", typeof(TParentEntity).FullName, parentKey, childEntityParentKeyProperty, typeof(TEntity).FullName, entity.GetKey()));
			}

			return parentEntity.LocalId;
		}

		/// <summary>
		/// Gets the parent entity by local id.
		/// </summary>
		/// <typeparam name="TParentEntity">The type of the parent entity.</typeparam>
		/// <param name="parentLocalId">The parent local id.</param>
		/// <param name="childEntityParentKeyProperty">The child entity parent key property.</param>
		/// <returns></returns>
		protected static TParentEntity GetParentEntityByLocalId<TParentEntity>(string parentLocalId, string childEntityParentKeyProperty) where TParentEntity : BaseBusinessEntity, new() {
			IBroker<TParentEntity> broker = BrokerFactory.CreateBroker<TParentEntity>();
			var parentEntity = broker.GetItemByLocalId(parentLocalId);

			if (parentEntity == null) {
				throw new ArgumentException(String.Format("{0}: Parent {1} with local ID '{2}' does not exist. Please make sure such an entity with the given local ID value exists. The local ID value is case sensitive.", childEntityParentKeyProperty, typeof(TParentEntity).Name, parentLocalId));
			}

			return parentEntity;
		}

		/// <summary>
		/// Gets the parent entity by key.
		/// </summary>
		/// <typeparam name="TParentEntity">The type of the parent entity.</typeparam>
		/// <param name="parentKey">The parent key.</param>
		/// <param name="childEntityParentKeyProperty">The child entity parent key property.</param>
		/// <returns></returns>
		protected static TParentEntity GetParentEntityByKey<TParentEntity>(string parentKey, string childEntityParentKeyProperty) where TParentEntity : BaseBusinessEntity, new() {
			IBroker<TParentEntity> broker = BrokerFactory.CreateBroker<TParentEntity>();
			var parentEntity = broker.GetItem(parentKey);

			if (parentEntity == null) {
				throw new ArgumentException(String.Format("{0}: Parent {1} with key '{2}' does not exist", childEntityParentKeyProperty, typeof(TParentEntity).Name, parentKey));
			}

			return parentEntity;
		}

		/// <summary>
		/// Resolves the location reference.
		/// </summary>
		/// <typeparam name="TLocationEntity">The type of the location entity.</typeparam>
		/// <param name="childEntityMappingRecord">The child entity mapping record.</param>
		/// <param name="persistedEntity">The persisted entity.</param>
		protected void ResolveLocationReferenceForImport<TLocationEntity>(MappingRecord<TLocationEntity> childEntityMappingRecord, TLocationEntity persistedEntity) where TLocationEntity : TEntity, ISupportLocation {
			// Was the location set on the entity? The two checks are important because in RSP entities we assign the value
			// to the location as default, but we don't set the IsPropertyAssigned flag.
			if (string.IsNullOrEmpty(childEntityMappingRecord.Entity.KeyLocation) && !childEntityMappingRecord.IsPropertyAssigned(e => e.KeyLocation)) {
				return;
			}

			// We cannot resolve a location local ID without knowing what type it is. Two checks for the same reason as above.
			if (childEntityMappingRecord.Entity.LocationTypeName == HierarchySpatialTypeName.None && !childEntityMappingRecord.IsPropertyAssigned(e => e.LocationTypeName)) {
				throw new ArgumentException("LocationTypeName : Value missing. Cannot assign KeyLocation without assigning LocationTypeName.");
			}

			// Resolve the location
			MemberInfo childEntityParentKeyPropertyInfo = Helper.GetMemberFromExpression<TLocationEntity, string>(e => e.KeyLocation);
			string childEntityParentKeyPropertyName = childEntityParentKeyPropertyInfo.Name;

			switch (childEntityMappingRecord.Entity.LocationTypeName) {
				case HierarchySpatialTypeName.IfcSite:
					ResolveParentReferenceForImport<Site>(childEntityParentKeyPropertyName, childEntityMappingRecord.Entity, persistedEntity);
					break;
				case HierarchySpatialTypeName.IfcBuilding:
					ResolveParentReferenceForImport<Building>(childEntityParentKeyPropertyName, childEntityMappingRecord.Entity, persistedEntity);
					break;
				case HierarchySpatialTypeName.IfcBuildingStorey:
					ResolveParentReferenceForImport<BuildingStorey>(childEntityParentKeyPropertyName, childEntityMappingRecord.Entity, persistedEntity);
					break;
				case HierarchySpatialTypeName.IfcSpace:
					ResolveParentReferenceForImport<Space>(childEntityParentKeyPropertyName, childEntityMappingRecord.Entity, persistedEntity);
					break;
				case HierarchySpatialTypeName.IfcFurniture:
					ResolveParentReferenceForImport<Furniture>(childEntityParentKeyPropertyName, childEntityMappingRecord.Entity, persistedEntity);
					break;
				case HierarchySpatialTypeName.None:
					throw new ArgumentException("Value of LocationTypeName cannot be set to None when KeyLocation is assigned a value.");
				default:
					throw new ArgumentException(string.Format("Unexpected HierarchySpatialTypeName value."));
			}
		}

		/// <summary>
		/// Resolves the location reference for export.
		/// </summary>
		/// <typeparam name="TLocationEntity">The type of the location entity.</typeparam>
		/// <param name="entity">The entity.</param>
		protected void ResolveLocationReferenceForExport<TLocationEntity>(TLocationEntity entity) where TLocationEntity : TEntity, ISupportLocation {
			// Was the location set on the entity?
			if (string.IsNullOrEmpty(entity.KeyLocation)) {
				return;
			}

			// Resolve the location
			MemberInfo childEntityParentKeyPropertyInfo = Helper.GetMemberFromExpression<TLocationEntity, string>(e => e.KeyLocation);
			string childEntityParentKeyPropertyName = childEntityParentKeyPropertyInfo.Name;

			switch (entity.LocationTypeName) {
				case HierarchySpatialTypeName.IfcSite:
					ResolveParentReferenceForExport<Site>(childEntityParentKeyPropertyName, entity);
					break;
				case HierarchySpatialTypeName.IfcBuilding:
					ResolveParentReferenceForExport<Building>(childEntityParentKeyPropertyName, entity);
					break;
				case HierarchySpatialTypeName.IfcBuildingStorey:
					ResolveParentReferenceForExport<BuildingStorey>(childEntityParentKeyPropertyName, entity);
					break;
				case HierarchySpatialTypeName.IfcSpace:
					ResolveParentReferenceForExport<Space>(childEntityParentKeyPropertyName, entity);
					break;
				case HierarchySpatialTypeName.IfcFurniture:
					ResolveParentReferenceForExport<Furniture>(childEntityParentKeyPropertyName, entity);
					break;
				case HierarchySpatialTypeName.None:
					throw new ArgumentException("Value of LocationTypeName cannot be set to None when KeyLocation is assigned a value.");
				default:
					throw new ArgumentException(string.Format("Unexpected HierarchySpatialTypeName value."));
			}
		}

		/// <summary>
		/// Executes the mapping for the specified entities.
		/// </summary>
		/// <param name="mappingRecords">The mapping records.</param>
		/// <param name="action">The action.</param>
		/// <param name="mappingContext">The mapping context</param>
		/// <param name="formResponses">The form responses.</param>
		public virtual void Mapping(IEnumerable<IMappingRecord> mappingRecords, string action,
									MappingContext mappingContext,
									out IEnumerable<Tuple<string, FormResponse>> formResponses) {
			var singleFormResponses = new List<Tuple<string, FormResponse>>();

			foreach (var mappingRecord in mappingRecords) {
				FormResponse formResponse;
				
				try {
					Mapping(mappingRecord, action, out formResponse);
				}
				catch (Exception exception) {
					formResponse = new FormResponse {success = false };
					formResponse.AddError(string.Empty, exception.Message);
					formResponse.exception = exception;
				}

				singleFormResponses.Add(new Tuple<string, FormResponse>(mappingRecord.LocalId, formResponse));
			}

			formResponses = singleFormResponses;
		}

		/// <summary>
		/// Executes the mapping for the specified entity.
		/// </summary>
		/// <param name="mappingRecord">The mapping record.</param>
		/// <param name="action">The action.</param>
		/// <param name="formResponse">The form response.</param>
		private void Mapping(IMappingRecord mappingRecord, string action, out FormResponse formResponse) {
			formResponse = null;
			string localid = mappingRecord.LocalId;

			var entityMappingRecord = (MappingRecord<TEntity>)mappingRecord;

			// Remove the psets from the entity so they won't be handled when the entity is saved.
			// We will handle them later ourselves to validate the input, since this is a mapping.
			var psets = RemovePsetsFromEntity(entityMappingRecord.Entity);

			TEntity entityPersisted = null;
			switch (action) {
				case CrudAction.Create:
					entityPersisted = Mapping_Create(entityMappingRecord, localid, out formResponse);
					break;

				case CrudAction.Update:
					entityPersisted = Mapping_Update(entityMappingRecord, localid, out formResponse);
					break;

				case CrudAction.Destroy:
					Mapping_Delete(entityMappingRecord, localid, out formResponse);
					break;
			}
			if (formResponse.success == false)
				return;

			// Run our custom Pset handling logic on the psets we extracted and deleted from the entity earlier.
			HandlePsetsMapping(action, psets, entityPersisted, ref formResponse);

			EnsureEntityCanBeRetreived(entityPersisted, localid);
		}

		/// <summary>
		/// Resolves the references during mapping.
		/// </summary>
		/// <param name="mappingRecord">The mapping record.</param>
		/// <param name="persistedEntity">The persisted entity.</param>
		protected abstract void ResolveReferencesForImport(MappingRecord<TEntity> mappingRecord, TEntity persistedEntity);

		/// <summary>
		/// Resolves the references for export.
		/// </summary>
		/// <param name="entity">The entity.</param>
		protected abstract void ResolveReferencesForExport(TEntity entity);

		/// <summary>
		/// Resolves the list for export.
		/// </summary>
		/// <param name="list">The list.</param>
		/// <param name="loadMode">The load mode.</param>
		/// <returns></returns>
		public void ResolveForExport(List<TEntity> list, EntityLoadMode loadMode) {
			if (list == null || loadMode == EntityLoadMode.Standard) return;

			foreach (TEntity entity in list) {
				ResolveForExport(entity, loadMode);
			}
		}

		/// <summary>
		/// Resolves the entity for export.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="loadMode">The load mode.</param>
		/// <returns></returns>
		public void ResolveForExport(TEntity entity, EntityLoadMode loadMode) {
			if (entity == null) return;

			switch (loadMode) {
				case EntityLoadMode.Standard:
					// Do nothing.
					break;
				case EntityLoadMode.ForExport:
					ResolveReferencesForExport(entity);

					if (string.IsNullOrEmpty(entity.LocalId)) {
						throw new VizeliaException(string.Format("Entity of type {0} with key {1} has no local ID value, cannot export it.", typeof(TEntity).FullName, entity.GetKey()));
					}

					entity.SetKey(entity.LocalId);
					break;
				default:
					throw new ArgumentOutOfRangeException("loadMode");
			}
		}

		/// <summary>
		/// Ensures the entity can be retreived by querying for it using the local id.
		/// Some entities require some async SQL background processing. For example, hierarchical entities.
		/// For those kind of entities we wait until we can retreive them before finishing their mapping.
		/// </summary>
		private void EnsureEntityCanBeRetreived(TEntity entityPersisted, string localid) {
			//if (!string.IsNullOrEmpty(localid)) {
			//    TracingService.Write(string.Format("Making sure that the entity with local id {0} is retreivable...", localid));

			//    // Maximum wait of one minute.
			//    Helper.RetryIfFalse(12, 5000, () => {
			//        TEntity retreivedEntity = GetItemByLocalId(localid);
			//        bool retreived = retreivedEntity != null;

			//        return retreived;
			//    });
			//}
			//else {
			//    string key = entityPersisted.GetKey();
			//    if (!string.IsNullOrEmpty(key)) {
			//        TracingService.Write(string.Format("Making sure that the entity with key {0} is retreivable...", key));

			//        // Maximum wait of one minute.
			//        Helper.RetryIfFalse(12, 5000, () => {
			//            TEntity retreivedEntity = GetItem(key);
			//            bool retreived = retreivedEntity != null;

			//            return retreived;
			//        });
			//    }
			//    else {
			//        TracingService.Write("Cannot make sure that the entity is retreivable, as it has no value in the local id or key properties");
			//    }
			//}

		}

		/// <summary>
		/// Handles the Psets mapping.
		/// </summary>
		/// <param name="action">The action.</param>
		/// <param name="psets">The Psets.</param>
		/// <param name="entityPersisted">The entity persisted.</param>
		/// <param name="formResponse">The form response.</param>
		private void HandlePsetsMapping(string action, List<Pset> psets, TEntity entityPersisted, ref FormResponse formResponse) {

			switch (action) {
				case CrudAction.Create:
				case CrudAction.Update:
					var entityPersistedPset = entityPersisted as IPset;

					if (entityPersistedPset != null && psets.Any()) {
						entityPersistedPset.PropertySetList = psets;
						FormResponse responsePset = Mapping_Pset(entityPersisted);
						if (responsePset == null)
							break;
						if (!responsePset.success) {
							formResponse.success = false;
							formResponse.msg = responsePset.msg;
							formResponse.errors = responsePset.errors;
						}
					}
					break;
			}
		}

		/// <summary>
		/// Removes Psets from entity and returns them.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <returns>The Psets removed from the entity.</returns>
		private static List<Pset> RemovePsetsFromEntity(object entity) {
			var psets = new List<Pset>();
			var entityPset = entity as IPset;

			if (entityPset != null && entityPset.PropertySetList != null && entityPset.PropertySetList.Count > 0) {
				psets = entityPset.PropertySetList;
				// We don't want the regular entity persistance handling of psets since the source is a mapping,
				// and we have to perform some logic to validate the input.
				entityPset.PropertySetList = new List<Pset>();
			}
			return psets;
		}


		/// <summary>
		/// Executes Mapping for a create action.
		/// </summary>
		/// <param name="mappingRecord">The mapping record.</param>
		/// <param name="localId">The localid.</param>
		/// <param name="formResponse">The form response.</param>
		/// <returns></returns>
		/// <remarks>
		/// The method is declared as protected virtual because it could be overrided in subclass.
		/// </remarks>
		protected virtual TEntity Mapping_Create(MappingRecord<TEntity> mappingRecord, string localId, out FormResponse formResponse) {
			TEntity persistedEntity = null;

			if (!String.IsNullOrEmpty(localId)) {
				persistedEntity = GetItemByLocalId(localId);
			}

			if (persistedEntity != null) {
				return Mapping_Update(mappingRecord, localId, out formResponse);
			}

			PrepareEntityForImport(mappingRecord, null, localId, out formResponse);

			if (!formResponse.success) {
				return null;
			}
			formResponse = FormCreate(mappingRecord.Entity);
			TEntity entity = (TEntity)formResponse.data;
			return entity;
		}

		/// <summary>
		/// Executes Mapping for an update action.
		/// </summary>
		/// <param name="mappingRecord">The mapping record.</param>
		/// <param name="localId">The localid.</param>
		/// <param name="formResponse">The form response.</param>
		/// <returns></returns>
		/// <remarks>
		/// The method is declared as protected virtual because it could be overrided in subclass.
		/// For example in ClassificationItemRelationshipBroker we need to deactivate Mapping_Update.
		/// </remarks>
		protected virtual TEntity Mapping_Update(MappingRecord<TEntity> mappingRecord, string localId, out FormResponse formResponse) {
			TEntity persistedEntity = null;

			if (!String.IsNullOrEmpty(localId)) {
				persistedEntity = GetItemByLocalId(localId);
			}

			if (persistedEntity == null) {
				return Mapping_Create(mappingRecord, localId, out formResponse);
			}

			PrepareEntityForImport(mappingRecord, persistedEntity, localId, out formResponse);

			if (!formResponse.success) {
				return null;
			}

			TEntity entity = Update(mappingRecord.Entity, out formResponse);
			return entity;
		}

		/// <summary>
		/// Resolves the entity.
		/// </summary>
		/// <param name="mappingRecord">The mapping record.</param>
		/// <param name="persistedEntity">The persisted entity.</param>
		/// <param name="localId">The local id.</param>
		/// <param name="formResponse">The form response.</param>
		/// <returns></returns>
		protected void PrepareEntityForImport(MappingRecord<TEntity> mappingRecord, TEntity persistedEntity, string localId, out FormResponse formResponse) {
			formResponse = new FormResponse { success = true };

			try {
				ResolveReferencesForImport(mappingRecord, persistedEntity);
			}
			catch (ArgumentException exception) {
				// Handle a case a reference cannot be resolved
				formResponse.AddError(localId, exception.Message);
				formResponse.success = false;
				return;
			}

			// Copy values from persisted entity to imported entity
			if (persistedEntity != null) {
				PopulateMissingValues(mappingRecord, persistedEntity);
			}

			// Overwrite the Key property
			if (ShouldOverwriteKeyInMapping) {
				string newKey = null;
				if (persistedEntity != null) {
					newKey = persistedEntity.GetKey();
				}
				mappingRecord.Entity.SetKey(newKey);
			}

		}

		/// <summary>
		/// Gets a value indicating whether the Key property should be overwritten in the mapping proccess.
		/// This should be overridden only in classes in which the Key property is OK to be populated from the mapping.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if Key property should be overwritten; otherwise, <c>false</c>.
		/// </value>
		protected virtual bool ShouldOverwriteKeyInMapping {
			get { return true; }
		}

		/// <summary>
		/// Gets a value indicating whether this broker writes failures to the trace or not.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if this broker writes failures to the trace; otherwise, <c>false</c>.
		/// </value>
		public virtual bool WritesFailuresToTrace {
			get { return false; }
		}
		
		/// <summary>
		/// Populates the missing values in the entity by the existing values in the persisted entity.
		/// Overriding method implementations may assume that persistedEntity is not null, and should not assign the key
		/// as it will be assigned by the MappingEnabledBroker.
		/// </summary>
		/// <param name="mappingRecord">The mapping record.</param>
		/// <param name="persistedEntity">The persisted entity.</param>
		protected virtual void PopulateMissingValues(MappingRecord<TEntity> mappingRecord, TEntity persistedEntity) {

			List<PropertyInfo> allProperties = CacheService.MemoryCacheGetData(typeof(TEntity).FullName + "**cache") as List<PropertyInfo>;

			if (allProperties == null) {
				// Get only properties that can be both read & written to, and that were not assigned.
				allProperties = (from p in typeof(TEntity).GetProperties(BindingFlags.Instance | BindingFlags.FlattenHierarchy | BindingFlags.Public)
								 let isCopyable = (p.GetGetMethod() != null && p.GetSetMethod() != null)
								 where isCopyable
								 select p).ToList();

				CacheService.MemoryCacheAdd(typeof(TEntity).FullName + "**cache", allProperties);

			}

			// Get only properties that can be both read & written to, and that were not assigned.
			var propertiesToCopy = from p in allProperties
								   where  !mappingRecord.IsPropertyAssigned(p.Name)
								   select p;

			foreach (PropertyInfo property in propertiesToCopy) {
				object existingValue = property.GetValue(persistedEntity, null);
				property.SetValue(mappingRecord.Entity, existingValue, null);
			}

		}

		/// <summary>
		/// Executes Mapping for a delete action.
		/// </summary>
		/// <param name="mappingRecord">The mapping record.</param>
		/// <param name="localId">The localid.</param>
		/// <param name="formResponse">The form response.</param>
		/// <remarks>
		/// The method is declared as protected virtual because it could be overrided in subclass.
		/// For example in ClassificationItemRelationshipBroker we need to rewrite Mapping_Delete.
		/// </remarks>
		protected virtual void Mapping_Delete(MappingRecord<TEntity> mappingRecord, string localId, out FormResponse formResponse) {
			formResponse = new FormResponse();
			TEntity self = default(TEntity);
			if (!String.IsNullOrEmpty(localId))
				self = GetItemByLocalId(localId);
			if (self != null) {
				bool deleted = false;
				try {
					deleted = this.Delete(self);
				}
				catch (Exception ex) {
					formResponse.msg = ex.Message;
					formResponse.exception = ex;
				}
				formResponse.success = deleted;
			}
			else {
				formResponse.success = false;
				formResponse.msg = "Entity not found";
			}
		}

		/// <summary>
		/// Executes Mapping for the psets.
		/// </summary>
		/// <param name="entity">The entity.</param>
		private FormResponse Mapping_Pset(TEntity entity) {
			FormResponse formResponse = null;
			IPset entityPset = entity as IPset;
			if (entityPset != null && entityPset.PropertySetList != null && entityPset.PropertySetList.Count > 0) {
				if (this.ResolvePsets(entity)) {
					PsetBroker brokerPset = Helper.CreateInstance<PsetBroker>();
					List<Pset> result = brokerPset.Create(entityPset.PropertySetList, out formResponse);
					//((IPset)formResponse.data).PropertySetList = result;
				}
			}
			return formResponse;
		}

		/// <summary>
		/// Resolves the psets.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <returns>Returns true if at least one pset value was resolved, false otherwise.</returns>
		private bool ResolvePsets(TEntity entity) {
			// Mapping for the psets of the entity
			IPset psetInterface = entity as IPset;
			bool updated = false;
			if (psetInterface != null && psetInterface.PropertySetList != null && psetInterface.PropertySetList.Count > 0) {
				List<Pset> psets = psetInterface.PropertySetList;
				var broker = new PsetDefinitionBroker();
				List<PsetDefinition> lstPsetDefinition = broker.GetListByUsageName((string)entity.GetPropertyValue("IfcType"));
				if (lstPsetDefinition.Count <= 0) {
					return false;
				}

				// we create a empty new pset list
				List<Pset> newPsets;
				var query_newPsets = from p in lstPsetDefinition
									 select new Pset {
										 PsetName = p.PsetName,
										 KeyPset = p.KeyPropertySet,
										 KeyObject = (string)entity.GetPropertyValue(Helper.GetPropertyNameId(typeof(TEntity))),
										 Attributes = (from a in p.Attributes
													   select new PsetAttribute {
														   Key = a.AttributeName,
														   Value = null
													   }).ToList()
									 };
				newPsets = query_newPsets.ToList();

				// we filter the psets provided in the mapping document to exclude unfound psets
				var query_foundPset = from p in newPsets
									  join a in psets
										on
										p.PsetName equals a.PsetName
									  select new { Target = p, Source = a };

				foreach (var el in query_foundPset) {
					foreach (var attribute in el.Source.Attributes) {
						PsetAttribute att = el.Target.Attributes.FindLast(p => p.Key == attribute.Key);
						// in case the Key provided in the mapping document exists in the psetdefinition
						if (att != null) {
							// then we update the value
							att.Value = attribute.Value;
							updated = true;
						}
					}
				}
				if (updated)
					psetInterface.PropertySetList = newPsets;
			}
			return updated;
		}

	}
}

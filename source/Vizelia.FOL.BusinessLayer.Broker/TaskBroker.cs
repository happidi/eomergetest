﻿using System;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.DataLayer.BrokerDB;

namespace Vizelia.FOL.BusinessLayer.Broker {

	/// <summary>
	/// The broker for business entity Task
	/// </summary>
	public class TaskBroker : BaseBroker<Task, TaskBrokerDB> {
		/// <summary>
		/// Gets a list of Tasks by its ActionRequest key.
		/// </summary>
		/// <param name="keyActionRequest">The key of the ActionRequest.</param>
		/// <returns></returns>
		public virtual JsonStore<Task> GetListByKeyActionRequest(string keyActionRequest) {
			var retVal = ((TaskBrokerDB)m_brokerDB).GetListByKeyActionRequest(keyActionRequest).ToJsonStore();
			return retVal;
		}

		/// <summary>
		/// Gets a json store of Task for a specific ActionRequest
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyActionRequest">The Key of the ActionRequest.</param>
		/// <returns>A json store.</returns>
		public JsonStore<Task> GetStoreByKeyActionRequest(PagingParameter paging, string KeyActionRequest) {
			int total;
			return ((TaskBrokerDB)m_brokerDB).GetAllPagingByKeyActionRequest(paging, KeyActionRequest, out total).ToJsonStore(total, paging);
		}


		/// <summary>
		/// Gets a item by its instanceId.
		/// </summary>
		/// <param name="instanceId">The instanceId of the item.</param>
		/// <returns></returns>
		public virtual Task GetItemByInstanceId([NotNullValidator] Guid instanceId) {
			return ((TaskBrokerDB)m_brokerDB).GetItemByInstanceId(instanceId);
		}
	}

}

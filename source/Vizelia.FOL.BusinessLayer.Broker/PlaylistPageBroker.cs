﻿using System.Collections.Generic;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.DataLayer.BrokerDB;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for business entity PlaylistPage.
	/// </summary>
	public class PlaylistPageBroker : MappingEnabledBroker<PlaylistPage, PlaylistPageBrokerDB> {

		/// <summary>
		/// Get the list of saved PlaylistPage from a Playlist
		/// </summary>
		/// <param name="KeyPlaylist">The key of the Playlist.</param>
		public List<PlaylistPage> GetListByKeyPlaylist(string KeyPlaylist) {
			return ((PlaylistPageBrokerDB)m_brokerDB).GetListByKeyPlaylist(KeyPlaylist);
		}

		/// <summary>
		/// Gets a store of PlaylistPage for a specific Playlist.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyPlaylist">The Key of the Playlist.</param>
		/// <returns>A JsonStore.</returns>
		public JsonStore<PlaylistPage> GetStoreByKeyPlaylist(PagingParameter paging, string KeyPlaylist) {
			int total;
			return ((PlaylistPageBrokerDB)m_brokerDB).GetAllPagingByKeyPlaylist(paging, KeyPlaylist, out total).ToJsonStore(total, paging);
		}


		/// <summary>
		/// Saves the Playlist PlaylistPages.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="PlaylistPage">The PlaylistPage store.</param>
		/// <returns></returns>
		public List<PlaylistPage> SavePlaylistPlaylistPage(Playlist entity, CrudStore<PlaylistPage> PlaylistPage) {
			return ((PlaylistPageBrokerDB)m_brokerDB).SavePlaylistPlaylistPage(entity, PlaylistPage);
		}

		/// <summary>
		/// Resolves the references for export.
		/// </summary>
		/// <param name="entity">The entity.</param>
		protected override void ResolveReferencesForExport(PlaylistPage entity) {
			ResolveParentReferenceForExport<Playlist>(e => e.KeyPlaylist, entity);
			ResolveParentReferenceForExport<PortalTab>(e => e.KeyPortalTab, entity);
		}

		/// <summary>
		/// Resolves the references during mapping.
		/// </summary>
		/// <param name="mappingRecord">The mapping record.</param>
		/// <param name="persistedEntity">The persisted entity.</param>
		protected override void ResolveReferencesForImport(MappingRecord<PlaylistPage> mappingRecord, PlaylistPage persistedEntity) {
			ResolveParentReferenceForImport<Playlist>(e => e.KeyPlaylist, mappingRecord.Entity, persistedEntity);
			ResolveParentReferenceForImport<PortalTab>(e => e.KeyPortalTab, mappingRecord.Entity, persistedEntity);
		}
	}

}
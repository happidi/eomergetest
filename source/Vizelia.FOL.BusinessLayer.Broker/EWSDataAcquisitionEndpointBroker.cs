﻿using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.DataLayer.BrokerDB;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for business entity EWSDataAcquisitionEndpoint.
	/// </summary>
	public class EWSDataAcquisitionEndpointBroker : DataAcquisitionEndpointBroker<EWSDataAcquisitionEndpoint, EWSDataAcquisitionEndpointBrokerDB> {
		/// <summary>
		/// Resolves the references for import.
		/// </summary>
		/// <param name="mappingRecord">The mapping record.</param>
		/// <param name="persistedEntity">The persisted entity.</param>
		protected override void ResolveReferencesForImport(MappingRecord<EWSDataAcquisitionEndpoint> mappingRecord, EWSDataAcquisitionEndpoint persistedEntity) {
			ResolveParentReferenceForImport<EWSDataAcquisitionContainer>(x => x.KeyDataAcquisitionContainer, mappingRecord.Entity, persistedEntity);
		}

		/// <summary>
		/// Resolves the references for export.
		/// </summary>
		/// <param name="entity">The entity.</param>
		protected override void ResolveReferencesForExport(EWSDataAcquisitionEndpoint entity) {
			ResolveParentReferenceForExport<EWSDataAcquisitionContainer>(x => x.KeyDataAcquisitionContainer, entity);
		}
	}
}
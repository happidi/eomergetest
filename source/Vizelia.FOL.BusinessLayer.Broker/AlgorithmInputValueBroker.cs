﻿using System.Collections.Generic;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.DataLayer.BrokerDB;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for business entity AlgorithmInputValue.
	/// </summary>
	public class AlgorithmInputValueBroker : BaseBroker<AlgorithmInputValue, AlgorithmInputValueBrokerDB> {

		/// <summary>
		/// Get the list of saved AlgorithmInputValue from a Algorithm
		/// </summary>
		/// <param name="KeyChart">The key chart.</param>
		/// <param name="KeyAlgorithm">The key of the Algorithm.</param>
		/// <returns></returns>
		public List<AlgorithmInputValue> GetListByKeyChart(string KeyChart, string KeyAlgorithm) {
			return ((AlgorithmInputValueBrokerDB)m_brokerDB).GetListByKeyChart(KeyChart, KeyAlgorithm);
		}

		/// <summary>
		/// Gets a store of AlgorithmInputValue for a specific Algorithm.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyChart">The key chart.</param>
		/// <param name="KeyAlgorithm">The Key of the Algorithm.</param>
		/// <returns>
		/// A JsonStore.
		/// </returns>
		public JsonStore<AlgorithmInputValue> GetStoreByKeyChart(PagingParameter paging, string KeyChart, string KeyAlgorithm) {
			int total;
			return ((AlgorithmInputValueBrokerDB)m_brokerDB).GetAllPagingByKeyAlgorithm(paging, KeyChart, KeyAlgorithm, out total).ToJsonStore(total, paging);
		}


		/// <summary>
		/// Saves the Algorithm AlgorithmInputValues.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="values">The values.</param>
		/// <returns></returns>
		public List<AlgorithmInputValue> SaveChartAlgorithmAlgorithmInputValue(ChartAlgorithm entity, CrudStore<AlgorithmInputValue> values) {
			return ((AlgorithmInputValueBrokerDB)m_brokerDB).SaveChartAlgorithmAlgorithmInputValue(entity, values);
		}
	}

}
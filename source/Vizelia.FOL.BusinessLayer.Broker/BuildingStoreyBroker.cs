﻿using System;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.DataLayer.BrokerDB;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for business entity BuildingStorey.
	/// </summary>
	public class BuildingStoreyBroker : MappingEnabledBroker<BuildingStorey, BuildingStoreyBrokerDB> {
		/// <summary>
		/// Resolves the references for export.
		/// </summary>
		/// <param name="entity">The entity.</param>
		protected override void ResolveReferencesForExport(BuildingStorey entity) {
			ResolveParentReferenceForExport<Building>(e => e.KeyBuilding, entity);
		}

		/// <summary>
		/// Resolves the references during mapping.
		/// </summary>
		/// <param name="mappingRecord">The mapping record.</param>
		/// <param name="persistedEntity">The persisted entity.</param>
		protected override void ResolveReferencesForImport(MappingRecord<BuildingStorey> mappingRecord, BuildingStorey persistedEntity) {
			ResolveParentReferenceForImport<Building>(e => e.KeyBuilding, mappingRecord.Entity, persistedEntity);
		}
	}
}

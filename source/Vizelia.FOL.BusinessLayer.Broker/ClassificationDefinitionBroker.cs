﻿using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.DataLayer.BrokerDB;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for business entity ClassificationDefinition.
	/// </summary>
	public class ClassificationDefinitionBroker : MappingEnabledBroker<ClassificationDefinition, ClassificationDefinitionBrokerDB> {

		/// <summary>
		/// Public ctor.
		/// </summary>
		public ClassificationDefinitionBroker()
			: base() {
			// TODO : Uncomment this line to add new error definitions.
			// m_database_errors.Add(1, Langue.error_msg_uniquefield, "PsetName");
		}




		/// <summary>
		/// Creates the specified item.
		/// The overrides is necessary to update ClassificationHelper.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <param name="formResponse">The form response.</param>
		/// <returns></returns>
		protected override ClassificationDefinition Create(ClassificationDefinition item, out FormResponse formResponse) {
			ClassificationDefinition result = base.Create(item, out formResponse);
			ClassificationItemBroker.ClearClassificationItemDefinitionCache();
			return result;
		}

		/// <summary>
		/// Updates the specified item.
		/// The overrides is necessary to update ClassificationHelper.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <param name="formResponse">The form response.</param>
		/// <returns></returns>
		protected override ClassificationDefinition Update(ClassificationDefinition item, out FormResponse formResponse) {
			ClassificationDefinition result = base.Update(item, out formResponse);
			ClassificationItemBroker.ClearClassificationItemDefinitionCache();
			return result;
		}

		/// <summary>
		/// Deletes the specified item.
		/// The overrides is necessary to update ClassificationHelper.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <param name="persistedItem">The persisted item.</param>
		/// <returns></returns>
		public override bool Delete(ClassificationDefinition item, ClassificationDefinition persistedItem = null) {
			bool result = base.Delete(item, persistedItem);
			ClassificationItemBroker.ClearClassificationItemDefinitionCache();
			return result;
		}

		/// <summary>
		/// Resolves the references during mapping.
		/// </summary>
		/// <param name="mappingRecord">The mapping record.</param>
		/// <param name="persistedEntity">The persisted entity.</param>
		protected override void ResolveReferencesForImport(MappingRecord<ClassificationDefinition> mappingRecord, ClassificationDefinition persistedEntity) {
			// Do nothing.
		}

		/// <summary>
		/// Resolves the references for export.
		/// </summary>
		/// <param name="entity">The entity.</param>
		protected override void ResolveReferencesForExport(ClassificationDefinition entity) {
			// Do nothing.
		}

		/// <summary>
		/// Gets a value indicating whether the Key property should be overwritten in the mapping proccess.
		/// This should be overridden in classes in which the Key property is OK to be populated from the mapping.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if Key property should be overwritten; otherwise, <c>false</c>.
		/// </value>
		protected override bool ShouldOverwriteKeyInMapping {
			get { return false; }
		}

	}
}
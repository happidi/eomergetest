﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.DataLayer.BrokerDB;

namespace Vizelia.FOL.BusinessLayer.Broker {

	/// <summary>
	/// The broker for business entity MeterOperation.
	/// </summary>
	public class MeterOperationBroker : MappingEnabledBroker<MeterOperation, MeterOperationBrokerDB> {
		/// <summary>
		/// Get the list of saved MeterOperation from a Meter
		/// </summary>
		/// <param name="KeyMeter">The key of the meter.</param>
		public List<MeterOperation> GetListByKeyMeter(string KeyMeter) {
			return ((MeterOperationBrokerDB)m_brokerDB).GetListByKeyMeter(KeyMeter);
		}

		/// <summary>
		/// Gets a store of MeterOperation for a specific meter.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyMeter">The Key of the meter.</param>
		/// <param name="KeyMeterOperation">The key meter operation to exclude.</param>
		/// <param name="order">The order if the current meter operation in order to filter available results. 0 to disable filtering.</param>
		/// <returns>
		/// A JsonStore.
		/// </returns>
		public JsonStore<MeterOperation> GetStoreByKeyMeter(PagingParameter paging, string KeyMeter, string KeyMeterOperation, int order) {
			int total;
			return ((MeterOperationBrokerDB)m_brokerDB).GetAllPagingByKeyMeter(paging, KeyMeter, KeyMeterOperation, order, out total).ToJsonStore(total, paging);
		}


		/// <summary>
		/// Saves the meter MeterOperations.
		/// </summary>
		/// <param name="entity">The meter entity.</param>
		/// <param name="meterOperations">The meter operations.</param>
		/// <returns></returns>
		public List<MeterOperation> SaveMeterMeterOperations(Meter entity, CrudStore<MeterOperation> meterOperations) {
			return ((MeterOperationBrokerDB)m_brokerDB).SaveMeterMeterOperations(entity, meterOperations);
		}

		/// <summary>
		/// Resolves the references during mapping.
		/// </summary>
		/// <param name="mappingRecord">The mapping record.</param>
		/// <param name="persistedEntity">The persisted entity.</param>
		protected override void ResolveReferencesForImport(MappingRecord<MeterOperation> mappingRecord, MeterOperation persistedEntity) {
			ResolveParentReferenceForImport<Meter>(e => e.KeyMeter, mappingRecord.Entity, persistedEntity);
			ResolveParentReferenceForImport<Meter>(e => e.KeyMeter1, mappingRecord.Entity, persistedEntity);
			ResolveParentReferenceForImport<Meter>(e => e.KeyMeter2, mappingRecord.Entity, persistedEntity);

			ResolveParentReferenceForImport<MeterOperation>(e => e.KeyMeterOperation1, mappingRecord.Entity, persistedEntity);
			ResolveParentReferenceForImport<MeterOperation>(e => e.KeyMeterOperation2, mappingRecord.Entity, persistedEntity);
		}

		/// <summary>
		/// Resolves the references for export.
		/// </summary>
		/// <param name="entity">The entity.</param>
		protected override void ResolveReferencesForExport(MeterOperation entity) {
			ResolveParentReferenceForExport<Meter>(e => e.KeyMeter, entity);
			ResolveParentReferenceForExport<Meter>(e => e.KeyMeter1, entity);
			ResolveParentReferenceForExport<Meter>(e => e.KeyMeter2, entity);

			ResolveParentReferenceForExport<MeterOperation>(e => e.KeyMeterOperation1, entity);
			ResolveParentReferenceForExport<MeterOperation>(e => e.KeyMeterOperation2, entity);
		}
	}
}
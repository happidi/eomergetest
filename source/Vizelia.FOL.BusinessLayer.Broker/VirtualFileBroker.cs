﻿using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.DataLayer.BrokerDB;
using Vizelia.FOL.Common.Localization;
using System;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for business entity VirtualFile.
	/// </summary>
	public class VirtualFileBroker : MappingEnabledBroker<VirtualFile, VirtualFileBrokerDB> {

		/// <summary>
		/// Public ctor.
		/// </summary>
		public VirtualFileBroker() {
			m_database_errors.Add(1, Langue.error_msg_uniquefield, "Path");
		}

		/// <summary>
		/// Returns a VirtualFile from a Path.
		/// </summary>
		/// <param name="path">the path.</param>
		/// <returns></returns>
		public VirtualFile GetItemByPath(string path) {
			return ((VirtualFileBrokerDB)m_brokerDB).GetItemByPath(path);
		}

		/// <summary>
		/// Returns a VirtualFile from an Entity.
		/// </summary>
		/// <param name="KeyEntity">The key entity.</param>
		/// <param name="entityType">Type of the entity.</param>
		/// <returns></returns>
		public VirtualFile GetItemByKeyEntity(string KeyEntity, Type entityType) {
			return ((VirtualFileBrokerDB)m_brokerDB).GetItemByKeyEntity(KeyEntity, entityType);
		}


		/// <summary>
		/// Resolves the references for export.
		/// </summary>
		/// <param name="entity">The entity.</param>
		protected override void ResolveReferencesForExport(VirtualFile entity) {
			if (!string.IsNullOrWhiteSpace(entity.KeyEntity)) {
				switch (entity.EntityType) {
					case "Vizelia.FOL.BusinessEntities.Chart":
						ResolveParentReferenceForExport<Chart>(e => e.KeyEntity, entity);
						break;
					case "Vizelia.FOL.BusinessEntities.Meter":
						ResolveParentReferenceForExport<Meter>(e => e.KeyEntity, entity);
						break;
					case "Vizelia.FOL.BusinessEntities.Playlist":
						ResolveParentReferenceForExport<Playlist>(e => e.KeyEntity, entity);
						break;
				}
			}
		}

		/// <summary>
		/// Resolves the references during mapping.
		/// </summary>
		/// <param name="mappingRecord">The mapping record.</param>
		/// <param name="persistedEntity">The persisted entity.</param>
		protected override void ResolveReferencesForImport(MappingRecord<VirtualFile> mappingRecord, VirtualFile persistedEntity) {
			if (!string.IsNullOrWhiteSpace(mappingRecord.Entity.KeyEntity)) {
				switch (mappingRecord.Entity.EntityType) {
					case "Vizelia.FOL.BusinessEntities.Chart":
						ResolveParentReferenceForImport<Chart>(e => e.KeyEntity, mappingRecord.Entity, persistedEntity);
						break;
					case "Vizelia.FOL.BusinessEntities.Meter":
						ResolveParentReferenceForImport<Meter>(e => e.KeyEntity, mappingRecord.Entity, persistedEntity);
						break;
					case "Vizelia.FOL.BusinessEntities.Playlist":
						ResolveParentReferenceForImport<Playlist>(e => e.KeyEntity, mappingRecord.Entity, persistedEntity);
						break;
				}
			}
		}

	}

}

﻿using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.DataLayer.BrokerDB;


namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for business entity RESTDataAcquisitionContainer.
	/// </summary>
	public class RESTDataAcquisitionContainerBroker : MappingEnabledBroker<RESTDataAcquisitionContainer, RESTDataAcquisitionContainerBrokerDB> {
		/// <summary>
		/// Resolves the references for import.
		/// </summary>
		/// <param name="mappingRecord">The mapping record.</param>
		/// <param name="persistedEntity">The persisted entity.</param>
		protected override void ResolveReferencesForImport(MappingRecord<RESTDataAcquisitionContainer> mappingRecord, RESTDataAcquisitionContainer persistedEntity) {
			// Do nothing.
		}

		/// <summary>
		/// Resolves the references for export.
		/// </summary>
		/// <param name="entity">The entity.</param>
		protected override void ResolveReferencesForExport(RESTDataAcquisitionContainer entity) {
			// Do nothing.
		}
	}
}
﻿using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.DataLayer.BrokerDB;
using Vizelia.FOL.Common.Localization;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for business entity Occupant.
	/// </summary>
	public class OccupantBroker : MappingEnabledBroker<Occupant, OccupantBrokerDB> {
		/// <summary>
		/// Public ctor.
		/// </summary>
		public OccupantBroker() {
			// we change the m_database_errors to return an error on Id when duplicate are inserted.
			m_database_errors = new DatabaseErrors();
			m_database_errors.Add(100, Langue.error_msg_uniquefield, "Id");
		}

		/// <summary>
		/// Resolves the references for export.
		/// </summary>
		/// <param name="entity">The entity.</param>
		protected override void ResolveReferencesForExport(Occupant entity) {
			// Resolve reference for Organization
			ResolveParentReferenceForExport<Organization>(e => e.KeyOrganization, entity);
			ResolveParentReferenceForExport<ClassificationItem>(e => e.KeyClassificationItemOrganization, entity);
			ResolveParentReferenceForExport<ClassificationItem>(e => e.KeyClassificationItemWorktype, entity);

			// Resolve reference for Location
			if (entity.KeyLocation != null) {
				ResolveLocationReferenceForExport(entity);
			}
		}

		/// <summary>
		/// Resolves the references during mapping.
		/// </summary>
		/// <param name="mappingRecord">The mapping record.</param>
		/// <param name="persistedEntity">The persisted entity.</param>
		protected override void ResolveReferencesForImport(MappingRecord<Occupant> mappingRecord, Occupant persistedEntity) {
			// Resolve reference for Organization
			ResolveParentReferenceForImport<Organization>(e => e.KeyOrganization, mappingRecord.Entity, persistedEntity);
			ResolveParentReferenceForImport<ClassificationItem>(e => e.KeyClassificationItemOrganization, mappingRecord.Entity, persistedEntity);
			ResolveParentReferenceForImport<ClassificationItem>(e => e.KeyClassificationItemWorktype, mappingRecord.Entity, persistedEntity);

			// Resolve reference for Location
			if (mappingRecord.Entity.KeyLocation != null) {
				ResolveLocationReferenceForImport(mappingRecord, persistedEntity);
			}
		}

		/// <summary>
		/// Gets an item by its Actor.
		/// </summary>
		/// <param name="KeyActor">The key of the Actor.</param>
		/// <returns>The item.</returns>
		public Occupant GetItemByKeyActor(string KeyActor) {
			return ((OccupantBrokerDB)m_brokerDB).GetItemByKeyActor(KeyActor);
		}

	}

}

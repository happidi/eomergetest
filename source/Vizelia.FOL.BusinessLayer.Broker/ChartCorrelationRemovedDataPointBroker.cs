﻿using System.Collections.Generic;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.DataLayer.BrokerDB;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for business entity ChartCorrelationRemovedDataPoint.
	/// </summary>
	public class ChartCorrelationRemovedDataPointBroker : BaseBroker<ChartCorrelationRemovedDataPoint, ChartCorrelationRemovedDataPointBrokerDB> {

		/// <summary>
		/// Get the list of saved ChartCorrelationRemovedDataPoint from a Chart
		/// </summary>
		/// <param name="KeyChart">The key of the chart.</param>
		public List<ChartCorrelationRemovedDataPoint> GetListByKeyChart(string KeyChart) {
			return ((ChartCorrelationRemovedDataPointBrokerDB)m_brokerDB).GetListByKeyChart(KeyChart);
		}

		/// <summary>
		/// Gets a store of ChartCorrelationRemovedDataPoint for a specific chart.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyChart">The Key of the chart.</param>
		/// <returns>A JsonStore.</returns>
		public JsonStore<ChartCorrelationRemovedDataPoint> GetStoreByKeyChart(PagingParameter paging, string KeyChart) {
			int total;
			return ((ChartCorrelationRemovedDataPointBrokerDB)m_brokerDB).GetAllPagingByKeyChart(paging, KeyChart, out total).ToJsonStore(total, paging);
		}
	
	}

}
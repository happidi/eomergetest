using System.Collections.Generic;
using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// A DataAcquisitionEndpoint Broker interface.
	/// </summary>
	/// <typeparam name="TEndpoint">The type of the endpoint.</typeparam>
	public interface IDataAcquisitionEndpointBroker<TEndpoint> : IBroker<TEndpoint> where TEndpoint : DataAcquisitionEndpoint, new() {
		/// <summary>
		/// Get the list of saved TEndpoint from a container
		/// </summary>
		/// <param name="KeyDataAcquisitionContainer">The key data acquisition container.</param>
		/// <returns></returns>
		List<TEndpoint> GetListByKeyPlaylist(string KeyDataAcquisitionContainer);

		/// <summary>
		/// Gets a store of TEndpoint for a specific container.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyDataAcquisitionContainer">The key data acquisition container.</param>
		/// <returns>
		/// A JsonStore.
		/// </returns>
		JsonStore<TEndpoint> GetStoreByKeyDataAcquisitionContainer(PagingParameter paging, string KeyDataAcquisitionContainer);
	}
}
﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Transactions;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.DataLayer.BrokerDB;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for business entity UserProfilePreference
	/// </summary>
	public class UserProfilePreferenceBroker : BaseBroker<UserProfilePreference, UserProfilePreferenceBrokerDB> {
		/// <summary>
		/// Gets the store by key user.
		/// </summary>
		/// <param name="KeyUser">The key user.</param>
		/// <returns></returns>
		public List<UserProfilePreference> GetListByKeyUser(string KeyUser) {
			var retVal = ((UserProfilePreferenceBrokerDB)m_brokerDB).GetListByKeyUser(KeyUser);
			return retVal;
		}

		/// <summary>
		/// Deletes all data by user name.
		/// </summary>
		/// <param name="KeyUser">The Key user.</param>
		public bool DeleteAllDataByKeyUser(string KeyUser) {
			var retVal = ((UserProfilePreferenceBrokerDB)m_brokerDB).DeleteAllDataByKeyUser(KeyUser);
			return retVal;
		}
	}
}

﻿using System;
using System.Collections.Generic;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using Vizelia.FOL.DataLayer.BrokerDB;

namespace Vizelia.FOL.BusinessLayer.Broker {
    /// <summary>
    /// The broker for business entity Portal.
    /// </summary>
    public class PortalWindowBroker : MappingEnabledBroker<PortalWindow, PortalWindowBrokerDB> {
        /// <summary>
        /// Populate the chidren items specified as fields. "*" indicates that all fields should be populated.
        /// </summary>
        /// <param name="item">The item</param>
        /// <param name="loadMode">The load mode.</param>
        /// <param name="fields">The list of fields. "*" indicates all fields.</param>
        public override void PopulateList(PortalWindow item, EntityLoadMode loadMode, params string[] fields) {
            foreach (string field in fields) {
                if (field.ContainsAny("*", "Tabs", "GetPortal")) {
                    PortalTabBroker brokerTab = Helper.CreateInstance<PortalTabBroker>();
                    var tabs = brokerTab.GetListByKeyPortalWindow(item.KeyPortalWindow);
                    if (tabs != null && tabs.Count > 0) {
                        if (!field.Equals("GetPortal") || !item.PreloadTab) {
                            brokerTab.PopulateList(tabs, loadMode, field.Equals("GetPortal") ? new[] { "*" } : fields);
                        }
                        brokerTab.ResolveForExport(tabs, loadMode);
                        item.Tabs = tabs;
                    }
                }
            }
        }



        /// <summary>
        /// Copy an existing PortalWindow.
        /// </summary>
        /// <param name="item">the item to be copied.</param>
        /// <param name="formResponse">The response of the operation.</param>
        /// <param name="Creator">The creator.</param>
        /// <param name="incrementTitle">if set to <c>true</c> [increment title].</param>
        /// <returns></returns>
        public override PortalWindow Copy(PortalWindow item, out FormResponse formResponse, string Creator = null, bool incrementTitle = true) {
            if (incrementTitle)
                item.Title = ChartHelper.Increment(item.Title);

            var newItem = base.Copy(item, out formResponse, Creator, incrementTitle);
            //newItem.KeyPortalTemplateOrigin = null;
            newItem.Tabs = new List<PortalTab>();
            if (formResponse.success) {
                PortalTabBroker brokerTab = Helper.CreateInstance<PortalTabBroker>();
                foreach (var tab in item.Tabs) {
                    FormResponse response;
                    tab.KeyPortalWindow = newItem.KeyPortalWindow;
                    newItem.Tabs.Add(brokerTab.Copy(tab, out response, Creator, incrementTitle));
                }
            }
            return newItem;
        }

        /// <summary>
        /// Gets a list of PortalWindows for a specific ChartScheduler.
        /// </summary>
        /// <param name="KeyChartScheduler">The Key ChartScheduler.</param>
        /// <returns></returns>
        public virtual List<PortalWindow> GetListByKeyChartScheduler(string KeyChartScheduler) {
            return ((PortalWindowBrokerDB)m_brokerDB).GetListByKeyChartScheduler(KeyChartScheduler);
        }


        /// <summary>
        /// Saves the chartscheduler portalwindows.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="portalwindows">The portalwindows.</param>
        /// <returns></returns>
        public List<PortalWindow> SavePortalWindowChartScheduler(ChartScheduler entity, CrudStore<PortalWindow> portalwindows) {
            return ((PortalWindowBrokerDB)m_brokerDB).SavePortalWindowChartScheduler(entity, portalwindows);
        }


        /// <summary>
        /// Gets a store of portalwindows for a specific chartscheduler.
        /// </summary>
        /// <param name="paging">The paging parameters.</param>
        /// <param name="KeyChartScheduler">The Key of the chartscheduler.</param>
        /// <returns>A JsonStore.</returns>
        public JsonStore<PortalWindow> GetStoreByKeyChartScheduler(PagingParameter paging, string KeyChartScheduler) {
            int total;
            return ((PortalWindowBrokerDB)m_brokerDB).GetAllPagingByKeyChartScheduler(paging, KeyChartScheduler, out total).ToJsonStore(total, paging);
        }


        /// <summary>
        /// Gets a list of PortalWindows for a specific PortalTemplate.
        /// </summary>
        /// <param name="KeyPortalTemplate">The Key PortalTemplate.</param>
        /// <returns></returns>
        public virtual List<PortalWindow> GetListByKeyPortalTemplate(string KeyPortalTemplate) {
            return ((PortalWindowBrokerDB)m_brokerDB).GetListByKeyPortalTemplate(KeyPortalTemplate);
        }


        /// <summary>
        /// Saves the PortalTemplate portalwindows.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="portalwindows">The portalwindows.</param>
        /// <returns></returns>
        public List<PortalWindow> SavePortalWindowPortalTemplate(PortalTemplate entity, CrudStore<PortalWindow> portalwindows) {
            return ((PortalWindowBrokerDB)m_brokerDB).SavePortalWindowPortalTemplate(entity, portalwindows);
        }


        /// <summary>
        /// Gets a store of portalwindows for a specific PortalTemplate.
        /// </summary>
        /// <param name="paging">The paging parameters.</param>
        /// <param name="KeyPortalTemplate">The Key of the PortalTemplate.</param>
        /// <returns>A JsonStore.</returns>
        public JsonStore<PortalWindow> GetStoreByKeyPortalTemplate(PagingParameter paging, string KeyPortalTemplate) {
            return ((PortalWindowBrokerDB)m_brokerDB).GetListByKeyPortalTemplate(KeyPortalTemplate).ToJsonStoreWithFilter(paging);
        }

        /// <summary>
        /// Resolves the references for export.
        /// </summary>
        /// <param name="entity">The entity.</param>
        protected override void ResolveReferencesForExport(PortalWindow entity) {
            ResolveParentReferenceForExport<FOLMembershipUser>(e => e.Creator, entity);
            ResolveParentReferenceForExport<DynamicDisplayImage>(e => e.KeyImage, entity);
        }


        /// <summary>
        /// Resolves the references during mapping.
        /// </summary>
        /// <param name="mappingRecord">The mapping record.</param>
        /// <param name="persistedEntity">The persisted entity.</param>
        protected override void ResolveReferencesForImport(MappingRecord<PortalWindow> mappingRecord, PortalWindow persistedEntity) {
            ResolveParentReferenceForImport<FOLMembershipUser>(e => e.Creator, mappingRecord.Entity, persistedEntity);
            ResolveParentReferenceForImport<DynamicDisplayImage>(e => e.KeyImage, mappingRecord.Entity, persistedEntity);
        }


        /// <summary>
        /// Gets a list of PortalWindows for a specific Creator (user).
        /// </summary>
        /// <param name="Creator">The Key of the User.</param>
        /// <returns></returns>
        public List<PortalWindow> GetListByKeyCreator(string Creator) {
            return ((PortalWindowBrokerDB)m_brokerDB).GetListByKeyCreator(Creator);
        }

        /// <summary>
        /// Updates the date modified.
        /// </summary>
        /// <param name="Portal">The portal.</param>
        /// <param name="DateTimeLastModified">The date time last modified.</param>
        public void UpdateDateTimeModified(PortalWindow Portal, DateTime? DateTimeLastModified) {
            m_PortalWindowBrokerDB.UpdateDateTimeModified(Portal.KeyPortalWindow, DateTimeLastModified);
        }
    }

}
﻿using System.Collections.Generic;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.DataLayer.BrokerDB;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for business entity PaletteColor.
	/// </summary>
	public class PaletteColorBroker : MappingEnabledBroker<PaletteColor, PaletteColorBrokerDB> {

		/// <summary>
		/// Get the list of saved PaletteColor from a Palette
		/// </summary>
		/// <param name="KeyPalette">The key of the Palette.</param>
		public List<PaletteColor> GetListByKeyPalette(string KeyPalette) {
			return ((PaletteColorBrokerDB)m_brokerDB).GetListByKeyPalette(KeyPalette);
		}

		/// <summary>
		/// Gets a store of PaletteColor for a specific Palette.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyPalette">The Key of the Palette.</param>
		/// <returns>A JsonStore.</returns>
		public JsonStore<PaletteColor> GetStoreByKeyPalette(PagingParameter paging, string KeyPalette) {
			int total;
			return ((PaletteColorBrokerDB)m_brokerDB).GetAllPagingByKeyPalette(paging, KeyPalette, out total).ToJsonStore(total, paging);
		}


		/// <summary>
		/// Saves the Portal PaletteColor.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="paletteColor">The PaletteColor store.</param>
		/// <returns></returns>
		public List<PaletteColor> SavePalettePaletteColor(Palette entity, CrudStore<PaletteColor> paletteColor) {
			return ((PaletteColorBrokerDB)m_brokerDB).SavePalettePaletteColor(entity, paletteColor);
		}

		/// <summary>
		/// Resolves the references for export.
		/// </summary>
		/// <param name="entity">The entity.</param>
		protected override void ResolveReferencesForExport(PaletteColor entity) {
			ResolveParentReferenceForExport<Palette>(e => e.KeyPalette, entity);
		}

		/// <summary>
		/// Resolves the references during mapping.
		/// </summary>
		/// <param name="mappingRecord">The mapping record.</param>
		/// <param name="persistedEntity">The persisted entity.</param>
		protected override void ResolveReferencesForImport(MappingRecord<PaletteColor> mappingRecord, PaletteColor persistedEntity) {
			ResolveParentReferenceForImport<Palette>(e => e.KeyPalette, mappingRecord.Entity, persistedEntity);
		}
	}

}
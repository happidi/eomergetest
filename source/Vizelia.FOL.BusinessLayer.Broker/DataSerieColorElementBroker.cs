﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.DataLayer.BrokerDB;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Localization;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for business entity DataSerieColorElement.
	/// </summary>
	public class DataSerieColorElementBroker : MappingEnabledBroker<DataSerieColorElement, DataSerieColorElementBrokerDB> {
		/// <summary>
		/// Get the list of saved DataSerieColorElement from a DataSerie
		/// </summary>
		/// <param name="KeyDataSerie">The key of the DataSerie.</param>
		public List<DataSerieColorElement> GetListByKeyDataSerie(string KeyDataSerie) {
			return ((DataSerieColorElementBrokerDB)m_brokerDB).GetListByKeyDataSerie(KeyDataSerie);
		}

		/// <summary>
		/// Gets a store of DataSerieColorElement for a specific DataSerie.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyDataSerie">The Key of the DataSerie.</param>
		/// <returns>A JsonStore.</returns>
		public JsonStore<DataSerieColorElement> GetStoreByKeyDataSerie(PagingParameter paging, string KeyDataSerie) {
			int total;
			return ((DataSerieColorElementBrokerDB)m_brokerDB).GetAllPagingByKeyDataSerie(paging, KeyDataSerie, out total).ToJsonStore(total, paging);
		}


		/// <summary>
		/// Saves the DataSerie DataSerieColorElement.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="colorelements">The DataSerieColorElement store.</param>
		/// <returns></returns>
		public List<DataSerieColorElement> SaveDataSerieDataSerieColorElement(DataSerie entity, CrudStore<DataSerieColorElement> colorelements) {
			return ((DataSerieColorElementBrokerDB)m_brokerDB).SaveDataSerieDataSerieColorElement(entity, colorelements);
		}

		/// <summary>
		/// Resolves the references for import.
		/// </summary>
		/// <param name="mappingRecord">The mapping record.</param>
		/// <param name="persistedEntity">The persisted entity.</param>
		protected override void ResolveReferencesForImport(BusinessEntities.Mapping.MappingRecord<DataSerieColorElement> mappingRecord, DataSerieColorElement persistedEntity) {
			ResolveParentReferenceForImport<DataSerie>(e => e.KeyDataSerie, mappingRecord.Entity, persistedEntity);
		}

		/// <summary>
		/// Resolves the references for export.
		/// </summary>
		/// <param name="entity">The entity.</param>
		protected override void ResolveReferencesForExport(DataSerieColorElement entity) {
			ResolveParentReferenceForExport<DataSerie>(e => e.KeyDataSerie, entity);
		}
	}
}
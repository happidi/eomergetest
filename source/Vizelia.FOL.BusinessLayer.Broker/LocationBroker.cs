﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using Microsoft.Practices.EnterpriseLibrary.Validation.PolicyInjection;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;
using Vizelia.FOL.DataLayer.BrokerDB;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for business entity Location.
	/// </summary>
	public class LocationBroker : MappingEnabledBroker<Location, LocationBrokerDB> {

		/// <summary>
		/// Gets a dictionnary for all the Locations, by KeyLocation. 
		/// </summary>
		/// <returns></returns>
		public Dictionary<string, Location> GetDictionary() {
			var list = ((LocationBrokerDB)m_brokerDB).GetAll();
			var retVal = list.ToDictionary(x => x.KeyLocation);
			return retVal;
		}

		/// <summary>
		/// Gets a store of filter spatial for a specific chart.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="keyChart">The Key of the chart.</param>
		/// <returns>
		/// A JsonStore.
		/// </returns>
		public JsonStore<Location> GetFilterSpatialStoreByKeyChart(PagingParameter paging, string keyChart) {
			int total;
			return ((LocationBrokerDB)m_brokerDB).GetFilterSpatialAllPagingByKeyChart(paging, keyChart, out total).ToJsonStore(total, paging);
		}

		/// <summary>
		/// Gets a store of filter spatial for a specific AlarmDefinition.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="keyAlarmDefinition">The Key of the AlarmDefinition.</param>
		/// <returns>A JsonStore.</returns>
		public JsonStore<Location> GetFilterSpatialStoreByKeyAlarmDefinition(PagingParameter paging, string keyAlarmDefinition) {
			int total;
			return ((LocationBrokerDB)m_brokerDB).GetFilterSpatialAllPagingByKeyAlarmDefinition(paging, keyAlarmDefinition, out total).ToJsonStore(total, paging);
		}

		/// <summary>
		/// Gets a store of filter spatial for a specific MeterValidationRule.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="keyMeterValidationRule">The Key of the MeterValidationRule.</param>
		/// <returns>A JsonStore.</returns>
		public JsonStore<Location> GetFilterSpatialStoreByKeyMeterValidationRule(PagingParameter paging, string keyMeterValidationRule) {
			int total;
			return ((LocationBrokerDB)m_brokerDB).GetFilterSpatialAllPagingByKeyMeterValidationRule(paging, keyMeterValidationRule, out total).ToJsonStore(total, paging);
		}

		/// <summary>
		/// Gets a store of filter spatial for a specific AlarmTable.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="keyAlarmTable">The Key of the AlarmTable.</param>
		/// <returns>A JsonStore.</returns>
		public JsonStore<Location> GetFilterSpatialStoreByKeyAlarmTable(PagingParameter paging, string keyAlarmTable) {
			int total;
			return ((LocationBrokerDB)m_brokerDB).GetFilterSpatialAllPagingByKeyAlarmTable(paging, keyAlarmTable, out total).ToJsonStore(total, paging);
		}

		/// <summary>
		/// Gets a store of filter spatial for a specific PortalTemplate.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="keyPortalTemplate">The key portal template.</param>
		/// <returns>
		/// A JsonStore.
		/// </returns>
		public JsonStore<Location> GetFilterSpatialStoreByKeyPortalTemplate(PagingParameter paging, string keyPortalTemplate) {
			int total;
			return ((LocationBrokerDB)m_brokerDB).GetFilterSpatialAllPagingByKeyPortalTemplate(paging, keyPortalTemplate, out total).ToJsonStore(total, paging);
		}

		/// <summary>
		/// Gets a store of filter spatial for a specific chart.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="keyChart">The Key of the chart.</param>
		/// <returns>A JsonStore.</returns>
		public JsonStore<Location> GetFilterSpatialPsetAllPagingByKeyChart(PagingParameter paging, string keyChart) {
			int total;
			return ((LocationBrokerDB)m_brokerDB).GetFilterSpatialPsetAllPagingByKeyChart(paging, keyChart, out total).ToJsonStore(total, paging);
		}

		/// <summary>
		/// Gets a store of filter spatial for a specific AlarmDefinition.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="keyAlarmDefinition">The Key of the AlarmDefinition.</param>
		/// <returns>A JsonStore.</returns>
		public JsonStore<Location> GetFilterSpatialPsetAllPagingByKeyAlarmDefinition(PagingParameter paging, string keyAlarmDefinition) {
			int total;
			return ((LocationBrokerDB)m_brokerDB).GetFilterSpatialPsetAllPagingByKeyAlarmDefinition(paging, keyAlarmDefinition, out total).ToJsonStore(total, paging);
		}

		/// <summary>
		/// Gets a json store of locations for a specific mail
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="keyMail">The Key of the mail.</param>
		/// <returns>A json store.</returns>
		public JsonStore<Location> GetStoreByKeyMail(PagingParameter paging, string keyMail) {
			int total;
			return ((LocationBrokerDB)m_brokerDB).GetAllPagingByKeyMail(paging, keyMail, out total).ToJsonStore(total, paging);
		}

		/// <summary>
		/// Gets a json store of locations for a specific key word
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="keyword">The keyword.</param>
		/// <returns>
		/// A json store.
		/// </returns>
		public JsonStore<Location> GetAllPagingByKeyword(PagingParameter paging, string keyword) {
			int total;
			return ((LocationBrokerDB)m_brokerDB).GetAllPagingByKeyword(paging, keyword, out total).ToJsonStore(total, paging);
		}


		/// <summary>
		/// Resolves the references for export.
		/// </summary>
		/// <param name="entity">The entity.</param>
		protected override void ResolveReferencesForExport(Location entity) {
			// Do nothing.
		}

		/// <summary>
		/// Resolves the references during mapping.
		/// </summary>
		/// <param name="mappingRecord">The mapping record.</param>
		/// <param name="persistedEntity">The persisted entity.</param>
		protected override void ResolveReferencesForImport(MappingRecord<Location> mappingRecord, Location persistedEntity) {
			// Do nothing.
		}

		/// <summary>
		/// Saves the chart filter spatial.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="filterSpatial">The filter spatial.</param>
		/// <returns></returns>
		public List<Location> SaveChartFilterSpatial(Chart entity, CrudStore<Location> filterSpatial) {
			return ((LocationBrokerDB)m_brokerDB).SaveChartFilterSpatial(entity, filterSpatial);
		}

		/// <summary>
		/// Saves the AlarmDefinition filter spatial.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="filterSpatial">The filter spatial.</param>
		/// <returns></returns>
		public List<Location> SaveAlarmDefinitionFilterSpatial(AlarmDefinition entity, CrudStore<Location> filterSpatial) {
			return ((LocationBrokerDB)m_brokerDB).SaveAlarmDefinitionFilterSpatial(entity, filterSpatial);
		}

		/// <summary>
		/// Saves the AlarmDefinition filter spatial.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="filterSpatial">The filter spatial.</param>
		/// <returns></returns>
		public List<Location> SaveMeterValidationRuleFilterSpatial(MeterValidationRule entity, CrudStore<Location> filterSpatial) {
			return ((LocationBrokerDB)m_brokerDB).SaveMeterValidationRuleFilterSpatial(entity, filterSpatial);
		}

		/// <summary>
		/// Saves the chart filter spatial.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="filterSpatial">The filter spatial.</param>
		/// <returns></returns>
		public List<Location> SavePortalTemplateFilterSpatial(PortalTemplate entity, CrudStore<Location> filterSpatial) {
			return ((LocationBrokerDB)m_brokerDB).SavePortalTemplateFilterSpatial(entity, filterSpatial);
		}

		/// <summary>
		/// Saves the Alarmtable filter spatial.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="filterSpatial">The filter spatial.</param>
		/// <returns></returns>
		public List<Location> SaveAlarmTableFilterSpatial(AlarmTable entity, CrudStore<Location> filterSpatial) {
			return ((LocationBrokerDB)m_brokerDB).SaveAlarmTableFilterSpatial(entity, filterSpatial);
		}

		/// <summary>
		/// Saves the mail locations.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="locations">The locations.</param>
		/// <returns></returns>
		public List<Location> SaveMailLocations(Mail entity, CrudStore<Location> locations) {
			return ((LocationBrokerDB)m_brokerDB).SaveMailLocations(entity, locations);
		}

		/// <summary>
		/// Gets the item by local id.
		/// </summary>
		/// <param name="localId">The local id.</param>
		/// <returns></returns>
		public override Location GetItemByLocalId(string localId) {
			// The Location broker is only used as part of doing a one-to-many or many-to-many association.

			// Separate the local ID and Location type.
			MatchCollection matches = Regex.Matches(localId, "^(?<type>.*):(?<id>.*)$", RegexOptions.RightToLeft);

			if (matches.Count != 1 || !matches[0].Success || matches[0].Groups.Count != 3) {
				throw new ArgumentException("localId cannot be parsed as a location entity in format 'HierarchySpatialTypeName:LocalId'.");
			}

			string type = matches[0].Groups["type"].Value;
			string id = matches[0].Groups["id"].Value;
			var locationType = (HierarchySpatialTypeName)Enum.Parse(typeof(HierarchySpatialTypeName), type);
			Location location = GetLocationByLocalId(locationType, id);

			return location;
		}

		/// <summary>
		/// Gets the location by local id.
		/// </summary>
		/// <param name="locationType">Type of the location.</param>
		/// <param name="id">The id.</param>
		/// <returns></returns>
		private Location GetLocationByLocalId(HierarchySpatialTypeName locationType, string id) {
			Location item;

			switch (locationType) {
				case HierarchySpatialTypeName.IfcSite:
					item = GetLocationByLocalId<Site>(id);
					break;
				case HierarchySpatialTypeName.IfcBuilding:
					item = GetLocationByLocalId<Building>(id);
					break;
				case HierarchySpatialTypeName.IfcBuildingStorey:
					item = GetLocationByLocalId<BuildingStorey>(id);
					break;
				case HierarchySpatialTypeName.IfcSpace:
					item = GetLocationByLocalId<Space>(id);
					break;
				case HierarchySpatialTypeName.IfcFurniture:
					item = GetLocationByLocalId<Furniture>(id);
					break;
				case HierarchySpatialTypeName.IfcMeter:
				case HierarchySpatialTypeName.None:
					throw new ArgumentException("Disallowed location type " + locationType);
				default:
					throw new ArgumentOutOfRangeException("locationType");
			}

			return item;
		}

		/// <summary>
		/// Gets the location by local id.
		/// </summary>
		/// <typeparam name="TLocationEntity">The type of the location entity.</typeparam>
		/// <param name="localId">The local id.</param>
		/// <returns></returns>
		private Location GetLocationByLocalId<TLocationEntity>(string localId) where TLocationEntity : BaseBusinessEntity, ILocation {
			IBroker<TLocationEntity> broker = BrokerFactory.CreateBroker<TLocationEntity>();
			TLocationEntity entity = broker.GetItemByLocalId(localId);
			
			Location location = entity != null ? entity.GetLocation() : null;

			return location;
		}

		/// <summary>
		/// Get the list of Locations from a Map
		/// </summary>
		/// <param name="keyMap">The key of the Map.</param>
		public List<Location> GetListByKeyMap(string keyMap) {
			return ((LocationBrokerDB)m_brokerDB).GetListByKeyMap(keyMap);
		}

		/// <summary>
		/// Creates the specified item.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <param name="formResponse">The form response.</param>
		/// <returns></returns>
		/// <exception cref="System.ArgumentException">Disallowed location type  + item.TypeName</exception>
		/// <exception cref="System.ArgumentOutOfRangeException">item.TypeName</exception>
		protected override Location Create(Location item, out FormResponse formResponse) {
			ILocation location = null;
			switch (item.TypeName) {
				case HierarchySpatialTypeName.IfcSite:
					var siteBroker = Helper.CreateInstance<SiteBroker>();
					formResponse = siteBroker.FormCreate(new Site(item));
					location = (Site)formResponse.data;
					break;
				case HierarchySpatialTypeName.IfcBuilding:
					var buildingBroker = Helper.CreateInstance<BuildingBroker>();
					formResponse = buildingBroker.FormCreate(new Building(item));
					location = (Building)formResponse.data;
					break;
				case HierarchySpatialTypeName.IfcBuildingStorey:
					var buildingStoreyBroker = Helper.CreateInstance<BuildingStoreyBroker>();
					formResponse = buildingStoreyBroker.FormCreate(new BuildingStorey(item));
					location = (BuildingStorey)formResponse.data;
					break;
				case HierarchySpatialTypeName.IfcSpace:
					var spaceBroker = Helper.CreateInstance<SpaceBroker>();
					formResponse = spaceBroker.FormCreate(new Space(item));
					location = (Space)formResponse.data;
					break;
				case HierarchySpatialTypeName.IfcFurniture:
					var furnitureBroker = Helper.CreateInstance<FurnitureBroker>();
					formResponse = furnitureBroker.FormCreate(new Furniture(item));
					location = (Furniture)formResponse.data;
					break;
				case HierarchySpatialTypeName.IfcMeter:
				// NOTE: Used fieldname of "LocationType" instead of "TypeName" because the response is sent externally where LocationType exists and TypeName does not
					formResponse = InvalidLocationTypeErrorFormResponse("LocationType", String.Empty, "Disallowed location type");
					break;
				case HierarchySpatialTypeName.None:
					formResponse = InvalidLocationTypeErrorFormResponse("LocationType", String.Empty);
					break;
				default:
					formResponse = InvalidLocationTypeErrorFormResponse("LocationType", String.Empty);
					break;
			}
	
			formResponse.data = location != null ? location.GetLocation() : null;
			return formResponse.data as Location;
		}

		/// <summary>
		/// Updates the specified item.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <param name="formResponse">The form response.</param>
		/// <returns></returns>
		/// <exception cref="System.ArgumentException">Disallowed location type  + item.TypeName</exception>
		/// <exception cref="System.ArgumentOutOfRangeException">item.TypeName</exception>
		protected override Location Update(Location item, out FormResponse formResponse) {
			ILocation location = null;
			switch (item.TypeName) {
				case HierarchySpatialTypeName.IfcSite:
					var siteBroker = Helper.CreateInstance<SiteBroker>();
					formResponse = siteBroker.FormUpdate(new Site(item));
					location = (Site)formResponse.data;
					break;
				case HierarchySpatialTypeName.IfcBuilding:
					var buildingBroker = Helper.CreateInstance<BuildingBroker>();
					formResponse = buildingBroker.FormUpdate(new Building(item));
					location = (Building)formResponse.data;
					break;
				case HierarchySpatialTypeName.IfcBuildingStorey:
					var buildingStoreyBroker = Helper.CreateInstance<BuildingStoreyBroker>();
					formResponse = buildingStoreyBroker.FormUpdate(new BuildingStorey(item));
					location = (BuildingStorey)formResponse.data;
					break;
				case HierarchySpatialTypeName.IfcSpace:
					var spaceBroker = Helper.CreateInstance<SpaceBroker>();
					formResponse = spaceBroker.FormUpdate(new Space(item));
					location = (Space)formResponse.data;
					break;
				case HierarchySpatialTypeName.IfcFurniture:
					var furnitureBroker = Helper.CreateInstance<FurnitureBroker>();
					formResponse = furnitureBroker.FormUpdate(new Furniture(item));
					location = (Furniture)formResponse.data;
					break;
				case HierarchySpatialTypeName.IfcMeter:
					// NOTE: Used fieldname of "LocationType" instead of "TypeName" because the response is sent externally where LocationType exists and TypeName does not
					formResponse = InvalidLocationTypeErrorFormResponse("LocationType", item.KeyLocation, "Disallowed location type");
					break;
				case HierarchySpatialTypeName.None:
					formResponse = InvalidLocationTypeErrorFormResponse("LocationType", item.KeyLocation);
					break;
				default:
					formResponse = InvalidLocationTypeErrorFormResponse("LocationType", item.KeyLocation);
					break;
			}

			formResponse.data = location != null ? location.GetLocation() : null;
			return formResponse.data as Location;
		}

		/// <summary>
		/// Deletes the specified item.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <returns></returns>
		/// <exception cref="System.ArgumentException">Disallowed location type  + item.TypeName</exception>
		/// <exception cref="System.ArgumentOutOfRangeException">item.TypeName</exception>
		public override bool Delete(string key) {
			bool retVal;
			Location item = GetItem(key);
			switch (item.TypeName) {
				case HierarchySpatialTypeName.IfcSite:
					var siteBroker = Helper.CreateInstance<SiteBroker>();
					retVal = siteBroker.Delete(new Site(item));
					break;
				case HierarchySpatialTypeName.IfcBuilding:
					var buildingBroker = Helper.CreateInstance<BuildingBroker>();
					retVal = buildingBroker.Delete(new Building(item));
					break;
				case HierarchySpatialTypeName.IfcBuildingStorey:
					var buildingStoreyBroker = Helper.CreateInstance<BuildingStoreyBroker>();
					retVal = buildingStoreyBroker.Delete(new BuildingStorey(item));
					break;
				case HierarchySpatialTypeName.IfcSpace:
					var spaceBroker = Helper.CreateInstance<SpaceBroker>();
					retVal = spaceBroker.Delete(new Space(item));
					break;
				case HierarchySpatialTypeName.IfcFurniture:
					var furnitureBroker = Helper.CreateInstance<FurnitureBroker>();
					retVal = furnitureBroker.Delete(new Furniture(item));
					break;
				case HierarchySpatialTypeName.IfcMeter:
					throw InvalidLocationTypeError("LocationType", key, "Disallowed location type");
				case HierarchySpatialTypeName.None:
					throw InvalidLocationTypeError("LocationType", key);
				//					throw new ArgumentException("Disallowed location type " + item.TypeName);
				default:
					throw InvalidLocationTypeError("LocationType", key);
			}
			return retVal;
		}

		/// <summary>
		/// Use when checking LocationType or TypeName for an invalid value.
		/// Throws a validation exception to allow OData developers to see
		/// it as a validation error (403) instead of a server error (500).
		/// </summary>
		/// <param name="fieldname">The name of the element being validated.</param>
		/// <param name="key">The primary key for the Location</param>
		/// <param name="altMessage">When not null, this is the actual error message shown.</param>
		protected ArgumentValidationException InvalidLocationTypeError(string fieldname, string key, string altMessage = null) {
			// this normally happens in OData when the user forgot to assign LocationType
			ValidationResult valResult = new ValidationResult(
				altMessage ?? String.Format("{0} must be assigned.", fieldname), // not localized because its a programmer's error when this happens
				null, fieldname, key ?? String.Empty, new VizStringLengthValidator(1, 100));	// the actual validator does not matter as much as it the ValidationResult requires it
			ValidationResults valResults = new ValidationResults();
			valResults.AddResult(valResult);
			return new ArgumentValidationException(valResults, key);

		}
		/// <summary>
		/// Use when checking LocationType or TypeName for an invalid value.
		/// Creates a FormResponse with a validation exception to allow OData developers to see
		/// it as a validation error (403) instead of a server error (500).
		/// </summary>
		/// <param name="fieldname">The name of the element being validated.</param>
		/// <param name="key">The primary key for the Location</param>
		/// <param name="altMessage">When not null, this is the actual error message shown.</param>
		protected FormResponse InvalidLocationTypeErrorFormResponse(string fieldname, string key, string altMessage = null) {
			ArgumentValidationException exception = InvalidLocationTypeError(fieldname, key, altMessage);

			FormResponse formResponse = new FormResponse();
			formResponse.success = false;
			formResponse.exception = exception;
			var query = from r in exception.ValidationResults
						group r by r.Key into g
						select new FormError { id = g.Key, msg = g.First().Message };

			formResponse.errors = query.ToList();
			return formResponse;
		}

		/// <summary>
		/// Populates the list.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <param name="loadMode">The load mode.</param>
		/// <param name="fields">The fields.</param>
		public override void PopulateList(Location item, EntityLoadMode loadMode, params string[] fields) {
			foreach (var field in fields) {
				if (field.ContainsAny("Children")) {
					int total;
					var pagingParameter = new PagingParameter {
						filters = new List<GridFilter>
						{
							new GridFilter
							{
								field = "KeyParent",
								data = new GridData
								{
									comparison = "eq",
									type = "string",
									value = item.KeyLocation
								}
							}
						}
					};
					var locations = this.GetAll(pagingParameter, PagingLocation.Database, out total);
					this.ResolveForExport(locations, loadMode);
					item.Children = locations;
				}
			}
		}

		/// <summary>
		/// Gets all the locations according to list of local id's.
		/// </summary>
		/// <param name="localIds"></param>
		/// <returns></returns>
		public List<Location> GetListByLocalIds(string[] localIds) {
			return ((LocationBrokerDB) m_brokerDB).GetListByLocalIds(localIds);
		}
	}

}

﻿using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using Vizelia.FOL.DataLayer.BrokerDB;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for business entity EnergyCertificate.
	/// </summary>
	public class EnergyCertificateBroker : MappingEnabledBroker<EnergyCertificate, EnergyCertificateBrokerDB> {
		/// <summary>
		/// Populate the chidren items specified as fields. "*" indicates that all fields should be populated.
		/// </summary>
		/// <param name="item">The item</param>
		/// <param name="loadMode">The load mode.</param>
		/// <param name="fields">The list of fields. "*" indicates all fields.</param>
		public override void PopulateList(EnergyCertificate item, EntityLoadMode loadMode, params string[] fields) {
			foreach (string field in fields) {
				if (field.ContainsAny("*", "Categories")) {
					var brokerCat = Helper.CreateInstance<EnergyCertificateCategoryBroker>();
					item.Categories = brokerCat.GetListByKeyEnergyCertificate(item.KeyEnergyCertificate);
				}
			}
		}

		/// <summary>
		/// Copies the specified item.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <param name="formResponse">The form response.</param>
		/// <param name="Creator">The creator.</param>
		/// <param name="incrementTitle">if set to <c>true</c> [increment title].</param>
		/// <returns></returns>
		public override EnergyCertificate Copy(EnergyCertificate item, out FormResponse formResponse, string Creator = null, bool incrementTitle = true) {
			var brokerCat = Helper.CreateInstance<EnergyCertificateCategoryBroker>();
			var categories = brokerCat.GetListByKeyEnergyCertificate(item.KeyEnergyCertificate);

			if (incrementTitle)
				item.Name = ChartHelper.Increment(item.Name);

			var newItem = base.Copy(item, out formResponse, Creator, incrementTitle);
			if (categories != null && categories.Count > 0) {
				categories.ForEach(cat => {
					cat.KeyEnergyCertificateCategory = null;
					cat.KeyEnergyCertificate = newItem.KeyEnergyCertificate;
					brokerCat.Save(cat);
				});
			}
			return newItem;
		}

		/// <summary>
		/// Resolves the references for import.
		/// </summary>
		/// <param name="mappingRecord">The mapping record.</param>
		/// <param name="persistedEntity">The persisted entity.</param>
		protected override void ResolveReferencesForImport(MappingRecord<EnergyCertificate> mappingRecord, EnergyCertificate persistedEntity) {
			//Do nothing.
		}

		/// <summary>
		/// Resolves the references for export.
		/// </summary>
		/// <param name="entity">The entity.</param>
		protected override void ResolveReferencesForExport(EnergyCertificate entity) {
			//Do nothing.
		}
	}

}
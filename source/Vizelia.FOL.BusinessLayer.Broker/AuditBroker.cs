﻿using System.Diagnostics;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.DataLayer.BrokerDB;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for Audit entity.
	/// </summary>
	public class AuditBroker : BaseBroker<Audit, AuditBrokerDB> {
		/// <summary>
		/// Gets the audit store by key.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="location">The location.</param>
		/// <param name="entityName">The name.</param>
		/// <returns></returns>
		public JsonStore<Audit> GetAuditStore(PagingParameter paging, PagingLocation location, string entityName) {
			int total;
			entityName = entityName.Split('.')[0];
			var type = typeof(BaseBusinessEntity);
			Debug.Assert(type.FullName != null, "type.FullName != null");
			var columnLangueDefinitionList = ((AuditBrokerDB)m_brokerDB).LoadPropertiesTree(type.Assembly.GetType(type.FullName.Replace(type.Name, entityName)));
			var auditList = m_brokerDB.GetAll(paging, location, out total);
			var retVal = auditList.ToAuditJsonStore(total, paging, columnLangueDefinitionList);
			return retVal;
		}
	}
}
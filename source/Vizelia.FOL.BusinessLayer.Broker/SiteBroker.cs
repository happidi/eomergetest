﻿using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.DataLayer.BrokerDB;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for business entity Site.
	/// </summary>
	public class SiteBroker : MappingEnabledBroker<Site, SiteBrokerDB> {

		/// <summary>
		/// Get the max level of site in the Spatial Hierarchy.
		/// </summary>
		/// <returns></returns>
		public int GetMaxLevel() {
			return ((SiteBrokerDB)m_brokerDB).GetMaxLevel();
		}

		/// <summary>
		/// Resolves the references for export.
		/// </summary>
		/// <param name="entity">The entity.</param>
		protected override void ResolveReferencesForExport(Site entity) {
			ResolveParentReferenceForExport<Site>("KeySiteParent", entity);
		}

		/// <summary>
		/// Resolves the references during mapping.
		/// </summary>
		/// <param name="mappingRecord">The mapping record.</param>
		/// <param name="persistedEntity">The persisted entity.</param>
		protected override void ResolveReferencesForImport(MappingRecord<Site> mappingRecord, Site persistedEntity) {
			ResolveParentReferenceForImport<Site>("KeySiteParent", mappingRecord.Entity, persistedEntity);
		}

	}

}
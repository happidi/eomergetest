﻿using System;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.DataLayer.BrokerDB;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for business entity Furniture.
	/// </summary>
	public class FurnitureBroker : MappingEnabledBroker<Furniture, FurnitureBrokerDB> {
		/// <summary>
		/// Resolves the references for export.
		/// </summary>
		/// <param name="entity">The entity.</param>
		protected override void ResolveReferencesForExport(Furniture entity) {
            ResolveParentReferenceForExport<Space>(e => e.KeyLocation, entity);
            ResolveParentReferenceForExport<ClassificationItem>(e => e.KeyClassificationItem, entity);
		}

		/// <summary>
		/// Resolves the references during mapping.
		/// </summary>
		/// <param name="mappingRecord">The mapping record.</param>
		/// <param name="persistedEntity">The persisted entity.</param>
		protected override void ResolveReferencesForImport(MappingRecord<Furniture> mappingRecord, Furniture persistedEntity) {
			ResolveLocationReferenceForImport(mappingRecord, persistedEntity);
            ResolveParentReferenceForImport<ClassificationItem>(e => e.KeyClassificationItem, mappingRecord.Entity, persistedEntity);
		}
	}
}

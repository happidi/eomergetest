﻿using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Vizelia.FOL.DataLayer.BrokerDB;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for business entity Logo.
	/// </summary>
	public class LogoBroker : BaseBroker<Logo, LogoBrokerDB> {
		/// <summary>
		/// Populate the chidren items specified as fields. "*" indicates that all fields should be populated.
		/// </summary>
		/// <param name="item">The item</param>
		/// <param name="loadMode">The load mode.</param>
		/// <param name="fields">The list of fields. "*" indicates all fields.</param>
		public override void PopulateList(Logo item, EntityLoadMode loadMode, params string[] fields) {
			foreach (string field in fields) {
				if (field.ContainsAny("*", "Image")) {
					if (item != null && !string.IsNullOrEmpty(item.KeyImage)) {
						DocumentBroker brokerDocument = Helper.CreateInstance<DocumentBroker>();
						item.Image = brokerDocument.GetItem(item.KeyImage.Trim());
					}
				}
			}
		}

		/// <summary>
		/// Updates the documents.
		/// </summary>
		/// <param name="item">The item.</param>
		protected override void UpdateDocuments(Logo item) {
			if (!string.IsNullOrWhiteSpace(item.KeyImage)) {
				if (item.KeyImage.StartsWith("document_")) {
					CleanDocuments(item);
					SaveDocuments(item);
				}
				else {
					item.KeyImage = m_brokerDB.GetItem(item.KeyLogo).KeyImage;
				}
			}
		}
	}
}


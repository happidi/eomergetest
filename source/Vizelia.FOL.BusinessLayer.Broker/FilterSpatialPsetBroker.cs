﻿using System.Collections.Generic;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.DataLayer.BrokerDB;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for business entity FilterSpatialPset.
	/// </summary>
	public class FilterSpatialPsetBroker : MappingEnabledBroker<FilterSpatialPset, FilterSpatialPsetBrokerDB> {

		/// <summary>
		/// Get the list of saved FilterSpatialPset from a Chart
		/// </summary>
		/// <param name="KeyChart">The key of the chart.</param>
		public List<FilterSpatialPset> GetListByKeyChart(string KeyChart) {
			return ((FilterSpatialPsetBrokerDB)m_brokerDB).GetListByKeyChart(KeyChart);
		}

		/// <summary>
		/// Gets a store of FilterSpatialPset for a specific chart.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyChart">The Key of the chart.</param>
		/// <returns>A JsonStore.</returns>
		public JsonStore<FilterSpatialPset> GetStoreByKeyChart(PagingParameter paging, string KeyChart) {
			int total;
			return ((FilterSpatialPsetBrokerDB)m_brokerDB).GetAllPagingByKeyChart(paging, KeyChart, out total).ToJsonStore(total, paging);
		}


		/// <summary>
		/// Saves the chart FilterSpatialPset.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="psetFilters">The FilterSpatialPset store.</param>
		/// <returns></returns>
		public List<FilterSpatialPset> SaveChartFilterSpatialPset(Chart entity, CrudStore<FilterSpatialPset> psetFilters) {
			return ((FilterSpatialPsetBrokerDB)m_brokerDB).SaveChartFilterSpatialPset(entity, psetFilters);
		}

		/// <summary>
		/// Get the list of saved FilterSpatialPset from a AlarmDefinition
		/// </summary>
		/// <param name="KeyAlarmDefinition">The key of the AlarmDefinition.</param>
		public List<FilterSpatialPset> GetListByKeyAlarmDefinition(string KeyAlarmDefinition) {
			return ((FilterSpatialPsetBrokerDB)m_brokerDB).GetListByKeyAlarmDefinition(KeyAlarmDefinition);
		}

		/// <summary>
		/// Gets a store of FilterSpatialPset for a specific AlarmDefinition.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyAlarmDefinition">The Key of the AlarmDefinition.</param>
		/// <returns>A JsonStore.</returns>
		public JsonStore<FilterSpatialPset> GetStoreByKeyAlarmDefinition(PagingParameter paging, string KeyAlarmDefinition) {
			int total;
			return ((FilterSpatialPsetBrokerDB)m_brokerDB).GetAllPagingByKeyAlarmDefinition(paging, KeyAlarmDefinition, out total).ToJsonStore(total, paging);
		}


		/// <summary>
		/// Saves the AlarmDefinition FilterSpatialPset.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="psetFilters">The FilterSpatialPset store.</param>
		/// <returns></returns>
		public List<FilterSpatialPset> SaveAlarmDefinitionFilterSpatialPset(AlarmDefinition entity, CrudStore<FilterSpatialPset> psetFilters) {
			return ((FilterSpatialPsetBrokerDB)m_brokerDB).SaveAlarmDefinitionFilterSpatialPset(entity, psetFilters);
		}

		/// <summary>
		/// Resolves the references during mapping.
		/// </summary>
		/// <param name="mappingRecord">The mapping record.</param>
		/// <param name="persistedEntity">The persisted entity.</param>
		protected override void ResolveReferencesForImport(MappingRecord<FilterSpatialPset> mappingRecord, FilterSpatialPset persistedEntity) {
			ResolveParentReferenceForImport<AlarmDefinition>(e => e.KeyAlarmDefinition, mappingRecord.Entity, persistedEntity);
			ResolveParentReferenceForImport<Chart>(e => e.KeyChart, mappingRecord.Entity, persistedEntity);
		}

		/// <summary>
		/// Resolves the references for export.
		/// </summary>
		/// <param name="entity">The entity.</param>
		protected override void ResolveReferencesForExport(FilterSpatialPset entity) {
			ResolveParentReferenceForExport<AlarmDefinition>(e => e.KeyAlarmDefinition, entity);
			ResolveParentReferenceForExport<Chart>(e => e.KeyChart, entity);
		}
	}

}
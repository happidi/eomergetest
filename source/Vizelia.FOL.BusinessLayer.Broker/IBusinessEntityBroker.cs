using Vizelia.FOL.BusinessEntities;
using System.Collections.Generic;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// A broker of a business entity
	/// </summary>
	public interface IBusinessEntityBroker {

		/// <summary>
		/// Copy an item in the database
		/// </summary>
		/// <param name="item">The item to copy.
		/// The use of "this" inside this function will not use PI. So we must pass instead the PI proxy from the outside.</param>
		/// <param name="formResponse">The form response.</param>
		/// <param name="Creator">The creator.</param>
		/// <param name="incrementTitle">if set to <c>true</c> [increment title].</param>
		/// <returns></returns>
		BaseBusinessEntity Copy(BaseBusinessEntity item, out FormResponse formResponse, string Creator = null, bool incrementTitle = true);

		/// <summary>
		/// Deletes an item from database.
		/// </summary>
		/// <param name="Key">The Key of the item to delete.</param>
		/// <returns>
		/// True if the delete operation succeeded, false otherwise.
		/// </returns>
		bool Delete(string Key);

		/// <summary>
		/// Gets all items without paging, filtering or ordering.
		/// </summary>
		/// <returns>A list of items</returns>
		List<BaseBusinessEntity> GetAll();

		/// <summary>
		/// Gets all items.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="total">The total of results.</param>
		/// <returns>A list of items</returns>
		List<BaseBusinessEntity> GetAll(PagingParameter paging, PagingLocation location, out int total);

		/// <summary>
		/// Gets a item.
		/// </summary>
		/// <param name="Key">The Key of the item.</param>
		/// <returns>The item.</returns>
		BaseBusinessEntity GetItem(string Key);

		/// <summary>
		/// Gets a item.
		/// </summary>
		/// <param name="Key">The Key of the item.</param>
		/// <param name="fields">the list of field to fetch. "*" to fetch all children fields.</param>
		/// <returns>The item.</returns>
		BaseBusinessEntity GetItem(string Key, params string[] fields);

		/// <summary>
		/// Saves the specified item.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <returns></returns>
		BaseBusinessEntity Save(BaseBusinessEntity item);

		/// <summary>
		/// Saves the specified item, returning the entity and the formResponse.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <param name="formResponse">Output the results. Check it for errors.</param>
		/// <returns>The entity or null when there was an error. Check formResponse for details.</returns>
		BaseBusinessEntity Save(BaseBusinessEntity item, out FormResponse formResponse);

	}
}

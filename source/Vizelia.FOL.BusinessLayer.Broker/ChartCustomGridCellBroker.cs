﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using Vizelia.FOL.DataLayer.BrokerDB;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for business entity ChartCustomGridCell.
	/// </summary>
	public class ChartCustomGridCellBroker : BaseBroker<ChartCustomGridCell, ChartCustomGridCellBrokerDB> {

		/// <summary>
		/// Get the list of saved ChartCustomGridCell from a Chart
		/// </summary>
		/// <param name="KeyChart">The key of the chart.</param>
		public List<ChartCustomGridCell> GetListByKeyChart(string KeyChart) {
			return ((ChartCustomGridCellBrokerDB)m_brokerDB).GetListByKeyChart(KeyChart);
		}

		/// <summary>
		/// Get the list of saved ChartCustomGridCell from a Chart
		/// </summary>
		/// <param name="chart">The chart.</param>
		/// <returns></returns>
		public List<ChartCustomGridCell> GetPopulatedListChart(Chart chart) {
			var list = GetListByKeyChart(chart.KeyChart);

			foreach (var cell in list) {
				cell.Value = cell.Tag;
			}
			
			var templates = (from cell in list where cell.Tag.Contains("@") select cell.Tag);
			var templatesList = TemplateService.ParseMany(templates, chart, "ChartTemplateBase");
			foreach(var cell in list) {
				if (cell.Tag.Contains("@")) {
					cell.Value = templatesList.First();
					templatesList.RemoveAt(0);
				}
				if (cell.Value.Contains("%")) {
					cell.Value = Helper.LocalizeInText(cell.Value, "%");
				}

			// At this point, we have HTML styles that may contain colors in the format '#rrggbb'
			// such as color='#000000' and style='background-color:#FFFFFF'.
			// The # character is also used as a delimiter in FormulaEvaluatorService.EvaluateText
			// so long as there are two of them to enclose the token to evaluate.
			// We'll temporarily strip out the # characters belonging to #rrggbb.

				if (cell.Value.Contains("#")) {
					// expressions: 
					// ='#rrggbb'
					// ="#rrggbb"
					// =#rrggbb>
					// = #rrggbb[space] 
					// = #rrggbb>
					// plus all of the above replace = with :
					// :#rrggbb;
					// : #rrggbb;
					// Rare case: #rgb (3 char format) is not supported.
					Regex rgbRegex = new Regex(@"[=\:]\s*['""]?#[A-Fa-f0-9]{6}['""\s\>;]");
					MatchEvaluator matchEval = (match) => match.Value.Replace("#", "{PLACEHOLDER}");
					string textToChange = rgbRegex.Replace(cell.Value, matchEval);

					textToChange = FormulaEvaluatorService.EvaluateText(textToChange, '#', chart.NumericFormat, null, new ChartFormulaEvaluatorContext(chart));

					cell.Value = textToChange.Replace("{PLACEHOLDER}", "#");
				}

			}

			return list;
		}

		/// <summary>
		/// Gets a store of ChartCustomGridCell for a specific chart.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyChart">The Key of the chart.</param>
		/// <returns>A JsonStore.</returns>
		public JsonStore<ChartCustomGridCell> GetStoreByKeyChart(PagingParameter paging, string KeyChart) {
			int total;
			return ((ChartCustomGridCellBrokerDB)m_brokerDB).GetAllPagingByKeyChart(paging, KeyChart, out total).ToJsonStore(total, paging);
		}


		/// <summary>
		/// Saves the chart ChartCustomGridCell.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="chartcustomgridcell">The chartcustomgridcell.</param>
		/// <returns></returns>
		public List<ChartCustomGridCell> SaveChartChartCustomGridCell(Chart entity, CrudStore<ChartCustomGridCell> chartcustomgridcell) {
			return ((ChartCustomGridCellBrokerDB)m_brokerDB).SaveChartChartCustomGridCell(entity, chartcustomgridcell);
		}


	}

}
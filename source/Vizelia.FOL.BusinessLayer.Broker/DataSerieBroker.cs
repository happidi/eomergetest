﻿using System.Collections.Generic;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Localization;
using Vizelia.FOL.DataLayer.BrokerDB;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for business entity DataSerie.
	/// </summary>
	public class DataSerieBroker : MappingEnabledBroker<DataSerie, DataSerieBrokerDB> {

		/// <summary>
		/// Initializes a new instance of the <see cref="DataSerieBroker"/> class.
		/// </summary>
		public DataSerieBroker() {
			m_database_errors.Add(1, Langue.error_msg_uniquefield, "KeyClassificationItem");
			m_database_errors.Add(2, Langue.error_msg_uniquefield, "LocalId");
		}
		/// <summary>
		/// Get the list of saved DataSerie from a Chart
		/// </summary>
		/// <param name="KeyChart">The key of the chart.</param>
		public List<DataSerie> GetListByKeyChart(string KeyChart) {
			return ((DataSerieBrokerDB)m_brokerDB).GetListByKeyChart(KeyChart);
		}

		/// <summary>
		/// Gets a store of dataserie for a specific chart.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyChart">The Key of the chart.</param>
		/// <returns>A JsonStore.</returns>
		public JsonStore<DataSerie> GetStoreByKeyChart(PagingParameter paging, string KeyChart) {
			int total;
			return ((DataSerieBrokerDB)m_brokerDB).GetAllPagingByKeyChart(paging, KeyChart, out total).ToJsonStore(total, paging);
		}


		/// <summary>
		/// Saves the chart dataseries.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="dataseries">The dataseries.</param>
		/// <returns></returns>
		public List<DataSerie> SaveChartDataSeries(Chart entity, CrudStore<DataSerie> dataseries) {
			return ((DataSerieBrokerDB)m_brokerDB).SaveChartDataSeries(entity, dataseries);
		}

		/// <summary>
		/// Resolves the references for export.
		/// </summary>
		/// <param name="entity">The entity.</param>
		protected override void ResolveReferencesForExport(DataSerie entity) {
			ResolveParentReferenceForExport<Chart>(e => e.KeyChart, entity);
			ResolveParentReferenceForExport<ChartAxis>(e => e.KeyYAxis, entity);
			ResolveParentReferenceForExport<ClassificationItem>(e => e.KeyClassificationItem, entity);
		}

		/// <summary>
		/// Resolves the references during mapping.
		/// </summary>
		/// <param name="mappingRecord">The mapping record.</param>
		/// <param name="persistedEntity">The persisted entity.</param>
		protected override void ResolveReferencesForImport(MappingRecord<DataSerie> mappingRecord, DataSerie persistedEntity) {
			ResolveParentReferenceForImport<Chart>(e => e.KeyChart, mappingRecord.Entity, persistedEntity);
			ResolveParentReferenceForImport<ChartAxis>(e => e.KeyYAxis, mappingRecord.Entity, persistedEntity);
			ResolveParentReferenceForImport<ClassificationItem>(e => e.KeyClassificationItem, mappingRecord.Entity, persistedEntity);
		}

		/// <summary>
		/// Get or Create a DataSerie based on the Chart and the LocalId. If the DataSerie is created, it s not saved yet.
		/// </summary>
		/// <param name="KeyChart">The key chart.</param>
		/// <param name="KeyDataSerie">The key data serie.</param>
		/// <param name="localId">The local id of the dataserie.</param>
		/// <param name="serieName">Name of the serie as optional parameter. If not suplied it will default to the localId</param>
		/// <returns></returns>
		public DataSerie GetOrCreate(string KeyChart, string KeyDataSerie, string localId, string serieName = null) {
			DataSerie serie = null;
			if (!string.IsNullOrWhiteSpace(KeyDataSerie)) {
				serie = GetItem(KeyDataSerie);
			}
			return serie ?? (new DataSerie {
				KeyChart = KeyChart,
				LocalId = localId,
				Name = string.IsNullOrWhiteSpace(serieName) ? localId: serieName,
			});
		}

		/// <summary>
		/// Populate the chidren items specified as fields. "*" indicates that all fields should be populated.
		/// </summary>
		/// <param name="item">The item</param>
		/// <param name="loadMode">The load mode.</param>
		/// <param name="fields">The list of fields. "*" indicates all fields.</param>
		public override void PopulateList(DataSerie item, EntityLoadMode loadMode, params string[] fields) {
			foreach (string field in fields) {
				if (field.ContainsAny("*", "PsetRatios")) {
					var brokerPsetRatio = Helper.CreateInstance<DataSeriePsetRatioBroker>();
					item.PsetRatios = brokerPsetRatio.GetListByKeyDataSerie(item.KeyDataSerie);
				}
				if (field.ContainsAny("*", "ColorElements")) {
					var brokerColorElement = Helper.CreateInstance<DataSerieColorElementBroker>();
					item.ColorElements = brokerColorElement.GetListByKeyDataSerie(item.KeyDataSerie);
				}
			}
		}


		/// <summary>
		/// Populate the chidren items specified as fields. "*" indicates that all fields should be populated.
		/// </summary>
		/// <param name="item">The item</param>
		/// <param name="formResponse">The form response.</param>
		/// <param name="Creator">The creator.</param>
		/// <param name="incrementTitle">if set to <c>true</c> [increment title].</param>
		/// <returns></returns>
		public override DataSerie Copy(DataSerie item, out FormResponse formResponse, string Creator = null, bool incrementTitle = true) {

			var brokerPsetRatio = Helper.CreateInstance<DataSeriePsetRatioBroker>();
			var psetRatios = brokerPsetRatio.GetListByKeyDataSerie(item.KeyDataSerie);

			var brokerColorElement = Helper.CreateInstance<DataSerieColorElementBroker>();
			var colorElements = brokerColorElement.GetListByKeyDataSerie(item.KeyDataSerie);

			var serieLocalId = item.LocalId;
			var newItem = base.Copy(item, out formResponse, Creator);

			//we need to fix the LocalId of the DataSerie because it s set to null in the base.Copy and so a GUID is generated.
			newItem.LocalId = serieLocalId;
			this.Save(newItem);


			if (psetRatios != null && psetRatios.Count > 0) {
				psetRatios.ForEach(psetRatio => {
					psetRatio.LocalId = null;
					psetRatio.KeyDataSerie = newItem.KeyDataSerie;
					psetRatio.KeyDataSeriePsetRatio = null;
					brokerPsetRatio.Save(psetRatio);
				});
			}

			if (colorElements != null && colorElements.Count > 0) {
				colorElements.ForEach(colorElement => {
					colorElement.KeyDataSerie = newItem.KeyDataSerie;
					colorElement.KeyDataSerieColorElement = null;
					brokerColorElement.Save(colorElement);
				});
			}
			return newItem;

		}

		/// <summary>
		/// Returns whether the Audit the should return changed fields even if not should be audited.
		/// </summary>
		/// <returns></returns>
		protected override bool ShouldReturnAuditChangedFields() {
			return true;
		}

	}

}
﻿using System.Collections.Generic;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.DataLayer.BrokerDB;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for business entity StatisticalSerie.
	/// </summary>
	public class StatisticalSerieBroker : MappingEnabledBroker<StatisticalSerie, StatisticalSerieBrokerDB> {

		/// <summary>
		/// Get the list of saved StatisticalSerie from a Chart
		/// </summary>
		/// <param name="KeyChart">The key of the chart.</param>
		public List<StatisticalSerie> GetListByKeyChart(string KeyChart) {
			return ((StatisticalSerieBrokerDB)m_brokerDB).GetListByKeyChart(KeyChart);
		}

		/// <summary>
		/// Gets a store of StatisticalSerie for a specific chart.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyChart">The Key of the chart.</param>
		/// <returns>A JsonStore.</returns>
		public JsonStore<StatisticalSerie> GetStoreByKeyChart(PagingParameter paging, string KeyChart) {
			int total;
			return ((StatisticalSerieBrokerDB)m_brokerDB).GetAllPagingByKeyChart(paging, KeyChart, out total).ToJsonStore(total, paging);
		}


		/// <summary>
		/// Saves the chart StatisticalSeries.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="StatisticalSeries">The StatisticalSeries.</param>
		/// <returns></returns>
		public List<StatisticalSerie> SaveChartStatisticalSeries(Chart entity, CrudStore<StatisticalSerie> StatisticalSeries) {
			return ((StatisticalSerieBrokerDB)m_brokerDB).SaveChartStatisticalSeries(entity, StatisticalSeries);
		}

		/// <summary>
		/// Resolves the references for export.
		/// </summary>
		/// <param name="entity">The entity.</param>
		protected override void ResolveReferencesForExport(StatisticalSerie entity) {
			ResolveParentReferenceForExport<Chart>(e => e.KeyChart, entity);
			ResolveParentReferenceForExport<ClassificationItem>(e => e.KeyClassificationItem, entity);
		}

		/// <summary>
		/// Resolves the references during mapping.
		/// </summary>
		/// <param name="mappingRecord">The mapping record.</param>
		/// <param name="persistedEntity">The persisted entity.</param>
		protected override void ResolveReferencesForImport(MappingRecord<StatisticalSerie> mappingRecord, StatisticalSerie persistedEntity) {
			ResolveParentReferenceForImport<Chart>(e => e.KeyChart, mappingRecord.Entity, persistedEntity);
			ResolveParentReferenceForImport<ClassificationItem>(e => e.KeyClassificationItem, mappingRecord.Entity, persistedEntity);
		}

		/// <summary>
		/// Populate the chidren items specified as fields. "*" indicates that all fields should be populated.
		/// </summary>
		/// <param name="item">The item</param>
		/// <param name="formResponse">The form response.</param>
		/// <param name="Creator">The creator.</param>
		/// <param name="incrementTitle">if set to <c>true</c> [increment title].</param>
		/// <returns></returns>
		public override StatisticalSerie Copy(StatisticalSerie item, out FormResponse formResponse, string Creator = null, bool incrementTitle = true) {

			var serieLocalId = item.LocalId;
			var newItem = base.Copy(item, out formResponse, Creator);

			//we need to fix the LocalId of the StatisticalSerie because it's set to null in the base.Copy and so a GUID is generated.
			newItem.LocalId = serieLocalId;
			this.Save(newItem);
			return newItem;

		}
	}

}
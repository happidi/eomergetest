﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessLayer.Broker;
using Vizelia.FOL.Common;
using System.Data;

namespace Vizelia.FOL.BusinessLayer.Interfaces {
	/// <summary>
	/// Extension for pset.
	/// </summary>
	public static class BaseBusinessExtensions {
		/// <summary>
		/// Populates the PropertySet attribute of a list of business entities with the specified pset name.
		/// </summary>
		/// <typeparam name="TSource">The type of business entity.</typeparam>
		/// <param name="results">The list of business entities</param>
		/// <returns>The list of business entites with attribute PropertySet populated.</returns>
		public static List<TSource> PopulatePropertySet<TSource>(this List<TSource> results) where TSource : BaseBusinessEntity, IPset {
			PsetBroker broker = Helper.CreateInstance<PsetBroker>();
			// The name of the Key attribute of the business entity.
			string propertyNameId = Helper.GetPropertyNameId(typeof(TSource));
			var keys = from item in results
					   select (string)item.GetPropertyValue(propertyNameId);

			List<Pset> attributes = broker.GetList(keys.ToArray());

			var query = from item in results
						select new {
							item = item,
							Attributes = (from attribute in attributes
										  where attribute.KeyObject == (string)item.GetPropertyValue(propertyNameId)
										  select attribute).ToList()
						};

			foreach (var p in query) {
				p.item.PropertySetList = p.Attributes;
			}
			return results;
		}

		/// <summary>
		/// Populates the list of attributes definition for a given list of PsetDefinition.
		/// </summary>
		/// <param name="results">a list of PsetDefinition</param>
		/// <returns>The returned list of PsetDefinition populated with attributes definition.</returns>
		public static List<PsetDefinition> PopulateChildren(this List<PsetDefinition> results) {
			PsetAttributeDefinitionBroker broker = Helper.CreateInstance<PsetAttributeDefinitionBroker>();
			string propertyNameId = Helper.GetPropertyNameId(typeof(PsetDefinition));
			var keys = from item in results
					   select (string)item.GetPropertyValue(propertyNameId);
			List<PsetAttributeDefinition> attributes = broker.GetListFromParents(keys.ToArray());

			var query = from item in results
						select new {
							item = item,
							Attributes = (from attribute in attributes
										  where attribute.KeyPropertySet == item.KeyPropertySet
										  select attribute).ToList()
						};

			foreach (var p in query) {
				p.item.Attributes = p.Attributes;
			}
			return results;
		}





	}
}

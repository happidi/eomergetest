﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web.Security;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Helpers;
using Vizelia.FOL.Common.Localization;
using Vizelia.FOL.DataLayer.BrokerDB;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for FOLMembershipUser.
	/// This class is not built as others broker because it just need to implement IMappingBroker as we are using the MembershipProvider instead of a custom entity broker.
	/// </summary>
	public class FOLMembershipUserBroker : MappingEnabledBroker<FOLMembershipUser, FOLMembershipUserBrokerDB> {
		private readonly IFOLRoleProvider m_ProviderRole;
		private readonly IFOLSqlMembershipProvider m_ProviderMembership;

		/// <summary>
		/// Public ctor.
		/// </summary>
		public FOLMembershipUserBroker() {
			m_brokerDB = Helper.CreateInstance<FOLMembershipUserBrokerDB>();
			m_ProviderRole = (IFOLRoleProvider)Roles.Provider;
			m_ProviderMembership = (IFOLSqlMembershipProvider)Membership.Provider;
		}
		/// <summary>
		/// Populate the chidren items specified as fields. "*" indicates that all fields should be populated.
		/// </summary>
		/// <param name="item">The item</param>
		/// <param name="loadMode">The load mode.</param>
		/// <param name="fields">The list of fields. "*" indicates all fields.</param>
		public override void PopulateList(FOLMembershipUser item, EntityLoadMode loadMode, params string[] fields) {
			foreach (var field in fields) {
				if (field.ContainsAny("*", "UserPreferences")) {
					var userPreferencesBroker = Helper.CreateInstance<UserPreferencesBroker>();
					item.Preferences = userPreferencesBroker.GetItem(item.KeyUser);
				}
			}
		}

		/// <summary>
		/// Get the last part of a string.
		/// </summary>
		/// <param name="value">The value of the string.</param>
		/// <param name="separator">The separator.</param>
		/// <returns></returns>
		private static string GetLastPart(string value, string separator) {
			if (String.IsNullOrEmpty(value))
				return value;
			if (value.IndexOf(separator) < 0)
				return value;
			var parts = value.Split(separator.ToCharArray());
			return parts[parts.Length - 1];
		}

		/// <summary>
		/// Resolves the references for import.
		/// </summary>
		/// <param name="mappingRecord">The mapping record.</param>
		/// <param name="persistedEntity">The persisted entity.</param>
		protected override void ResolveReferencesForImport(MappingRecord<FOLMembershipUser> mappingRecord, FOLMembershipUser persistedEntity) {
			ResolveParentReferenceForImport<Occupant>(e => e.ProviderUserKey.KeyOccupant, mappingRecord.Entity, persistedEntity);
			if (persistedEntity != null) {
				mappingRecord.Entity.ProviderUserKey.Guid = persistedEntity.ProviderUserKey.Guid;
			}
		}

		/// <summary>
		/// Resolves the references for export.
		/// </summary>
		/// <param name="entity">The entity.</param>
		protected override void ResolveReferencesForExport(FOLMembershipUser entity) {
			ResolveParentReferenceForExport<Occupant>(e => e.ProviderUserKey.KeyOccupant, entity);
		}

		/// <summary>
		/// Gets a value indicating whether [should overwrite key in mapping].
		/// </summary>
		/// <value>
		/// 	<c>true</c> if [should overwrite key in mapping]; otherwise, <c>false</c>.
		/// </value>
		protected override bool ShouldOverwriteKeyInMapping {
			get { return false; }
		}
		/// <summary>
		/// Gets a item by its localId.
		/// </summary>
		/// <param name="localId">The LocalId of the item.</param>
		/// <returns></returns>
		public override FOLMembershipUser GetItemByLocalId(string localId) {
			return User_GetByUserName(localId);
		}

		/// <summary>
		/// Gets a user from it's username.
		/// </summary>
		/// <param name="username">The user name.</param>
		/// <returns>A FOLMembershipUser.</returns>
		public FOLMembershipUser User_GetByUserName(string username) {
			FOLMembershipUser result = null;
			MembershipUser user = Membership.GetUser(username, false);

			if (user != null) {
				result = new FOLMembershipUser(user);
				result.Preferences = Helper.CreateInstance<UserPreferencesBroker>().GetItem(result.KeyUser);
			}
			return result;
			//return user != null ? new FOLMembershipUser(user) : null;
		}

		/// <summary>
		/// Gets all.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="location">The location.</param>
		/// <param name="total">The total.</param>
		/// <param name="useSecurableFilter">if set to <c>true</c> [use securable filter].</param>
		/// <param name="isHierarchyEnabled">if set to <c>true</c> [is hierarchy enabled].</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public override List<FOLMembershipUser> GetAll(PagingParameter paging, PagingLocation location, out int total, bool useSecurableFilter = true, bool isHierarchyEnabled = true, params string[] fields) {
			// we change here the paging parameter because we want to flatten the fields name.
			paging.sort = GetLastPart(paging.sort, ".");
			if (paging.filters != null) {
				foreach (GridFilter filter in paging.filters) {
					filter.field = GetLastPart(filter.field, ".");
				}
			}

			var users = base.GetAll(paging, location, out total, useSecurableFilter, isHierarchyEnabled, fields);
			CalculateApplicationGroupsForFOLMembershipUsers(users);
			
			return users;
		}

		/// <summary>
		/// Gets all users from all tenants.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="location">The location.</param>
		/// <param name="total">The total.</param>
		/// <param name="useSecurableFilter">if set to <c>true</c> [use securable filter].</param>
		/// <returns></returns>
		public List<FOLMembershipUser> GetAllFromAllTenants(PagingParameter paging, PagingLocation location, out int total, bool useSecurableFilter = true) {
			((FOLMembershipUserBrokerDB)m_brokerDB).IsTenantAware = false;
			return GetAll(paging, location, out total, useSecurableFilter);
		}

		/// <summary>
		/// Saves the ApplicationGroup users.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="users">The users.</param>
		/// <returns></returns>
		public void SaveApplicationGroupFOLMembershipUser(ApplicationGroup entity, CrudStore<FOLMembershipUser> users) {
			m_ProviderRole.CreateGroupMembers(entity, users);
		}

		/// <summary>
		/// Gets the list by application group id.
		/// </summary>
		/// <param name="applicationGroupId">The application group id.</param>
		/// <returns></returns>
		public List<FOLMembershipUser> GetListByApplicationGroupId(string applicationGroupId) {
			return m_ProviderRole.Authorization_GetListByApplicationGroupId(applicationGroupId);
		}

		/// <summary>
		/// Gets a list of user authorization for a specific application groups.
		/// </summary>
		/// <param name="filterId">The AzMan filter id for which to retreive the authorizations.</param>
		/// <returns></returns>
		public List<FOLMembershipUser> GetListByAzManFilter(string filterId) {
			return m_ProviderRole.AzManFilter_UserAuthorization_GetList(filterId);
		}

		/// <summary>
		/// Saves the FOL membership user authorization item.
		/// </summary>
		/// <param name="authorizationItem">The authorization item.</param>
		/// <param name="users">The users.</param>
		public void SaveAuthorizationItemFOLMembershipUser(AuthorizationItem authorizationItem, CrudStore<FOLMembershipUser> users) {
			m_ProviderRole.CreateAzManItemMembers(authorizationItem, users);
		}

		/// <summary>
		/// Creates the specified item.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <returns></returns>
		public override FOLMembershipUser Create(FOLMembershipUser item) {
			FOLMembershipUser retVal;
			try {
				ValidationHelper.Validate(item);
				// we make sure we clear the guid because we are creating a new user.
				item.ProviderUserKey.Guid = null;
				MembershipCreateStatus status;
				if (item.UserName == item.Password) {
					throw new ArgumentException(Langue.msg_username_same_as_password);
				}

				MembershipUser newUser = m_ProviderMembership.CreateUser(item, out status);
				retVal = new FOLMembershipUser(newUser);
				
				item.Preferences = item.Preferences ?? new UserPreferences();
				item.Preferences.KeyUser = retVal.KeyUser;
				var preferencesBroker = Helper.CreateInstance<UserPreferencesBroker>();
				item.Preferences = preferencesBroker.Create(item.Preferences);
				
				AuditAction(CrudAction.Create, null, retVal);
			}
			catch (MembershipCreateUserException ex) {
				Exception exception = GetLocalizedException(ex.StatusCode);
				throw exception;
			}
			return retVal;
		}

		/// <summary>
		/// Updates the specified item.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <param name="changedFields">The changed fields.</param>
		/// <returns></returns>
		public override FOLMembershipUser Update(FOLMembershipUser item, ref List<Tuple<string, string, string>> changedFields) {
			FOLMembershipUser retVal;
			try {
				ValidationHelper.Validate(item);

				MembershipUser membershipUser = m_ProviderMembership.GetUser(item.ProviderUserKey.Guid, false);
				FOLMembershipUser persistedUser = null;
				if (ShouldAudit()) {
					persistedUser = new FOLMembershipUser(membershipUser);
				}
				if (membershipUser == null)
					throw new MembershipCreateUserException(MembershipCreateStatus.InvalidProviderUserKey);

				if (membershipUser.UserName != item.UserName) {
					((IFOLSqlMembershipProvider)Membership.Provider).ChangeUserName(membershipUser.UserName, item.UserName);
					membershipUser = m_ProviderMembership.GetUser(item.UserName, false);
				}
				membershipUser.IsApproved = item.IsApproved;
				membershipUser.Email = item.Email;
				membershipUser.Comment = item.Comment;

				var FOLProviderUserKey = (FOLProviderUserKey) membershipUser.ProviderUserKey;
				FOLProviderUserKey.KeyOccupant = item.ProviderUserKey.KeyOccupant;
				FOLProviderUserKey.NeverExpires = item.ProviderUserKey.NeverExpires;
				FOLProviderUserKey.KeyLocation = item.ProviderUserKey.KeyLocation;
				FOLProviderUserKey.FirstName = item.ProviderUserKey.FirstName;
				FOLProviderUserKey.LastName = item.ProviderUserKey.LastName;
				
				m_ProviderMembership.UpdateUser(membershipUser);

				if (item.Preferences != null) {
					item.Preferences.KeyUser = item.KeyUser;
					Helper.CreateInstance<UserPreferencesBroker>().Save(item.Preferences);
				}

				if (!item.IsLockedOut)
					membershipUser.UnlockUser();
				else
					m_ProviderMembership.LockUser(membershipUser.UserName);

				if (!String.IsNullOrEmpty(item.Password)) {
					if (item.UserName == item.Password) {
						throw new ArgumentException(Langue.msg_username_same_as_password);
					}
					m_ProviderMembership.ChangePassword(membershipUser.UserName, item.Password);
					item.Password = Helper.LocalizeText("msg_password_changed");
				}
				retVal = new FOLMembershipUser(m_ProviderMembership.GetUser(item.UserName, false)) {
					// For auditing purpose
					Password = item.Password
				};
				m_ProviderRole.InvalidateStorageCache();
				changedFields = AuditAction(CrudAction.Update, persistedUser, retVal);
			}
			catch (MembershipCreateUserException ex) {
				Exception exception = GetLocalizedException(ex.StatusCode);
				throw exception;
			}
			return retVal;
		}

		/// <summary>
		/// Deletes the specified item.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <param name="persistedItem">The persisted item.</param>
		/// <returns></returns>
		public override bool Delete(FOLMembershipUser item, FOLMembershipUser persistedItem = null) {
			using (TransactionScope t = Helper.CreateTransactionScope()) {
				persistedItem = persistedItem ?? (ShouldAudit() ? User_GetByUserName(item.UserName) : null);
				m_ProviderRole.User_Remove(item.UserName);
				bool success = m_ProviderMembership.DeleteUser(item.UserName, true);
				AuditAction(CrudAction.Destroy, persistedItem, null);
				t.Complete();

				return success;
			}
		}

		/// <summary>
		/// Deletes the specified username.
		/// </summary>
		/// <param name="username">The username.</param>
		/// <returns></returns>
		public override bool Delete(string username) {
			var deletedUser = ShouldAudit() ? User_GetByUserName(username) : null;
			return Delete(new FOLMembershipUser { UserName = username }, deletedUser);
		}

		/// <summary>
		/// Gets a localized exception messages from the provider. 
		/// </summary>
		/// <param name="statusCode"></param>
		private Exception GetLocalizedException(MembershipCreateStatus statusCode) {
			MembershipException exception;

			switch (statusCode) {
				case MembershipCreateStatus.DuplicateUserName:
					exception = new MembershipException(Langue.error_membership_duplicateuser) { Id = "UserName" };
					return exception;
				case MembershipCreateStatus.UserRejected:
					exception = new MembershipException(Langue.error_membership_userrejected) { Id = "UserName" };
					return exception;
				case MembershipCreateStatus.InvalidUserName:
					exception = new MembershipException(Langue.error_membership_invalidusername) { Id = "UserName" };
					return exception;
				case MembershipCreateStatus.DuplicateEmail:
					exception = new MembershipException(Langue.error_membership_duplicateemail) { Id = "Email" };
					return exception;
				case MembershipCreateStatus.InvalidEmail:
					exception = new MembershipException(Langue.error_membership_invalidemail) { Id = "Email" };
					return exception;
				case MembershipCreateStatus.InvalidAnswer:
					exception = new MembershipException(Langue.error_membership_invalidanswer) { Id = "SecurityAnswer" };
					return exception;
				case MembershipCreateStatus.InvalidPassword: {
					var sqlMembershipProvider  = m_ProviderMembership as SqlMembershipProvider;
					if (sqlMembershipProvider != null && !String.IsNullOrWhiteSpace(sqlMembershipProvider.PasswordStrengthRegularExpression)) {
						exception = new MembershipException(Langue.error_msg_password_regex_config) { Id = "password" }; ;	
					}
					else {
						exception = new MembershipException(Langue.error_membership_invalidpassword) { Id = "password" };	
					}
					return exception;
				}
				case MembershipCreateStatus.InvalidQuestion:
					exception = new MembershipException(Langue.error_membership_invalidquestion) { Id = "SecurityQuestion" };
					return exception;
				case MembershipCreateStatus.InvalidProviderUserKey:
					return new Exception(Langue.error_membership_invalidprovideruserkey);
				case MembershipCreateStatus.DuplicateProviderUserKey:
					return new Exception(Langue.error_membership_duplicateprovideruserkey);
				default:
					// This is in case the enum wasn't handled fully.
					throw new ArgumentOutOfRangeException("statusCode");
			}
		}

		/// <summary>
		/// Gets a list of FOLMembershipUsers for a specific PortalTemplate.
		/// </summary>
		/// <param name="KeyPortalTemplate">The Key PortalTemplate.</param>
		/// <returns></returns>
		public virtual List<FOLMembershipUser> GetListByKeyPortalTemplate(string KeyPortalTemplate) {
			return ((FOLMembershipUserBrokerDB)m_brokerDB).GetListByKeyPortalTemplate(KeyPortalTemplate);
		}

		/// <summary>
		/// Saves the PortalTemplate FOLMembershipUsers.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="users">The users.</param>
		/// <returns></returns>
		public List<FOLMembershipUser> SaveFOLMembershipUserPortalTemplate(PortalTemplate entity, CrudStore<FOLMembershipUser> users) {
			return ((FOLMembershipUserBrokerDB)m_brokerDB).SaveFOLMembershipUserPortalTemplate(entity, users);
		}

		/// <summary>
		/// Gets a store of FOLMembershipUsers for a specific PortalTemplate.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyPortalTemplate">The Key of the PortalTemplate.</param>
		/// <returns>A JsonStore.</returns>
		public JsonStore<FOLMembershipUser> GetStoreByKeyPortalTemplate(PagingParameter paging, string KeyPortalTemplate) {
			var users = ((FOLMembershipUserBrokerDB) m_brokerDB).GetListByKeyPortalTemplate(KeyPortalTemplate);
			CalculateApplicationGroupsForFOLMembershipUsers(users);
            PopulateList(users, "UserPreferences");
			return users.ToJsonStoreWithFilter(paging);
		}

		/// <summary>
		/// Sets the Application groups propery for FOLMembership users
		/// </summary>
		/// <param name="users">User list</param>
		/// <returns>User list with calculated property</returns>
		public List<FOLMembershipUser> CalculateApplicationGroupsForFOLMembershipUsers(List<FOLMembershipUser> users) {
			var usersApplicationGroupsDictionary = m_ProviderRole.Users_ApplicationGroups_GetAll();

			foreach (var user in users.Where(user => usersApplicationGroupsDictionary.ContainsKey(user.KeyUser))) {
				user.ProviderUserKey.ApplicationGroups = usersApplicationGroupsDictionary[user.KeyUser].Aggregate((i, j) => i + ", " + j);
			}

			return users;
		}
	}

}

﻿using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.DataLayer.BrokerDB;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for business entity DynamicDisplay.
	/// </summary>
	public class DynamicDisplayColorTemplateBroker : MappingEnabledBroker<DynamicDisplayColorTemplate, DynamicDisplayColorTemplateBrokerDB> {
		/// <summary>
		/// Resolves the references for export.
		/// </summary>
		/// <param name="entity">The entity.</param>
		protected override void ResolveReferencesForExport(DynamicDisplayColorTemplate entity) {
			// Do nothing.
		}

		/// <summary>
		/// Resolves the references during mapping.
		/// </summary>
		/// <param name="mappingRecord">The mapping record.</param>
		/// <param name="persistedEntity">The persisted entity.</param>
		protected override void ResolveReferencesForImport(MappingRecord<DynamicDisplayColorTemplate> mappingRecord, DynamicDisplayColorTemplate persistedEntity) {
			// Do nothing.
		}
	}

}
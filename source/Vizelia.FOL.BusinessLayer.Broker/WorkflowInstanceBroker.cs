﻿using System;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.DataLayer.BrokerDB;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using System.Collections.Generic;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for business entity WorkflowInstance.
	/// </summary>
	public class WorkflowInstanceBroker : BaseBroker<WorkflowInstance, WorkflowInstanceBrokerDB> {

		/// <summary>
		/// Gets a item by its instanceId.
		/// </summary>
		/// <param name="instanceId">The instanceId of the item.</param>
		/// <returns></returns>
		public virtual WorkflowInstance GetItemByInstanceId([NotNullValidator] Guid instanceId) {
			return ((WorkflowInstanceBrokerDB)m_brokerDB).GetItemByInstanceId(instanceId);
		}


	}

}
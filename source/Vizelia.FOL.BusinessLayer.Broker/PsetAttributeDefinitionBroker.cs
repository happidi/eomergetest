﻿using System.Collections.Generic;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common.Localization;
using Vizelia.FOL.DataLayer.BrokerDB;


namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for business entity PsetAttributeDefinition
	/// </summary>
	public class PsetAttributeDefinitionBroker : MappingEnabledBroker<PsetAttributeDefinition, PsetAttributeDefinitionBrokerDB> {

		/// <summary>
		/// Public ctor.
		/// </summary>
		public PsetAttributeDefinitionBroker() {
			m_database_errors.Add(1, Langue.error_msg_uniquefield, "AttributeName");

		}

		/// <summary>
		/// Gets the attributes definition of a pset definition.
		/// </summary>
		/// <param name="Keys">The list of keys PsetDefinition.</param>
		/// <returns></returns>
		public List<PsetAttributeDefinition> GetAttributesFromPsetDefinitionList(string[] Keys) {
			return ((PsetAttributeDefinitionBrokerDB)m_brokerDB).GetAttributesFromPsetDefinitionList(Keys);
		}


		/// <summary>
		/// Gets the list from parents PropertySet keys.
		/// </summary>
		/// <param name="Keys">The parent PropertySet keys.</param>
		/// <returns></returns>
		public List<PsetAttributeDefinition> GetListFromParents(string[] Keys) {
			return ((PsetAttributeDefinitionBrokerDB)m_brokerDB).GetListFromParents(Keys);
		}

		/// <summary>
		/// Resolves the references.
		/// </summary>
		/// <param name="mappingRecord">The mapping record.</param>
		/// <param name="persistedEntity">The persisted entity.</param>
		protected override void ResolveReferencesForImport(MappingRecord<PsetAttributeDefinition> mappingRecord, PsetAttributeDefinition persistedEntity) {
			ResolveParentReferenceForImport<PsetDefinition>(e => e.KeyPropertySet, mappingRecord.Entity, persistedEntity);
		}

		/// <summary>
		/// Resolves the references for export.
		/// </summary>
		/// <param name="entity">The entity.</param>
		protected override void ResolveReferencesForExport(PsetAttributeDefinition entity) {
			ResolveParentReferenceForExport<PsetDefinition>(e => e.KeyPropertySet, entity);
		}
	}
}

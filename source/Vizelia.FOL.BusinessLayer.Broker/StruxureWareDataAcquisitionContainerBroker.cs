﻿using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.DataLayer.BrokerDB;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for business entity StruxureWareDataAcquisitionEndpoint.
	/// </summary>
	public class StruxureWareDataAcquisitionEndpointBroker : DataAcquisitionEndpointBroker<StruxureWareDataAcquisitionEndpoint, StruxureWareDataAcquisitionEndpointBrokerDB> {
		/// <summary>
		/// Resolves the references for import.
		/// </summary>
		/// <param name="mappingRecord">The mapping record.</param>
		/// <param name="persistedEntity">The persisted entity.</param>
		protected override void ResolveReferencesForImport(MappingRecord<StruxureWareDataAcquisitionEndpoint> mappingRecord, StruxureWareDataAcquisitionEndpoint persistedEntity) {
			ResolveParentReferenceForImport<StruxureWareDataAcquisitionContainer>(x => x.KeyDataAcquisitionContainer, mappingRecord.Entity, persistedEntity);
		}

		/// <summary>
		/// Resolves the references for export.
		/// </summary>
		/// <param name="entity">The entity.</param>
		protected override void ResolveReferencesForExport(StruxureWareDataAcquisitionEndpoint entity) {
			ResolveParentReferenceForExport<StruxureWareDataAcquisitionContainer>(x => x.KeyDataAcquisitionContainer, entity);
		}
	}
}
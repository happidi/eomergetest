﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.DataLayer.BrokerDB;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Localization;
using Vizelia.FOL.BusinessEntities.Mapping;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for business entity CalendarOccurrences.
	/// </summary>
	public class CalendarOccurrencesBroker : BaseBroker<CalendarOccurrences, CalendarOccurrencesBrokerDB> {
		/// <summary>
		/// Deletes the CalendarOccurrences by key calendar.
		/// </summary>
		/// <param name="keyCalendar">The key calendar.</param>
		public void DeleteByKeyCalendar(string keyCalendar) {
			((CalendarOccurrencesBrokerDB)m_brokerDB).DeleteByKeyCalendar(keyCalendar);
		}
	}

}
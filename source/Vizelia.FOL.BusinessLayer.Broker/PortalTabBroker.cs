﻿using System.Collections.Generic;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.DataLayer.BrokerDB;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for business entity PortalTab.
	/// </summary>
	public class PortalTabBroker : MappingEnabledBroker<PortalTab, PortalTabBrokerDB> {

		/// <summary>
		/// Get the list of saved PortalTab from a Portal
		/// </summary>
		/// <param name="KeyPortalWindow">The key of the Portal window.</param>
		public List<PortalTab> GetListByKeyPortalWindow(string KeyPortalWindow) {
			return ((PortalTabBrokerDB)m_brokerDB).GetListByKeyPortalWindow(KeyPortalWindow);
		}

		/// <summary>
		/// Gets a store of PortalTab for a specific Portal.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyPortalWindow">The Key of the Portal.</param>
		/// <returns>A JsonStore.</returns>
		public JsonStore<PortalTab> GetStoreByKeyPortalWindow(PagingParameter paging, string KeyPortalWindow) {
			int total;
			return ((PortalTabBrokerDB)m_brokerDB).GetAllPagingByKeyPortalWindow(paging, KeyPortalWindow, out total).ToJsonStore(total, paging);
		}


		/// <summary>
		/// Saves the Portal PortalTab.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="portaltab">The PortalTab store.</param>
		/// <returns></returns>
		public List<PortalTab> SavePortalPortalTab(PortalWindow entity, CrudStore<PortalTab> portaltab) {
			return ((PortalTabBrokerDB)m_brokerDB).SavePortalPortalTab(entity, portaltab);
		}


		/// <summary>
		/// Populate the chidren items specified as fields. "*" indicates that all fields should be populated.
		/// </summary>
		/// <param name="item">The item</param>
		/// <param name="loadMode">The load mode.</param>
		/// <param name="fields">The list of fields. "*" indicates all fields.</param>
		public override void PopulateList(PortalTab item, EntityLoadMode loadMode, params string[] fields) {
			foreach (string field in fields) {
				if (field.ContainsAny("*", "Columns")) {
					PortalColumnBroker brokerColumn = Helper.CreateInstance<PortalColumnBroker>();
					var columns = brokerColumn.GetListByKeyPortalTab(item.KeyPortalTab);
					if (columns != null && columns.Count > 0) {
						brokerColumn.PopulateList(columns, loadMode, fields);
						brokerColumn.ResolveForExport(columns, loadMode);
						item.Columns = columns;
					}
					break;
				}

				if (field.ContainsAny("*", "VirtualFile")) {
					VirtualFileBroker brokerVirtualFile = Helper.CreateInstance<VirtualFileBroker>();
					var virtualFile = brokerVirtualFile.GetItemByKeyEntity(item.KeyPortalTab, item.GetType());
					if (virtualFile != null) {
						brokerVirtualFile.ResolveForExport(virtualFile, loadMode);
						item.VirtualFile = virtualFile;
					}
				}
			}
		}

		/// <summary>
		/// Copy an existing PortalTab.
		/// </summary>
		/// <param name="item">the item to be copied.</param>
		/// <param name="formResponse">The response of the operation.</param>
		/// <param name="Creator">The creator.</param>
		/// <param name="incrementTitle">if set to <c>true</c> [increment title].</param>
		/// <returns></returns>
		public override PortalTab Copy(PortalTab item, out FormResponse formResponse, string Creator = null, bool incrementTitle = true) {
			if (incrementTitle)
				item.Title = ChartHelper.Increment(item.Title);

			var newItem = base.Copy(item, out formResponse, Creator, incrementTitle);
			newItem.Columns = new List<PortalColumn>();
			if (formResponse.success) {
				PortalColumnBroker brokerColumn = Helper.CreateInstance<PortalColumnBroker>();
				foreach (var column in item.Columns) {
					FormResponse response;
					column.KeyPortalTab = newItem.KeyPortalTab;
					newItem.Columns.Add(brokerColumn.Copy(column, out response, Creator, incrementTitle));
				}
			}
			return newItem;
		}

		/// <summary>
		/// Resolves the references for export.
		/// </summary>
		/// <param name="entity">The entity.</param>
		protected override void ResolveReferencesForExport(PortalTab entity) {
			ResolveParentReferenceForExport<PortalWindow>(e => e.KeyPortalWindow, entity);
		}

		/// <summary>
		/// Resolves the references during mapping.
		/// </summary>
		/// <param name="mappingRecord">The mapping record.</param>
		/// <param name="persistedEntity">The persisted entity.</param>
		protected override void ResolveReferencesForImport(MappingRecord<PortalTab> mappingRecord, PortalTab persistedEntity) {
			ResolveParentReferenceForImport<PortalWindow>(e => e.KeyPortalWindow, mappingRecord.Entity, persistedEntity);
		}

	}

}

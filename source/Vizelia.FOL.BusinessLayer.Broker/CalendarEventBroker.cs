﻿using System;
using System.Linq;
using DDay.iCal;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Localization;
using Vizelia.FOL.DataLayer;
using Vizelia.FOL.DataLayer.BrokerDB;
using Vizelia.FOL.DataLayer.Interfaces;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for business entity CalendarEvent.
	/// </summary>
	public class CalendarEventBroker : MappingEnabledBroker<CalendarEvent, CalendarEventBrokerDB> {
		private readonly ICoreDataAccess m_CoreDataAccess;
		private readonly CalendarOccurrencesBroker m_CalendarOccurrencesBroker = Helper.CreateInstance<CalendarOccurrencesBroker>();

		/// <summary>
		/// Initializes a new instance of the <see cref="CalendarEventBroker"/> class.
		/// </summary>
		public CalendarEventBroker() {
			m_CoreDataAccess = Helper.CreateInstance<CoreDataAccess, ICoreDataAccess>();
		}
		/// <summary>
		/// Resolves the references for import.
		/// </summary>
		/// <param name="mappingRecord">The mapping record.</param>
		/// <param name="persistedEntity">The persisted entity.</param>
		protected override void ResolveReferencesForImport(MappingRecord<CalendarEvent> mappingRecord, CalendarEvent persistedEntity) {
			ResolveLocationReferenceForImport(mappingRecord, persistedEntity);
		}

		/// <summary>
		/// Resolves the references for export.
		/// </summary>
		/// <param name="entity">The entity.</param>
		protected override void ResolveReferencesForExport(CalendarEvent entity) {
			ResolveLocationReferenceForExport(entity);
		}

		/// <summary>
		/// Deletes the specified CalendarEvent from its Lcoation.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <param name="KeyLocation">The key location.</param>
		/// <returns></returns>
		public bool Delete(CalendarEvent item, string KeyLocation) {
			string iCalData = m_CoreDataAccess.Calendar_GetFromObject(KeyLocation);
			iCalendar iCal = CalendarHelper.Deserialize(iCalData);
			iCal.RemoveChild(iCal.Events[item.Key]);
			if (iCal.Events.Count == 0)
				// in case there are no events anymore we delete the calendar for this location.
				m_CoreDataAccess.Calendar_DeleteForObject(KeyLocation);
			else
				iCalData = CalendarHelper.Serialize(iCal);
			m_CoreDataAccess.Calendar_SaveForObject(KeyLocation, iCalData, item.TimeZoneId);

			m_CalendarOccurrencesBroker.DeleteByKeyCalendar(KeyLocation);
			return true;
		}

		/// <summary>
		/// Forms the create.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <param name="KeyLocation">The key location.</param>
		/// <returns></returns>
		public FormResponse FormCreate(CalendarEvent item, string KeyLocation) {
			item.KeyLocation = KeyLocation;
			FormResponse formResponse;
			m_CalendarOccurrencesBroker.DeleteByKeyCalendar(KeyLocation);
			this.Create(item, out formResponse);
			return FormLoad(item.Key, KeyLocation);
		}

		/// <summary>
		/// Calendars the event_ form load.
		/// </summary>
		/// <param name="Key">The key.</param>
		/// <param name="KeyLocation">The key location.</param>
		/// <returns></returns>
		public FormResponse FormLoad(string Key, string KeyLocation) {
			FormResponse response;
			CalendarRecurrencePattern rp = default(CalendarRecurrencePattern);
			string iCalData = m_CoreDataAccess.Calendar_GetFromObject(KeyLocation);
			//string itemZoneId = m_CoreDataAccess.Calendar_GetTimeZoneId(KeyLocation);
			iCalendar iCal = CalendarHelper.Deserialize(iCalData);

			if (iCal.Events[Key] != null) {
				Event ev = (Event)iCal.Events[Key];
				if (ev.RecurrenceRules != null && ev.RecurrenceRules.Count > 0) {
					rp = ((RecurrencePattern)ev.RecurrenceRules[0]).ToBusinessEntity();
				}
				CalendarEvent item = ev.ToBusinessEntity(KeyLocation);
				item.RecurrencePattern = rp;
				//item.TimeZoneId = itemZoneId;
				item.TimeZoneOffset[0] = item.TimeZone.GetUtcOffset(item.StartDate.Value);
				item.TimeZoneOffset[1] = item.TimeZone.GetUtcOffset(item.EndDate.Value);
				if (rp != null && rp.Until != null) {
					rp.Until = CalendarHelper.ConvertClientDateToUTC(rp.Until.Value, item.TimeZoneId);
					item.TimeZoneOffset[2] = item.TimeZone.GetUtcOffset(rp.Until.Value);
				}
				response = item.ToFormResponse();
			}
			else {
				response = new FormResponse { success = false, msg = Langue.error_msg_getitem };
			}
			return response;
		}

		/// <summary>
		/// Forms the update.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <param name="KeyLocation">The key location.</param>
		/// <returns></returns>
		public FormResponse FormUpdate(CalendarEvent item, string KeyLocation) {
			string iCalData = m_CoreDataAccess.Calendar_GetFromObject(KeyLocation);
			iCalendar iCal = CalendarHelper.Deserialize(iCalData);
			iCal.RemoveChild(iCal.Events[item.Key]);

			FixCalendarEventDates(item, iCal);
			item.ToiCal(iCal);
			iCalData = CalendarHelper.Serialize(iCal);
			m_CoreDataAccess.Calendar_SaveForObject(KeyLocation, iCalData, item.TimeZoneId);
			m_CalendarOccurrencesBroker.DeleteByKeyCalendar(KeyLocation);
			return FormLoad(item.Key, KeyLocation);
		}

		/// <summary>
		/// Saves the store.
		/// </summary>
		/// <param name="store">The store.</param>
		/// <param name="KeyLocation">The key location.</param>
		/// <returns></returns>
		public JsonStore<CalendarEvent> SaveStore(CrudStore<CalendarEvent> store, string KeyLocation) {
			string iCalData = m_CoreDataAccess.Calendar_GetFromObject(KeyLocation);
			iCalendar iCal = CalendarHelper.Deserialize(iCalData);
			if (store.create != null) {
				foreach (CalendarEvent item in store.create.Where(x => x.EventType == CalendarEventType.Instance)) {
					item.Key = null; // forces a new key
					FixCalendarEventDates(item, iCal);
					item.ToiCal(iCal);
				}
			}
			if (store.update != null) {
				foreach (CalendarEvent item in store.update.Where(x => x.EventType == CalendarEventType.Instance)) {
					iCal.RemoveChild(iCal.Events[item.Key]);
					FixCalendarEventDates(item, iCal);
					item.ToiCal(iCal);
				}
				foreach (CalendarEvent item in store.update.Where(x => x.EventType == CalendarEventType.Occurrence)) {
					string instanceKey = item.GetInstanceKey();
					if (CalendarHelper.EventHasRecurrence(instanceKey, iCal)) {
						CalendarHelper.MarkOccurrenceAsDeleted(item, iCal);
						item.Key = null; // forces a new key
						item.RelatedTo = instanceKey; // retains the relationship with the parent event.
					}
					else {
						iCal.RemoveChild(iCal.Events[instanceKey]);
						item.Key = instanceKey;
					}
					FixCalendarEventDates(item, iCal);
					item.ToiCal(iCal);
				}

			}
			if (store.destroy != null) {
				foreach (CalendarEvent item in store.destroy.Where(x => x.EventType == CalendarEventType.Instance)) {
					iCal.RemoveChild(iCal.Events[item.Key]);
				}
				foreach (CalendarEvent item in store.destroy.Where(x => x.EventType == CalendarEventType.Occurrence)) {
					string instanceKey = item.GetInstanceKey();
					if (CalendarHelper.EventHasRecurrence(instanceKey, iCal)) {
						CalendarHelper.MarkOccurrenceAsDeleted(item, iCal);
					}
					else {
						iCal.RemoveChild(iCal.Events[instanceKey]);
					}
				}
			}
			if (iCal.Events.Count == 0) {
				// in case there are no events anymore we delete the calendar for this location.
				m_CoreDataAccess.Calendar_DeleteForObject(KeyLocation);
			}
			else {
				iCalData = CalendarHelper.Serialize(iCal);
				var defaultTimeZone = (store.create != null) ? store.create[0].TimeZoneId : "";
				m_CoreDataAccess.Calendar_SaveForObject(KeyLocation, iCalData, defaultTimeZone);
			}
			m_CalendarOccurrencesBroker.DeleteByKeyCalendar(KeyLocation);
			JsonStore<CalendarEvent> result = new JsonStore<CalendarEvent> { success = true };
			return result;
		}

		/// <summary>
		/// Gets the item by local id.
		/// </summary>
		/// <param name="localId">The local id.</param>
		/// <returns></returns>
		public override CalendarEvent GetItemByLocalId(string localId) {
			return null;
		}

		/// <summary>
		/// Creates the specified item.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <param name="formResponse">The form response.</param>
		/// <returns></returns>
		protected override CalendarEvent Create(CalendarEvent item, out FormResponse formResponse) {
			formResponse = new FormResponse { success = true };
			string iCalData = m_CoreDataAccess.Calendar_GetFromObject(item.KeyLocation);
			iCalendar iCal = iCalData == null ? new iCalendar() : CalendarHelper.Deserialize(iCalData);

			FixCalendarEventDates(item, iCal);
			item.Key = null; // forces a new id
			item.ToiCal(iCal);
			iCalData = CalendarHelper.Serialize(iCal);
			m_CoreDataAccess.Calendar_SaveForObject(item.KeyLocation, iCalData, item.TimeZoneId);
			return item;
		}

		private static void FixCalendarEventDates(CalendarEvent item, iCalendar iCal) {
			CalendarHelper.AddTimeZone(iCal, item.TimeZone);
			if (item.StartDate.Value.Kind == DateTimeKind.Unspecified) {
				DateTime.SpecifyKind(item.StartDate.Value, DateTimeKind.Utc);
				DateTime.SpecifyKind(item.EndDate.Value, DateTimeKind.Utc);
			}
			else if (item.StartDate.Value.Kind == DateTimeKind.Local)
				CalendarHelper.UpdateEventClientDates(item);
			else if (item.IsAllDay)
				item.EndDate = item.EndDate.Value.AddSeconds(-1);
		}
	}
}
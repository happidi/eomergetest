﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Configuration;
using Vizelia.FOL.Common.Localization;
using Vizelia.FOL.DataLayer.BrokerDB;
using Vizelia.FOL.Providers;
using System.Collections.Concurrent;

namespace Vizelia.FOL.BusinessLayer.Broker{
	/// <summary>
	///  The broker for business entity MeterData.
	/// </summary>
	public class MeterDataBroker : MappingEnabledBroker<MeterData, MeterDataBrokerDB>{
		private readonly MeterDataValidity m_DefaultMeterDataValidity = VizeliaConfiguration.Instance.Deployment.DefaultMeterDataValidity;
		private const string const_mapping_meters_localIds_key = "MetersLocalIdsInMeterData";
		private const int const_meter_data_energy_aggregator_notification_page_size = 10000;

		private static readonly object m_MeterCreateSyncLock = new object();
		private static readonly object m_LocationCreateSyncLock = new object();
		private static readonly object m_ClassificationCreateSyncLock = new object();


		/// <summary>
		///     Resolves the references.
		/// </summary>
		/// <param name="mappingRecord">The mapping record.</param>
		/// <param name="persistedEntity">The persisted entity.</param>
		protected override void ResolveReferencesForImport(MappingRecord<MeterData> mappingRecord, MeterData persistedEntity){
			throw new NotSupportedException("This method should not be called.");
		}

		/// <summary>
		///     Prepares to export entity.
		/// </summary>
		/// <param name="entity">The entity.</param>
		protected override void ResolveReferencesForExport(MeterData entity){
			ResolveParentReferenceForExport<Meter>(e => e.KeyMeter, entity);
		}

		/// <summary>
		/// Gets the maximum size of the batch.
		/// </summary>
		/// <value>
		/// The maximum size of the batch.
		/// </value>
		public override int MaximumBatchSize {
			get { return VizeliaConfiguration.Instance.Mapping.ImportMeterDataBatchSize; }
		}

		/// <summary>
		///     Maps the specified mapping records.
		/// </summary>
		/// <param name="mappingRecords">The mapping records.</param>
		/// <param name="action">The action.</param>
		/// <param name="mappingContext">The mapping mappingContext.</param>
		/// <param name="formResponses">The form responses.</param>
		/// <exception cref="System.ArgumentOutOfRangeException">action</exception>
		public override void Mapping(IEnumerable<IMappingRecord> mappingRecords,
			string action,
			MappingContext mappingContext,
			out IEnumerable<Tuple<string, FormResponse>> formResponses){
			formResponses = new List<Tuple<string, FormResponse>>();

			// We perform a bulk operation here.
			// Unlike the usual mapping process, here we do not perform resolving parent references or checking whether the item
			// exists in the DB or not. We just send it all to the stored procedure and it does all the work.
			// Also, we ignore Psets and Documents of the entity, since it doesn't have any of these.
			
			List<MeterData> meterDatas = mappingRecords.Select(record => CreateMeterData(record, action)).ToList();
			if (!meterDatas.Any()){
				return;
			}

			//Auto create unknown meters if needed before importing their data
			if (mappingContext.MappingConfiguration.AutoCreateMeterOptions != AutoCreateMeterOptions.Off){
				CreateUnknownMeters(mappingContext, meterDatas);
			}

			// Perform the bulk operation by the action.
			IEnumerable<MeterDataBulkOperationResult> report;

			switch (action){
				case CrudAction.Create:
				case CrudAction.Update:
					report = ((MeterDataBrokerDB) m_brokerDB).ImportBulk(meterDatas);
					break;

				case CrudAction.Destroy:
					report = ((MeterDataBrokerDB) m_brokerDB).DeleteBulk(meterDatas);
					break;

				default:
					throw new ArgumentOutOfRangeException("action");
			}

			formResponses = ProcessBulkResults(report, meterDatas.Count);
		}


		/// <summary>
		/// Creates the meters that are still not created but the meter data for them has arrived from import.
		/// </summary>
		/// <param name="mappingContext">The mapping context.</param>
		/// <param name="metersData">The meters data.</param>
		private static void CreateUnknownMeters(MappingContext mappingContext, IEnumerable<MeterData> metersData){
			//We select the meter by the KeyMeter which actually holds the LocalId of the meter
			IEnumerable<string> distinctMetersLocalIds = metersData.Distinct(md => md.KeyMeter).Select(md => md.KeyMeter);
			
			lock (m_MeterCreateSyncLock){
				
				try{
					HashSet<string> cachedLocalIds = ContextHelper.Get(const_mapping_meters_localIds_key) as HashSet<string>;
					if (cachedLocalIds == null){
						cachedLocalIds = new HashSet<string>();
						ContextHelper.Add(const_mapping_meters_localIds_key, cachedLocalIds);
					}

					IEnumerable<string> localIdsNotInCache = GetMetersLocalIdsThatAreNotInCache(distinctMetersLocalIds, cachedLocalIds);
					if (!localIdsNotInCache.Any()) {
						return; //all of these meters already exist	
					}

					//Get LocalIds from DB
					var meterBroker = Helper.CreateInstance<MeterBroker>();
					HashSet<string> metersLocalIdsFromDb = meterBroker.GetMeterLocalIdsByLocalIds(localIdsNotInCache);

					Location unknownMetersLocation = null;
					ClassificationItem unknownMetersClassification = null;
					foreach (string localId in localIdsNotInCache){
						bool shouldAddToCache = true;
						
						if (!metersLocalIdsFromDb.Contains(localId)){
							unknownMetersLocation = unknownMetersLocation ?? GetUnknownMetersLocation(mappingContext.MappingConfiguration);
							unknownMetersClassification = unknownMetersClassification ?? GetUnknownMetersDefaultClassification();
							shouldAddToCache = CreateUnknownMeter(localId,
															meterBroker,
															unknownMetersLocation.KeyLocation,
															unknownMetersClassification.KeyClassificationItem,
															mappingContext.MappingSummary);
						}
						if (shouldAddToCache){
							cachedLocalIds.Add(localId);
						}
					}
				}
				catch (Exception exception){
					string error ="Failed to create unknown meters due to the following exception : " + exception.Message;
					mappingContext.MappingSummary.LogError(new Exception(error, exception), Langue.msg_mapping_error);	
				}
				
			}
		}


		/// <summary>
		/// Gets the meters local ids that are not currently in the cache.
		/// </summary>
		/// <param name="metersLocalIds">The meters local ids.</param>
		/// <param name="cachedLocalIds">The cached local ids.</param>
		/// <returns></returns>
		private static IEnumerable<string> GetMetersLocalIdsThatAreNotInCache(IEnumerable<string> metersLocalIds, IEnumerable<string> cachedLocalIds) {
			IEnumerable<string> localIdsNotInCache;
			if (cachedLocalIds == null) {
				localIdsNotInCache = metersLocalIds;
			}
			else {
				localIdsNotInCache = metersLocalIds.Except(cachedLocalIds);
			}

			return localIdsNotInCache;
		}

		/// <summary>
		/// Creates a meter as part of the mapping process.
		/// </summary>
		/// <param name="meterlocalId">The meterlocal identifier.</param>
		/// <param name="meterBroker">The meter broker.</param>
		/// <param name="keyLocation">The key location.</param>
		/// <param name="keyClassification">The key classification.</param>
		/// <param name="mappingSummary">The mapping summary.</param>
		/// <returns></returns>
		private static bool CreateUnknownMeter(string meterlocalId, MeterBroker meterBroker, string keyLocation, string keyClassification, IMappingSummary mappingSummary){
			bool success;
			
			try{
				var meter = new Meter {
					LocalId = meterlocalId,
					Name = meterlocalId,
					KeyLocation = keyLocation,
					KeyClassificationItem = keyClassification
				};


				meterBroker.FormCreate(meter);
				success = true;
			}
			catch (Exception exception) {
				success = false;
				string error =
					string.Format("Failed to create meter with LocalId: {0} due to the following exception {1}: ", meterlocalId, exception.Message);
				mappingSummary.LogError(new Exception(error, exception), Langue.msg_mapping_error);
			}

			return success;
		}

		/// <summary>
		///  Gets the location of the unknown meters.
		/// </summary>
		/// <param name="mappingConfig">The mapping configuration.</param>
		private static Location GetUnknownMetersLocation(MappingConfiguration mappingConfig){
			var broker = Helper.CreateInstance<LocationBroker>();
			Location metersLocation;

			if (mappingConfig.AutoCreateMeterOptions == AutoCreateMeterOptions.AutoCreateInCustomLocation &&
			    !string.IsNullOrWhiteSpace(mappingConfig.AutoCreatedMetersKeyLocation)){
				
				metersLocation = broker.GetItem(mappingConfig.AutoCreatedMetersKeyLocation);
				if (metersLocation == null){
					throw new ArgumentException(string.Format("The specified location with key {0} does not exist.",
															   mappingConfig.AutoCreatedMetersKeyLocation),
															   "mappingConfig.AutoCreatedMetersKeyLocation");
				}

				return metersLocation;
			}

			metersLocation = GetUnknownMetersDefaultLocation();

			return metersLocation;
		}

		/// <summary>
		///  Gets the default location of the unknown meters.
		/// </summary>
		private static Location GetUnknownMetersDefaultLocation(){
			var siteBroker = Helper.CreateInstance<SiteBroker>();
			Site defaultLocation = siteBroker.GetItemByLocalId(MappingConstants.const_unresolved_meters_default_spatial_id);

			//double check locking
			if (defaultLocation == null){
				lock (m_LocationCreateSyncLock){
					defaultLocation = siteBroker.GetItemByLocalId(MappingConstants.const_unresolved_meters_default_spatial_id);
					if (defaultLocation == null){
						defaultLocation = CreateDefaultSiteForUnknownMeters(siteBroker,
							MappingConstants.const_unresolved_meters_default_spatial_id,
							MappingConstants.const_unresolved_meters_default_spatial_name);
					}
				}
			}

			return defaultLocation.GetLocation();
		}

		/// <summary>
		///  Creates the default site for the unknown meters.
		/// </summary>
		/// <param name="siteBroker">The site broker.</param>
		/// <param name="defaultLocationLocalId">Id of the default location</param>
		/// <param name="defaultLocationName">Name of the default location.</param>
		private static Site CreateDefaultSiteForUnknownMeters(SiteBroker siteBroker, string defaultLocationLocalId, string defaultLocationName) {
			var defaultSite = new Site
			{
				LocalId = defaultLocationLocalId,
				Name = defaultLocationName
			};

			FormResponse response = siteBroker.FormCreate(defaultSite);

			return response.data as Site;
		}

		/// <summary>
		///  Creates the unknown meters classification.
		/// </summary>
		/// <param name="broker">The broker.</param>
		/// <returns></returns>
		private static ClassificationItem CreateUnknownMetersClassification(ClassificationItemBroker broker){
			var ci = new ClassificationItem
			{
				LocalId = MappingConstants.const_unresolved_meters_default_classification_id,
				Title = MappingConstants.const_unresolved_meters_default_classification_name,
				KeyParent = broker.GetCategoryRoot(ClassificationCategory.Meter).KeyClassificationItem
			};

			FormResponse response = broker.FormCreate(ci);

			return response.data as ClassificationItem;
		}

		/// <summary>
		///  Gets the default classification for the unknown meters.
		/// </summary>
		/// <returns></returns>
		private static ClassificationItem GetUnknownMetersDefaultClassification(){
			var classificationItemBroker = Helper.CreateInstance<ClassificationItemBroker>();
			ClassificationItem ci = classificationItemBroker.GetItemByLocalId(MappingConstants.const_unresolved_meters_default_classification_id);

			//double check locking
			if (ci == null){
				lock (m_ClassificationCreateSyncLock){
					ci = classificationItemBroker.GetItemByLocalId(MappingConstants.const_unresolved_meters_default_classification_id);
					if (ci == null){
						ci = CreateUnknownMetersClassification(classificationItemBroker);
					}
				}
			}

			return ci;
		}


		/// <summary>
		///  Creates the meter data from a mapping record.
		/// </summary>
		/// <param name="mappingRecord">The mapping record.</param>
		/// <param name="action">The action (destroy/update/create).</param>
		/// <returns></returns>
		private MeterData CreateMeterData(IMappingRecord mappingRecord, string action){
			var entity = mappingRecord.Entity;
			var meterData = entity as MeterData;
			if (meterData == null)
				return null;
			if (!mappingRecord.IsPropertyAssigned("Validity") && (action != CrudAction.Destroy)){
				meterData.Validity = m_DefaultMeterDataValidity;
			}
			return meterData;
		}

		/// <summary>
		/// Creates large amounts of Meter Data items.
		/// </summary>
		/// <param name="items">The items.</param>
		public void CreateBulk(List<MeterData> items){
			IEnumerable<MeterDataBulkOperationResult> createdItems = ((MeterDataBrokerDB) m_brokerDB).ImportBulk(items);

			foreach (var createdItem in createdItems) {
				ExternalCrudNotificationService.SendNotification(createdItem.MeterData, CrudAction.Create);
			}
		}

		private IEnumerable<Tuple<string, FormResponse>> ProcessBulkResults(IEnumerable<MeterDataBulkOperationResult> report, int meterDataCount) {
			int successfulMeterDatasCount = 0;
			int failedMeterDatasCount = 0;
			var unresolvableMeterLocalIds = new SortedSet<string>();
			var responses = new List<Tuple<string, FormResponse>>();

			foreach (MeterDataBulkOperationResult result in report){
				switch (result.ResultCode){
					case MeterDataBulkOperationResultCode.Success:
						successfulMeterDatasCount++;

						if (successfulMeterDatasCount%const_meter_data_energy_aggregator_notification_page_size == 0){
							// We flush every bulk so we won't exceed the maximum request size to the EA.
							ExternalCrudNotificationService.FlushWaitingNotifications();
						}

						break;
					case MeterDataBulkOperationResultCode.ResolveMeterLocalIdFailed:
						// Failure.
						failedMeterDatasCount++;
						unresolvableMeterLocalIds.Add(result.MeterLocalId);
						break;
					case MeterDataBulkOperationResultCode.ValueNaN:
						// Right now do nothing, it will be handled in the form response.
						break;
					default:
						throw new ArgumentOutOfRangeException("MeterDataBulkOperationResultCode");
				}

				Tuple<string, FormResponse> response = ProcessBulkResult(result);
				responses.Add(response);
			}

			if (failedMeterDatasCount > 0){
				string joinedMeterLocalIds = "\"" + string.Join("\", \"", unresolvableMeterLocalIds) + "\"";
				TracingService.Write(TraceEntrySeverity.Warning,
					string.Format(
						"Could not process a total of {0} Meter Data items within this batch of {1} items , since their parent Meter Local Id could not be resolved. These are the unresolved Meter Local Ids: {2}",
						failedMeterDatasCount, meterDataCount, joinedMeterLocalIds));
			}

			return responses;
		}

		/// <summary>
		/// Gets a value indicating whether this broker writes failures to the trace or not.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if this broker writes failures to the trace; otherwise, <c>false</c>.
		/// </value>
		public override bool WritesFailuresToTrace {
			get { return true; }
		}

		/// <summary>
		///     Creates the form response for bulk item.
		/// </summary>
		/// <param name="result">The result.</param>
		/// <returns></returns>
		private Tuple<string, FormResponse> ProcessBulkResult(MeterDataBulkOperationResult result){
			var formResponse = new FormResponse();

			switch (result.ResultCode){
				case MeterDataBulkOperationResultCode.Success:
					formResponse.success = true;
					ExternalCrudNotificationService.SendNotification(result.MeterData, result.CrudAction);
					break;
				case MeterDataBulkOperationResultCode.ResolveMeterLocalIdFailed:
					formResponse.AddError("KeyMeter", string.Format("Cannot resolve parent Meter with local ID '{0}'", result.MeterLocalId));
					break;
				case MeterDataBulkOperationResultCode.ValueNaN:
					formResponse.AddError("Value", "Ignoring meter data with value 'NaN'");
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}

			var bulkResult = new Tuple<string, FormResponse>(MeterData.FormatLocalId(result.MeterData.AcquisitionDateTime.ToUniversalTime(), result.MeterLocalId), formResponse);
			return bulkResult;
		}

		/// <summary>
		/// Gets the complete list of MeterDataRaw (optimized expression of MeterData) grouped by KeyMeter as int, filtered by date range.
		/// </summary>
		/// <param name="startDate">The start date.</param>
		/// <param name="endDate">The end date.</param>
		/// <param name="KeyMeters">The key meters.</param>
		/// <returns></returns>
		public ConcurrentDictionary<int, SortedList<DateTime, MD>> GetAllByDateRange(DateTime? startDate, DateTime? endDate, string[] KeyMeters=null) {
			var retVal = ((MeterDataBrokerDB)m_brokerDB).GetAllByDateRange(startDate, endDate, KeyMeters);
			return retVal;
		}

		/// <summary>
		/// Gets a value indicating the the key should not be overwritten.
		/// </summary>
		protected override bool ShouldOverwriteKeyInMapping {
			get { return false; }
		}

		/// <summary>
		///     Gets the last meter data by key meter.
		/// </summary>
		/// <param name="KeyMeter">The key meter.</param>
		/// <returns></returns>
		public MeterData GetLastByKeyMeter(string KeyMeter){
			var retVal = ((MeterDataBrokerDB)m_brokerDB).GetLastByKeyMeter(KeyMeter);
			return retVal;
		}



		/// <summary>
		///     Gets the item by local id.
		/// </summary>
		/// <param name="localId">The local id.</param>
		/// <returns></returns>
		public override MeterData GetItemByLocalId(string localId){
			// The meter data key is a combination of the meter key and the date/time.
			// So we let the database update existing meter data records.
			return null;
		}

		/// <summary>
		///     Deletes the meter data by date range as a bulk operation.
		/// </summary>
		/// <param name="keyMeter">The key meter.</param>
		/// <param name="startDate">The start date.</param>
		/// <param name="endDate">The end date.</param>
		/// <param name="pagingLimit">The paging limit.</param>
		/// <returns></returns>
		public bool DeleteByDateRangeBulk(string keyMeter, DateTime startDate, DateTime endDate, int pagingLimit){
			List<MeterData> report = ((MeterDataBrokerDB)m_brokerDB).DeleteByDateRangeBulk(keyMeter, startDate, endDate, pagingLimit);

			foreach (var meterData in report) {
				SendCrudNotification(meterData, CrudAction.Destroy);
			}

			bool hasMore = report.Count == pagingLimit;
			return hasMore;
		}

		/// <summary>
		///     Shoulds the audit.
		/// </summary>
		/// <param name="typeName">Name of the type.</param>
		/// <returns></returns>
		protected override bool ShouldAudit(string typeName = null){
			return false;
		}
	}
}

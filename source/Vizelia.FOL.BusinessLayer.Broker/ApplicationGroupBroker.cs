﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Security;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Helpers;
using Vizelia.FOL.DataLayer.BrokerDB;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.BusinessLayer.Broker {

	/// <summary>
	/// The broker for ApplicationGroup.
	/// </summary>
	public class ApplicationGroupBroker : MappingEnabledBroker<ApplicationGroup, ApplicationGroupBrokerDB> {
		private readonly IFOLRoleProvider m_ProviderRole;

		//private OccupantBroker m_OccupantBroker;
		/// <summary>
		/// Public ctor.
		/// </summary>
		public ApplicationGroupBroker() {
			m_ProviderRole = (IFOLRoleProvider)Roles.Provider;
		}

		/// <summary>
		/// Resolves the references for import.
		/// </summary>
		/// <param name="mappingRecord">The mapping record.</param>
		/// <param name="persistedEntity">The persisted entity.</param>
		protected override void ResolveReferencesForImport(MappingRecord<ApplicationGroup> mappingRecord, ApplicationGroup persistedEntity) {
			// Do nothing.
		}

		/// <summary>
		/// Resolves the references for export.
		/// </summary>
		/// <param name="entity">The entity.</param>
		protected override void ResolveReferencesForExport(ApplicationGroup entity) {
			// Do nothing.
		}

		/// <summary>
		/// Gets all items.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="total">The total.</param>
		/// <param name="useSecurableFilter">if set to <c>true</c> [use securable filter].</param>
		/// <param name="isHierarchyEnabled">if set to <c>true</c> [is hierarchy enabled].</param>
		/// <param name="fields">The fields.</param>
		/// <returns>
		/// A list of items
		/// </returns>
		public override List<ApplicationGroup> GetAll(PagingParameter paging, PagingLocation location, out int total, bool useSecurableFilter = true, bool isHierarchyEnabled = true, params string[] fields) {
			var applicationGroups = m_ProviderRole.ApplicationGroup_GetAll().Select(p => new ApplicationGroup(p));
			applicationGroups = ApplyFilters(paging, applicationGroups);
			var results = applicationGroups.ToList();
			total = results.Count;
			var pagedRecords = results.Rank(paging.sort, paging.dir, paging.start, paging.limit);
			return pagedRecords;
		}

		/// <summary>
		/// Applies the filters.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="applicationGroups">The application groups.</param>
		/// <returns></returns>
		private static IEnumerable<ApplicationGroup> ApplyFilters(PagingParameter paging, IEnumerable<ApplicationGroup> applicationGroups) {
			if (paging.filters != null) {
				var nameFilter = paging.filters.FirstOrDefault(f => f.field == "Name");
				if (nameFilter != null) {
					string nameFilterValue = nameFilter.data.value;
					applicationGroups = applicationGroups.Where(ap => ap.Name.ToLower().Contains(nameFilterValue.ToLower()));
				}

				var descriptionFilter = paging.filters.FirstOrDefault(f => f.field == "Description");
				if (descriptionFilter != null) {
					string descriptionFilterValue = descriptionFilter.data.value;
					applicationGroups = applicationGroups.Where(ap => ap.Description.ToLower().Contains(descriptionFilterValue.ToLower()));
				}
			}
			return applicationGroups;
		}

		/// <summary>
		/// Creates an application group in the database
		/// </summary>
		/// <param name="item">The application group.</param>
		/// <returns></returns>
		public override ApplicationGroup Create(ApplicationGroup item) {
			var retVal = m_ProviderRole.ApplicationGroup_Create(item);
			if (retVal != null) {
				AuditAction(CrudAction.Create, null, retVal);
			}
			return retVal;
		}

		/// <summary>
		/// Updates the specified item.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <param name="changedFields">The changed fields.</param>
		/// <returns></returns>
		public override ApplicationGroup Update(ApplicationGroup item, ref List<Tuple<string, string, string>> changedFields) {
			ValidationHelper.Validate(item);
			var key = item.Id;
			ApplicationGroup persistedItem = null;
			if (key != null && ShouldAudit()) {
				persistedItem = GetItem(key);
			}
			var retVal = m_ProviderRole.ApplicationGroup_Update(item);
			if (key != null && retVal != null) {
				changedFields = AuditAction(CrudAction.Update, persistedItem, retVal);
			}
			return retVal;
		}

		/// <summary>
		/// Deletes the specified item.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <returns></returns>
		private bool Delete(ApplicationGroup item) {
			return Delete(item, null);
		}

		/// <summary>
		/// Deletes the specified item.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <param name="persistedItem">The persisted item.</param>
		/// <returns></returns>
		public override bool Delete(ApplicationGroup item, ApplicationGroup persistedItem = null) {
			if (ShouldAudit()) {
				if (persistedItem == null && item.Id != null) {
					persistedItem = GetItem(item.Id);
				}
				if (persistedItem == null && item.Name != null) {
					persistedItem = GetItemByLocalId(item.Name);
				}
			}

			var success = m_ProviderRole.ApplicationGroup_Delete(item);

			if (success) {
				AuditAction(CrudAction.Destroy, persistedItem, null);
			}
			return success;
		}

		/// <summary>
		/// Gets the item by local id.
		/// </summary>
		/// <param name="localId">The local id.</param>
		/// <returns></returns>
		public override ApplicationGroup GetItemByLocalId(string localId) {
			return m_ProviderRole.ApplicationGroup_GetItemByName(localId);
		}

		/// <summary>
		/// Saves the application group authorization item.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="applicationGroups">The application groups.</param>
		public void SaveApplicationGroupAuthorizationItem(AuthorizationItem entity, CrudStore<ApplicationGroup> applicationGroups) {
			m_ProviderRole.CreateAzManItemApplicationGroups(entity, applicationGroups);
		}

		/// <summary>
		/// Saves the store of Application Groups.
		/// </summary>
		/// <param name="store">The crud store to save.</param>
		/// <returns></returns>
		public override JsonStore<ApplicationGroup> SaveStore(CrudStore<ApplicationGroup> store) {
			var result = new JsonStore<ApplicationGroup>();
			using (var t = Helper.CreateTransactionScope()) {
				SaveCrudStore(CrudAction.Destroy, store.destroy, result, Delete);
				SaveCrudStore(CrudAction.Create, store.create, result, Create);
				SaveCrudStore(CrudAction.Update, store.update, result, Update);
				if (result.success) {
					t.Complete();
				}
				else {
					result.records = null;
				}
			}

			return result;
		}

		private delegate K CrudDelegate<T, K>(T item);
		private delegate K CrudWithFieldDelegate<T, K>(T item, ref List<Tuple<string, string, string>> changedFields);


		/// <summary>
		/// Saves a list of item. The responseStore argument should be instanciated outside of the function.
		/// </summary>
		/// <param name="action">The action (create, update, destroy).</param>
		/// <param name="items">The list of items to save.</param>
		/// <param name="responseStore">The response.</param>
		/// <param name="crud">The crud function.</param>
		private void SaveCrudStore<T, K>(string action, List<T> items, JsonStore<T> responseStore, CrudWithFieldDelegate<T, K> crud) where T : BaseBusinessEntity {
			List<Tuple<string, string, string>> changedFields = null;

			if (items == null)
				return;
			if (responseStore == null)
				responseStore = new JsonStore<T>();
			foreach (T item in items) {
				try {
					crud(item, ref changedFields);
				}
				catch (Exception ex) {
					responseStore.success = false;
					responseStore.message = ex.Message;
					return;
				}
			}
		}



		/// <summary>
		/// Saves a list of item. The responseStore argument should be instanciated outside of the function.
		/// </summary>
		/// <param name="action">The action (create, update, destroy).</param>
		/// <param name="items">The list of items to save.</param>
		/// <param name="responseStore">The response.</param>
		/// <param name="crud">The crud function.</param>
		private void SaveCrudStore<T, K>(string action, List<T> items, JsonStore<T> responseStore, CrudDelegate<T, K> crud) where T : BaseBusinessEntity {
			if (items == null)
				return;
			if (responseStore == null)
				responseStore = new JsonStore<T>();
			foreach (T item in items) {
				try {
					crud(item);
				}
				catch (Exception ex) {
					responseStore.success = false;
					responseStore.message = ex.Message;
					return;
				}

			}

		}

		/// <summary>
		/// Gets the tree.
		/// </summary>
		/// <param name="Key">The key.</param>
		/// <returns></returns>
		public override List<TreeNode> GetTree(string Key) {
			return m_ProviderRole.ApplicationGroup_GetTree();
		}

		/// <summary>
		/// Gets the item.
		/// </summary>
		/// <param name="Key">The key.</param>
		/// <returns></returns>
		public override ApplicationGroup GetItem(string Key) {
			return m_ProviderRole.ApplicationGroup_GetItem(Key);
		}

		/// <summary>
		/// Gets a list of auhorization for a specific application groups.
		/// </summary>
		/// <param name="filterId">The AzMan filter id for which to retreive the authorizations.</param>
		/// <returns></returns>
		public List<ApplicationGroup> GetListByAzManFilter(string filterId) {
			return m_ProviderRole.AzManFilter_Authorization_GetList(filterId);
		}

		/// <summary>
		/// Gets the store by user.
		/// </summary>
		/// <param name="username">The username.</param>
		/// <returns></returns>
		public List<ApplicationGroup> GetStoreByUser(string username) {
			return m_ProviderRole.User_ApplicationGroup_GetList(username);
		}

		/// <summary>
		/// Invalidates the storage cache.
		/// </summary>
		public void InvalidateStorageCache() {
			m_ProviderRole.InvalidateStorageCache();
		}
	}
}

﻿using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.DataLayer.BrokerDB;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for business entity Space.
	/// </summary>
	public class SpaceBroker : MappingEnabledBroker<Space, SpaceBrokerDB> {
		/// <summary>
		/// Resolves the references for export.
		/// </summary>
		/// <param name="entity">The entity.</param>
		protected override void ResolveReferencesForExport(Space entity) {
			ResolveParentReferenceForExport<BuildingStorey>(e => e.KeyBuildingStorey, entity);
		}

		/// <summary>
		/// Resolves the references during mapping.
		/// </summary>
		/// <param name="mappingRecord">The mapping record.</param>
		/// <param name="persistedEntity">The persisted entity.</param>
		protected override void ResolveReferencesForImport(MappingRecord<Space> mappingRecord, Space persistedEntity) {
			ResolveParentReferenceForImport<BuildingStorey>(e => e.KeyBuildingStorey, mappingRecord.Entity, persistedEntity);
		}
	}
}

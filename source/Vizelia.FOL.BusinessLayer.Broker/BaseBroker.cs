﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Transactions;
using Microsoft.Practices.EnterpriseLibrary.Validation.PolicyInjection;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.BusinessEntities;
using Vizelia.FOL.Common.Helpers;
using Vizelia.FOL.Common.Localization;
using Vizelia.FOL.Common.Validators;
using Vizelia.FOL.DataLayer;
using Vizelia.FOL.DataLayer.BrokerDB;
using Vizelia.FOL.DataLayer.Interfaces;
using Vizelia.FOL.Providers;


namespace Vizelia.FOL.BusinessLayer.Broker {

	/// <summary>
	/// Base business layer class for business entity broker.
	/// </summary>
	/// <typeparam name="TEntity">The business entity.</typeparam>
	/// <typeparam name="TEntityBrokerDB">The data layer broker.</typeparam>
	public abstract class BaseBroker<TEntity, TEntityBrokerDB> : MarshalByRefObject, IBroker<TEntity>, IBusinessEntityBroker
		where TEntity : BaseBusinessEntity, new()
		where TEntityBrokerDB : BaseBrokerDBGeneric<TEntity>, new() {

		/// <summary>
		/// The name of the id attribute of the business entity.
		/// </summary>
		protected string idProperty;
		/// <summary>
		/// The database provider.
		/// </summary>
		protected BaseBrokerDBGeneric<TEntity> m_brokerDB;
		/// <summary>
		/// The CoreDataAccess object. Needed for accessing hierarchies function
		/// </summary>
		protected ICoreDataAccess m_coreDataAccess;
		/// <summary>
		/// Defines the possible errors returned from database.
		/// each broker should populate this dictionnary.
		/// </summary>
		protected DatabaseErrors m_database_errors;
		/// <summary>
		/// The document broker DB
		/// </summary>
		protected DocumentBrokerDB m_DocumentBrokerDB;

        /// <summary>
        /// The portal window broker DB
        /// </summary>
        protected PortalWindowBrokerDB m_PortalWindowBrokerDB;

        /// <summary>
        /// The audit entity broker DB
        /// </summary>
		private AuditEntityBroker m_AuditEntityBroker;


		/// <summary>
		/// Public ctor.
		/// </summary>
		protected BaseBroker() {
			// We initiate the idProperty of the business entity, so we can use it when return batch save errors.
			idProperty = Helper.GetPropertyNameId(typeof(TEntity));
			if (!(this is AuditEntityBroker)) {
				m_AuditEntityBroker = Helper.CreateInstance<AuditEntityBroker>();
			}
			m_database_errors = new DatabaseErrors();
			m_database_errors.Add(100, Langue.error_msg_uniquefield, "LocalId");
			m_brokerDB = Helper.CreateInstance<TEntityBrokerDB>();
			m_DocumentBrokerDB = Helper.CreateInstance<DocumentBrokerDB>();
		    m_PortalWindowBrokerDB = Helper.CreateInstance<PortalWindowBrokerDB>();
			m_coreDataAccess = Helper.Resolve<ICoreDataAccess>();
		}



		/// <summary>
		/// Cleans the documents.
		/// </summary>
		/// <param name="item">The item.</param>
		public void CleanDocuments(TEntity item) {

			CleanPsetDocuments(item);

			var itemWithDocuments = item as ISupportDocuments;
			if (itemWithDocuments == null)
				return;

			var persistedItem = GetItem(item);
			var persistedItemWithDocuments = persistedItem as ISupportDocuments;
			if (persistedItemWithDocuments != null) {
				//have to create the document broker here otherwise it causes an inifinte loop, because DocumentBroker:BaseBroker
				var documentBroker = Helper.CreateInstance<DocumentBroker>();
				documentBroker.CleanDocuments(persistedItemWithDocuments, itemWithDocuments);
			}
		}

		/// <summary>
		/// Cleans the pset documents.
		/// </summary>
		/// <param name="item">The item.</param>
		private void CleanPsetDocuments(TEntity item) {
			var ifcItem = item as IfcBaseBusinessEntity;
			if (ifcItem == null)
				return;
			var psetItem = item as IPset;
			if (psetItem == null)
				return;
			if (psetItem.PropertySetList == null)
				return;

			PsetDefinitionBroker psetDefinitionBroker = Helper.CreateInstance<PsetDefinitionBroker>();
			var psetDefinitions = psetDefinitionBroker.GetListByUsageName(ifcItem.IfcType);
			foreach (var pset in psetItem.PropertySetList) {
				var psetDefinition = psetDefinitions.FirstOrDefault(psetDef => psetDef.KeyPropertySet == pset.KeyPset);
				if (psetDefinition != null) {
					CleanPsetDocuments(pset, psetDefinition);
				}
			}
		}

		/// <summary>
		/// Deletes the old documents from psets
		/// </summary>
		/// <param name="pset">The pset.</param>
		/// <param name="psetDefinition">The pset definition.</param>
		private void CleanPsetDocuments(Pset pset, PsetDefinition psetDefinition) {

			//Cant make this a field because it would cause an inifite loop because PsetBroker inherits from BaseBroker
			var psetBroker = Helper.CreateInstance<PsetBroker>();
			Pset oldItem = psetBroker.GetItem(psetDefinition.PsetName, pset.KeyObject);

			if (oldItem != null) {
				//have to create the document broker here otherwise it causes an inifinte loop, because DocumentBroker:BaseBroker
				var documentBroker = Helper.CreateInstance<DocumentBroker>();
				documentBroker.CleanPsetDocuments(pset, oldItem, psetDefinition);
			}
		}

        /// <summary>
        /// Clears the cache.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns> the list of charts that was cleared from the cache</returns>
		private static List<string> ClearCache(TEntity entity) {
			var retChartKeys = new List<string>();
            if (entity == null || entity is ChartCustomGridCell)
                return retChartKeys;

			var entityType = entity.GetType();

			// Does entity have a KeyChart property?
			var prop = entityType.GetProperty(Helper.GetPropertyNameId(typeof(Chart)));
			if (prop != null) {
				var keyChart = prop.GetValue(entity, null);
				if (keyChart == null)
                    return retChartKeys;

				Chart chart;
				var key = keyChart.ToString();

				if (entity is Chart) {
					chart = entity as Chart;
				}
				else {
					var chartBroker = Helper.CreateInstance<ChartBroker>();
					chart = chartBroker.GetItem(key);
				}

				if (chart != null) {
					ChartHelper.Chart_ClearCache(key, chart.KeyDynamicDisplay);
                    retChartKeys.Add(chart.KeyChart);
				}
			}
			// DataSerie
			else {
				prop = entityType.GetProperty(Helper.GetPropertyNameId(typeof(DataSerie)));
				if (prop != null) {
					var keyDataSerie = prop.GetValue(entity, null);
					if (keyDataSerie != null) {
						var brokerDataSerie = Helper.CreateInstance<DataSerieBroker>();
						var dataserie = brokerDataSerie.GetItem(keyDataSerie.ToString());

						if (dataserie != null) {
							ChartHelper.Chart_ClearCache(dataserie.KeyChart);
                            retChartKeys.Add(dataserie.KeyChart);
						}
					}
				}
				// Chart Axis
				else {
					prop = entityType.GetProperty(Helper.GetPropertyNameId(typeof(ChartAxis)));
					if (prop != null) {
						var keyChartAxis = prop.GetValue(entity, null);
						if (keyChartAxis != null) {
							var brokerChartAxis = Helper.CreateInstance<ChartAxisBroker>();
							var chartaxis = brokerChartAxis.GetItem(keyChartAxis.ToString());
						    if (chartaxis != null) {
						        ChartHelper.Chart_ClearCache(chartaxis.KeyChart);
                                retChartKeys.Add(chartaxis.KeyChart);
						    }
						}
					}
				}
				// Dynamic Display
				prop = entityType.GetProperty(Helper.GetPropertyNameId(typeof(DynamicDisplay)));
				if (prop != null) {
					var keyDynamicDisplay = prop.GetValue(entity, null);
					if (keyDynamicDisplay != null) {
						var chartBroker = Helper.CreateInstance<ChartBroker>();
						var chartKeys = chartBroker.GetKeysByKeyDynamicDisplay(keyDynamicDisplay.ToString());
						ChartHelper.DynamicDisplay_ClearCache(keyDynamicDisplay.ToString(), chartKeys);
                        retChartKeys.AddRange(chartKeys);
					}
				}
			}
            return retChartKeys;
        }

		/// <summary>
		/// Copy an item in the database
		/// </summary>
		/// <param name="item">The item to copy.
		/// The use of "this" inside this function will not use PI. So we must pass instead the PI proxy from the outside.</param>
		/// <param name="formResponse">The response of the operation.</param>
		/// <param name="creator">The creator.</param>
		/// <param name="incrementTitle">if set to <c>true</c> [increment title].</param>
		/// <returns></returns>
		public virtual TEntity Copy(TEntity item, out FormResponse formResponse, string creator = null, bool incrementTitle = true) {
			item.SetPropertyValue(Helper.GetPropertyNameId(typeof(TEntity)), null);
			if (Helper.IsSecurableType(typeof(TEntity)) && !string.IsNullOrEmpty(creator)) {
				((ISecurableObject)item).Creator = creator;
			}
			var portlet = item as IPortlet;
			if (incrementTitle && portlet != null) {
				portlet.SetTitle(ChartHelper.Increment(portlet.GetTitle()));
			}

			return Create(item, out formResponse);
		}

		/// <summary>
		/// Creates an item in the database.
		/// The function will validate the business entity before processing.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns>The item created</returns>
		public virtual TEntity Create(TEntity item) {
			Validate(item);

			// detects if the item is participating in a workflow and if true initializes the InstanceId when it is null.
			IWorkflowEntity workflow = item as IWorkflowEntity;
			if (workflow != null && workflow.InstanceId == Guid.Empty) {
				workflow.InstanceId = Guid.NewGuid();
			}
			ChangeItemProperties(item);
			SaveDocuments(item);
			var retVal = m_brokerDB.Create(item);
			if (retVal != null) {
				AuditAction(CrudAction.Create, null, retVal);
			}
			return retVal;
		}

		/// <summary>
		/// Validates the specified item.
		/// </summary>
		/// <param name="item">The item.</param>
		protected virtual void Validate(TEntity item) {
			ValidationHelper.Validate(item);
		}

		/// <summary>
		/// Returns whether the entity should be audited or not.
		/// </summary>
		/// <param name="typeName">Name of the type.</param>
		/// <returns>True if yes; otherwise false.</returns>
		protected virtual bool ShouldAudit(string typeName = null) {
			typeName = GetAuditTypeName(typeName);

			if (m_AuditEntityBroker == null) {
				m_AuditEntityBroker = Helper.CreateInstance<AuditEntityBroker>();
			}
			var auditEntityList = m_AuditEntityBroker.GetListAggregated();

			bool shouldAuditEntity = auditEntityList.Any(x => x.EntityName.Equals(typeName) && x.IsAuditEnabled);

			return shouldAuditEntity;
		}

		/// <summary>
		/// Returns whether the Audit the should return changed fields even if not should be audited.
		/// </summary>
		/// <returns></returns>
		protected virtual bool ShouldReturnAuditChangedFields() {
			return false;
		}

		/// <summary>
		/// Audits the action.
		/// </summary>
		/// <param name="crudAction">The crud action.</param>
		/// <param name="deleted">The deleted.</param>
		/// <param name="inserted">The inserted.</param>
		/// <param name="typeName">Name of the type.</param>
		protected List<Tuple<string, string, string>> AuditAction(string crudAction, TEntity deleted, TEntity inserted, string typeName = null) {
			var auditFields = new List<Tuple<string, string, string, bool>>();
			bool shouldAudit = ShouldAudit(typeName);
			typeName = GetAuditTypeName(typeName);

			if (!shouldAudit && !ShouldReturnAuditChangedFields()) return new List<Tuple<string, string, string>>();

			var type = typeof(TEntity);
			var keyAttribute = Helper.GetAttribute<KeyAttribute>(type);
			var properties = type.GetProperties();
			var keyProperty = properties.FirstOrDefault(prop => prop.Name == keyAttribute.PropertyNameId);
			if (keyProperty != null) {
				string dmlType = "";
				string keyValue = null;
				switch (crudAction) {
					case CrudAction.Create:
						keyValue = keyProperty.GetValue(inserted, null) as string;
						dmlType = "I";
						break;
					case CrudAction.Update:
						keyValue = keyProperty.GetValue(inserted, null) as string;
						dmlType = "U";
						break;
					case CrudAction.Destroy:
						keyValue = keyProperty.GetValue(deleted, null) as string;
						dmlType = "D";
						break;
				}
				auditFields = GetTypeAuditFields(deleted, inserted, properties);
				if (shouldAudit && auditFields.Any(x => x.Item4)) {
					Helper.StartNewThread(() => {
						var modifiedBy = Helper.GetCurrentUserName();
						var changeTime = DateTime.UtcNow;
						var auditParametersDictionary = CreateAuditParametersDictionary(typeName, keyValue, keyProperty.Name, dmlType, modifiedBy, auditFields, changeTime);
						m_brokerDB.SaveAuditAction(auditParametersDictionary);
					}, false);

				}
			}
			return auditFields.Where(x => x.Item4).Select(x => new Tuple<string, string, string>(x.Item1, x.Item2, x.Item3)).ToList();
		}

		private static List<Tuple<string, string, string, bool>> GetTypeAuditFields(object deleted, object inserted, IEnumerable<PropertyInfo> properties, string namePrefix = "") {
			var auditFields = new List<Tuple<string, string, string, bool>>();
			foreach (var propertyInfo in properties) {
				object insertedValue = null;
				object deletedValue = null;
				if (propertyInfo.HasAttribute<AuditListAttribute>()) {
					if (inserted != null) {
						var insertedList = propertyInfo.GetValue(inserted, null) as IEnumerable;
						if (insertedList != null) {
							var insertedValueText = insertedList.Cast<object>().Aggregate(string.Empty, (current, item) => (current + (item + ",")));
							insertedValue = !string.IsNullOrEmpty(insertedValueText) ? insertedValueText.Remove(insertedValueText.Length - 1) : insertedValueText;
						}
					}
					if (deleted != null) {
						var deletedList = propertyInfo.GetValue(deleted, null) as IEnumerable;
						if (deletedList != null) {
							var deletedValueText = deletedList.Cast<object>().Aggregate(string.Empty, (current, item) => (current + (item + ",")));
							deletedValue = !string.IsNullOrEmpty(deletedValueText) ? deletedValueText.Remove(deletedValueText.Length - 1) : deletedValueText;
						}
					}
				}
				else {
					insertedValue = inserted != null ? propertyInfo.GetValue(inserted, null) : null;
					deletedValue = deleted != null ? propertyInfo.GetValue(deleted, null) : null;
				}
				var propertyName = namePrefix + propertyInfo.Name;
				var propertyType = propertyInfo.PropertyType;
				if (propertyInfo.HasAttribute<AuditedClassAttribute>()) {
					auditFields.AddRange(GetTypeAuditFields(deletedValue, insertedValue, propertyType.GetProperties(), propertyName + "_"));
				}
				else {
					var deletedValueString = deletedValue != null ? deletedValue.ToString() : null;
					var insertedValueString = insertedValue != null ? insertedValue.ToString() : null;
					var isFieldChanged = inserted == null || deleted == null
						|| (insertedValue != null && deletedValue != null && !insertedValueString.Equals(deletedValueString))
						|| (insertedValue == null && deletedValue != null)
						|| (insertedValue != null && deletedValue == null);
					auditFields.Add(new Tuple<string, string, string, bool>(propertyName, deletedValueString, insertedValueString, isFieldChanged));

					if (propertyInfo.HasAttribute<AuditKeyAttribute>()) {
						var businessEntityType = propertyInfo.GetAttributeValueAsObject<AuditKeyAttribute>("BaseBusinessEntityType") as Type;
						auditFields.Add(
							new Tuple<string, string, string, bool>(
								propertyName + "_ToString",
								deletedValue != null ? GetKeyDescription(deletedValueString, businessEntityType) : null,
								insertedValue != null ? GetKeyDescription(insertedValueString, businessEntityType) : null,
								isFieldChanged));
					}
				}
			}
			return auditFields;
		}

		private string GetAuditTypeName(string typeName) {
			Type type = typeof(TEntity);
			string auditTypeName = (typeName != null) ? string.Format("{0}.{1}", type.Name, typeName) : type.Name;

			return auditTypeName;
		}

		private static string GetKeyDescription(string key, Type baseBusinessEntityType) {
			var broker = BrokerFactory.CreateBroker(baseBusinessEntityType) as IBusinessEntityBroker;
			var retVal = broker == null ? null : broker.GetItem(key).ToString();
			return retVal;
		}

		private Dictionary<string, object> CreateAuditParametersDictionary(string name, string keyValue, string keyName, string dmlType, string modifiedBy, IEnumerable<Tuple<string, string, string, bool>> allFields, DateTime changeTime) {
			var parametersDictionary = new Dictionary<string, object>
			                           	{
			                           		{"TenantName", ContextHelper.ApplicationName},
			                           		{"TableName", name},
			                           		{"DMLType", dmlType},
			                           		{"PrimaryKeyField", keyName},
			                           		{"PrimaryKeyValue", keyValue},
			                           		{"ChangeTime", changeTime},
			                           		{"ModifiedBy", modifiedBy}
			                           	};

			var i = 0;
			var changedFieldsText = "";

			foreach (var field in allFields) {
				parametersDictionary.Add(string.Format("FieldName{0}", i), field.Item1);
				parametersDictionary.Add(string.Format("OldValue{0}", i), field.Item2);
				parametersDictionary.Add(string.Format("NewValue{0}", i), field.Item3);
				if (field.Item4) {
					changedFieldsText += field.Item1 + ',';
				}
				i++;
			}

			// filling in the fields up to 160.
			for (; i < 160; i++) {
				parametersDictionary.Add(string.Format("FieldName{0}", i), null);
				parametersDictionary.Add(string.Format("OldValue{0}", i), null);
				parametersDictionary.Add(string.Format("NewValue{0}", i), null);
			}

			changedFieldsText = changedFieldsText.Remove(changedFieldsText.Length - 1);

			parametersDictionary.Add("ChangedFields", changedFieldsText);
			return parametersDictionary;
		}

		/// <summary>
		/// Changes the item properties before it is created.
		/// </summary>
		/// <param name="item">The item.</param>
		protected virtual void ChangeItemProperties(TEntity item) { }

		/// <summary>
		/// Creates an item in the database
		/// </summary>
		/// <param name="item">The item to create.
		/// The use of "this" inside this function will not use PI. So we must pass instead the PI proxy from the outside. 
		/// </param>
		/// <param name="formResponse">The response of the operation.</param>
		/// <returns></returns>
		protected virtual TEntity Create(TEntity item, out FormResponse formResponse) {
			return Save(CrudAction.Create, item, out formResponse);
		}

		/// <summary>
		/// Deletes an item from database.
		/// </summary>
		/// <param name="Key">The Key of the item to delete.</param>
		/// <returns>True if the delete operation succeeded, false otherwise.</returns>
		public virtual bool Delete(string Key) {
			var persistedItem = GetItem(Key);
			if (persistedItem == null) return false;
			return Delete(persistedItem, persistedItem);
		}

		/// <summary>
		/// Deletes an item from database.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <param name="persistedItem">The persisted item.</param>
		/// <returns>
		/// True if the delete operation succeeded, false otherwise.
		/// </returns>
		public virtual bool Delete(TEntity item, TEntity persistedItem = null) {
			if (ShouldAudit()) {
				persistedItem = persistedItem ?? GetItem(item.GetKey());
			}

			var workflow = item as IWorkflowEntity;

			if (workflow != null) {
				m_brokerDB.WorkflowDeleteInstance(workflow.InstanceId);
			}

			DeleteDocuments(item);
            var chartKeys = ClearCache(item);
			bool success = m_brokerDB.Delete(item);

            UpdatePortalWindowModifiedDate(item, chartKeys, CrudAction.Destroy);

			SendCrudNotification(item, CrudAction.Destroy);
			if (ShouldAudit() && success) {
				AuditAction(CrudAction.Destroy, persistedItem, null);
			}

			return success;
		}

		/// <summary>
		/// Removes the associated documents of the item we are going to delete
		/// </summary>
		/// <param name="item">The item.</param>
		private void DeleteDocuments(TEntity item) {
			var itemWithDocuments = item as ISupportDocuments;
			if (itemWithDocuments == null) {
				return;
			}
			//have to create the document broker here otherwise it causes an inifinte loop, because DocumentBroker:BaseBroker
			var documentBroker = Helper.CreateInstance<DocumentBroker>();
			documentBroker.DeleteDocuments(itemWithDocuments);
		}

		/// <summary>
		/// Gives a chance to replace the suplied values for filter parameters with a drill down tree of values.
		/// Should be implemented in any broker that exposes filter that are hierarchies innerjoin (but not innerpath!).
		/// An example of implementation is : 
		/// <example>
		/// var filterLocation = PagingParameter.GetFilter("KeyLocation", paging);
		///	if (filterLocation != null) {
		///		filterLocation.data.SetListValue(m_coreDataAccess.GetSpatialHierarchy(filterLocation.data.GetArrayValue(), "", HierarchyDirection.DESC));
		///	}
		/// </example>
		/// </summary>
		/// <param name="paging">The paging parameters..</param>
		protected virtual void DrillDownFilters(PagingParameter paging) { }

		private string FormatError(List<FormError> errors) {
			var q = from p in errors
					select String.Format("error for field {0} : {1}", p.id, p.msg);
			return String.Join("<br/>", q.ToArray());
		}

		/// <summary>
		/// Creates an item and return a valid FormResponse for Extjs.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <returns></returns>
		public FormResponse FormCreate(TEntity item) {
			FormResponse formResponse;
			Create(item, out formResponse);
			return formResponse;
		}

		/// <summary>
		/// Gets an item and returns a json form response.
		/// </summary>
		/// <param name="Key">The Key of the item.</param>
		/// <returns>A json form response encapsulating the item.</returns>
		public FormResponse FormLoad(string Key) {
			ValidationHelper.ThrowOnArgumentNull(Key, "Key");
			return GetItem(Key).ToFormResponse();
		}

		/// <summary>
		/// Gets an item and returns a json form response.
		/// </summary>
		/// <param name="Key">The Key of the item.</param>
		/// <param name="fields">The fields.</param>
		/// <returns>
		/// A json form response encapsulating the item.
		/// </returns>
		public FormResponse FormLoad(string Key, params string[] fields) {
			ValidationHelper.ThrowOnArgumentNull(Key, "Key");
			return GetItem(Key, fields).ToFormResponse();
		}

		/// <summary>
		/// Updates an item and return a valid FormResponse for Extjs.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <returns></returns>
		public FormResponse FormUpdate(TEntity item) {
			FormResponse formResponse;
			Update(item, out formResponse);
			return formResponse;
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse FormUpdateBatch(string[] keys, TEntity item) {
			FormResponse response = new FormResponse();

			try {
				ExternalCrudNotificationService.ThrowOnNotReady();
				SaveDocuments(item);
				int result = m_brokerDB.UpdateBatch(keys, item);
				response.success = true;
			}
			catch (Exception ex) {
				response.success = false;
				response.msg = ex.Message;
			}
			if (item is IEnergyAggregatorNotifyableEntity) {
				foreach (var key in keys) {
					var entity = GetItem(key);
					SendCrudNotification(entity, CrudAction.Update);
				}
			}
			return response;
		}

		/// <summary>
		/// Gets all items without paging, filtering or ordering.
		/// </summary>
		/// <param name="useSecurableFilter">if set to <c>true</c> [use securable filter].</param>
		/// <returns>
		/// A list of items
		/// </returns>
		public virtual List<TEntity> GetAll(bool useSecurableFilter = true) {
			int total;
			var paging = new PagingParameter { limit = 0 };
			return m_brokerDB.GetAll(paging, PagingLocation.Memory, out total, useSecurableFilter);
		}

		/// <summary>
		/// Gets all items.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="total">The total.</param>
		/// <param name="useSecurableFilter">if set to <c>true</c> [use securable filter].</param>
		/// <param name="isHierarchyEnabled">if set to <c>true</c> [is hierarchy enabled].</param>
		/// <param name="fields">The fields.</param>
		/// <returns>
		/// A list of items
		/// </returns>
		public virtual List<TEntity> GetAll(PagingParameter paging, PagingLocation location, out int total, bool useSecurableFilter = true, bool isHierarchyEnabled = true, params string[] fields) {
			DrillDownFilters(paging);
			m_brokerDB.IsHierarchical = isHierarchyEnabled && m_brokerDB.IsHierarchical;
			var entities = m_brokerDB.GetAll(paging, location, out total, useSecurableFilter);

			if (entities != null && entities.Any() && fields != null && fields.Length > 0) {
				// Load in one round trip to the DB all the psets of the entities.
				Dictionary<string, List<Pset>> psetDictionary = null;
				if (typeof (IPset).IsAssignableFrom(typeof (TEntity)) && fields.Contains("PropertySetList")) {
					psetDictionary = PopulatePropertySet(entities.Select(x => x.GetKey()).ToArray())
						.GroupBy(x => x.KeyObject, x => x).ToDictionary(x => x.Key, x => x.ToList());
				}

				foreach (var entity in entities) {
					PopulateList(entity, EntityLoadMode.Standard, fields);

					if (psetDictionary != null && psetDictionary.ContainsKey(entity.GetKey())) {
						((IPset) entity).PropertySetList = psetDictionary[entity.GetKey()];
				}
			}
			}

			return entities;
		}

		/// <summary>
		/// Gets the children of a specific item.
		/// </summary>
		/// <param name="Key">The Key of the parent</param>
		/// <returns>The list of children.</returns>
		public virtual List<TEntity> GetChildren(string Key) {
			return m_brokerDB.GetChildren(Key);
		}

		/// <summary>
		/// Gets a item.
		/// </summary>
		/// <param name="Key">The Key of the item.</param>
		/// <returns>The item.</returns>
		public virtual TEntity GetItem(string Key) {
			ValidationHelper.ThrowOnArgumentNull(Key, "Key");
			return m_brokerDB.GetItem(Key);
		}

		/// <summary>
		/// Gets a item.
		/// </summary>
		/// <param name="Key">The Key of the item.</param>
		/// <param name="fields">the list of field to fetch. "*" to fetch all children fields.</param>
		/// <returns>The item.</returns>
		public TEntity GetItem(string Key, params string[] fields) {
			ValidationHelper.ThrowOnArgumentNull(Key, "Key");
			var item = GetItem(Key);

			if (item != null && fields != null && fields.Length > 0) {
				PopulateList(item, EntityLoadMode.Standard, fields);
				PopulatePsetForItem(fields, item);
			}

			return item;
		}

		private static void PopulatePsetForItem(string[] fields, TEntity item) {
			var psetItem = item as IPset;
			if (psetItem != null && fields.Contains("PropertySetList")) {
				psetItem.PropertySetList = PopulatePropertySet(item.GetKey());
			}

			if (psetItem != null && fields.Contains("PsetValues")) {
				psetItem.PsetValues = PopulatePsetValues(item.GetKey());
			}
		}

		/// <summary>
		/// Populates the property set of an business entity with the specified pset name.
		/// </summary>
		/// <param name="keys">The key.</param>
		/// <returns>
		/// The business entity with attribute.
		/// </returns>
		public static List<Pset> PopulatePropertySet(params string[] keys) {
			PsetBroker broker = Helper.CreateInstance<PsetBroker>();
			var psetList = broker.GetList(keys);
			return psetList;
		}

		/// <summary>
		/// Populates the pset values.
		/// </summary>
		/// <param name="keys">The key.</param>
		/// <returns>
		/// The business entity with attribute.	
		/// </returns>
		public static List<PsetValue> PopulatePsetValues(params string[] keys) {
			var broker = Helper.CreateInstance<PsetValueBroker>();
			var psetList = broker.GetList(keys);
			return psetList;
		}

		/// <summary>
		/// Gets a item by its localId.
		/// </summary>
		/// <param name="localId">The LocalId of the item.</param>
		/// <returns></returns>
		public virtual TEntity GetItemByLocalId(string localId) {
			ValidationHelper.ThrowOnArgumentNull(localId, "localId");
			var item = m_brokerDB.GetItemByLocalId(localId);
			return item;
		}

		/// <summary>
		/// Gets a item by its localId.
		/// </summary>
		/// <param name="localId">The LocalId of the item.</param>
		/// <param name="throwWhenItemNotFound">if set to <c>true</c> [throw when item not found].</param>
		/// <returns></returns>
		public virtual TEntity GetItemByLocalId(string localId, bool throwWhenItemNotFound) {
			var item = GetItemByLocalId(localId);
			if (item == null && throwWhenItemNotFound) {
				throw new Exception(string.Format("{0} with LocalId '{1}' not found", typeof(TEntity).Name, localId));
			}
			return item;
		}

		/// <summary>
		/// Gets a list of items.
		/// </summary>
		/// <param name="Keys">The array of Keys.</param>
		/// <returns>The list of corresponding items.</returns>
		public virtual List<TEntity> GetList(string[] Keys) {
			ValidationHelper.ThrowOnArgumentNull(Keys, "Keys");
			return m_brokerDB.GetList(Keys);
		}

		/// <summary>
		/// Gets the persisted version of this item.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <returns>The persisted version of the object we are now updating</returns>
		private TEntity GetItem(TEntity item) {
			var keyAttribute = Helper.GetAttribute<KeyAttribute>(typeof(TEntity));
			var keyProperty = typeof(TEntity).GetProperties().FirstOrDefault(prop => prop.Name == keyAttribute.PropertyNameId);
			if (keyProperty != null) {
				var key = keyProperty.GetValue(item, null) as string;
				var persistedObject = m_brokerDB.GetItem(key);
				return persistedObject;
			}
			return null;
		}

		/// <summary>
		/// Populate the chidren items specified as fields. "*" indicates that all fields should be populated.
		/// </summary>
		/// <param name="item">The item</param>
		/// <param name="loadMode">The load mode.</param>
		/// <param name="forcePopulate">if set to <c>true</c> [force populate].</param>
		/// <param name="fields">The list of fields. "*" indicates all fields.</param>
		public virtual void PopulateList(TEntity item, EntityLoadMode loadMode, bool forcePopulate = false, params string[] fields) {
		}

		/// <summary>
		/// Populate the chidren items specified as fields. "*" indicates that all fields should be populated.
		/// </summary>
		/// <param name="item">The item</param>
		/// <param name="loadMode">The load mode.</param>
		/// <param name="fields">The list of fields. "*" indicates all fields.</param>
		public virtual void PopulateList(TEntity item, EntityLoadMode loadMode, params string[] fields) {
		}

		/// <summary>
		/// Populate the chidren items specified as fields. "*" indicates that all fields should be populated.
		/// </summary>
		/// <param name="item">The item</param>
		/// <param name="fields">The list of fields. "*" indicates all fields.</param>
		public void PopulateList(TEntity item, params string[] fields) {
			PopulateList(item, EntityLoadMode.Standard, fields);
		}
		/// <summary>
		/// Populates the list of fields with filtered list results.
		/// </summary>
		/// <param name="items">The items.</param>
		/// <param name="fields">The fields.</param>
		public void PopulateList(List<TEntity> items, params string[] fields) {
			if (items == null)
				return;

			foreach (TEntity item in items) {
				PopulateList(item, EntityLoadMode.Standard, fields);
			}
		}

		/// <summary>
		/// Populates the list of fields with filtered list results.
		/// </summary>
		/// <param name="items">The items.</param>
		/// <param name="loadMode">The load mode.</param>
		/// <param name="fields">The fields.</param>
		public void PopulateList(List<TEntity> items, EntityLoadMode loadMode, params string[] fields) {
			if (items == null)
				return;

			foreach (TEntity item in items) {
				PopulateList(item, loadMode, fields);
			}
		}

		/// <summary>
		/// Gets all the items and returns a JsonStore.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="location">The location of parameter.</param>
		/// <param name="useSecurableFilter">if set to <c>true</c> [use securable filter].</param>
		/// <param name="fields">The fields.</param>
		/// <returns>
		/// A JsonStore.
		/// </returns>
		public virtual JsonStore<TEntity> GetStore(PagingParameter paging, PagingLocation location, bool useSecurableFilter = true, params string[] fields) {
			int total;
			return GetAll(paging, location, out total, useSecurableFilter, fields: fields).ToJsonStore(total, paging);
		}

		/// <summary>
		/// Generates the json structure of the business entity
		/// </summary>
		/// <param name="Key">The Key of the parent node.</param>
		/// <returns>A list of treenodes in json.</returns>
		public virtual List<TreeNode> GetTree(string Key) {
			ValidationHelper.ThrowOnArgumentNull(Key, "Key");
			if (typeof(TEntity).GetInterface(typeof(ITreeNode).FullName) == null)
				throw new ArgumentException(String.Format("Type {0} does not expose ITreeNode interface", typeof(TEntity).FullName));

			List<TEntity> list = GetChildren(Key);

			var nodes = list.Cast<ITreeNode>().Select(node => node.GetTree());

			return nodes.ToList();
		}

		/// <summary>
		/// Copy an item in the database
		/// </summary>
		/// <param name="item">The item to copy.
		/// The use of "this" inside this function will not use PI. So we must pass instead the PI proxy from the outside.</param>
		/// <param name="formResponse">The response of the operation.</param>
		/// <param name="Creator">The creator.</param>
		/// <param name="incrementTitle"></param>
		/// <returns></returns>
		BaseBusinessEntity IBusinessEntityBroker.Copy(BaseBusinessEntity item, out FormResponse formResponse, string Creator, bool incrementTitle) {
			var entity = Copy((TEntity)item, out formResponse, Creator, incrementTitle);
			return entity;
		}

		/// <summary>
		/// Deletes an item from database.
		/// </summary>
		/// <param name="Key">The Key of the item to delete.</param>
		/// <returns>
		/// True if the delete operation succeeded, false otherwise.
		/// </returns>
		bool IBusinessEntityBroker.Delete(string Key) {
			var retVal = Delete(Key);
			return retVal;
		}

		/// <summary>
		/// Gets all items without paging, filtering or ordering.
		/// </summary>
		/// <returns>A list of items</returns>
		List<BaseBusinessEntity> IBusinessEntityBroker.GetAll() {
			return GetAll().Cast<BaseBusinessEntity>().ToList();
		}


		/// <summary>
		/// Gets all items without paging, filtering or ordering.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="total">The total of results.</param>
		/// <returns>
		/// A list of items
		/// </returns>
		List<BaseBusinessEntity> IBusinessEntityBroker.GetAll(PagingParameter paging, PagingLocation location, out int total) {
			return GetAll(paging, location, out total).Cast<BaseBusinessEntity>().ToList();
		}

		/// <summary>
		/// Gets a item.
		/// </summary>
		/// <param name="Key">The Key of the item.</param>
		/// <returns>The item.</returns>
		BaseBusinessEntity IBusinessEntityBroker.GetItem(string Key) {
			return GetItem(Key);
		}

		/// <summary>
		/// Gets a item.
		/// </summary>
		/// <param name="Key">The Key of the item.</param>
		/// <param name="fields">the list of field to fetch. "*" to fetch all children fields.</param>
		/// <returns>The item.</returns>
		BaseBusinessEntity IBusinessEntityBroker.GetItem(string Key, params string[] fields) {
			return GetItem(Key, fields);
		}

		/// <summary>
		/// Saves the specified item.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <returns></returns>
		BaseBusinessEntity IBusinessEntityBroker.Save(BaseBusinessEntity item) {
			var entity = Save((TEntity)item);
			return entity;
		}

		/// <summary>
		/// Saves the specified item.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <returns></returns>
		public virtual TEntity Save(TEntity item) {
			FormResponse formResponse;
			var retVal = ((IBusinessEntityBroker)this).Save(item, out formResponse) as TEntity;
			return retVal;
		}

		/// <summary>
		/// Saves the specified item, returning the entity and the formResponse.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <param name="formResponse">Output the results. Check it for errors.</param>
		/// <returns>The entity or null when there was an error. Check formResponse for details.</returns>
		BaseBusinessEntity IBusinessEntityBroker.Save(BaseBusinessEntity item, out FormResponse formResponse) {
			if (!(item is TEntity))
				throw new NotSupportedException(String.Format("Item must inherit from {0}", typeof(TEntity).Name));
			var key = item.GetKey();
			string action = String.IsNullOrEmpty(key) ? "create" : "update";
			var retVal = Save(action, (TEntity)item, out formResponse);
			return retVal;
		}

		/// <summary>
		/// Saves a list of item. The responseStore argument should be instanciated outside of the function.
		/// </summary>
		/// <param name="action"></param>
		/// <param name="items"></param>
		/// <param name="responseStore"></param>
		private void Save(string action, List<TEntity> items, JsonStore<TEntity> responseStore) {
			if (items == null)
				return;

			responseStore = responseStore ?? new JsonStore<TEntity>();
			foreach (TEntity item in items) {
				FormResponse formResponse;
				Save(action, item, out formResponse);
				if (!formResponse.success) {
					responseStore.success = false;
					responseStore.message += String.Format(Langue.msg_store_error_summary,
															action,
															item.GetPropertyValue(idProperty),
															formResponse.errors != null ? FormatError(formResponse.errors) : formResponse.msg);
				}
				else
					responseStore.records.Add((TEntity)formResponse.data);
			}
		}

		/// <summary>
		/// Saves a item in database. No CallValidationHandler here because we want to get any error in the FormResponse.
		/// </summary>
		/// <param name="action">The save action. (update, create)</param>
		/// <param name="item">The item to save.</param>
		/// <param name="formResponse">The form response.</param>
		/// <returns></returns>
		private TEntity Save(string action, TEntity item, out FormResponse formResponse) {
			formResponse = new FormResponse { success = false };
			try {
				TEntity result;
				using (TransactionScope t = Helper.CreateTransactionScope()) {
					result = SaveEntity(action, item, formResponse);
					SavePset(action, item, formResponse);
					t.Complete();
				}
				return result;
			}

			catch (VizeliaDatabaseException ex) {
				formResponse.success = false;
				if (!m_database_errors.ContainsKey(ex.Status)) {
					formResponse.msg = ex.Message;
					formResponse.exception = ex;
				}
				else {
					DatabaseError error = m_database_errors[ex.Status];
					string msg = Langue.ResourceManager.GetString(error.msg);
					if (String.IsNullOrEmpty(msg))
						msg = error.msg;
					// if the field was provided with error code then we add it to the collection so it will be visible next to the field in the UI. 
					if (!String.IsNullOrEmpty(error.field))
						formResponse.AddError(error.field, msg);
					else
						formResponse.msg = msg;

				// These errors are really a kind of validation error and should 
				// report as such.
					formResponse.exception = ConstraintValidator.MakeValidationException(error.field, msg, item);
				}
			}
			catch (ArgumentValidationException ex) {
				formResponse.success = false;
				formResponse.exception = ex;
				var query = from r in ex.ValidationResults
							group r by r.Key into g
							select new FormError { id = g.Key, msg = g.First().Message };

				formResponse.errors = query.ToList();
			}
			catch (MembershipException ex) {
				formResponse.exception = ex;
				formResponse.AddError(ex.Id, ex.Message);
			}
			catch (Exception ex) {
				formResponse.success = false;
				formResponse.msg = ex.Message;
				formResponse.exception = ex;
			}

			return default(TEntity);
		}


		/// <summary>
		/// Converts the documents guids to document ids.
		/// Inspects the object by reflection to find the properties marked with the appropriate attribute
		/// </summary>
		/// <param name="item">The item.</param>
		protected virtual void SaveDocuments(TEntity item) {
			var psetItem = item as Pset;
			var itemWithDocuments = item as ISupportDocuments;
			if (psetItem == null && itemWithDocuments == null)
				return;

			//have to create the document broker here otherwise it causes an inifinte loop, because DocumentBroker:BaseBroker
			var documentBroker = Helper.CreateInstance<DocumentBroker>();
			if (psetItem != null) {
				documentBroker.SaveDocumentPsets(psetItem);
			}

			if (itemWithDocuments != null) {
				documentBroker.SaveDocuments(itemWithDocuments);
			}
		}

		/// <summary>
		/// Saves the entity.
		/// </summary>
		/// <param name="action">The action.</param>
		/// <param name="item">The item.</param>
		/// <param name="formResponse">The form response.</param>
		/// <returns></returns>
		private TEntity SaveEntity(string action, TEntity item, FormResponse formResponse) {
			TEntity result = null;
			List<Tuple<string, string, string>> changedFields = null;
			switch (action) {
				case CrudAction.Create:
					result = Create(item);
					break;
				case CrudAction.Update:
					result = Update(item, ref changedFields);
					break;
				case CrudAction.Destroy:
					Delete(item);
					break;
			}

            var entity = action == CrudAction.Destroy ? item : result;

			SendCrudNotification(entity, action);
		    var chartKeys = ClearCache(entity);

		    UpdatePortalWindowModifiedDate(entity, chartKeys, action);

			formResponse.data = result;
			formResponse.success = true;
			return result;
		}

        /// <summary>
        /// Updates the portal window modified date.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="chartKeys">The chart keys.</param>
        /// <param name="action">The action.</param>
	    private void UpdatePortalWindowModifiedDate(TEntity entity, List<string> chartKeys, string action) {
            // alarm table, chart, drawing canvas, flip card, map, weather location, web frame
            if (entity is IPortlet) {
                List<string> portalWindowsKeys = m_PortalWindowBrokerDB.GetListByPortlet(entity.GetKey(), entity.GetType().FullName).Select(p => p.KeyPortalWindow).ToList();
                m_PortalWindowBrokerDB.UpdateDateTimeModified(portalWindowsKeys);
            }

            // portal tab, portal column, portlet
            var portalElement = entity as IPortalWindowElement;
            if (portalElement != null) {
                m_PortalWindowBrokerDB.UpdateDateTimeModified(portalElement.KeyPortalWindow);
            }

            // charts
            if (chartKeys != null) {
                foreach (var key in chartKeys) {
                    List<string> portalWindowsKeys = m_PortalWindowBrokerDB.GetListByPortlet(key, typeof(Chart).FullName).Select(p => p.KeyPortalWindow).ToList();
                    m_PortalWindowBrokerDB.UpdateDateTimeModified(portalWindowsKeys);
                } 
            }
        }

	    /// <summary>
		/// Saves the pset.
		/// </summary>
		/// <param name="action">The action.</param>
		/// <param name="item">The item.</param>
		/// <param name="formResponse">The form response.</param>
		private void SavePset(string action, TEntity item, FormResponse formResponse) {
			// saves psets.
			var entityPset = item as IPset;
			var ifcEntity = item as IfcBaseBusinessEntity;

			if (ifcEntity != null && entityPset != null && entityPset.PropertySetList != null && entityPset.PropertySetList.Count > 0) {
				string ifcType = ifcEntity.IfcType;
				PsetBroker brokerPset = Helper.CreateInstance<PsetBroker>();

				switch (action) {
					case CrudAction.Create:
						brokerPset.Save(formResponse, entityPset.PropertySetList);
						break;
					case CrudAction.Update:
						brokerPset.Save(formResponse, entityPset.PropertySetList);
						string attributeValue = Helper.GetAttributeValue<PsetClassificationItemKeyAttribute>(item.GetType());
						if (!String.IsNullOrEmpty(attributeValue)) {
							string keyClassificationItem = item.GetPropertyValueString(attributeValue, false, false);
							if (!String.IsNullOrEmpty(keyClassificationItem)) {
								brokerPset.CleanObsoleteValues(item.GetPropertyValueString(idProperty, false, false), ifcType, keyClassificationItem);
							}
						}
						break;
				}
			}
		}

		/// <summary>
		/// Saves a CRUD request.
		/// Transactional.
		/// </summary>
		/// <param name="store">The crud store that need to saved.</param>
		/// <returns></returns>
		public virtual JsonStore<TEntity> SaveStore(CrudStore<TEntity> store) {
			JsonStore<TEntity> result = new JsonStore<TEntity>();
			using (TransactionScope t = Helper.CreateTransactionScope()) {
				Save(CrudAction.Destroy, store.destroy, result);
				Save(CrudAction.Create, store.create, result);
				Save(CrudAction.Update, store.update, result);
				if (!result.success) {
					result.records = null;
					return result;
				}
				t.Complete();
			}
			return result;

		}

		/// <summary>
		/// Send the Crud notification to the External Crud Provider.
		/// WARNING: Only use this method if you are performing a bulk CRUD operation.
		/// Otherwise, single entity CRUD operations are already calling this method via BaseBroker methods.
		/// Perform "Find Usages" first to see whether it is really needed to directly call this method or not.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="action">The action.</param>
		protected void SendCrudNotification(TEntity entity, string action) {
			/* Did you read the warning in the documentation above, before calling this method? */
			if (entity == null)
				return;
			ExternalCrudNotificationService.SendNotification(entity, action);
		}


		/// <summary>
		/// Updates an item in the database.
		/// The function will validate the business entity before processing.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <param name="changedFields">The changed fields.</param>
		/// <returns>
		/// The item updated
		/// </returns>
		public virtual TEntity Update(TEntity item, ref List<Tuple<string, string, string>> changedFields) {
			Validate(item);

			UpdateDocuments(item);

			var key = item.GetKey();
			TEntity persistedItem = null;

			var mappableItem = item != null ? item as IMappableEntity : null;
			bool checkLocalId = mappableItem != null && DoesLocalIdUpdateTaskExist();

			if (key != null && (ShouldAudit() || ShouldReturnAuditChangedFields() || checkLocalId)) {
				persistedItem = m_brokerDB.GetItem(key);
				if (checkLocalId) {
					ValidateLocalIdUpdate(mappableItem, persistedItem as IMappableEntity);
			}
			}

			TEntity retVal = m_brokerDB.Update(item);

			if (key != null && retVal != null) {
				changedFields = AuditAction(CrudAction.Update, persistedItem, retVal);
			}

			return retVal;
		}

		/// <summary>
		/// LocalId task name of the current entity.
		/// </summary>
		private string LocalIdTaskName {
			get {

				//Getting the task name corresponding to the entity
				var localIdUpdateTaskName = string.Format("@@@@ {0} - LocalId Update", typeof (TEntity).Name);
				return localIdUpdateTaskName;
			}
		}

		/// <summary>
		/// Checks in the Azman if the user allowed to update local id.
		/// </summary>
		/// <returns></returns>
		private bool IsUserAllowedUpdateLocalIdInAzman() {
			bool isAllowed = true;

			var currentUser = Helper.GetCurrentUserName();
			var provider = ((IFOLRoleProvider)System.Web.Security.Roles.Provider);

			bool doesUserHasTask = provider.IsUserInRole(currentUser, LocalIdTaskName);
			if (!doesUserHasTask) {
				isAllowed = false;
			}

			return isAllowed;
		}

		/// <summary>
		/// Checks if local id task of the entity exists in Azman.
		/// </summary>
		/// <returns></returns>
		private bool DoesLocalIdUpdateTaskExist() {
			var provider = ((IFOLRoleProvider)System.Web.Security.Roles.Provider);
			var allTasks = CacheService.Get(CacheKey.const_cache_all_tasks, provider.AzManTask_GetAll);
			bool doesTaskExist = allTasks.Any(p => p.Name.Equals(LocalIdTaskName));
			return doesTaskExist;
		}

		/// <summary>
		/// Validating wheter the user is allowed to update the local id corresponding to the entity and the persisted entity values.
		/// </summary>
		/// <param name="entity"></param>
		/// <param name="persistedEntity"></param>
		private void ValidateLocalIdUpdate(IMappableEntity entity, IMappableEntity persistedEntity) {

			if (persistedEntity == null) {
				throw new ArgumentNullException("No persisted entity was found");
			}
			if (entity == null) {
				throw new ArgumentNullException("No entity was found");
			}

			var entityLocalIdValue = entity.LocalId;
			var persistedLocalIdValue = persistedEntity.LocalId;
			
			if (entityLocalIdValue != persistedLocalIdValue) {
				if (!IsUserAllowedUpdateLocalIdInAzman()) {
					throw new VizeliaValidationException(Langue.msg_local_id_change_authorization_error);
				}
			}
		}
		

		/// <summary>
		/// Updates the documents of the item. Used in update scenario.
		/// </summary>
		/// <param name="item">The item.</param>
		protected virtual void UpdateDocuments(TEntity item) {
			CleanDocuments(item);
			SaveDocuments(item);
		}

		/// <summary>
		/// Updates an item in the database.
		/// </summary>
		/// <param name="item">The item to update.
		/// The use of "this" inside this function will not use PI. So we must pass instead the PI proxy from the outside. 
		/// </param>
		/// <param name="formResponse">The response of the operation.</param>
		/// <returns></returns>
		protected virtual TEntity Update(TEntity item, out FormResponse formResponse) {
			return Save(CrudAction.Update, item, out formResponse);
		}


		/// <summary>
		/// Gets the business entity broker.
		/// </summary>
		/// <param name="TypeEntity">The type entity.</param>
		/// <returns>
		/// The broker of the contained entity
		/// </returns>
		protected static IBusinessEntityBroker GetBusinessEntityBroker(string TypeEntity) {
			// The default type is Chart.
			Type entityType = typeof(Chart);
			if (!string.IsNullOrEmpty(TypeEntity)) {
				entityType = Helper.GetPortletEntityType(TypeEntity) ?? entityType;
			}

			object brokerObject = BrokerFactory.CreateBroker(entityType);
			var broker = (IBusinessEntityBroker)brokerObject;
			return broker;
		}
	}
}

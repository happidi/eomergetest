﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.DataLayer.BrokerDB;

namespace Vizelia.FOL.BusinessLayer.Broker {

	/// <summary>
	/// Meter Data Binary Broker.
	/// </summary>
	public class MeterDataBinaryBroker : BaseBroker<MeterDataBinary, MeterDataBinaryBrokerDB> {

		/// <summary>
		/// Get the total count of meter data inside the table.
		/// </summary>
		/// <returns></returns>
		public int GetCount() {
			return ((MeterDataBinaryBrokerDB)m_brokerDB).GetCount();
		}

		/// <summary>
		/// Gets the most requested meters that feats into the specified memory.
		/// </summary>
		/// <param name="maxMemorySize">Size of the max memory.</param>
		/// <returns></returns>
		public List<MeterDataBinary> GetMostRequested(double maxMemorySize) {
			return ((MeterDataBinaryBrokerDB)m_brokerDB).GetMostRequested(maxMemorySize);
		
		}

		/// <summary>
		/// Increase the requested count of a specific meter.
		/// </summary>
		/// <param name="KeyMeter">The key meter.</param>
		public void IncreaseRequestCount(int KeyMeter) {
			((MeterDataBinaryBrokerDB)m_brokerDB).IncreaseRequestCount(KeyMeter);
		}

	}
}

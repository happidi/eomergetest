﻿using System;
using System.Collections.Generic;
using System.Linq;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using Vizelia.FOL.DataLayer.BrokerDB;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for business entity PsetAttributeHistorical.
	/// </summary>
	public class PsetAttributeHistoricalBroker : MappingEnabledBroker<PsetAttributeHistorical, PsetAttributeHistoricalBrokerDB> {

		/// <summary>
		/// Gets a dictionnary for all the PsetAttributeHistorical, by KeyPsetAttributeHistorical. 
		/// </summary>
		/// <returns></returns>
		public Dictionary<string, PsetAttributeHistorical> GetDictionary() {
			var retVal = ((PsetAttributeHistoricalBrokerDB)m_brokerDB).GetAll().ToDictionary(x => x.KeyPsetAttributeHistorical);
			return retVal;
		}

		/// <summary>
		/// Gets a store of PsetAttributeHistorical for a specific Object.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="keyObject">The key object.</param>
		/// <param name="keyPropertySingleValue">The key property single value.</param>
		/// <returns>
		/// A JsonStore.
		/// </returns>
		public JsonStore<PsetAttributeHistorical> GetStoreByKeyObject(PagingParameter paging, string keyObject, string keyPropertySingleValue) {
			int total;
			return ((PsetAttributeHistoricalBrokerDB)m_brokerDB).GetAllPagingByKeyObject(new PagingParameter(), keyObject, keyPropertySingleValue, out total).ToJsonStoreWithFilter(paging);
		}

		/// <summary>
		/// Get the last PsetAttributeHistorical from a Object.
		/// </summary>
		/// <param name="keyObject">The key object.</param>
		/// <param name="keyPropertySingleValue">The key property single value.</param>
		/// <returns></returns>
		public PsetAttributeHistorical GetLastByKeyObject(string keyObject, string keyPropertySingleValue) {
			return ((PsetAttributeHistoricalBrokerDB)m_brokerDB).GetLastByKeyObject(keyObject, keyPropertySingleValue);
		}

		/// <summary>
		/// Resolves the references for export.
		/// </summary>
		/// <param name="entity">The entity.</param>
		protected override void ResolveReferencesForExport(PsetAttributeHistorical entity) {
			ResolveParentReferenceForExport<PsetAttributeDefinition>(e => e.KeyPropertySingleValue, entity);
			if (Enum.IsDefined(typeof(HierarchySpatialTypeName), entity.UsageName)) {
				var locationType = entity.UsageName.ParseAsEnum<HierarchySpatialTypeName>();
				switch (locationType) {
					case HierarchySpatialTypeName.IfcSite:
						ResolveParentReferenceForExport<Site>("KeyObject", entity);
						break;
					case HierarchySpatialTypeName.IfcBuilding:
						ResolveParentReferenceForExport<Building>("KeyObject", entity);
						break;
					case HierarchySpatialTypeName.IfcBuildingStorey:
						ResolveParentReferenceForExport<BuildingStorey>("KeyObject", entity);
						break;
					case HierarchySpatialTypeName.IfcSpace:
						ResolveParentReferenceForExport<Space>("KeyObject", entity);
						break;
					case HierarchySpatialTypeName.IfcFurniture:
						ResolveParentReferenceForExport<Furniture>("KeyObject", entity);
						break;
					case HierarchySpatialTypeName.None:
						throw new ArgumentException("Value of LocationTypeName cannot be set to None when KeyLocation is assigned a value.");
					default:
						throw new ArgumentException(string.Format("Unexpected HierarchySpatialTypeName value."));
				}
			}
		}

		/// <summary>
		/// Resolves the references during mapping.
		/// </summary>
		/// <param name="mappingRecord">The mapping record.</param>
		/// <param name="persistedEntity">The persisted entity.</param>
		protected override void ResolveReferencesForImport(MappingRecord<PsetAttributeHistorical> mappingRecord, PsetAttributeHistorical persistedEntity) {
			ResolveParentReferenceForImport<PsetAttributeDefinition>(e => e.KeyPropertySingleValue, mappingRecord.Entity, persistedEntity);
			if (Enum.IsDefined(typeof(HierarchySpatialTypeName), mappingRecord.Entity.UsageName)) {
				var locationType = mappingRecord.Entity.UsageName.ParseAsEnum<HierarchySpatialTypeName>();
				switch (locationType) {
					case HierarchySpatialTypeName.IfcSite:
						ResolveParentReferenceForImport<Site>("KeyObject", mappingRecord.Entity, persistedEntity);
						break;
					case HierarchySpatialTypeName.IfcBuilding:
						ResolveParentReferenceForImport<Building>("KeyObject", mappingRecord.Entity, persistedEntity);
						break;
					case HierarchySpatialTypeName.IfcBuildingStorey:
						ResolveParentReferenceForImport<BuildingStorey>("KeyObject", mappingRecord.Entity, persistedEntity);
						break;
					case HierarchySpatialTypeName.IfcSpace:
						ResolveParentReferenceForImport<Space>("KeyObject", mappingRecord.Entity, persistedEntity);
						break;
					case HierarchySpatialTypeName.IfcFurniture:
						ResolveParentReferenceForImport<Furniture>("KeyObject", mappingRecord.Entity, persistedEntity);
						break;
					case HierarchySpatialTypeName.IfcMeter:
						ResolveParentReferenceForImport<Meter>("KeyObject", mappingRecord.Entity, persistedEntity);
						break;
					case HierarchySpatialTypeName.None:
						throw new ArgumentException("Value of LocationTypeName cannot be set to None when KeyLocation is assigned a value.");
					default:
						throw new ArgumentException(string.Format("Unexpected HierarchySpatialTypeName value."));
				}
			}
		}

		/// <summary>
		/// Get LatestHistoricalPset entities By location key
		/// </summary>
		/// <param name="keyLocation">The location key</param>
		/// <returns></returns>
		public List<LatestHistoricalPset> GetLatestHistoricalPsetValuesByLocationKey(string keyLocation) {
			var results = ((PsetAttributeHistoricalBrokerDB)m_brokerDB).LatestHistoricalPsetGetValuesByLocationKey(keyLocation);
			return results;
		}

		/// <summary>
		/// Get LatestHistoricalPset entities By Pset
		/// </summary>
		/// <param name="keyPropertySet">the pset key</param>
		/// <returns></returns>
		public List<LatestHistoricalPset> GetLatestHistoricalPsetValuesOfLocationsByPset(string keyPropertySet) {
			var results = ((PsetAttributeHistoricalBrokerDB)m_brokerDB).LatestHistoricalPsetGetValuesOfLocationsByPset(keyPropertySet);
			return results;
		}
	}
}
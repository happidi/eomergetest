﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.DataLayer.BrokerDB;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Localization;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for business entity Logging.
	/// </summary>
	public class LoggingBroker : BaseBroker<Logging, LoggingBrokerDB> {

		/// <summary>
		/// Gets a store of Loggins for a specific method.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="methodName">Name of the method.</param>
		/// <returns></returns>
		public JsonStore<Logging> GetStoreByMethodName(PagingParameter paging, string methodName) {
			int total;
			if (paging.filters == null)
				paging.filters = new List<GridFilter>();

			paging.filters.Add(new GridFilter {
				data = new GridData {
					comparison = "eq",
					type = "string",
					value = methodName
				},
				field = "CallerKey"
			});
			return m_brokerDB.GetAll(paging, PagingLocation.Database, out total).ToJsonStore(total, paging);
		}

		/// <summary>
		/// Deletes the by caller key.
		/// </summary>
		/// <param name="callerKey">The caller key.</param>
		/// <returns></returns>
		public bool DeleteByCallerKey(string callerKey) {
			return ((LoggingBrokerDB)m_brokerDB).DeleteByCallerKey(callerKey);
		}

		/// <summary>
		/// Purges the specified days to keep logs.
		/// </summary>
		/// <param name="daysToKeepLogs">The days to keep logs.</param>
		public void Purge(int daysToKeepLogs) {
			((LoggingBrokerDB)m_brokerDB).Purge(daysToKeepLogs);
		}
	}

}
﻿using System.Collections.Generic;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.DataLayer.BrokerDB;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for business entity Mail.
	/// </summary>
	public class MailBroker : MappingEnabledBroker<Mail, MailBrokerDB> {
		/// <summary>
		/// constant for ServiceDesk classification local id
		/// </summary>
		public const string const_servicedesk = "MAIL_ServiceDesk";
		/// <summary>
		/// constant for ResetPassword classification local id
		/// </summary>
		public const string const_resetpassword = "MAIL_ResetPassword";
		/// <summary>
		/// constant for ForgotPassword classification local id
		/// </summary>
		public const string const_forgotpassword = "MAIL_ForgotPassword";
		/// <summary>
		/// constant for InitTenant classification local id
		/// </summary>
		public const string const_inittenant = "MAIL_InitTenant";
		/// <summary>
		/// constant for ChartScheduler classification local id
		/// </summary>
		public const string const_chartscheduler = "MAIL_ChartScheduler";
		/// <summary>
		/// constant for MappingReport classification local id
		/// </summary>
		public const string const_mappingreport = "MAIL_MappingReport";
		/// <summary>
		/// constant for LongRunningOperationResult classification local id
		/// </summary>
		public const string const_longrunningoperationresult = "MAIL_LongRunningOperationResult";
		/// <summary>
		/// constant for Alarm classification local id
		/// </summary>
		public const string const_alarm = "MAIL_Alarm";
		/// <summary>
		/// constant for MeterValidationRule classification local id
		/// </summary>
		public const string const_metervalidationrule = "MAIL_MeterValidationRule";

		/// <summary>
		/// Resolves the references for export.
		/// </summary>
		/// <param name="entity">The entity.</param>
		protected override void ResolveReferencesForExport(Mail entity) {
			ResolveParentReferenceForExport<ClassificationItem>(e => e.KeyClassificationItem, entity);
		}

		/// <summary>
		/// Resolves the references during mapping.
		/// </summary>
		/// <param name="mappingRecord">The mapping record.</param>
		/// <param name="persistedEntity">The persisted entity.</param>
		protected override void ResolveReferencesForImport(MappingRecord<Mail> mappingRecord, Mail persistedEntity) {
			ResolveParentReferenceForImport<ClassificationItem>(e => e.KeyClassificationItem, mappingRecord.Entity, persistedEntity);
		}

		/// <summary>
		/// Gets a store of workflow action elements paging by key mail.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="KeyMail">The key mail.</param>
		/// <returns>The list of WorkflowActionElement.</returns>
		public JsonStore<WorkflowActionElement> GetStoreWorkflowActionElementByKeyMail(PagingParameter paging, string KeyMail) {
			int total;
			return ((MailBrokerDB)m_brokerDB).GetAllWorkflowActionElementPagingByKeyMail(paging, KeyMail, out total).ToJsonStore(total, paging);
		}

		/// <summary>
		/// Saves the associated workflow action elements for a specified mail.
		/// </summary>
		/// <param name="entityMail">The entity mail.</param>
		/// <param name="workflowActionElements">The workflow action elements.</param>
		/// <returns></returns>
		public List<WorkflowActionElement> SaveMailWorkflowActionElements(Mail entityMail, CrudStore<WorkflowActionElement> workflowActionElements) {
			return ((MailBrokerDB)m_brokerDB).SaveMailWorkflowActionElements(entityMail, workflowActionElements);
		}

		/// <summary>
		/// Gets the list of possibles mails from a within a workflow.
		/// </summary>
		/// <param name="classificationItemKeyParent"></param>
		/// <param name="classificationItemKeyChildren"></param>
		/// <param name="priorityID"></param>
		/// <param name="keyLocation">The key location.</param>
		/// <param name="actionId"></param>
		/// <param name="assemblyQualifiedName"></param>
		/// <returns></returns>
		public List<Mail> GetListFromWorkflow(string classificationItemKeyParent, string classificationItemKeyChildren, int priorityID, string keyLocation, string actionId, string assemblyQualifiedName) {
			return ((MailBrokerDB)m_brokerDB).GetListFromWorkflow(classificationItemKeyParent, classificationItemKeyChildren, priorityID, keyLocation, actionId, assemblyQualifiedName);
		}

		/// <summary>
		/// Gets the list by classification item local id.
		/// </summary>
		/// <param name="classificationItemLocalId">The classification item local id.</param>
		/// <returns></returns>
		public List<Mail> GetListByClassificationItemLocalId(string classificationItemLocalId) {
			return ((MailBrokerDB)m_brokerDB).GetListByClassificationLocalId(classificationItemLocalId);
		}
	}
}
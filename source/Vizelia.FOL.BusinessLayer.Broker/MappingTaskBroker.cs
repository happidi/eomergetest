﻿using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.DataLayer.BrokerDB;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for business entity MappingTask.
	/// </summary>
	public class MappingTaskBroker : BaseBroker<MappingTask, MappingTaskBrokerDB> {
		/// <summary>
		/// Validates the specified item.
		/// </summary>
		/// <param name="item">The item.</param>
		protected override void Validate(MappingTask item) {
			IRemoteFileAccessClient client = RemoteFileAccessService.GetClient(item);
			client.Validate();
			base.Validate(item);
		}
	}
}
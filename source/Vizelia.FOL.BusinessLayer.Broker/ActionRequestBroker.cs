﻿using System;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.DataLayer.BrokerDB;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;

namespace Vizelia.FOL.BusinessLayer.Broker {

	/// <summary>
	/// The broker for business entity ActionRequest
	/// </summary>
	public class ActionRequestBroker : MappingEnabledBroker<ActionRequest, ActionRequestBrokerDB> {

		/// <summary>
		/// Gets a item by its instanceId.
		/// </summary>
		/// <param name="instanceId">The instanceId of the item.</param>
		/// <returns></returns>
		public virtual ActionRequest GetItemByInstanceId([NotNullValidator] Guid instanceId) {
			return ((ActionRequestBrokerDB)m_brokerDB).GetItemByInstanceId(instanceId);
		}

		/// <summary>
		/// Prepares to export entity.
		/// </summary>
		/// <param name="entity">The entity.</param>
		protected override void ResolveReferencesForExport(ActionRequest entity) {
			ResolveParentReferenceForExport<ClassificationItem>(e => e.ClassificationKeyParent, entity);
			ResolveParentReferenceForExport<ClassificationItem>(e => e.ClassificationKeyChildren, entity);

			ResolveLocationReferenceForExport(entity);

			ResolveParentReferenceForExport<Occupant>(e => e.Requestor, entity);
			ResolveParentReferenceForExport<Occupant>(e => e.KeyApprover, entity);
			ResolveParentReferenceForExport<Occupant>(e => e.AuditModifiedBy, entity);

		}

		/// <summary>
		/// Resolves the references during mapping.
		/// </summary>
		/// <param name="mappingRecord">The mapping record.</param>
		/// <param name="persistedEntity">The persisted entity.</param>
		protected override void ResolveReferencesForImport(MappingRecord<ActionRequest> mappingRecord, ActionRequest persistedEntity) {
			ResolveParentReferenceForImport<ClassificationItem>(e => e.ClassificationKeyParent, mappingRecord.Entity, persistedEntity);
			ResolveParentReferenceForImport<ClassificationItem>(e => e.ClassificationKeyChildren, mappingRecord.Entity, persistedEntity);
			//TODO: HOW CAN THIS BE AN INT IF ALL KEYS IN THE SYSTEM ARE STRINGS???
			//ResolveParentReferenceForImport<Priority>(e => e.PriorityID, mappingRecord.Entity, persistedEntity);
			
			ResolveLocationReferenceForImport(mappingRecord, persistedEntity);

			ResolveParentReferenceForImport<Occupant>(e => e.Requestor, mappingRecord.Entity, persistedEntity);
			ResolveParentReferenceForImport<Occupant>(e => e.KeyApprover, mappingRecord.Entity, persistedEntity);
			ResolveParentReferenceForImport<Occupant>(e => e.AuditModifiedBy, mappingRecord.Entity, persistedEntity);

		}
	}

}

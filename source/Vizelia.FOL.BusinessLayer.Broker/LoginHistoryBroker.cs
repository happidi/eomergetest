﻿using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.DataLayer.BrokerDB;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for business entity LoginHistory.
	/// </summary>
	public class LoginHistoryBroker : BaseBroker<LoginHistory, LoginHistoryBrokerDB> {
	}

}
﻿using System.Collections.Generic;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.DataLayer.BrokerDB;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for business entity Priority.
	/// </summary>
	public class PriorityBroker : MappingEnabledBroker<Priority, PriorityBrokerDB> {

		/// <summary>
		/// Creates an association between a ClassificationItem and a Priority.
		/// </summary>
		/// <param name="KeyClassificationChildren">The Key of the ClassificationItem.</param>
		/// <param name="KeyClassificationParent">The Key of the parent ClassificationItem in case the priority is added to a relationship.</param>
		/// <param name="KeyPriority">The Key of the priority.</param>
		/// <returns></returns>
		public bool AddPriorityClassification(string KeyClassificationChildren, string KeyClassificationParent, string KeyPriority) {
			return ((PriorityBrokerDB)m_brokerDB).AddPriorityClassification(KeyClassificationChildren, KeyClassificationParent, KeyPriority);
		}

		/// <summary>
		/// Deletes an association between a ClassificationItem and a Priority.
		/// </summary>
		/// <param name="KeyClassificationChildren">The Key of the ClassificationItem.</param>
		/// <param name="KeyClassificationParent">The Key of the parent ClassificationItem in case the priority is added to a relationship.</param>
		/// <returns></returns>
		public bool DeletePriorityClassification(string KeyClassificationChildren, string KeyClassificationParent) {
			return ((PriorityBrokerDB)m_brokerDB).DeletePriorityClassification(KeyClassificationChildren, KeyClassificationParent);
		}


		/// <summary>
		/// Gets the priority assigned to a ClassificationItem.
		/// </summary>
		/// <param name="KeyClassificationChildren">The key classification children.</param>
		/// <param name="KeyClassificationParent">The key classification parent.</param>
		/// <returns></returns>
		public Priority GetItemByClassification(string KeyClassificationChildren, string KeyClassificationParent) {
			return ((PriorityBrokerDB)m_brokerDB).GetItemByClassification(KeyClassificationChildren, KeyClassificationParent);
		}

		/// <summary>
		/// Gets the priority assigned to a any upper level of ClassificationItem Hierarchy.
		/// </summary>
		/// <param name="KeyClassificationChildren">The key classification children.</param>
		/// <param name="KeyClassificationParent">The key classification parent.</param>
		/// <returns></returns>
		public Priority GetItemByClassificationAscendant(string KeyClassificationChildren, string KeyClassificationParent) {
			return ((PriorityBrokerDB)m_brokerDB).GetItemByClassificationAscendant(KeyClassificationChildren, KeyClassificationParent);
		}

		/// <summary>
		/// Gets a json store of priorities for a specific mail
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyMail">The Key of the mail.</param>
		/// <returns>A json store.</returns>
		public JsonStore<Priority> GetStoreByKeyMail(PagingParameter paging, string KeyMail) {
			int total;
			return ((PriorityBrokerDB)m_brokerDB).GetAllPagingByKeyMail(paging, KeyMail, out total).ToJsonStore(total, paging);
		}

		/// <summary>
		/// Saves the mail priorities.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="priorities">The priorities.</param>
		/// <returns></returns>
		public List<Priority> SaveMailPriorities(Mail entity, CrudStore<Priority> priorities) {
			return ((PriorityBrokerDB)m_brokerDB).SaveMailPriorities(entity, priorities);
		}

		/// <summary>
		/// Resolves the references for export.
		/// </summary>
		/// <param name="entity">The entity.</param>
		protected override void ResolveReferencesForExport(Priority entity) {
			// Do nothing.
		}

		/// <summary>
		/// Resolves the references during mapping.
		/// </summary>
		/// <param name="mappingRecord">The mapping record.</param>
		/// <param name="persistedEntity">The persisted entity.</param>
		protected override void ResolveReferencesForImport(MappingRecord<Priority> mappingRecord, Priority persistedEntity) {
			// Do nothing.
		}

	}

}
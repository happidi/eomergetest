﻿using System.Collections.Generic;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.DataLayer.BrokerDB;
using Vizelia.FOL.Common.Localization;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for business entity AlgorithmInputDefinition.
	/// </summary>
	public class AlgorithmInputDefinitionBroker : BaseBroker<AlgorithmInputDefinition, AlgorithmInputDefinitionBrokerDB> {

		/// <summary>
		/// Public ctor.
		/// </summary>
		public AlgorithmInputDefinitionBroker()
			: base() {
				m_database_errors.Add(1, Langue.error_msg_uniquefield, "Name");
		}


		/// <summary>
		/// Get the list of saved AlgorithmInputDefinition from a Algorithm
		/// </summary>
		/// <param name="KeyAlgorithm">The key of the Algorithm.</param>
		public List<AlgorithmInputDefinition> GetListByKeyAlgorithm(string KeyAlgorithm) {
			return ((AlgorithmInputDefinitionBrokerDB)m_brokerDB).GetListByKeyAlgorithm(KeyAlgorithm);
		}

		/// <summary>
		/// Gets a store of AlgorithmInputDefinition for a specific Algorithm.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyAlgorithm">The Key of the Algorithm.</param>
		/// <returns>A JsonStore.</returns>
		public JsonStore<AlgorithmInputDefinition> GetStoreByKeyAlgorithm(PagingParameter paging, string KeyAlgorithm) {
			int total;
			return ((AlgorithmInputDefinitionBrokerDB)m_brokerDB).GetAllPagingByKeyAlgorithm(paging, KeyAlgorithm, out total).ToJsonStore(total, paging);
		}


		/// <summary>
		/// Saves the Algorithm AlgorithmInputDefinitions.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="AlgorithmInputDefinition">The AlgorithmInputDefinition store.</param>
		/// <returns></returns>
		public List<AlgorithmInputDefinition> SaveAlgorithmAlgorithmInputDefinition(Algorithm entity, CrudStore<AlgorithmInputDefinition> AlgorithmInputDefinition) {
			return ((AlgorithmInputDefinitionBrokerDB)m_brokerDB).SaveAlgorithmAlgorithmInputDefinition(entity, AlgorithmInputDefinition);
		}
	}

}
﻿using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.DataLayer.BrokerDB;
using Vizelia.FOL.Common.Localization;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for business entity WeatherLocation.
	/// </summary>
	public class WeatherLocationBroker : MappingEnabledBroker<WeatherLocation, WeatherLocationBrokerDB> {

		/// <summary>
		/// Public ctor.
		/// </summary>
		public WeatherLocationBroker() {
			m_database_errors.Add(1, Langue.error_msg_uniquefield, "LocationId");
		}

		/// <summary>
		/// Resolves the references for export.
		/// </summary>
		/// <param name="entity">The entity.</param>
		protected override void ResolveReferencesForExport(WeatherLocation entity) {
			// Do nothing.
		}

		/// <summary>
		/// Resolves the references during mapping.
		/// </summary>
		/// <param name="mappingRecord">The mapping record.</param>
		/// <param name="persistedEntity">The persisted entity.</param>
		protected override void ResolveReferencesForImport(MappingRecord<WeatherLocation> mappingRecord, WeatherLocation persistedEntity) {
			// Do nothing.
		}

	}

}
﻿using System.Collections.Generic;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.DataLayer.BrokerDB;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for business entity Portlet.
	/// </summary>
	public class PortletBroker : MappingEnabledBroker<Portlet, PortletBrokerDB> {

		/// <summary>
		/// Get the list of saved Portlet from a Portal Column.
		/// </summary>
		/// <param name="KeyPortalColumn">The key of the Portal Column.</param>
		public List<Portlet> GetListByKeyPortalColumn(string KeyPortalColumn) {
			return ((PortletBrokerDB)m_brokerDB).GetListByKeyPortalColumn(KeyPortalColumn);
		}

		/// <summary>
		/// Gets a store of Portlet for a specific Portal Column.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyPortalColumn">The Key of the Portal Column.</param>
		/// <returns>A JsonStore.</returns>
		public JsonStore<Portlet> GetStoreByKeyPortalColumn(PagingParameter paging, string KeyPortalColumn) {
			int total;
			return ((PortletBrokerDB)m_brokerDB).GetAllPagingByKeyPortalColumn(paging, KeyPortalColumn, out total).ToJsonStore(total, paging);
		}

		/// <summary>
		/// Get the list of saved Portlet from a specific Entity.
		/// </summary>
		/// <param name="KeyEntity">The key entity.</param>
		/// <param name="TypeEntity">The type entity.</param>
		/// <returns></returns>
		public List<Portlet> GetListByKeyEntity(string KeyEntity, string TypeEntity) {
			return ((PortletBrokerDB)m_brokerDB).GetListByKeyEntity(KeyEntity, TypeEntity);
		}

		/// <summary>
		/// Gets a store of Portlet for a specific Entity.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyEntity">The key entity.</param>
		/// <param name="TypeEntity">The type entity.</param>
		/// <returns>
		/// A JsonStore.
		/// </returns>
		public JsonStore<Portlet> GetStoreByKeyEntity(PagingParameter paging, string KeyEntity, string TypeEntity) {
			int total;
			return ((PortletBrokerDB)m_brokerDB).GetAllPagingByKeyEntity(paging, KeyEntity, TypeEntity, out total).ToJsonStore(total, paging);
		}

		/// <summary>
		/// Saves the Portal Column Portlet.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="Portlet">The Portlet store.</param>
		/// <returns></returns>
		public List<Portlet> SavePortalColumnPortlet(PortalColumn entity, CrudStore<Portlet> Portlet) {
			return ((PortletBrokerDB)m_brokerDB).SavePortalColumnPortlet(entity, Portlet);
		}

		/// <summary>
		/// Populate the chidren items specified as fields. "*" indicates that all fields should be populated.
		/// </summary>
		/// <param name="item">The item</param>
		/// <param name="loadMode">The load mode.</param>
		/// <param name="fields">The list of fields. "*" indicates all fields.</param>
		public override void PopulateList(Portlet item, EntityLoadMode loadMode, params string[] fields) {
			foreach (string field in fields) {
				if (field.ContainsAny("*", "Entity")) {
					if (item != null && !string.IsNullOrEmpty(item.KeyEntity)) {
						IBusinessEntityBroker broker = GetBusinessEntityBroker(item.TypeEntity);
						if (item.GetEntityType() == typeof(FlipCard)) {
							fields = new[] { "*" };
						}
						var entity = broker.GetItem(item.KeyEntity, fields);
						if (entity != null) {
							var mappingBroker = broker as IMappingBroker;
							if (mappingBroker != null) {
								mappingBroker.ResolveForExport(entity, loadMode);
							}
							item.Entity = entity;

							item.AlarmTableEntity = entity as AlarmTable;
							item.ChartEntity = entity as Chart;
							item.DrawingCanvasEntity = entity as DrawingCanvas;
							item.FlipCardEntity = entity as FlipCard;
							item.MapEntity = entity as Map;
							item.WeatherLocationEntity = entity as WeatherLocation;
							item.WebFrameEntity = entity as WebFrame;
						}
					}
					break;
				}
			}
		}

		/// <summary>
		/// Copy an existing Portlet.
		/// </summary>
		/// <param name="item">the item to be copied.</param>
		/// <param name="formResponse">The response of the operation.</param>
		/// <param name="Creator">The creator.</param>
		/// <param name="incrementTitle">if set to <c>true</c> [increment title].</param>
		/// <returns></returns>
		public override Portlet Copy(Portlet item, out FormResponse formResponse, string Creator = null, bool incrementTitle = true) {
			item.KeyPortlet = null;
			BaseBusinessEntity entity = null;
			if (!string.IsNullOrEmpty(item.KeyEntity)) {
				//we copy the contained entity
				FormResponse response;
				IBusinessEntityBroker broker = GetBusinessEntityBroker(item.TypeEntity);
				entity = broker.GetItem(item.KeyEntity, "*");
				entity = broker.Copy(entity, out response, Creator, incrementTitle);
				item.KeyEntity = entity.GetKey();
			}
			//we copy the portlet
			var newItem = base.Copy(item, out formResponse, Creator);
			newItem.Entity = entity;
			return newItem;
		}

		/// <summary>
		/// Resolves the references for export.
		/// </summary>
		/// <param name="entity">The entity.</param>
		protected override void ResolveReferencesForExport(Portlet entity) {
			ResolveParentReferenceForExport<PortalColumn>(e => e.KeyPortalColumn, entity);
			if (!string.IsNullOrWhiteSpace(entity.KeyEntity)) {
				switch (entity.TypeEntity) {
					case "Vizelia.FOL.BusinessEntities.AlarmTable":
						ResolveParentReferenceForExport<AlarmTable>(e => e.KeyEntity, entity);
						break;
					case "Vizelia.FOL.BusinessEntities.Chart":
						ResolveParentReferenceForExport<Chart>(e => e.KeyEntity, entity);
						break;
					case "Vizelia.FOL.BusinessEntities.DrawingCanvas":
						ResolveParentReferenceForExport<DrawingCanvas>(e => e.KeyEntity, entity);
						break;
					case "Vizelia.FOL.BusinessEntities.Map":
						ResolveParentReferenceForExport<Map>(e => e.KeyEntity, entity);
						break;
					case "Vizelia.FOL.BusinessEntities.WeatherLocation":
						ResolveParentReferenceForExport<WeatherLocation>(e => e.KeyEntity, entity);
						break;
					case "Vizelia.FOL.BusinessEntities.WebFrame":
						ResolveParentReferenceForExport<WebFrame>(e => e.KeyEntity, entity);
						break;
					case "Vizelia.FOL.BusinessEntities.FlipCard":
						ResolveParentReferenceForExport<FlipCard>(e => e.KeyEntity, entity);
						break;
					default:
						break;
				}
			}
		}

		/// <summary>
		/// Resolves the references during mapping.
		/// </summary>
		/// <param name="mappingRecord">The mapping record.</param>
		/// <param name="persistedEntity">The persisted entity.</param>
		protected override void ResolveReferencesForImport(MappingRecord<Portlet> mappingRecord, Portlet persistedEntity) {
			ResolveParentReferenceForImport<PortalColumn>(e => e.KeyPortalColumn, mappingRecord.Entity, persistedEntity);
			if (!string.IsNullOrWhiteSpace(mappingRecord.Entity.KeyEntity)) {
				switch (mappingRecord.Entity.TypeEntity) {
					case "Vizelia.FOL.BusinessEntities.AlarmTable":
						ResolveParentReferenceForImport<AlarmTable>(e => e.KeyEntity, mappingRecord.Entity, persistedEntity);
						break;
					case "Vizelia.FOL.BusinessEntities.Chart":
						ResolveParentReferenceForImport<Chart>(e => e.KeyEntity, mappingRecord.Entity, persistedEntity);
						break;
					case "Vizelia.FOL.BusinessEntities.DrawingCanvas":
						ResolveParentReferenceForImport<DrawingCanvas>(e => e.KeyEntity, mappingRecord.Entity, persistedEntity);
						break;
					case "Vizelia.FOL.BusinessEntities.Map":
						ResolveParentReferenceForImport<Map>(e => e.KeyEntity, mappingRecord.Entity, persistedEntity);
						break;
					case "Vizelia.FOL.BusinessEntities.WeatherLocation":
						ResolveParentReferenceForImport<WeatherLocation>(e => e.KeyEntity, mappingRecord.Entity, persistedEntity);
						break;
					case "Vizelia.FOL.BusinessEntities.WebFrame":
						ResolveParentReferenceForImport<WebFrame>(e => e.KeyEntity, mappingRecord.Entity, persistedEntity);
						break;
					case "Vizelia.FOL.BusinessEntities.FlipCard":
						ResolveParentReferenceForImport<FlipCard>(e => e.KeyEntity, mappingRecord.Entity, persistedEntity);
						break;
					default:
						break;
				}
			}
		}
	}
}
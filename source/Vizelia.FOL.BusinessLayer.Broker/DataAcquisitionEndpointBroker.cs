using System.Collections.Generic;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.DataLayer.BrokerDB;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// A DataAcquisitionEndpoint base broker.
	/// </summary>
	/// <typeparam name="TEndpoint">The type of the endpoint.</typeparam>
	/// <typeparam name="TEndpointBrokerDB">The type of the endpoint broker DB.</typeparam>
	public abstract class DataAcquisitionEndpointBroker<TEndpoint, TEndpointBrokerDB> : MappingEnabledBroker<TEndpoint, TEndpointBrokerDB>, IDataAcquisitionEndpointBroker<TEndpoint>
		where TEndpointBrokerDB : DataAcquisitionEndpointBrokerDB<TEndpoint>, new()
		where TEndpoint : DataAcquisitionEndpoint, new() {

		/// <summary>
		/// Get the list of saved TEndpoint from a container
		/// </summary>
		/// <param name="KeyDataAcquisitionContainer">The key data acquisition container.</param>
		/// <returns></returns>
		public List<TEndpoint> GetListByKeyPlaylist(string KeyDataAcquisitionContainer) {
			return ((TEndpointBrokerDB)m_brokerDB).GetListByKeyDataAcquisitionContainer(KeyDataAcquisitionContainer);
		}

		/// <summary>
		/// Gets a store of TEndpoint for a specific container.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyDataAcquisitionContainer">The key data acquisition container.</param>
		/// <returns>
		/// A JsonStore.
		/// </returns>
		public JsonStore<TEndpoint> GetStoreByKeyDataAcquisitionContainer(PagingParameter paging, string KeyDataAcquisitionContainer) {
			int total;
			return ((TEndpointBrokerDB)m_brokerDB).GetAllPagingByKeyDataAcquisitionContainer(paging, KeyDataAcquisitionContainer, out total).ToJsonStore(total, paging);
		}
	}
}
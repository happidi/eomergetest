﻿using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using Vizelia.FOL.DataLayer.BrokerDB;

namespace Vizelia.FOL.BusinessLayer.Broker {
	/// <summary>
	/// The broker for business entity MeterValidationRule.
	/// </summary>
	public class MeterValidationRuleBroker : MappingEnabledBroker<MeterValidationRule, MeterValidationRuleBrokerDB> {
		/// <summary>
		/// Public ctor.
		/// </summary>
		public MeterValidationRuleBroker()
			: base() {
		}


		/// <summary>
		/// Populate the chidren items specified as fields. "*" indicates that all fields should be populated.
		/// </summary>
		/// <param name="item">The item</param>
		/// <param name="loadMode">The load mode.</param>
		/// <param name="fields">The list of fields. "*" indicates all fields.</param>
		public override void PopulateList(MeterValidationRule item, EntityLoadMode loadMode, params string[] fields) {
			foreach (string field in fields) {
				if (field.ContainsAny("*", "Meters")) {
					var brokerMeter = Helper.CreateInstance<MeterBroker>();
					var meters = brokerMeter.GetCompleteListByKeyMeterValidationRule(item.KeyMeterValidationRule);
					if (meters != null && meters.Count > 0) {
						brokerMeter.PopulateList(meters, loadMode, "*");
						brokerMeter.ResolveForExport(meters, loadMode);
						item.Meters = meters;
					}
					break;
				}

			}
		}

		/// <summary>
		/// Resolves the references for import.
		/// </summary>
		/// <param name="mappingRecord">The mapping record.</param>
		/// <param name="persistedEntity">The persisted entity.</param>
		protected override void ResolveReferencesForImport(MappingRecord<MeterValidationRule> mappingRecord, MeterValidationRule persistedEntity) {
			// Do nothing.
		}

		/// <summary>
		/// Resolves the references for export.
		/// </summary>
		/// <param name="entity">The entity.</param>
		protected override void ResolveReferencesForExport(MeterValidationRule entity) {
			// Do nothing.
		}
	}
}
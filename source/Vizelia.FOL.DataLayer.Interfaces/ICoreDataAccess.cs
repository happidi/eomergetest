﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.DataLayer.Interfaces {
	/// <summary>
	/// Data layer interface for Core.
	/// </summary>
	public interface ICoreDataAccess {

		/// <summary>
		/// Gets the TypeName of a specific record.
		/// </summary>
		/// <param name="Key">The Key of the record.</param>
		/// <returns>The TypeName of the record.</returns>
		string GetTypeNameFromRecord(string Key);


		/// <summary>
		/// Gets all the created entities for a specific User.
		/// </summary>
		/// <param name="KeyUser">The key of th User.</param>
		/// <param name="type">The type of entity.</param>
		/// <returns></returns>
		List<string> GetCreatedEntity(string KeyUser, Type type);


		/// <summary>
		/// Gets the calendar data as a string from a location.
		/// </summary>
		/// <param name="KeyObject">The key of the object related to the calendar.</param>
		/// <returns></returns>
		string Calendar_GetFromObject(string KeyObject);

		/// <summary>
		/// Gets the TimeZone Id as a string from a location.
		/// </summary>
		/// <param name="KeyObject">The key of the object related to the calendar.</param>
		/// <returns></returns>
		string Calendar_GetTimeZoneId(string KeyObject);

		/// <summary>
		/// Gets a dictionnary of all the calendar data as string in a dictionnary group by key object.
		/// </summary>
		/// <returns></returns>
		Dictionary<string, Tuple<string, string>> Calendar_GetAll();

		/// <summary>
		/// Save a string representation of the calendar for a location.
		/// </summary>
		/// <param name="KeyObject">The key of the object related to the calendar.</param>
		/// <param name="iCalData">The iCal data persisted as a string.</param>
		/// <param name="TimeZoneId">The time zone id.</param>
		void Calendar_SaveForObject(string KeyObject, string iCalData, string TimeZoneId);

		/// <summary>
		/// Deletes a calendar for an object (for example a location).
		/// </summary>
		/// <param name="KeyObject">The key of the object related to the calendar.</param>
		void Calendar_DeleteForObject(string KeyObject);

		/// <summary>
		/// Builds the spatial hierarchy from a list of Keys.
		/// The level excluded is IfcSpace.
		/// </summary>
		/// <param name="keys">The Keys.</param>
		/// <returns>The spatial hierarchy from top level to down level.</returns>
		List<Filter> GetSpatialHierarchy(string[] keys);

		/// <summary>
		/// Builds the spatial hierarchy from a list of Keys.
		/// </summary>
		/// <param name="keys">The Keys.</param>
		/// <param name="levelExcluded">The ifc level to exclude. For example IfcSpace.</param>
		/// <returns>The spatial hierarchy from top level to down level.</returns>
		List<Filter> GetSpatialHierarchy(string[] keys, string levelExcluded);

		/// <summary>
		/// Builds the spatial hierarchy from a list of Keys.
		/// </summary>
		/// <param name="keys">The Keys</param>
		/// <param name="levelExcluded">The ifc level to exclude. For example IfcSpace.</param>
		/// <param name="direction">The direction. For example ALL, ASC, DESC.</param>
		/// <returns>The spatial hierarchy from top level to down level.</returns>
		List<Filter> GetSpatialHierarchy(string[] keys, string levelExcluded, HierarchyDirection direction);

		/// <summary>
		/// Builds the classification item hierarchy from a list of Keys.
		/// </summary>
		/// <param name="keys">The Keys</param>
		/// <returns>The classification item hierarchy from top level to down level.</returns>
		List<Filter> GetClassificationItemHierarchy(string[] keys);

		/// <summary>
		/// Builds the classification item hierarchy from a list of Keys.
		/// </summary>
		/// <param name="keys">The Keys</param>
		/// <param name="direction">The direction. For example ALL, ASC, DESC.</param>
		/// <returns>The classification item hierarchy from top level to down level.</returns>
		List<Filter> GetClassificationItemHierarchy(string[] keys, HierarchyDirection direction);

		/// <summary>
		/// Builds the organization hierarchy from a list of Keys.
		/// </summary>
		/// <param name="keys">The Keys</param>
		/// <returns>The organization hierarchy from top level to down level.</returns>
		List<Filter> GetOrganizationHierarchy(string[] keys);

		/// <summary>
		/// Builds the organization hierarchy from a list of Keys.
		/// </summary>
		/// <param name="keys">The Keys</param>
		/// <param name="direction">The direction. For example ALL, ASC, DESC.</param>
		/// <returns>The organization hierarchy from top level to down level.</returns>
		List<Filter> GetOrganizationHierarchy(string[] keys, HierarchyDirection direction);

		/// <summary>
		/// Sends a message to the service broker in order to refresh all the hierarchies.
		/// </summary>
		void RefreshHierarchies();


		/// <summary>
		/// Returns all the cache keys that are LIKE the given key
		/// </summary>
		/// <param name="key">The key.</param>
		/// <returns></returns>
		List<string> DBCacheItems_GetAllLikeKey(string key);


		/// <summary>
		/// Gets the count of all entities.
		/// </summary>
		/// <returns></returns>
		List<ListElementGeneric<long>> EntityCount_GetAll();
	}

}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;


namespace Vizelia.FOL.DataLayer.Interfaces {
	/// <summary>
	/// Data layer interface for ServiceDesk.
	/// </summary>
	public interface IServiceDeskDataAccess {

		/// <summary>
		/// Gets the Action requestor for a specific Requestor.
		/// </summary>
		/// <param name="KeyOccupant">The KeyOccupant of the requestor.</param>
		/// <returns>A DataSet of the ActionRequests.</returns>
		DataSet GetRequestorActionRequest(string KeyOccupant);

	

	}


}

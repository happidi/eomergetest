﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.DataLayer.Interfaces {

	/// <summary>
	/// Data layer interface for SpaceManagement.
	/// </summary>
	public interface ISpaceManagementDataAccess {


		/// <summary>
		/// Gets the zoning 
		/// </summary>
		/// <param name="KeyBuildingStorey"></param>
		/// <param name="KeyClassificationItem">The filter for Classifications (optional)</param>
		/// <returns></returns>
		JsonStore<Space> BuildingStorey_GetZoning(string KeyBuildingStorey, string KeyClassificationItem);
	}
}

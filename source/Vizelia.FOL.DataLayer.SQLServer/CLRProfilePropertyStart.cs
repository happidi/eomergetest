﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.SqlServer.Server;
using System.Text.RegularExpressions;

public partial class UserDefinedFunctions {

	/// <summary>
	/// Gets the start index of a propertyName when SqlProfileProvider is used to store profiles.
	/// </summary>
	/// <param name="propertyNames">The property names as stored in column [PropertyNames] of [aspnet_Profile table].</param>
	/// <param name="propertyName">The property name to extract.</param>
	/// <returns>The starting index of the property to extract.</returns>
	[Microsoft.SqlServer.Server.SqlFunction]
	public static SqlInt32 CLRProfilePropertyStart([SqlFacet(MaxSize = -1)]  string propertyNames, [SqlFacet(MaxSize = -1)]  string propertyName) {
		//string pattern = propertyName.Value.ToLower() + @"\:[A-Za-z]*\:\d{1,3}\:\d{1,3}\";
		//Regex regex = new Regex(pattern);
		//Match match = regex.Match(propertyNames.Value.ToLower());
		//if (match.Success) {
		//    string prop = match.Groups[0].Value;
		//    pattern = @"\d{1,3}\:\d{1,3}";
		//    regex = new Regex(pattern);
		//    match = regex.Match(prop);
		//    Int32 index = Int32.Parse(match.Groups[0].Value.Split(':')[0]);
		//    return index;
		//}
		//return -1;
		string propertyDefinition = GetPropertyDefinition(propertyNames, propertyName);
		if (!String.IsNullOrEmpty(propertyDefinition))
			return Int32.Parse(propertyDefinition.Split(':')[0]);
		else
			return 0;
	}

	/// <summary>
	/// Gets the definition of a propertyName when SqlProfileProvider is used to store profiles.
	/// </summary>
	/// <param name="propertyNames">The property names as stored in column [PropertyNames] of [aspnet_Profile table].</param>
	/// <param name="propertyName">The property name to extract.</param>
	/// <returns>The string with xx:yy where xx is the strart index and yy the end index.</returns>
	private static string GetPropertyDefinition(string propertyNames, string propertyName) {
		if (String.IsNullOrEmpty(propertyNames) || String.IsNullOrEmpty(propertyName))
			return null;
		string pattern = propertyName.ToLower() + "\\:[A-Za-z]*\\:\\d{1,3}\\:\\d{1,3}";
		Regex regex = new Regex(pattern);
		Match match = regex.Match(propertyNames.ToLower());
		if (match.Success) {
			string prop = match.Groups[0].Value;
			pattern = "\\d{1,3}\\:\\d{1,3}";
			regex = new Regex(pattern);
			match = regex.Match(prop);
			return match.Groups[0].Value;
		}
		return null;
	}

	[Microsoft.SqlServer.Server.SqlFunction]
	public static SqlInt32 CLRProfileProperty([SqlFacet(MaxSize = -1)]  string propertyNames, [SqlFacet(MaxSize = -1)]  string propertyName) {
			string propertyDefinition = GetPropertyDefinition(propertyNames, propertyName);
		if (!String.IsNullOrEmpty(propertyDefinition))
			return Int32.Parse(propertyDefinition.Split(':')[0]);
		else
			return 0;
	}
};


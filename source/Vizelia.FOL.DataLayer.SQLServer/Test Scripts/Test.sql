-- Exemples de requ�tes qui utilisent diff�rents objets SQL impl�ment�s par cet assembly

-----------------------------------------------------------------------------------------
-- Proc�dure stock�e
-----------------------------------------------------------------------------------------
-- exec StoredProcedureName


-----------------------------------------------------------------------------------------
-- Fonctions d�finies par l'utilisateur
-----------------------------------------------------------------------------------------
-- select dbo.FunctionName()


-----------------------------------------------------------------------------------------
-- Type d�fini par l'utilisateur.
-----------------------------------------------------------------------------------------
-- CREATE TABLE test_table (col1 UserType)
-- go
--
-- INSERT INTO test_table VALUES (convert(uri, 'Instantiation String 1'))
-- INSERT INTO test_table VALUES (convert(uri, 'Instantiation String 2'))
-- INSERT INTO test_table VALUES (convert(uri, 'Instantiation String 3'))
--
-- select col1::method1() from test_table



-----------------------------------------------------------------------------------------
-- Type d�fini par l'utilisateur.
-----------------------------------------------------------------------------------------
-- select dbo.AggregateName(Column1) from Table1
--select dbo.[CLRConvertNotationValue]('0001-0001')

--select dbo.CLRCreateGuid()

--select dbo.CLRProfilePropertyStart('FirstName:S:0:7:LastName:S:7:6:' , 'LastName')

/*select vizelia_data.dbo.CLRProfilePropertyStart(PropertyNames , 'IP')
FROM
vizelia_membership.dbo.aspnet_Users
INNER JOIN
vizelia_membership.dbo.aspnet_Applications
ON
vizelia_membership.dbo.aspnet_Applications.ApplicationId = vizelia_membership.dbo.aspnet_Users.ApplicationId
LEFT JOIN
vizelia_membership.dbo.aspnet_Profile
ON
vizelia_membership.dbo.aspnet_Users.UserId = vizelia_membership.dbo.aspnet_Profile.UserId
*/

--select 'Pour ex�cuter votre projet, modifiez le fichier Test.sql dans votre projet. Ce fichier se trouve dans le dossier Test Scripts dans l�Explorateur de solutions.'



DECLARE @iCalData nvarchar(max)
SELECT TOP 1 @iCalData = iCalData FROM vizelia_code.dbo.Calendar
SELECT @iCalData

SELECT * FROM
vizelia_data.dbo.CLRGenerateTableFromiCal(@iCalData)
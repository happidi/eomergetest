﻿In order to add the reference to iCalData dll,
Do the following:

1. Make sure you connect as "sa" user to the database, both in SQL Server Management Studio and in Visual Studio.

2. Make sure you are the dbowner of the vizelia_data database. You can run this command to change:
	use [vizelia_data]
	EXEC sp_changedbowner 'sa'

3. Run this script:
	ALTER DATABASE [vizelia_data] SET Trustworthy ON

	use [vizelia_data]

	CREATE ASSEMBLY [System.Runtime.Serialization]
	FROM 'C:\Windows\Microsoft.NET\Framework\v3.0\Windows Communication Foundation\System.Runtime.Serialization.dll'
	WITH PERMISSION_SET = UNSAFE 

	CREATE ASSEMBLY [DDay.iCal]
	FROM 'D:\TFS\vizelia\dev\BIN\DDay.iCal.dll'
	WITH PERMISSION_SET = UNSAFE

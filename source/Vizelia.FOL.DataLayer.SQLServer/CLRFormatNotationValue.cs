﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.SqlServer.Server;

public partial class UserDefinedFunctions {
	[Microsoft.SqlServer.Server.SqlFunction]
	public static SqlString CLRFormatNotationValue(string notationValue) {
		return new SqlString (notationValue.PadLeft(6, '0'));
		/*
		if (notationValue.Length < 4) {
			notationValue = "0000" + notationValue;
			return new SqlString(notationValue.Substring(notationValue.Length - 4));
		}
		else
			return new SqlString(notationValue);
		 */
	}
};


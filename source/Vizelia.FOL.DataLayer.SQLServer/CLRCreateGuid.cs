﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.SqlServer.Server;
using System.Runtime.InteropServices;
using Vizelia.FOL.DataLayer.IFCSQL;

public partial class UserDefinedFunctions {
	//[STAThread]
	//[DllImport("CreateGuid64.dll")]
	//public extern static string CreateCompressedGuidString(string BufString, int N);

	[Microsoft.SqlServer.Server.SqlFunction]
	public static SqlString CLRCreateGuid() {
		//string BufString, GlobalId;
		//BufString = "                       ";
		//GlobalId = CreateCompressedGuidString(BufString, 23);
		//GlobalId = GlobalId.Substring(0, 22);
		//return GlobalId;
		ShortGuid guid = ShortGuid.NewGuid();
		return guid.ToString();
	}

};
﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.SqlServer.Server;

public partial class UserDefinedFunctions {
	[Microsoft.SqlServer.Server.SqlFunction]
	public static SqlInt32 CLRConvertNotationValue(string notationValue) {
		int index;
		index = notationValue.LastIndexOf("-");
		return new SqlInt32(Convert.ToInt32(notationValue.Substring(index + 1)));
	}
};


﻿using System;
using System.Activities;
using System.Activities.Presentation.Model;
using System.Activities.Presentation.Services;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Activities.Core.Presentation;
using System.Activities.Presentation;
using System.Activities.Presentation.View;
using System.Reflection;
using System.Windows.Threading;
using Vizelia.FOL.BusinessEntities;

namespace DesignerHosting {
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window {
		private WorkflowDesigner _workflowDesigner;
		private WorkflowViewerArgument _workflowViewerArgument;

		private delegate void TimerDelegate();
		private System.Timers.Timer _timer;


		/// <summary>
		/// Initializes a new instance of the <see cref="MainWindow"/> class.
		/// </summary>
		public MainWindow() {
			InitializeComponent();
			_workflowViewerArgument = DesignerHosting.App.Arguments;
		}

		/// <summary>
		/// Handles the Loaded event of the StackPanel control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
		private void StackPanel_Loaded(object sender, RoutedEventArgs e) {
			new DesignerMetadata().Register();
			_workflowDesigner = new WorkflowDesigner();
			LoadAssemblies();

			if (!String.IsNullOrEmpty(_workflowViewerArgument.XamlDesign)) {
				_workflowDesigner.Text = _workflowViewerArgument.XamlDesign;
				_workflowDesigner.Load();
			}
			else {
				if(String.IsNullOrEmpty(_workflowViewerArgument.XamlDesign))
					_workflowViewerArgument.XamlDesign = "<Activity x:Class=\"ActivityLibrary1.Activity1\" xmlns=\"http://schemas.microsoft.com/netfx/2009/xaml/activities\" mva:VisualBasic.Settings=\"Assembly references and imported namespaces for internal implementation\" xmlns:mv=\"clr-namespace:Microsoft.VisualBasic;assembly=System\" xmlns:mva=\"clr-namespace:Microsoft.VisualBasic.Activities;assembly=System.Activities\" xmlns:s=\"clr-namespace:System;assembly=mscorlib\" xmlns:s1=\"clr-namespace:System;assembly=System\" xmlns:s2=\"clr-namespace:System;assembly=System.Xml\" xmlns:s3=\"clr-namespace:System;assembly=System.Core\" xmlns:scg=\"clr-namespace:System.Collections.Generic;assembly=System\" xmlns:scg1=\"clr-namespace:System.Collections.Generic;assembly=System.ServiceModel\" xmlns:scg2=\"clr-namespace:System.Collections.Generic;assembly=System.Core\" xmlns:scg3=\"clr-namespace:System.Collections.Generic;assembly=mscorlib\" xmlns:sd=\"clr-namespace:System.Data;assembly=System.Data\" xmlns:sl=\"clr-namespace:System.Linq;assembly=System.Core\" xmlns:st=\"clr-namespace:System.Text;assembly=mscorlib\" xmlns:x=\"http://schemas.microsoft.com/winfx/2006/xaml\"></Activity>";
				_workflowDesigner.Load(_workflowViewerArgument.XamlFile);
			}


			Window win = new Window {
				WindowStyle = WindowStyle.None,
				Width = _workflowViewerArgument.Width,
				Height = _workflowViewerArgument.Height
			};

			win.Show();
			win.Content = _workflowDesigner.View;

			WaitForWorkflowToLoad();
		}

		/// <summary>
		/// Waits for workflow to load.
		/// </summary>
		private void WaitForWorkflowToLoad() {
			_timer = new System.Timers.Timer(100) { AutoReset = false };
			_timer.Elapsed += new ElapsedEventHandler(CaptureTimer_Elapsed);
			_timer.Start();
			_workflowDesigner.View.LayoutUpdated += new EventHandler(View_LayoutUpdated);
		}

		/// <summary>
		/// Handles the LayoutUpdated event of the View control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		private void View_LayoutUpdated(object sender, EventArgs e) {
			if (_timer != null) {
				_timer.Stop();
				_timer.Start();
			}
		}

		/// <summary>
		/// Handles the Elapsed event of the CaptureTimer control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Timers.ElapsedEventArgs"/> instance containing the event data.</param>
		private void CaptureTimer_Elapsed(object sender, ElapsedEventArgs e) {
			_timer.Stop();
			_timer = null;
			_workflowDesigner.View.Dispatcher.BeginInvoke(
				DispatcherPriority.ApplicationIdle,
				(TimerDelegate)delegate {
				SelectState(_workflowViewerArgument.CurrentState);
				((RoutedCommand)DesignerView.FitToScreenCommand).Execute(null, _workflowDesigner.Context.Services.GetService<DesignerView>());
				_workflowDesigner.View.UpdateLayout();
				CreateWorkflowDefinitionImage();
				Application.Current.Shutdown();
			});

		}

		void CreateWorkflowDefinitionImage() {
			/*
			ModelService modelService = _workflowDesigner.Context.Services.GetService<ModelService>();
			IEnumerable<ModelItem> activityCollection = modelService.Find(modelService.Root, typeof(object));
			Selection.Select(_workflowDesigner.Context, activityCollection.ElementAt(9));
			ModelItemExtensions.Focus(activityCollection.ElementAt(9));

			activityCollection.ElementAt(9).Focus();
			*/

			const double dpi = 96.0;
			var el = ((System.Windows.Controls.Grid)(_workflowDesigner.View));

			Visual areaToSave = el;
			Rect size = VisualTreeHelper.GetDescendantBounds(areaToSave);

			RenderTargetBitmap bitmap = new RenderTargetBitmap((int)size.Width, (int)size.Height, dpi, dpi, PixelFormats.Pbgra32);
			DrawingVisual dv = new DrawingVisual();
			using (DrawingContext ctx = dv.RenderOpen()) {
				VisualBrush vb = new VisualBrush(areaToSave);
				// we crop the image to exclude upper and lower border
				ctx.DrawRectangle(vb, null, new Rect(new Point(0, -30), new Size(size.Width, size.Height + 60)));
			}
			bitmap.Render(dv);
			SaveImageToFile(_workflowViewerArgument.ResultFile, BitmapFrame.Create(bitmap));
		}

		/// <summary>
		/// Selects the state.
		/// </summary>
		/// <param name="stateName">Name of the state.</param>
		private void SelectState(string stateName) {
			var borders = GetChildrenOfType<Border>(_workflowDesigner.View).Where(b => b.Name == "ShapeBorder").ToList();
			foreach (var b in borders) {
				var isRoot = GetChildrenOfType<TextBlock>(b).Where(t => t.Text == "StateMachine").Any();
				var containsTargetState = GetChildrenOfType<TextBlock>(b).Where(t => t.Text == stateName).Any();
				var htmlColorCurrentState = _workflowViewerArgument.HtmlColorCurrentState;
				var colorCurrentState = System.Drawing.ColorTranslator.FromHtml(htmlColorCurrentState);
				if (!isRoot && containsTargetState) {
					b.BorderBrush = new SolidColorBrush(Color.FromArgb(colorCurrentState.A, colorCurrentState.R, colorCurrentState.G, colorCurrentState.B));
					b.BorderThickness = new Thickness(4);
				}
				else {
					b.BorderBrush = new SolidColorBrush(Colors.White);
					b.BorderThickness = new Thickness(0);
				}

			}
		}

		/// <summary>
		/// Saves the image to file.
		/// </summary>
		/// <param name="fileName">Name of the file.</param>
		/// <param name="image">The image.</param>
		void SaveImageToFile(string fileName, BitmapFrame image) {
			using (FileStream fs = new FileStream(fileName, FileMode.Create)) {
				//BitmapEncoder encoder = new JpegBitmapEncoder();
				BitmapEncoder encoder = new PngBitmapEncoder();
				encoder.Frames.Add(BitmapFrame.Create(image));
				encoder.Save(fs);
				fs.Close();
			}
		}

		/// <summary>
		/// Loads the assemblies.
		/// </summary>
		private void LoadAssemblies() {
			var assemblies = _workflowViewerArgument.AssembliesToLoad;
			foreach (var assembly in assemblies) {
				Assembly.Load(assembly);
			}
		}

		/// <summary>
		/// Gets the type of the children of.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="parent">The parent.</param>
		/// <returns></returns>
		public List<T> GetChildrenOfType<T>(DependencyObject parent) where T : DependencyObject {
			List<T> list = new List<T>();
			int childCount = VisualTreeHelper.GetChildrenCount(parent);
			for (int i = 0; i < childCount; i++) {
				DependencyObject child = VisualTreeHelper.GetChild(parent, i);
				//get the current child                 
				if (child is T) list.Add((T)child);
				//if it is of type that you are looking for, then add it to the list        
				list.AddRange(GetChildrenOfType<T>(child)); // on that get a list of children that it has.            
			}
			return list;
		}


	}
}
﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;


namespace DesignerHosting {
	/// <summary>
	/// Interaction logic for App.xaml
	/// </summary>
	public partial class App : Application {

		/// <summary>
		/// The passed arguments.
		/// </summary>
		public static WorkflowViewerArgument Arguments;
		
		/// <summary>
		/// Raises the <see cref="E:System.Windows.Application.Startup"/> event.
		/// </summary>
		/// <param name="e">A <see cref="T:System.Windows.StartupEventArgs"/> that contains the event data.</param>
		protected override void OnStartup(StartupEventArgs e) {
			//Debugger.Launch();
			Arguments = Helper.DeserializeXml<WorkflowViewerArgument>(e.Args[0]);
		}
	}
}

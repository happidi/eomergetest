﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Mail;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using Microsoft.Practices.EnterpriseLibrary.Logging.Formatters;
using Microsoft.Practices.EnterpriseLibrary.Logging.TraceListeners;
using Vizelia.FOL.Practices.Configuration;

namespace Vizelia.FOL.Practices {
	/// <summary>
	/// 
	/// </summary>
	[ConfigurationElementType(typeof(VizeliaEmailTraceListenerData))]
	public class VizeliaEmailTraceListener : CustomTraceListener {
		private string toAddress = String.Empty;
		private string fromAddress = String.Empty;
		private string subjectLine = String.Empty;

		/// <summary>
		/// Initializes a new instance of the <see cref="VizeliaEmailTraceListener"/> class.
		/// </summary>
		/// <param name="formatter">The formatter.</param>
		/// <param name="toAddress">To address.</param>
		/// <param name="fromAddress">From address.</param>
		/// <param name="subjectLine">The subject line.</param>
		public VizeliaEmailTraceListener(ILogFormatter formatter, string toAddress, string fromAddress, string subjectLine) {
			Formatter = formatter;
			this.toAddress = toAddress;
			this.fromAddress = fromAddress;
			this.subjectLine = subjectLine;
		}

		/// <summary>
		/// When overridden in a derived class, writes the specified message to the listener you create in the derived class.
		/// </summary>
		/// <param name="message">A message to write.</param>
		public override void Write(string message) {
			VizeliaEmailMessage mailMessage = new VizeliaEmailMessage(toAddress, fromAddress, subjectLine, message, this.Formatter);
			mailMessage.Send();
		}

		/// <summary>
		/// When overridden in a derived class, writes a message to the listener you create in the derived class, followed by a line terminator.
		/// </summary>
		/// <param name="message">A message to write. </param><filterpriority>2</filterpriority>
		public override void WriteLine(string message) {
			Write(message);
		}

		/// <summary>
		/// Delivers the trace data as an email message.
		/// </summary>
		/// <param name="eventCache">The context information provided by <see cref="System.Diagnostics"/>.</param>
		/// <param name="source">The name of the trace source that delivered the trace data.</param>
		/// <param name="eventType">The type of event.</param>
		/// <param name="id">The id of the event.</param>
		/// <param name="data">The data to trace.</param>
		public override void TraceData(TraceEventCache eventCache, string source, TraceEventType eventType, int id, object data) {
			if ((Filter != null) && !Filter.ShouldTrace(eventCache, source, eventType, id, null, null,
				data, null))
				return;

			if (data is LogEntry) {
				LogEntry logEntry = (LogEntry)data;
				string address = toAddress;
				if (logEntry.ExtendedProperties.ContainsKey("toAddress"))
					address += ";" + (string)logEntry.ExtendedProperties["toAddress"];
				VizeliaEmailMessage message = new VizeliaEmailMessage(address, fromAddress, subjectLine, data as LogEntry, this.Formatter);
				if (logEntry.ExtendedProperties.ContainsKey("attachments"))
					message.Attachments = (List<Attachment>)logEntry.ExtendedProperties["attachments"];
				message.Send();
			}
			else {
				base.TraceData(eventCache, source, eventType, id, data);
			}


		}
	}
}
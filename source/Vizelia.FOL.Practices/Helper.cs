﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Vizelia.FOL.Practices {
	/// <summary>
	/// Internal helper class.
	/// This class makes the reference of Vizelia.FOL.Common unnecessary.
	/// </summary>
	internal static class Helper {
		
		/// <summary>
		/// Convert a string to an enum.
		/// </summary>
		/// <typeparam name="T">The enum type.</typeparam>
		/// <param name="value">The value of the enum as a string.</param>
		/// <returns>The value is returned as an enum.</returns>
		public static T ParseAsEnum<T>(string value) {
			if (!typeof(T).IsEnum) {
				throw new NotSupportedException("T must be an Enum");
			}
			if (Enum.IsDefined(typeof(T), value))
				return (T)Enum.Parse(typeof(T), value);
			else
				throw new ArgumentException(String.Format("The value {0} could not be found in Enum {1}", value, typeof(T).FullName));

			
		}
	}
}

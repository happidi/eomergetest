﻿using System.Threading;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using Microsoft.Practices.EnterpriseLibrary.Logging.PolicyInjection;
using Microsoft.Practices.Unity.InterceptionExtension;
using Vizelia.FOL.Practices.Configuration;
using Vizelia.FOL.Common.Configuration;

namespace Vizelia.FOL.Practices {

	/// <summary>
	/// The vizelia logging class, adds user id
	/// </summary>
	[ConfigurationElementType(typeof(VizeliaLogCallHandlerData))]
	public class VizeliaLogCallHandler : OriginalLogCallHandler {
		/// <summary>
		/// Initializes a new instance of the <see cref="VizeliaLogCallHandler"/> class.
		/// </summary>
		public VizeliaLogCallHandler() {
		}


		/// <summary>
		/// Initializes a new instance of the <see cref="VizeliaLogCallHandler"/> class.
		/// </summary>
		/// <param name="logWriter">The log writer.</param>
		public VizeliaLogCallHandler(LogWriter logWriter)
			: base(logWriter) {
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="VizeliaLogCallHandler"/> class.
		/// </summary>
		/// <param name="logWriter">The log writer.</param>
		/// <param name="eventId">The event id.</param>
		/// <param name="logBeforeCall">if set to <c>true</c> [log before call].</param>
		/// <param name="logAfterCall">if set to <c>true</c> [log after call].</param>
		/// <param name="beforeMessage">The before message.</param>
		/// <param name="afterMessage">The after message.</param>
		/// <param name="includeParameters">if set to <c>true</c> [include parameters].</param>
		/// <param name="includeCallStack">if set to <c>true</c> [include call stack].</param>
		/// <param name="includeCallTime">if set to <c>true</c> [include call time].</param>
		/// <param name="priority">The priority.</param>
		public VizeliaLogCallHandler(LogWriter logWriter, int eventId, bool logBeforeCall, bool logAfterCall, string beforeMessage, string afterMessage, bool includeParameters, bool includeCallStack, bool includeCallTime, int priority)
			: base(logWriter, eventId, logBeforeCall, logAfterCall, beforeMessage, afterMessage, includeParameters, includeCallStack, includeCallTime, priority) {
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="VizeliaLogCallHandler"/> class.
		/// </summary>
		/// <param name="logWriter">The log writer.</param>
		/// <param name="eventId">The event id.</param>
		/// <param name="logBeforeCall">if set to <c>true</c> [log before call].</param>
		/// <param name="logAfterCall">if set to <c>true</c> [log after call].</param>
		/// <param name="beforeMessage">The before message.</param>
		/// <param name="afterMessage">The after message.</param>
		/// <param name="includeParameters">if set to <c>true</c> [include parameters].</param>
		/// <param name="includeCallStack">if set to <c>true</c> [include call stack].</param>
		/// <param name="includeCallTime">if set to <c>true</c> [include call time].</param>
		/// <param name="priority">The priority.</param>
		/// <param name="order">The order.</param>
		public VizeliaLogCallHandler(LogWriter logWriter, int eventId, bool logBeforeCall, bool logAfterCall, string beforeMessage, string afterMessage, bool includeParameters, bool includeCallStack, bool includeCallTime, int priority, int order)
			: base(logWriter, eventId, logBeforeCall, logAfterCall, beforeMessage, afterMessage, includeParameters, includeCallStack, includeCallTime, priority, order) {
		}

		

		

		///<summary>
		/// Creates a subclass of TraceLogEntry.
		/// </summary>
		/// <param name="input">The input method</param>
		/// <returns>A TraceLogEntry</returns>
		protected override TraceLogEntry GetLogEntry(IMethodInvocation input) {
			TraceLogEntry log = base.GetLogEntry(input);
			VizeliaTraceLogEntry logEntry = new VizeliaTraceLogEntry(log);
			return logEntry;
		}

	
	}
}



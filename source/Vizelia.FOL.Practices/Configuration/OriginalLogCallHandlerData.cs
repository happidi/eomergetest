﻿using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Logging.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Logging.PolicyInjection;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.InterceptionExtension;
using System.ComponentModel;

namespace Vizelia.FOL.Practices.Configuration {
	/// <summary>
	/// A configuration element for the data for the <see cref="VizeliaLogCallHandler"/> VizeliaLogCallHandler.
	/// </summary>
	public class OriginalLogCallHandlerData<TCallHandler> : CallHandlerData where TCallHandler : ICallHandler   {

		private const string LogBehaviorPropertyName = "logBehavior";
		private const string CategoriesPropertyName = "categories";
		private const string BeforeMessagePropertyName = "beforeMessage";
		private const string AfterMessagePropertyName = "afterMessage";
		private const string EventIdPropertyName = "eventId";
		private const string IncludeParameterValuesPropertyName = "includeParameterValues";
		private const string IncludeCallStackPropertyName = "includeCallStack";
		private const string IncludeCallTimePropertyName = "includeCallTime";
		private const string PriorityPropertyName = "priority";
		private const string SeverityPropertyName = "severity";
		
	

		/// <summary>
		/// Construct a new <see cref="LogCallHandlerData"/> instance.
		/// </summary>
		public OriginalLogCallHandlerData()
			: base("VizeliaLogCallHandler", typeof(VizeliaLogCallHandler)) {
		}


		/// <summary>
		/// Construct a new <see cref="VizeliaLogCallHandlerData"/> instance.
		/// </summary>
		/// <param name="handlerName">Handler name</param>
		public OriginalLogCallHandlerData(string handlerName)
			: base(handlerName, typeof(TCallHandler)) {
		}

		/// <summary>
		/// Construct a new <see cref="VizeliaLogCallHandlerData"/> instance.
		/// </summary>
		/// <param name="handlerName">Handler name</param>
		/// <param name="handlerOrder">Handler order</param>
		public OriginalLogCallHandlerData(string handlerName, int handlerOrder)
			: base(handlerName, typeof(TCallHandler)) {
			this.Order = handlerOrder;
		}

		


		/// <summary>
		/// Should the handler log before the call, after the call, or both?
		/// </summary>
		/// <value>The "logBehavior" config attribute.</value>
		[ConfigurationProperty(LogBehaviorPropertyName, DefaultValue = HandlerLogBehavior.BeforeAndAfter)]
		public HandlerLogBehavior LogBehavior {
			get {
				return (HandlerLogBehavior)base[LogBehaviorPropertyName];
			}
			set {
				base[LogBehaviorPropertyName] = value;
			}
		}

		/// <summary>
		/// Collection of log categories to use in the log message.
		/// </summary>
		/// <value>The "categories" nested element.</value>
		[ConfigurationProperty(CategoriesPropertyName)]
		[ConfigurationCollection(typeof(LogCallHandlerCategoryEntry))]
		[Editor("Microsoft.Practices.EnterpriseLibrary.Configuration.Design.ComponentModel.Editors.ElementCollectionEditor, Microsoft.Practices.EnterpriseLibrary.Configuration.DesignTime", "System.Windows.FrameworkElement, PresentationFramework, Version=3.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35")]
		public NamedElementCollection<LogCallHandlerCategoryEntry> Categories {
			get {
				return (NamedElementCollection<LogCallHandlerCategoryEntry>)base[CategoriesPropertyName];
			}
			set {
				base[CategoriesPropertyName] = value;
			}
		}

		/// <summary>
		/// Message for the log entry in before-call logging.
		/// </summary>
		/// <value>The "beforeMessage" config attribute.</value>
		[ConfigurationProperty(BeforeMessagePropertyName, DefaultValue = "")]
		public string BeforeMessage {
			get {
				return (string)base[BeforeMessagePropertyName];
			}
			set {
				base[BeforeMessagePropertyName] = value;
			}
		}

		/// <summary>
		/// Message for the log entry in after-call logging.
		/// </summary>
		/// <value>The "afterMessage" config attribute.</value>
		[ConfigurationProperty(AfterMessagePropertyName, DefaultValue = "")]
		public string AfterMessage {
			get {
				return (string)base[AfterMessagePropertyName];
			}
			set {
				base[AfterMessagePropertyName] = value;
			}
		}

		/// <summary>
		/// Event Id to put into log entries.
		/// </summary>
		/// <value>The "eventId" config attribute.</value>
		[ConfigurationProperty(EventIdPropertyName, DefaultValue = LogCallHandlerDefaults.EventId)]
		public int EventId {
			get {
				return (int)base[EventIdPropertyName];
			}
			set {
				base[EventIdPropertyName] = value;
			}
		}

		/// <summary>
		/// Include parameter values and return values in the log entry
		/// </summary>
		/// <value>The "includeParameterValues" config attribute.</value>
		[ConfigurationProperty(IncludeParameterValuesPropertyName, DefaultValue = LogCallHandlerDefaults.IncludeParameters)]
		public bool IncludeParameterValues {
			get {
				return (bool)base[IncludeParameterValuesPropertyName];
			}
			set {
				base[IncludeParameterValuesPropertyName] = value;
			}
		}

		/// <summary>
		/// Include the call stack in the log entries.
		/// </summary>
		/// <remarks>Setting this to true requires UnmanagedCode permissions.</remarks>
		/// <value>The "includeCallStack" config attribute.</value>
		[ConfigurationProperty(IncludeCallStackPropertyName, DefaultValue = LogCallHandlerDefaults.IncludeCallStack)]
		public bool IncludeCallStack {
			get {
				return (bool)base[IncludeCallStackPropertyName];
			}
			set {
				base[IncludeCallStackPropertyName] = value;
			}
		}

		/// <summary>
		/// Include the time to execute the call in the log entries.
		/// </summary>
		/// <value>The "includeCallTime" config attribute.</value>
		[ConfigurationProperty(IncludeCallTimePropertyName, DefaultValue = LogCallHandlerDefaults.IncludeCallTime)]
		public bool IncludeCallTime {
			get {
				return (bool)base[IncludeCallTimePropertyName];
			}
			set {
				base[IncludeCallTimePropertyName] = value;
			}
		}

		/// <summary>
		/// The priority of the log entries.
		/// </summary>
		/// <value>the "priority" config attribute.</value>
		[ConfigurationProperty(PriorityPropertyName, DefaultValue = LogCallHandlerDefaults.Priority)]
		public int Priority {
			get {
				return (int)base[PriorityPropertyName];
			}
			set {
				base[PriorityPropertyName] = value;
			}
		}

		/// <summary>
		/// The severity of the log entry.
		/// </summary>
		/// <value>the "severity" config attribute.</value>
		[ConfigurationProperty(SeverityPropertyName, DefaultValue = LogCallHandlerDefaults.Severity, IsRequired = false)]
		public TraceEventType Severity {
			get {
				return (TraceEventType)base[SeverityPropertyName];
			}
			set {
				base[SeverityPropertyName] = value;
			}
		}
	}
}

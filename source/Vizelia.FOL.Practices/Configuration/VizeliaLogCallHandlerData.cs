﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration.ContainerModel;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using Microsoft.Practices.EnterpriseLibrary.Logging.Configuration;
using Microsoft.Practices.Unity;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.Unity.InterceptionExtension;

namespace Vizelia.FOL.Practices.Configuration {

	/// <summary>
	/// A configuration element for the data for the <see cref="VizeliaLogCallHandler"/> VizeliaLogCallHandler.
	/// </summary>
	public class VizeliaLogCallHandlerData : OriginalLogCallHandlerData<VizeliaLogCallHandler> {
		private const string EnabledPropertyName = "enabled";
	
		/// <summary>
		/// Construct a new <see cref="VizeliaLogCallHandler"/> instance.
		/// </summary>
		public VizeliaLogCallHandlerData()
			: base() {
		}



		/// <summary>
		/// Get the set of <see cref="TypeRegistration"/> objects needed to
		/// register the call handler represented by this config element and its associated objects.
		/// </summary>
		/// <param name="nameSuffix">A suffix for the names in the generated type registration objects.</param>
		/// <returns>The set of <see cref="TypeRegistration"/> objects.</returns>
		public override IEnumerable<TypeRegistration> GetRegistrations(string nameSuffix) {

			var logBeforeCall = false;
			var logAfterCall = false;
			switch (this.LogBehavior) {
				case HandlerLogBehavior.Before:
					logBeforeCall = true;
					break;

				case HandlerLogBehavior.After:
					logAfterCall = true;
					break;

				case HandlerLogBehavior.BeforeAndAfter:
					logBeforeCall = true;
					logAfterCall = true;
					break;
			}
			var categories = new List<string>(this.Categories.Select(cat => cat.Name));

			yield return
				new TypeRegistration<ICallHandler>(() =>
												   new VizeliaLogCallHandler(Container.Resolved<LogWriter>()) {
													   Order = this.Order,
													   LogBeforeCall = logBeforeCall,
													   LogAfterCall = logAfterCall,
													   BeforeMessage = this.BeforeMessage,
													   AfterMessage = this.AfterMessage,
													   EventId = this.EventId,
													   IncludeCallStack = this.IncludeCallStack,
													   IncludeCallTime = this.IncludeCallTime,
													   IncludeParameters = this.IncludeParameterValues,
													   Priority = this.Priority,
													   Severity = this.Severity,
													   Categories = categories
													   
												   }) {
													   Name = this.Name + nameSuffix,
													   Lifetime = TypeRegistrationLifetime.Transient
												   };

		}

		/// <summary>
		/// Construct a new <see cref="VizeliaLogCallHandlerData"/> instance.
		/// </summary>
		/// <param name="handlerName">Handler name</param>
		public VizeliaLogCallHandlerData(string handlerName)
			: base(handlerName) {
		}

		/// <summary>
		/// Construct a new <see cref="VizeliaLogCallHandlerData"/> instance.
		/// </summary>
		/// <param name="handlerName">Handler name</param>
		/// <param name="handlerOrder">Handler order</param>
		public VizeliaLogCallHandlerData(string handlerName, int handlerOrder)
			: base(handlerName, handlerOrder) {
		}




		/// <summary>
		/// Is the handler enabled?
		/// </summary>
		/// <value>The "enabled" config attribute</value>
		[ConfigurationProperty(EnabledPropertyName, DefaultValue = true)]
		public bool Enabled {
			get {
				return (bool)base[EnabledPropertyName];
			}
			set {
				base[EnabledPropertyName] = value;
			}
		}

		

		/// <summary>
		/// Adds the call handler represented by this configuration object to <paramref name="policy"/>.
		/// </summary>
		/// <param name="policy">The policy to which the rule must be added.</param>
		/// <param name="configurationSource">The configuration source from which additional information
		/// can be retrieved, if necessary.</param>
		public void ConfigurePolicy(PolicyDefinition policy, IConfigurationSource configurationSource) {
			bool logBeforeCall = false;
			bool logAfterCall = false;
			switch (this.LogBehavior) {
				case HandlerLogBehavior.Before:
					logBeforeCall = true;
					break;

				case HandlerLogBehavior.After:
					logAfterCall = true;
					break;

				case HandlerLogBehavior.BeforeAndAfter:
					logBeforeCall = true;
					logAfterCall = true;
					break;
			}

			List<string> categories = new List<string>();
			this.Categories.ForEach(
				delegate(LogCallHandlerCategoryEntry entry) {
					categories.Add(entry.Name);
				}
			);

			policy.AddCallHandler<VizeliaLogCallHandler>(
				new ContainerControlledLifetimeManager(),
				new InjectionConstructor(new InjectionParameter<IConfigurationSource>(configurationSource)),
				new InjectionProperty("LogBeforeCall", logBeforeCall),
				new InjectionProperty("LogAfterCall", logAfterCall),
				new InjectionProperty("Order", this.Order),
				new InjectionProperty("BeforeMessage", new InjectionParameter<string>(this.BeforeMessage)),
				new InjectionProperty("AfterMessage", new InjectionParameter<string>(this.AfterMessage)),
				new InjectionProperty("EventId", this.EventId),
				new InjectionProperty("IncludeCallStack", this.IncludeCallStack),
				new InjectionProperty("IncludeCallTime", this.IncludeCallTime),
				new InjectionProperty("IncludeParameters", this.IncludeParameterValues),
				new InjectionProperty("Priority", this.Priority),
				new InjectionProperty("Severity", this.Severity),
				new InjectionProperty("Categories", categories),
				new InjectionProperty("Enabled", this.Enabled));
		}
	}
}

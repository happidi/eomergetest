﻿//===============================================================================
// Microsoft patterns & practices Enterprise Library
// Logging Application Block
//===============================================================================
// Copyright © Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//===============================================================================

using System;
using System.Net.Mail;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Logging.Formatters;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using System.Linq;
using System.Collections.Generic;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.Practices.Configuration {
	/// <summary>
	/// Represents a <see cref="VizeliaEmailMessage"/>.
	/// Encapsulates a System.Net.MailMessage with functions to accept a LogEntry, Formatting, and sending of emails
	/// </summary>

	public class VizeliaEmailMessage {
		private VizeliaEmailTraceListenerData configurationData;
		private ILogFormatter formatter;
		private LogEntry logEntry;


		/// <summary>
		/// Gets or sets the attachments.
		/// </summary>
		/// <value>The attachments.</value>
		public List<Attachment> Attachments { get; set; }


		/// <summary>
		/// Initializes a <see cref="VizeliaEmailMessage"/> with email configuration data, logentry, and formatter 
		/// </summary>
		/// <param name="configurationData">The configuration data <see cref="VizeliaEmailTraceListenerData"/> 
		/// that represents how to create the email message</param>
		/// <param name="logEntry">The LogEntry <see cref="LogEntry"/> to send via email.</param>
		/// <param name="formatter">The Formatter <see cref="ILogFormatter"/> which determines how the 
		/// email message should be formatted</param>
		public VizeliaEmailMessage(VizeliaEmailTraceListenerData configurationData, LogEntry logEntry, ILogFormatter formatter) {
			this.configurationData = configurationData;
			this.logEntry = logEntry;
			this.formatter = formatter;
		}

		/// <summary>
		/// Initializes a <see cref="VizeliaEmailMessage"/> with the raw data to create and email, the logentry, and the formatter
		/// </summary>
		/// <param name="toAddress">A semicolon delimited string the represents to whom the email should be sent.</param>
		/// <param name="fromAddress">Represents from whom the email is sent.</param>
		/// <param name="subjectLine">The subject line.</param>
		/// <param name="logEntry">The LogEntry <see cref="LogEntry"/> to send via email.</param>
		/// <param name="formatter">The Formatter <see cref="ILogFormatter"/> which determines how the
		/// email message should be formatted</param>
		public VizeliaEmailMessage(string toAddress, string fromAddress, string subjectLine, LogEntry logEntry, ILogFormatter formatter) {
			this.configurationData = new VizeliaEmailTraceListenerData(toAddress, fromAddress, subjectLine, string.Empty);
			this.logEntry = logEntry;
			this.formatter = formatter;
		}

		/// <summary>
		/// Initializes a <see cref="VizeliaEmailMessage"/> with the raw data to create and email, a message, and the formatter
		/// </summary>
		/// <param name="toAddress">A semicolon delimited string the represents to whom the email should be sent.</param>
		/// <param name="fromAddress">Represents from whom the email is sent.</param>
		/// <param name="subjectLine">The subject line.</param>
		/// <param name="message">Represents the message to send via email.</param>
		/// <param name="formatter">The Formatter <see cref="ILogFormatter"/> which determines how the
		/// email message should be formatted</param>
		public VizeliaEmailMessage(string toAddress, string fromAddress, string subjectLine, string message, ILogFormatter formatter) {
			this.configurationData = new VizeliaEmailTraceListenerData(toAddress, fromAddress, subjectLine, string.Empty);
			this.logEntry = new LogEntry();
			logEntry.Message = message;
			this.formatter = formatter;
		}

		/// <summary>
		/// Determines whether the string is <see langword="null"/> or empty
		/// </summary>
		/// <param name="subjectLineMarker">string to evaluate</param>
		/// <returns>Boolean value that returns true if the string is <see langword="null"/> or empty</returns>
		private bool IsEmpty(string subjectLineMarker) {
			return subjectLineMarker == null || subjectLineMarker.Length == 0;
		}

		/// <summary>
		/// Creates the prefix for the subject line
		/// </summary>
		/// <param name="subjectLineField">string to add as the subject line prefix (plus whitespace) if it is not empty.</param>
		/// <returns>modified string to use as subject line prefix</returns>
		private string GenerateSubjectPrefix(string subjectLineField) {
			return IsEmpty(subjectLineField)
				? ""
				: subjectLineField + " ";
		}

		/// <summary>
		/// Creates the suffix for the subject line.
		/// </summary>
		/// <param name="subjectLineField">string to add as the subject line suffix (plus whitespace) if it is not empty.</param>
		/// <returns>modified string to use as subject line suffix</returns>
		private string GenerateSubjectSuffix(string subjectLineField) {
			return IsEmpty(subjectLineField)
				? ""
				: " " + subjectLineField;
		}

		/// <summary>
		/// Creates a <see cref="MailMessage"/> from the configuration data which was used to create the instance of this object.
		/// </summary>
		/// <returns>A new <see cref="MailMessage"/>.</returns>
		protected MailMessage CreateMailMessage() {
			string sendToSmtpSubject;

			if(!string.IsNullOrWhiteSpace(configurationData.SubjectLine)) {
				sendToSmtpSubject = FormatField(configurationData.SubjectLine);
			}
			else {
				sendToSmtpSubject = logEntry.Severity.ToString();
			}
			
			MailMessage message = new MailMessage();
			message.Body = (formatter != null) ? formatter.Format(logEntry) : logEntry.Message;
			message.Subject = sendToSmtpSubject;
			message.BodyEncoding = Encoding.UTF8;

			string[] toAddresses = configurationData.ToAddress.Split(';');
			// get rid of empty addresses.
			foreach (string toAddress in toAddresses.Where(a => !string.IsNullOrWhiteSpace(a))) {
				int indexOfLt = toAddress.IndexOf("<");
				int indexOfGt = toAddress.IndexOf(">");
				string address;
				string displayName;

				if (indexOfLt != -1 && indexOfGt != -1) {
					address = toAddress.Substring(indexOfLt + 1, indexOfGt - indexOfLt - 1);
					displayName = toAddress.Substring(0, indexOfLt).Trim();
				}
				else {
					address = toAddress;
					displayName = toAddress;
				}

				message.To.Add(new MailAddress(address, displayName, Encoding.UTF8));
			}

			if (!(String.IsNullOrWhiteSpace(configurationData.FromAddress)))
			{
				var fieldValue = FormatField(configurationData.FromAddress);
				message.From = new MailAddress(fieldValue);
			}

			if (this.Attachments != null) {
				foreach (var item in this.Attachments) {
					message.Attachments.Add(item);
				}
			}

			return message;
		}

		private string FormatField(string field) {
			var fieldFormatter = new TextFormatter(field);
			string fieldValue = fieldFormatter.Format(logEntry);
			return fieldValue;
		}

		/// <summary>
		/// Uses the settings for the SMTP server and SMTP port to send the new mail message
		/// </summary>
		public virtual void Send() {
			// The message cannot be disposed here because it may be sent async.
			// Lets just let it be disposed when it is garbage collected...
			MailMessage message = CreateMailMessage();
			SendMessage(message);
		}

		/// <summary>
		/// Sends the message.
		/// </summary>
		/// <param name="message">The message to send</param>
		public virtual void SendMessage(MailMessage message) {
			MailService.Send(message);
		}
	}
}

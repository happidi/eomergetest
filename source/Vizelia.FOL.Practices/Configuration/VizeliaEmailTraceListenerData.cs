﻿//===============================================================================
// Microsoft patterns & practices Enterprise Library
// Logging Application Block
//===============================================================================
// Copyright © Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//===============================================================================

using System.ComponentModel;
using System.Configuration;
using System.Diagnostics;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration.Design;
using Microsoft.Practices.EnterpriseLibrary.Logging.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Logging.Formatters;
using Microsoft.Practices.EnterpriseLibrary.Logging.TraceListeners;
using Container = Microsoft.Practices.EnterpriseLibrary.Common.Configuration.ContainerModel.Container;

namespace Vizelia.FOL.Practices.Configuration {
	/// <summary>
	/// Represents the configuration settings that describe a <see cref="EmailTraceListener"/>.
	/// </summary>
	public class VizeliaEmailTraceListenerData : TraceListenerData {
		private const string toAddressProperty = "toAddress";
		private const string fromAddressProperty = "fromAddress";
		private const string subjectLineProperty = "subjectLine";
		private const string formatterNameProperty = "formatter";


		/// <summary>
		/// Gets the creation expression used to produce a <see cref="T:Microsoft.Practices.EnterpriseLibrary.Common.Configuration.ContainerModel.TypeRegistration"/> during
		/// <see cref="M:Microsoft.Practices.EnterpriseLibrary.Logging.Configuration.TraceListenerData.GetRegistrations"/>.
		/// </summary>
		/// <returns>
		/// A <see cref="T:System.Linq.Expressions.Expression"/> that creates a <see cref="T:System.Diagnostics.TraceListener"/>
		/// </returns>
		protected override System.Linq.Expressions.Expression<System.Func<TraceListener>> GetCreationExpression() {
			return () => new VizeliaEmailTraceListener(Container.ResolvedIfNotNull<ILogFormatter>(Formatter), ToAddress, FromAddress, SubjectLine);
		}

		/// <summary>
		/// Initializes a <see cref="EmailTraceListenerData"/>.
		/// </summary>
		public VizeliaEmailTraceListenerData()
			: base(typeof(VizeliaEmailTraceListener)) {
			ListenerDataType = GetType();
		}



		/// <summary>
		/// Initializes a <see cref="EmailTraceListenerData"/> with a toaddress,
		/// fromaddress, subjectLineStarter, subjectLineEnder, smtpServer, and a formatter name.
		/// </summary>
		/// <param name="toAddress">A semicolon delimited string the represents to whom the email should be sent.</param>
		/// <param name="fromAddress">Represents from whom the email is sent.</param>
		/// <param name="subjectLine">The subject line.</param>
		/// <param name="formatterName">The name of the Formatter <see cref="ILogFormatter"/> which determines how the
		/// email message should be formatted</param>
		public VizeliaEmailTraceListenerData(string toAddress, string fromAddress, string subjectLine, string formatterName)
			: this("unnamed", toAddress, fromAddress, subjectLine, formatterName) {
			ListenerDataType = GetType();
		}

		/// <summary>
		/// Initializes a <see cref="EmailTraceListenerData"/> with a toaddress,
		/// fromaddress, subjectLineStarter, subjectLineEnder, smtpServer, and a formatter name.
		/// </summary>
		/// <param name="name">The name of this listener</param>
		/// <param name="toAddress">A semicolon delimited string the represents to whom the email should be sent.</param>
		/// <param name="fromAddress">Represents from whom the email is sent.</param>
		/// <param name="subjectLine">The subject line.</param>
		/// <param name="formatterName">The name of the Formatter <see cref="ILogFormatter"/> which determines how the email message should be formatted</param>
		public VizeliaEmailTraceListenerData(string name, string toAddress, string fromAddress, string subjectLine, string formatterName)
			: this(name, toAddress, fromAddress, subjectLine, formatterName, TraceOptions.None) {
			ListenerDataType = GetType();
		}

		/// <summary>
		/// Initializes a <see cref="EmailTraceListenerData"/> with a toaddress,
		/// fromaddress, subjectLineStarter, subjectLineEnder, smtpServer, a formatter name and trace options.
		/// </summary>
		/// <param name="name">The name of this listener</param>
		/// <param name="toAddress">A semicolon delimited string the represents to whom the email should be sent.</param>
		/// <param name="fromAddress">Represents from whom the email is sent.</param>
		/// <param name="subjectLine">The subject line.</param>
		/// <param name="formatterName">The name of the Formatter <see cref="ILogFormatter"/> which determines how the
		/// email message should be formatted</param>
		/// <param name="traceOutputOptions">The trace options.</param>
		public VizeliaEmailTraceListenerData(string name, string toAddress, string fromAddress, string subjectLine, string formatterName, TraceOptions traceOutputOptions)
			: base(name, typeof(EmailTraceListener), traceOutputOptions) {
			ToAddress = toAddress;
			FromAddress = fromAddress;
			SubjectLine = subjectLine;
			Formatter = formatterName;
			ListenerDataType = GetType();
		}

		/// <summary>
		/// Initializes a <see cref="EmailTraceListenerData"/> with a toaddress,
		/// fromaddress, subjectLineStarter, subjectLineEnder, smtpServer, a formatter name and trace options.
		/// </summary>
		/// <param name="name">The name of this listener</param>
		/// <param name="toAddress">A semicolon delimited string the represents to whom the email should be sent.</param>
		/// <param name="fromAddress">Represents from whom the email is sent.</param>
		/// <param name="subjectLine">The subject line.</param>
		/// <param name="formatterName">The name of the Formatter <see cref="ILogFormatter"/> which determines how the
		/// email message should be formatted</param>
		/// <param name="traceOutputOptions">The trace options.</param>
		/// <param name="filter">The filter to apply.</param>
		public VizeliaEmailTraceListenerData(string name, string toAddress, string fromAddress, string subjectLine, string formatterName, TraceOptions traceOutputOptions, SourceLevels filter)
			: base(name, typeof(EmailTraceListener), traceOutputOptions, filter) {
			ToAddress = toAddress;
			FromAddress = fromAddress;
			SubjectLine = subjectLine;
			Formatter = formatterName;
			ListenerDataType = GetType();
		}

		/// <summary>
		/// Gets and sets the ToAddress.  One or more email semicolon separated addresses.
		/// </summary>
		[ConfigurationProperty(toAddressProperty, IsRequired = true)]
		public string ToAddress {
			get { return (string)base[toAddressProperty]; }
			set { base[toAddressProperty] = value; }
		}

		/// <summary>
		/// Gets and sets the FromAddress. Email address that messages will be sent from.
		/// </summary>
		[ConfigurationProperty(fromAddressProperty, IsRequired = false)]
		[DisplayName("From Address")]
		[Description("The Form Address value will be processed through the TextFormatter, variables are allowed.")]
		public string FromAddress {
			get { return (string)base[fromAddressProperty]; }
			set { base[fromAddressProperty] = value; }
		}

		/// <summary>
		/// Gets and sets the Subject format.
		/// </summary>
		[ConfigurationProperty(subjectLineProperty)]
		[DisplayName("Subject Line")]
		[Description("The subject Line value will be processed through the TextFormatter, variables are allowed.")]
		public string SubjectLine {
			get { return (string)base[subjectLineProperty]; }
			set { base[subjectLineProperty] = value; }
		}

		/// <summary>
		/// Gets and sets the formatter name.
		/// </summary>
		[ConfigurationProperty(formatterNameProperty, IsRequired = false),
		 Reference(typeof(NameTypeConfigurationElementCollection<FormatterData, CustomFormatterData>), typeof(FormatterData))]
		public string Formatter {
			get { return (string)base[formatterNameProperty]; }
			set { base[formatterNameProperty] = value; }
		}
	}

}

﻿using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Logging.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Logging.TraceListeners;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using System.Diagnostics;

namespace Vizelia.FOL.Practices {

	/// <summary>
	/// A custom trace listener that redirect its messages to a extended property (output) of the logEntry.
	/// </summary>
	[ConfigurationElementType(typeof(CustomTraceListenerData))]
	public class VizeliaStringBuilderTraceListener : CustomTraceListener {
	
		private StringBuilder builder;

		/// <summary>
		///  Writes trace information, a data object and event information to the listener specific output
		/// </summary>
		/// <param name="eventCache">A System.Diagnostics.TraceEventCache object that contains the current process ID, thread ID, and stack trace information.</param>
		/// <param name="source">A name used to identify the output, typically the name of the application that generated the trace event.</param>
		/// <param name="eventType">One of the System.Diagnostics.TraceEventType values specifying the type of event that has caused the trace.</param>
		/// <param name="id">A numeric identifier for the event.</param>
		/// <param name="data">The trace data to emit.</param>
		public override void TraceData(TraceEventCache eventCache, string source, TraceEventType eventType, int id, object data) {
			if (!(data is LogEntry))
				return;
			var logEntry = (LogEntry)data;
			// This string was once - Common.Helper.const_extendedproperty_output
			if (logEntry.ExtendedProperties.ContainsKey("output"))
				builder = (StringBuilder)logEntry.ExtendedProperties["output"];
			else
				builder = null;

			if (this.Formatter != null) {
				this.WriteLine(this.Formatter.Format(data as LogEntry));
			}
			else {
				this.WriteLine(data.ToString());
			}
		}

		/// <summary>
		/// writes the specified message to the listener
		/// </summary>
		/// <param name="message">A message to write.</param>
		public override void Write(string message) {
			if (builder != null)
				builder.Append(message);
		}

		/// <summary>
		/// writes a message to the listener followed by a line terminator.
		/// </summary>
		/// <param name="message">A message to write.</param>
		public override void WriteLine(string message) {
			if (builder != null)
				builder.AppendLine(message);
		}


	}

}

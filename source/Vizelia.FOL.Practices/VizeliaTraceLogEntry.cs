﻿using System;
using Microsoft.Practices.EnterpriseLibrary.Logging.PolicyInjection;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using Vizelia.FOL.Common.Configuration;
using System.Web;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Localization;


namespace Vizelia.FOL.Practices {
	/// <summary>
	/// Custom class for log entries.
	/// New properties can be added here and exposed through the token {property()} in the formatter.
	/// </summary>
	public class VizeliaTraceLogEntry : TraceLogEntry {

		/// <summary>
		/// The run instace id of the job
		/// </summary>
		public string JobRunInstanceId { get; set; }

		/// <summary>
		/// Id of the membership user.
		/// </summary>
		public string UserId {
			get { return GetUserId(); }
			set { }
		}


		/// <summary>
		/// Gets or sets the name of the application.
		/// </summary>
		/// <value>
		/// The name of the application.
		/// </value>
		public string ApplicationName {
			get { return ContextHelper.ApplicationName; }
			set { }
		}

		/// <summary>
		/// Gets or sets the start time.
		/// </summary>
		/// <value>
		/// The start time.
		/// </value>
		public string StartTime {
			get {
				DateTime startTime;

				if (CallTime.HasValue) {
					startTime = TimeStamp - CallTime.Value;
				}
				else {
					startTime = TimeStamp;
				}

				// We want this format: '2012-06-24 14:53:49.293'
				string s = startTime.ToString("yyyy'-'MM'-'dd HH':'mm':'ss'.'fff");

				return s;
			}
			set { }
		}

		/// <summary>
		/// Gets the version.
		/// </summary>
		public string Version {
			get { return Common.Version.Number; }
			set { }
		}

		/// <summary>
		/// Gets or sets the URL.
		/// </summary>
		/// <value>
		/// The URL.
		/// </value>
		public string Url {
			get { return GetUrl(); }
			set { }
		}
		/// <summary>
		/// Gets the user id.
		/// </summary>
		/// <returns></returns>
		private string GetUserId() {
			return Vizelia.FOL.Common.Helper.GetCurrentUserName() ?? "";
		}

		/// <summary>
		/// Gets the URL.
		/// </summary>
		/// <returns></returns>
		private string GetUrl() {
			string url = "";
			try {
				url = ContextHelper.Url;
			}
			catch { }
			return url ?? "";
		}

		/// <summary>
		/// Public ctor.
		/// </summary>
		public VizeliaTraceLogEntry() {

		}

		/// <summary>
		/// Gets or sets the application title.
		/// </summary>
		/// <value>
		/// The application title.
		/// </value>
		public string ApplicationTitle {
			get { return Langue.msg_application_title; }
			set { }
		}

		/// <summary>
		/// Public ctor. To build from a TraceLogEntry logEntry.
		/// </summary>
		/// <param name="logEntry">The TraceLogEntry.</param>
		public VizeliaTraceLogEntry(TraceLogEntry logEntry) {
			this.ActivityId = logEntry.ActivityId;
			this.AppDomainName = logEntry.AppDomainName;
			this.CallStack = logEntry.CallStack;
			this.CallTime = logEntry.CallTime;
			this.Categories = logEntry.Categories;
			this.EventId = logEntry.EventId;
			this.Exception = logEntry.Exception;
			this.ExtendedProperties = logEntry.ExtendedProperties;
			this.MachineName = logEntry.MachineName;
			this.ManagedThreadName = logEntry.ManagedThreadName;
			this.Message = logEntry.Message;
			this.MethodName = logEntry.MethodName;
			this.Priority = logEntry.Priority;
			this.ProcessId = logEntry.ProcessId;
			this.ProcessName = logEntry.ProcessName;
			this.RelatedActivityId = logEntry.RelatedActivityId;
			this.ReturnValue = logEntry.ReturnValue;
			this.Severity = logEntry.Severity;
			this.TimeStamp = logEntry.TimeStamp;
			this.Title = logEntry.Title;
			this.TypeName = logEntry.TypeName;
			this.Win32ThreadId = logEntry.Win32ThreadId;
		}

		/// <summary>
		/// Public ctor. To build from a LogEntry logEntry.
		/// </summary>
		/// <param name="logEntry">The TraceLogEntry.</param>
		public VizeliaTraceLogEntry(LogEntry logEntry) {
			this.ActivityId = logEntry.ActivityId;
			this.AppDomainName = logEntry.AppDomainName;
			this.Categories = logEntry.Categories;
			this.EventId = logEntry.EventId;
			this.ExtendedProperties = logEntry.ExtendedProperties;
			this.MachineName = logEntry.MachineName;
			this.ManagedThreadName = logEntry.ManagedThreadName;
			this.Message = logEntry.Message;
			this.Priority = logEntry.Priority;
			this.ProcessId = logEntry.ProcessId;
			this.ProcessName = logEntry.ProcessName;
			this.RelatedActivityId = logEntry.RelatedActivityId;
			this.Severity = logEntry.Severity;
			this.TimeStamp = logEntry.TimeStamp;
			this.Title = logEntry.Title;
			this.Win32ThreadId = logEntry.Win32ThreadId;
		}
	}
}

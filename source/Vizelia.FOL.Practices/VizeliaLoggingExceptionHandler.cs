﻿using System;
using System.Collections;
using System.Diagnostics;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.Logging;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using Vizelia.FOL.Practices.Configuration;

namespace Vizelia.FOL.Practices {
	/// <summary>
	/// A Vizelia Logging Exception Handler that logs using the VizeliaTraceLogEntry.
	/// </summary>
	[ConfigurationElementType(typeof(VizeliaLoggingExceptionHandlerData))]
	public class VizeliaLoggingExceptionHandler : LoggingExceptionHandler {
		private readonly string logCategory;
		private readonly int eventId;
		private readonly TraceEventType severity;
		private readonly string defaultTitle;
		private readonly int minimumPriority;
		private readonly LogWriter logWriter;

		/// <summary>
		/// Initializes a new instance of the <see cref="VizeliaLoggingExceptionHandler"/> class.
		/// </summary>
		/// <param name="logCategory">The log category.</param>
		/// <param name="eventId">The event id.</param>
		/// <param name="severity">The severity.</param>
		/// <param name="title">The title.</param>
		/// <param name="priority">The priority.</param>
		/// <param name="formatterType">Type of the formatter.</param>
		/// <param name="writer">The writer.</param>
		public VizeliaLoggingExceptionHandler(string logCategory, int eventId, TraceEventType severity, string title, int priority, Type formatterType, LogWriter writer) : base(logCategory, eventId, severity, title, priority, formatterType, writer) {
			this.logCategory = logCategory;
			this.eventId = eventId;
			this.severity = severity;
			this.defaultTitle = title;
			this.minimumPriority = priority;
			this.logWriter = writer;
		}

		/// <summary>
		/// Writes the specified log message using 
		/// the Logging Application Block's <see cref="Logger.Write(LogEntry)"/>
		/// method.
		/// </summary>
		/// <param name="logMessage">The message to write to the log.</param>
		/// <param name="exceptionData">The exception's data.</param>
		protected override void WriteToLog(string logMessage, IDictionary exceptionData) {
			LogEntry entry = new LogEntry(
				logMessage,
				logCategory,
				minimumPriority,
				eventId,
				severity,
				defaultTitle,
				null);

			foreach (DictionaryEntry dataEntry in exceptionData) {
				if (dataEntry.Key is string) {
					entry.ExtendedProperties.Add(dataEntry.Key as string, dataEntry.Value);
				}
			}

			var vizeliaLogEntry = new VizeliaTraceLogEntry(entry);
			this.logWriter.Write(vizeliaLogEntry);
		}

	}
}
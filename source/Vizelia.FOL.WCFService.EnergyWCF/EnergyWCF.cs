﻿using System;
using System.Collections.Generic;
using System.IO;
using System.ServiceModel;
using System.ServiceModel.Web;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessLayer;
using Vizelia.FOL.BusinessLayer.Interfaces;
using Vizelia.FOL.Common;
using Vizelia.FOL.Providers;
using Vizelia.FOL.WCFService.Contracts;

namespace Vizelia.FOL.WCFService {
	/// <summary>
	/// Implementation for SpaceManagement Services.
	/// </summary>
	[ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple, InstanceContextMode = InstanceContextMode.PerCall, UseSynchronizationContext = false)]
	public class EnergyWCF : BaseModuleWCF, IEnergyWCF {

		private readonly IEnergyBusinessLayer m_EnergyBusinessLayer;



		/// <summary>
		/// Public ctor.
		/// </summary>
		public EnergyWCF() {
			// Policy injection on the business layer instance.
			m_EnergyBusinessLayer = Helper.CreateInstance<EnergyBusinessLayer, IEnergyBusinessLayer>();
		}



		/// <summary>
		/// Deletes an existing business entity AlarmDefinition.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool AlarmDefinition_Delete(AlarmDefinition item) {
			return m_EnergyBusinessLayer.AlarmDefinition_Delete(item);
		}

		/// <summary>
		/// Creates a new business entity AlarmDefinition and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <param name="meters">The meters.</param>
		/// <param name="filterSpatial">The filter spatial.</param>
		/// <param name="filterMeterClassification">The filter meter classification.</param>
		/// <param name="charts">The charts.</param>
		/// <returns></returns>
		public FormResponse AlarmDefinition_FormCreate(AlarmDefinition item, CrudStore<Meter> meters,
													   CrudStore<Location> filterSpatial,
													   CrudStore<ClassificationItem> filterMeterClassification,
													   CrudStore<Chart> charts) {
			return m_EnergyBusinessLayer.AlarmDefinition_FormCreate(item, meters, filterSpatial, filterMeterClassification,
																	charts);
		}

		/// <summary>
		/// Loads a specific item for the business entity AlarmDefinition.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse AlarmDefinition_FormLoad(string Key) {
			return m_EnergyBusinessLayer.AlarmDefinition_FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity AlarmDefinition and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <param name="meters">The meters.</param>
		/// <param name="filterSpatial">The filter spatial.</param>
		/// <param name="filterMeterClassification">The filter meter classification.</param>
		/// <param name="filterSpatialPset">The filter spatial pset.</param>
		/// <param name="charts">The charts.</param>
		/// <returns></returns>
		public FormResponse AlarmDefinition_FormUpdate(AlarmDefinition item, CrudStore<Meter> meters, CrudStore<Location> filterSpatial, CrudStore<ClassificationItem> filterMeterClassification, CrudStore<FilterSpatialPset> filterSpatialPset, CrudStore<Chart> charts) {
			return m_EnergyBusinessLayer.AlarmDefinition_FormUpdate(item, meters, filterSpatial, filterMeterClassification, filterSpatialPset, charts);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse AlarmDefinition_FormUpdateBatch(string[] keys, AlarmDefinition item) {
			return m_EnergyBusinessLayer.AlarmDefinition_FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity AlarmDefinition. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		public List<AlarmDefinition> AlarmDefinition_GetAll(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.AlarmDefinition_GetAll(paging, location);
		}

		/// <summary>
		/// Gets a specific item for the business entity AlarmDefinition.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public AlarmDefinition AlarmDefinition_GetItem(string Key) {
			return m_EnergyBusinessLayer.AlarmDefinition_GetItem(Key);
		}

		/// <summary>
		/// Return a json store of the complete list of Meters associated with a AlarmDefinition.
		/// including meters associated via SpatialFilter cross SpatialFilterPset cross MeterClassification
		/// </summary>
		/// <param name="KeyAlarmDefinition">The key of the AlarmDefinition.</param>
		/// <param name="paging">The paging.</param>
		/// <returns></returns>
		public JsonStore<Meter> AlarmDefinition_GetMetersStore(string KeyAlarmDefinition, PagingParameter paging) {
			return m_EnergyBusinessLayer.AlarmDefinition_GetMetersStore(KeyAlarmDefinition, paging);
		}

		/// <summary>
		/// Gets a json store for the business entity AlarmDefinition.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<AlarmDefinition> AlarmDefinition_GetStore(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.AlarmDefinition_GetStore(paging, location);
		}

		/// <summary>
		/// Gets the AlarmDefinition store by a job and a step.
		/// </summary>
		/// <param name="keyJob">The key job.</param>
		/// <param name="keyStep">The key step.</param>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>
		/// A json store.
		/// </returns>
		public JsonStore<AlarmDefinition> AlarmDefinition_GetStoreByJobStep(string keyJob, string keyStep, PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.AlarmDefinition_GetStoreByJobStep(keyJob, keyStep, paging, location);
		}

		/// <summary>
		/// Gets a json store of AlarmDefinition for a specific AlarmTable.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyAlarmTable">The Key of the AlarmTable.</param>
		/// <returns>
		/// A json store.
		/// </returns>
		public JsonStore<AlarmDefinition> AlarmDefinition_GetStoreByKeyAlarmTable(PagingParameter paging, string KeyAlarmTable) {
			return m_EnergyBusinessLayer.AlarmDefinition_GetStoreByKeyAlarmTable(paging, KeyAlarmTable);
		}

		/// <summary>
		/// Gets all the enabled AlarmDefinition and process them to generate the AlarmInstances
		/// </summary>
		/// <param name="Keys">The keys.</param>
		public void AlarmDefinition_Process(string[] Keys) {
			m_EnergyBusinessLayer.AlarmDefinition_Process(Keys);
		}

		/// <summary>
		/// Gets all the enabled AlarmDefinition and process them to generate the AlarmInstanceswith a long running operation.
		/// </summary>
		/// <param name="operationId">The id of the operation.</param>
		/// <param name="Keys">The keys.</param>
		public void AlarmDefinition_ProcessBegin(Guid operationId, string[] Keys) {
			m_EnergyBusinessLayer.AlarmDefinition_ProcessBegin(operationId, Keys);
		}

		/// <summary>
		/// Saves a crud store for the business entity AlarmDefinition.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<AlarmDefinition> AlarmDefinition_SaveStore(CrudStore<AlarmDefinition> store) {
			return m_EnergyBusinessLayer.AlarmDefinition_SaveStore(store);
		}

		/// <summary>
		/// Begins long running operation to Save a crud store for the business entity AlarmDefinition.
		/// </summary>
		/// <param name="operationId">The operation identifier.</param>
		/// <param name="store">The crud store.</param>
		public void AlarmDefinition_BeginSaveStore(Guid operationId, CrudStore<AlarmDefinition> store) {
			m_EnergyBusinessLayer.AlarmDefinition_BeginSaveStore(operationId, store);
		}

		/// <summary>
		/// Generates the json structure of the ClassificationItem treeview for the AlarmDefinition Classification..
		/// </summary>
		/// <param name="Key">The Key of the parent node.</param>
		/// <param name="entity">The parent entity.</param>
		/// <returns></returns>
		public List<TreeNode> AlarmDefinitionClassification_GetTree(string Key, ClassificationItem entity) {
			return m_EnergyBusinessLayer.AlarmDefinitionClassification_GetTree(Key, entity);
		}

		/// <summary>
		/// Deletes an existing business entity AlarmInstance.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool AlarmInstance_Delete(AlarmInstance item) {
			return m_EnergyBusinessLayer.AlarmInstance_Delete(item);
		}

		/// <summary>
		/// Deletes all existing alarm instances for a specific AlarmDefinition or MeterValidationRule.
		/// </summary>
		/// <param name="KeyAlarmDefinition">The key alarm definition.</param>
		/// <param name="KeyMeterValidationRule">The key meter validation rule.</param>
		/// <returns></returns>
		public bool AlarmInstance_DeleteAll(string KeyAlarmDefinition, string KeyMeterValidationRule) {
			return m_EnergyBusinessLayer.AlarmInstance_DeleteAll(KeyAlarmDefinition, KeyMeterValidationRule);
		}

		/// <summary>
		/// Begins long running operation to deletes all existing alarm instances for a specific AlarmDefinition or MeterValidationRule.
		/// </summary>
		/// <param name="operationId">The operation identifier.</param>
		/// <param name="KeyAlarmDefinition">The key alarm definition.</param>
		/// <param name="KeyMeterValidationRule">The key meter validation rule.</param>
		public void AlarmInstance_BeginDeleteAll(Guid operationId, string KeyAlarmDefinition, string KeyMeterValidationRule) {
			m_EnergyBusinessLayer.AlarmInstance_BeginDeleteAll(operationId, KeyAlarmDefinition, KeyMeterValidationRule);
		}

		/// <summary>
		/// Creates a new business entity AlarmInstance and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse AlarmInstance_FormCreate(AlarmInstance item) {
			return m_EnergyBusinessLayer.AlarmInstance_FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity AlarmInstance.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse AlarmInstance_FormLoad(string Key) {
			return m_EnergyBusinessLayer.AlarmInstance_FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity AlarmInstance and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse AlarmInstance_FormUpdate(AlarmInstance item) {
			return m_EnergyBusinessLayer.AlarmInstance_FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse AlarmInstance_FormUpdateBatch(string[] keys, AlarmInstance item) {
			return m_EnergyBusinessLayer.AlarmInstance_FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity AlarmInstance. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		public List<AlarmInstance> AlarmInstance_GetAll(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.AlarmInstance_GetAll(paging, location);
		}

		/// <summary>
		/// Gets a specific item for the business entity AlarmInstance.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public AlarmInstance AlarmInstance_GetItem(string Key) {
			return m_EnergyBusinessLayer.AlarmInstance_GetItem(Key);
		}

		/// <summary>
		/// Get the latest AlarmInstances with an InstanceDatetime newer than Now - seconds.
		/// </summary>
		/// <param name="seconds">The seconds.</param>
		/// <returns></returns>
		public List<AlarmInstance> AlarmInstance_GetLatest(int seconds) {
			return m_EnergyBusinessLayer.AlarmInstance_GetLatest(seconds);
		}

		/// <summary>
		/// Gets a json store for the business entity AlarmInstance.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<AlarmInstance> AlarmInstance_GetStore(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.AlarmInstance_GetStore(paging, location);
		}

		/// <summary>
		/// Gets all the AlarmInstance for a specific AlarmDefinition.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyAlarmDefinition">The Key of the AlarmDefinition.</param>
		public JsonStore<AlarmInstance> AlarmInstance_GetStoreByKeyAlarmDefinition(PagingParameter paging,
																				   string KeyAlarmDefinition) {
			return m_EnergyBusinessLayer.AlarmInstance_GetStoreByKeyAlarmDefinition(paging, KeyAlarmDefinition);
		}

		/// <summary>
		/// Gets all the AlarmInstance for a specific AlarmTable.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyAlarmTable">The Key of the AlarmTable.</param>
		public JsonStore<AlarmInstance> AlarmInstance_GetStoreByKeyAlarmTable(PagingParameter paging, string KeyAlarmTable) {
			return m_EnergyBusinessLayer.AlarmInstance_GetStoreByKeyAlarmTable(paging, KeyAlarmTable);
		}

		/// <summary>
		/// Gets all the AlarmInstance for a specific Meter.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyMeter">The key meter.</param>
		/// <returns></returns>
		public JsonStore<AlarmInstance> AlarmInstance_GetStoreByKeyMeter(PagingParameter paging, string KeyMeter) {
			return m_EnergyBusinessLayer.AlarmInstance_GetStoreByKeyMeter(paging, KeyMeter);
		}

		/// <summary>
		/// Gets all the AlarmInstance for a specific MeterData.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyMeterData">The key meter data.</param>
		/// <returns></returns>
		public JsonStore<AlarmInstance> AlarmInstance_GetStoreByKeyMeterData(PagingParameter paging, string KeyMeterData) {
			return m_EnergyBusinessLayer.AlarmInstance_GetStoreByKeyMeterData(paging, KeyMeterData);
		}

		/// <summary>
		/// Gets all the AlarmInstance for a specific MeterValidationRule.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyMeterValidationRule">The key meter validation rule.</param>
		/// <returns></returns>
		public JsonStore<AlarmInstance> AlarmInstance_GetStoreByKeyMeterValidationRule(PagingParameter paging, string KeyMeterValidationRule) {
			return m_EnergyBusinessLayer.AlarmInstance_GetStoreByKeyMeterValidationRule(paging, KeyMeterValidationRule);
		}

		/// <summary>
		/// Saves a crud store for the business entity AlarmInstance.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<AlarmInstance> AlarmInstance_SaveStore(CrudStore<AlarmInstance> store) {
			return m_EnergyBusinessLayer.AlarmInstance_SaveStore(store);
		}

		/// <summary>
		/// Deletes an existing business entity AlarmTable.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool AlarmTable_Delete(AlarmTable item) {
			return m_EnergyBusinessLayer.AlarmTable_Delete(item);
		}

		/// <summary>
		/// Creates a new business entity AlarmTable and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <param name="alarmdefinitions">The alarmdefinitions.</param>
		/// <param name="filterSpatial">The filter spatial.</param>
		/// <param name="filterAlarmDefinitionClassification">The filter alarm definition classification.</param>
		/// <returns></returns>
		public FormResponse AlarmTable_FormCreate(AlarmTable item, CrudStore<AlarmDefinition> alarmdefinitions,
												  CrudStore<Location> filterSpatial,
												  CrudStore<ClassificationItem> filterAlarmDefinitionClassification) {
			return m_EnergyBusinessLayer.AlarmTable_FormCreate(item, alarmdefinitions, filterSpatial,
															   filterAlarmDefinitionClassification);
		}

		/// <summary>
		/// Loads a specific item for the business entity AlarmTable.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse AlarmTable_FormLoad(string Key) {
			return m_EnergyBusinessLayer.AlarmTable_FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity AlarmTable and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <param name="alarmdefinitions">The alarmdefinitions.</param>
		/// <param name="filterSpatial">The filter spatial.</param>
		/// <param name="filterAlarmDefinitionClassification">The filter alarm definition classification.</param>
		/// <returns></returns>
		public FormResponse AlarmTable_FormUpdate(AlarmTable item, CrudStore<AlarmDefinition> alarmdefinitions,
												  CrudStore<Location> filterSpatial,
												  CrudStore<ClassificationItem> filterAlarmDefinitionClassification) {
			return m_EnergyBusinessLayer.AlarmTable_FormUpdate(item, alarmdefinitions, filterSpatial,
															   filterAlarmDefinitionClassification);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse AlarmTable_FormUpdateBatch(string[] keys, AlarmTable item) {
			return m_EnergyBusinessLayer.AlarmTable_FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Return a json store of the complete list of AlarmDefinitions associated with a AlarmTable
		/// including AlarmDefinitions associated via SpatialFilter  cross AlarmDefinitionClassification.
		/// </summary>
		/// <param name="KeyAlarmTable">The key alarm table.</param>
		/// <param name="paging">The paging.</param>
		/// <returns></returns>
		public JsonStore<AlarmDefinition> AlarmTable_GetAlarmDefinitionsStore(string KeyAlarmTable, PagingParameter paging) {
			return m_EnergyBusinessLayer.AlarmTable_GetAlarmDefinitionsStore(KeyAlarmTable, paging);
		}

		/// <summary>
		/// Gets a list for the business entity AlarmTable. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		public List<AlarmTable> AlarmTable_GetAll(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.AlarmTable_GetAll(paging, location);
		}

		/// <summary>
		/// Gets a specific item for the business entity AlarmTable.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public AlarmTable AlarmTable_GetItem(string Key) {
			return m_EnergyBusinessLayer.AlarmTable_GetItem(Key);
		}

		/// <summary>
		/// Gets a json store for the business entity AlarmTable.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<AlarmTable> AlarmTable_GetStore(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.AlarmTable_GetStore(paging, location);
		}

		/// <summary>
		/// Saves a crud store for the business entity AlarmTable.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<AlarmTable> AlarmTable_SaveStore(CrudStore<AlarmTable> store) {
			return m_EnergyBusinessLayer.AlarmTable_SaveStore(store);
		}

		/// <summary>
		/// Checks if the formula is valid.
		/// </summary>
		///<param name="KeyChart">The Key of the Chart the calculatedseries belong to.</param>
		/// <param name="field">the name of the field.</param>
		/// <param name="value">the value of the field.</param>
		/// <returns></returns>
		public FormResponse CalculatedSerie_CheckFormula(string KeyChart, string field, string value) {
			return m_EnergyBusinessLayer.CalculatedSerie_CheckFormula(KeyChart, field, value);
		}

		/// <summary>
		/// Deletes an existing business entity CalculatedSerie.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool CalculatedSerie_Delete(CalculatedSerie item) {
			return m_EnergyBusinessLayer.CalculatedSerie_Delete(item);
		}

		/// <summary>
		/// Creates a new business entity CalculatedSerie and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse CalculatedSerie_FormCreate(CalculatedSerie item) {
			return m_EnergyBusinessLayer.CalculatedSerie_FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity CalculatedSerie.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse CalculatedSerie_FormLoad(string Key) {
			return m_EnergyBusinessLayer.CalculatedSerie_FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity CalculatedSerie and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse CalculatedSerie_FormUpdate(CalculatedSerie item) {
			return m_EnergyBusinessLayer.CalculatedSerie_FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse CalculatedSerie_FormUpdateBatch(string[] keys, CalculatedSerie item) {
			return m_EnergyBusinessLayer.CalculatedSerie_FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity CalculatedSerie. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		public List<CalculatedSerie> CalculatedSerie_GetAll(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.CalculatedSerie_GetAll(paging, location);
		}

		/// <summary>
		/// Gets a specific item for the business entity CalculatedSerie.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public CalculatedSerie CalculatedSerie_GetItem(string Key) {
			return m_EnergyBusinessLayer.CalculatedSerie_GetItem(Key);
		}

		/// <summary>
		/// Gets a json store for the business entity CalculatedSerie.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<CalculatedSerie> CalculatedSerie_GetStore(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.CalculatedSerie_GetStore(paging, location);
		}

		/// <summary>
		/// Gets a store of calculatedSeries for a specific chart.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyChart">The key of the chart.</param>
		/// <returns></returns>
		public JsonStore<CalculatedSerie> CalculatedSerie_GetStoreByKeyChart(PagingParameter paging, string KeyChart) {
			return m_EnergyBusinessLayer.CalculatedSerie_GetStoreByKeyChart(paging, KeyChart);
		}

		/// <summary>
		/// Saves a crud store for the business entity CalculatedSerie.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<CalculatedSerie> CalculatedSerie_SaveStore(CrudStore<CalculatedSerie> store) {
			return m_EnergyBusinessLayer.CalculatedSerie_SaveStore(store);
		}

		/// <summary>
		/// Add a classification item to an existing Chart(used in dragdrop of classification tree to chart).
		/// </summary>
		/// <param name="KeyChart">The key chart.</param>
		/// <param name="Key">The key of the entity.</param>
		/// <param name="type">The type of entity.</param>
		/// <returns></returns>
		public Chart Chart_AddClassificationItemEntity(string KeyChart, string Key, string type) {
			return m_EnergyBusinessLayer.Chart_AddClassificationItemEntity(KeyChart, Key, type);
		}

		/// <summary>
		/// Add a spatial entity (Site, Building, Meter) to an existing Chart(used in dragdrop of spatial tree to chart).
		/// </summary>
		/// <param name="KeyChart">The key chart.</param>
		/// <param name="Key">The key of the entity.</param>
		/// <param name="type">The type of entity.</param>
		/// <returns></returns>
		public Chart Chart_AddSpatialEntity(string KeyChart, string Key, string type) {
			return m_EnergyBusinessLayer.Chart_AddSpatialEntity(KeyChart, Key, type);
		}

		/// <summary>
		/// Apply ModernUI color to a chart.
		/// </summary>
		/// <param name="Key">The key.</param>
		/// <param name="color">The color.</param>
		/// <returns></returns>
		public Chart Chart_ApplyModernUIColor(string Key, ModernUIColor color) {
			return m_EnergyBusinessLayer.Chart_ApplyModernUIColor(Key, color);
		}

		/// <summary>
		/// Builds the image (with map) from a chart.
		/// </summary>
		/// <param name="Key">The key.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <returns></returns>
		public ImageMap Chart_BuildImage(string Key, int width, int height) {
			return m_EnergyBusinessLayer.Chart_BuildImage(Key, width, height);
		}

		/// <summary>
		/// Clear the cache of any information related to this Charts.
		/// </summary>
		/// <param name="Key">The key.</param>
		/// <param name="KeyDynamicDisplay">The key dynamic display.</param>
		public void Chart_ClearCache(string Key, string KeyDynamicDisplay = null) {
			m_EnergyBusinessLayer.Chart_ClearCache(Key, KeyDynamicDisplay);
		}

		/// <summary>
		/// Copy existing business entity Chart.
		/// </summary>
		/// <param name="keys">The list of keys of item to copy </param>
		/// <returns></returns>
		public FormResponse Chart_Copy(string[] keys) {
			return m_EnergyBusinessLayer.Chart_Copy(keys);
		}

		/// <summary>
		/// Remove a point from the Correlation analysis.
		/// </summary>
		/// <param name="KeyChart">The key chart.</param>
		/// <param name="xValue">The x value.</param>
		/// <param name="yValue">The y value.</param>
		/// <param name="LocalId">The local id.</param>
		/// <returns></returns>
		public Chart Chart_CorrelationRemovePoint(string KeyChart, double xValue, double yValue, string LocalId) {
			return m_EnergyBusinessLayer.Chart_CorrelationRemovePoint(KeyChart, xValue, yValue, LocalId);
		}

		/// <summary>
		/// Deletes an existing business entity Chart.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool Chart_Delete(Chart item) {
			return m_EnergyBusinessLayer.Chart_Delete(item);
		}

		/// <summary>
		/// Creates a new business entity Chart and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <param name="filterSpatial">The filter spatial.</param>
		/// <param name="filterMeterClassification">The filter meter classification.</param>
		/// <param name="filterEventLogClassification">The filter event log classification.</param>
		/// <param name="filterAlarmDefinitionClassification">The filter alarm definition classification.</param>
		/// <param name="filterAzManRoles">The filter KPI az man roles.</param>
		/// <param name="meters">The store of the meters of the chart.</param>
		/// <param name="dataseries">The store of the dataseries of the Chart.</param>
		/// <param name="calculatedseries">The store of the calculatedseries of the Chart.</param>
		/// <param name="statisticalseries">The statisticalseries.</param>
		/// <param name="chartaxis">The store of chartaxis of the Chart.</param>
		/// <param name="chartmarker">The store of chartmarker of the Chart.</param>
		/// <param name="charthistoricalanalysis">the store of ChartHistoricalAnalysis of the Chart.</param>
		/// <param name="historicals">The historicals.</param>
		/// <param name="Algorithms">The Algorithms.</param>
		/// <param name="KeyChartToCopy">The key chart to copy (coming from the Chart wizard KPI).</param>
		/// <returns></returns>
		public FormResponse Chart_FormCreate(Chart item, CrudStore<Location> filterSpatial, CrudStore<ClassificationItem> filterMeterClassification, CrudStore<ClassificationItem> filterEventLogClassification, CrudStore<ClassificationItem> filterAlarmDefinitionClassification, CrudStore<AuthorizationItem> filterAzManRoles, CrudStore<Meter> meters, CrudStore<DataSerie> dataseries, CrudStore<CalculatedSerie> calculatedseries, CrudStore<StatisticalSerie> statisticalseries, CrudStore<ChartAxis> chartaxis, CrudStore<ChartMarker> chartmarker, CrudStore<ChartHistoricalAnalysis> charthistoricalanalysis, CrudStore<ChartPsetAttributeHistorical> historicals, CrudStore<ChartAlgorithm> Algorithms, string KeyChartToCopy) {
			return m_EnergyBusinessLayer.Chart_FormCreate(item, filterSpatial, filterMeterClassification, filterEventLogClassification, filterAlarmDefinitionClassification, filterAzManRoles, meters, dataseries, calculatedseries, statisticalseries, chartaxis, chartmarker, charthistoricalanalysis, historicals, Algorithms, KeyChartToCopy);
		}

		/// <summary>
		/// Loads a specific item for the business entity Chart.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse Chart_FormLoad(string Key) {
			return m_EnergyBusinessLayer.Chart_FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity Chart and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <param name="filterSpatial">The filter spatial.</param>
		/// <param name="filterMeterClassification">The filter meter classification.</param>
		/// <param name="filterEventLogClassification">The filter event log classification.</param>
		/// <param name="filterAlarmDefinitionClassification">The filter alarm definition classification.</param>
		/// <param name="filterAzManRoles">The filter KPI az man roles.</param>
		/// <param name="meters">The store of the meters of the Chart.</param>
		/// <param name="dataseries">The store of the dataseries of the Chart.</param>
		/// <param name="calculatedseries">The store of the calculatedseries of the Chart.</param>
		/// <param name="statisticalseries">The statisticalseries.</param>
		/// <param name="chartaxis">The store of chartaxis of the Chart.</param>
		/// <param name="chartmarker">The store of chartmarker of the Chart.</param>
		/// <param name="charthistoricalanalysis">the store of ChartHistoricalAnalysis of the Chart.</param>
		/// <param name="chartfilterpset">The pset filters associated to the chart.</param>
		/// <param name="historicals">The historicals.</param>
		/// <param name="Algorithms">The Algorithms.</param>
		/// <returns></returns>
		public FormResponse Chart_FormUpdate(Chart item, CrudStore<Location> filterSpatial, CrudStore<ClassificationItem> filterMeterClassification, CrudStore<ClassificationItem> filterEventLogClassification, CrudStore<ClassificationItem> filterAlarmDefinitionClassification, CrudStore<AuthorizationItem> filterAzManRoles, CrudStore<Meter> meters, CrudStore<DataSerie> dataseries, CrudStore<CalculatedSerie> calculatedseries, CrudStore<StatisticalSerie> statisticalseries, CrudStore<ChartAxis> chartaxis, CrudStore<ChartMarker> chartmarker, CrudStore<ChartHistoricalAnalysis> charthistoricalanalysis, CrudStore<FilterSpatialPset> chartfilterpset, CrudStore<ChartPsetAttributeHistorical> historicals, CrudStore<ChartAlgorithm> Algorithms) {
			return m_EnergyBusinessLayer.Chart_FormUpdate(item, filterSpatial, filterMeterClassification, filterEventLogClassification,
				filterAlarmDefinitionClassification, filterAzManRoles, meters, dataseries, calculatedseries, statisticalseries,
				chartaxis, chartmarker, charthistoricalanalysis, chartfilterpset, historicals, Algorithms);
		}

		/// <summary>
		/// Returns the requested chart. Unlike UpdateChart method, which updates all the Chart additional information(meters, spatial hierarchy, classification),
		/// this methos updates only the core Chart entity.
		/// </summary>
		/// <param name="item"></param>
		/// <returns></returns>
		public FormResponse Chart_ChangeView(Chart item) {
			return m_EnergyBusinessLayer.Chart_ChangeView(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse Chart_FormUpdateBatch(string[] keys, Chart item) {
			return m_EnergyBusinessLayer.Chart_FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity Chart. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		public List<Chart> Chart_GetAll(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.Chart_GetAll(paging, location);
		}

		/// <summary>
		/// Return a store of available Series for a specific Chart.
		/// </summary>
		/// <param name="Key">The Key of the Chart.</param>
		/// <param name="includeExisting">True to include existing serie, false to return only series to be created.</param>
		/// <returns></returns>
		public JsonStore<ListElement> Chart_GetAvailableDataSerieLocalId(string Key, bool includeExisting) {
			return m_EnergyBusinessLayer.Chart_GetAvailableDataSerieLocalId(Key, includeExisting);
		}

		/// <summary>
		/// Return the Chart Classification level store.
		/// </summary>
		/// <returns></returns>
		public JsonStore<SimpleListElementGeneric<int>> Chart_GetClassificationLevelStore() {
			var retVal = m_EnergyBusinessLayer.Chart_GetClassificationLevelStore();
			return retVal;
		}

		/// <summary>
		/// Gets a specific item for the business entity Chart.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public Chart Chart_GetItem(string Key) {
			return m_EnergyBusinessLayer.Chart_GetItem(Key);
		}

		/// <summary>
		/// Gets a specific item for the business entity Chart and fetch data to create the series .
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns>The Chart Item</returns>
		public Chart Chart_GetItemWithData(string Key) {
			return m_EnergyBusinessLayer.Chart_GetItemWithData(Key);
		}

		/// <summary>
		/// Gets the store of possible grouping localisation for Charts. 
		/// </summary>
		/// <returns></returns>
		public JsonStore<ListElementGeneric<int>> Chart_GetLocalisationStore() {
			return m_EnergyBusinessLayer.Chart_GetLocalisationStore();
		}

		/// <summary>
		/// Return a json store of the complete list of Meters associated with a Chart.
		/// including meters associated via SpatialFilter cross SpatialFilterPset cross MeterClassification
		/// </summary>
		/// <param name="KeyChart">The key of the Chart.</param>
		/// <param name="paging">The paging.</param>
		/// <returns></returns>
		public JsonStore<Meter> Chart_GetMetersStore(string KeyChart, PagingParameter paging) {
			return m_EnergyBusinessLayer.Chart_GetMetersStore(KeyChart, paging);
		}

		/// <summary>
		/// Gets a json store for the business entity Chart.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<Chart> Chart_GetStore(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.Chart_GetStore(paging, location);
		}

		/// <summary>
		/// Gets a json store for the business entity Chart for a specific AlarmDefinition.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyAlarmDefinition">The key AlarmDefinition.</param>
		/// <returns>
		/// A json store.
		/// </returns>
		public JsonStore<Chart> Chart_GetStoreByKeyAlarmDefinition(PagingParameter paging, string KeyAlarmDefinition) {
			return m_EnergyBusinessLayer.Chart_GetStoreByKeyAlarmDefinition(paging, KeyAlarmDefinition);
		}

		/// <summary>
		/// Gets a json store for the business entity Chart for a specific ChartScheduler.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyChartScheduler">The key chart scheduler.</param>
		/// <returns>
		/// A json store.
		/// </returns>
		public JsonStore<Chart> Chart_GetStoreByKeyChartScheduler(PagingParameter paging, string KeyChartScheduler) {
			return m_EnergyBusinessLayer.Chart_GetStoreByKeyChartScheduler(paging, KeyChartScheduler);
		}

		/// <summary>
		/// Gets a json store for the business entity Chart KPI.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="KeyClassificationItemKPI">The key classification item KPI.</param>
		/// <returns>
		/// A json store.
		/// </returns>
		public JsonStore<Chart> Chart_GetStoreKPI(PagingParameter paging, PagingLocation location, string KeyClassificationItemKPI) {
			return m_EnergyBusinessLayer.Chart_GetStoreKPI(paging, location, KeyClassificationItemKPI);
		}

		/// <summary>
		/// Returns the stream of the excel file from a chart.
		/// </summary>
		/// <param name="Key">The key of the Chart.</param>
		/// <returns></returns>
		public Stream Chart_GetStreamExcel(string Key) {
			StreamResult result = m_EnergyBusinessLayer.Chart_GetStreamExcel(Key);
			this.PrepareFileAttachment(result);
			return result.ContentStream;
		}

		/// <summary>
		/// Exports the Chart as an Excel File with a long running operation.
		/// </summary>
		/// <param name="operationId">The id of the operation.</param>
		/// <param name="Key">The key of the Chart.</param>
		public void Chart_GetStreamExcelBegin(Guid operationId, string Key) {
			m_EnergyBusinessLayer.Chart_GetStreamExcelBegin(operationId, Key);
		}

		/// <summary>
		/// Returns the stream of the image (without map) from a chart.
		/// </summary>
		/// <param name="Key">The key of the Chart.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <returns></returns>
		public Stream Chart_GetStreamImage(string Key, int width, int height) {
			var stream = m_EnergyBusinessLayer.Chart_GetStreamImage(Key, width, height);
			WebOperationContext.Current.OutgoingResponse.ContentType = stream.MimeType;
			return stream.ContentStream;
		}

		/// <summary>
		/// Returns the stream of the image (with map) from a chart stored in the cache.
		/// </summary>
		/// <param name="guid">The guid of the image.</param>
		/// <returns></returns>
		public Stream Chart_GetStreamImageFromCache(string guid) {
			WebOperationContext.Current.OutgoingResponse.ContentType = MimeType.Png;
			return m_EnergyBusinessLayer.Chart_GetStreamImageFromCache(guid);
		}

		/// <summary>
		/// Returns the chart as a stream of javascript code (by default using hightcharts.com).
		/// </summary>
		/// <param name="Key">The key of the Chart.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <returns></returns>
		public Stream Chart_GetStreamJavascript(string Key, int width, int height) {
			var stream = m_EnergyBusinessLayer.Chart_GetStreamJavascript(Key, width, height);
			WebOperationContext.Current.OutgoingResponse.ContentType = stream.MimeType;
			return stream.ContentStream;
		}

		/// <summary>
		/// Returns the chart as a stream of html.
		/// </summary>
		/// <param name="Key">The key of the Chart.</param>
		/// <returns></returns>
		public Stream Chart_GetStreamHtml(string Key) {
			var stream = m_EnergyBusinessLayer.Chart_GetStreamHtml(Key);
			WebOperationContext.Current.OutgoingResponse.ContentType = stream.MimeType;
			return stream.ContentStream;
		}

		/// <summary>
		/// Returns the stream of the excel file from a chart.
		/// </summary>
		/// <param name="Key">The key of the Chart.</param>
		/// <returns></returns>
		public Stream Chart_GetStreamPdf(string Key) {
			StreamResult result = m_EnergyBusinessLayer.Chart_GetStreamPdf(Key);
			this.PrepareFileAttachment(result);
			return result.ContentStream;
		}

		/// <summary>
		/// Exports the Chart as an Pdf File with a long running operation.
		/// </summary>
		/// <param name="operationId">The id of the operation.</param>
		/// <param name="Key">The key of the Chart.</param>
		public void Chart_GetStreamPdfBegin(Guid operationId, string Key) {
			m_EnergyBusinessLayer.Chart_GetStreamPdfBegin(operationId, Key);
		}

		/// <summary>
		/// Returns the stream of the image of a Micro chart.
		/// </summary>
		/// <param name="text">The text.</param>
		/// <param name="value">The value.</param>
		/// <param name="targetValue">The target value.</param>
		/// <param name="minValue">The min value.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <returns></returns>
		public Stream Chart_MicroChartGetStreamImage(string text, double value, double targetValue, string minValue, int width, int height) {
			double? min = null;
			if (string.IsNullOrEmpty(minValue) == false) {
				double m;
				double.TryParse(minValue, out m);
				min = m;
			}
			var stream = m_EnergyBusinessLayer.Chart_MicroChartGetStreamImage(text, value, targetValue, min, width, height);
			WebOperationContext.Current.OutgoingResponse.ContentType = stream.MimeType;
			return stream.ContentStream;
		}

		/// <summary>
		/// Preload in cache all the Charts that are used inside a PortalWindow.
		/// </summary>
		/// <param name="clearCache">if set to <c>true</c> [clear cache].</param>
		/// <param name="KeyPortalTab">The key portal tab.</param>
		/// <param name="KeyPortalWindow">The key portal window.</param>
		public PortalTab Chart_PreLoadAll(bool clearCache = false, string KeyPortalTab = null, string KeyPortalWindow = null) {
			return m_EnergyBusinessLayer.Chart_PreLoadAll(clearCache, null, KeyPortalTab, KeyPortalWindow);
		}

		/// <summary>
		/// Preload in cache all the Charts that are used inside a PortalWindow using a long running operation.
		/// </summary>
		/// <param name="operationId">The operation id.</param>
		/// <param name="clearCache">if set to <c>true</c> [clear cache].</param>
		/// <param name="KeyPortalTab">The key portal tab.</param>
		/// <param name="KeyPortalWindow">The key portal window.</param>
		public void Chart_PreLoadAllBegin(Guid operationId, bool clearCache = false, string KeyPortalTab = null, string KeyPortalWindow = null) {
			m_EnergyBusinessLayer.Chart_PreLoadAllBegin(operationId, clearCache, KeyPortalTab, KeyPortalWindow);
		}

		/// <summary>
		/// Saves a crud store for the business entity Chart.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<Chart> Chart_SaveStore(CrudStore<Chart> store) {
			return m_EnergyBusinessLayer.Chart_SaveStore(store);
		}

		/// <summary>
		/// Deletes an existing business entity ChartAxis.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool ChartAxis_Delete(ChartAxis item) {
			return m_EnergyBusinessLayer.ChartAxis_Delete(item);
		}

		/// <summary>
		/// Creates a new business entity ChartAxis and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse ChartAxis_FormCreate(ChartAxis item) {
			return m_EnergyBusinessLayer.ChartAxis_FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity ChartAxis.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse ChartAxis_FormLoad(string Key) {
			return m_EnergyBusinessLayer.ChartAxis_FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity ChartAxis and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse ChartAxis_FormUpdate(ChartAxis item) {
			return m_EnergyBusinessLayer.ChartAxis_FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse ChartAxis_FormUpdateBatch(string[] keys, ChartAxis item) {
			return m_EnergyBusinessLayer.ChartAxis_FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity ChartAxis. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		public List<ChartAxis> ChartAxis_GetAll(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.ChartAxis_GetAll(paging, location);
		}

		/// <summary>
		/// Gets a specific item for the business entity ChartAxis.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public ChartAxis ChartAxis_GetItem(string Key) {
			return m_EnergyBusinessLayer.ChartAxis_GetItem(Key);
		}

		/// <summary>
		/// Gets a json store for the business entity ChartAxis.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<ChartAxis> ChartAxis_GetStore(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.ChartAxis_GetStore(paging, location);
		}

		/// <summary>
		/// Gets a json store of chartaxis for a specific chart.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyChart">The Key of the chart.</param>
		/// <returns>A json store.</returns>
		public JsonStore<ChartAxis> ChartAxis_GetStoreByKeyChart(PagingParameter paging, string KeyChart) {
			return m_EnergyBusinessLayer.ChartAxis_GetStoreByKeyChart(paging, KeyChart);
		}

		/// <summary>
		/// Saves a crud store for the business entity ChartAxis.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<ChartAxis> ChartAxis_SaveStore(CrudStore<ChartAxis> store) {
			return m_EnergyBusinessLayer.ChartAxis_SaveStore(store);
		}

		/// <summary>
		/// Add a new selection to Calendar View.
		/// </summary>
		/// <param name="KeyChart">The key chart.</param>
		/// <param name="startDate">The start date.</param>
		/// <param name="endDate">The end date.</param>
		public void ChartCalendarViewSelection_Add(string KeyChart, string startDate, string endDate) {
			m_EnergyBusinessLayer.ChartCalendarViewSelection_Add(KeyChart, startDate, endDate);
		}

		/// <summary>
		/// Delete a Chart Calendar View Selection.
		/// </summary>
		/// <param name="KeyChartCalendarViewSelection">The key chart calendar view selection.</param>
		public bool ChartCalendarViewSelection_Delete(string KeyChartCalendarViewSelection) {
			return m_EnergyBusinessLayer.ChartCalendarViewSelection_Delete(KeyChartCalendarViewSelection);
		}

		/// <summary>
		/// Reset the Chart Calendar View Selection.
		/// </summary>
		/// <param name="KeyChart">The key chart.</param>
		public void ChartCalendarViewSelection_Reset(string KeyChart) {
			m_EnergyBusinessLayer.ChartCalendarViewSelection_Reset(KeyChart);
		}

		/// <summary>
		/// Deletes an existing business entity ChartCustomGridCell.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool ChartCustomGridCell_Delete(ChartCustomGridCell item) {
			return m_EnergyBusinessLayer.ChartCustomGridCell_Delete(item);
		}

		/// <summary>
		/// Creates a new business entity ChartCustomGridCell and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse ChartCustomGridCell_FormCreate(ChartCustomGridCell item) {
			return m_EnergyBusinessLayer.ChartCustomGridCell_FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity ChartCustomGridCell.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse ChartCustomGridCell_FormLoad(string Key) {
			return m_EnergyBusinessLayer.ChartCustomGridCell_FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity ChartCustomGridCell and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse ChartCustomGridCell_FormUpdate(ChartCustomGridCell item) {
			return m_EnergyBusinessLayer.ChartCustomGridCell_FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse ChartCustomGridCell_FormUpdateBatch(string[] keys, ChartCustomGridCell item) {
			return m_EnergyBusinessLayer.ChartCustomGridCell_FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity ChartCustomGridCell. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		public List<ChartCustomGridCell> ChartCustomGridCell_GetAll(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.ChartCustomGridCell_GetAll(paging, location);
		}

		/// <summary>
		/// Gets a specific item for the business entity ChartCustomGridCell.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public ChartCustomGridCell ChartCustomGridCell_GetItem(string Key) {
			return m_EnergyBusinessLayer.ChartCustomGridCell_GetItem(Key);
		}

		/// <summary>
		/// Gets a list of ChartCustomGridCell for a specific chart.
		/// </summary>
		/// <param name="KeyChart">The key of the chart.</param>
		/// <param name="populateValues">if set to <c>true</c> we fetch the chart with data and we populate the values.</param>
		/// <returns></returns>
		public List<ChartCustomGridCell> ChartCustomGridCell_GetListByKeyChart(string KeyChart, bool populateValues) {
			var retVal = m_EnergyBusinessLayer.ChartCustomGridCell_GetListByKeyChart(KeyChart, populateValues);
			return retVal;

		}

		/// <summary>
		/// Gets a json store for the business entity ChartCustomGridCell.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<ChartCustomGridCell> ChartCustomGridCell_GetStore(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.ChartCustomGridCell_GetStore(paging, location);
		}

		/// <summary>
		/// Save a list of ChartCustomGridCell for a specific chart.
		/// </summary>
		/// <param name="cells">The cells.</param>
		/// <returns></returns>
		public bool ChartCustomGridCell_SaveList(List<ChartCustomGridCell> cells) {
			return m_EnergyBusinessLayer.ChartCustomGridCell_SaveList(cells);
		}

		/// <summary>
		/// Saves a crud store for the business entity ChartCustomGridCell.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<ChartCustomGridCell> ChartCustomGridCell_SaveStore(CrudStore<ChartCustomGridCell> store) {
			return m_EnergyBusinessLayer.ChartCustomGridCell_SaveStore(store);
		}

		/// <summary>
		/// Deletes an existing business entity ChartDrillDown.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool ChartDrillDown_Delete(ChartDrillDown item) {
			return m_EnergyBusinessLayer.ChartDrillDown_Delete(item);
		}

		/// <summary>
		/// Creates a new business entity ChartDrillDown and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse ChartDrillDown_FormCreate(ChartDrillDown item) {
			return m_EnergyBusinessLayer.ChartDrillDown_FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity ChartDrillDown.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse ChartDrillDown_FormLoad(string Key) {
			return m_EnergyBusinessLayer.ChartDrillDown_FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity ChartDrillDown and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse ChartDrillDown_FormUpdate(ChartDrillDown item) {
			return m_EnergyBusinessLayer.ChartDrillDown_FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse ChartDrillDown_FormUpdateBatch(string[] keys, ChartDrillDown item) {
			return m_EnergyBusinessLayer.ChartDrillDown_FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity ChartDrillDown. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		public List<ChartDrillDown> ChartDrillDown_GetAll(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.ChartDrillDown_GetAll(paging, location);
		}

		/// <summary>
		/// Gets a specific item for the business entity ChartDrillDown.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public ChartDrillDown ChartDrillDown_GetItem(string Key) {
			return m_EnergyBusinessLayer.ChartDrillDown_GetItem(Key);
		}

		/// <summary>
		/// Gets a json store for the business entity ChartDrillDown.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<ChartDrillDown> ChartDrillDown_GetStore(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.ChartDrillDown_GetStore(paging, location);
		}

		/// <summary>
		/// Reset the Chart drill down.
		/// </summary>
		/// <param name="KeyChart">The key chart.</param>
		public void ChartDrillDown_Reset(string KeyChart) {
			m_EnergyBusinessLayer.ChartDrillDown_Reset(KeyChart);
		}

		/// <summary>
		/// Saves a crud store for the business entity ChartDrillDown.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<ChartDrillDown> ChartDrillDown_SaveStore(CrudStore<ChartDrillDown> store) {
			return m_EnergyBusinessLayer.ChartDrillDown_SaveStore(store);
		}

		/// <summary>
		/// Update (or Create) the ChartDrillDown after a user click on the Chart.
		/// </summary>
		/// <param name="KeyChart">The key chart.</param>
		/// <param name="XDateTime">The X date time.</param>
		/// <param name="KeyLocation">The key location.</param>
		/// <param name="KeyClassificationItem">The key classification item.</param>
		/// <param name="Type">Type of the drill down.</param>
		/// <param name="KeyPortalWindow">The key portal window.</param>
		/// <param name="KeyPortalTab">The key portal tab.</param>
		/// <returns></returns>
		public List<ChartDrillDown> ChartDrillDown_Update(string KeyChart, string XDateTime, string KeyLocation, string KeyClassificationItem, DrillDownType Type, string KeyPortalWindow, string KeyPortalTab) {
			return m_EnergyBusinessLayer.ChartDrillDown_Update(KeyChart, XDateTime, KeyLocation, KeyClassificationItem, Type, KeyPortalWindow, KeyPortalTab);
		}

		/// <summary>
		/// Deletes an existing business entity ChartHistoricalAnalysis.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool ChartHistoricalAnalysis_Delete(ChartHistoricalAnalysis item) {
			return m_EnergyBusinessLayer.ChartHistoricalAnalysis_Delete(item);
		}

		/// <summary>
		/// Creates a new business entity ChartHistoricalAnalysis and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse ChartHistoricalAnalysis_FormCreate(ChartHistoricalAnalysis item) {
			return m_EnergyBusinessLayer.ChartHistoricalAnalysis_FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity ChartHistoricalAnalysis.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse ChartHistoricalAnalysis_FormLoad(string Key) {
			return m_EnergyBusinessLayer.ChartHistoricalAnalysis_FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity ChartHistoricalAnalysis and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse ChartHistoricalAnalysis_FormUpdate(ChartHistoricalAnalysis item) {
			return m_EnergyBusinessLayer.ChartHistoricalAnalysis_FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse ChartHistoricalAnalysis_FormUpdateBatch(string[] keys, ChartHistoricalAnalysis item) {
			return m_EnergyBusinessLayer.ChartHistoricalAnalysis_FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity ChartHistoricalAnalysis. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		public List<ChartHistoricalAnalysis> ChartHistoricalAnalysis_GetAll(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.ChartHistoricalAnalysis_GetAll(paging, location);
		}

		/// <summary>
		/// Gets a specific item for the business entity ChartHistoricalAnalysis.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public ChartHistoricalAnalysis ChartHistoricalAnalysis_GetItem(string Key) {
			return m_EnergyBusinessLayer.ChartHistoricalAnalysis_GetItem(Key);
		}

		/// <summary>
		/// Gets a json store for the business entity ChartHistoricalAnalysis.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<ChartHistoricalAnalysis> ChartHistoricalAnalysis_GetStore(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.ChartHistoricalAnalysis_GetStore(paging, location);
		}

		/// <summary>
		/// Gets a store of ChartHistoricalAnalysis for a specific chart.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyChart">The key of the chart.</param>
		/// <returns></returns>
		public JsonStore<ChartHistoricalAnalysis> ChartHistoricalAnalysis_GetStoreByKeyChart(PagingParameter paging, string KeyChart) {
			return m_EnergyBusinessLayer.ChartHistoricalAnalysis_GetStoreByKeyChart(paging, KeyChart);
		}

		/// <summary>
		/// Saves a crud store for the business entity ChartHistoricalAnalysis.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<ChartHistoricalAnalysis> ChartHistoricalAnalysis_SaveStore(CrudStore<ChartHistoricalAnalysis> store) {
			return m_EnergyBusinessLayer.ChartHistoricalAnalysis_SaveStore(store);
		}

		/// <summary>
		/// Deletes an existing business entity ChartMarker.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool ChartMarker_Delete(ChartMarker item) {
			return m_EnergyBusinessLayer.ChartMarker_Delete(item);
		}

		/// <summary>
		/// Creates a new business entity ChartMarker and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse ChartMarker_FormCreate(ChartMarker item) {
			return m_EnergyBusinessLayer.ChartMarker_FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity ChartMarker.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse ChartMarker_FormLoad(string Key) {
			return m_EnergyBusinessLayer.ChartMarker_FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity ChartMarker and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse ChartMarker_FormUpdate(ChartMarker item) {
			return m_EnergyBusinessLayer.ChartMarker_FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse ChartMarker_FormUpdateBatch(string[] keys, ChartMarker item) {
			return m_EnergyBusinessLayer.ChartMarker_FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity ChartMarker. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		public List<ChartMarker> ChartMarker_GetAll(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.ChartMarker_GetAll(paging, location);
		}

		/// <summary>
		/// Gets a specific item for the business entity ChartMarker.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public ChartMarker ChartMarker_GetItem(string Key) {
			return m_EnergyBusinessLayer.ChartMarker_GetItem(Key);
		}

		/// <summary>
		/// Gets a json store for the business entity ChartMarker.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<ChartMarker> ChartMarker_GetStore(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.ChartMarker_GetStore(paging, location);
		}

		/// <summary>
		/// Gets a json store of ChartMarker for a specific chart.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyChart">The Key of the chart.</param>
		/// <returns>A json store.</returns>
		public JsonStore<ChartMarker> ChartMarker_GetStoreByKeyChart(PagingParameter paging, string KeyChart) {
			return m_EnergyBusinessLayer.ChartMarker_GetStoreByKeyChart(paging, KeyChart);
		}

		/// <summary>
		/// Saves a crud store for the business entity ChartMarker.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<ChartMarker> ChartMarker_SaveStore(CrudStore<ChartMarker> store) {
			return m_EnergyBusinessLayer.ChartMarker_SaveStore(store);
		}

		/// <summary>
		/// Gets the chart tree.
		/// </summary>
		/// <param name="KeyChart">The key chart.</param>
		/// <returns></returns>
		public List<TreeNode> ChartModel_GetMenuTree(string KeyChart) {
			var retVal = m_EnergyBusinessLayer.ChartModel_GetMenuTree(KeyChart);
			return retVal;
		}

		/// <summary>
		/// Deletes an existing business entity ChartPsetAttributeHistorical.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool ChartPsetAttributeHistorical_Delete(ChartPsetAttributeHistorical item) {
			return m_EnergyBusinessLayer.ChartPsetAttributeHistorical_Delete(item);
		}

		/// <summary>
		/// Creates a new business entity ChartPsetAttributeHistorical and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse ChartPsetAttributeHistorical_FormCreate(ChartPsetAttributeHistorical item) {
			return m_EnergyBusinessLayer.ChartPsetAttributeHistorical_FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity ChartPsetAttributeHistorical.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse ChartPsetAttributeHistorical_FormLoad(string Key) {
			return m_EnergyBusinessLayer.ChartPsetAttributeHistorical_FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity ChartPsetAttributeHistorical and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse ChartPsetAttributeHistorical_FormUpdate(ChartPsetAttributeHistorical item) {
			return m_EnergyBusinessLayer.ChartPsetAttributeHistorical_FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse ChartPsetAttributeHistorical_FormUpdateBatch(string[] keys, ChartPsetAttributeHistorical item) {
			return m_EnergyBusinessLayer.ChartPsetAttributeHistorical_FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity ChartPsetAttributeHistorical. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		public List<ChartPsetAttributeHistorical> ChartPsetAttributeHistorical_GetAll(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.ChartPsetAttributeHistorical_GetAll(paging, location);
		}

		/// <summary>
		/// Gets a specific item for the business entity ChartPsetAttributeHistorical.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public ChartPsetAttributeHistorical ChartPsetAttributeHistorical_GetItem(string Key) {
			return m_EnergyBusinessLayer.ChartPsetAttributeHistorical_GetItem(Key);
		}

		/// <summary>
		/// Gets a json store for the business entity ChartPsetAttributeHistorical.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<ChartPsetAttributeHistorical> ChartPsetAttributeHistorical_GetStore(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.ChartPsetAttributeHistorical_GetStore(paging, location);
		}

		/// <summary>
		/// Gets a store of ChartPsetAttributeHistorical for a specific chart.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyChart">The key of the chart.</param>
		/// <returns></returns>
		public JsonStore<ChartPsetAttributeHistorical> ChartPsetAttributeHistorical_GetStoreByKeyChart(PagingParameter paging, string KeyChart) {
			return m_EnergyBusinessLayer.ChartPsetAttributeHistorical_GetStoreByKeyChart(paging, KeyChart);
		}

		/// <summary>
		/// Saves a crud store for the business entity ChartPsetAttributeHistorical.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<ChartPsetAttributeHistorical> ChartPsetAttributeHistorical_SaveStore(CrudStore<ChartPsetAttributeHistorical> store) {
			return m_EnergyBusinessLayer.ChartPsetAttributeHistorical_SaveStore(store);
		}

		/// <summary>
		/// Deletes an existing business entity ChartScheduler.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool ChartScheduler_Delete(ChartScheduler item) {
			return m_EnergyBusinessLayer.ChartScheduler_Delete(item);
		}

		/// <summary>
		/// Creates a new business entity ChartScheduler and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <param name="charts">The charts.</param>
		/// <param name="portalwindows">The portalwindows.</param>
		/// <returns></returns>
		public FormResponse ChartScheduler_FormCreate(ChartScheduler item, CrudStore<Chart> charts, CrudStore<PortalWindow> portalwindows) {
			return m_EnergyBusinessLayer.ChartScheduler_FormCreate(item, charts, portalwindows);
		}

		/// <summary>
		/// Loads a specific item for the business entity ChartScheduler.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse ChartScheduler_FormLoad(string Key) {
			return m_EnergyBusinessLayer.ChartScheduler_FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity ChartScheduler and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <param name="charts">The charts.</param>
		/// <param name="portalwindows">The portalwindows.</param>
		/// <returns></returns>
		public FormResponse ChartScheduler_FormUpdate(ChartScheduler item, CrudStore<Chart> charts, CrudStore<PortalWindow> portalwindows) {
			return m_EnergyBusinessLayer.ChartScheduler_FormUpdate(item, charts, portalwindows);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse ChartScheduler_FormUpdateBatch(string[] keys, ChartScheduler item) {
			return m_EnergyBusinessLayer.ChartScheduler_FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Generates the Scheduled Reports and sends them via email.
		/// </summary>
		/// <param name="KeyChartScheduler">the ChartScheduler Key.</param>
		/// <returns></returns>
		public void ChartScheduler_Generate(string KeyChartScheduler) {
			m_EnergyBusinessLayer.ChartScheduler_Generate(KeyChartScheduler);
		}

		/// <summary>
		/// Generates the Scheduled Reports and sends them via email with a long running operation.
		/// </summary>
		/// <param name="operationId">The operation id.</param>
		/// <param name="KeyChartScheduler">the ChartScheduler Key.</param>
		public void ChartScheduler_GenerateBegin(Guid operationId, string KeyChartScheduler) {
			m_EnergyBusinessLayer.ChartScheduler_GenerateBegin(operationId, KeyChartScheduler);
		}

		/// <summary>
		/// Gets a list for the business entity ChartScheduler. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		public List<ChartScheduler> ChartScheduler_GetAll(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.ChartScheduler_GetAll(paging, location);
		}

		/// <summary>
		/// Gets a specific item for the business entity ChartScheduler.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public ChartScheduler ChartScheduler_GetItem(string Key) {
			return m_EnergyBusinessLayer.ChartScheduler_GetItem(Key);
		}

		/// <summary>
		/// Gets a json store for the business entity ChartScheduler.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<ChartScheduler> ChartScheduler_GetStore(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.ChartScheduler_GetStore(paging, location);
		}

		/// <summary>
		/// Saves a crud store for the business entity ChartScheduler.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<ChartScheduler> ChartScheduler_SaveStore(CrudStore<ChartScheduler> store) {
			return m_EnergyBusinessLayer.ChartScheduler_SaveStore(store);
		}

		/// <summary>
		/// Get the tree of DataAcquisition instances and endpoints.
		/// </summary>
		/// <param name="entity">The parent entity.</param>
		/// <param name="entityContainer">The entity container.</param>
		/// <returns></returns>
		public List<TreeNode> DataAcquisition_GetTree(DataAcquisitionItem entity, DataAcquisitionContainer entityContainer) {
			return m_EnergyBusinessLayer.DataAcquisition_GetTree(entity, entityContainer);
		}

		/// <summary>
		/// Get the stream image of	a data acquisition endpoint.
		/// </summary>
		/// <param name="EndpointType">Type of the endpoint.</param>
		/// <param name="KeyEndpoint">The key endpoint.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <returns></returns>
		public ImageMap DataAcquisitionEndpoint_BuildImage(string EndpointType, string KeyEndpoint, int width, int height) {
			return m_EnergyBusinessLayer.DataAcquisitionEndpoint_BuildImage(EndpointType, KeyEndpoint, width, height);
		}

		/// <summary>
		/// Gets a json store for the business entity DataAcquisitionEndpoint.
		/// paging is done in memory because of the multiples brokers.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <returns>
		/// A json store.
		/// </returns>
		public JsonStore<DataAcquisitionEndpoint> DataAcquisitionEndpoint_GetStore(PagingParameter paging) {
			return m_EnergyBusinessLayer.DataAcquisitionEndpoint_GetStore(paging);
		}


		/// <summary>
		/// Gets ths store of datapoint from a chart.
		/// </summary>
		/// <param name="KeyChart">The KeyChart.</param>
		/// <param name="paging">The paging parameter.</param>
		/// <returns></returns>
		public JsonStore<DataPoint> DataPoint_GetStoreFromChart(string KeyChart, PagingParameter paging) {
			return m_EnergyBusinessLayer.DataPoint_GetStoreFromChart(KeyChart, paging);
		}


		/// <summary>
		/// Deletes an existing business entity DataSerie.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool DataSerie_Delete(DataSerie item) {
			return m_EnergyBusinessLayer.DataSerie_Delete(item);
		}

		/// <summary>
		/// Creates a new business entity DataSerie and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse DataSerie_FormCreate(DataSerie item) {
			return m_EnergyBusinessLayer.DataSerie_FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity DataSerie.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse DataSerie_FormLoad(string Key) {
			return m_EnergyBusinessLayer.DataSerie_FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity DataSerie and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <param name="ratios">The ratios.</param>
		/// <param name="colorelements">The colorelements.</param>
		/// <returns></returns>
		public FormResponse DataSerie_FormUpdate(DataSerie item, CrudStore<DataSeriePsetRatio> ratios, CrudStore<DataSerieColorElement> colorelements) {
			return m_EnergyBusinessLayer.DataSerie_FormUpdate(item, ratios, colorelements);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse DataSerie_FormUpdateBatch(string[] keys, DataSerie item) {
			return m_EnergyBusinessLayer.DataSerie_FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity DataSerie. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		public List<DataSerie> DataSerie_GetAll(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.DataSerie_GetAll(paging, location);
		}

		/// <summary>
		/// Gets a specific item for the business entity DataSerie.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public DataSerie DataSerie_GetItem(string Key) {
			return m_EnergyBusinessLayer.DataSerie_GetItem(Key);
		}

		/// <summary>
		/// Gets a json store for the business entity DataSerie.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<DataSerie> DataSerie_GetStore(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.DataSerie_GetStore(paging, location);
		}

		/// <summary>
		/// Gets a json store of dataserie for a specific chart.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyChart">The Key of the chart.</param>
		/// <returns>A json store.</returns>
		public JsonStore<DataSerie> DataSerie_GetStoreByKeyChart(PagingParameter paging, string KeyChart) {
			return m_EnergyBusinessLayer.DataSerie_GetStoreByKeyChart(paging, KeyChart);
		}

		/// <summary>
		/// Saves a crud store for the business entity DataSerie.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<DataSerie> DataSerie_SaveStore(CrudStore<DataSerie> store) {
			return m_EnergyBusinessLayer.DataSerie_SaveStore(store);
		}

		/// <summary>
		/// Toggle the visibility of a specific dataserie (and creates it of its not instanciated yet).
		/// </summary>
		/// <param name="KeyChart">The key chart.</param>
		/// <param name="KeyDataSerie">The key data serie.</param>
		/// <param name="localId">The local id.</param>
		/// <param name="Visible">if set to <c>true</c> [visible].</param>
		/// <param name="serieName">Name of the serie as optional parameter. If not suplied it will default to the localId</param>
		/// <returns></returns>
		public DataSerie DataSerie_ToggleVisibility(string KeyChart, string KeyDataSerie, string localId, bool Visible, string serieName = null) {
			return m_EnergyBusinessLayer.DataSerie_ToggleVisibility(KeyChart, KeyDataSerie, localId, Visible, serieName);
		}

		/// <summary>
		/// Toggle the visibility of a specific dataserie (and creates it of its not instanciated yet).
		/// </summary>
		/// <param name="KeyChart">The key chart.</param>
		/// <param name="KeyDataSerie">The key data serie.</param>
		/// <param name="localId">The local id.</param>
		/// <param name="type">The type.</param>
		/// <param name="serieName">Name of the serie as optional parameter. If not suplied it will default to the localId</param>
		/// <returns></returns>
		public DataSerie DataSerie_TypeUpdate(string KeyChart, string KeyDataSerie, string localId, DataSerieType type, string serieName = null) {
			return m_EnergyBusinessLayer.DataSerie_TypeUpdate(KeyChart, KeyDataSerie, localId, type, serieName);
		}

		/// <summary>
		/// Assign a secondary axis to a specific dataserie (and creates it of its not instanciated yet).
		/// </summary>
		/// <param name="KeyChart">The key chart.</param>
		/// <param name="KeyDataSerie">The key data serie.</param>
		/// <param name="localId">The local id.</param>
		/// <param name="serieName">Name of the serie as optional parameter. If not suplied it will default to the localId</param>
		/// <returns></returns>
		public DataSerie DataSerie_UseSecondaryAxis(string KeyChart, string KeyDataSerie, string localId, string serieName = null) {
			return m_EnergyBusinessLayer.DataSerie_UseSecondaryAxis(KeyChart, KeyDataSerie, localId, serieName);
		}

		/// <summary>
		/// Deletes an existing business entity DataSerieColorElement.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool DataSerieColorElement_Delete(DataSerieColorElement item) {
			return m_EnergyBusinessLayer.DataSerieColorElement_Delete(item);
		}

		/// <summary>
		/// Creates a new business entity DataSerieColorElement and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse DataSerieColorElement_FormCreate(DataSerieColorElement item) {
			return m_EnergyBusinessLayer.DataSerieColorElement_FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity DataSerieColorElement.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse DataSerieColorElement_FormLoad(string Key) {
			return m_EnergyBusinessLayer.DataSerieColorElement_FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity DataSerieColorElement and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse DataSerieColorElement_FormUpdate(DataSerieColorElement item) {
			return m_EnergyBusinessLayer.DataSerieColorElement_FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse DataSerieColorElement_FormUpdateBatch(string[] keys, DataSerieColorElement item) {
			return m_EnergyBusinessLayer.DataSerieColorElement_FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity DataSerieColorElement. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		public List<DataSerieColorElement> DataSerieColorElement_GetAll(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.DataSerieColorElement_GetAll(paging, location);
		}

		/// <summary>
		/// Gets a specific item for the business entity DataSerieColorElement.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public DataSerieColorElement DataSerieColorElement_GetItem(string Key) {
			return m_EnergyBusinessLayer.DataSerieColorElement_GetItem(Key);
		}

		/// <summary>
		/// Gets a json store for the business entity DataSerieColorElement.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<DataSerieColorElement> DataSerieColorElement_GetStore(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.DataSerieColorElement_GetStore(paging, location);
		}

		/// <summary>
		/// Gets a store of DataSerieColorElement for a specific DataSerie.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyDataSerie">The key of the dataserie.</param>
		/// <returns></returns>
		public JsonStore<DataSerieColorElement> DataSerieColorElement_GetStoreByKeyDataSerie(PagingParameter paging, string KeyDataSerie) {
			return m_EnergyBusinessLayer.DataSerieColorElement_GetStoreByKeyDataSerie(paging, KeyDataSerie);
		}

		/// <summary>
		/// Saves a crud store for the business entity DataSerieColorElement.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<DataSerieColorElement> DataSerieColorElement_SaveStore(CrudStore<DataSerieColorElement> store) {
			return m_EnergyBusinessLayer.DataSerieColorElement_SaveStore(store);
		}

		/// <summary>
		/// Deletes an existing business entity DataSeriePsetRatio.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool DataSeriePsetRatio_Delete(DataSeriePsetRatio item) {
			return m_EnergyBusinessLayer.DataSeriePsetRatio_Delete(item);
		}

		/// <summary>
		/// Creates a new business entity DataSeriePsetRatio and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse DataSeriePsetRatio_FormCreate(DataSeriePsetRatio item) {
			return m_EnergyBusinessLayer.DataSeriePsetRatio_FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity DataSeriePsetRatio.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse DataSeriePsetRatio_FormLoad(string Key) {
			return m_EnergyBusinessLayer.DataSeriePsetRatio_FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity DataSeriePsetRatio and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse DataSeriePsetRatio_FormUpdate(DataSeriePsetRatio item) {
			return m_EnergyBusinessLayer.DataSeriePsetRatio_FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse DataSeriePsetRatio_FormUpdateBatch(string[] keys, DataSeriePsetRatio item) {
			return m_EnergyBusinessLayer.DataSeriePsetRatio_FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity DataSeriePsetRatio. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		public List<DataSeriePsetRatio> DataSeriePsetRatio_GetAll(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.DataSeriePsetRatio_GetAll(paging, location);
		}

		/// <summary>
		/// Gets a specific item for the business entity DataSeriePsetRatio.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public DataSeriePsetRatio DataSeriePsetRatio_GetItem(string Key) {
			return m_EnergyBusinessLayer.DataSeriePsetRatio_GetItem(Key);
		}

		/// <summary>
		/// Gets a json store for the business entity DataSeriePsetRatio.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<DataSeriePsetRatio> DataSeriePsetRatio_GetStore(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.DataSeriePsetRatio_GetStore(paging, location);
		}

		/// <summary>
		/// Gets a store of DataSeriePsetRatio for a specific DataSerie.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyDataSerie">The key of the dataserie.</param>
		/// <returns></returns>
		public JsonStore<DataSeriePsetRatio> DataSeriePsetRatio_GetStoreByKeyDataSerie(PagingParameter paging, string KeyDataSerie) {
			return m_EnergyBusinessLayer.DataSeriePsetRatio_GetStoreByKeyDataSerie(paging, KeyDataSerie);
		}

		/// <summary>
		/// Saves a crud store for the business entity DataSeriePsetRatio.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<DataSeriePsetRatio> DataSeriePsetRatio_SaveStore(CrudStore<DataSeriePsetRatio> store) {
			return m_EnergyBusinessLayer.DataSeriePsetRatio_SaveStore(store);
		}

		/// <summary>
		/// Deletes an existing business entity DrawingCanvas.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool DrawingCanvas_Delete(DrawingCanvas item) {
			return m_EnergyBusinessLayer.DrawingCanvas_Delete(item);
		}

		/// <summary>
		/// Creates a new business entity DrawingCanvas and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse DrawingCanvas_FormCreate(DrawingCanvas item) {
			return m_EnergyBusinessLayer.DrawingCanvas_FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity DrawingCanvas.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse DrawingCanvas_FormLoad(string Key) {
			return m_EnergyBusinessLayer.DrawingCanvas_FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity DrawingCanvas and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <param name="charts">The charts.</param>
		/// <param name="images">The images.</param>
		/// <returns></returns>
		public FormResponse DrawingCanvas_FormUpdate(DrawingCanvas item, CrudStore<DrawingCanvasChart> charts, CrudStore<DrawingCanvasImage> images) {
			return m_EnergyBusinessLayer.DrawingCanvas_FormUpdate(item, charts, images);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse DrawingCanvas_FormUpdateBatch(string[] keys, DrawingCanvas item) {
			return m_EnergyBusinessLayer.DrawingCanvas_FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity DrawingCanvas. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		public List<DrawingCanvas> DrawingCanvas_GetAll(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.DrawingCanvas_GetAll(paging, location);
		}

		/// <summary>
		/// Gets a specific item for the business entity DrawingCanvas.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public DrawingCanvas DrawingCanvas_GetItem(string Key) {
			return m_EnergyBusinessLayer.DrawingCanvas_GetItem(Key);
		}

		/// <summary>
		/// Gets a json store for the business entity DrawingCanvas.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<DrawingCanvas> DrawingCanvas_GetStore(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.DrawingCanvas_GetStore(paging, location);
		}

		/// <summary>
		/// Saves a crud store for the business entity DrawingCanvas.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<DrawingCanvas> DrawingCanvas_SaveStore(CrudStore<DrawingCanvas> store) {
			return m_EnergyBusinessLayer.DrawingCanvas_SaveStore(store);
		}

		/// <summary>
		/// Deletes an existing business entity DrawingCanvasChart.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool DrawingCanvasChart_Delete(DrawingCanvasChart item) {
			return m_EnergyBusinessLayer.DrawingCanvasChart_Delete(item);
		}

		/// <summary>
		/// Creates a new business entity DrawingCanvasChart and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse DrawingCanvasChart_FormCreate(DrawingCanvasChart item) {
			return m_EnergyBusinessLayer.DrawingCanvasChart_FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity DrawingCanvasChart.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse DrawingCanvasChart_FormLoad(string Key) {
			return m_EnergyBusinessLayer.DrawingCanvasChart_FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity DrawingCanvasChart and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse DrawingCanvasChart_FormUpdate(DrawingCanvasChart item) {
			return m_EnergyBusinessLayer.DrawingCanvasChart_FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse DrawingCanvasChart_FormUpdateBatch(string[] keys, DrawingCanvasChart item) {
			return m_EnergyBusinessLayer.DrawingCanvasChart_FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity DrawingCanvasChart. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		public List<DrawingCanvasChart> DrawingCanvasChart_GetAll(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.DrawingCanvasChart_GetAll(paging, location);
		}

		/// <summary>
		/// Gets a specific item for the business entity DrawingCanvasChart.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public DrawingCanvasChart DrawingCanvasChart_GetItem(string Key) {
			return m_EnergyBusinessLayer.DrawingCanvasChart_GetItem(Key);
		}

		/// <summary>
		/// Gets a list of DrawingCanvasChart for a specific DrawingCanvas.
		/// </summary>
		/// <param name="KeyDrawingCanvas">The key of the DrawingCanvas.</param>
		/// <returns></returns>
		public List<DrawingCanvasChart> DrawingCanvasChart_GetListByKeyDrawingCanvas(string KeyDrawingCanvas) {
			return m_EnergyBusinessLayer.DrawingCanvasChart_GetListByKeyDrawingCanvas(KeyDrawingCanvas);
		}

		/// <summary>
		/// Gets a json store for the business entity DrawingCanvasChart.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<DrawingCanvasChart> DrawingCanvasChart_GetStore(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.DrawingCanvasChart_GetStore(paging, location);
		}

		/// <summary>
		/// Gets a store of DrawingCanvasChart for a specific DrawingCanvas.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyDrawingCanvas">The key of the DrawingCanvas.</param>
		/// <returns></returns>
		public JsonStore<DrawingCanvasChart> DrawingCanvasChart_GetStoreByKeyDrawingCanvas(PagingParameter paging, string KeyDrawingCanvas) {
			return m_EnergyBusinessLayer.DrawingCanvasChart_GetStoreByKeyDrawingCanvas(paging, KeyDrawingCanvas);
		}

		/// <summary>
		/// Saves a crud store for the business entity DrawingCanvasChart.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<DrawingCanvasChart> DrawingCanvasChart_SaveStore(CrudStore<DrawingCanvasChart> store) {
			return m_EnergyBusinessLayer.DrawingCanvasChart_SaveStore(store);
		}

		/// <summary>
		/// Deletes an existing business entity DrawingCanvasImage.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool DrawingCanvasImage_Delete(DrawingCanvasImage item) {
			return m_EnergyBusinessLayer.DrawingCanvasImage_Delete(item);
		}

		/// <summary>
		/// Creates a new business entity DrawingCanvasImage and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse DrawingCanvasImage_FormCreate(DrawingCanvasImage item) {
			return m_EnergyBusinessLayer.DrawingCanvasImage_FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity DrawingCanvasImage.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse DrawingCanvasImage_FormLoad(string Key) {
			return m_EnergyBusinessLayer.DrawingCanvasImage_FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity DrawingCanvasImage and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse DrawingCanvasImage_FormUpdate(DrawingCanvasImage item) {
			return m_EnergyBusinessLayer.DrawingCanvasImage_FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse DrawingCanvasImage_FormUpdateBatch(string[] keys, DrawingCanvasImage item) {
			return m_EnergyBusinessLayer.DrawingCanvasImage_FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity DrawingCanvasImage. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		public List<DrawingCanvasImage> DrawingCanvasImage_GetAll(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.DrawingCanvasImage_GetAll(paging, location);
		}

		/// <summary>
		/// Gets a specific item for the business entity DrawingCanvasImage.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public DrawingCanvasImage DrawingCanvasImage_GetItem(string Key) {
			return m_EnergyBusinessLayer.DrawingCanvasImage_GetItem(Key);
		}

		/// <summary>
		/// Gets a list of DrawingCanvasImage for a specific DrawingCanvas.
		/// </summary>
		/// <param name="KeyDrawingCanvas">The key of the DrawingCanvas.</param>
		/// <returns></returns>
		public List<DrawingCanvasImage> DrawingCanvasImage_GetListByKeyDrawingCanvas(string KeyDrawingCanvas) {
			return m_EnergyBusinessLayer.DrawingCanvasImage_GetListByKeyDrawingCanvas(KeyDrawingCanvas);
		}

		/// <summary>
		/// Gets a json store for the business entity DrawingCanvasImage.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<DrawingCanvasImage> DrawingCanvasImage_GetStore(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.DrawingCanvasImage_GetStore(paging, location);
		}

		/// <summary>
		/// Gets a store of DrawingCanvasImage for a specific DrawingCanvas.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyDrawingCanvas">The key of the DrawingCanvas.</param>
		/// <returns></returns>
		public JsonStore<DrawingCanvasImage> DrawingCanvasImage_GetStoreByKeyDrawingCanvas(PagingParameter paging, string KeyDrawingCanvas) {
			return m_EnergyBusinessLayer.DrawingCanvasImage_GetStoreByKeyDrawingCanvas(paging, KeyDrawingCanvas);
		}

		/// <summary>
		/// Saves a crud store for the business entity DrawingCanvasImage.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<DrawingCanvasImage> DrawingCanvasImage_SaveStore(CrudStore<DrawingCanvasImage> store) {
			return m_EnergyBusinessLayer.DrawingCanvasImage_SaveStore(store);
		}

		/// <summary>
		/// Deletes an existing business entity DynamicDisplay.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool DynamicDisplay_Delete(DynamicDisplay item) {
			return m_EnergyBusinessLayer.DynamicDisplay_Delete(item);
		}

		/// <summary>
		/// Creates a new business entity DynamicDisplay and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse DynamicDisplay_FormCreate(DynamicDisplay item) {
			return m_EnergyBusinessLayer.DynamicDisplay_FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity DynamicDisplay.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse DynamicDisplay_FormLoad(string Key) {
			return m_EnergyBusinessLayer.DynamicDisplay_FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity DynamicDisplay and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse DynamicDisplay_FormUpdate(DynamicDisplay item) {
			return m_EnergyBusinessLayer.DynamicDisplay_FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse DynamicDisplay_FormUpdateBatch(string[] keys, DynamicDisplay item) {
			return m_EnergyBusinessLayer.DynamicDisplay_FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity DynamicDisplay. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		public List<DynamicDisplay> DynamicDisplay_GetAll(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.DynamicDisplay_GetAll(paging, location);
		}

		/// <summary>
		/// Gets a specific item for the business entity DynamicDisplay.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public DynamicDisplay DynamicDisplay_GetItem(string Key) {
			return m_EnergyBusinessLayer.DynamicDisplay_GetItem(Key);
		}

		/// <summary>
		/// Generate modern UI dynamic displays.
		/// </summary>
		/// <returns></returns>
		public bool DynamicDisplay_GenerateModernUI() {
			return m_EnergyBusinessLayer.DynamicDisplay_GenerateModernUI();
		}

		/// <summary>
		/// Gets a json store for the business entity DynamicDisplay.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<DynamicDisplay> DynamicDisplay_GetStore(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.DynamicDisplay_GetStore(paging, location);
		}

		/// <summary>
		/// Returns the stream of the image  from a dynamicdisplay.
		/// </summary>
		/// <param name="Key">The key of the dynamicdisplay.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <param name="displayBogusChart">if set to <c>true</c> [display bogus chart].</param>
		/// <returns></returns>
		public Stream DynamicDisplay_GetStreamImage(string Key, int width, int height, bool displayBogusChart = true) {
			var retVal = m_EnergyBusinessLayer.DynamicDisplay_GetStreamImage(Key, width, height, displayBogusChart);
			WebOperationContext.Current.OutgoingResponse.ContentType = retVal.MimeType;
			return retVal.ContentStream;

		}

		/// <summary>
		/// Saves a crud store for the business entity DynamicDisplay.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<DynamicDisplay> DynamicDisplay_SaveStore(CrudStore<DynamicDisplay> store) {
			return m_EnergyBusinessLayer.DynamicDisplay_SaveStore(store);
		}

		/// <summary>
		/// Deletes an existing business entity DynamicDisplayColorTemplate.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool DynamicDisplayColorTemplate_Delete(DynamicDisplayColorTemplate item) {
			return m_EnergyBusinessLayer.DynamicDisplayColorTemplate_Delete(item);
		}

		/// <summary>
		/// Creates a new business entity DynamicDisplayColorTemplate and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse DynamicDisplayColorTemplate_FormCreate(DynamicDisplayColorTemplate item) {
			return m_EnergyBusinessLayer.DynamicDisplayColorTemplate_FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity DynamicDisplayColorTemplate.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse DynamicDisplayColorTemplate_FormLoad(string Key) {
			return m_EnergyBusinessLayer.DynamicDisplayColorTemplate_FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity DynamicDisplayColorTemplate and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse DynamicDisplayColorTemplate_FormUpdate(DynamicDisplayColorTemplate item) {
			return m_EnergyBusinessLayer.DynamicDisplayColorTemplate_FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse DynamicDisplayColorTemplate_FormUpdateBatch(string[] keys, DynamicDisplayColorTemplate item) {
			return m_EnergyBusinessLayer.DynamicDisplayColorTemplate_FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity DynamicDisplayColorTemplate. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		public List<DynamicDisplayColorTemplate> DynamicDisplayColorTemplate_GetAll(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.DynamicDisplayColorTemplate_GetAll(paging, location);
		}

		/// <summary>
		/// Gets a specific item for the business entity DynamicDisplayColorTemplate.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public DynamicDisplayColorTemplate DynamicDisplayColorTemplate_GetItem(string Key) {
			return m_EnergyBusinessLayer.DynamicDisplayColorTemplate_GetItem(Key);
		}

		/// <summary>
		/// Gets a json store for the business entity DynamicDisplayColorTemplate.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<DynamicDisplayColorTemplate> DynamicDisplayColorTemplate_GetStore(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.DynamicDisplayColorTemplate_GetStore(paging, location);
		}

		/// <summary>
		/// Saves a crud store for the business entity DynamicDisplayColorTemplate.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<DynamicDisplayColorTemplate> DynamicDisplayColorTemplate_SaveStore(CrudStore<DynamicDisplayColorTemplate> store) {
			return m_EnergyBusinessLayer.DynamicDisplayColorTemplate_SaveStore(store);
		}

		/// <summary>
		/// Deletes an existing business entity DynamicDisplayImage.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool DynamicDisplayImage_Delete(DynamicDisplayImage item) {
			return m_EnergyBusinessLayer.DynamicDisplayImage_Delete(item);
		}

		/// <summary>
		/// Creates a new business entity DynamicDisplayImage and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse DynamicDisplayImage_FormCreate(DynamicDisplayImage item) {
			return m_EnergyBusinessLayer.DynamicDisplayImage_FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity DynamicDisplayImage.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse DynamicDisplayImage_FormLoad(string Key) {
			return m_EnergyBusinessLayer.DynamicDisplayImage_FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity DynamicDisplayImage and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse DynamicDisplayImage_FormUpdate(DynamicDisplayImage item) {
			return m_EnergyBusinessLayer.DynamicDisplayImage_FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse DynamicDisplayImage_FormUpdateBatch(string[] keys, DynamicDisplayImage item) {
			return m_EnergyBusinessLayer.DynamicDisplayImage_FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity DynamicDisplayImage. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		public List<DynamicDisplayImage> DynamicDisplayImage_GetAll(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.DynamicDisplayImage_GetAll(paging, location);
		}

		/// <summary>
		/// Gets a specific item for the business entity DynamicDisplayImage.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public DynamicDisplayImage DynamicDisplayImage_GetItem(string Key) {
			return m_EnergyBusinessLayer.DynamicDisplayImage_GetItem(Key);
		}

		/// <summary>
		/// Gets a json store for the business entity DynamicDisplayImage.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="type">The type.</param>
		/// <returns>
		/// A json store.
		/// </returns>
		public JsonStore<DynamicDisplayImage> DynamicDisplayImage_GetStore(PagingParameter paging, PagingLocation location, DynamicDisplayImageType type) {
			return m_EnergyBusinessLayer.DynamicDisplayImage_GetStore(paging, location, type);
		}

		/// <summary>
		/// Returns the stream of the image  from a dynamicdisplayimage.
		/// </summary>
		/// <param name="Key">The key of the dynamicdisplayimage.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <returns></returns>
		public Stream DynamicDisplayImage_GetStreamImage(string Key, int width, int height) {
			var retVal = m_EnergyBusinessLayer.DynamicDisplayImage_GetStreamImage(Key, width, height);
			WebOperationContext.Current.OutgoingResponse.ContentType = retVal.MimeType;
			return retVal.ContentStream;
		}

		/// <summary>
		/// Saves a crud store for the business entity DynamicDisplayImage.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<DynamicDisplayImage> DynamicDisplayImage_SaveStore(CrudStore<DynamicDisplayImage> store) {
			return m_EnergyBusinessLayer.DynamicDisplayImage_SaveStore(store);
		}

		/// <summary>
		/// Deletes an existing business entity EnergyCertificate.
		/// </summary>
		/// <param name="keys">The list of keys of item to copy</param>
		/// <returns></returns>
		public FormResponse EnergyCertificate_Copy(string[] keys) {
			return m_EnergyBusinessLayer.EnergyCertificate_Copy(keys);
		}

		/// <summary>
		/// Deletes an existing business entity EnergyCertificate.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool EnergyCertificate_Delete(EnergyCertificate item) {
			return m_EnergyBusinessLayer.EnergyCertificate_Delete(item);
		}

		/// <summary>
		/// Creates a new business entity EnergyCertificate and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <param name="categories">The categories.</param>
		/// <returns></returns>
		public FormResponse EnergyCertificate_FormCreate(EnergyCertificate item, CrudStore<EnergyCertificateCategory> categories) {
			return m_EnergyBusinessLayer.EnergyCertificate_FormCreate(item, categories);
		}

		/// <summary>
		/// Loads a specific item for the business entity EnergyCertificate.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse EnergyCertificate_FormLoad(string Key) {
			return m_EnergyBusinessLayer.EnergyCertificate_FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity EnergyCertificate and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <param name="categories">The categories.</param>
		/// <returns></returns>
		public FormResponse EnergyCertificate_FormUpdate(EnergyCertificate item, CrudStore<EnergyCertificateCategory> categories) {
			return m_EnergyBusinessLayer.EnergyCertificate_FormUpdate(item, categories);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse EnergyCertificate_FormUpdateBatch(string[] keys, EnergyCertificate item) {
			return m_EnergyBusinessLayer.EnergyCertificate_FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity EnergyCertificate. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		public List<EnergyCertificate> EnergyCertificate_GetAll(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.EnergyCertificate_GetAll(paging, location);
		}

		/// <summary>
		/// Gets a specific item for the business entity EnergyCertificate.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public EnergyCertificate EnergyCertificate_GetItem(string Key) {
			return m_EnergyBusinessLayer.EnergyCertificate_GetItem(Key);
		}

		/// <summary>
		/// Gets a json store for the business entity EnergyCertificate.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<EnergyCertificate> EnergyCertificate_GetStore(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.EnergyCertificate_GetStore(paging, location);
		}

		/// <summary>
		/// Saves a crud store for the business entity EnergyCertificate.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<EnergyCertificate> EnergyCertificate_SaveStore(CrudStore<EnergyCertificate> store) {
			return m_EnergyBusinessLayer.EnergyCertificate_SaveStore(store);
		}

		/// <summary>
		/// Deletes an existing business entity EnergyCertificateCategory.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool EnergyCertificateCategory_Delete(EnergyCertificateCategory item) {
			return m_EnergyBusinessLayer.EnergyCertificateCategory_Delete(item);
		}

		/// <summary>
		/// Creates a new business entity EnergyCertificateCategory and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse EnergyCertificateCategory_FormCreate(EnergyCertificateCategory item) {
			return m_EnergyBusinessLayer.EnergyCertificateCategory_FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity EnergyCertificateCategory.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse EnergyCertificateCategory_FormLoad(string Key) {
			return m_EnergyBusinessLayer.EnergyCertificateCategory_FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity EnergyCertificateCategory and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse EnergyCertificateCategory_FormUpdate(EnergyCertificateCategory item) {
			return m_EnergyBusinessLayer.EnergyCertificateCategory_FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse EnergyCertificateCategory_FormUpdateBatch(string[] keys, EnergyCertificateCategory item) {
			return m_EnergyBusinessLayer.EnergyCertificateCategory_FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity EnergyCertificateCategory. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		public List<EnergyCertificateCategory> EnergyCertificateCategory_GetAll(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.EnergyCertificateCategory_GetAll(paging, location);
		}

		/// <summary>
		/// Gets a specific item for the business entity EnergyCertificateCategory.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public EnergyCertificateCategory EnergyCertificateCategory_GetItem(string Key) {
			return m_EnergyBusinessLayer.EnergyCertificateCategory_GetItem(Key);
		}

		/// <summary>
		/// Gets a json store for the business entity EnergyCertificateCategory.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<EnergyCertificateCategory> EnergyCertificateCategory_GetStore(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.EnergyCertificateCategory_GetStore(paging, location);
		}

		/// <summary>
		/// Gets a store of  EnergyCertificateCategory for a specific EnergyCertificate.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyEnergyCertificate">The key energy certificate.</param>
		/// <returns></returns>
		public JsonStore<EnergyCertificateCategory> EnergyCertificateCategory_GetStoreByKeyEnergyCertificate(PagingParameter paging, string KeyEnergyCertificate) {
			return m_EnergyBusinessLayer.EnergyCertificateCategory_GetStoreByKeyEnergyCertificate(paging, KeyEnergyCertificate);
		}

		/// <summary>
		/// Saves a crud store for the business entity EnergyCertificateCategory.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<EnergyCertificateCategory> EnergyCertificateCategory_SaveStore(CrudStore<EnergyCertificateCategory> store) {
			return m_EnergyBusinessLayer.EnergyCertificateCategory_SaveStore(store);
		}

		/// <summary>
		/// Deletes an existing business entity EventLog.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool EventLog_Delete(EventLog item) {
			return m_EnergyBusinessLayer.EventLog_Delete(item);
		}

		/// <summary>
		/// Creates a new business entity EventLog and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse EventLog_FormCreate(EventLog item) {
			return m_EnergyBusinessLayer.EventLog_FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity EventLog.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse EventLog_FormLoad(string Key) {
			return m_EnergyBusinessLayer.EventLog_FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity EventLog and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse EventLog_FormUpdate(EventLog item) {
			return m_EnergyBusinessLayer.EventLog_FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse EventLog_FormUpdateBatch(string[] keys, EventLog item) {
			return m_EnergyBusinessLayer.EventLog_FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity EventLog. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		public List<EventLog> EventLog_GetAll(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.EventLog_GetAll(paging, location);
		}

		/// <summary>
		/// Gets a specific item for the business entity EventLog.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public EventLog EventLog_GetItem(string Key) {
			return m_EnergyBusinessLayer.EventLog_GetItem(Key);
		}

		/// <summary>
		/// Gets a json store for the business entity EventLog.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<EventLog> EventLog_GetStore(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.EventLog_GetStore(paging, location);
		}

		/// <summary>
		/// Saves a crud store for the business entity EventLog.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<EventLog> EventLog_SaveStore(CrudStore<EventLog> store) {
			return m_EnergyBusinessLayer.EventLog_SaveStore(store);
		}

		/// <summary>
		/// Deletes an existing business entity EWSDataAcquisitionContainer.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool EWSDataAcquisitionContainer_Delete(EWSDataAcquisitionContainer item) {
			return m_EnergyBusinessLayer.EWSDataAcquisitionContainer_Delete(item);
		}

		/// <summary>
		/// Creates a new business entity EWSDataAcquisitionContainer and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse EWSDataAcquisitionContainer_FormCreate(EWSDataAcquisitionContainer item) {
			return m_EnergyBusinessLayer.EWSDataAcquisitionContainer_FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity EWSDataAcquisitionContainer.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse EWSDataAcquisitionContainer_FormLoad(string Key) {
			return m_EnergyBusinessLayer.EWSDataAcquisitionContainer_FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity EWSDataAcquisitionContainer and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse EWSDataAcquisitionContainer_FormUpdate(EWSDataAcquisitionContainer item) {
			return m_EnergyBusinessLayer.EWSDataAcquisitionContainer_FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse EWSDataAcquisitionContainer_FormUpdateBatch(string[] keys, EWSDataAcquisitionContainer item) {
			return m_EnergyBusinessLayer.EWSDataAcquisitionContainer_FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity EWSDataAcquisitionContainer. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		public List<EWSDataAcquisitionContainer> EWSDataAcquisitionContainer_GetAll(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.EWSDataAcquisitionContainer_GetAll(paging, location);
		}

		/// <summary>
		/// Gets a specific item for the business entity EWSDataAcquisitionContainer.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public EWSDataAcquisitionContainer EWSDataAcquisitionContainer_GetItem(string Key) {
			return m_EnergyBusinessLayer.EWSDataAcquisitionContainer_GetItem(Key);
		}

		/// <summary>
		/// Gets a json store for the business entity EWSDataAcquisitionContainer.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<EWSDataAcquisitionContainer> EWSDataAcquisitionContainer_GetStore(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.EWSDataAcquisitionContainer_GetStore(paging, location);
		}

		/// <summary>
		/// Saves a crud store for the business entity EWSDataAcquisitionContainer.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<EWSDataAcquisitionContainer> EWSDataAcquisitionContainer_SaveStore(CrudStore<EWSDataAcquisitionContainer> store) {
			return m_EnergyBusinessLayer.EWSDataAcquisitionContainer_SaveStore(store);
		}

		/// <summary>
		/// Deletes an existing business entity EWSDataAcquisitionEndpoint.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool EWSDataAcquisitionEndpoint_Delete(EWSDataAcquisitionEndpoint item) {
			return m_EnergyBusinessLayer.EWSDataAcquisitionEndpoint_Delete(item);
		}

		/// <summary>
		/// Creates a new business entity EWSDataAcquisitionEndpoint and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse EWSDataAcquisitionEndpoint_FormCreate(EWSDataAcquisitionEndpoint item) {
			return m_EnergyBusinessLayer.EWSDataAcquisitionEndpoint_FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity EWSDataAcquisitionEndpoint.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse EWSDataAcquisitionEndpoint_FormLoad(string Key) {
			return m_EnergyBusinessLayer.EWSDataAcquisitionEndpoint_FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity EWSDataAcquisitionEndpoint and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse EWSDataAcquisitionEndpoint_FormUpdate(EWSDataAcquisitionEndpoint item) {
			return m_EnergyBusinessLayer.EWSDataAcquisitionEndpoint_FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse EWSDataAcquisitionEndpoint_FormUpdateBatch(string[] keys, EWSDataAcquisitionEndpoint item) {
			return m_EnergyBusinessLayer.EWSDataAcquisitionEndpoint_FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity EWSDataAcquisitionEndpoint. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		public List<EWSDataAcquisitionEndpoint> EWSDataAcquisitionEndpoint_GetAll(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.EWSDataAcquisitionEndpoint_GetAll(paging, location);
		}

		/// <summary>
		/// Gets a specific item for the business entity EWSDataAcquisitionEndpoint.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public EWSDataAcquisitionEndpoint EWSDataAcquisitionEndpoint_GetItem(string Key) {
			return m_EnergyBusinessLayer.EWSDataAcquisitionEndpoint_GetItem(Key);
		}

		/// <summary>
		/// Gets a json store for the business entity EWSDataAcquisitionEndpoint.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<EWSDataAcquisitionEndpoint> EWSDataAcquisitionEndpoint_GetStore(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.EWSDataAcquisitionEndpoint_GetStore(paging, location);
		}

		/// <summary>
		/// Gets a store of  EWSDataAcquisitionEndpoint by key of the container.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="KeyDataAcquisitionContainer">The key data acquisition container.</param>
		/// <returns></returns>
		public JsonStore<EWSDataAcquisitionEndpoint> EWSDataAcquisitionEndpoint_GetStoreByKeyDataAcquisitionContainer(PagingParameter paging, string KeyDataAcquisitionContainer) {
			return m_EnergyBusinessLayer.EWSDataAcquisitionEndpoint_GetStoreByKeyDataAcquisitionContainer(paging, KeyDataAcquisitionContainer);
		}

		/// <summary>
		/// Saves a crud store for the business entity EWSDataAcquisitionEndpoint.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<EWSDataAcquisitionEndpoint> EWSDataAcquisitionEndpoint_SaveStore(CrudStore<EWSDataAcquisitionEndpoint> store) {
			return m_EnergyBusinessLayer.EWSDataAcquisitionEndpoint_SaveStore(store);
		}

		/// <summary>
		/// Gets a json store of the Filter AlarmDefinition Classification for a specific KeyAlarmTable.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="KeyAlarmTable">The key KeyAlarmTable.</param>
		/// <returns></returns>           
		public JsonStore<ClassificationItem> FilterAlarmDefinitionClassification_GetStoreByKeyAlarmTable(PagingParameter paging, string KeyAlarmTable) {
			var retVal = m_EnergyBusinessLayer.FilterAlarmDefinitionClassification_GetStoreByKeyAlarmTable(paging, KeyAlarmTable);
			return retVal;
		}

		/// <summary>
		/// Gets a json store of the Filter AlarmDefinition Classification for a specific Chart.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="KeyChart">The key chart.</param>
		/// <returns></returns>
		public JsonStore<ClassificationItem> FilterAlarmDefinitionClassification_GetStoreByKeyChart(PagingParameter paging, string KeyChart) {
			var retVal = m_EnergyBusinessLayer.FilterAlarmDefinitionClassification_GetStoreByKeyChart(paging, KeyChart);
			return retVal;
		}

		/// <summary>
		/// Gets a json store of the Filter EventLog Classification for a specific chart.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="KeyChart">The key chart.</param>
		/// <returns></returns>
		public JsonStore<ClassificationItem> FilterEventLogClassification_GetStoreByKeyChart(PagingParameter paging, string KeyChart) {
			var retVal = m_EnergyBusinessLayer.FilterEventLogClassification_GetStoreByKeyChart(paging, KeyChart);
			return retVal;
		}

		/// <summary>
		/// Gets a json store of the Filter Meter Classification for a specific AlarmDefinition.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="KeyAlarmDefinition">The key AlarmDefinition.</param>
		/// <returns></returns>
		public JsonStore<ClassificationItem> FilterMeterClassification_GetStoreByKeyAlarmDefinition(PagingParameter paging, string KeyAlarmDefinition) {
			var retVal = m_EnergyBusinessLayer.FilterMeterClassification_GetStoreByKeyAlarmDefinition(paging, KeyAlarmDefinition);
			return retVal;
		}

		/// <summary>
		/// Gets a json store of the Filter Meter Classification for a specific chart.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="KeyChart">The key chart.</param>
		/// <returns></returns>
		public JsonStore<ClassificationItem> FilterMeterClassification_GetStoreByKeyChart(PagingParameter paging, string KeyChart) {
			var retVal = m_EnergyBusinessLayer.FilterMeterClassification_GetStoreByKeyChart(paging, KeyChart);
			return retVal;
		}

		/// <summary>
		/// Gets a json store of the Filter Meter Classification for a specific MeterValidationRule.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="KeyMeterValidationRule">The key MeterValidationRule.</param>
		/// <returns></returns>
		public JsonStore<ClassificationItem> FilterMeterClassification_GetStoreByKeyMeterValidationRule(PagingParameter paging, string KeyMeterValidationRule) {
			return m_EnergyBusinessLayer.FilterMeterClassification_GetStoreByKeyMeterValidationRule(paging, KeyMeterValidationRule);
		}

		/// <summary>
		/// Gets a json store of the Filter Spatial for a specific AlarmDefinition.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="KeyAlarmDefinition">The key AlarmDefinition.</param>
		/// <returns></returns>
		public JsonStore<Location> FilterSpatial_GetStoreByKeyAlarmDefinition(PagingParameter paging, string KeyAlarmDefinition) {
			var retVal = m_EnergyBusinessLayer.FilterSpatial_GetStoreByKeyAlarmDefinition(paging, KeyAlarmDefinition);
			return retVal;
		}

		/// <summary>
		/// Gets a json store of the Filter Spatial for a specific AlarmTable.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="KeyAlarmTable">The key of the AlarmTable.</param>
		/// <returns></returns>
		public JsonStore<Location> FilterSpatial_GetStoreByKeyAlarmTable(PagingParameter paging, string KeyAlarmTable) {
			var retVal = m_EnergyBusinessLayer.FilterSpatial_GetStoreByKeyAlarmTable(paging, KeyAlarmTable);
			return retVal;
		}

		/// <summary>
		/// Gets a json store of the Filter Spatial for a specific chart.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="KeyChart">The key chart.</param>
		/// <returns></returns>
		public JsonStore<Location> FilterSpatial_GetStoreByKeyChart(PagingParameter paging, string KeyChart) {
			var retVal = m_EnergyBusinessLayer.FilterSpatial_GetStoreByKeyChart(paging, KeyChart);
			return retVal;
		}

		/// <summary>
		/// Gets a json store of the Filter Spatial for a specific MeterValidationRule.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="KeyMeterValidationRule">The key MeterValidationRule.</param>
		/// <returns></returns>
		public JsonStore<Location> FilterSpatial_GetStoreByKeyMeterValidationRule(PagingParameter paging, string KeyMeterValidationRule) {
			return m_EnergyBusinessLayer.FilterSpatial_GetStoreByKeyMeterValidationRule(paging, KeyMeterValidationRule);
		}

		/// <summary>
		/// Gets a json store of the Filter Spatial for a specific PortalTemplate.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="KeyPortalTemplate">The key PortalTemplate.</param>
		/// <returns></returns>
		public JsonStore<Location> FilterSpatial_GetStoreByKeyPortalTemplate(PagingParameter paging, string KeyPortalTemplate) {
			return m_EnergyBusinessLayer.FilterSpatial_GetStoreByKeyPortalTemplate(paging, KeyPortalTemplate);
		}

		/// <summary>
		/// Deletes an existing business entity FilterSpatialPset.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool FilterSpatialPset_Delete(FilterSpatialPset item) {
			return m_EnergyBusinessLayer.FilterSpatialPset_Delete(item);
		}

		/// <summary>
		/// Creates a new business entity FilterSpatialPset and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse FilterSpatialPset_FormCreate(FilterSpatialPset item) {
			return m_EnergyBusinessLayer.FilterSpatialPset_FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity FilterSpatialPset.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse FilterSpatialPset_FormLoad(string Key) {
			return m_EnergyBusinessLayer.FilterSpatialPset_FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity FilterSpatialPset and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse FilterSpatialPset_FormUpdate(FilterSpatialPset item) {
			return m_EnergyBusinessLayer.FilterSpatialPset_FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse FilterSpatialPset_FormUpdateBatch(string[] keys, FilterSpatialPset item) {
			return m_EnergyBusinessLayer.FilterSpatialPset_FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity FilterSpatialPset. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		public List<FilterSpatialPset> FilterSpatialPset_GetAll(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.FilterSpatialPset_GetAll(paging, location);
		}

		/// <summary>
		/// Gets a specific item for the business entity FilterSpatialPset.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FilterSpatialPset FilterSpatialPset_GetItem(string Key) {
			return m_EnergyBusinessLayer.FilterSpatialPset_GetItem(Key);
		}

		/// <summary>
		/// Gets a store of Location for a specific AlarmDefinition based on the FilterSpatialPset attached to it.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyAlarmDefinition">The key of the AlarmDefinition.</param>
		/// <returns></returns>
		public JsonStore<Location> FilterSpatialPset_GetLocationStoreByKeyAlarmDefinition(PagingParameter paging, string KeyAlarmDefinition) {
			return m_EnergyBusinessLayer.FilterSpatialPset_GetLocationStoreByKeyAlarmDefinition(paging, KeyAlarmDefinition);
		}

		/// <summary>
		/// Gets a store of Location for a specific chart based on the FilterSpatialPset attached to it.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyChart">The key of the chart.</param>
		/// <returns></returns>
		public JsonStore<Location> FilterSpatialPset_GetLocationStoreByKeyChart(PagingParameter paging, string KeyChart) {
			return m_EnergyBusinessLayer.FilterSpatialPset_GetLocationStoreByKeyChart(paging, KeyChart);
		}

		/// <summary>
		/// Gets a json store for the business entity FilterSpatialPset.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<FilterSpatialPset> FilterSpatialPset_GetStore(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.FilterSpatialPset_GetStore(paging, location);
		}

		/// <summary>
		/// Gets a store of FilterSpatialPset for a specific AlarmDefinition.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyAlarmDefinition">The key of the AlarmDefinition.</param>
		/// <returns></returns>
		public JsonStore<FilterSpatialPset> FilterSpatialPset_GetStoreByKeyAlarmDefinition(PagingParameter paging, string KeyAlarmDefinition) {
			return m_EnergyBusinessLayer.FilterSpatialPset_GetStoreByKeyAlarmDefinition(paging, KeyAlarmDefinition);
		}

		/// <summary>
		/// Gets a store of FilterSpatialPset for a specific chart.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyChart">The key of the chart.</param>
		/// <returns></returns>
		public JsonStore<FilterSpatialPset> FilterSpatialPset_GetStoreByKeyChart(PagingParameter paging, string KeyChart) {
			return m_EnergyBusinessLayer.FilterSpatialPset_GetStoreByKeyChart(paging, KeyChart);
		}

		/// <summary>
		/// Saves a crud store for the business entity FilterSpatialPset.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<FilterSpatialPset> FilterSpatialPset_SaveStore(CrudStore<FilterSpatialPset> store) {
			return m_EnergyBusinessLayer.FilterSpatialPset_SaveStore(store);
		}

		/// <summary>
		/// Gets a json store for the business entity FOLMembershipUser for a specific PortalTemplate.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyPortalTemplate">The Key PortalTemplate.</param>
		/// <returns>
		/// A json store.
		/// </returns>
		public JsonStore<FOLMembershipUser> FOLMembershipUser_GetStoreByKeyPortalTemplate(PagingParameter paging, string KeyPortalTemplate) {
			return m_EnergyBusinessLayer.FOLMembershipUser_GetStoreByKeyPortalTemplate(paging, KeyPortalTemplate);
		}

		/// <summary>
		/// Initializes the energy aggregator service agent.
		/// </summary>
		public void InitializeEnergyAggregatorServiceAgent() {
			m_EnergyBusinessLayer.InitializeEnergyAggregatorServiceAgent();
		}

		/// <summary>
		/// Deletes an existing business entity MachineInstance.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool MachineInstance_Delete(MachineInstance item) {
			return m_EnergyBusinessLayer.MachineInstance_Delete(item);
		}

		/// <summary>
		/// Create a new Machine Instance
		/// </summary>
		/// <param name="item">The item.</param>
		/// <returns></returns>
		public FormResponse MachineInstance_FormCreate(MachineInstance item) {
			return m_EnergyBusinessLayer.MachineInstance_FormCreate(item);
		}

		/// <summary>
		/// Gets a json store for the business entity MachineInstance.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <returns>
		/// A json store.
		/// </returns>
		public JsonStore<MachineInstance> MachineInstance_GetStore(PagingParameter paging) {
			return m_EnergyBusinessLayer.MachineInstance_GetStore(paging);
		}

		/// <summary>
		/// Build the Chart of the Machines memory usage.
		/// </summary>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <returns></returns>
		public ImageMap MachineInstance_MemoryUsageBuildImage(int width, int height) {
			return m_EnergyBusinessLayer.MachineInstance_MemoryUsageBuildImage(width, height);
		}

		/// <summary>
		///Get stream image of Machines memory usage from cache.
		/// </summary>
		/// <param name="guid">The GUID.</param>
		/// <returns></returns>
		public Stream MachineInstance_MemoryUsageGetStreamImageFromCache(string guid) {
			return m_EnergyBusinessLayer.MachineInstance_MemoryUsageGetStreamImageFromCache(guid);
		}

		/// <summary>
		/// Restart the passed machine instance.
		/// </summary>
		/// <param name="item">The item.</param>
		public void MachineInstance_Restart(MachineInstance item) {
			m_EnergyBusinessLayer.MachineInstance_Restart(item);
		}

		/// <summary>
		/// Returns if the MachineInstance service is enabled or not.
		/// </summary>
		/// <returns></returns>
		public bool MachineInstance_ServiceIsEnabled() {
			return m_EnergyBusinessLayer.MachineInstance_ServiceIsEnabled();
		}

		/// <summary>
		/// Deletes an existing business entity Map.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool Map_Delete(Map item) {
			return m_EnergyBusinessLayer.Map_Delete(item);
		}

		/// <summary>
		/// Creates a new business entity Map and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse Map_FormCreate(Map item) {
			return m_EnergyBusinessLayer.Map_FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity Map.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse Map_FormLoad(string Key) {
			return m_EnergyBusinessLayer.Map_FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity Map and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse Map_FormUpdate(Map item) {
			return m_EnergyBusinessLayer.Map_FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse Map_FormUpdateBatch(string[] keys, Map item) {
			return m_EnergyBusinessLayer.Map_FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity Map. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		public List<Map> Map_GetAll(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.Map_GetAll(paging, location);
		}

		/// <summary>
		/// Gets a specific item for the business entity Map.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public Map Map_GetItem(string Key) {
			return m_EnergyBusinessLayer.Map_GetItem(Key);
		}

		/// <summary>
		///Return the Map Key to be used when instanciating a new Key client side.
		/// </summary>
		/// <returns></returns>
		public string Map_GetKey() {
			return m_EnergyBusinessLayer.Map_GetKey();
		}

		/// <summary>
		/// Get the Map Pushpins.
		/// </summary>
		/// <param name="Key">The key.</param>
		/// <returns></returns>
		public List<MapPushpin> Map_GetPushpins(string Key) {
			return m_EnergyBusinessLayer.Map_GetPushpins(Key);
		}

		/// <summary>
		/// Beging long running operation to Get the Map Pushpins.
		/// </summary>
		/// <param name="operationId">The Operation Id.</param>
		/// <param name="key">The key.</param>
		/// <returns></returns>
		public void Map_BeginGetPushpins(Guid operationId, string key) {
			m_EnergyBusinessLayer.Map_BeginGetPushpins(operationId, key);
		}

		/// <summary>
		/// Gets a json store for the business entity Map.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<Map> Map_GetStore(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.Map_GetStore(paging, location);
		}

		/// <summary>
		/// Saves a crud store for the business entity Map.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<Map> Map_SaveStore(CrudStore<Map> store) {
			return m_EnergyBusinessLayer.Map_SaveStore(store);
		}

		/// <summary>
		/// Builds the image (with map) from a single meter.
		/// </summary>
		/// <param name="Key">The key.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <returns></returns>
		public ImageMap Meter_BuildImage(string Key, int width, int height) {
			return m_EnergyBusinessLayer.Meter_BuildImage(Key, width, height);
		}

		/// <summary>
		/// Deletes an existing business entity Meter.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool Meter_Delete(Meter item) {
			return m_EnergyBusinessLayer.Meter_Delete(item);
		}

		/// <summary>
		/// Creates a new business entity Meter and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <param name="meterOperationsWizard">The meter operations wizard.</param>
		/// <returns></returns>
		public FormResponse Meter_FormCreate(Meter item, CrudStore<Meter> meterOperationsWizard) {
			return m_EnergyBusinessLayer.Meter_FormCreate(item, meterOperationsWizard);
		}

		/// <summary>
		/// Loads a specific item for the business entity Meter.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse Meter_FormLoad(string Key) {
			return m_EnergyBusinessLayer.Meter_FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity Meter and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <param name="meterOperations">The meter operations.</param>
		/// <returns></returns>
		public FormResponse Meter_FormUpdate(Meter item, CrudStore<MeterOperation> meterOperations) {
			return m_EnergyBusinessLayer.Meter_FormUpdate(item, meterOperations);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse Meter_FormUpdateBatch(string[] keys, Meter item) {
			return m_EnergyBusinessLayer.Meter_FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity Meter. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		public List<Meter> Meter_GetAll(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.Meter_GetAll(paging, location);
		}

		/// <summary>
		/// Gets a specific item for the business entity Meter.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public Meter Meter_GetItem(string Key) {
			return m_EnergyBusinessLayer.Meter_GetItem(Key);
		}

		/// <summary>
		/// Gets a json store for the business entity Meter.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<Meter> Meter_GetStore(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.Meter_GetStore(paging, location);
		}

		/// <summary>
		/// Gets a json store of meters for a specific AlarmDefinition.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyAlarmDefinition">The Key of the AlarmDefinition.</param>
		/// <returns>A json store.</returns>
		public JsonStore<Meter> Meter_GetStoreByKeyAlarmDefinition(PagingParameter paging, string KeyAlarmDefinition) {
			return m_EnergyBusinessLayer.Meter_GetStoreByKeyAlarmDefinition(paging, KeyAlarmDefinition);
		}

		/// <summary>
		/// Gets a json store of meters for a specific chart.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyChart">The Key of the chart.</param>
		/// <returns>A json store.</returns>
		public JsonStore<Meter> Meter_GetStoreByKeyChart(PagingParameter paging, string KeyChart) {
			return m_EnergyBusinessLayer.Meter_GetStoreByKeyChart(paging, KeyChart);
		}

		/// <summary>
		/// Gets a json store of meters for a specific meter data export task.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyMeterDataExportTask">The key meter data export task.</param>
		/// <returns>A json store.</returns>
		public JsonStore<Meter> Meter_GetStoreByKeyMeterDataExportTask(PagingParameter paging, string KeyMeterDataExportTask) {
			return m_EnergyBusinessLayer.Meter_GetStoreByKeyMeterDataExportTask(paging, KeyMeterDataExportTask);
		}

		/// <summary>
		/// Gets a store of meters for a specific MeterValidationRule.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyMeterValidationRule">The key of the MeterValidationRule.</param>
		/// <returns></returns>
		public JsonStore<Meter> Meter_GetStoreByKeyMeterValidationRule(PagingParameter paging, string KeyMeterValidationRule) {
			return m_EnergyBusinessLayer.Meter_GetStoreByKeyMeterValidationRule(paging, KeyMeterValidationRule);
		}

		/// <summary>
		/// Gets a store of meters for a specific location and classification item.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyLocation">The key of the Location.</param>
		/// <param name="KeyClassificationItem">The key classification item.</param>
		/// <returns></returns>
		public JsonStore<Meter> Meter_GetStoreByLocationAndClassification(PagingParameter paging, string KeyLocation, string KeyClassificationItem) {
			var retVal = m_EnergyBusinessLayer.Meter_GetStoreByLocationAndClassification(paging, KeyLocation, KeyClassificationItem);
			return retVal;
		}

		/// <summary>
		/// Save the current end point value of a meter.
		/// </summary>
		/// <param name="KeyMeter">The key meter.</param>
		public bool Meter_SaveEndpointValue(string KeyMeter) {
			var retVal = m_EnergyBusinessLayer.Meter_SaveEndpointValue(KeyMeter);
			return retVal;
		}

		/// <summary>
		/// Saves a crud store for the business entity Meter.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<Meter> Meter_SaveStore(CrudStore<Meter> store) {
			return m_EnergyBusinessLayer.Meter_SaveStore(store);
		}

		/// <summary>
		/// Generates the json structure of the ClassificationItem treeview for the Meter Classification filtered by Chart or AlarmDefinition.
		/// </summary>
		/// <param name="Key">The Key of the parent node.</param>
		/// <param name="entity">The parent entity.</param>
		/// <param name="KeyChart">The key chart.</param>
		/// <param name="KeyAlarmDefinition">The key alarm definition.</param>
		/// <param name="KeyMeterValidationRule">The key meter validation rule.</param>
		/// <param name="existing">if set to <c>true</c>
		/// the method will only return MeterClassifcation for meter that are attached to the chart  or AlarmDefinition,
		/// else it will return all available MeterClassification in the spatial filter of the chart or AlarmDefinition.</param>
		/// <param name="filterSpatial">The current filter spatial of the Chart or AlarmDefinition.</param>
		/// <returns></returns>
		public List<TreeNode> MeterClassification_GetTree(string Key, ClassificationItem entity, string KeyChart, string KeyAlarmDefinition, string KeyMeterValidationRule, bool existing, CrudStore<Location> filterSpatial) {
			return m_EnergyBusinessLayer.MeterClassification_GetTree(Key, entity, KeyChart, KeyAlarmDefinition, KeyMeterValidationRule, existing, filterSpatial);
		}

		/// <summary>
		/// Deletes an existing business entity MeterData.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool MeterData_Delete(MeterData item) {
			return m_EnergyBusinessLayer.MeterData_Delete(item);
		}

		/// <summary>
		/// Delete MeterData by Date Range (long running start point).
		/// </summary>
		/// <param name="operationId">The operation id.</param>
		/// <param name="KeyMeter">The key of the meter.</param>
		/// <param name="StartDate">The start date.</param>
		/// <param name="EndDate">The end date.</param>
		public void MeterData_DeleteByDateRangeBegin(Guid operationId, string KeyMeter, DateTime StartDate, DateTime EndDate) {
			m_EnergyBusinessLayer.MeterData_DeleteByDateRangeBegin(operationId, KeyMeter, StartDate, EndDate);
		}

		/// <summary>
		/// Creates a new business entity MeterData and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse MeterData_FormCreate(MeterData item) {
			return m_EnergyBusinessLayer.MeterData_FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity MeterData.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse MeterData_FormLoad(string Key) {
			return m_EnergyBusinessLayer.MeterData_FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity MeterData and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse MeterData_FormUpdate(MeterData item) {
			return m_EnergyBusinessLayer.MeterData_FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse MeterData_FormUpdateBatch(string[] keys, MeterData item) {
			return m_EnergyBusinessLayer.MeterData_FormUpdateBatch(keys, item);

		}

		/// <summary>
		/// Generate Random MeterData.
		/// </summary>
		/// <param name="operationId">The operation id.</param>
		/// <param name="KeyMeter">The key of the meter.</param>
		/// <param name="StartDate">The start date.</param>
		/// <param name="EndDate">The end date.</param>
		/// <param name="FrequencyNumber">The frequency number.</param>
		/// <param name="Frequency">The frequency.</param>
		/// <param name="Minimum">The minimum.</param>
		/// <param name="Maximum">The maximum.</param>
		/// <returns></returns>
		public void MeterData_GenerateBegin(Guid operationId, string KeyMeter, DateTime StartDate, DateTime EndDate, int FrequencyNumber, AxisTimeInterval Frequency, double Minimum, double Maximum) {
			m_EnergyBusinessLayer.MeterData_GenerateBegin(operationId, KeyMeter, StartDate, EndDate, FrequencyNumber, Frequency, Minimum, Maximum);
		}

		/// <summary>
		/// Gets a specific item for the business entity MeterData.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public MeterData MeterData_GetItem(string Key) {
			return m_EnergyBusinessLayer.MeterData_GetItem(Key);
		}

		/// <summary>
		/// Export all data for a list of Meters
		/// </summary>
		/// <param name="localIds">The meter local ids.</param>
		/// <param name="startDate">The start date.</param>
		/// <param name="endDate">The end date.</param>
		/// <param name="zip">if set to <c>true</c> [zip].</param>
		/// <returns></returns>
		public Stream MeterData_GetCSVExport(List<string> localIds, DateTime? startDate, DateTime? endDate, bool zip) {
			StreamResult result = m_EnergyBusinessLayer.MeterData_GetCSVExport(localIds, startDate, endDate, zip);
			this.PrepareFileAttachment(result);
			return result.ContentStream;
		}

		/// <summary>
		/// Gets a json store for the business entity MeterData.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<MeterData> MeterData_GetStore(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.MeterData_GetStore(paging, location);
		}

		/// <summary>
		/// Gets a json store for the business entity MeterData.
		/// </summary>
		/// <param name="KeyMeter">The Key of the parent Meter.</param>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="timeZoneId">The time zone id.</param>
		/// <param name="rawData">if set to <c>true</c> [raw data].</param>
		/// <param name="datetimeFormat">The datetime format.</param>
		/// <returns>
		/// A json store.
		/// </returns>
		public JsonStore<MeterData> MeterData_GetStoreFromMeter(string KeyMeter, PagingParameter paging, PagingLocation location, string timeZoneId, bool rawData, string datetimeFormat) {
			return m_EnergyBusinessLayer.MeterData_GetStoreFromMeter(KeyMeter, paging, location, timeZoneId, rawData, datetimeFormat);
		}

		/// <summary>
		/// Offset MeterData by Date Range (long running start point) (md.Value * Factor - Offset).
		/// </summary>
		/// <param name="operationId">The operation id.</param>
		/// <param name="KeyMeter">The key of the meter.</param>
		/// <param name="StartDate">The start date.</param>
		/// <param name="EndDate">The end date.</param>
		/// <param name="Factor">The factor.</param>
		/// <param name="Offset">The offset</param>
		public void MeterData_OffsetByDateRangeBegin(Guid operationId, string KeyMeter, DateTime StartDate, DateTime EndDate, double Factor, double Offset) {
			m_EnergyBusinessLayer.MeterData_OffsetByDateRangeBegin(operationId, KeyMeter, StartDate, EndDate, Factor, Offset);

		}

		/// <summary>
		/// Saves a crud store for the business entity MeterData.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<MeterData> MeterData_SaveStore(CrudStore<MeterData> store) {
			return m_EnergyBusinessLayer.MeterData_SaveStore(store);
		}

		/// <summary>
		/// Validate a new MeterData (used in the MeterData form to validate a meter data when inputted).
		/// </summary>
		/// <param name="item">The Meter Data.</param>
		/// <returns></returns>
		public List<AlarmInstance> MeterData_Validate(MeterData item) {
			return m_EnergyBusinessLayer.MeterData_Validate(item);
		}

		/// <summary>
		/// Deletes an existing business entity MeterOperation.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool MeterOperation_Delete(MeterOperation item) {
			return m_EnergyBusinessLayer.MeterOperation_Delete(item);
		}

		/// <summary>
		/// Creates a new business entity MeterOperation and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse MeterOperation_FormCreate(MeterOperation item) {
			return m_EnergyBusinessLayer.MeterOperation_FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity MeterOperation.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse MeterOperation_FormLoad(string Key) {
			return m_EnergyBusinessLayer.MeterOperation_FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity MeterOperation and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse MeterOperation_FormUpdate(MeterOperation item) {
			return m_EnergyBusinessLayer.MeterOperation_FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse MeterOperation_FormUpdateBatch(string[] keys, MeterOperation item) {
			return m_EnergyBusinessLayer.MeterOperation_FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity MeterOperation. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		public List<MeterOperation> MeterOperation_GetAll(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.MeterOperation_GetAll(paging, location);
		}

		/// <summary>
		/// Gets a specific item for the business entity MeterOperation.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public MeterOperation MeterOperation_GetItem(string Key) {
			return m_EnergyBusinessLayer.MeterOperation_GetItem(Key);
		}

		/// <summary>
		/// Gets a json store for the business entity MeterOperation.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<MeterOperation> MeterOperation_GetStore(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.MeterOperation_GetStore(paging, location);
		}

		/// <summary>
		/// Gets a store of MeterOperations for a specific meter.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyMeter">The key of the meter.</param>
		/// <param name="KeyMeterOperation">The key meter operation  to exclude.</param>
		/// <param name="order">The order if the current meter operation in order to filter available results. 0 to disable filtering.</param>
		/// <returns></returns>
		public JsonStore<MeterOperation> MeterOperation_GetStoreByKeyMeter(PagingParameter paging, string KeyMeter, string KeyMeterOperation, int order) {
			return m_EnergyBusinessLayer.MeterOperation_GetStoreByKeyMeter(paging, KeyMeter, KeyMeterOperation, order);
		}

		/// <summary>
		/// Saves a crud store for the business entity MeterOperation.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<MeterOperation> MeterOperation_SaveStore(CrudStore<MeterOperation> store) {
			return m_EnergyBusinessLayer.MeterOperation_SaveStore(store);
		}

		/// <summary>
		/// Deletes an existing business entity MeterValidationRule.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool MeterValidationRule_Delete(MeterValidationRule item) {
			return m_EnergyBusinessLayer.MeterValidationRule_Delete(item);
		}

		/// <summary>
		/// Creates a new business entity MeterValidationRule and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <param name="meters">The meters.</param>
		/// <param name="filterSpatial">The filter spatial.</param>
		/// <param name="filterMeterClassification">The filter meter classification.</param>
		/// <returns></returns>
		public FormResponse MeterValidationRule_FormCreate(MeterValidationRule item, CrudStore<Meter> meters, CrudStore<Location> filterSpatial, CrudStore<ClassificationItem> filterMeterClassification) {
			return m_EnergyBusinessLayer.MeterValidationRule_FormCreate(item, meters, filterSpatial, filterMeterClassification);
		}

		/// <summary>
		/// Loads a specific item for the business entity MeterValidationRule.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse MeterValidationRule_FormLoad(string Key) {
			return m_EnergyBusinessLayer.MeterValidationRule_FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity MeterValidationRule and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <param name="meters">The meters.</param>
		/// <param name="filterSpatial">The filter spatial.</param>
		/// <param name="filterMeterClassification">The filter meter classification.</param>
		/// <returns></returns>
		public FormResponse MeterValidationRule_FormUpdate(MeterValidationRule item, CrudStore<Meter> meters, CrudStore<Location> filterSpatial, CrudStore<ClassificationItem> filterMeterClassification) {
			return m_EnergyBusinessLayer.MeterValidationRule_FormUpdate(item, meters, filterSpatial, filterMeterClassification);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse MeterValidationRule_FormUpdateBatch(string[] keys, MeterValidationRule item) {
			return m_EnergyBusinessLayer.MeterValidationRule_FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity MeterValidationRule. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		public List<MeterValidationRule> MeterValidationRule_GetAll(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.MeterValidationRule_GetAll(paging, location);
		}

		/// <summary>
		/// Gets a specific item for the business entity MeterValidationRule.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public MeterValidationRule MeterValidationRule_GetItem(string Key) {
			return m_EnergyBusinessLayer.MeterValidationRule_GetItem(Key);
		}

		/// <summary>
		/// Return a json store of the complete list of Meters associated with a MeterValidationRule.
		/// including meters associated via SpatialFilter cross SpatialFilterPset cross MeterClassification
		/// </summary>
		/// <param name="KeyMeterValidationRule">The key of the MeterValidationRule.</param>
		/// <param name="paging">The paging.</param>
		/// <returns></returns>
		public JsonStore<Meter> MeterValidationRule_GetMetersStore(string KeyMeterValidationRule, PagingParameter paging) {
			return m_EnergyBusinessLayer.MeterValidationRule_GetMetersStore(KeyMeterValidationRule, paging);
		}

		/// <summary>
		/// Gets a json store for the business entity MeterValidationRule.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<MeterValidationRule> MeterValidationRule_GetStore(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.MeterValidationRule_GetStore(paging, location);
		}

		/// <summary>
		/// Gets all the enabled MeterValidationRule and process them to generate the AlarmInstances
		/// </summary>
		/// <param name="Keys">The keys.</param>
		public void MeterValidationRule_Process(string[] Keys) {
			m_EnergyBusinessLayer.MeterValidationRule_Process(Keys);
		}

		/// <summary>
		/// Gets all the enabled MeterValidationRule and process them to generate the AlarmInstances with a long running operation.
		/// </summary>
		/// <param name="operationId">The id of the operation.</param>
		/// <param name="Keys">The keys.</param>
		public void MeterValidationRule_ProcessBegin(Guid operationId, string[] Keys) {
			m_EnergyBusinessLayer.MeterValidationRule_ProcessBegin(operationId, Keys);
		}

		/// <summary>
		/// Saves a crud store for the business entity MeterValidationRule.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<MeterValidationRule> MeterValidationRule_SaveStore(CrudStore<MeterValidationRule> store) {
			return m_EnergyBusinessLayer.MeterValidationRule_SaveStore(store);
		}

		/// <summary>
		/// Begins long running operation to Save a crud store for the business entity MeterValidationRule.
		/// </summary>
		/// <param name="operationId">The operation identifier.</param>
		/// <param name="store">The crud store.</param>
		public void MeterValidationRule_BeginSaveStore(Guid operationId, CrudStore<MeterValidationRule> store) {
			m_EnergyBusinessLayer.MeterValidationRule_BeginSaveStore(operationId, store);
		}


		/// <summary>
		/// Copy the specified palettes and colors..
		/// </summary>
		/// <param name="keys">The keys.</param>
		/// <returns></returns>
		public FormResponse Palette_Copy(string[] keys) {
			return m_EnergyBusinessLayer.Palette_Copy(keys);
		}
		/// <summary>
		/// Deletes an existing business entity Palette.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool Palette_Delete(Palette item) {
			return m_EnergyBusinessLayer.Palette_Delete(item);
		}

		/// <summary>
		/// Creates a new business entity Palette and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <param name="palettecolor">The palettecolor crudstore.</param>
		/// <returns></returns>
		public FormResponse Palette_FormCreate(Palette item, CrudStore<PaletteColor> palettecolor) {
			return m_EnergyBusinessLayer.Palette_FormCreate(item, palettecolor);
		}

		/// <summary>
		/// Loads a specific item for the business entity Palette.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse Palette_FormLoad(string Key) {
			return m_EnergyBusinessLayer.Palette_FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity Palette and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <param name="palettecolor">The palettecolor crudstore.</param>
		/// <returns></returns>
		public FormResponse Palette_FormUpdate(Palette item, CrudStore<PaletteColor> palettecolor) {
			return m_EnergyBusinessLayer.Palette_FormUpdate(item, palettecolor);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse Palette_FormUpdateBatch(string[] keys, Palette item) {
			return m_EnergyBusinessLayer.Palette_FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity Palette. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		public List<Palette> Palette_GetAll(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.Palette_GetAll(paging, location);
		}

		/// <summary>
		/// Gets a specific item for the business entity Palette.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public Palette Palette_GetItem(string Key) {
			return m_EnergyBusinessLayer.Palette_GetItem(Key);
		}

		/// <summary>
		/// Gets a json store for the business entity Palette.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<Palette> Palette_GetStore(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.Palette_GetStore(paging, location);
		}

		/// <summary>
		/// Returns the stream of the image that represent a palette.
		/// </summary>
		/// <param name="Key">The key of the Palette.</param>
		/// <param name="height">The height.</param>
		/// <param name="width">The width.</param>
		/// <returns></returns>
		public Stream Palette_GetStreamImage(string Key, int height, int width) {
			var retVal = m_EnergyBusinessLayer.Palette_GetStreamImage(Key, height, width);
			WebOperationContext.Current.OutgoingResponse.ContentType = retVal.MimeType;
			return retVal.ContentStream;
		}

		/// <summary>
		/// Saves a crud store for the business entity Palette.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<Palette> Palette_SaveStore(CrudStore<Palette> store) {
			return m_EnergyBusinessLayer.Palette_SaveStore(store);
		}

		/// <summary>
		/// Deletes an existing business entity PaletteColor.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool PaletteColor_Delete(PaletteColor item) {
			return m_EnergyBusinessLayer.PaletteColor_Delete(item);
		}

		/// <summary>
		/// Creates a new business entity PaletteColor and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse PaletteColor_FormCreate(PaletteColor item) {
			return m_EnergyBusinessLayer.PaletteColor_FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity PaletteColor.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse PaletteColor_FormLoad(string Key) {
			return m_EnergyBusinessLayer.PaletteColor_FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity PaletteColor and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse PaletteColor_FormUpdate(PaletteColor item) {
			return m_EnergyBusinessLayer.PaletteColor_FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse PaletteColor_FormUpdateBatch(string[] keys, PaletteColor item) {
			return m_EnergyBusinessLayer.PaletteColor_FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity PaletteColor. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		public List<PaletteColor> PaletteColor_GetAll(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.PaletteColor_GetAll(paging, location);
		}

		/// <summary>
		/// Gets a specific item for the business entity PaletteColor.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public PaletteColor PaletteColor_GetItem(string Key) {
			return m_EnergyBusinessLayer.PaletteColor_GetItem(Key);
		}

		/// <summary>
		/// Gets a list of PaletteColor for a specific Palette.
		/// </summary>
		/// <param name="KeyPalette">The key of the Palette.</param>
		/// <returns></returns>
		public List<PaletteColor> PaletteColor_GetListByKeyPalette(string KeyPalette) {
			return m_EnergyBusinessLayer.PaletteColor_GetListByKeyPalette(KeyPalette);
		}

		/// <summary>
		/// Gets a json store for the business entity PaletteColor.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<PaletteColor> PaletteColor_GetStore(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.PaletteColor_GetStore(paging, location);
		}

		/// <summary>
		/// Gets a store of PaletteColor for a specific Palette.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyPalette">The key of the Palette.</param>
		/// <returns></returns>
		public JsonStore<PaletteColor> PaletteColor_GetStoreByKeyPalette(PagingParameter paging, string KeyPalette) {
			return m_EnergyBusinessLayer.PaletteColor_GetStoreByKeyPalette(paging, KeyPalette);
		}

		/// <summary>
		/// Saves a crud store for the business entity PaletteColor.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<PaletteColor> PaletteColor_SaveStore(CrudStore<PaletteColor> store) {
			return m_EnergyBusinessLayer.PaletteColor_SaveStore(store);
		}

		/// <summary>
		/// Deletes an existing business entity Playlist.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool Playlist_Delete(Playlist item) {
			return m_EnergyBusinessLayer.Playlist_Delete(item);
		}

		/// <summary>
		/// Creates a new business entity Playlist and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <param name="pages">The pages.</param>
		/// <returns></returns>
		public FormResponse Playlist_FormCreate(Playlist item, CrudStore<PlaylistPage> pages) {
			return m_EnergyBusinessLayer.Playlist_FormCreate(item, pages);
		}

		/// <summary>
		/// Loads a specific item for the business entity Playlist.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse Playlist_FormLoad(string Key) {
			return m_EnergyBusinessLayer.Playlist_FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity Playlist and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <param name="pages">The pages.</param>
		/// <returns></returns>
		public FormResponse Playlist_FormUpdate(Playlist item, CrudStore<PlaylistPage> pages) {
			return m_EnergyBusinessLayer.Playlist_FormUpdate(item, pages);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse Playlist_FormUpdateBatch(string[] keys, Playlist item) {
			return m_EnergyBusinessLayer.Playlist_FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Generates the differents playlist as images. with a long running operation.
		/// </summary>
		/// <param name="operationId">The id of the operation.</param>
		/// <param name="Keys">The keys.</param>
		public void Playlist_GenerateBegin(Guid operationId, string[] Keys) {
			m_EnergyBusinessLayer.Playlist_GenerateBegin(operationId, Keys);
		}

		/// <summary>
		/// Generates the differents playlist as images.
		/// </summary>
		/// <param name="Keys">The keys.</param>
		public void Playlist_Generate(string[] Keys) {
			m_EnergyBusinessLayer.Playlist_Generate(Keys);
		}

		/// <summary>
		/// Gets a list for the business entity Playlist. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		public List<Playlist> Playlist_GetAll(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.Playlist_GetAll(paging, location);
		}

		/// <summary>
		/// Gets a specific item for the business entity Playlist.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public Playlist Playlist_GetItem(string Key) {
			return m_EnergyBusinessLayer.Playlist_GetItem(Key);
		}

		/// <summary>
		/// Gets a json store for the business entity Playlist.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<Playlist> Playlist_GetStore(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.Playlist_GetStore(paging, location);
		}

		/// <summary>
		/// Gets the Playlist store by the job step.
		/// </summary>
		/// <param name="keyJob">The key job.</param>
		/// <param name="keyStep">The key step.</param>
		/// <param name="paging">The paging.</param>
		/// <param name="location">The location.</param>
		/// <returns></returns>
		public JsonStore<Playlist> Playlist_GetStoreByJobStep(string keyJob, string keyStep, PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.Playlist_GetStoreByJobStep(keyJob, keyStep, paging, location);
		}

		/// <summary>
		/// Saves a crud store for the business entity Playlist.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<Playlist> Playlist_SaveStore(CrudStore<Playlist> store) {
			return m_EnergyBusinessLayer.Playlist_SaveStore(store);
		}

		/// <summary>
		/// Deletes an existing business entity PlaylistPage.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool PlaylistPage_Delete(PlaylistPage item) {
			return m_EnergyBusinessLayer.PlaylistPage_Delete(item);
		}

		/// <summary>
		/// Creates a new business entity PlaylistPage and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse PlaylistPage_FormCreate(PlaylistPage item) {
			return m_EnergyBusinessLayer.PlaylistPage_FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity PlaylistPage.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse PlaylistPage_FormLoad(string Key) {
			return m_EnergyBusinessLayer.PlaylistPage_FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity PlaylistPage and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse PlaylistPage_FormUpdate(PlaylistPage item) {
			return m_EnergyBusinessLayer.PlaylistPage_FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse PlaylistPage_FormUpdateBatch(string[] keys, PlaylistPage item) {
			return m_EnergyBusinessLayer.PlaylistPage_FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity PlaylistPage. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		public List<PlaylistPage> PlaylistPage_GetAll(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.PlaylistPage_GetAll(paging, location);
		}

		/// <summary>
		/// Gets a specific item for the business entity PlaylistPage.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public PlaylistPage PlaylistPage_GetItem(string Key) {
			return m_EnergyBusinessLayer.PlaylistPage_GetItem(Key);
		}

		/// <summary>
		/// Gets a json store for the business entity PlaylistPage.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<PlaylistPage> PlaylistPage_GetStore(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.PlaylistPage_GetStore(paging, location);
		}

		/// <summary>
		/// Gets a store of  PlaylistPage for a specific Playlist.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyPlaylist">The key of the Playlist.</param>
		/// <returns></returns>
		public JsonStore<PlaylistPage> PlaylistPage_GetStoreByKeyPlaylist(PagingParameter paging, string KeyPlaylist) {
			return m_EnergyBusinessLayer.PlaylistPage_GetStoreByKeyPlaylist(paging, KeyPlaylist);
		}

		/// <summary>
		/// Returns the stream of the image from a playlist page (corresponding to the latest generation of the playlist).
		/// </summary>
		/// <param name="Key">The key of the playlist page.</param>
		/// <returns></returns>
		public Stream PlaylistPage_GetStreamImage(string Key) {
			var stream = m_EnergyBusinessLayer.PlaylistPage_GetStreamImage(Key);
			WebOperationContext.Current.OutgoingResponse.ContentType = stream.MimeType;
			return stream.ContentStream;
		}

		/// <summary>
		/// Saves a crud store for the business entity PlaylistPage.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<PlaylistPage> PlaylistPage_SaveStore(CrudStore<PlaylistPage> store) {
			return m_EnergyBusinessLayer.PlaylistPage_SaveStore(store);
		}

		/// <summary>
		/// Deletes an existing business entity PortalColumn.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <param name="deleteContent">if set to <c>true</c> [delete content].</param>
		/// <returns></returns>
		public bool PortalColumn_Delete(PortalColumn item, bool deleteContent = false) {
			return m_EnergyBusinessLayer.PortalColumn_Delete(item, deleteContent);
		}

		/// <summary>
		/// Creates a new business entity PortalColumn and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <param name="portlet">the portlet crud store attached to this portal column.</param>
		/// <returns></returns>
		public FormResponse PortalColumn_FormCreate(PortalColumn item, CrudStore<Portlet> portlet) {
			return m_EnergyBusinessLayer.PortalColumn_FormCreate(item, portlet);
		}

		/// <summary>
		/// Loads a specific item for the business entity PortalColumn.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse PortalColumn_FormLoad(string Key) {
			return m_EnergyBusinessLayer.PortalColumn_FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity PortalColumn and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <param name="portlet">the portlet crud store attached to this portal column.</param>
		/// <returns></returns>
		public FormResponse PortalColumn_FormUpdate(PortalColumn item, CrudStore<Portlet> portlet) {
			return m_EnergyBusinessLayer.PortalColumn_FormUpdate(item, portlet);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse PortalColumn_FormUpdateBatch(string[] keys, PortalColumn item) {
			return m_EnergyBusinessLayer.PortalColumn_FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity PortalColumn. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		public List<PortalColumn> PortalColumn_GetAll(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.PortalColumn_GetAll(paging, location);
		}

		/// <summary>
		/// Gets a specific item for the business entity PortalColumn.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public PortalColumn PortalColumn_GetItem(string Key) {
			return m_EnergyBusinessLayer.PortalColumn_GetItem(Key);
		}

		/// <summary>
		/// Gets a list of portalcolumn for a specific portal tab.
		/// </summary>
		/// <param name="KeyPortalTab">The key of the portal tab.</param>
		/// <returns></returns>
		public List<PortalColumn> PortalColumn_GetListByKeyPortalTab(string KeyPortalTab) {
			return m_EnergyBusinessLayer.PortalColumn_GetListByKeyPortalTab(KeyPortalTab);
		}

		/// <summary>
		/// Gets a json store for the business entity PortalColumn.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<PortalColumn> PortalColumn_GetStore(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.PortalColumn_GetStore(paging, location);
		}

		/// <summary>
		/// Gets a json store of portalcolumn for a specific portal tab.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyPortalTab">The Key of the portal tab.</param>
		/// <returns>A json store.</returns>
		public JsonStore<PortalColumn> PortalColumn_GetStoreByKeyPortalTab(PagingParameter paging, string KeyPortalTab) {
			return m_EnergyBusinessLayer.PortalColumn_GetStoreByKeyPortalTab(paging, KeyPortalTab);
		}

		/// <summary>
		/// Saves a crud store for the business entity PortalColumn.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<PortalColumn> PortalColumn_SaveStore(CrudStore<PortalColumn> store) {
			return m_EnergyBusinessLayer.PortalColumn_SaveStore(store);
		}

		/// <summary>
		/// Apply the filter Spatial to all the Charts contained in the portal tab.
		/// </summary>
		/// <param name="Key">The portaltab Key.</param>
		/// <param name="filterSpatial">The spatial filter.</param>
		/// <returns></returns>
		public FormResponse PortalTab_ApplySpatialFilter(string Key, List<BaseBusinessEntity> filterSpatial) {
			return m_EnergyBusinessLayer.PortalTab_ApplySpatialFilter(Key, filterSpatial);
		}

		/// <summary>
		/// Apply the filter Spatial to all the Charts contained in all tabs of a portal window.
		/// </summary>
		/// <param name="Key">The portaltab Key.</param>
		/// <param name="filterSpatial">The spatial filter.</param>
		/// <returns></returns>
		public FormResponse PortalWindow_ApplySpatialFilter(string Key, List<BaseBusinessEntity> filterSpatial) {
			return m_EnergyBusinessLayer.PortalWindow_ApplySpatialFilter(Key, filterSpatial);
		}

		/// <summary>
		/// Copy existing business entity PortalTab and it s content.
		/// </summary>
		/// <param name="keys">The list of keys of item to copy </param>
		/// <returns></returns>
		public FormResponse PortalTab_Copy(string[] keys) {
			return m_EnergyBusinessLayer.PortalTab_Copy(keys);
		}

		/// <summary>
		/// Deletes an existing business entity PortalTab.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <param name="deleteContent">if set to <c>true</c> [delete content].</param>
		/// <returns></returns>
		public bool PortalTab_Delete(PortalTab item, bool deleteContent = false) {
			return m_EnergyBusinessLayer.PortalTab_Delete(item, deleteContent);
		}

		/// <summary>
		/// Creates a new business entity PortalTab and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <param name="portalcolumn">the portalcolumn crud store attached to this portal tab.</param>
		/// <returns></returns>
		public FormResponse PortalTab_FormCreate(PortalTab item, CrudStore<PortalColumn> portalcolumn) {
			return m_EnergyBusinessLayer.PortalTab_FormCreate(item, portalcolumn);
		}

		/// <summary>
		/// Loads a specific item for the business entity PortalTab.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse PortalTab_FormLoad(string Key) {
			return m_EnergyBusinessLayer.PortalTab_FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity PortalTab and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <param name="portalcolumn">the portalcolumn crud store attached to this portal tab.</param>
		/// <returns></returns>
		public FormResponse PortalTab_FormUpdate(PortalTab item, CrudStore<PortalColumn> portalcolumn) {
			return m_EnergyBusinessLayer.PortalTab_FormUpdate(item, portalcolumn);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse PortalTab_FormUpdateBatch(string[] keys, PortalTab item) {
			return m_EnergyBusinessLayer.PortalTab_FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity PortalTab. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		public List<PortalTab> PortalTab_GetAll(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.PortalTab_GetAll(paging, location);
		}

		/// <summary>
		/// Get the PortalTab total flex based on the columns type (Main,Header, Footer).
		/// </summary>
		/// <param name="KeyPortalTab">The key portal tab.</param>
		/// <param name="type">The type of column.</param>
		/// <returns></returns>
		public int PortalTab_GetFlex(string KeyPortalTab, PortalColumnType type) {
			return m_EnergyBusinessLayer.PortalTab_GetFlex(KeyPortalTab, type);
		}

		/// <summary>
		/// Gets a specific item for the business entity PortalTab.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public PortalTab PortalTab_GetItem(string Key, params string[] fields) {
			return m_EnergyBusinessLayer.PortalTab_GetItem(Key, fields);
		}

		/// <summary>
		/// Gets a list of portaltab for a specific portal.
		/// </summary>
		/// <param name="KeyPortalWindow">The key of the portal window.</param>
		/// <returns></returns>
		public List<PortalTab> PortalTab_GetListByKeyPortalWindow(string KeyPortalWindow) {
			return m_EnergyBusinessLayer.PortalTab_GetListByKeyPortalWindow(KeyPortalWindow);
		}

		/// <summary>
		/// Gets a json store for the business entity PortalTab.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<PortalTab> PortalTab_GetStore(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.PortalTab_GetStore(paging, location);
		}

		/// <summary>
		/// Gets a json store of portaltab for a specific portal.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyPortalWindow">The Key of the portal window.</param>
		/// <returns>A json store.</returns>
		public JsonStore<PortalTab> PortalTab_GetStoreByKeyPortalWindow(PagingParameter paging, string KeyPortalWindow) {
			return m_EnergyBusinessLayer.PortalTab_GetStoreByKeyPortalWindow(paging, KeyPortalWindow);
		}

		/// <summary>
		/// Returns the stream of the excel file from a portaltab.
		/// </summary>
		/// <param name="Key">The key of the PortalTab.</param>
		/// <returns></returns>
		public Stream PortalTab_GetStreamExcel(string Key) {
			StreamResult result = m_EnergyBusinessLayer.PortalTab_GetStreamExcel(Key);
			this.PrepareFileAttachment(result);
			return result.ContentStream;
		}

		/// <summary>
		/// Exports the PortalTab as an Excel File with a long running operation.
		/// </summary>
		/// <param name="operationId">The id of the operation.</param>
		/// <param name="Key">The key of the Chart.</param>
		public void PortalTab_GetStreamExcelBegin(Guid operationId, string Key) {
			m_EnergyBusinessLayer.PortalTab_GetStreamExcelBegin(operationId, Key);
		}

		/// <summary>
		/// Returns the stream of the image  from a portaltab.
		/// </summary>
		/// <param name="Key">The key of the portaltab.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <returns></returns>
		public Stream PortalTab_GetStreamImage(string Key, int width, int height) {
			var stream = m_EnergyBusinessLayer.PortalTab_GetStreamImage(Key, width, height);
			WebOperationContext.Current.OutgoingResponse.ContentType = stream.MimeType;
			return stream.ContentStream;
		}

		/// <summary>
		/// Returns the stream of the pdf file from a portaltab.
		/// </summary>
		/// <param name="Key">The key of the PortalTab.</param>
		/// <returns></returns>
		public Stream PortalTab_GetStreamPdf(string Key) {
			StreamResult result = m_EnergyBusinessLayer.PortalTab_GetStreamPdf(Key);
			this.PrepareFileAttachment(result);
			return result.ContentStream;
		}

		/// <summary>
		/// Exports the PortalTab as an Pdf File with a long running operation.
		/// </summary>
		/// <param name="operationId">The id of the operation.</param>
		/// <param name="Key">The key of the Chart.</param>
		public void PortalTab_GetStreamPdfBegin(Guid operationId, string Key) {
			m_EnergyBusinessLayer.PortalTab_GetStreamPdfBegin(operationId, Key);
		}

		/// <summary>
		/// Saves a crud store for the business entity PortalTab.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<PortalTab> PortalTab_SaveStore(CrudStore<PortalTab> store) {
			return m_EnergyBusinessLayer.PortalTab_SaveStore(store);
		}

		/// <summary>
		/// Deletes an existing business entity PortalTemplate.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool PortalTemplate_Delete(PortalTemplate item) {
			return m_EnergyBusinessLayer.PortalTemplate_Delete(item);
		}

		/// <summary>
		/// Creates a new business entity PortalTemplate and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <param name="portalwindows">The portalwindows.</param>
		/// <param name="users">The users.</param>
		/// <param name="filterSpatial">The filter spatial.</param>
		/// <returns></returns>
		public FormResponse PortalTemplate_FormCreate(PortalTemplate item, CrudStore<PortalWindow> portalwindows, CrudStore<FOLMembershipUser> users, CrudStore<Location> filterSpatial) {
			return m_EnergyBusinessLayer.PortalTemplate_FormCreate(item, portalwindows, users, filterSpatial);
		}

		/// <summary>
		/// Loads a specific item for the business entity PortalTemplate.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse PortalTemplate_FormLoad(string Key) {
			return m_EnergyBusinessLayer.PortalTemplate_FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity PortalTemplate and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <param name="portalwindows">The portalwindows.</param>
		/// <param name="users">The users.</param>
		/// <param name="filterSpatial">The filter spatial.</param>
		/// <returns></returns>
		public FormResponse PortalTemplate_FormUpdate(PortalTemplate item, CrudStore<PortalWindow> portalwindows, CrudStore<FOLMembershipUser> users, CrudStore<Location> filterSpatial) {
			return m_EnergyBusinessLayer.PortalTemplate_FormUpdate(item, portalwindows, users, filterSpatial);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse PortalTemplate_FormUpdateBatch(string[] keys, PortalTemplate item) {
			return m_EnergyBusinessLayer.PortalTemplate_FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity PortalTemplate. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		public List<PortalTemplate> PortalTemplate_GetAll(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.PortalTemplate_GetAll(paging, location);
		}

		/// <summary>
		/// Gets a specific item for the business entity PortalTemplate.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public PortalTemplate PortalTemplate_GetItem(string Key) {
			return m_EnergyBusinessLayer.PortalTemplate_GetItem(Key);
		}

		/// <summary>
		/// Gets a json store for the business entity PortalTemplate.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<PortalTemplate> PortalTemplate_GetStore(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.PortalTemplate_GetStore(paging, location);
		}

		/// <summary>
		/// Gets the PortalTemplate store by the job step.
		/// </summary>
		/// <param name="keyJob">The key job.</param>
		/// <param name="keyStep">The key step.</param>
		/// <param name="paging">The paging.</param>
		/// <param name="location">The location.</param>
		/// <returns></returns>
		public JsonStore<PortalTemplate> PortalTemplate_GetStoreByJobStep(string keyJob, string keyStep, PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.PortalTemplate_GetStoreByJobStep(keyJob, keyStep, paging, location);
		}

		/// <summary>
		/// Push the modification made to the associated portalwindows to the associated users of the PortalTemplate.
		/// </summary>
		/// <param name="KeyPortalTemplates">The key portal templates.</param>
        /// <param name="userKeys">The user keys.</param>
        /// <returns></returns>
        public bool PortalTemplate_PushUpdates(string[] KeyPortalTemplates, string[] userKeys = null) {
			return m_EnergyBusinessLayer.PortalTemplate_PushUpdates(KeyPortalTemplates, userKeys: userKeys);
		}

		/// <summary>
		///  Push the modification made to the associated portalwindows to the associated users of the PortalTemplaten with long running operation.
		/// </summary>
		/// <param name="operationId">The id of the operation.</param>
		/// <param name="Keys">The keys.</param>
        /// <param name="userKeys">The user keys.</param>
		public void PortalTemplate_PushUpdatesBegin(Guid operationId, string[] Keys, string[] userKeys = null) {
			m_EnergyBusinessLayer.PortalTemplate_PushUpdatesBegin(operationId, Keys, userKeys);
		}

		/// <summary>
		///  Delete a portaltemplate and the portalwindows to the associated users with long running operation.
		/// </summary>
		/// <param name="operationId">The id of the operation.</param>
		/// <param name="Keys">The keys.</param>
		public void PortalTemplate_DeleteBegin(Guid operationId, string[] Keys) {
			m_EnergyBusinessLayer.PortalTemplate_DeleteBegin(operationId, Keys);
		}

		/// <summary>
		/// Saves a crud store for the business entity PortalTemplate.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<PortalTemplate> PortalTemplate_SaveStore(CrudStore<PortalTemplate> store) {
			return m_EnergyBusinessLayer.PortalTemplate_SaveStore(store);
		}

        /// <summary>
        /// Gets a list of new user keys by key portal template.
        /// </summary>
        /// <param name="keyPortalTemplate">The keyPortalTemplate.</param>
        /// <returns></returns>
        public List<string> FOLMembershipUser_GetNewUserKeysByKeyPortalTemplate(int keyPortalTemplate) {
            return m_EnergyBusinessLayer.FOLMembershipUser_GetNewUserKeysByKeyPortalTemplate(keyPortalTemplate);
        }

		/// <summary>
		/// Copy existing business entity PortalWindow.
		/// </summary>
		/// <param name="keys">The list of keys of item to copy </param>
		/// <returns></returns>
		public FormResponse PortalWindow_Copy(string[] keys) {
			return m_EnergyBusinessLayer.PortalWindow_Copy(keys);
		}

		/// <summary>
		/// Deletes an existing business entity PortalWindow.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <param name="deleteContent">if set to <c>true</c> [delete content].</param>
		/// <returns></returns>
		public bool PortalWindow_Delete(PortalWindow item, bool deleteContent = false) {
			return m_EnergyBusinessLayer.PortalWindow_Delete(item, deleteContent);
		}

		/// <summary>
		/// Creates a new business entity PortalWindow and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <param name="portaltab">the portaltab crud store attached to this portal.</param>
		/// <returns></returns>
		public FormResponse PortalWindow_FormCreate(PortalWindow item, CrudStore<PortalTab> portaltab) {
			return m_EnergyBusinessLayer.PortalWindow_FormCreate(item, portaltab);
		}

		/// <summary>
		/// Loads a specific item for the business entity PortalWindow.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse PortalWindow_FormLoad(string Key) {
			return m_EnergyBusinessLayer.PortalWindow_FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity PortalWindow and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <param name="portaltab">the portaltab crud store attached to this portal.</param>
		/// <returns></returns>
		public FormResponse PortalWindow_FormUpdate(PortalWindow item, CrudStore<PortalTab> portaltab) {
			return m_EnergyBusinessLayer.PortalWindow_FormUpdate(item, portaltab);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse PortalWindow_FormUpdateBatch(string[] keys, PortalWindow item) {
			return m_EnergyBusinessLayer.PortalWindow_FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity PortalWindow. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		public List<PortalWindow> PortalWindow_GetAll(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.PortalWindow_GetAll(paging, location);
		}

		/// <summary>
		/// Gets a specific item for the business entity PortalWindow.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public PortalWindow PortalWindow_GetItem(string Key) {
			return m_EnergyBusinessLayer.PortalWindow_GetItem(Key);
		}

		/// <summary>
		/// Gets a specific item for the business entity PortalWindow.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public PortalWindow PortalWindow_GetPortal(string Key, params string[] fields) {
			return m_EnergyBusinessLayer.PortalWindow_GetItem(Key, "GetPortal");
		}

		/// <summary>
		/// Gets a json store for the business entity PortalWindow.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<PortalWindow> PortalWindow_GetStore(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.PortalWindow_GetStore(paging, location);
		}

		/// <summary>
		/// Gets a json store for the business entity PortalWindow_ for a specific ChartScheduler.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyChartScheduler">The key chart scheduler.</param>
		/// <returns>
		/// A json store.
		/// </returns>
		public JsonStore<PortalWindow> PortalWindow_GetStoreByKeyChartScheduler(PagingParameter paging, string KeyChartScheduler) {
			return m_EnergyBusinessLayer.PortalWindow_GetStoreByKeyChartScheduler(paging, KeyChartScheduler);
		}

		/// <summary>
		/// Gets a json store for the business entity PortalWindow for a specific PortalTemplate.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyPortalTemplate">The Key PortalTemplate.</param>
		/// <returns>
		/// A json store.
		/// </returns>
		public JsonStore<PortalWindow> PortalWindow_GetStoreByKeyPortalTemplate(PagingParameter paging, string KeyPortalTemplate) {
			return m_EnergyBusinessLayer.PortalWindow_GetStoreByKeyPortalTemplate(paging, KeyPortalTemplate);
		}

		/// <summary>
		/// Returns the stream of the excel file from a portalWindow.
		/// </summary>
		/// <param name="Key">The key of the PortalWindow.</param>
		/// <returns></returns>
		public Stream PortalWindow_GetStreamExcel(string Key) {
			StreamResult result = m_EnergyBusinessLayer.PortalWindow_GetStreamExcel(Key);
			this.PrepareFileAttachment(result);
			return result.ContentStream;
		}

		/// <summary>
		/// Exports the PortalWindow as an Excel File with a long running operation.
		/// </summary>
		/// <param name="operationId">The id of the operation.</param>
		/// <param name="Key">The key of the PortalWindow.</param>
		public void PortalWindow_GetStreamExcelBegin(Guid operationId, string Key) {
			m_EnergyBusinessLayer.PortalWindow_GetStreamExcelBegin(operationId, Key);
		}

		/// <summary>
		/// Returns the stream of the pdf file from a portalWindow.
		/// </summary>
		/// <param name="Key">The key of the PortalWindow.</param>
		/// <returns></returns>
		public Stream PortalWindow_GetStreamPdf(string Key) {
			StreamResult result = m_EnergyBusinessLayer.PortalWindow_GetStreamPdf(Key);
			this.PrepareFileAttachment(result);
			return result.ContentStream;
		}

		/// <summary>
		/// Exports the PortalWindow as an Pdf File with a long running operation.
		/// </summary>
		/// <param name="operationId">The id of the operation.</param>
		/// <param name="Key">The key of the PortalWindow.</param>
		public void PortalWindow_GetStreamPdfBegin(Guid operationId, string Key) {
			m_EnergyBusinessLayer.PortalWindow_GetStreamPdfBegin(operationId, Key);
		}

		/// <summary>
		/// Saves a crud store for the business entity PortalWindow.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<PortalWindow> PortalWindow_SaveStore(CrudStore<PortalWindow> store) {
			return m_EnergyBusinessLayer.PortalWindow_SaveStore(store);
		}

		/// <summary>
		/// Update the PortalWindow desktop shortcut position.
		/// </summary>
		/// <param name="KeyPortalWindow">The PortalWindow key.</param>
		/// <param name="desktopShortcutIconX">The desktop shortcut icon X.</param>
		/// <param name="desktopShortcutIconY">The desktop shortcut icon Y.</param>
		/// <returns></returns>
		public FormResponse PortalWindow_UpdateDesktopShortcut(string KeyPortalWindow, int desktopShortcutIconX, int desktopShortcutIconY) {
			return m_EnergyBusinessLayer.PortalWindow_UpdateDesktopShortcut(KeyPortalWindow, desktopShortcutIconX, desktopShortcutIconY);
		}

		/// <summary>
		/// Copy an existing Portlet.
		/// </summary>
		/// <param name="keys">The item to copy.</param>
		/// <returns></returns>
		public FormResponse Portlet_Copy(string[] keys) {
			return m_EnergyBusinessLayer.Portlet_Copy(keys);
		}

		/// <summary>
		/// Create a Portlet from a ClassificationItem (used in the drag drop of a ClassificationItem in a portal column).
		/// </summary>
		/// <param name="KeyClassificationItem">The key classification item.</param>
		/// <param name="KeyPortalColumn">The key portal column.</param>
		/// <param name="Order">The order.</param>
		/// <param name="TimeZoneId">The time zone id.</param>
		/// <param name="KeyChartToCopy"></param>
		/// <returns></returns>
		public Portlet Portlet_CreateFromClassification(string KeyClassificationItem, string KeyPortalColumn, int Order, string TimeZoneId, string KeyChartToCopy) {
			return m_EnergyBusinessLayer.Portlet_CreateFromClassification(KeyClassificationItem, KeyPortalColumn, Order, TimeZoneId, KeyChartToCopy);
		}

		/// <summary>
		/// Create a Portlet from a Location (used in the drag drop of a Location in a portal column).
		/// </summary>
		/// <param name="KeyLocation">The key location.</param>
		/// <param name="KeyPortalColumn">The key portal column.</param>
		/// <param name="Order">The order.</param>
		/// <param name="TimeZoneId">The time zone id.</param>
		/// <param name="KeyChartToCopy"></param>
		/// <returns></returns>
		public Portlet Portlet_CreateFromLocation(string KeyLocation, string KeyPortalColumn, int Order, string TimeZoneId, string KeyChartToCopy) {
			return m_EnergyBusinessLayer.Portlet_CreateFromLocation(KeyLocation, KeyPortalColumn, Order, TimeZoneId, KeyChartToCopy);
		}

		/// <summary>
		/// Create a Portlet from a Meter (used in the drag drop of a meter in a portal column).
		/// </summary>
		/// <param name="KeyMeter">the Key Meter.</param>
		/// <param name="KeyPortalColumn">The key portal column.</param>
		/// <param name="Order">The order.</param>
		/// <param name="TimeZoneId">The time zone id.</param>
		/// <param name="KeyChartToCopy"></param>
		/// <returns></returns>
		public Portlet Portlet_CreateFromMeter(string KeyMeter, string KeyPortalColumn, int Order, string TimeZoneId, string KeyChartToCopy) {
			return m_EnergyBusinessLayer.Portlet_CreateFromMeter(KeyMeter, KeyPortalColumn, Order, TimeZoneId, KeyChartToCopy);
		}

		/// <summary>
		/// Deletes an existing business entity Portlet.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <param name="deleteContent">if set to <c>true</c> [delete content].</param>
		/// <returns></returns>
		public bool Portlet_Delete(Portlet item, bool deleteContent = false) {
			return m_EnergyBusinessLayer.Portlet_Delete(item, deleteContent);
		}

		/// <summary>
		/// Creates a new business entity Portlet and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse Portlet_FormCreate(Portlet item) {
			return m_EnergyBusinessLayer.Portlet_FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity Portlet.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse Portlet_FormLoad(string Key) {
			return m_EnergyBusinessLayer.Portlet_FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity Portlet and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse Portlet_FormUpdate(Portlet item) {
			return m_EnergyBusinessLayer.Portlet_FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse Portlet_FormUpdateBatch(string[] keys, Portlet item) {
			return m_EnergyBusinessLayer.Portlet_FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity Portlet. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		public List<Portlet> Portlet_GetAll(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.Portlet_GetAll(paging, location);
		}

		/// <summary>
		///Gets all the open on start up portets.
		/// </summary>
		/// <returns></returns>
		public List<BaseBusinessEntity> Portlet_GetAllOpenOnStartup() {
			return m_EnergyBusinessLayer.Portlet_GetAllOpenOnStartup();
		}

		/// <summary>
		/// Gets a specific item for the business entity Portlet.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public Portlet Portlet_GetItem(string Key) {
			return m_EnergyBusinessLayer.Portlet_GetItem(Key);
		}

		/// <summary>
		/// Gets a list of portlet for a specific portal column.
		/// </summary>
		/// <param name="KeyPortalColumn">The key of the portal column.</param>
		/// <returns></returns>
		public List<Portlet> Portlet_GetListByKeyPortalColumn(string KeyPortalColumn) {
			return m_EnergyBusinessLayer.Portlet_GetListByKeyPortalColumn(KeyPortalColumn);
		}

		/// <summary>
		/// Gets a json store for the business entity Portlet.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<Portlet> Portlet_GetStore(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.Portlet_GetStore(paging, location);
		}

		/// <summary>
		/// Gets a store of portlet for a specific entity.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyEntity">The key entity.</param>
		/// <param name="TypeEntity">The type entity.</param>
		/// <returns></returns>
		public JsonStore<Portlet> Portlet_GetStoreByKeyEntity(PagingParameter paging, string KeyEntity, string TypeEntity) {
			return m_EnergyBusinessLayer.Portlet_GetStoreByKeyEntity(paging, KeyEntity, TypeEntity);

		}

		/// <summary>
		/// Gets a json store of portlet for a specific portal column.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyPortalColumn">The Key of the portal column.</param>
		/// <returns>A json store.</returns>
		public JsonStore<Portlet> Portlet_GetStoreByKeyPortalColumn(PagingParameter paging, string KeyPortalColumn) {
			return m_EnergyBusinessLayer.Portlet_GetStoreByKeyPortalColumn(paging, KeyPortalColumn);
		}

		/// <summary>
		/// Generates the json structure of the Energy available portlets.
		/// </summary>
		/// <param name="Key">The Key of the parent node.</param>
		/// <param name="entity">The entity.</param>
		/// <returns>
		/// a list of treenodes in json.
		/// </returns>
		public List<TreeNode> Portlet_GetTree(string Key, BaseBusinessEntity entity) {
			return m_EnergyBusinessLayer.Portlet_GetTree(Key, entity);
		}

		/// <summary>
		/// Saves a crud store for the business entity Portlet.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<Portlet> Portlet_SaveStore(CrudStore<Portlet> store) {
			return m_EnergyBusinessLayer.Portlet_SaveStore(store);
		}

		/// <summary>
		/// Deletes an existing business entity PsetAttributeHistoricalEndpoint.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool PsetAttributeHistoricalEndpoint_Delete(PsetAttributeHistoricalEndpoint item) {
			return m_EnergyBusinessLayer.PsetAttributeHistoricalEndpoint_Delete(item);
		}

		/// <summary>
		/// Creates a new business entity PsetAttributeHistoricalEndpoint and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse PsetAttributeHistoricalEndpoint_FormCreate(PsetAttributeHistoricalEndpoint item) {
			return m_EnergyBusinessLayer.PsetAttributeHistoricalEndpoint_FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity PsetAttributeHistoricalEndpoint.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse PsetAttributeHistoricalEndpoint_FormLoad(string Key) {
			return m_EnergyBusinessLayer.PsetAttributeHistoricalEndpoint_FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity PsetAttributeHistoricalEndpoint and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse PsetAttributeHistoricalEndpoint_FormUpdate(PsetAttributeHistoricalEndpoint item) {
			return m_EnergyBusinessLayer.PsetAttributeHistoricalEndpoint_FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse PsetAttributeHistoricalEndpoint_FormUpdateBatch(string[] keys, PsetAttributeHistoricalEndpoint item) {
			return m_EnergyBusinessLayer.PsetAttributeHistoricalEndpoint_FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity PsetAttributeHistoricalEndpoint. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		public List<PsetAttributeHistoricalEndpoint> PsetAttributeHistoricalEndpoint_GetAll(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.PsetAttributeHistoricalEndpoint_GetAll(paging, location);
		}

		/// <summary>
		/// Gets a specific item for the business entity PsetAttributeHistoricalEndpoint.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public PsetAttributeHistoricalEndpoint PsetAttributeHistoricalEndpoint_GetItem(string Key) {
			return m_EnergyBusinessLayer.PsetAttributeHistoricalEndpoint_GetItem(Key);
		}

		/// <summary>
		///  Get the  PsetAttributeHistoricalEndpoint for a specific Object.
		/// </summary>
		/// <param name="KeyObject">The key object.</param>
		/// <param name="KeyPropertySingleValue">The key property single value.</param>
		/// <returns></returns>
		public PsetAttributeHistoricalEndpoint PsetAttributeHistoricalEndpoint_GetItemByKeyObject(string KeyObject, string KeyPropertySingleValue) {
			return m_EnergyBusinessLayer.PsetAttributeHistoricalEndpoint_GetItemByKeyObject(KeyObject, KeyPropertySingleValue);
		}

		/// <summary>
		/// Gets a json store for the business entity PsetAttributeHistoricalEndpoint.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<PsetAttributeHistoricalEndpoint> PsetAttributeHistoricalEndpoint_GetStore(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.PsetAttributeHistoricalEndpoint_GetStore(paging, location);
		}

		/// <summary>
		/// Saves a crud store for the business entity PsetAttributeHistoricalEndpoint.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<PsetAttributeHistoricalEndpoint> PsetAttributeHistoricalEndpoint_SaveStore(CrudStore<PsetAttributeHistoricalEndpoint> store) {
			return m_EnergyBusinessLayer.PsetAttributeHistoricalEndpoint_SaveStore(store);
		}

		/// <summary>
		/// Deletes an existing business entity RESTDataAcquisitionContainer.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool RESTDataAcquisitionContainer_Delete(RESTDataAcquisitionContainer item) {
			return m_EnergyBusinessLayer.RESTDataAcquisitionContainer_Delete(item);
		}

		/// <summary>
		/// Creates a new business entity RESTDataAcquisitionContainer and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse RESTDataAcquisitionContainer_FormCreate(RESTDataAcquisitionContainer item) {
			return m_EnergyBusinessLayer.RESTDataAcquisitionContainer_FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity RESTDataAcquisitionContainer.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse RESTDataAcquisitionContainer_FormLoad(string Key) {
			return m_EnergyBusinessLayer.RESTDataAcquisitionContainer_FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity RESTDataAcquisitionContainer and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse RESTDataAcquisitionContainer_FormUpdate(RESTDataAcquisitionContainer item) {
			return m_EnergyBusinessLayer.RESTDataAcquisitionContainer_FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse RESTDataAcquisitionContainer_FormUpdateBatch(string[] keys, RESTDataAcquisitionContainer item) {
			return m_EnergyBusinessLayer.RESTDataAcquisitionContainer_FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity RESTDataAcquisitionContainer. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		public List<RESTDataAcquisitionContainer> RESTDataAcquisitionContainer_GetAll(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.RESTDataAcquisitionContainer_GetAll(paging, location);
		}

		/// <summary>
		/// Gets a specific item for the business entity RESTDataAcquisitionContainer.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public RESTDataAcquisitionContainer RESTDataAcquisitionContainer_GetItem(string Key) {
			return m_EnergyBusinessLayer.RESTDataAcquisitionContainer_GetItem(Key);
		}

		/// <summary>
		/// Gets a json store for the business entity RESTDataAcquisitionContainer.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<RESTDataAcquisitionContainer> RESTDataAcquisitionContainer_GetStore(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.RESTDataAcquisitionContainer_GetStore(paging, location);
		}

		/// <summary>
		/// Saves a crud store for the business entity RESTDataAcquisitionContainer.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<RESTDataAcquisitionContainer> RESTDataAcquisitionContainer_SaveStore(CrudStore<RESTDataAcquisitionContainer> store) {
			return m_EnergyBusinessLayer.RESTDataAcquisitionContainer_SaveStore(store);
		}

		/// <summary>
		/// Deletes an existing business entity RESTDataAcquisitionEndpoint.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool RESTDataAcquisitionEndpoint_Delete(RESTDataAcquisitionEndpoint item) {
			return m_EnergyBusinessLayer.RESTDataAcquisitionEndpoint_Delete(item);
		}

		/// <summary>
		/// Creates a new business entity RESTDataAcquisitionEndpoint and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse RESTDataAcquisitionEndpoint_FormCreate(RESTDataAcquisitionEndpoint item) {
			return m_EnergyBusinessLayer.RESTDataAcquisitionEndpoint_FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity RESTDataAcquisitionEndpoint.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse RESTDataAcquisitionEndpoint_FormLoad(string Key) {
			return m_EnergyBusinessLayer.RESTDataAcquisitionEndpoint_FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity RESTDataAcquisitionEndpoint and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse RESTDataAcquisitionEndpoint_FormUpdate(RESTDataAcquisitionEndpoint item) {
			return m_EnergyBusinessLayer.RESTDataAcquisitionEndpoint_FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse RESTDataAcquisitionEndpoint_FormUpdateBatch(string[] keys, RESTDataAcquisitionEndpoint item) {
			return m_EnergyBusinessLayer.RESTDataAcquisitionEndpoint_FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity RESTDataAcquisitionEndpoint. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		public List<RESTDataAcquisitionEndpoint> RESTDataAcquisitionEndpoint_GetAll(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.RESTDataAcquisitionEndpoint_GetAll(paging, location);
		}

		/// <summary>
		/// Gets a specific item for the business entity RESTDataAcquisitionEndpoint.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public RESTDataAcquisitionEndpoint RESTDataAcquisitionEndpoint_GetItem(string Key) {
			return m_EnergyBusinessLayer.RESTDataAcquisitionEndpoint_GetItem(Key);
		}

		/// <summary>
		/// Gets a json store for the business entity RESTDataAcquisitionEndpoint.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<RESTDataAcquisitionEndpoint> RESTDataAcquisitionEndpoint_GetStore(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.RESTDataAcquisitionEndpoint_GetStore(paging, location);
		}

		/// <summary>
		/// Gets a store of  RESTDataAcquisitionEndpoint by key of the container.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="KeyDataAcquisitionContainer">The key data acquisition container.</param>
		/// <returns></returns>
		public JsonStore<RESTDataAcquisitionEndpoint> RESTDataAcquisitionEndpoint_GetStoreByKeyDataAcquisitionContainer(PagingParameter paging, string KeyDataAcquisitionContainer) {
			var retVal = m_EnergyBusinessLayer.RESTDataAcquisitionEndpoint_GetStoreByKeyDataAcquisitionContainer(paging, KeyDataAcquisitionContainer);
			return retVal;
		}

		/// <summary>
		/// Saves a crud store for the business entity RESTDataAcquisitionEndpoint.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<RESTDataAcquisitionEndpoint> RESTDataAcquisitionEndpoint_SaveStore(CrudStore<RESTDataAcquisitionEndpoint> store) {
			return m_EnergyBusinessLayer.RESTDataAcquisitionEndpoint_SaveStore(store);
		}

		/// <summary>
		/// Return a store of available REST DataAcquisition Provider.
		/// </summary>
		/// <returns></returns>
		public JsonStore<ListElement> RESTDataAcquisitionProvider_GetStore() {
			return m_EnergyBusinessLayer.RESTDataAcquisitionProvider_GetStore();
		}

		/// <summary>
		/// Deletes an existing business entity StatisticalSerie.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool StatisticalSerie_Delete(StatisticalSerie item) {
			return m_EnergyBusinessLayer.StatisticalSerie_Delete(item);
		}

		/// <summary>
		/// Creates a new business entity StatisticalSerie and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse StatisticalSerie_FormCreate(StatisticalSerie item) {
			return m_EnergyBusinessLayer.StatisticalSerie_FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity StatisticalSerie.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse StatisticalSerie_FormLoad(string Key) {
			return m_EnergyBusinessLayer.StatisticalSerie_FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity StatisticalSerie and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse StatisticalSerie_FormUpdate(StatisticalSerie item) {
			return m_EnergyBusinessLayer.StatisticalSerie_FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse StatisticalSerie_FormUpdateBatch(string[] keys, StatisticalSerie item) {
			return m_EnergyBusinessLayer.StatisticalSerie_FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity StatisticalSerie. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		public List<StatisticalSerie> StatisticalSerie_GetAll(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.StatisticalSerie_GetAll(paging, location);
		}

		/// <summary>
		/// Gets a specific item for the business entity StatisticalSerie.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public StatisticalSerie StatisticalSerie_GetItem(string Key) {
			return m_EnergyBusinessLayer.StatisticalSerie_GetItem(Key);
		}

		/// <summary>
		/// Gets a json store for the business entity StatisticalSerie.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<StatisticalSerie> StatisticalSerie_GetStore(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.StatisticalSerie_GetStore(paging, location);
		}

		/// <summary>
		/// Gets a store of StatisticalSeries for a specific chart.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyChart">The key of the chart.</param>
		/// <returns></returns>
		public JsonStore<StatisticalSerie> StatisticalSerie_GetStoreByKeyChart(PagingParameter paging, string KeyChart) {
			return m_EnergyBusinessLayer.StatisticalSerie_GetStoreByKeyChart(paging, KeyChart);
		}

		/// <summary>
		/// Saves a crud store for the business entity StatisticalSerie.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<StatisticalSerie> StatisticalSerie_SaveStore(CrudStore<StatisticalSerie> store) {
			return m_EnergyBusinessLayer.StatisticalSerie_SaveStore(store);
		}

		/// <summary>
		/// Deletes an existing business entity StruxureWareDataAcquisitionContainer.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool StruxureWareDataAcquisitionContainer_Delete(StruxureWareDataAcquisitionContainer item) {
			return m_EnergyBusinessLayer.StruxureWareDataAcquisitionContainer_Delete(item);
		}

		/// <summary>
		/// Creates a new business entity StruxureWareDataAcquisitionContainer and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse StruxureWareDataAcquisitionContainer_FormCreate(StruxureWareDataAcquisitionContainer item) {
			return m_EnergyBusinessLayer.StruxureWareDataAcquisitionContainer_FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity StruxureWareDataAcquisitionContainer.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse StruxureWareDataAcquisitionContainer_FormLoad(string Key) {
			return m_EnergyBusinessLayer.StruxureWareDataAcquisitionContainer_FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity StruxureWareDataAcquisitionContainer and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse StruxureWareDataAcquisitionContainer_FormUpdate(StruxureWareDataAcquisitionContainer item) {
			return m_EnergyBusinessLayer.StruxureWareDataAcquisitionContainer_FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse StruxureWareDataAcquisitionContainer_FormUpdateBatch(string[] keys, StruxureWareDataAcquisitionContainer item) {
			return m_EnergyBusinessLayer.StruxureWareDataAcquisitionContainer_FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity StruxureWareDataAcquisitionContainer. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		public List<StruxureWareDataAcquisitionContainer> StruxureWareDataAcquisitionContainer_GetAll(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.StruxureWareDataAcquisitionContainer_GetAll(paging, location);
		}

		/// <summary>
		/// Gets a specific item for the business entity StruxureWareDataAcquisitionContainer.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public StruxureWareDataAcquisitionContainer StruxureWareDataAcquisitionContainer_GetItem(string Key) {
			return m_EnergyBusinessLayer.StruxureWareDataAcquisitionContainer_GetItem(Key);
		}

		/// <summary>
		/// Gets a json store for the business entity StruxureWareDataAcquisitionContainer.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<StruxureWareDataAcquisitionContainer> StruxureWareDataAcquisitionContainer_GetStore(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.StruxureWareDataAcquisitionContainer_GetStore(paging, location);
		}

		/// <summary>
		/// Saves a crud store for the business entity StruxureWareDataAcquisitionContainer.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<StruxureWareDataAcquisitionContainer> StruxureWareDataAcquisitionContainer_SaveStore(CrudStore<StruxureWareDataAcquisitionContainer> store) {
			return m_EnergyBusinessLayer.StruxureWareDataAcquisitionContainer_SaveStore(store);
		}

		/// <summary>
		/// Deletes an existing business entity StruxureWareDataAcquisitionEndpoint.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool StruxureWareDataAcquisitionEndpoint_Delete(StruxureWareDataAcquisitionEndpoint item) {
			return m_EnergyBusinessLayer.StruxureWareDataAcquisitionEndpoint_Delete(item);
		}

		/// <summary>
		/// Creates a new business entity StruxureWareDataAcquisitionEndpoint and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse StruxureWareDataAcquisitionEndpoint_FormCreate(StruxureWareDataAcquisitionEndpoint item) {
			return m_EnergyBusinessLayer.StruxureWareDataAcquisitionEndpoint_FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity StruxureWareDataAcquisitionEndpoint.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse StruxureWareDataAcquisitionEndpoint_FormLoad(string Key) {
			return m_EnergyBusinessLayer.StruxureWareDataAcquisitionEndpoint_FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity StruxureWareDataAcquisitionEndpoint and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse StruxureWareDataAcquisitionEndpoint_FormUpdate(StruxureWareDataAcquisitionEndpoint item) {
			return m_EnergyBusinessLayer.StruxureWareDataAcquisitionEndpoint_FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse StruxureWareDataAcquisitionEndpoint_FormUpdateBatch(string[] keys, StruxureWareDataAcquisitionEndpoint item) {
			return m_EnergyBusinessLayer.StruxureWareDataAcquisitionEndpoint_FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity StruxureWareDataAcquisitionEndpoint. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		public List<StruxureWareDataAcquisitionEndpoint> StruxureWareDataAcquisitionEndpoint_GetAll(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.StruxureWareDataAcquisitionEndpoint_GetAll(paging, location);
		}

		/// <summary>
		/// Gets a specific item for the business entity StruxureWareDataAcquisitionEndpoint.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public StruxureWareDataAcquisitionEndpoint StruxureWareDataAcquisitionEndpoint_GetItem(string Key) {
			return m_EnergyBusinessLayer.StruxureWareDataAcquisitionEndpoint_GetItem(Key);
		}

		/// <summary>
		/// Gets a json store for the business entity StruxureWareDataAcquisitionEndpoint.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<StruxureWareDataAcquisitionEndpoint> StruxureWareDataAcquisitionEndpoint_GetStore(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.StruxureWareDataAcquisitionEndpoint_GetStore(paging, location);
		}

		/// <summary>
		/// Gets a store of  StruxureWareDataAcquisitionEndpoint by key of the container.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="KeyDataAcquisitionContainer">The key data acquisition container.</param>
		/// <returns></returns>
		public JsonStore<StruxureWareDataAcquisitionEndpoint> StruxureWareDataAcquisitionEndpoint_GetStoreByKeyDataAcquisitionContainer(PagingParameter paging, string KeyDataAcquisitionContainer) {
			return m_EnergyBusinessLayer.StruxureWareDataAcquisitionEndpoint_GetStoreByKeyDataAcquisitionContainer(paging, KeyDataAcquisitionContainer);
		}

		/// <summary>
		/// Saves a crud store for the business entity StruxureWareDataAcquisitionEndpoint.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<StruxureWareDataAcquisitionEndpoint> StruxureWareDataAcquisitionEndpoint_SaveStore(CrudStore<StruxureWareDataAcquisitionEndpoint> store) {
			return m_EnergyBusinessLayer.StruxureWareDataAcquisitionEndpoint_SaveStore(store);
		}

		/// <summary>
		/// Deletes an existing business entity VirtualFile.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool VirtualFile_Delete(VirtualFile item) {
			return m_EnergyBusinessLayer.VirtualFile_Delete(item);
		}

		/// <summary>
		/// Creates a new business entity VirtualFile and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse VirtualFile_FormCreate(VirtualFile item) {
			return m_EnergyBusinessLayer.VirtualFile_FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity VirtualFile.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse VirtualFile_FormLoad(string Key) {
			return m_EnergyBusinessLayer.VirtualFile_FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity VirtualFile and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse VirtualFile_FormUpdate(VirtualFile item) {
			return m_EnergyBusinessLayer.VirtualFile_FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse VirtualFile_FormUpdateBatch(string[] keys, VirtualFile item) {
			return m_EnergyBusinessLayer.VirtualFile_FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity VirtualFile. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		public List<VirtualFile> VirtualFile_GetAll(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.VirtualFile_GetAll(paging, location);
		}

		/// <summary>
		/// Gets a specific item for the business entity VirtualFile.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public VirtualFile VirtualFile_GetItem(string Key) {
			return m_EnergyBusinessLayer.VirtualFile_GetItem(Key);
		}

		/// <summary>
		/// Gets a json store for the business entity VirtualFile.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<VirtualFile> VirtualFile_GetStore(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.VirtualFile_GetStore(paging, location);
		}

		/// <summary>
		/// Return a stream from a VirtualFile Path.
		/// </summary>
		/// <param name="direct">the path.(the name of the attribute is the url prefix to access the stream ie : /direct/path)</param>
		/// <returns></returns>
		public Stream VirtualFile_GetStreamImage(string direct) {
			StreamResult result = m_EnergyBusinessLayer.VirtualFile_GetStreamImage(direct);
			WebOperationContext.Current.OutgoingResponse.ContentType = result.MimeType;
			return result.ContentStream;
		}

		/// <summary>
		/// Saves a crud store for the business entity VirtualFile.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<VirtualFile> VirtualFile_SaveStore(CrudStore<VirtualFile> store) {
			return m_EnergyBusinessLayer.VirtualFile_SaveStore(store);
		}

		/// <summary>
		/// Deletes an existing business entity WeatherLocation.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool WeatherLocation_Delete(WeatherLocation item) {
			return m_EnergyBusinessLayer.WeatherLocation_Delete(item);
		}

		/// <summary>
		/// Creates a new business entity WeatherLocation and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse WeatherLocation_FormCreate(WeatherLocation item) {
			return m_EnergyBusinessLayer.WeatherLocation_FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity WeatherLocation.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse WeatherLocation_FormLoad(string Key) {
			return m_EnergyBusinessLayer.WeatherLocation_FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity WeatherLocation and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse WeatherLocation_FormUpdate(WeatherLocation item) {
			return m_EnergyBusinessLayer.WeatherLocation_FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse WeatherLocation_FormUpdateBatch(string[] keys, WeatherLocation item) {
			return m_EnergyBusinessLayer.WeatherLocation_FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity WeatherLocation. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		public List<WeatherLocation> WeatherLocation_GetAll(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.WeatherLocation_GetAll(paging, location);
		}

		/// <summary>
		/// Returns the 5 day Forecast for this Weather Location.
		/// </summary>
		/// <param name="KeyWeatherLocation">The WeatherLocation key.</param>
		/// <returns></returns>
		public List<WeatherForecast> WeatherLocation_GetFiveDayForecast(string KeyWeatherLocation) {
			return m_EnergyBusinessLayer.WeatherLocation_GetFiveDayForecast(KeyWeatherLocation);
		}

		/// <summary>
		/// Gets a specific item for the business entity WeatherLocation.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public WeatherLocation WeatherLocation_GetItem(string Key) {
			return m_EnergyBusinessLayer.WeatherLocation_GetItem(Key);
		}

		/// <summary>
		/// Gets a json store for the business entity WeatherLocation.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<WeatherLocation> WeatherLocation_GetStore(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.WeatherLocation_GetStore(paging, location);
		}

		/// <summary>
		/// Saves a crud store for the business entity WeatherLocation.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<WeatherLocation> WeatherLocation_SaveStore(CrudStore<WeatherLocation> store) {
			return m_EnergyBusinessLayer.WeatherLocation_SaveStore(store);
		}

		/// <summary>
		/// Gets a json store of WeatherLocation based on a specific search.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <returns>
		/// A json store.
		/// </returns>
		public JsonStore<WeatherLocation> WeatherLocationSearch_GetStore(PagingParameter paging) {
			return m_EnergyBusinessLayer.WeatherLocationSearch_GetStore(paging);
		}

		/// <summary>
		/// Deletes an existing business entity WebFrame.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool WebFrame_Delete(WebFrame item) {
			return m_EnergyBusinessLayer.WebFrame_Delete(item);
		}

		/// <summary>
		/// Creates a new business entity WebFrame and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse WebFrame_FormCreate(WebFrame item) {
			return m_EnergyBusinessLayer.WebFrame_FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity WebFrame.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse WebFrame_FormLoad(string Key) {
			return m_EnergyBusinessLayer.WebFrame_FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity WebFrame and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse WebFrame_FormUpdate(WebFrame item) {
			return m_EnergyBusinessLayer.WebFrame_FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse WebFrame_FormUpdateBatch(string[] keys, WebFrame item) {
			return m_EnergyBusinessLayer.WebFrame_FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity WebFrame. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		public List<WebFrame> WebFrame_GetAll(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.WebFrame_GetAll(paging, location);
		}

		/// <summary>
		/// Gets a specific item for the business entity WebFrame.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public WebFrame WebFrame_GetItem(string Key) {
			return m_EnergyBusinessLayer.WebFrame_GetItem(Key);
		}

		/// <summary>
		/// Gets a json store for the business entity WebFrame.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<WebFrame> WebFrame_GetStore(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.WebFrame_GetStore(paging, location);
		}

		/// <summary>
		/// Saves a crud store for the business entity WebFrame.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<WebFrame> WebFrame_SaveStore(CrudStore<WebFrame> store) {
			return m_EnergyBusinessLayer.WebFrame_SaveStore(store);
		}




		/// <summary>
		/// Gets a json store for the business entity Algorithm.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<Algorithm> Algorithm_GetStore(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.Algorithm_GetStore(paging, location);
		}

		/// <summary>
		/// Saves a crud store for the business entity Algorithm.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<Algorithm> Algorithm_SaveStore(CrudStore<Algorithm> store) {
			return m_EnergyBusinessLayer.Algorithm_SaveStore(store);
		}

		/// <summary>
		/// Gets a list for the business entity Algorithm. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		public List<Algorithm> Algorithm_GetAll(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.Algorithm_GetAll(paging, location);
		}

		/// <summary>
		/// Gets a specific item for the business entity Algorithm.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public Algorithm Algorithm_GetItem(string Key) {
			return m_EnergyBusinessLayer.Algorithm_GetItem(Key);
		}

		/// <summary>
		/// Loads a specific item for the business entity Algorithm.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse Algorithm_FormLoad(string Key) {
			return m_EnergyBusinessLayer.Algorithm_FormLoad(Key);
		}


		/// <summary>
		/// Creates a new business entity Algorithm and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <param name="inputs">The inputs.</param>
		/// <returns></returns>
		public FormResponse Algorithm_FormCreate(Algorithm item, CrudStore<AlgorithmInputDefinition> inputs) {
			return m_EnergyBusinessLayer.Algorithm_FormCreate(item, inputs);
		}

		/// <summary>
		/// Updates an existing business entity Algorithm and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <param name="inputs">The inputs.</param>
		/// <returns></returns>
		public FormResponse Algorithm_FormUpdate(Algorithm item, CrudStore<AlgorithmInputDefinition> inputs) {
			return m_EnergyBusinessLayer.Algorithm_FormUpdate(item, inputs);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse Algorithm_FormUpdateBatch(string[] keys, Algorithm item) {
			return m_EnergyBusinessLayer.Algorithm_FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Deletes an existing business entity Algorithm.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool Algorithm_Delete(Algorithm item) {
			return m_EnergyBusinessLayer.Algorithm_Delete(item);
		}

		/// <summary>
		/// Gets a json store for the business entity ChartAlgorithm.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<ChartAlgorithm> ChartAlgorithm_GetStore(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.ChartAlgorithm_GetStore(paging, location);
		}

		/// <summary>
		/// Saves a crud store for the business entity ChartAlgorithm.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<ChartAlgorithm> ChartAlgorithm_SaveStore(CrudStore<ChartAlgorithm> store) {
			return m_EnergyBusinessLayer.ChartAlgorithm_SaveStore(store);
		}

		/// <summary>
		/// Gets a list for the business entity ChartAlgorithm. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		public List<ChartAlgorithm> ChartAlgorithm_GetAll(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.ChartAlgorithm_GetAll(paging, location);
		}

		/// <summary>
		/// Gets a specific item for the business entity ChartAlgorithm.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public ChartAlgorithm ChartAlgorithm_GetItem(string Key) {
			return m_EnergyBusinessLayer.ChartAlgorithm_GetItem(Key);
		}

		/// <summary>
		/// Loads a specific item for the business entity ChartAlgorithm.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse ChartAlgorithm_FormLoad(string Key) {
			return m_EnergyBusinessLayer.ChartAlgorithm_FormLoad(Key);
		}


		/// <summary>
		/// Creates a new business entity ChartAlgorithm and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <param name="inputs">The inputs.</param>
		/// <returns></returns>
		public FormResponse ChartAlgorithm_FormCreate(ChartAlgorithm item, CrudStore<AlgorithmInputValue> inputs) {
			return m_EnergyBusinessLayer.ChartAlgorithm_FormCreate(item, inputs);
		}

		/// <summary>
		/// Updates an existing business entity ChartAlgorithm and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <param name="inputs">The inputs.</param>
		/// <returns></returns>
		public FormResponse ChartAlgorithm_FormUpdate(ChartAlgorithm item, CrudStore<AlgorithmInputValue> inputs) {
			return m_EnergyBusinessLayer.ChartAlgorithm_FormUpdate(item, inputs);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse ChartAlgorithm_FormUpdateBatch(string[] keys, ChartAlgorithm item) {
			return m_EnergyBusinessLayer.ChartAlgorithm_FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Deletes an existing business entity ChartAlgorithm.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool ChartAlgorithm_Delete(ChartAlgorithm item) {
			return m_EnergyBusinessLayer.ChartAlgorithm_Delete(item);
		}

		/// <summary>
		/// Get a store of Algorithm for a specific chart.
		/// </summary>
		/// <param name="KeyChart">The key chart.</param>
		/// <param name="paging">The paging.</param>
		/// <returns></returns>
		public JsonStore<ChartAlgorithm> ChartAlgorithm_GetStoreByKeyChart(string KeyChart, PagingParameter paging) {
			return m_EnergyBusinessLayer.ChartAlgorithm_GetStoreByKeyChart(KeyChart, paging);
		}


		/// <summary>
		/// Returns the list of existing events title from a specific calendar category.
		/// </summary>
		/// <param name="KeyCalendarEventCategory">The key calendar event category.</param>
		/// <param name="KeyMeter">The key meter.</param>
		/// <param name="paging">The paging.</param>
		/// <returns></returns>
		public JsonStore<SimpleListElement> Calendar_GetEventsTitle(string KeyCalendarEventCategory, string KeyMeter, PagingParameter paging) {
			return m_EnergyBusinessLayer.Calendar_GetEventsTitle(KeyCalendarEventCategory, KeyMeter, paging);
		}




		/// <summary>
		/// Gets a json store for the business entity FlipCard.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<FlipCard> FlipCard_GetStore(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.FlipCard_GetStore(paging, location);
		}

		/// <summary>
		/// Saves a crud store for the business entity FlipCard.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<FlipCard> FlipCard_SaveStore(CrudStore<FlipCard> store) {
			return m_EnergyBusinessLayer.FlipCard_SaveStore(store);
		}

		/// <summary>
		/// Gets a list for the business entity FlipCard. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		public List<FlipCard> FlipCard_GetAll(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.FlipCard_GetAll(paging, location);
		}

		/// <summary>
		/// Gets a specific item for the business entity FlipCard.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FlipCard FlipCard_GetItem(string Key) {
			return m_EnergyBusinessLayer.FlipCard_GetItem(Key);
		}

		/// <summary>
		/// Loads a specific item for the business entity FlipCard.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse FlipCard_FormLoad(string Key) {
			return m_EnergyBusinessLayer.FlipCard_FormLoad(Key);
		}


		/// <summary>
		/// Creates a new business entity FlipCard and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <param name="cards">The cards.</param>
		/// <returns></returns>
		public FormResponse FlipCard_FormCreate(FlipCard item, CrudStore<FlipCardEntry> cards) {
			return m_EnergyBusinessLayer.FlipCard_FormCreate(item, cards);
		}

		/// <summary>
		/// Updates an existing business entity FlipCard and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <param name="cards">The cards.</param>
		/// <returns></returns>
		public FormResponse FlipCard_FormUpdate(FlipCard item, CrudStore<FlipCardEntry> cards) {
			return m_EnergyBusinessLayer.FlipCard_FormUpdate(item, cards);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse FlipCard_FormUpdateBatch(string[] keys, FlipCard item) {
			return m_EnergyBusinessLayer.FlipCard_FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Deletes an existing business entity FlipCard.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool FlipCard_Delete(FlipCard item) {
			return m_EnergyBusinessLayer.FlipCard_Delete(item);
		}






		/// <summary>
		/// Gets a json store for the business entity FlipCardEntry.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<FlipCardEntry> FlipCardEntry_GetStore(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.FlipCardEntry_GetStore(paging, location);
		}

		/// <summary>
		/// Saves a crud store for the business entity FlipCardEntry.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<FlipCardEntry> FlipCardEntry_SaveStore(CrudStore<FlipCardEntry> store) {
			return m_EnergyBusinessLayer.FlipCardEntry_SaveStore(store);
		}

		/// <summary>
		/// Gets a list for the business entity FlipCardEntry. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		public List<FlipCardEntry> FlipCardEntry_GetAll(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.FlipCardEntry_GetAll(paging, location);
		}

		/// <summary>
		/// Gets a specific item for the business entity FlipCardEntry.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FlipCardEntry FlipCardEntry_GetItem(string Key) {
			return m_EnergyBusinessLayer.FlipCardEntry_GetItem(Key);
		}

		/// <summary>
		/// Loads a specific item for the business entity FlipCardEntry.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse FlipCardEntry_FormLoad(string Key) {
			return m_EnergyBusinessLayer.FlipCardEntry_FormLoad(Key);
		}


		/// <summary>
		/// Creates a new business entity FlipCardEntry and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse FlipCardEntry_FormCreate(FlipCardEntry item) {
			return m_EnergyBusinessLayer.FlipCardEntry_FormCreate(item);
		}

		/// <summary>
		/// Updates an existing business entity FlipCardEntry and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse FlipCardEntry_FormUpdate(FlipCardEntry item) {
			return m_EnergyBusinessLayer.FlipCardEntry_FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse FlipCardEntry_FormUpdateBatch(string[] keys, FlipCardEntry item) {
			return m_EnergyBusinessLayer.FlipCardEntry_FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Deletes an existing business entity FlipCardEntry.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool FlipCardEntry_Delete(FlipCardEntry item) {
			return m_EnergyBusinessLayer.FlipCardEntry_Delete(item);
		}

		/// <summary>
		/// Gets a store of  FlipCardEntry for a specific FlipCard.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyFlipCard"></param>
		/// <returns></returns>
		public JsonStore<FlipCardEntry> FlipCardEntry_GetStoreByKeyFlipCard(PagingParameter paging, string KeyFlipCard) {
			return m_EnergyBusinessLayer.FlipCardEntry_GetStoreByKeyFlipCard(paging, KeyFlipCard);
		}


		/// <summary>
		/// Determines whether [is energy aggregator crud notification in progress].
		/// </summary>
		public bool IsEnergyAggregatorCrudNotificationInProgress() {
			return m_EnergyBusinessLayer.IsEnergyAggregatorCrudNotificationInProgress();
		}


		/// <summary>
		/// Gets a json store for the business entity AlgorithmInputValue.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<AlgorithmInputValue> AlgorithmInputValue_GetStore(PagingParameter paging, PagingLocation location) {
			JsonStore<AlgorithmInputValue> store = m_EnergyBusinessLayer.AlgorithmInputValue_GetStore(paging, location);
			AlgorithmHelper.ClearPasswordValue(store.records);

			return store;
		}

		/// <summary>
		/// Saves a crud store for the business entity AlgorithmInputValue.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<AlgorithmInputValue> AlgorithmInputValue_SaveStore(CrudStore<AlgorithmInputValue> store) {
			AlgorithmHelper.RemovePasswordValueForUpdate(store);
			return m_EnergyBusinessLayer.AlgorithmInputValue_SaveStore(store);
		}

		/// <summary>
		/// Gets a list for the business entity AlgorithmInputValue. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		public List<AlgorithmInputValue> AlgorithmInputValue_GetAll(PagingParameter paging, PagingLocation location) {
			List<AlgorithmInputValue> values = m_EnergyBusinessLayer.AlgorithmInputValue_GetAll(paging, location);
			AlgorithmHelper.ClearPasswordValue(values);

			return values;
		}

		/// <summary>
		/// Gets a specific item for the business entity AlgorithmInputValue.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public AlgorithmInputValue AlgorithmInputValue_GetItem(string Key) {
			return m_EnergyBusinessLayer.AlgorithmInputValue_GetItem(Key);
		}

		/// <summary>
		/// Loads a specific item for the business entity AlgorithmInputValue.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse AlgorithmInputValue_FormLoad(string Key) {
			FormResponse formResponse = m_EnergyBusinessLayer.AlgorithmInputValue_FormLoad(Key);

			var inputValue = formResponse.data as AlgorithmInputValue;
			AlgorithmHelper.ClearPasswordValue(inputValue);

			return formResponse;
		}


		/// <summary>
		/// Creates a new business entity AlgorithmInputValue and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse AlgorithmInputValue_FormCreate(AlgorithmInputValue item) {
			return m_EnergyBusinessLayer.AlgorithmInputValue_FormCreate(item);
		}

		/// <summary>
		/// Updates an existing business entity AlgorithmInputValue and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse AlgorithmInputValue_FormUpdate(AlgorithmInputValue item) {
			return m_EnergyBusinessLayer.AlgorithmInputValue_FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse AlgorithmInputValue_FormUpdateBatch(string[] keys, AlgorithmInputValue item) {
			return m_EnergyBusinessLayer.AlgorithmInputValue_FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Deletes an existing business entity AlgorithmInputValue.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool AlgorithmInputValue_Delete(AlgorithmInputValue item) {
			return m_EnergyBusinessLayer.AlgorithmInputValue_Delete(item);
		}


		/// <summary>
		/// Gets a json store for the business entity AlgorithmInputDefinition.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<AlgorithmInputDefinition> AlgorithmInputDefinition_GetStore(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.AlgorithmInputDefinition_GetStore(paging, location);
		}

		/// <summary>
		/// Saves a crud store for the business entity AlgorithmInputDefinition.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<AlgorithmInputDefinition> AlgorithmInputDefinition_SaveStore(CrudStore<AlgorithmInputDefinition> store) {
			return m_EnergyBusinessLayer.AlgorithmInputDefinition_SaveStore(store);
		}

		/// <summary>
		/// Gets a list for the business entity AlgorithmInputDefinition. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		public List<AlgorithmInputDefinition> AlgorithmInputDefinition_GetAll(PagingParameter paging, PagingLocation location) {
			return m_EnergyBusinessLayer.AlgorithmInputDefinition_GetAll(paging, location);
		}

		/// <summary>
		/// Gets a specific item for the business entity AlgorithmInputDefinition.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public AlgorithmInputDefinition AlgorithmInputDefinition_GetItem(string Key) {
			return m_EnergyBusinessLayer.AlgorithmInputDefinition_GetItem(Key);
		}

		/// <summary>
		/// Loads a specific item for the business entity AlgorithmInputDefinition.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse AlgorithmInputDefinition_FormLoad(string Key) {
			return m_EnergyBusinessLayer.AlgorithmInputDefinition_FormLoad(Key);
		}


		/// <summary>
		/// Creates a new business entity AlgorithmInputDefinition and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse AlgorithmInputDefinition_FormCreate(AlgorithmInputDefinition item) {
			return m_EnergyBusinessLayer.AlgorithmInputDefinition_FormCreate(item);
		}

		/// <summary>
		/// Updates an existing business entity AlgorithmInputDefinition and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse AlgorithmInputDefinition_FormUpdate(AlgorithmInputDefinition item) {
			return m_EnergyBusinessLayer.AlgorithmInputDefinition_FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse AlgorithmInputDefinition_FormUpdateBatch(string[] keys, AlgorithmInputDefinition item) {
			return m_EnergyBusinessLayer.AlgorithmInputDefinition_FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Deletes an existing business entity AlgorithmInputDefinition.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool AlgorithmInputDefinition_Delete(AlgorithmInputDefinition item) {
			return m_EnergyBusinessLayer.AlgorithmInputDefinition_Delete(item);
		}


		/// <summary>
		/// Gets a json store of AlgorithmInputDefinition for a specific Algorithm.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyAlgorithm">The key script.</param>
		/// <returns>
		/// A json store.
		/// </returns>
		public JsonStore<AlgorithmInputDefinition> AlgorithmInputDefinition_GetStoreByKeyAlgorithm(PagingParameter paging, string KeyAlgorithm) {
			return m_EnergyBusinessLayer.AlgorithmInputDefinition_GetStoreByKeyAlgorithm(paging, KeyAlgorithm);
		}



		/// <summary>
		/// Gets a json store of AlgorithmInputValue for a specific Algorithm/Chart.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyChart">The key chart.</param>
		/// <param name="KeyAlgorithm">The key algorithm.</param>
		/// <returns>
		/// A json store.
		/// </returns>
		public JsonStore<AlgorithmInputValue> AlgorithmInputValue_GetStoreByKeyChart(PagingParameter paging, string KeyChart, string KeyAlgorithm) {
			JsonStore<AlgorithmInputValue> store = m_EnergyBusinessLayer.AlgorithmInputValue_GetStoreByKeyChart(paging, KeyChart, KeyAlgorithm);
			AlgorithmHelper.ClearPasswordValue(store.records);

			return store;
		}


		/// <summary>
		/// Gets the specific data models tree.
		/// </summary>
		/// <param name="KeyScript">The key script.</param>
		/// <returns></returns>
		public List<TreeNode> ScriptModel_GetTree(string KeyScript) {
			return m_EnergyBusinessLayer.ScriptModel_GetTree(KeyScript);
		}


		/// <summary>
		/// Get the C# class squeleton for Analytics.
		/// </summary>
		/// <param name="className">Name of the class.</param>
		/// <returns></returns>
		public string Algorithm_GetCode(string className) {
			return m_EnergyBusinessLayer.Algorithm_GetCode(className);
		}

		/// <summary>
		/// Get data points of Analytics.
		/// </summary>
		/// <param name="keyChart">The key chart.</param>
		/// <param name="paging">The paging.</param>
		/// <param name="timeInterval">The time interval.</param>
		/// <param name="localisation">The localisation.</param>
		/// <param name="classificationLevel">The classification level.</param>
		/// <param name="dynamicTimeScaleEnabled">if set to <c>true</c> [dynamic time scale enabled].</param>
		/// <param name="startDate">The start date.</param>
		/// <param name="endDate">The end date.</param>
		/// <param name="filterSpatialKeys">The filter spatial keys.</param>
		/// <param name="metersClassificationsKeys">The meters classifications keys.</param>
		/// <param name="metersKeys">The meters keys.</param>
		/// <returns></returns>
		public List<DataPoint> Analytics_GetDataPoints(string keyChart, PagingParameter paging, AxisTimeInterval timeInterval,
				ChartLocalisation localisation, int classificationLevel, bool dynamicTimeScaleEnabled, DateTime startDate,
				DateTime endDate, List<string> filterSpatialKeys, List<string> metersClassificationsKeys, List<string> metersKeys) {

			throw new NotImplementedException("No longer supported, Use OData API instead");
		}

		/// <summary>
		/// Convert date to time zone
		/// </summary>
		/// <param name="date"></param>
		/// <param name="timeZoneId"></param>
		/// <returns></returns>
		public DateTime ConvertDateToTimeZone(DateTime date, string timeZoneId) {
			return m_EnergyBusinessLayer.ConvertDateToTimeZone(date, timeZoneId);
		}

		/// <summary>
		/// Convert Date to Utc from a date in a given time zone
		/// </summary>
		/// <param name="date"></param>
		/// <param name="timeZoneId"></param>
		/// <returns></returns>
		public DateTime ConvertDateToUtcFromTimeZone(DateTime date, string timeZoneId) {
			return m_EnergyBusinessLayer.ConvertDateToUtcFromTimeZone(date, timeZoneId);
		}
	}
}

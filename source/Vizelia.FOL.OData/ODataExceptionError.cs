﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Data.OData;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using Microsoft.Practices.EnterpriseLibrary.Validation.PolicyInjection;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Localization;

namespace Vizelia.FOL.OData {

/// <summary>
/// ODataError that hosts an exception. It exposes the Message, exception type,
/// and if in debug mode, StackTrace.
/// </summary>
	public class ODataExceptionError : BaseODataError {
		/// <summary>
		/// Constructor. Converts the exception into the properties of this class.
		/// </summary>
		/// <param name="exception"></param>
		public ODataExceptionError(Exception exception)
			: base() {
				ErrorCode = Langue.error_odata_exception_code;
				Message = exception.Message;
				MessageLanguage = Helper.GetCurrentUICulture();
				ExceptionClass = exception.GetType().Name;
#if DEBUG
			// this would be better if it was exposed by authorization roles...
				StackTrace = exception.StackTrace;
#endif
		}
		/// <summary>
		/// Holds the class name of the Exception
		/// </summary>
		public string ExceptionClass { get; set; }

		/// <summary>
		/// Holds the stack trace from the Exception.
		/// Only output when in DEBUG mode.
		/// </summary>
		public string StackTrace { get; set; }
	}

}

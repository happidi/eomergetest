﻿// Copyright (c) Microsoft Open Technologies, Inc. All rights reserved. See License.txt in the project root for license information.

using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Microsoft.Data.OData;
using Microsoft.Practices.EnterpriseLibrary.Validation.PolicyInjection;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Localization;

namespace Vizelia.FOL.OData {
	/// <summary>
	/// A set of useful correctly formatted OData errors.
	/// </summary>
	public static class ODataErrors {
		/// <summary>
		/// Entity not found OData error.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public static HttpResponseException EntityNotFound(this HttpRequestMessage request) {
			return new HttpResponseException(
					request.CreateResponse(
						HttpStatusCode.NotFound,
						new ODataError {
							ErrorCode = Langue.error_odata_entitynotfound_code,
							Message = Langue.error_odata_entitynotfound_msg,
							MessageLanguage = Helper.GetCurrentUICulture()
						}
					)
			);
		}

		/// <summary>
		/// Not supported OData error.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <param name="message">The message.</param>
		/// <returns></returns>
		public static HttpResponseException NotSupported(this HttpRequestMessage request, string message) {
			return new HttpResponseException(
					request.CreateResponse(
						HttpStatusCode.NotImplemented,
						new ODataError {
							ErrorCode = Langue.error_odata_notsupported_code,
							Message = message,
							MessageLanguage = Helper.GetCurrentUICulture()
						}
					)
			);
		}

		/// <summary>
		/// Method not allowed OData error.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <param name="message">The message.</param>
		/// <returns></returns>
		public static HttpResponseException MethodNotAllowed(this HttpRequestMessage request, string message) {
			return new HttpResponseException(
					request.CreateResponse(
						HttpStatusCode.MethodNotAllowed,
						new ODataError {
							ErrorCode = Langue.error_odata_methodnotallowed_code,
							Message = message,
							MessageLanguage = Helper.GetCurrentUICulture()
						}
					)
			);
		}

		/// <summary>
		/// General OData error.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <param name="errorMessage">The error message.</param>
		/// <returns></returns>
		public static HttpResponseException ErrorOccurred(this HttpRequestMessage request, string errorMessage) {
			return new HttpResponseException(
					request.CreateResponse(
						HttpStatusCode.InternalServerError,
						new ODataError {
							ErrorCode = Langue.error_odata_erroroccurred_code,
							Message = errorMessage,
							MessageLanguage = Helper.GetCurrentUICulture()
						}
					)
			);
		}

		/// <summary>
		/// Null entity OData error.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public static HttpResponseException NullEntity(this HttpRequestMessage request) {
			return new HttpResponseException(
					request.CreateResponse(
						HttpStatusCode.BadRequest,
						new ODataError {
							ErrorCode = Langue.error_odata_nullentity_code,
							Message = Langue.error_odata_nullentity_msg,
							MessageLanguage = Helper.GetCurrentUICulture()
						}
					)
			);
		}

		/// <summary>
		/// Null key OData error.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public static HttpResponseException NullKey(this HttpRequestMessage request) {
			return new HttpResponseException(
					request.CreateResponse(
						HttpStatusCode.BadRequest,
						new ODataError {
							ErrorCode = Langue.error_odata_nullkey_code,
							Message = Langue.error_odata_nullkey_msg,
							MessageLanguage = Helper.GetCurrentUICulture()
						}
					)
			);
		}

		/// <summary>
		/// Convert the ArgumentValidationException into InstanceAnnotations,
		/// where each describes the name/value pairs of errors.
		/// </summary>
		/// <param name="request"></param>
		/// <param name="exception"></param>
		/// <returns></returns>
		public static HttpResponseException ValidationError(this HttpRequestMessage request,
			ArgumentValidationException exception) {
			ODataValidationError errorDetails = new ODataValidationError(exception);
 
			HttpResponseMessage message = request.CreateResponse<ODataValidationError>(
						HttpStatusCode.NotModified, errorDetails);
			HttpResponseException result =  new HttpResponseException(message);
			return result;
		}

		/// <summary>
		/// Exception occurred.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <param name="exception">The exception.</param>
		/// <returns></returns>
		public static HttpResponseException ExceptionOccurred(this HttpRequestMessage request, Exception exception) {
			return new HttpResponseException(
					request.CreateResponse(
						HttpStatusCode.InternalServerError,
						new ODataExceptionError(exception)
					)
			);
		}


	}


}
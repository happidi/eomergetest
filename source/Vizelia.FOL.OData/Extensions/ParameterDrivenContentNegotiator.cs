﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Formatting;
using System.Collections.ObjectModel;
using System.Net.Http;

namespace Vizelia.FOL.OData {
	/// <summary>
	/// Allows these query string parameters to override the default ContentNegotiator.
	/// $format=json, $format=xml
	/// </summary>
	/// <remarks>
	/// <para>Any MediaTypeFormatterMatch objects defined will return an ODataMediaTypeFormatter
	/// because it knows how to create a Serializer specific to XML or JSON and the type of data (like Entity,
	/// Collection, Feed, etc).</para>
	/// <para>ODataLib 3 is limited in its support for XML and JSON, for these cases:</para>
	/// <para>* XML does not support "Feed" with the ODataFeedSerializer. Feed supports this pattern: /api/v1/EntitySetName.</para>
	/// <para>* XML does not support "Entry" with the ODataEntityTypeSerializer. Entry supports this pattern: /api/v1/EntitySetName(key)</para>
	/// <para>* JSON does not support "MetadataDocument" with the ODataMetadataSerializer. /api/v1/$metadata.</para>
	/// </remarks>
	public class ParameterDrivenContentNegotiator : DefaultContentNegotiator {
		/// <summary>
		/// Constructor
		/// </summary>
		public ParameterDrivenContentNegotiator()
			: base() {

		}
		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="excludeMatchOnTypeOnly">True to exclude formatters that match
		/// only the object type; false otherwise.</param>
		public ParameterDrivenContentNegotiator(bool excludeMatchOnTypeOnly)
			: base(excludeMatchOnTypeOnly) {

		}


		/// <summary>
		/// If the $format parameter exists, it creates a list of possible results along with the JsonMediaTypeFormatter
		/// or XmlMediaTypeFormatter that drives them.
		/// </summary>
		/// <param name="type"></param>
		/// <param name="request"></param>
		/// <param name="formatters"></param>
		/// <returns></returns>
		protected override Collection<MediaTypeFormatterMatch> ComputeFormatterMatches(Type type, HttpRequestMessage request, IEnumerable<MediaTypeFormatter> formatters) {
			IEnumerable<KeyValuePair<string, string>> queryParameters = request.GetQueryNameValuePairs();
			var formattersList = formatters.ToList();
			foreach (var item in queryParameters) {
				if (item.Key.Equals("$format") && !String.IsNullOrEmpty(item.Value)) {
					Collection<MediaTypeFormatterMatch> result = ProcessMediaTypeToken(item.Value, type, request, formattersList);
					if (result != null)
						return result;

					break;
				}

			}
			return base.ComputeFormatterMatches(type, request, formattersList);
		}

		/// <summary>
		/// Processes the media type token.
		/// </summary>
		/// <param name="mediaTypeRequested">The media type requested.</param>
		/// <param name="type">The type.</param>
		/// <param name="request">The request.</param>
		/// <param name="formatters">The formatters.</param>
		/// <returns></returns>
		/// <exception cref="System.InvalidOperationException">
		/// $metadata requests do not support $format=json. They are restricted to xml output.
		/// or
		/// Single entity requests do not support $format=xml. They are restricted to json output.
		/// </exception>
		protected virtual Collection<MediaTypeFormatterMatch> ProcessMediaTypeToken(string mediaTypeRequested,
			Type type, HttpRequestMessage request, IEnumerable<MediaTypeFormatter> formatters) {

			string contentType = null;
			string otherParameterName = null;
			string otherParameterValue = null;
			switch (mediaTypeRequested.ToLowerInvariant()) {
				case "json":
					contentType = "application/json";
					break;
				case "jsonminimal":
					contentType = "application/json";
					otherParameterName = "odata";
					otherParameterValue = "minimalmetadata";
					break;
				case "jsonfull":
					contentType = "application/json";
					otherParameterName = "odata";
					otherParameterValue = "fullmetadata";
					break;
				case "jsonverbose":
					contentType = "application/json";
					otherParameterName = "odata";
					otherParameterValue = "verbose";
					break;
				case "jsonnometadata":
					contentType = "application/json";
					otherParameterName = "odata";
					otherParameterValue = "nometadata";
					break;
				case "xml":
					contentType = "application/xml";
					break;
			}
			if (contentType != null) {
				// 	JSON is not allowed on Metadatadocuments because ODataMetadataSerializer is limited to XML.
				// XML is not allowed in SingleResult objects because ODataEntityTypeFeed is limited to JSON.
				// This provides a much clearer error message than what will happen if this wasn't here
				if (typeof(Microsoft.Data.Edm.IEdmModel).IsAssignableFrom(type) && contentType.Equals("application/json")) {
					throw new InvalidOperationException("$metadata requests do not support $format=json. They are restricted to xml output.");
				}
				if (typeof(System.Web.Http.SingleResult).IsAssignableFrom(type) && contentType.Equals("application/xml")) {
					throw new InvalidOperationException("Single entity requests do not support $format=xml. They are restricted to json output.");

				}


				Collection<MediaTypeFormatterMatch> updatedMatches = new Collection<MediaTypeFormatterMatch>();
				foreach (MediaTypeFormatter formatter in formatters) {
					if (formatter.CanWriteType(type))
						foreach (var headerValue in formatter.SupportedMediaTypes)
							if (headerValue.MediaType.Equals(contentType)) {
								bool found = true;
								if (otherParameterName != null) {
									found = false;
									foreach (var parmValue in headerValue.Parameters)
										if (otherParameterName.Equals(parmValue.Name) && otherParameterValue.Equals(parmValue.Value)) {
											found = true;
											break;
										}
								}
								else if (headerValue.Parameters.Any())
									found = false;
								if (found) {
									MediaTypeFormatterMatch match = new MediaTypeFormatterMatch(
										formatter,
										headerValue,
										1.0,	// 1.0 = perfect match. 0.0 = no match
										MediaTypeFormatterMatchRanking.MatchOnRequestWithMediaTypeMapping);
									updatedMatches.Add(match);
								}
							}
				}
				if (updatedMatches.Any())
					return updatedMatches;
			}
			return null;
		}
	}
}

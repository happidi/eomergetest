﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http.Controllers;
using System.Web.Http.OData.Routing;
using System.Web.Http.OData.Routing.Conventions;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.OData {
	/// <summary>
	/// Expanded ODataPathHandler that allows you to inject custom RouteHandlers that 
	/// will detect a user and map it to specific actions and controllers. As a result,
	/// it also is the app's IODataRoutingConvention class.
	/// </summary>
	/// <remarks>
	/// <para>Defined your RouteHandlers based on BaseRouteHandler
	/// and add them to the RouteHandlers collection.</para>
	/// </remarks>
	public class PathAndRouteConventionHandler : DefaultODataPathHandler, IODataRoutingConvention {
		/// <summary>
		/// Any ODataPathParsers that should get a first chance at the odata path in Parse().
		/// </summary>
		/// <remarks>
		/// Add these when the PathAndRouteConventionHandler instance is first created as it cannot
		/// handle multithreaded modifications.
		/// </remarks>
		public List<BaseRouteHandler> RouteHandlers {
			get { return m_RouteHandlers ?? (m_RouteHandlers = new List<BaseRouteHandler>()); }
		}
		private List<BaseRouteHandler> m_RouteHandlers;


#if false
		private static Regex _forMetadata = new Regex(@"(?<entityset>\w*)/((?<property>\w*)/)?\$metadata", RegexOptions.IgnoreCase);
#endif
		/// <summary>
		/// Parses the overall odatapath into segments described in an ODataPath object.
		/// If CustomParsers were defined, they get to try to parse before using the default
		/// from DefaultODataPathHandler.
		/// </summary>
		/// <param name="model"></param>
		/// <param name="odataPath"></param>
		/// <returns></returns>
		public override ODataPath Parse(Microsoft.Data.Edm.IEdmModel model, string odataPath) {
			if (m_RouteHandlers != null)
				foreach (var parser in RouteHandlers) {
					ODataPathWithRoutingConvention result = parser.TryParse(model, odataPath);
					if (result != null)
						return result;
				}
#if false
			Match match = _forMetadata.Match(odataPath);
			if (match.Success)
			{
				List<ODataPathSegment> segments = new List<ODataPathSegment>();
				segments.Add(new EntitySetPathSegment(match.Groups["entityset"].Value));
				if (match.Groups["property"].Success)
					segments.Add(new PropertyAccessPathSegment(match.Groups["property"].Value));
				segments.Add(new ActionPathSegment("Metadata"));    // capitalized to match the method name GetMetadata. 
				return new ODataPath(segments);
			}
#endif

			ODataPath resultODataPath = base.Parse(model, odataPath);

			// now that the default processor has figured out segments, allow custom parsers
			// to use that data and create a custom ODataPathWithRoutingConvention that handles SelectAction and SelectController.
			if ((resultODataPath != null) && (m_RouteHandlers != null)) {
				foreach (var parser in RouteHandlers) {
					ODataPathWithRoutingConvention result = parser.PostDefaultParser(model, resultODataPath);
					if (result != null)
						return result;
				}
			}
			return resultODataPath;
		}



		#region IODataRoutingConvention Members

		/// <summary>
		/// Selects the action for OData requests.
		/// </summary>
		/// <param name="odataPath">The OData path.</param>
		/// <param name="controllerContext">The controller context.</param>
		/// <param name="actionMap">The action map.</param>
		/// <returns>
		/// null if the request isn't handled by this convention; otherwise, the name of the selected action
		/// </returns>
		public virtual string SelectAction(ODataPath odataPath, HttpControllerContext controllerContext, ILookup<string, HttpActionDescriptor> actionMap) {
			// Setting the culture for all requests
			if (controllerContext.Request.Headers.AcceptLanguage.Any()) {
				Helper.SetUICulture(controllerContext.Request.Headers.AcceptLanguage.OrderByDescending(x => x.Quality ?? 1).First().Value);
			}
			ODataPathWithRoutingConvention path = odataPath as ODataPathWithRoutingConvention;
			return path != null ? path.SelectAction(controllerContext, actionMap) : null;
		}

		/// <summary>
		/// Selects the controller for OData requests.
		/// </summary>
		/// <param name="odataPath">The OData path.</param>
		/// <param name="request">The request.</param>
		/// <returns>
		/// null if the request isn't handled by this convention; otherwise, the name of the selected controller
		/// </returns>
		public virtual string SelectController(ODataPath odataPath, System.Net.Http.HttpRequestMessage request) {
			ODataPathWithRoutingConvention path = odataPath as ODataPathWithRoutingConvention;
			return path != null ? path.SelectController(request) : null;
		}

		#endregion
	}
}

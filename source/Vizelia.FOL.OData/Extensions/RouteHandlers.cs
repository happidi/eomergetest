﻿using System.Globalization;
using System.Web.Routing;
using Microsoft.Data.Edm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Web.Http.Controllers;
using System.Web.Http.OData.Routing;

namespace Vizelia.FOL.OData {
	/// <summary>
	/// Assigned to PathAndRouteConventionHandler.RouteHandlers to get the first
	/// shot at parsing. If its TryParse method returns an instance of ODataPathWithRoutingConvention,
	/// it can later be called to get the controller and action, effectively
	/// handling IODataRoutingConvention interface.
	/// </summary>
	/// <remarks>
	/// <para>Intended to be multithreaded. Do not include thread specific properties.</para>
	/// </remarks>
	abstract public class BaseRouteHandler {
		/// <summary>
		/// Called to parse the odataPath. If it can, it returns a ODataPathWithRoutingConvention object.
		/// If it cannot, it returns null.
		/// </summary>
		/// <remarks>
		/// <para>Intended to be called by PathAndRouteConventionHandler.Parse before it does it default processing.
		/// If it returns null, default processing will continue. If it returns an object, that object is returned
		/// from Parse immedidately.</para>
		/// <para>This class always returns null.</para>
		/// </remarks>
		/// <returns></returns>
		public virtual ODataPathWithRoutingConvention TryParse(IEdmModel model, string odataPath) {
			return null;
		}

		/// <summary>
		/// After the DefaultODataPathHandler resolves the odatapath segments, if you want to replace its
		/// SelectAction and SelectController, return a ODataPathWithRoutingConvention with those methods setup.
		/// </summary>
		/// <remarks>
		/// <para>Intended to be called by PathAndRouteConventionHandler.Parse after it lets the 
		/// DefaultODataPathHandler create an ODataPath object from the OData path. This either
		/// returns a new instance of ODataPathWithRoutingConvention (use new ODataPathWithRoutingConvention(this, odataPath))
		/// or null if you want to let the ODataPath you were passed to be returned by Parse.</para>
		/// </remarks>
		/// <param name="model"></param>
		/// <param name="odataPath"></param>
		/// <returns>If replaced, return ODataPathWithRoutingConvention. Otherwise return null.</returns>
		public virtual ODataPathWithRoutingConvention PostDefaultParser(IEdmModel model, ODataPath odataPath) {
			return null;
		}

		/// <summary>
		/// Selects the action for OData requests based on the given ODataPath.
		/// </summary>
		/// <param name="odataPath">The OData path.</param>
		/// <param name="controllerContext">The controller context.</param>
		/// <param name="actionMap">The action map.</param>
		/// <returns>
		/// null if the request isn't handled by this convention; otherwise, the name of the selected action
		/// </returns>
		public virtual string SelectAction(ODataPathWithRoutingConvention odataPath, HttpControllerContext controllerContext, ILookup<string, HttpActionDescriptor> actionMap) {
			return null;
		}

		/// <summary>
		/// Selects the controller for OData requests based on the given ODataPath.
		/// </summary>
		/// <param name="odataPath">The OData path.</param>
		/// <param name="request">The request.</param>
		/// <returns>
		/// null if the request isn't handled by this convention; otherwise, the name of the selected controller
		/// </returns>
		public virtual string SelectController(ODataPathWithRoutingConvention odataPath, HttpRequestMessage request) {
			return null;
		}
	}


	/// <summary>
	/// Extends ODataPath to provide data specific to the BaseRouteHandler
	/// when its TryParse() method returns. This class introduces the reference
	/// to the original BaseRouteHandler so its SelectAction and SelectController
	/// methods can be invoked on the same object.
	/// </summary>
	public class ODataPathWithRoutingConvention : ODataPath {
		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="routeHandler"></param>
		/// <param name="segments"></param>
		public ODataPathWithRoutingConvention(BaseRouteHandler routeHandler, params ODataPathSegment[] segments)
			: base(segments) {
			if (routeHandler == null)
				throw new ArgumentNullException("routeHandler");
			RouteHandler = routeHandler;
		}

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="routeHandler"></param>
		/// <param name="segments"></param>
		public ODataPathWithRoutingConvention(BaseRouteHandler routeHandler, List<ODataPathSegment> segments)
			: base(segments) {
			if (routeHandler == null)
				throw new ArgumentNullException("routeHandler");
			RouteHandler = routeHandler;
		}

		/// <summary>
		/// Constructor that converts an existing ODataPath object into this class by copying
		/// its Segments.
		/// </summary>
		/// <param name="routeHandler"></param>
		/// <param name="odataPath">A source for Segments.</param>
		public ODataPathWithRoutingConvention(BaseRouteHandler routeHandler, ODataPath odataPath)
			: base(odataPath.Segments) {
			if (routeHandler == null)
				throw new ArgumentNullException("routeHandler");
			RouteHandler = routeHandler;
		}

		/// <summary>
		/// The instance that created this in TryParse.
		/// </summary>
		public BaseRouteHandler RouteHandler { get; protected set; }

		/// <summary>
		/// Selects the action for OData requests.
		/// </summary>
		/// <remarks>
		/// <para>This class redirects to the Owner.SelectAction, but its reasonable to override
		/// it to support logic here.</para>
		/// </remarks>
		/// <param name="controllerContext">The controller context.</param>
		/// <param name="actionMap">The action map.</param>
		/// <returns>
		/// null if the request isn't handled by this convention; otherwise, the name of the selected action
		/// </returns>
		public virtual string SelectAction(HttpControllerContext controllerContext, ILookup<string, HttpActionDescriptor> actionMap) {
			return RouteHandler.SelectAction(this, controllerContext, actionMap);
		}

		/// <summary>
		/// Selects the controller for OData requests.
		/// </summary>
		/// <remarks>
		/// <para>This class redirects to the Owner.SelectController, but its reasonable to override
		/// it to support logic here.</para>
		/// </remarks>
		/// <param name="request">The request.</param>
		/// <returns>
		/// null if the request isn't handled by this convention; otherwise, the name of the selected controller
		/// </returns>
		public virtual string SelectController(HttpRequestMessage request) {
			return RouteHandler.SelectController(this, request);
		}

	}


	/// <summary>
	/// Base class for parsers looking for the entityset in the first segment.
	/// It defines the SelectController to return a string reflecting the EntitySet name.
	/// </summary>
	/// <remarks>
	/// <para>If your ODataPath.PathTemplate contains a NavigationPathSegment, it is used
	/// to select the controller instead of the EntitySetPathSegment.</para>
	/// </remarks>
	abstract public class BaseEntitySetRouteHandler : BaseRouteHandler {

		/// <summary>
		/// Subclass uses this to turn on the check for the NavigationPathSegment.
		/// It defaults to false.
		/// </summary>
		/// <returns>false</returns>
		protected virtual bool UsesNavigationPathSegment() {
			return false;
		}

		/// <summary>
		/// If the first segment is EntitySetPathSegment, its EntitySetName is returned.
		/// If it has another segment of NavigationPathSegment and it identifies a EdmTypeKind.Collection,
		/// it returns odataPath.EntitySet.Name. This requires overriding UsesNavigationPathSegment()
		/// to return true.
		/// </summary>
		/// <param name="odataPath"></param>
		/// <param name="request"></param>
		/// <returns></returns>
		public override string SelectController(ODataPathWithRoutingConvention odataPath, HttpRequestMessage request) {
			if (odataPath.Segments.Count == 0)
				return null;
			var entitySetPathSegment = odataPath.Segments[0] as EntitySetPathSegment;
			string result = null;
			if (entitySetPathSegment != null) {
				result = entitySetPathSegment.EntitySetName;
				if (UsesNavigationPathSegment()) {
					for (int i = 1; i < odataPath.Segments.Count; i++) {
						NavigationPathSegment navPathSegment = odataPath.Segments[i] as NavigationPathSegment;
						if (navPathSegment != null) {
							var isCollection = navPathSegment.NavigationProperty.Type.Definition.TypeKind == EdmTypeKind.Collection;

							result = isCollection ? odataPath.EntitySet.Name : entitySetPathSegment.EntitySetName;
							break;
						}
					}   // for

				}
			}   // if
			return result;
		}

		/// <summary>
		/// Gets the name of the action.
		/// </summary>
		/// <param name="method">The method.</param>
		/// <param name="actionName">Name of the action.</param>
		/// <returns></returns>
		protected static string GetActionName(HttpMethod method, string actionName) {
			return (method == HttpMethod.Get ? "Get" : string.Empty) + actionName;
		}

	}

#if false// this was for a future feature called "entityset/$metadata. See also MetadataRouteHandler
	/// <summary>
	/// Supports ~/entityset/$metadata and ~/entityset/property/$metadata.
	/// </summary>
	public class MetadataRouteHandler : BaseEntitySetRouteHandler {
		private static readonly Regex m_ForMetadata = new Regex(@"(?<entityset>\w*)/((?<property>\w*)/)?\$metadata", RegexOptions.IgnoreCase);

		/// <summary>
		/// Called to parse the odataPath. If it can, it returns a ODataPathWithRoutingConvention object.
		/// If it cannot, it returns null.
		/// </summary>
		/// <returns></returns>
		public override ODataPathWithRoutingConvention TryParse(IEdmModel model, string odataPath) {
			Match match = m_ForMetadata.Match(odataPath);
			if (match.Success) {
				List<ODataPathSegment> segments = new List<ODataPathSegment>();
				segments.Add(new EntitySetPathSegment(match.Groups["entityset"].Value));
				if (match.Groups["property"].Success)
					segments.Add(new PropertyAccessPathSegment(match.Groups["property"].Value));
				segments.Add(new ActionPathSegment("$metadata"));
				return new ODataPathWithRoutingConvention(this, segments);
			}
			return null;
		}

		/// <summary>
		/// Works with BaseController[T].GetMetadata.
		/// </summary>
		/// <param name="odataPath">The OData path.</param>
		/// <param name="controllerContext">The controller context.</param>
		/// <param name="actionMap">The action map.</param>
		/// <returns>
		/// "GetMetadata"
		/// </returns>
		public override string SelectAction(ODataPathWithRoutingConvention odataPath, HttpControllerContext controllerContext, ILookup<string, HttpActionDescriptor> actionMap) {
			return "GetMetadata";
		}
	}
#endif

	/// <summary>
	/// Handles the path "~/entityset/key/navigation".
	/// </summary>
	public class EntitySetKeyNavigationRouteHandler : BaseEntitySetRouteHandler {
		/// <returns>true</returns>
		protected override bool UsesNavigationPathSegment() {
			return true;
		}
		/// <summary>
		/// Handles the path "~/entityset/key/navigation".
		/// </summary>
		/// <param name="model"></param>
		/// <param name="odataPath"></param>
		/// <returns></returns>
		public override ODataPathWithRoutingConvention PostDefaultParser(IEdmModel model, ODataPath odataPath) {
			if (odataPath.PathTemplate == "~/entityset/key/navigation")
				return new ODataPathWithRoutingConvention(this, odataPath);
			return null;
		}

		/// <summary>
		/// Selects GetNavigationSet or GetNavigationProperty on BaseController based on if
		/// the NavigationPathSegment is a EdmTypeKind.Collection or not.
		/// </summary>
		/// <param name="odataPath"></param>
		/// <param name="controllerContext"></param>
		/// <param name="actionMap"></param>
		/// <returns></returns>
		public override string SelectAction(ODataPathWithRoutingConvention odataPath, HttpControllerContext controllerContext, ILookup<string, HttpActionDescriptor> actionMap) {
			controllerContext.RouteData.Values[ODataHelper.const_parent_entity_name] = (odataPath.Segments[0] as EntitySetPathSegment).EntitySet.ElementType.Name;
			controllerContext.RouteData.Values[ODataRouteConstants.Key] = (odataPath.Segments[1] as KeyValuePathSegment).Value;
			IEdmNavigationProperty navigationProperty = (odataPath.Segments[2] as NavigationPathSegment).NavigationProperty;
			var isCollection = navigationProperty.Type.Definition.TypeKind == EdmTypeKind.Collection;
			return isCollection ? "GetNavigationSet" : "GetNavigationProperty";
		}

		/// <summary>
		/// EntitySetName from odataPath.Segments[0]
		/// </summary>
		/// <param name="odataPath"></param>
		/// <param name="request"></param>
		/// <returns></returns>
		public override string SelectController(ODataPathWithRoutingConvention odataPath, HttpRequestMessage request) {
			var entitySetPathSegment = odataPath.Segments[0] as EntitySetPathSegment;
			return entitySetPathSegment.EntitySetName;
		}
	}


	/// <summary>
	/// Handles routes with "~/entityset/key/property" 
	/// </summary>
	public class EntitySetKeyPropertyRouteHandler : BaseEntitySetRouteHandler {
		/// <summary>
		/// Handles routes with "~/entityset/key/property" 
		/// </summary>
		/// <param name="model"></param>
		/// <param name="odataPath"></param>
		/// <returns></returns>
		public override ODataPathWithRoutingConvention PostDefaultParser(IEdmModel model, ODataPath odataPath) {
			if (odataPath.PathTemplate == "~/entityset/key/property")
				return new ODataPathWithRoutingConvention(this, odataPath);
			return null;
		}


		/// <param name="odataPath"></param>
		/// <param name="controllerContext"></param>
		/// <param name="actionMap"></param>
		/// <returns>"GetProperty"</returns>
		public override string SelectAction(ODataPathWithRoutingConvention odataPath, HttpControllerContext controllerContext, ILookup<string, HttpActionDescriptor> actionMap) {
			return "GetProperty";
		}
	}


	/// <summary>
	/// Handles routes with "~/entityset/key/property/$value"
	/// </summary>
	public class EntitySetKeyPropertyValueRouteHandler : BaseEntitySetRouteHandler {
		/// <summary>
		/// Handles routes with "~/entityset/key/property/$value"
		/// </summary>
		/// <param name="model"></param>
		/// <param name="odataPath"></param>
		/// <returns></returns>
		public override ODataPathWithRoutingConvention PostDefaultParser(IEdmModel model, ODataPath odataPath) {
			if (odataPath.PathTemplate == "~/entityset/key/property/$value")
				return new ODataPathWithRoutingConvention(this, odataPath);
			return null;
		}

		/// <param name="odataPath"></param>
		/// <param name="controllerContext"></param>
		/// <param name="actionMap"></param>
		/// <returns>"GetProperty"</returns>
		public override string SelectAction(ODataPathWithRoutingConvention odataPath, HttpControllerContext controllerContext, ILookup<string, HttpActionDescriptor> actionMap) {
			return "GetProperty";
		}
	}


	/// <summary>
	/// Handles routes with "~/entityset/key/action"
	/// </summary>
	public class EntitySetKeyActionRouteHandler : BaseEntitySetRouteHandler {
		/// <summary>
		/// Handles routes with "~/entityset/key/action"
		/// </summary>
		public override ODataPathWithRoutingConvention PostDefaultParser(IEdmModel model, ODataPath odataPath) {
			if (odataPath.PathTemplate == "~/entityset/key/action")
				return new ODataPathWithRoutingConvention(this, odataPath);
			return null;
		}

		/// <summary>
		/// Converts the action name into a Get method on the BaseController.
		/// Note that action names must be supplied in the correct case.
		/// </summary>
		/// <param name="odataPath"></param>
		/// <param name="controllerContext"></param>
		/// <param name="actionMap"></param>
		/// <returns>"Get" + the action name (case preserved)</returns>
		public override string SelectAction(ODataPathWithRoutingConvention odataPath, HttpControllerContext controllerContext, ILookup<string, HttpActionDescriptor> actionMap) {
			var actionName = (odataPath.Segments[2] as ActionPathSegment).ActionName.Replace("Default.Container.", "");
			
			return GetActionName(controllerContext.Request.Method, actionName);
		}
	}


	/// <summary>
	/// Handles routes with "entityset" and "action". Override to supply the position
	/// of ActionPathSegment in the Segments collection.
	/// </summary>
	abstract public class BaseEntitySetActionRouteHandler : BaseEntitySetRouteHandler {
		/// <summary>
		/// The index into Segments containing the ActionPathSegment.
		/// </summary>
		/// <returns></returns>
		protected abstract int ActionIndex();

		/// <summary>
		/// Handles routes with "~/entityset/property/action"
		/// </summary>
		public override ODataPathWithRoutingConvention PostDefaultParser(IEdmModel model, ODataPath odataPath) {
			if (odataPath.PathTemplate == "~/entityset/property/action")
				return new ODataPathWithRoutingConvention(this, odataPath);
			return null;
		}

		/// <summary>
		/// Converts the action name into a Get method on the BaseController.
		/// Note that action names must be supplied in the correct case.
		/// </summary>
		/// <param name="odataPath"></param>
		/// <param name="controllerContext"></param>
		/// <param name="actionMap"></param>
		/// <returns>"Get" + the action name (case preserved)</returns>
		public override string SelectAction(ODataPathWithRoutingConvention odataPath, HttpControllerContext controllerContext, ILookup<string, HttpActionDescriptor> actionMap) {
			var actionName = (odataPath.Segments[ActionIndex()] as ActionPathSegment).ActionName.Replace("Default.Container.", "");

			return GetActionName(controllerContext.Request.Method, actionName);
		}
	}


	/// <summary>
	/// Handles routes with "~/entityset/property/action"
	/// </summary>
	public class EntitySetPropertyActionRouteHandler : BaseEntitySetActionRouteHandler {
		/// <summary>
		/// Handles routes with "~/entityset/property/action"
		/// </summary>
		public override ODataPathWithRoutingConvention PostDefaultParser(IEdmModel model, ODataPath odataPath) {
			if (odataPath.PathTemplate == "~/entityset/property/action")
				return new ODataPathWithRoutingConvention(this, odataPath);
			return null;
		}

		/// <returns>2</returns>
		protected override int ActionIndex() {
			return 2;
		}
	}


	/// <summary>
	/// Handles routes with "~/entityset/action"
	/// </summary>
	public class EntitySetActionRouteHandler : BaseEntitySetActionRouteHandler {
		/// <summary>
		/// Handles routes with "~/entityset/action"
		/// </summary>
		public override ODataPathWithRoutingConvention PostDefaultParser(IEdmModel model, ODataPath odataPath) {
			if (odataPath.PathTemplate == "~/entityset/action")
				return new ODataPathWithRoutingConvention(this, odataPath);
			return null;
		}

		/// <returns>1</returns>
		protected override int ActionIndex() {
			return 1;
		}
	}


	/// <summary>
	/// Handles routes with "~/entityset"
	/// </summary>
	public class EntitySetRouteHandler : BaseEntitySetRouteHandler {
		/// <summary>
		/// Handles routes with "~/entityset"
		/// </summary>
		public override ODataPathWithRoutingConvention PostDefaultParser(IEdmModel model, ODataPath odataPath) {
			if (odataPath.PathTemplate == "~/entityset")
				return new ODataPathWithRoutingConvention(this, odataPath);
			return null;
		}
	}


	/// <summary>
	/// Handles routes with "~/entityset/key/unresolved" to allow currently unsupported route "~/entityset/key/$value"
	/// </summary>
	public class EntitySetKeyUnresolvedRouteHandler : BaseEntitySetRouteHandler {
		/// <summary>
		/// Handles routes with "~/entityset/key/unresolved" to allow currently unsupported route "~/entityset/key/$value"
		/// </summary>
		/// <param name="model"></param>
		/// <param name="odataPath"></param>
		/// <returns></returns>
		public override ODataPathWithRoutingConvention PostDefaultParser(IEdmModel model, ODataPath odataPath) {
			if (odataPath.PathTemplate == "~/entityset/key/unresolved" && ((UnresolvedPathSegment)odataPath.Segments[2]).SegmentValue == "$value")
				return new ODataPathWithRoutingConvention(this, odataPath);
			return null;
		}

		/// <param name="odataPath"></param>
		/// <param name="controllerContext"></param>
		/// <param name="actionMap"></param>
		/// <returns>"GetProperty"</returns>
		public override string SelectAction(ODataPathWithRoutingConvention odataPath, HttpControllerContext controllerContext, ILookup<string, HttpActionDescriptor> actionMap) {
			return "GetEntityValue";
		}
	}
}
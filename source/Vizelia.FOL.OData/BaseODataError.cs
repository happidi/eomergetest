﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Data.OData;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using Microsoft.Practices.EnterpriseLibrary.Validation.PolicyInjection;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Localization;

namespace Vizelia.FOL.OData {
	/// <summary>
	/// Replacement for Microsoft.Data.OData.ODataError
	/// </summary>
	/// <remarks>
	/// <para>Microsoft.Data.OData.ODataError has a fatal flaw:
	/// It is sealed! This is a replacement, although it omits
	/// the InnerError and InstanceAnnotations properties because we aren't using them.</para>
	/// <para>REMINDER: Subclasses must be registered with the EdmModel as a complex type
	/// in ModelBuilder.RegisterODataValidationErrorElements.</para>
	/// </remarks>
	[Serializable]	// ensure GETters and SETers are public to include in serialization
	[System.Diagnostics.DebuggerDisplay("{ErrorCode}: {Message}")]
	abstract public class BaseODataError : ODataAnnotatable {

		/// <summary>
		/// Constructor
		/// </summary>
		public BaseODataError()
			: base() {

		}

		/// <summary>
		/// Gets or sets the error code to be used in payloads.
		/// </summary>
		public string ErrorCode { get; set; }

		/// <summary>
		/// Gets or sets the error message.
		/// </summary>
		public string Message { get; set; }

		/// <summary>
		/// Gets or sets the language for the exception Message.
		/// </summary>
		public string MessageLanguage { get; set; }
	}

}

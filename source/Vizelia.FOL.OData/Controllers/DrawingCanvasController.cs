﻿using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessLayer.Interfaces;

namespace Vizelia.FOL.OData.Controllers {
	/// <summary>
	/// OData controller for DrawingCanvas entity.
	/// </summary>
	public class DrawingCanvasController : BaseController<DrawingCanvas, IEnergyBusinessLayer> {
	}
}

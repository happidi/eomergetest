﻿using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessLayer.Interfaces;

namespace Vizelia.FOL.OData.Controllers {
	/// <summary>
	/// OData controller for WebFrame entity.
	/// </summary>
	public class WebFrameController : BaseController<WebFrame, IEnergyBusinessLayer> {
	}
}

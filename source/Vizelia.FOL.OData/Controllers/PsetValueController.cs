﻿using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessLayer.Interfaces;

namespace Vizelia.FOL.OData.Controllers {
	
	/// <summary>
	/// PsetValue Controller.
	/// </summary>
	public class PsetValueController : BaseController<PsetValue, ICoreBusinessLayer> {
	}
}

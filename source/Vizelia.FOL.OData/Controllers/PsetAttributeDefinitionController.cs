﻿using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessLayer.Interfaces;

namespace Vizelia.FOL.OData.Controllers {
	
	/// <summary>
	/// PsetAttributeDefinition Controller.
	/// </summary>
	public class PsetAttributeDefinitionController : BaseController<PsetAttributeDefinition, ICoreBusinessLayer> {

	}
}

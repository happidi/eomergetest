﻿using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessLayer.Interfaces;

namespace Vizelia.FOL.OData.Controllers {
	/// <summary>
	/// OData controller for User entity.
	/// </summary>
	public class UserController : BaseController<FOLMembershipUser, IAuthenticationBusinessLayer> {
		/// <summary>
		/// Updates the business entity.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="key">The key.</param>
		/// <returns></returns>
		protected override FormResponse UpdateBusinessEntity(FOLMembershipUser entity, string key) {
			entity.KeyUser = ODataHelper.DenormalizeKey(key, TypeName);
			var methodName = GetFullMethodName(const_methodname_formupdate);
			return RunMethod(() => BusinessLayer.User_FormUpdate(entity), methodName);
		}
	}
}

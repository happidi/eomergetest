﻿using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessLayer.Interfaces;

namespace Vizelia.FOL.OData.Controllers {
	/// <summary>
	/// OData controller for DrawingCanvasChart entity.
	/// </summary>
	public class DrawingCanvasChartController : BaseController<DrawingCanvasChart, IEnergyBusinessLayer> {
	}
}

﻿using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.OData.Extensions;
using System.Web.Http.OData.Routing;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessLayer.Interfaces;

namespace Vizelia.FOL.OData {
	/// <summary>
	/// Analytics controller
	/// </summary>
	public class AnalyticsController : BaseController<Analytics, IEnergyBusinessLayer> {
		private const string const_methodname_getdatapoints = "GetDataPoints";
		private const string const_methodname_getcustomgrid = "GetCustomGrid";
		private const string const_methodname_gethtml5 = "GetHtml5";
		private const string const_methodname_gethtml = "GetHtml";
		private const string const_methodname_gethtmlmap = "GetHtmlMap";
		private const string const_methodname_getcachedimagebyguid = "GetCachedImageByGuid";

		/// <summary>
		/// Gets the data points.
		/// </summary>
		/// <param name="analytics">The analytics.</param>
		/// <returns></returns>
		[HttpGet]
		public List<DataPoint> GetDataPoints([FromUri] Analytics analytics) {
			analytics.KeyAnalytics = GetAnalyticsKey();
			var methodName = GetFullMethodName(const_methodname_getdatapoints);
			var dataPoints = RunMethod(() => BusinessLayer.Analytics_GetDataPoints(analytics), methodName);
			return dataPoints;
		}

		/// <summary>
		/// Gets the image.
		/// </summary>
		/// <param name="analytics">The analytics.</param>
		/// <param name="width">The Width of the chart</param>
		/// <param name="height">The height of the chart</param>
		/// <returns></returns>
		[HttpGet]
		public HttpResponseMessage GetImage([FromUri] Analytics analytics, int width = ChartSize.DefaultImageWidth, int height = ChartSize.DefaultImageHeight) {
			analytics.KeyAnalytics = GetAnalyticsKey();
			return GetEntityValue(width, height, analytics);
		}

		/// <summary>
		/// Gets the CustomGrid.
		/// </summary>
		/// <param name="analytics">The analytics.</param>
		/// <returns></returns>
		[HttpGet]
		public List<ChartCustomGridCell> GetCustomGrid([FromUri] Analytics analytics) {
			analytics.KeyAnalytics = GetAnalyticsKey();
			var methodName = GetFullMethodName(const_methodname_getcustomgrid);
			var customGrid = RunMethod(() => BusinessLayer.Analytics_GetCustomGrid(analytics), methodName);
			return customGrid;
		}

		/// <summary>
		/// Gets the CustomGrid.
		/// </summary>
		/// <param name="analytics">The analytics.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <returns></returns>
		[HttpGet]
		public string GetHtml5([FromUri] Analytics analytics, int width = ChartSize.DefaultImageWidth, int height = ChartSize.DefaultImageHeight) {
			analytics.KeyAnalytics = GetAnalyticsKey();
			var methodName = GetFullMethodName(const_methodname_gethtml5);
			var streamResult = RunMethod(() => BusinessLayer.Analytics_GetHtml5(analytics, width, height), methodName);
			
			// convert stream to string
			StreamReader reader = new StreamReader(streamResult.ContentStream);
			string text = reader.ReadToEnd();
			return text;
		}

		/// <summary>
		/// Gets the CustomGrid.
		/// </summary>
		/// <param name="analytics">The analytics.</param>
		/// <returns></returns>
		[HttpGet]
		public string GetHtml([FromUri] Analytics analytics) {
			analytics.KeyAnalytics = GetAnalyticsKey();
			var methodName = GetFullMethodName(const_methodname_gethtml);
			var html = RunMethod(() => BusinessLayer.Analytics_GetHtml(analytics), methodName);
			return html;
		}

		/// <summary>
		/// Gets the image with HTML map.
		/// </summary>
		/// <param name="analytics">The analytics.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <returns></returns>
		[HttpGet]
		public ImageMap GetHtmlMap([FromUri] Analytics analytics, int width = ChartSize.DefaultImageWidth, int height = ChartSize.DefaultImageHeight) {
			analytics.KeyAnalytics = GetAnalyticsKey();
			var methodName = GetFullMethodName(const_methodname_gethtmlmap);
			var imageMap = RunMethod(() => BusinessLayer.Analytics_GetHtmlMap(analytics, width, height), methodName);
			return imageMap;
		}

		/// <summary>
		/// Gets the image by unique identifier.
		/// </summary>
		/// <param name="guidString">The unique identifier string.</param>
		/// <returns></returns>
		[HttpGet]
		public HttpResponseMessage GetCachedImageByGuid(string guidString) {
			var methodName = GetFullMethodName(const_methodname_getcachedimagebyguid);
			var streamContent = RunMethod(() => BusinessLayer.Analytics_GetCachedImageByGuid(guidString), methodName);
			var response = new HttpResponseMessage { Content = streamContent, StatusCode = HttpStatusCode.OK };
			return response;
		}

		private string GetAnalyticsKey() {
			ODataPath oDataPath = Request.ODataProperties().Path;
			var keySegment = oDataPath.Segments[1] as KeyValuePathSegment;

			return keySegment != null ? keySegment.Value : null;
		}
	}
}
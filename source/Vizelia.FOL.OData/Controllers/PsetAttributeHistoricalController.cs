﻿using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessLayer.Interfaces;

namespace Vizelia.FOL.OData.Controllers {
	/// <summary>
	/// OData controller for PsetAttributeHistorical entity.
	/// </summary>
	public class PsetAttributeHistoricalController : BaseController<PsetAttributeHistorical, ICoreBusinessLayer> {
	}
}

﻿using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessLayer.Interfaces;

namespace Vizelia.FOL.OData.Controllers {
	/// <summary>
	/// OData controller for Map entity.
	/// </summary>
	public class MapController : BaseController<Map, IEnergyBusinessLayer> {
	}
}

﻿using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessLayer.Interfaces;

namespace Vizelia.FOL.OData.Controllers {
	/// <summary>
	/// OData controller for StatisticalSerie entity.
	/// </summary>
	public class StatisticalSerieController : BaseController<StatisticalSerie, IEnergyBusinessLayer> {
	}
}

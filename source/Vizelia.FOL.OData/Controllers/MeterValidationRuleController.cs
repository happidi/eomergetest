﻿using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessLayer.Interfaces;

namespace Vizelia.FOL.OData.Controllers {
	/// <summary>
	/// OData controller for MeterValidationRule entity.
	/// </summary>
	public class MeterValidationRuleController : BaseController<MeterValidationRule, IEnergyBusinessLayer> {
	}
}

﻿using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessLayer.Interfaces;

namespace Vizelia.FOL.OData.Controllers {
	/// <summary>
	/// OData controller for AlarmDefinition entity.
	/// </summary>
	public class AlarmDefinitionController : BaseController<AlarmDefinition, IEnergyBusinessLayer> {
	}
}

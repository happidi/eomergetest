﻿using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessLayer.Interfaces;

namespace Vizelia.FOL.OData.Controllers {
	/// <summary>
	/// OData controller for Meter entity.
	/// </summary>
	public class MeterController : BaseController<Meter, IEnergyBusinessLayer> {
	}
}

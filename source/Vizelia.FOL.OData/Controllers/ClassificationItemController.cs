﻿using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessLayer.Interfaces;

namespace Vizelia.FOL.OData.Controllers {
	/// <summary>
	/// OData controller for  entity.
	/// </summary>
	public class ClassificationItemController : BaseController<ClassificationItem, ICoreBusinessLayer> {
	}
}

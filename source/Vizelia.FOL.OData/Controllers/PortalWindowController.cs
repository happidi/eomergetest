﻿using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessLayer.Interfaces;

namespace Vizelia.FOL.OData.Controllers {
	/// <summary>
	/// OData controller for PortalWindow entity.
	/// </summary>
	public class PortalWindowController : BaseController<PortalWindow, IEnergyBusinessLayer> {
	}
}

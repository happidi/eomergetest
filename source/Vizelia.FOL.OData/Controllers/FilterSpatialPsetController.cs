﻿using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessLayer.Interfaces;

namespace Vizelia.FOL.OData.Controllers {
	/// <summary>
	/// OData controller for FilterSpatialPset entity.
	/// </summary>
	public class FilterSpatialPsetController : BaseController<FilterSpatialPset, IEnergyBusinessLayer> {
	}
}

﻿using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessLayer.Interfaces;

namespace Vizelia.FOL.OData.Controllers {
	/// <summary>
	/// OData controller for PortalColumn entity.
	/// </summary>
	public class PortalColumnController : BaseController<PortalColumn, IEnergyBusinessLayer> {
	}
}

﻿using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessLayer.Interfaces;

namespace Vizelia.FOL.OData.Controllers {
	/// <summary>
	/// OData controller for FlipCard entity.
	/// </summary>
	public class FlipCardController : BaseController<FlipCard, IEnergyBusinessLayer> {
	}
}

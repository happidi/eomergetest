﻿using System;
using System.Web.Http;
using System.Web.Http.OData;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessLayer.Interfaces;

namespace Vizelia.FOL.OData.Controllers {
	/// <summary>
	/// OData controller for CustomData entity.
	/// </summary>
	public class CustomDataController : BaseController<CustomData, ICoreBusinessLayer> {

		private const string const_methodname_deleteall = "DeleteAll";

		/// <summary>
		/// Deletes all by container name and key user.
		/// </summary>
		/// <param name="parameters">The parameters.</param>
		[HttpPost]
		public void DeleteAll(ODataActionParameters parameters) {
			const string const_containername = "containerName";
			const string const_keyuser = "keyUser";

			if (!parameters.ContainsKey(const_containername)) {
				throw new ArgumentNullException(const_containername);
			}
			
			var containerName = parameters[const_containername] as string;

			string keyUser = null;
			
			if (parameters.ContainsKey(const_keyuser)) {
				keyUser = parameters[const_keyuser] as string;
			}

			var methodName = GetFullMethodName(const_methodname_deleteall);
			RunMethod(() => BusinessLayer.CustomData_DeleteAll(containerName, keyUser), methodName);
		}
	}
}
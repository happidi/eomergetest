﻿using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessLayer.Interfaces;

namespace Vizelia.FOL.OData.Controllers {
	/// <summary>
	/// OData controller for MeterOperation entity.
	/// </summary>
	public class MeterOperationController : BaseController<MeterOperation, IEnergyBusinessLayer> {
	}
}

﻿using System;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessLayer.Interfaces;

namespace Vizelia.FOL.OData.Controllers {
	/// <summary>
	/// OData controller for Portlet entity.
	/// </summary>
	public class PortletController : BaseController<Portlet, IEnergyBusinessLayer> {
		/// <summary>
		/// Ensures TypeEntity property is assigned to a default
		/// </summary>
		/// <param name="entity"></param>
		/// <returns></returns>
		protected override FormResponse CreateBusinessEntity(Portlet entity) {
			if (String.IsNullOrEmpty(entity.TypeEntity))
				entity.TypeEntity = typeof(Chart).FullName;	// TypeEntity is not defined as required field, but it really is required
			if (entity.Flex == 0)
				entity.Flex = 100;

			return base.CreateBusinessEntity(entity);
		}

		/// <summary>
		/// Ensures TypeEntity property is assigned to a default
		/// </summary>
		/// <param name="entity"></param>
		/// <returns></returns>
		protected override FormResponse UpdateBusinessEntity(Portlet entity, string key) {
			if (String.IsNullOrEmpty(entity.TypeEntity))
				entity.TypeEntity = typeof(Chart).FullName;	// TypeEntity is not defined as required field, but it really is required
			if (entity.Flex == 0)
				entity.Flex = 100;
			return base.UpdateBusinessEntity(entity, key);
		}

		/// <summary>
		/// Calls BusinessLayer.Portlet_Delete directly
		/// </summary>
		/// <param name="entity"></param>
		protected override void DeleteBusinessEntity(Portlet entity) {
			BusinessLayer.Portlet_Delete(entity, false);
		}
	}
}

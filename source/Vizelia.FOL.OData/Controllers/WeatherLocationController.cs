﻿using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessLayer.Interfaces;

namespace Vizelia.FOL.OData.Controllers {
	/// <summary>
	/// OData controller for WeatherLocation entity.
	/// </summary>
	public class WeatherLocationController : BaseController<WeatherLocation, IEnergyBusinessLayer> {
	}
}

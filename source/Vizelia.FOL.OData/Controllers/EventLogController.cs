﻿using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessLayer.Interfaces;

namespace Vizelia.FOL.OData.Controllers {
	/// <summary>
	/// OData controller for EventLog entity.
	/// </summary>
	public class EventLogController : BaseController<EventLog, IEnergyBusinessLayer> {
	}
}

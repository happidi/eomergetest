﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Web;
using System.Web.Http;
using System.Web.Http.OData;
using System.Web.Http.OData.Extensions;
using System.Web.Http.OData.Query;
using System.Web.Http.OData.Routing;
using Microsoft.Practices.EnterpriseLibrary.Validation.PolicyInjection;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Localization;
using Vizelia.FOL.Providers;
using Vizelia.FOL.Security;

namespace Vizelia.FOL.OData {
	/// <summary>
	/// Base Controller for all OData controllers.
	/// </summary>
	/// <remarks>
	/// <para>Controllers for specific entities typically override this without needing 
	/// to offer any additional code.</para>
	/// <para>Those whose business layer API calls differ from the supported formats
	/// built into BaseController must override the appropriate method:
	/// GetBusinessEntity, GetBusinessEntities, CreateBusinessEntity, UpdateBusinessEntity,
	/// DeleteBusinessEntity.</para>
	/// <para>Since this class uses .net Reflection to look up business layer API
	/// calls, you may improve your controllers by always overriding the above methods,
	/// and calling your API function directly. This will let the compiler catch errors.</para>
	/// <para>If you need to introduce a new Action method, be sure to register it
	/// within the ModelBuilder class associated with your entity's class.</para>
	/// </remarks>
	/// <typeparam name="TEntity">The type of the entity.</typeparam>
	/// <typeparam name="TInterface">The type of the interface.</typeparam>
	[ODataNullValue]
	public abstract class BaseController<TEntity, TInterface> : ODataController
		where TEntity : BaseBusinessEntity, new() {

		private readonly string m_BusinessLayerName;

		/// <summary>
		/// Gets or sets the business layer.
		/// </summary>
		/// <value>
		/// The business layer.
		/// </value>
		protected TInterface BusinessLayer { get; set; }

		/// <summary>
		/// Gets or sets the name of the type.
		/// </summary>
		/// <value>
		/// The name of the type.
		/// </value>
		protected string TypeName { get; set; }

		/// <summary>
		/// The constant method name for GetItem
		/// </summary>
		protected const string const_methodname_getitem = "GetItem";
		/// <summary>
		/// The constant method name for GetAll
		/// </summary>
		protected const string const_methodname_getall = "GetAll";
		/// <summary>
		/// The constant method name for FormCreate
		/// </summary>
		protected const string const_methodname_formcreate = "FormCreate";
		/// <summary>
		/// The constant method name for FormUpdate
		/// </summary>
		protected const string const_methodname_formupdate = "FormUpdate";
		/// <summary>
		/// The constant method name for Delete
		/// </summary>
		protected const string const_methodname_delete = "Delete";
		/// <summary>
		/// The constant method name for GetValue
		/// </summary>
		protected const string const_methodname_getvalue = "GetValue";

		/// <summary>
		/// The constant default page size for query
		/// </summary>
		protected const int const_default_page_size = 10000;

		/// <summary>
		/// Initializes a new instance of the <see cref="BaseController{TEntity, TInterface}"/> class.
		/// </summary>
		protected BaseController() {
			// Policy injection on the business layer instance.
			BusinessLayer = Helper.Resolve<TInterface>();
			m_BusinessLayerName = BusinessLayer.GetType().Name.Replace("BusinessLayer", "");

			TypeName = GetType().Name.Replace(ODataHelper.const_controller_keyword, string.Empty);
		}

		/// <summary>
		/// Gets the specified query options.
		/// </summary>
		/// <param name="queryOptions">The query options.</param>
		/// <returns></returns>
		[EnableQuery(AllowedQueryOptions = AllowedQueryOptions.All, PageSize = const_default_page_size)]
		[HttpGet]
		public virtual IQueryable<TEntity> Get(ODataQueryOptions<TEntity> queryOptions) {
			var fields = GetExpandFields(queryOptions);
			var paging = new PagingParameter();
			var queryable = GetEntities(null, paging, fields).AsQueryable();

			return queryable;
		}

		// GET api/v1/entity
		/// <summary>
		/// Gets this instance.
		/// </summary>
		/// <param name="queryOptions">The query options.</param>
		/// <param name="key">The key.</param>
		/// <returns></returns>
		[EnableQuery(AllowedQueryOptions = AllowedQueryOptions.All)]
		[HttpGet]
		public SingleResult<TEntity> Get(ODataQueryOptions<TEntity> queryOptions, [FromODataUri] string key) {
			CheckForNullKey(key);
			var fields = GetExpandFields(queryOptions);
			return SingleResult.Create(GetEntities(key, null, fields).AsQueryable());
		}

		// POST api/v1/entity
		/// <summary>
		/// Creates the entity.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <returns></returns>
		[HttpPost]
		public HttpResponseMessage Post(TEntity entity) {
			CheckForNullEntity(entity);
			var formResponse = CreateBusinessEntity(entity);
			ThrowOnFailure(formResponse);
			var response = Request.CreateResponse(HttpStatusCode.Created, formResponse.data as TEntity);
			return response;
		}

		// PUT api/v1/entity/5
		/// <summary>
		/// Updates the entity.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <param name="entity">The entity.</param>
		/// <returns></returns>
		[HttpPut]
		public TEntity Put([FromODataUri]string key, TEntity entity) {
			CheckForNullKey(key);
			CheckForNullEntity(entity);
			var formResponse = UpdateBusinessEntity(entity, key);
			ThrowOnFailure(formResponse);
			return formResponse.data as TEntity;
		}

		// DELETE api/v1/entity/5
		/// <summary>
		/// Deletes the specified key.
		/// </summary>
		/// <param name="key">The key.</param>
		[HttpDelete]
		public void Delete([FromODataUri]string key) {
			CheckForNullKey(key);
			var entity = GetBusinessEntity(ODataHelper.DenormalizeKey(key, TypeName));
			DeleteBusinessEntity(entity);
		}

		/// <summary>
		/// Gets the navigation set.
		/// </summary>
		/// <returns></returns>
		[EnableQuery(AllowedQueryOptions = AllowedQueryOptions.All, PageSize = const_default_page_size)]
		[HttpGet]
		public virtual HttpResponseMessage GetNavigationSet([FromODataUri] string key) {
			ODataPath oDataPath = this.Request.ODataProperties().Path;
			var navigationPropertyName = (oDataPath.Segments[2] as NavigationPathSegment).NavigationPropertyName;
			return GetProperty(key, navigationPropertyName);
		}

		/// <summary>
		/// Gets the navigation property.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <returns></returns>
		[HttpGet]
		public virtual HttpResponseMessage GetNavigationProperty([FromODataUri] string key) {
			ODataPath oDataPath = this.Request.ODataProperties().Path;
			var navigationPropertyName = (oDataPath.Segments[2] as NavigationPathSegment).NavigationPropertyName;
			return GetProperty(key, navigationPropertyName);
		}

		/// <summary>
		/// Gets the property.
		/// </summary>
		/// <returns></returns>
		[HttpGet]
		public virtual HttpResponseMessage GetProperty() {
			ODataPath oDataPath = this.Request.ODataProperties().Path;
			var propertyName = (oDataPath.Segments[2] as PropertyAccessPathSegment).PropertyName;
			var key = (oDataPath.Segments[1] as KeyValuePathSegment).Value;
			return GetProperty(key, propertyName);
		}
#if false // this was for a future feature called "entityset/$metadata. See also MetadataRouteHandler
		/// <summary>
		/// Gets the metadata.
		/// </summary>
		/// <returns></returns>
		[HttpGet]
		public HttpResponseMessage GetMetadata() {
			string content = "{'json' : 'value'}";
#if true // temporary while we use a string instead of objects to be formatted.
			var msg = new HttpResponseMessage(HttpStatusCode.OK) {
				Content = new StringContent(content, System.Text.Encoding.UTF8, "application/json")
			};
#else 
			HttpResponseMessage msg = Request.CreateResponse(HttpStatusCode.OK, content, 
				new System.Net.Http.Headers.MediaTypeHeaderValue("application/json")); 
#endif
			return msg;
		}
#endif
		/// <summary>
		/// Gets the method.
		/// </summary>
		/// <param name="methodName">The method name.</param>
		/// <param name="methodParameterTypes">The method parameter types.</param>
		/// <returns></returns>
		protected virtual MethodInfo GetMethod(string methodName, Type[] methodParameterTypes) {
			var fullMethodName = GetFullMethodName(methodName);

			EvaluateMethodAuthorization(fullMethodName);

			var cacheKey = string.Format("OData_{0}.{1}", m_BusinessLayerName, fullMethodName);
			var methodInfo = CacheService.Get(cacheKey,
				() => BusinessLayer.GetType().GetMethod(fullMethodName, BindingFlags.Public | BindingFlags.Instance, null, methodParameterTypes, null));
			if (methodInfo == null)
				throw Request.NotSupported(Langue.error_odata_notsupported_msg);
			return methodInfo;
		}

		/// <summary>
		/// Gets the full name of the method.
		/// </summary>
		/// <param name="methodName">Name of the method.</param>
		/// <returns></returns>
		protected virtual string GetFullMethodName(string methodName) {
			return string.Format("{0}_{1}", TypeName, methodName);
		}

		/// <summary>
		/// Runs the method.
		/// </summary>
		/// <param name="methodToRun">The method to run.</param>
		/// <param name="methodName">Name of the method.</param>
		/// <returns></returns>
		protected virtual T RunMethod<T>(Func<T> methodToRun, string methodName) {
			EvaluateMethodAuthorization(methodName);
			return methodToRun();
		}

		/// <summary>
		/// Checks that the user is authorized to continue.
		/// Supply the API method name that is associated with an AZMain Operation.
		/// That name will be checked against the AZMan database for the current login.
		/// If it fails, an exception is thrown.
		/// </summary>
		/// <param name="methodName">The name of an AZMan Operation.
		/// Usually it is identical to the name of the API function that you intend to invoke.</param>
		protected virtual void EvaluateMethodAuthorization(string methodName) {
			try {
				AuthorizationPolicy.Evaluate(m_BusinessLayerName, methodName);
			}
			catch (VizeliaSessionTimeoutException) {
				// As for now, Basic authentication use this exception to return 401 - Unauthorized 
				throw;
			}
			catch (Exception) {
				throw Request.MethodNotAllowed(string.Format(Langue.error_odata_methodnotallowed_msg, methodName, HttpContext.Current.User.Identity.Name));
			}
		}

		/// <summary>
		/// Gets the business entity. If not found, it throws an Entity Not Found exception.
		/// </summary>
		/// <remarks>
		/// <para>This is the low level function used by GET operations that have a Key to an entity.</para>
		/// <para>This class assumes the method is on the BusinessLayer, it has a specific 
		/// naming convention of "[EntityName]_GetItem", and the method takes the same
		/// two parameters as GetBusinessEntity. If those requirements are not met,
		/// override this method to call the appropriate API function.</para>
		/// <para>Also if you prefer strongly bound code instead of Reflection resolution,
		/// override this. For example:
		/// <code>
		/// return RunMethod&lt;Meter&gt;(() => BusinessLayer.Meter_GetItem(keyEntity, fields), "Meter_GetItem");</code>
		/// </para>
		/// </remarks>
		/// <param name="keyEntity">The key entity.</param>
		/// <param name="fields">The fields.</param>
		/// <returns>The entity. Never returns null.</returns>
		protected virtual TEntity GetBusinessEntity(string keyEntity, params string[] fields) {
			var methodGetItem = GetMethod(const_methodname_getitem, new[] { typeof(string), typeof(string[]) });
			var entity = methodGetItem.Invoke(BusinessLayer, new object[] { keyEntity, fields }) as TEntity;
			if (entity == null) {
				throw Request.EntityNotFound();
			}
			return entity;
		}


		/// <summary>
		/// Gets the business entities. 
		/// </summary>
		/// <remarks>
		/// <para>This is the low level function used by GET operations that retrieve multiple entities,
		/// optionally with filtering, sorting, and paging.</para>
		/// <para>This class assumes the method is on the BusinessLayer, it has a specific 
		/// naming convention of "[EntityName]_GetAll", and the method takes the same
		/// two parameters as GetBusinessEntities. If those requirements are not met,
		/// override this method to call the appropriate API function.</para>
		/// <para>Also if you prefer strongly bound code instead of Reflection resolution,
		/// override this. For example:
		/// <code>
		/// return RunMethod&lt;Meter&gt;(() => BusinessLayer.Meter_GetAll(paging, PagingLocation.Database, fields), "Meter_GetAll");</code>
		/// </para>
		/// </remarks>
		/// <param name="paging">The paging.</param>
		/// <param name="fields">The fields.</param>
		/// <returns>The entities found. If there are none found, it returns an empty list.
		/// Never returns null.</returns>
		protected virtual List<TEntity> GetBusinessEntities(PagingParameter paging, params string[] fields) {
			var methodGetAll = GetMethod(const_methodname_getall, new[] { typeof(PagingParameter), typeof(PagingLocation), typeof(string[]) });
			var entities = methodGetAll.Invoke(BusinessLayer, new object[] { paging, PagingLocation.Database, fields }) as List<TEntity>;
			return entities;
		}

		/// <summary>
		/// Creates the business entity.
		/// </summary>
		/// <remarks>
		/// <para>This is the low level function used by POST operations to create an entity.</para>
		/// <para>This class assumes the method is on the BusinessLayer, it has a specific 
		/// naming convention of "[EntityName]_FormCreate", and the method takes the same
		/// two parameter as CreateBusinessEntity. If those requirements are not met,
		/// override this method to call the appropriate API function.</para>
		/// <para>Also if you prefer strongly bound code instead of Reflection resolution,
		/// override this. For example:
		/// <code>
		/// return RunMethod&lt;Meter&gt;(() => BusinessLayer.Meter_FormCreate(entity), "Meter_FormCreate");</code>
		/// </para>
		/// </remarks>
		/// <param name="entity">The entity.</param>
		/// <returns>A FormResponse containing the resulting entity if successful and
		/// failure information, including validation errors if not successful.</returns>
		protected virtual FormResponse CreateBusinessEntity(TEntity entity) {
			var methodFormCreate = GetMethod(const_methodname_formcreate, new[] { typeof(TEntity) });
			return methodFormCreate.Invoke(BusinessLayer, new object[] { entity }) as FormResponse;
		}

		/// <summary>
		/// Updates the business entity.
		/// </summary>
		/// <remarks>
		/// <para>This is the low level function used by PUT operations to overwrite an entity.</para>
		/// <para>This class assumes the method is on the BusinessLayer, it has a specific 
		/// naming convention of "[EntityName]_FormUpdate", and the method takes a single 
		/// parameter with the entity, ensuring that the primary key property is assigned
		/// to the key parameter value before making the call. If those requirements are not met,
		/// override this method to call the appropriate API function.</para>
		/// <para>Also if you prefer strongly bound code instead of Reflection resolution,
		/// override this. For example:
		/// <code>
		/// entity.SetKey(ODataHelper.DenormalizeKey(key, TypeName));
		/// return RunMethod&lt;Meter&gt;(() => BusinessLayer.Meter_FormUpdate(entity), "Meter_FormUpdate");</code>
		/// </para>
		/// </remarks>
		/// <param name="entity">The entity.</param>
		/// <param name="key"></param>
		/// <returns>A FormResponse containing the resulting entity if successful and
		/// failure information, including validation errors if not successful.</returns>
		protected virtual FormResponse UpdateBusinessEntity(TEntity entity, string key) {
			entity.SetKey(ODataHelper.DenormalizeKey(key, TypeName));
			var methodFormUpdate = GetMethod(const_methodname_formupdate, new[] { typeof(TEntity) });
			return methodFormUpdate.Invoke(BusinessLayer, new object[] { entity }) as FormResponse;
		}

		/// <summary>
		/// Deletes the business entity.
		/// </summary>
		/// <remarks>
		/// <para>This is the low level function used by DELETE operations.</para>
		/// <para>This class assumes the method is on the BusinessLayer, it has a specific 
		/// naming convention of "[EntityName]_Delete", and the method takes
		/// a single parameter of a entity with its primary key property assigned to the
		/// record to be deleted. If those requirements are not met,
		/// override this method to call the appropriate API function.</para>
		/// <para>Also if you prefer strongly bound code instead of Reflection resolution,
		/// override this. For example:
		/// <code>
		/// return RunMethod&lt;Meter&gt;(() => BusinessLayer.Meter_Delete(entity), "Meter_Delete");</code>
		/// </para>
		/// </remarks>
		/// <param name="entity">The entity.</param>
		protected virtual void DeleteBusinessEntity(TEntity entity) {
			var methodDelete = GetMethod(const_methodname_delete, new[] { typeof(TEntity) });
			methodDelete.Invoke(BusinessLayer, new object[] { entity });
		}

		/// <summary>
		/// Resolves whether to get a single entity by its key or a list of entities.
		/// Calls either GetBusinessEntity or GetBusinessEntities based on that.
		/// </summary>
		/// <param name="key">When assigned a value, it means get one entity.
		/// When assigned null or "", it will always get multiple entities,
		/// applying the PagingParameter.</param>
		/// <param name="paging"></param>
		/// <param name="fields"></param>
		/// <returns>A list. If it resolved to get a single entity, the list contains only the one entity.</returns>
		protected virtual IEnumerable<TEntity> GetEntities(string key, PagingParameter paging, params string[] fields) {
			IEnumerable<TEntity> entities;

			if (!string.IsNullOrWhiteSpace(key)) {
				var entity = GetBusinessEntity(ODataHelper.DenormalizeKey(key, TypeName), fields);
				entities = new List<TEntity> { entity };
			}
			else {
				entities = GetBusinessEntities(paging, fields);
			}
			return entities;
		}

		/// <summary>
		/// Utility to create a list of property names ("fields") that will be
		/// passed into GetBusinessEntity and GetBusinessEntities functions.
		/// It uses the $select parameter from the ODataQueryOptions object passed
		/// into GET methods.
		/// </summary>
		/// <param name="queryOptions"></param>
		/// <returns></returns>
		protected static string[] GetExpandFields(ODataQueryOptions<TEntity> queryOptions) {
			string fields = queryOptions.SelectExpand != null ? queryOptions.SelectExpand.RawExpand : null;
			return fields != null ? fields.Split(',') : null;
		}

		/// <summary>
		/// Utility that throws a Null Key exception if the key is null.
		/// Call in methods that are passed a key parameter when it requires a value.
		/// </summary>
		/// <param name="key"></param>
		protected void CheckForNullKey(string key) {
			if (key == null) {
				throw Request.NullKey();
			}
		}

		/// <summary>
		/// Utility that throws a Null Entity exception if the parameter is null.
		/// Call in methods that are passed an entity parameter when its value cannot
		/// be null. Often the OData framework will call your functions with null
		/// when it could not resolve elements in the body of the request.
		/// (It also often throws an exception with a 500 status code before
		/// ever calling your function.)
		/// </summary>
		/// <param name="entity"></param>
		protected void CheckForNullEntity(TEntity entity) {
			if (entity == null) {
				throw Request.NullEntity();
			}
		}

		/// <summary>
		/// Utility to throw various exceptions when a FormResponse
		/// has its success property set to false.
		/// Call it after getting a FormResponse, even if success is true.
		/// </summary>
		/// <param name="formResponse"></param>
		protected virtual void ThrowOnFailure(FormResponse formResponse) {
			if (!formResponse.success) {
				if (formResponse.msg == Langue.msg_save_item_error_null_returned) {
					throw Request.EntityNotFound();
				}
				if (formResponse.exception is ArgumentValidationException) {
					throw Request.ValidationError((ArgumentValidationException)formResponse.exception);
				}
				if (formResponse.exception != null)
					throw Request.ExceptionOccurred(formResponse.exception);

				throw Request.ErrorOccurred(formResponse.FormatError());
			}
		}

		/// <summary>
		/// Utility for requests that need to access a specific property on an entity.
		/// It takes the primary key to the entity and loads up the entity. If that succeeds,
		/// it gets the property value and returns it wrapped in an HttpResponseMessage.
		/// </summary>
		/// <remarks>
		/// <para>Throws a Null Entity exception causing a 404 error if the entity was not found.</para>
		/// </remarks>
		/// <param name="key"></param>
		/// <param name="propertyName"></param>
		/// <returns></returns>
		protected virtual HttpResponseMessage GetProperty(string key, string propertyName) {
			var entity = GetEntities(key, null, new[] { propertyName }).FirstOrDefault();
			var propertyInfo = typeof(TEntity).GetProperty(propertyName, BindingFlags.Instance | BindingFlags.Public);
			var content = propertyInfo.GetValue(entity, null);

			// There are two System.Net.Http.HttpRequestMessageExtensions classes in two different dlls, we want the one on System.Web.Http.dll
			var assembly = AppDomain.CurrentDomain.GetAssemblies().First(x => x.FullName.StartsWith("System.Web.Http,"));
			var type = assembly.GetType("System.Net.Http.HttpRequestMessageExtensions");
			// Method with generic parameter T, can't get it via GetMethod so we look it up as the only CreateResponse with 3 parameters.
			MethodInfo method = type.GetMethods().First(x => x.Name == "CreateResponse" && x.GetParameters().Length == 3);
			MethodInfo generic = method.MakeGenericMethod(propertyInfo.PropertyType);
			var response = generic.Invoke(Request, new[] { Request, HttpStatusCode.OK, content });

			// Return our response.
			return response as HttpResponseMessage;
		}

		/// <summary>
		/// Gets the entity value. Designed to work with a route of ~/Entityset/Key/$value
		/// to get a representation of the entity that is some human understandable format like
		/// an image or spreadsheet.
		/// </summary>
		/// <remarks>
		/// <para>This method was built around Charts, converting its content into an image.</para>
		/// <para>It's underlying method, GetStreamContent, currently handles any entity that
		/// has a method named [Entityname]_GetValue.</para>
		/// </remarks>
		/// <param name="width">The Width of the chart</param>
		/// <param name="height">The height of the chart</param>
		/// <param name="entity">The entity</param>
		/// <returns></returns>
        public virtual HttpResponseMessage GetEntityValue(int width = ChartSize.DefaultImageWidth, int height = ChartSize.DefaultImageHeight, TEntity entity = null) {
			entity = entity ?? GetEntityForGetValue();
			var streamContent = GetStreamContent(entity, width, height);
			var response = new HttpResponseMessage { Content = streamContent, StatusCode = HttpStatusCode.OK };
			return response;
		}

		/// <summary>
		/// Gets the content of the stream.
		/// </summary>
		/// <remarks>
		/// <para>This function supports GetEntityValue, resolving the API call that gets the data.</para>
		/// <para>This implementation uses .net Reflection to call an API function with this format
		/// on the businesslayer: [Entityname]_GetValue. The function is excepted to take the same
		/// 3 parameters as GetStreamContent itself, making this pretty limited to image oriented output.</para>
		/// <para>It is likely that non-Chart entities will override this method to supply
		/// their own API call.</para>
		/// </remarks>
		/// <param name="entity">The entity.</param>
		/// <param name="width">The Width of the chart</param>
		/// <param name="height">The height of the chart</param>
		/// <returns></returns>
		protected virtual StreamContent GetStreamContent(TEntity entity, int width, int height) {
			var methodGetValue = GetMethod(const_methodname_getvalue,
				new[] { typeof(TEntity), typeof(int), typeof(int) });
			var streamContent = methodGetValue.Invoke(BusinessLayer, new object[] { entity, width, height }) as StreamContent;
			return streamContent;
		}

		/// <summary>
		/// Gets the entity for get value method.
		/// </summary>
		/// <returns></returns>
		public virtual TEntity GetEntityForGetValue() {
			ODataPath oDataPath = Request.ODataProperties().Path;
			var key = (oDataPath.Segments[1] as KeyValuePathSegment).Value;
			var entity = GetEntities(key, null).FirstOrDefault();
			return entity;
		}

#if false
/// <summary>
/// This is a template for creating the correct implementation of the OPTIONS verb.
/// It is not complete nor tested.
/// </summary>
/// <returns></returns>
		[HttpOptions]
		public HttpResponseMessage Options()
		{
			var response = Request.CreateResponse(HttpStatusCode.OK);
			response.Headers.Add("Access-Control-Allow-Credentials", "true");
		// !!!WARNING: This has not been tested!!!
			response.Headers.Add("Access-Control-Allow-Origin", 
				this.Request.RequestUri.Scheme + this.Request.RequestUri.Host);
			response.Headers.Add("Access-Control-Allow-Headers", "authorization");
			return response;
		}
#endif

	}
}
﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.OData;
using System.Web.Http.OData.Extensions;
using System.Web.Http.OData.Routing;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessLayer.Interfaces;

namespace Vizelia.FOL.OData.Controllers {
	/// <summary>
	/// OData controller for the Location entity.
	/// </summary>
	public class LocationController : BaseController<Location, ICoreBusinessLayer> {
		
		/// <summary>
		/// Gets the LatestHistoricalPset values by location key
		/// </summary>
		/// <param name="location">The location</param>
		/// <returns></returns>
		[HttpGet]
		public List<LatestHistoricalPset> GetLatestHistoricalPsetValues([FromUri] Location location) {

			var key = ODataHelper.DenormalizeKey(GetEntityKey(), TypeName);
			var methodName = GetFullMethodName("GetLatestHistoricalPsetValues");

			location = RunMethod(() => BusinessLayer.Location_GetItem(key), methodName);
			if (location == null) {
				throw Request.EntityNotFound();
			}

			var result = RunMethod(() => BusinessLayer.GetLatestHistoricalPsetValuesByLocationKey(key), methodName);
			return result;
		}

		/// <summary>
		/// Get the LatestHistoricalPset values of locations by Pset
		/// </summary>
		/// <param name="keyPropertySet">The pset</param>
		/// <returns></returns>
		[HttpGet]
		public List<LatestHistoricalPset> GetLatestHistoricalPsetValuesByPset(string keyPropertySet) {

			keyPropertySet = ODataHelper.DenormalizeKey(keyPropertySet, "Pset");
			var methodName = GetFullMethodName("GetLatestHistoricalPsetValuesByPset");
			var result = RunMethod(() => BusinessLayer.GetLatestHistoricalPsetValuesOfLocationsByPset(keyPropertySet), methodName);
	
			return result;
		}

		private string GetEntityKey() {
			ODataPath oDataPath = Request.ODataProperties().Path;
			var keySegment = oDataPath.Segments[1] as KeyValuePathSegment;

			return keySegment != null ? keySegment.Value : null;
		}
	}
}
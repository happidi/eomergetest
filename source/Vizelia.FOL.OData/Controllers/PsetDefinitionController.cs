﻿using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessLayer.Interfaces;

namespace Vizelia.FOL.OData.Controllers {
	
	/// <summary>
	/// PsetDefinition Controller.
	/// </summary>
	public class PsetDefinitionController : BaseController<PsetDefinition, ICoreBusinessLayer> { 
	}
}

﻿using System.Linq;
using System.Web.Http;
using System.Web.Http.OData;
using System.Web.Http.OData.Query;
using AutoMapper.QueryableExtensions;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessLayer.Interfaces;
using Vizelia.FOL.DataLayer.EntityFramework.Entities;
using Vizelia.FOL.DataLayer.EntityFramework.Repositories;

namespace Vizelia.FOL.OData.Controllers {
	/// <summary>
	/// MeterData OData Web API Controller
	/// </summary>
	public class MeterDataController : BaseController<MeterData, IEnergyBusinessLayer> {
		private readonly IDataRepository<MeterDataEF> m_DataRepository;

		/// <summary>
		/// Initializes a new instance of the <see cref="MeterDataController"/> class.
		/// </summary>
		public MeterDataController()
			: this(new MeterDataRepository()) {
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="MeterDataController"/> class.
		/// </summary>
		/// <param name="meterDataRepository">The meter data repository.</param>
		public MeterDataController(IDataRepository<MeterDataEF> meterDataRepository) {
			m_DataRepository = meterDataRepository;
		}

		/// <summary>
		/// Gets the MeterData IQueryable.
		/// </summary>
		/// <param name="queryOptions">The query options.</param>
		/// <returns></returns>
		[EnableQuery(AllowedQueryOptions = AllowedQueryOptions.All, PageSize = const_default_page_size)]
		[HttpGet]
		public override IQueryable<MeterData> Get(ODataQueryOptions<MeterData> queryOptions) {
			var meterDatas = RunMethod(BusinessLayer.MeterDataEF_GetAll(m_DataRepository).Project().To<MeterData>, "MeterData_GetAll");
			return meterDatas;
		}

		/// <summary>
		/// Releases the unmanaged resources that are used by the object and, optionally, releases the managed resources.
		/// </summary>
		/// <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
		protected override void Dispose(bool disposing) {
			if (disposing) {
				m_DataRepository.Dispose();
			}
			base.Dispose(disposing);
		}
	}
}
﻿using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessLayer.Interfaces;

namespace Vizelia.FOL.OData.Controllers {
	/// <summary>
	/// OData controller for DrawingCanvasImage entity.
	/// </summary>
	public class DrawingCanvasImageController : BaseController<DrawingCanvasImage, IEnergyBusinessLayer> {
	}
}

﻿using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessLayer.Interfaces;

namespace Vizelia.FOL.OData.Controllers {
	/// <summary>
	/// OData controller for LocalizationResource entity.
	/// </summary>
	public class LocalizationResourceController : BaseController<LocalizationResource, ICoreBusinessLayer> {
	}
}

﻿using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessLayer.Interfaces;

namespace Vizelia.FOL.OData.Controllers {
	/// <summary>
	/// OData controller for PortalTab entity.
	/// </summary>
	public class PortalTabController : BaseController<PortalTab, IEnergyBusinessLayer> {
	}
}

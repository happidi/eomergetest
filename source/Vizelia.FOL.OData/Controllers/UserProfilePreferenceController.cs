﻿using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessLayer.Interfaces;

namespace Vizelia.FOL.OData.Controllers {
	/// <summary>
	/// OData controller for UserProfilePreference entity.
	/// </summary>
	public class UserProfilePreferenceController : BaseController<UserProfilePreference, ICoreBusinessLayer> {
	}
}

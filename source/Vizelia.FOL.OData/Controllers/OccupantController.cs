﻿using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessLayer.Interfaces;

namespace Vizelia.FOL.OData.Controllers {
	/// <summary>
	/// OData controller for Occupant entity.
	/// </summary>
	public class OccupantController : BaseController<Occupant, ICoreBusinessLayer> {
	}
}

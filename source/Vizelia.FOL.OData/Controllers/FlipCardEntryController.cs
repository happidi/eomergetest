﻿using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessLayer.Interfaces;

namespace Vizelia.FOL.OData.Controllers {
	/// <summary>
	/// OData controller for FlipCardEntry entity.
	/// </summary>
	public class FlipCardEntryController : BaseController<FlipCardEntry, IEnergyBusinessLayer> {
	}
}

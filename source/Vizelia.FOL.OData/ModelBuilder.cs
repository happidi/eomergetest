﻿using System.Web.Http.OData.Builder;
using Microsoft.Data.Edm;
using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.OData {

	/// <summary>
	/// Helper class to build the EdmModels by either explicit or implicit method.
	/// </summary>
	public static class ModelBuilder {

		/// <summary>
		/// Generates a model from a few seeds (i.e. the names and types of the entity sets)
		/// by applying conventions.
		/// </summary>
		/// <returns>An implicitly configured model</returns>    
		public static IEdmModel GetEdmModel() {
			ODataModelBuilder modelBuilder = new ODataConventionModelBuilder();

			AddCalendarEventCategory(modelBuilder);
			AddAlarmInstance(modelBuilder);
			AddMeterValidationRule(modelBuilder);
			AddEventLog(modelBuilder);
			AddStatisticalSerie(modelBuilder);
			AddCalculatedSerie(modelBuilder);
			AddFilterSpatialPset(modelBuilder);
			AddChartHistoricalAnalysis(modelBuilder);
			AddClassificationItem(modelBuilder);
			AddOccupant(modelBuilder);
			AddMeterOperation(modelBuilder);
			AddFolMembershipUser(modelBuilder);
			AddAlarmDefinition(modelBuilder);
			AddChart(modelBuilder);
			AddPset(modelBuilder);
			AddMeter(modelBuilder);
			AddLocation(modelBuilder);
			AddMeterData(modelBuilder);
			AddDataSerie(modelBuilder);
			AddChartAxis(modelBuilder);
			AddPortal(modelBuilder);
			AddAlarmTable(modelBuilder);
			AddDrawingCanvas(modelBuilder);
			AddMap(modelBuilder);
			AddWeatherLocation(modelBuilder);
			AddWebFrame(modelBuilder);
			AddFilpCard(modelBuilder);
			AddLocalizationResource(modelBuilder);
			AddUserProfilePreference(modelBuilder);
			AddCustomData(modelBuilder);
			AddPsetAttributeHistorical(modelBuilder);
			AddDataPoint(modelBuilder);
			AddImageMap(modelBuilder);
			modelBuilder.ComplexType<ChartCustomGridCell>();
			AddAnalytics(modelBuilder);
			RegisterODataValidationErrorElements(modelBuilder);

			return modelBuilder.GetEdmModel();
		}

		private static void AddLocalizationResource(ODataModelBuilder modelBuilder) {
			modelBuilder.EntitySet<LocalizationResource>("LocalizationResource");
		}

		private static void AddWebFrame(ODataModelBuilder modelBuilder) {
			modelBuilder.EntitySet<WebFrame>("WebFrame");
		}

		private static void AddWeatherLocation(ODataModelBuilder modelBuilder) {
			modelBuilder.EntitySet<WeatherLocation>("WeatherLocation");
		}

		private static void AddMap(ODataModelBuilder modelBuilder) {
			var maps = modelBuilder.EntitySet<Map>("Map");
			var map = maps.EntityType;
			map.Ignore(x => x.KeyClassificationItemAlarmDefinitionPath);
			map.Ignore(x => x.ClassificationItemAlarmDefinitionLongPath);
			map.Ignore(x => x.ClassificationItemAlarmDefinitionLocalIdPath);
			map.Ignore(x => x.LocationLongPath);
			map.Ignore(x => x.LocationShortPath);
		}

		private static void AddAlarmTable(ODataModelBuilder modelBuilder) {
			modelBuilder.EntitySet<AlarmTable>("AlarmTable");
		}

		private static void AddStatisticalSerie(ODataModelBuilder modelBuilder) {
			modelBuilder.EntitySet<StatisticalSerie>("StatisticalSerie");
		}

		private static void AddEventLog(ODataModelBuilder modelBuilder) {
			modelBuilder.EntitySet<EventLog>("EventLog");
		}

		private static void AddMeterValidationRule(ODataModelBuilder modelBuilder) {
			modelBuilder.EntitySet<MeterValidationRule>("MeterValidationRule");
		}

		private static void AddAlarmInstance(ODataModelBuilder modelBuilder) {
			var alarmInstances = modelBuilder.EntitySet<AlarmInstance>("AlarmInstance");
			var alarmInstance = alarmInstances.EntityType;
			alarmInstance.Ignore(x => x.ChartInstanceDateTime);
			alarmInstance.Ignore(x => x.UTCInstanceDateTime);
		}

		private static void AddCalendarEventCategory(ODataModelBuilder modelBuilder) {
			var calendarEventCategories = modelBuilder.EntitySet<CalendarEventCategory>("CalendarEventCategory");
			var calendarEventCategory = calendarEventCategories.EntityType;
			calendarEventCategory.Ignore(x => x.ColorId);
		}

		private static void AddImageMap(ODataModelBuilder modelBuilder) {
			var imageMap = modelBuilder.ComplexType<ImageMap>();
			imageMap.Ignore(x => x.Chart);
		}

		private static void AddDataPoint(ODataModelBuilder modelBuilder) {
			var dataPoint = modelBuilder.ComplexType<DataPoint>();
			dataPoint.Ignore(x => x.Color);
		}

		private static void AddPsetAttributeHistorical(ODataModelBuilder modelBuilder) {
			var psetAttributeHistoricals = modelBuilder.EntitySet<PsetAttributeHistorical>("PsetAttributeHistorical");
			var psetAttributeHistorical = psetAttributeHistoricals.EntityType;
			psetAttributeHistorical.Ignore(x => x.IfcType);
			psetAttributeHistorical.Ignore(x => x.UsageName);

			modelBuilder.ComplexType<LatestHistoricalPset>();
		}

		private static void AddUserProfilePreference(ODataModelBuilder modelBuilder) {
			var userProfilePreferences = modelBuilder.EntitySet<UserProfilePreference>("UserProfilePreference");
			var userProfilePreference = userProfilePreferences.EntityType;
			userProfilePreference.Ignore(x => x.Creator);
			userProfilePreference.Ignore(x => x.KeyUser);
		}
			
		private static void AddFilpCard(ODataModelBuilder modelBuilder) {
			var flipCards = modelBuilder.EntitySet<FlipCard>("FlipCard");
			var flipCardEntries = modelBuilder.EntitySet<FlipCardEntry>("FlipCardEntry");
			var flipCardEntry = flipCardEntries.EntityType;
			flipCardEntry.Ignore(x => x.Entity);
		}

		private static void AddPset(ODataModelBuilder modelBuilder) {
			var psetDefinitions = modelBuilder.EntitySet<PsetDefinition>("PsetDefinition");
			var psetDefinition = psetDefinitions.EntityType;
			psetDefinition.Ignore(x => x.LocalId);
			psetDefinition.Ignore(x => x.IfcType);

			var psetAttributeDefinitions = modelBuilder.EntitySet<PsetAttributeDefinition>("PsetAttributeDefinition");
			var psetAttributeDefinition = psetAttributeDefinitions.EntityType;
			psetAttributeDefinition.Ignore(x => x.LocalId);
			psetAttributeDefinition.Ignore(x => x.IfcType);

			modelBuilder.EntitySet<PsetValue>("PsetValue");
		}

		private static void AddAlarmDefinition(ODataModelBuilder modelBuilder) {
			var alarmDefinitions = modelBuilder.EntitySet<AlarmDefinition>("AlarmDefinition");
			alarmDefinitions.EntityType.Ignore(x => x.Charts);
			alarmDefinitions.EntityType.Ignore(x => x.ConnectedCharts);
		}

		private static void AddPortal(ODataModelBuilder modelBuilder) {
			var portalWindows = modelBuilder.EntitySet<PortalWindow>("PortalWindow");
			portalWindows.EntityType.Ignore(x => x.CreatorLogin);
			var portalTabs = modelBuilder.EntitySet<PortalTab>("PortalTab");
			var portalTab = portalTabs.EntityType;
			portalTab.Ignore(x => x.VirtualFile); // avoids including VirtualFile complex type
			var portalColumns = modelBuilder.EntitySet<PortalColumn>("PortalColumn");
			var portlets = modelBuilder.EntitySet<Portlet>("Portlet");
			var portlet = portlets.EntityType;
			portlet.Ignore(x => x.Entity);
		}

		private static void AddDrawingCanvas(ODataModelBuilder modelBuilder) {
			var drawingCanvases = modelBuilder.EntitySet<DrawingCanvas>("DrawingCanvas");
			var drawingCanvas = drawingCanvases.EntityType;
			drawingCanvas.Ignore(x => x.Application);
			drawingCanvas.Ignore(x => x.Items);

			var drawingCanvasImages = modelBuilder.EntitySet<DrawingCanvasImage>("DrawingCanvasImage");
			var drawingCanvasImage = drawingCanvasImages.EntityType;
			drawingCanvasImage.Ignore(x => x.Application);
			drawingCanvasImage.Ignore(x => x.Title);

			var drawingCanvasCharts = modelBuilder.EntitySet<DrawingCanvasChart>("DrawingCanvasChart");
			var drawingCanvasChart = drawingCanvasCharts.EntityType;
			drawingCanvasChart.Ignore(x => x.Application);
			drawingCanvasChart.Ignore(x => x.Title);
		}

		private static void AddFolMembershipUser(ODataModelBuilder modelBuilder) {
			var folMembershipUsers = modelBuilder.EntitySet<FOLMembershipUser>("User");
			folMembershipUsers.EntityType.Ignore(x => x.ProviderUserKey);
			folMembershipUsers.EntityType.Ignore(x => x.KeyLocation);
			folMembershipUsers.EntityType.Ignore(x => x.ApplicationName);
			folMembershipUsers.EntityType.Ignore(x => x.Preferences);
		}

		private static void AddChartAxis(ODataModelBuilder modelBuilder) {
			var chartAxis = modelBuilder.EntitySet<ChartAxis>("ChartAxis");
			var chartAxisEntity = chartAxis.EntityType;
			chartAxisEntity.Ignore(x => x.Markers); // avoids including ChartMarkers complex type
		}
	
		private static void AddDataSerie(ODataModelBuilder modelBuilder) {
			var dataSeries = modelBuilder.EntitySet<DataSerie>("DataSerie");
			var dataSerie = dataSeries.EntityType;
			dataSerie.Ignore(x => x.ColorElements); // avoids including DataSerieColorElement complex type
			dataSerie.Ignore(x => x.PsetRatios); // avoids including DataSeriePsetRatio complex type
			dataSerie.Ignore(x => x.Points);
				// we retain PointsWithValue which contains the same data, avoiding massive lists of duplicate data
		}
			
		private static void AddMeterData(ODataModelBuilder modelBuilder) {
			var meterDatas = modelBuilder.EntitySet<MeterData>("MeterData");
			var meterData = meterDatas.EntityType;
			meterData.Ignore(x => x.IfcType);
			meterData.Ignore(x => x.MeterLocalId);
			meterData.Ignore(x => x.AcquisitionDateTimeLocal);
			meterData.Ignore(x => x.TimeZoneLocal);
			meterData.Ignore(x => x.KeyMeterData);
			meterData.Property(x => x.Key);
		}
			
		private static void AddMeter(ODataModelBuilder modelBuilder) {
			var meters = modelBuilder.EntitySet<Meter>("Meter");
			var meter = meters.EntityType;
			meter.Ignore(x => x.Application);
			meter.Ignore(x => x.IfcType);
			meter.Ignore(x => x.IsMeterDataRollupFixed);
			meter.Ignore(x => x.LocationTypeName);
			meter.Ignore(x => x.Data);
			meter.Ignore(x => x.PropertySetList);

			meter.HasMany(p => p.PsetValues);
		}

		private static void AddLocation(ODataModelBuilder modelBuilder) {
			var locations = modelBuilder.EntitySet<Location>("Location");
			var location = locations.EntityType;
			location.Ignore(x => x.IfcType);
			location.Ignore(x => x.TypeName);
			location.Ignore(x => x.LocalIdPath);
			location.Ignore(x => x.KeyLocationPath);
			location.Ignore(x => x.LongPath);
			location.Ignore(x => x.ShortPath);
			location.Ignore(x => x.PropertySetList);
			location.HasMany(p => p.PsetValues);

			location.Collection.Action("LatestHistoricalPsetValuesByPset");
			location.Action("LatestHistoricalPsetValues");
		}

		private static void AddCustomData(ODataModelBuilder modelBuilder) {
			var customDatas = modelBuilder.EntitySet<CustomData>("CustomData");
			var customData = customDatas.EntityType;
			var customDataDeleteAllAction = customData.Collection.Action("DeleteAll");
			customDataDeleteAllAction.Parameter<string>("containerName");
			customDataDeleteAllAction.Parameter<string>("keyUser");
		}

		private static void AddAnalytics(ODataModelBuilder modelBuilder) {
			var analytics = modelBuilder.EntitySet<Analytics>("Analytics");
			analytics.EntityType.Action("Image");
			analytics.EntityType.Collection.Action("Image");

			analytics.EntityType.Action("HtmlMap");
			analytics.EntityType.Collection.Action("HtmlMap");

			analytics.EntityType.Collection.Action("CachedImageByGuid");

			analytics.EntityType.Action("DataPoints");
			analytics.EntityType.Collection.Action("DataPoints");
			
			analytics.EntityType.Action("CustomGrid");
			analytics.EntityType.Collection.Action("CustomGrid");

			analytics.EntityType.Action("Html5");
			analytics.EntityType.Collection.Action("Html5");

			analytics.EntityType.Action("Html");
			analytics.EntityType.Collection.Action("Html");
		}

		private static void AddMeterOperation(ODataModelBuilder modelBuilder) {
			var meterOperations = modelBuilder.EntitySet<MeterOperation>("MeterOperation");
			var meterOperation = meterOperations.EntityType;
			meterOperation.Ignore(x => x.IfcType);
		}

		private static void AddOccupant(ODataModelBuilder modelBuilder) {
			var occupants = modelBuilder.EntitySet<Occupant>("Occupant");
			var occupant = occupants.EntityType;
			occupant.Ignore(x => x.IfcType);
			occupant.Ignore(x=> x.PropertySetList);
		}

		private static void AddCalculatedSerie(ODataModelBuilder modelBuilder) {
			var calculatedSeries = modelBuilder.EntitySet<CalculatedSerie>("CalculatedSerie");
			var calculatedSerie = calculatedSeries.EntityType;
			calculatedSerie.Ignore(x => x.KeyLocation);
			calculatedSerie.Ignore(x => x.RunSuccessfully);
		}

		private static void AddFilterSpatialPset(ODataModelBuilder modelBuilder) {
			var filterSpatialPsets = modelBuilder.EntitySet<FilterSpatialPset>("FilterSpatialPset");
			var filterSpatialPset = filterSpatialPsets.EntityType;
			filterSpatialPset.Ignore(x => x.KeyAlarmDefinition);
		}

		private static void AddChartHistoricalAnalysis(ODataModelBuilder modelBuilder) {
			var chartHistoricalAnalyses = modelBuilder.EntitySet<ChartHistoricalAnalysis>("ChartHistoricalAnalysis");
			var chartHistoricalAnalysis = chartHistoricalAnalyses.EntityType;
			chartHistoricalAnalysis.Ignore(x => x.StartDate);
			chartHistoricalAnalysis.Ignore(x => x.EndDate);
		}

		private static void AddClassificationItem(ODataModelBuilder modelBuilder) {
			var classificationItems = modelBuilder.EntitySet<ClassificationItem>("ClassificationItem");
			var classificationItem = classificationItems.EntityType;
			classificationItem.Ignore(x => x.IfcType);
			classificationItem.Ignore(x => x.LocalIdPath);
			classificationItem.Ignore(x => x.LongPath);
			classificationItem.Ignore(x => x.Relation);
			classificationItem.Ignore(x => x.Code);
		}

		private static void AddChart(ODataModelBuilder modelBuilder) {
			var charts = modelBuilder.EntitySet<Chart>("Chart");
			var chartEntityType = charts.EntityType;
			chartEntityType.Ignore(x => x.Application);
			chartEntityType.Ignore(x => x.AlarmInstances);
			chartEntityType.Ignore(x => x.Series);
			chartEntityType.Ignore(x => x.Axis);
			chartEntityType.Ignore(x => x.Errors);
			chartEntityType.Ignore(x => x.Warnings);
			chartEntityType.Ignore(x => x.HistoricalAnalysis);
			chartEntityType.Ignore(x => x.ScriptAlarmInstances);
			chartEntityType.Ignore(x => x.CorrelationRemovedPoints);
			chartEntityType.Ignore(x => x.ChartMarkers); // avoids including ChartMarker complex type
			chartEntityType.Ignore(x => x.DynamicDisplay); // avoids including DynamicDisplay complex type
			chartEntityType.Ignore(x => x.ChartAreaPictureImage); // avoids including DynamicDisplayImage complex type
			chartEntityType.Ignore(x => x.DynamicDisplayFooterPictureImage); // avoids including DynamicDisplayImage complex type
			chartEntityType.Ignore(x => x.DynamicDisplayHeaderPictureImage); // avoids including DynamicDisplayImage complex type
			chartEntityType.Ignore(x => x.DynamicDisplayMainPictureImage); // avoids including DynamicDisplayImage complex type
			chartEntityType.Ignore(x => x.VirtualFile); // avoids including VirtualFile complex type
			chartEntityType.Ignore(x => x.VirtualFilePath);
			chartEntityType.Ignore(x => x.Palette); // avoids including Palette complex type
			chartEntityType.Ignore(x => x.EnergyCertificate); // avoids including EnergyCertificate complex type
			chartEntityType.Ignore(x => x.HistoricalPsets); // avoids including ChartPsetAttributeHistorical complex type
			chartEntityType.Ignore(x => x.ChartCustomGridCells); // avoids including ChartCustomGridCell complex type
			chartEntityType.Ignore(x => x.DrillDown); // avoids including ChartDrillDown complex type
			chartEntityType.Ignore(x => x.CalendarViewSelections); // avoids including ChartCalendarViewSelection complex type
			chartEntityType.Ignore(x => x.Algorithms);
				// avoids including ChartAlgorithm, Algorithm, and AlgorithmInputValue complex types
			chartEntityType.Ignore(x => x.ResultingMeters);
				// no function will populate this. It is the result of running the Energy Aggregator
		}

		/// <summary>
		/// Used by GetEdmModel to include the complex types associated with ODataErrors.
		/// </summary>
		/// <param name="modelBuilder"></param>
		public static void RegisterODataValidationErrorElements(ODataModelBuilder modelBuilder) {
			modelBuilder.ComplexType<ODataValidationError>();
			modelBuilder.ComplexType<ValidationResultDetailWithChildren>();
			modelBuilder.ComplexType<ValidationResultDetail>();
			modelBuilder.ComplexType<ODataExceptionError>();
		}
#if false // not used, but could be used when a complex type needs to hide properties by using the BrowsableAttribute
		/// <summary>
		/// Utility to add properties to any StructuralTypeConfiguration (such as ComplexTypeConfiguration
		/// and EntitySetConfiguration). Pass in the type to resolve. If public properties should not be included,
		/// assign the BrowsableAttribute(false) to them.
		/// </summary>
		/// <remarks>
		/// <para>For any collection or complex type property, ensure their class is also registered
		/// with the ModelBuilder in order to fully include it. AddProperties will not attempt to 
		/// register children of collections or complex type properties.</para>
		/// </remarks>
		/// <param name="typeToResolve"></param>
		/// <param name="config"></param>
		static void AddProperties(Type typeToResolve, StructuralTypeConfiguration config) {
			PropertyInfo[] properties = typeToResolve.GetProperties(BindingFlags.Public | BindingFlags.Instance);
			foreach (PropertyInfo pi in properties) {
				Type propertyType = pi.PropertyType;
				BrowsableAttribute bAtt = propertyType.GetCustomAttribute<BrowsableAttribute>(true);
				if ((bAtt != null) && !bAtt.Browsable) {
					continue;
				}
				if (propertyType.IsPrimitive ||
					(propertyType == typeof(String)) || (propertyType == typeof(DateTime)) || (propertyType == typeof(TimeSpan)))
					config.AddProperty(pi);
				else if (typeof(IEnumerable).IsAssignableFrom(propertyType))
					config.AddCollectionProperty(pi);
				else if (propertyType.IsClass) {
					config.AddComplexProperty(pi);
				}
			}
		}
#endif
	}
}
﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Formatting;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Dispatcher;
using System.Web.Http.OData;
using System.Web.Http.OData.Extensions;
using System.Web.Http.OData.Formatter;
using System.Web.Http.OData.Query;
using System.Web.Http.OData.Routing.Conventions;
using Vizelia.FOL.Common.Configuration;


namespace Vizelia.FOL.OData {
/// <summary>
/// Sets up the httpConfiguration object during OData initialization.
/// </summary>
	public static class WebApiConfig {

		/// <summary>
		/// Primary entry point. Checks CORS; sets Route Handlers; sets Formatters; sets options.
		/// </summary>
		/// <param name="config"></param>
		public static void Register(HttpConfiguration config) {
			var allowedOrigins = string.Join(",", VizeliaConfiguration.Instance.ApiCorsAllowedUrls.Cast<ApiCorsAllowedUrlsElement>().Select(x => x.SiteUrl));
			if (!string.IsNullOrEmpty(allowedOrigins)) {
				var cors = new EnableCorsAttribute(allowedOrigins, "*", "*") {
					SupportsCredentials = true
				};
				config.EnableCors(cors);
			}

			SetupRoutingHandlers(config);
			SetupFormatters(config);

			config.AddODataQueryFilter(new EnableQueryAttribute{ AllowedQueryOptions = AllowedQueryOptions.All });
			config.IncludeErrorDetailPolicy = IncludeErrorDetailPolicy.Always;

			AutoMapperConfig.Register();
		}

		static void SetupFormatters(HttpConfiguration config) {
			config.Formatters.Clear();

			config.Formatters.AddRange(ODataMediaTypeFormatters.Create());
			// The default ODataMediaTypeFormatter for JSON is only missing MetadataDocument.
			/* 	Support for $metadata in json not available until ODataMetadataSerializer handles json
					config.Formatters.Add(CreateApplicationJson(ODataPayloadKind.MetadataDocument));
			 */
			// The default ODataMediaTypeFormatter for application/xml is only missing Entry, Feed,
			/* Support for EntitySet(key) not available until ODataEntityTypeSerializer handles XML for Entry
					config.Formatters.Add(CreateApplicationXml(ODataPayloadKind.Entry));
			 */

			/* Support for EntitySet as a list not available until ODataFeedSerializer handles XML for Feed
					config.Formatters.Add(CreateApplicationXml(ODataPayloadKind.Feed));
			 */

			ParameterDrivenContentNegotiator contentNegotiator = new ParameterDrivenContentNegotiator();
			config.Services.Replace(typeof(IContentNegotiator), contentNegotiator);
		}

#if false
		/// <summary>
		/// Lifted from ODataMediaTypeFormatters because we needed to slightly modify the list of
		/// ODataPayloadKinds, but the value on ODataMediaTypeFormatter and methods that create it are private.
		/// </summary>
		/// <remarks>
		/// <para>The default ODataMediaTypeFormatter for JSON is only missing MetadataDocument.</para>
		/// </remarks>
		/// <returns></returns>
		static ODataMediaTypeFormatter CreateApplicationJson(params ODataPayloadKind[] payloads) {
			ODataMediaTypeFormatter formatter = CreateFormatterWithoutMediaTypes(
				DefaultODataSerializerProvider.Instance, 
				DefaultODataDeserializerProvider.Instance, payloads);

			// Add minimal metadata as the first media type so it gets used when the request doesn't
			// ask for a specific content type

			formatter.SupportedMediaTypes.Add(MediaTypeHeaderValue.Parse("application/json;odata=minimalmetadata;streaming=true"));
			formatter.SupportedMediaTypes.Add(MediaTypeHeaderValue.Parse("application/json;odata=minimalmetadata;streaming=false"));
			formatter.SupportedMediaTypes.Add(MediaTypeHeaderValue.Parse("application/json;odata=minimalmetadata"));
			formatter.SupportedMediaTypes.Add(MediaTypeHeaderValue.Parse("application/json;odata=fullmetadata;streaming=true"));
			formatter.SupportedMediaTypes.Add(MediaTypeHeaderValue.Parse("application/json;odata=fullmetadata;streaming=false"));
			formatter.SupportedMediaTypes.Add(MediaTypeHeaderValue.Parse("application/json;odata=fullmetadata"));
			formatter.SupportedMediaTypes.Add(MediaTypeHeaderValue.Parse("application/json;odata=nometadata;streaming=true"));
			formatter.SupportedMediaTypes.Add(MediaTypeHeaderValue.Parse("application/json;odata=nometadata;streaming=false"));
			formatter.SupportedMediaTypes.Add(MediaTypeHeaderValue.Parse("application/json;odata=nometadata"));
			formatter.SupportedMediaTypes.Add(MediaTypeHeaderValue.Parse("application/json;odata=verbose"));
			formatter.SupportedMediaTypes.Add(MediaTypeHeaderValue.Parse("application/json;streaming=true"));
			formatter.SupportedMediaTypes.Add(MediaTypeHeaderValue.Parse("application/json;streaming=false"));

			formatter.SupportedMediaTypes.Add(MediaTypeHeaderValue.Parse("application/json"));
			return formatter;
		}

		/// <summary>
		/// Lifted from ODataMediaTypeFormatters because we needed to slightly modify the list of
		/// ODataPayloadKinds, but the value on ODataMediaTypeFormatter and methods that create it are private.
		/// </summary>
		/// <param name="payloads"The default ODataMediaTypeFormatter for XML is only missing Feed, Entry, and Parameter.></param>
		/// <returns></returns>
		static ODataMediaTypeFormatter CreateApplicationXml(params ODataPayloadKind[] payloads) {
			ODataMediaTypeFormatter formatter = CreateFormatterWithoutMediaTypes(
				DefaultODataSerializerProvider.Instance,
				DefaultODataDeserializerProvider.Instance,
				payloads);

			formatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("application/xml"));
			return formatter;
		}

		/// <summary>
		/// Lifted from ODataMediaTypeFormatters because we needed to slightly modify the list of
		/// ODataPayloadKinds, but the value on ODataMediaTypeFormatter and methods that create it are private.
		/// </summary>
		/// <remarks>
		/// <para>The default ODataMediaTypeFormatter for Text/XML is only missing 
		/// Feed, Entry, ServiceDocument, Error, MetadataDocument, and Parameter.</para>
		/// </remarks>
		/// <returns></returns>
		static ODataMediaTypeFormatter CreateTextXml(params ODataPayloadKind[] payloads) {
			ODataMediaTypeFormatter formatter = CreateFormatterWithoutMediaTypes(
				DefaultODataSerializerProvider.Instance,
				DefaultODataDeserializerProvider.Instance,
				payloads);

			formatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/xml"));
			return formatter;
		}
#endif

#if false
		private static ODataMediaTypeFormatter CreateApplicationAtomSvcXml() {
			ODataMediaTypeFormatter formatter = CreateFormatterWithoutMediaTypes(
				DefaultODataSerializerProvider.Instance,
				DefaultODataDeserializerProvider.Instance,
				ODataPayloadKind.ServiceDocument);
			formatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("application/atomsvc+xml"));
			return formatter;
		}

		private static ODataMediaTypeFormatter CreateApplicationAtomXmlTypeEntry() {
			ODataMediaTypeFormatter formatter = CreateFormatterWithoutMediaTypes(
				DefaultODataSerializerProvider.Instance,
				DefaultODataDeserializerProvider.Instance,
				ODataPayloadKind.Entry);
			formatter.SupportedMediaTypes.Add(MediaTypeHeaderValue.Parse("application/atom+xml;type=entry"));
			formatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("application/atom+xml"));
			return formatter;
		}

		private static ODataMediaTypeFormatter CreateApplicationAtomXmlTypeFeed() {
			ODataMediaTypeFormatter formatter = CreateFormatterWithoutMediaTypes(
				DefaultODataSerializerProvider.Instance,
				DefaultODataDeserializerProvider.Instance,
				ODataPayloadKind.Feed);
			formatter.SupportedMediaTypes.Add(MediaTypeHeaderValue.Parse("application/atom+xml;type=feed"));
			formatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("application/atom+xml"));
			return formatter;
		}
		/// <summary>
		/// Lifted from ODataMediaTypeFormatters to create a ODataMediaTypeFormatter that handles
		/// ODataPrimitiveValueMediaTypeMapping and ODataBinaryValueMediaTypeMapping
		/// </summary>
		/// <returns></returns>
		static ODataMediaTypeFormatter CreateRawValue() {
			ODataMediaTypeFormatter formatter = CreateFormatterWithoutMediaTypes(
				DefaultODataSerializerProvider.Instance,
				DefaultODataDeserializerProvider.Instance,
				ODataPayloadKind.Value);
			formatter.MediaTypeMappings.Add(new ODataPrimitiveValueMediaTypeMapping());
			formatter.MediaTypeMappings.Add(new ODataBinaryValueMediaTypeMapping());
			return formatter;
		}

#endif
#if false
		static ODataMediaTypeFormatter CreateFormatterWithoutMediaTypes(
			ODataSerializerProvider serializerProvider, ODataDeserializerProvider deserializerProvider, params ODataPayloadKind[] payloadKinds) {
			ODataMediaTypeFormatter formatter = new ODataMediaTypeFormatter(deserializerProvider, serializerProvider, payloadKinds);
			AddSupportedEncodings(formatter);
			return formatter;
		}

		private static void AddSupportedEncodings(MediaTypeFormatter formatter) {
			formatter.SupportedEncodings.Add(new System.Text.UTF8Encoding(encoderShouldEmitUTF8Identifier: false,
				throwOnInvalidBytes: true));
			formatter.SupportedEncodings.Add(new System.Text.UnicodeEncoding(bigEndian: false, byteOrderMark: true,
				throwOnInvalidBytes: true));
		}
#endif

		/// <summary>
		/// Sets up Route handlers and the tools that associates them with controllers.
		/// </summary>
		/// <param name="config"></param>
		static void SetupRoutingHandlers(HttpConfiguration config) {
			PathAndRouteConventionHandler pathAndRouteHandler = new PathAndRouteConventionHandler();    // this also implements our IODataRoutingConvention 
			pathAndRouteHandler.RouteHandlers.Add(new EntitySetKeyNavigationRouteHandler());
			pathAndRouteHandler.RouteHandlers.Add(new EntitySetKeyPropertyRouteHandler());
			pathAndRouteHandler.RouteHandlers.Add(new EntitySetKeyPropertyValueRouteHandler());
			pathAndRouteHandler.RouteHandlers.Add(new EntitySetKeyUnresolvedRouteHandler());
			pathAndRouteHandler.RouteHandlers.Add(new EntitySetKeyActionRouteHandler());
			pathAndRouteHandler.RouteHandlers.Add(new EntitySetPropertyActionRouteHandler());
			pathAndRouteHandler.RouteHandlers.Add(new EntitySetActionRouteHandler());
			pathAndRouteHandler.RouteHandlers.Add(new EntitySetRouteHandler());
#if false// this was for a future feature called "entityset/$metadata. See also MetadataRouteHandler
			pathAndRouteHandler.RouteHandlers.Add(new MetadataRouteHandler());
#endif
			IList<IODataRoutingConvention> routingConventions = ODataRoutingConventions.CreateDefault();
			routingConventions.Insert(0, pathAndRouteHandler);

			config.Routes.MapODataServiceRoute("ODataRoute", "api/v1", ModelBuilder.GetEdmModel(), pathAndRouteHandler, routingConventions);


		}
	}
}

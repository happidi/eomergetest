﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Data.OData;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using Microsoft.Practices.EnterpriseLibrary.Validation.PolicyInjection;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Localization;

namespace Vizelia.FOL.OData {

	/// <summary>
	/// Extended ODataError that generates a list of Errors from the exception.ValidationResults.
	/// </summary>
	/// <remarks>
	/// <para>This class must be registered with the EdmModel as a complex type.</para>
	/// </remarks>
	public class ODataValidationError : BaseODataError {

		/// <summary>
		/// Constructor. Converts the exception into the ValidationErrors collection.
		/// </summary>
		/// <param name="exception"></param>
		public ODataValidationError(ArgumentValidationException exception)
			: base() {
			ErrorCode = Langue.error_odata_validation_code;
			Message = Langue.error_odata_validation_msg;
			MessageLanguage = Helper.GetCurrentUICulture();
			ValidationErrors = new List<BaseValidationResultDetail>();
			foreach (ValidationResult validationResult in exception.ValidationResults) {
				if (validationResult.NestedValidationResults != null)
					ValidationErrors.Add(new ValidationResultDetailWithChildren(validationResult));
				else
					ValidationErrors.Add(new ValidationResultDetail(validationResult));

			}
		}

		/// <summary>
		/// The errors that will be shown to the user.
		/// </summary>
		public List<BaseValidationResultDetail> ValidationErrors { get; protected set; }

	}

	/// <summary>
	/// Holds the details from a ValidationResult that we want to expose.
	/// Its property names will be shown to the user.
	/// </summary>
	/// <remarks>
	/// <para>This class must be registered with the EdmModel as a complex type.</para>
	/// </remarks>
	[Serializable]	// ensure GETters and SETers are public to include in serialization
	abstract public class BaseValidationResultDetail {
		/// <summary>
		/// Constructor. Converts the ValidationResult into its properties.
		/// </summary>
		/// <param name="validationResult"></param>
		public BaseValidationResultDetail(ValidationResult validationResult) {
			PropertyName = validationResult.Key;
			Message = validationResult.Message;
			ValidationRule = ValidationRuleNamer.ConvertToRuleName(validationResult);

		}
		/// <summary>
		/// The property name associated with the message
		/// </summary>
		public string PropertyName { get; set; }
		/// <summary>
		/// The message from the validator
		/// </summary>
		public string Message { get; set; }
		/// <summary>
		/// The identity of the validation rule.
		/// </summary>
		public string ValidationRule { get; set; }

	}
	/// <summary>
	/// Holds the details from a ValidationResult that we want to expose.
	/// Its property names will be shown to the user.
	/// The type of ValidationResult has nested ValidationResults.
	/// </summary>
	/// <remarks>
	/// <para>
	/// Only one level of children allowed. This restriction allows this type to be 
	/// registered into the EdmModel as a complex type.
	/// For details, see:
	/// http://stackoverflow.com/questions/21535114/odata-exception-a-recursive-loop-of-complex-types-is-not-allowed</para>
	/// <para>This class must be registered with the EdmModel as a complex type.</para>
	/// </remarks>
	public class ValidationResultDetailWithChildren : BaseValidationResultDetail {
		/// <summary>
		/// Constructor. Converts the ValidationResult into its properties.
		/// </summary>
		/// <param name="validationResult"></param>
		public ValidationResultDetailWithChildren(ValidationResult validationResult)
			: base(validationResult) {
			if (validationResult.NestedValidationResults != null) {
				NestedValidationErrors = new List<ValidationResultDetail>();
				foreach (ValidationResult innerResult in validationResult.NestedValidationResults) {
					NestedValidationErrors.Add(new ValidationResultDetail(innerResult));
				}
			}
		}

		/// <summary>
		/// If there are ValidationResult.NestedValidationResults, they are included here.
		/// Null when there are none.
		/// </summary>
		public List<ValidationResultDetail> NestedValidationErrors { get; protected set; }

	}

	/// <summary>
	/// Holds the details from a ValidationResult that we want to expose.
	/// Its property names will be shown to the user.
	/// It does not support child ValidationResults.
	/// </summary>
	/// <remarks>
	/// <para>This class must be registered with the EdmModel as a complex type.</para>
	/// <para>Normally we'd use a single class, ValidationResultDetail, to handle
	/// everything, but it would be implemented like ValidationResultDetailWithChildren.
	/// It is illegal to declare the same class as a child of the base when
	/// using ComplexType registration. So we've created separate classes to avoid
	/// this problem.</para>
	/// </remarks>
	public class ValidationResultDetail : BaseValidationResultDetail {
/// <summary>
/// Constructor
/// </summary>
/// <param name="validationResult"></param>
		public ValidationResultDetail(ValidationResult validationResult)
			: base(validationResult) {
		}
	}

	/// <summary>
	/// Converts the ValidationResult into a constant name representing the validator.
	/// THIS WILL NEED TO BE UPDATED OR REDESIGNED AS NEW VALIDATORS ARE INTRODUCED.
	/// </summary>
	public class ValidationRuleNamer {
		/// <summary>
		/// Rule name for Required validator
		/// </summary>
		public const string m_RuleNameRequired = "Required";
		/// <summary>
		/// Rule name for Xss validators
		/// </summary>
		public const string m_RuleNameXss = "Xss";
		/// <summary>
		/// Rule name for StringLength validators
		/// </summary>
		public const string m_RuleNameStringLength = "StringLength";
		/// <summary>
		/// Rule name for when there is no clear validator
		/// </summary>
		public const string m_RuleNameUnknown = "Unknown";

		/// <summary>
		/// Rule name for Constraint validators
		/// </summary>
		public const string m_RuleNameConstraintViolation = "Constraint";

		/// <summary>
		/// Rule name for NotNullValidator
		/// </summary>
		public const string m_RuleNameNotNullViolation = "NotNull";

		/// <summary>
		/// Returns the validation rule name.
		/// </summary>
		/// <param name="validationResult"></param>
		/// <returns></returns>
		public static string ConvertToRuleName(ValidationResult validationResult) {
			if (validationResult.Validator == null)
				return m_RuleNameUnknown;
			if (validationResult.Validator is StringLengthValidator)	// includes VizStringLengthValidator
				return m_RuleNameStringLength;
			if (validationResult.Validator is NotNullValidator)
				return m_RuleNameNotNullViolation;
			if ((validationResult.Validator is Vizelia.FOL.Common.Validators.AntiXssValidator) ||
				(validationResult.Validator is Vizelia.FOL.Common.Validators.AntiXssListValidator))
				return m_RuleNameXss;
			ValidationAttributeValidator vav = validationResult.Validator as ValidationAttributeValidator;
			if (vav != null) {
				// this is a flawed approach, but it returns "Required" because at the 
				// time of this development, System.ComponentModel.DataAnnotions.RequiredAttribute
				// is the only ValidationAttribute it will return.
				// We should abandon this ValidationAttributeValidator because it hides
				// a list of all validators that may be associated with the message.
				// Instead, we should ensure there are Enterprise Library validators for every case,
				// or entirely switch to the System.ComponentModel.DataAnnotions validation system.
				return m_RuleNameRequired;
			}
			if (validationResult.Validator is Vizelia.FOL.Common.Validators.ConstraintValidator)
				return m_RuleNameConstraintViolation;
			return validationResult.Validator.GetType().Name.Replace("Validator", "");
		}
	}

}

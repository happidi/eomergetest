﻿using AutoMapper;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.DataLayer.EntityFramework.Entities;

namespace Vizelia.FOL.OData {
	/// <summary>
	/// Auto Mapper config.
	/// </summary>
	public class AutoMapperConfig {
		/// <summary>
		/// Registers this instance.
		/// </summary>
		public static void Register() {
			Mapper.CreateMap<MeterDataEF, MeterData>()
				.ForMember(x => x.KeyMeterData, o => o.Ignore())
				.ForMember(x => x.Key, o => o.MapFrom(t => t.KeyMeterData))
				.ForMember(x => x.Validity, o => o.MapFrom(t => (MeterDataValidity)t.Validity));
		}
	}
}
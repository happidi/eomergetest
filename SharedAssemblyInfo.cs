﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyCompany("Schneider Electric")]
[assembly: AssemblyProduct("Energy Operation")]
[assembly: AssemblyCopyright("© Schneider Electric 2014")]
[assembly: AssemblyConfiguration("Debug")]
//[assembly: AssemblyConfiguration("Licenced")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]
[assembly: AssemblyVersion("2.3.0.0")]
[assembly: AssemblyFileVersion("2.3.0.0")]
[assembly: AssemblyInformationalVersion("")]

